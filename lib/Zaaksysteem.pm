package Zaaksysteem;

use Zaaksysteem::Version;

=head1 NAME

Zaaksysteem - Case management system for improving business processes

=head1 SYNOPSIS

  # When cloned from source, you can start the local development server by running:

  bash$ ./script/zaaksysteem_server.pl

  ### Alternative, start fastcgi process with two instances

  bash# ./script/dev_fastcgi.pl

=head1 DESCRIPTION

Zaaksysteem is a case management solution for businesses. Althoug we primarily
focues on local government projects. The program is widely used in other SaaS
deployments

=head1 DOCUMENTATION

User documentation can be found on our wiki page:
L<http://wiki.zaaksysteem.nl/>

For development documentation, which gives insight in our API documentation. But also for
connecting Frontend code to our Backend, please start by reading our Manual:

L<Zaaksysteem::Manual>

This document gives insight in the API for Zaaksysteem

=head1 METHODS

B<Please, do NOT create any methods in this namespace, please use:>

L<Zaaksysteem::General::Actions>

You will be able to test them better, and documentation is in one place

=cut

use Moose;

use Digest::MD5 qw(md5_hex);
use File::Basename;
use IO::Socket;
use JSON::Path;
use Log::Log4perl::Catalyst;
use Log::Log4perl::MDC;
use Memory::Usage;
use Module::Find;
use Sys::Hostname;
use Syzygy::Object::Model;
use Time::HiRes qw/gettimeofday tv_interval/;
use Try::Tiny;

use Zaaksysteem::API::v1::Serializer;
use Zaaksysteem::Request;
use Zaaksysteem::Cache;
use Zaaksysteem::Environment;
use BTTW::Tools;
use Syzygy::Object::Model;

use Catalyst qw/
    ConfigLoader
    Static::Simple

    Authentication
    Authorization::Roles

    Cache::HTTP::Preempt

    Params::Profile

    I18N

    +Zaaksysteem::StatsD
    +Zaaksysteem::XML::Compile
    CustomErrorMessage

    Cache

    Session
    Session::State::Cookie
    +Zaaksysteem::Plugin::Session::Redis
/;

# Plugins which will be loaded based on env variables
my @additional_plugins = qw();

use CatalystX::RoleApplicator;

extends qw/Catalyst Zaaksysteem::General/;

#extends 'Catalyst';
#with 'CatalystX::LeakChecker';
#with 'CatalystX::LeakChecker';

__PACKAGE__->request_class('Zaaksysteem::Request');
__PACKAGE__->apply_request_class_roles(qw/
    Zaaksysteem::TraitFor::Request::Params
/);

# Configure the application.
#
# Note that settings in zaaksysteem.conf (or other external
# configuration file that you set up manually) take precedence
# over this when using ConfigLoader. Thus configuration
# details given here can function as a default configuration,
# with an external configuration file acting as an override for
# local deployment.

sub _assert_configfile {
    foreach ($ENV{ZAAKSYSTEEM_CONF}, '/etc/zaaksysteem/zaaksysteem.conf', 'etc/zaaksysteem.conf') {
        return $_ if (defined $_ && -f $_);
    }
    throw('Zaaksysteem', "No configfiles found, unable to start Zaaksysteem");
}

my $configfile = _assert_configfile;

__PACKAGE__->config(
    'Plugin::ConfigLoader' => {
        file => $configfile,
        driver => {
            # Make sure we can use arrays in our config as well
            'General' => { -ForceArray => 1 }
        },
    },
    'name'              => 'Zaaksysteem',
    'View::TT'          => {
        tpl       => 'zaak_v1',
        locale    => 'nl_NL',
    },
    'View::JSON' => {
        allow_callback  => 0,
        expose_stash    => [ 'json' ],
    },
    'View::JSONlegacy' => {
        allow_callback  => 0,    # defaults to 0
        expose_stash    => [ 'json' ],
    },
    'View::Email'          => {
        stash_key => 'email',
        template_prefix => 'tpl/zaak_v1/nl_NL',
        default => {
            content_type => 'text/plain',
            charset => 'utf-8',
            from    => 'info@zaaksysteem.nl',
            view        => 'TT',
        },
        sender => {
            'mailer'    => 'Sendmail'
        }
    },
    'default_view'      => 'TT',
    'Plugin::Static::Simple' => {
        ignore_extensions => [ qw/tmpl tt tt2/ ],
    },
    'Plugin::Authentication' => {
        default_realm => 'zaaksysteem',

        zaaksysteem => {
            interface => 'authldap',
            credential => {
                class => "+Zaaksysteem::Auth::Credential::LDAP"
            },
            store => {
                class => "+Zaaksysteem::Auth::Store"
            }
        },

        api => {
            interface => 'authldap',
            credential => {
                class => "+Zaaksysteem::Auth::Credential::API"
            },
            store => {
                class => "+Zaaksysteem::Auth::Store"
            }
        },

        saml => {
            interface => 'authldap',
            credential => {
                class => "+Zaaksysteem::Auth::Credential::SAML"
            },
            store => {
                class => "+Zaaksysteem::Auth::Store"
            }
        },

        platform => {
            interface => 'authldap',
            username => 'admin',
            credential => {
                class => '+Zaaksysteem::Auth::Credential::Platform'
            },
            store => {
                class => '+Zaaksysteem::Auth::Store'
            }
        },

        token => {
            interface => 'authtoken',
            credential => {
                class => '+Zaaksysteem::Auth::Credential::Token'
            },
            store => {
                class => '+Zaaksysteem::Auth::Store'
            }
        }
    },

    'Plugin::Cache::HTTP::Preempt' => {},

    recaptcha => {
        pub_key  => '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI',
        priv_key => '6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe',
        options  => { theme => 'clean', lang => 'nl' },
        version  => 'v2',
    },

    # So this need not to be in the configuration file
    'Model::DB' => {
        connect_info => {
            dsn            => "dbi:Pg:dbname=nonexistent",
            pg_enable_utf8 => 1
        }
    },

    'custom-error-message' => {
        'error-template' => 'error.tt',
    },

    'Plugin::Session' => {
        expires => $ENV{ ZS_SESSION_TIMEOUT } || 7200
    },
    'performance_per_action' => qr|(?:^api/v1)\|(?:^api/v0)\|(?:^file/)\|(?:zaak/action/data)\|(?:event/list)\|(?:api/object/search)\|(?:zaak/email/send_mail)\|(?:api/case/file)\|(?:api/message)\|(?:zaak/checklist)|,
    'performance_per_action_correction' => 15,
);

__PACKAGE__->mk_classdata($_) for qw/
    _additional_static
    customer
    class_prefixes_loaded_on_startup
/;

### Turn off stacktraces completely on debug output. Prevent throwing authentication data around.
### dump_these will normally throw out the contents of "c->stash" and "c->config". We
### reset it to {} to prevent outputting them. We stay away from "c->req" and "c->res". This is public
### data anyway.
around 'dump_these' => sub {
    my $next = shift;
    my $c    = shift;

    my @dump = $c->$next(@_);

    for my $dump (@dump) {
        if ($dump->[0] =~ /Stash|Config/) {
            $dump->[1] = {};
        }
    }

    return @dump;
};

__PACKAGE__->class_prefixes_loaded_on_startup([qw/
    Zaaksysteem::Object
    Zaaksysteem::API::v1
/]);


__PACKAGE__->set_zs_version($VERSION);
__PACKAGE__->config->{NEN2081_VERSION} = $NEN2082_VERSION;
__PACKAGE__->config->{config_directory} = dirname($configfile);

__PACKAGE__->setup(@additional_plugins);

__PACKAGE__->init_log4perl;

__PACKAGE__->load_xml_compile_classes;

__PACKAGE__->config->{'Plugin::Captcha'} = {
    session_name => 'captcha_string',
    new          => {
        width     => 300,
        height    => 100,
        scramble  => 1,
        ptsize    => 34,
        frame     => 2,
        rndmax    => 5,
        thickness => 1,
        lines     => 25,
        color     => '#FFCC00',
    },
    create   => ['ttf', 'circle', '#3E8FA4', '#999',],
    particle => [1500],
    out => { force => 'jpeg' }
};

## Preload Serializer
Zaaksysteem::API::v1::Serializer->preload;
Zaaksysteem::Object::ValueModel->preload;
Syzygy::Object::Model->get_instance->preload;

# For PIP of people with a lot of cases
$Template::Directive::WHILE_MAX = 10000;

sub _setup_authentication_interface {
    my $c           = shift;

    my $realm     = $c->get_auth_realm('default');
    my $module    = $realm->{config}{interface};
    my $interface = $c->model('DB::Interface')->find_by_module_name($module);

    unless($interface) {
        throw('auth/interface', "Unable to locate interface with module '$module'");
    }

    $realm->credential->interface($interface);

    $c->get_auth_realm('api')->credential->interface($interface);
    $c->get_auth_realm('saml')->credential->interface($interface);

    $c->model('DB')->schema->default_resultset_attributes->{ current_user } = $c->model('DB')->schema->current_user(undef);
}

sub _finish_graphing {
    my $c           = shift;
    my $t0          = shift;

    my $status      = $c->response->status;
    if (scalar @{ $c->error }) {
        $status = 500;
    }

    my $result      = $c->stats->elapsed;

    $c->statsd->timing('request.time', ($result*1000));
    Log::Log4perl::MDC->put('request_time', ($result));
    Log::Log4perl::MDC->put('upstream_time', ($result));

    $c->statsd->increment('response.status.' . $status, 1);
}


sub _mu_mdc {
    my ($c, $mu, $what) = @_;

    my $key;
    if ($what eq 'vsz') {
        $key = 2;
    }
    elsif ($what eq 'rss') {
        $key = 3;
    }
    elsif ($what eq 'shared') {
        $key = 4;
    }
    elsif ($what eq 'code') {
        $key = 5;
    }
    elsif ($what eq 'data') {
        $key = 6;
    }
    $c->set_log_context("${what}_begin", $mu->[0][$key]);
    $c->set_log_context("${what}_end",   $mu->[-1][$key]);
    $c->set_log_context("${what}_diff",  $mu->[-1][$key] - $mu->[0][$key]);
}

sub _dispatch_profile_memory {
    my ($c, $mu, $end) = @_;

    return unless $ENV{ZS_MEMORY_USAGE};

    my $authaction = $c->req->action;
    $authaction =~ s|^/|| unless $authaction eq '/';
    $authaction = lc($authaction);

    my $key = join(":",
        $end ? 'begin' : 'end',
        $authaction, $c->session->{request_id}, $$
    );

    $mu->record($key);

    if ($end) {
        if ($ENV{'ZS_MEMORY_USAGE'} > 1) {
            if ($mu->[-1]->[3] - $mu->[0]->[3] > ($ENV{'ZS_MEMORY_USAGE'} * 1024)) {
                $c->log->info('Process: ' . $$ . "\n" . $mu->report());
            }
        }
        $c->set_log_context('process_id', $$);
        # Only crave RSS and data/stack size
        foreach (qw(rss data)) {
            $c->_mu_mdc($mu, $_)
        }
    }
    return;
}

sub _dispatch_logger {
    my $c       = shift;

    BTTW::Tools::set_logger($c->log);

    my $session_id = substr($c->get_session_id()|| '', -6);

    # Generate a request ID
    $c->stash->{request_id} = sprintf(
        "%s-%s-%s",
        ($c->config->{logging_id} || ''),
        $session_id,
        substr(md5_hex(($c->config->{gemeente_id} || ''), time, rand), -6),
    );
    $c->session->{request_id} = $c->stash->{request_id};
    $Zaaksysteem::Environment::REQUEST_ID = $c->stash->{request_id};

    Log::Log4perl::MDC->put('instance_hostname',$c->config->{instance_hostname});

    Log::Log4perl::MDC->put('session_id',       $session_id);
    Log::Log4perl::MDC->put('logging_id',       $c->config->{logging_id});
    Log::Log4perl::MDC->put('request_id',       $c->stash->{request_id});
    Log::Log4perl::MDC->put('touch_case',           0);

}

sub _dispatch_verify_security {
    my $c       = shift;

    return 1 unless $c->sessionid;

    if (!$c->session->{__session_host}) {
        $c->session->{__session_host} = lc($c->req->uri->host);
        return 1;
    }

    ### Security breach, session copied, ABORT.. *HONK* *HONK*...DEFCON 1
    if ($c->session->{__session_host} ne lc($c->req->uri->host)) {
        $c->log->error('Security warning: URI host doesn\'t match the original session origin host from IP, origination IP: ' . $c->req->address);
        $c->delete_session('Invalid session_host');
        $c->res->redirect($c->uri_for('/'));

        return;
    }

    return 1;
}

around 'dispatch' => sub {
    my $orig    = shift;
    my $c       = shift;

    my $t0_part = my $t0 = Zaaksysteem::StatsD->statsd->start;

    Log::Log4perl::MDC->remove();

    Log::Log4perl::MDC->put('remote_addr', $c->get_client_ip);
    Log::Log4perl::MDC->put('hostname',    $c->req->uri->host);
    Log::Log4perl::MDC->put('zs_version',  $VERSION);

    $c->stash->{__request_t0} = $t0;

    ### First, check for available customer
    try {
        $c->customer_instance;
    }
    catch {
        $c->log->error($_);
        $c->res->status(523);

        $c->res->body(qq{
            <html><head><title>523 Origin is Unreachable - No zaaksysteem found for this host</title></head><body bgcolor="white"><center><h1>501 Not implemented</h1></center><hr><center>nginx</center></body></html>
        });
        $c->detach();
        return;
    };

    $c->_init_statsd;

    Zaaksysteem::StatsD->statsd->end('dispatch.customer_instance_loaded.time', $t0_part);
    $t0_part = Zaaksysteem::StatsD->statsd->start;

    ### Timer start
    my $schema  = $c->model('DB')->schema;

    my $mu = $ENV{ZS_MEMORY_USAGE} ? Memory::Usage->new() : undef;

    $c->_dispatch_logger();
    $c->_dispatch_profile_memory($mu);
    $c->dispatch_query_statistics($schema, 1);

    ### Security checks
    unless ($c->_dispatch_verify_security) {
        $c->log->_flush;
        Zaaksysteem::StatsD->statsd->end('dispatch.loggingflush.time', $t0_part);
        $c->_end_dispatch($schema, $mu);
        return;
    };

    Zaaksysteem::StatsD->statsd->end('dispatch.logger_and_stats_loaded.time', $t0_part);
    $t0_part = Zaaksysteem::StatsD->statsd->start;

    try {
        ### Setup authentication
        $c->_setup_authentication_interface();
    } catch {
        $c->log->warn(sprintf(
            'Authentication interface setup exception: %s',
            $_
        ));

        die $_;
    };

    Zaaksysteem::StatsD->statsd->end('dispatch.authentication_interface_loaded.time', $t0_part);
    $t0_part = Zaaksysteem::StatsD->statsd->start;

    if ($c->user_exists) {
        Log::Log4perl::MDC->put('remote_user', $c->user->username);
    }
    else {
        Log::Log4perl::MDC->put('remote_user', 'no_user');
    }

    $c->log->debug(sprintf('PID: %s - request: "%s"', $$, $c->req->uri));

    $c->log->_flush;
    Zaaksysteem::StatsD->statsd->end('dispatch.loggingflush.time', $t0_part);

    $t0_part = Zaaksysteem::StatsD->statsd->start;
    $c->get_customer_info;
    Zaaksysteem::StatsD->statsd->end('dispatch.customer_info_loaded.time', $t0_part);

    $t0_part = Zaaksysteem::StatsD->statsd->start;
    my $ret = $c->$orig(@_);
    Zaaksysteem::StatsD->statsd->end('dispatch.dispatched.time', $t0_part);

    $c->_end_dispatch($t0, $schema, $mu);
    return $ret;
};

sub _end_dispatch {
    my ($c, $t0, $schema, $mu) = @_;

    my $t0_part = Zaaksysteem::StatsD->statsd->start;

    $c->dispatch_query_statistics($schema, 0);

    undef $Zaaksysteem::Environment::REQUEST_ID;
    $schema->clear;
    $schema->disconnect;

    $c->_dispatch_profile_memory($mu, 1);
    $c->dispatch_performance_per_action();

    Log::Log4perl::MDC->put('end_dispatch', 'true'),
    $c->log->info(
        sprintf(
            "Finished action '%s' in %d milliseconds. Querycount: %d",
            Log::Log4perl::MDC->get('action_path'),
            $c->stats->elapsed*1000,
            (Log::Log4perl::MDC->get('sql_query_count') || 0),
        )
    );

    Zaaksysteem::StatsD->statsd->end('dispatch.cleanup.time', $t0_part);
    Zaaksysteem::StatsD->statsd->timing('catalyst.finish.time', ($c->stats->elapsed*1000));
    Zaaksysteem::StatsD->statsd->timing('catalyst.dispatch.time', $t0);
}

sub dispatch_performance_per_action {
    my ($c) = @_;

    my $elapsed = $c->stats->elapsed*1000 + ($c->config->{performance_per_action_correction} || 0);

    my $authaction = lc($c->req->action);
    $authaction =~ s|^/|| unless $authaction eq '/';

    $c->set_log_context('duration', $$);
    if ($authaction =~ $c->config->{performance_per_action}) {
        $authaction =~ s|/|_|g;
        $authaction =~ s/^[^0-9a-zA-Z_-]+$//g;

        my $metric = 'performance_' . $authaction;
        Zaaksysteem::StatsD->statsd->timing($metric, $elapsed);
    }
}

### We want to make sure we can send the upstream and request time via the headers to our
### zaaksysteem-frontend for measuring the difference between perl time and request time. This
### way we can calculate the time our linux kernel is queueing our connections
around 'finalize_headers' => sub {
    my $orig    = shift;
    my $c       = shift;

    ### Dispatch!
    $c->_finish_graphing($c->stash->{__request_t0});

    $c->res->header('ZS-Req-Session-Id',        Log::Log4perl::MDC->get('session_id'));
    $c->res->header('ZS-Req-ID',                Log::Log4perl::MDC->get('request_id'));
    $c->res->header('ZS-Req-User',              Log::Log4perl::MDC->get('remote_user'));
    $c->res->header('ZS-Req-Instance-Hostname', Log::Log4perl::MDC->get('instance_hostname'));
    $c->res->header('ZS-Req-Action-Path',       Log::Log4perl::MDC->get('action_path'));
    $c->res->header('ZS-Version',               Log::Log4perl::MDC->get('zs_version'));
    $c->res->header('ZS-Upstream-Time',         Log::Log4perl::MDC->get('upstream_time'));

    return $c->$orig(@_);
};

=pod

Implements case action 'queue coworker changes'. This makes exclusive editing
rights possible for case behandelaars, all other users will be forced to submit
to their approval. (except admins)

Returns a true value when the case is using that action, and the current user
is not the behandelaar (owner).

=cut

sub zs_cache {
    my $c   = shift;

    return $c->stash->{__zs_cache} if ref $c->stash->{__zs_cache} eq 'Zaaksysteem::Cache';

    $c->stash->{__zs_cache_store} = {};

    return Zaaksysteem::Cache->new(storage => $c->stash->{__zs_cache_store});
}

sub query_session {
    my $self = shift;

    return JSON::Path->new(shift)->value($self->session);
}

=head2 _ensure_zs_classes_are_loaded

Loops over the classdata accessor C<class_prefixes_loaded_on_startup> to find every prefix, and
loads every module below this prefix using C<Catalyst::Utils::ensure_class_loaded>

=cut

sub _ensure_zs_classes_are_loaded {
    for my $prefix (@{ __PACKAGE__->class_prefixes_loaded_on_startup }) {
        __PACKAGE__->log->trace("Preloading classes below " . $prefix) if __PACKAGE__->debug;
        for my $module (Module::Find::findallmod($prefix)) {
            Catalyst::Utils::ensure_class_loaded($module);
        }
    }
}

## Run on catalyst setup time
_ensure_zs_classes_are_loaded();

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Install>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 check_queue_coworker_changes

TODO: Fix the POD

=cut

=head2 format_error

TODO: Fix the POD

=cut

=head2 query_session

TODO: Fix the POD

=cut

=head2 zs_cache

TODO: Fix the POD

=cut
