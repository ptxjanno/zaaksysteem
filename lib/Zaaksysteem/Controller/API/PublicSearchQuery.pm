package Zaaksysteem::Controller::API::PublicSearchQuery;

use Moose;
use namespace::autoclean;
use JSON;

use BTTW::Tools;
use Zaaksysteem::Search::ZQL;

use Zaaksysteem::Types qw/UUID/;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head2 index

Base controller for SuperSaaS API-calls

=cut

=head2 public_search_base

Base controller for public_search_base

=cut

sub public_search_base : Chained('/api/base') : PathPart('public_search') : CaptureArgs(1): DisableACL {
    my ($self, $c, $interface_id) = @_;
    my $raw_search_query;

    throw('api/public_search/invalid_interface_id', 'Invalid ID given') unless $interface_id =~ /^\d+$/;

    my $interface           = $c->model('DB::Interface')->search_active(
        {
            id          => $interface_id,
            module      => 'publicsearchquery'
        }
    )->first;

    if (!$interface) {
        throw('api/public_search/not_found', 'Public search query not found');
    }

    my $search_query_id     = $interface->get_interface_config->{query}->{id};
    $raw_search_query       = $c->model('Object')->retrieve(uuid => $search_query_id) if $search_query_id;

    if (!$raw_search_query) {
        throw('api/public_search/no_saved_search', 'Saved search not found')
    }


    my $saved_search          = from_json($raw_search_query->{query});
    my $zql                   = Zaaksysteem::Search::ZQL->new($saved_search->{zql});

    ### Retrieve user
    my $user                  = $c->model('DB::Subject')->search_active({ 'me.id' => $interface->get_interface_config->{user}->{id} })->first;
    if (!$user) {
        throw('api/public_search/no_user_found', 'No user to query ZQL found');
    }

    my $object                = Zaaksysteem::Object::Model->new(
        schema      => $c->model('DB')->schema,
        user        => $user,
    );

    $c->stash->{ zql }        = $zql;
    $c->stash->{ search_rs }  = $zql->apply_to_resultset($object->rs);
}


=head2 search

Returns a resultset of objects for the given type. Now, only the object type
case is here to use.

=cut

sub search : Chained('public_search_base') : PathPart('search') : Args(0) : DisableACL: ZAPI {
    my ($self, $c) = @_;

    $c->forward('/api/object/search_select');

    ### Protect attributes from going to the outside
    # $c->stash->{zapi}->object_requested_attributes([qw/case.status/]);
}


=head2 configuration_base

Public URL for getting search configuration

=cut

sub configuration_base : Chained('/api/base') : PathPart('public_search') : CaptureArgs(1): DisableACL {}


=head2 configuration

Public URL for getting search configuration

=cut

sub configuration : Chained('configuration_base') : PathPart('config') : Args(1) : DisableACL: ZAPI {
    my ($self, $c) = @_;

    $c->forward('/api/search/configuration', ['case']);
}

=head2 file_base

Public URL for getting files via de public search API

=cut

sub file_base : Chained('public_search_base') : PathPart('file') : CaptureArgs(1): DisableACL {
    my ($self, $c, $filestore_id) = @_;

    $c->detach unless UUID->check($filestore_id);

    my $filestore   = $c->model('DB::Filestore')->search({ uuid => $filestore_id })->first;

    if (!$filestore) {
        throw('api/public_search/no_file_found', 'No file found by UUID: ' . $filestore_id);
    }

    my $file        = $filestore->files->first;

    if (!$file || !$file->get_column('case_id')) {
        throw('api/public_search/no_file_found', 'No file found by UUID: ' . $filestore_id);
    }

    if (!$c->stash->{ search_rs }->search({object_id => $file->get_column('case_id') })->count) {
        throw('api/public_search/permission_denied', 'Insufficient permissions');
    }

    $c->stash->{filestore} = $filestore;


}

=head2 file

Public URL for getting files via de public search API

=cut

sub file : Chained('file_base') : PathPart('') : Args(0) : DisableACL {
    my ($self, $c) = @_;

    $c->forward('/api/object/serve_file', [$c->stash->{filestore}]);
}


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
