package Zaaksysteem::Controller::API::v1::App::Meeting;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

=head1 NAME

Zaaksysteem::Controller::API::V1::App::Meeting - Meeting App specific api calls

=head1 DESCRIPTION

This is the controller API class for C<api/v1/app/app_meeting>. Extensive documentation about this
API can be found in:

L<Zaaksysteem::Manual::API::V1::App::Meeting>

Extensive tests about the usage via the JSON API can be found in:

L<TestFor::Catalyst::Controller::API::V1::App::Meeting>

=cut

use BTTW::Tools;
use Zaaksysteem::Types qw(UUID);

use constant MODULE_NAME => 'app_meeting';

sub BUILD {
    my $self = shift;

    $self->add_api_control_module_type('app', 'meeting');
    $self->add_api_context_permission('public_access');
}

=head1 ACTIONS

=head2 base

Reserves the C<api/v1/app/app_meeting> routing namespace.

=cut

sub base : Chained('/api/v1/app/base') : PathPart('app_meeting') : CaptureArgs(0) { }

=head2 case_base

Reserves the C</api/v1/app/app_meeting/case/[CASE_UUID]> routing namespace

=cut

sub case_base : Chained('base') : PathPart('case') : CaptureArgs(1) {
    my ($self, $c, $case_uuid) = @_;

    unless (UUID->check($case_uuid)) {
        throw('api/v1/app/invalid_case_id', 'Invalid uuid given')
    }

    ### Check for valid interface_id and existing of interface
    my $app = $c->model('DB::Interface')->search_active({
        module => MODULE_NAME
    })->first;

    if (!$app) {
        throw('api/v1/app/app_meeting/not_found', 'No app found by name: meeting');
    }

    if ($app->get_interface_config->{access} ne 'rw') {
        throw('api/v1/app/app_meeting/read_only', sprintf(
            'Interface "%s" (%s) configuration error, cannot be read-only',
            $app->name,
            $app->uuid
        ));
    }

    my $object_data = $c->model('DB::ObjectData')->search({
        object_class    => 'case',
        uuid            => $case_uuid,
    })->first;
    my $case        = $c->model('DB::Zaak')->find($object_data->object_id);

    throw('api/v1/app/app_meeting/case/not_found', sprintf(
        'Case not found by uuid: %s',
        $case_uuid
    )) unless ($object_data && $case);

    $c->stash->{zaak}            = $case;
    $c->stash->{case}            = $object_data;   
}

=head2 update_attributes

=head3 URL Path

C</api/v1/app/app_meeting/case/[CASE_UUID]/update_attributes>

=cut

define_profile update_attributes => (
    required    => {
        values  => 'HashRef',
    }
);

sub update_attributes : Chained('case_base') : PathPart('update_attributes') : Args(0) : RW {
    my ($self, $c)  = @_;
    my $params      = assert_profile($c->req->params)->valid;

    unless ($c->can_change()) {
        throw(
            'api/v1/app/app_meeting/no_permissions',
            'You have no permission to edit this case'
        );
    }

    $c->stash->{zaak}->zaak_kenmerken->update_fields_authorized({
        new_values  => $params->{values},
        zaak        => $c->stash->{zaak},
    });

    $c->stash->{zaak}->_touch();

    ### Reretrieve object
    $c->stash->{case}   = $c->model('DB::ObjectData')->find($c->stash->{case}->uuid);

    $c->detach('/api/v1/case/get');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
