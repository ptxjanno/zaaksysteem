package Zaaksysteem::Controller::Upload;

use Moose;

use Data::Dumper;

BEGIN { extends 'Zaaksysteem::Controller' }

sub index : Path : Args(0) {
    my ($self, $c) = @_;

    $c->forward('upload');

    $c->stash->{json} = $c->stash->{upload_result};
    $c->detach('Zaaksysteem::View::JSONlegacy');
}


sub upload : Local {
    my ($self, $c) = @_;

    my $uploaded_files = {};
    my $result = {};
    my $filestore = $c->model('DB::Filestore');

    for my $upload (map { $c->req->upload($_) } keys %{ $c->req->uploads }) {
        my $filestore_obj = $filestore->filestore_create({
            original_name    => $upload->filename,
            file_path        => $upload->tempname,
            ignore_extension => 1,
        });

        if($filestore_obj && $filestore_obj->id) {
            $uploaded_files->{
                $filestore_obj->id
            } = $upload;

            $result->{filename}     = $filestore_obj->original_name;
            $result->{uuid}         = $filestore_obj->uuid;
        }
        $c->stash->{uuid} = $filestore_obj->uuid;
    }

    $c->stash->{upload_result} = $result;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 index

TODO: Fix the POD

=cut

=head2 upload

TODO: Fix the POD

=cut

