package Zaaksysteem::Controller::Plugins::Bedrijfid;
use Moose;

use Clone 'clone';

use Zaaksysteem::Constants qw/
    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_DIGID
    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEDRIJFID

    VALIDATION_CONTACT_DATA
    VALIDATION_EXTERNAL_CONTACT_DATA
/;

BEGIN { extends 'Zaaksysteem::Controller' }

=head2 login

Process company login for PIP or /form (BedrijfID/eHerkenning).

=cut

sub login : Chained('/') : PathPart('auth/bedrijfid'): Args() {
    my ($self, $c, $loginpage) = @_;

    if (
        !$loginpage &&
        !(
            $c->session->{_saml} &&
            $c->session->{_saml}->{succes} &&
            $c->session->{_saml}->{used_profile} eq 'eherkenning'
        )
    ) {
        $c->stash->{ saml_interfaces } = [
            $c->model('DB::Interface')->search_module('samlidp', '$.login_type_company')
        ];

        $c->stash->{ enable_mintlab_id } = $c->model('DB::Config')->get('enable_mintlab_id');

        $c->create_zs_login_token();

        $c->stash->{template} = 'plugins/bedrijfid/info.tt';
        $c->stash->{ page_title } = 'Inloggen als bedrijf';

        $c->detach;
    }

    if (
        $c->model('Plugins::Bedrijfid')->succes ||
        (
            $c->session->{_saml} &&
            $c->session->{_saml}->{succes} &&
            ($c->session->{_saml}->{used_profile} =~ m[eherkenning|spoof])
        )
    ) {
        if ($c->model('Plugins::Bedrijfid')->success_endpoint) {
            $c->res->redirect(
                $c->model('Plugins::Bedrijfid')->success_endpoint
            );
        } else {
            $c->res->redirect($c->uri_for('/pip'));
        }

        $c->res->body('');
        $c->detach;
    }

    if ($c->req->params->{do_auth} && uc($c->req->method) eq 'POST') {
        $c->assert_zs_login_token();

        my $redir = $c->model('Plugins::Bedrijfid')->authenticate(
            login           => $c->req->params->{login},
            password        => $c->req->params->{password},
            success_endpoint    => $c->req->params->{success_endpoint}
        );

        if ($redir) {
            $c->res->redirect($redir);
            $c->res->body('');
            $c->detach;
        }
    }

    ### In case of an XHR, and we get here...define we are not logged in
    if ($c->req->is_xhr) {
        $c->res->status('401');
    }

    $c->create_zs_login_token();
    $c->stash->{template} = 'plugins/bedrijfid/login.tt';
    $c->stash->{ page_title } = sprintf('Inloggen met %sID', $c->config->{ gemeente }{ naam_kort });
}


sub logout : Chained('/') : PathPart('auth/bedrijfid/logout'): Args() {
    my ($self, $c) = @_;

    $c->model('Plugins::Bedrijfid')->logout;

    $c->delete_session;

    $c->res->redirect($c->uri_for('/pip'));
    $c->detach;
}

sub wachtwoord : Chained('/zaak/base') : PathPart('update/bedrijfid'): CaptureArgs(0) {
    my ($self, $c) = @_;

    $c->check_any_user_permission(qw/contact_nieuw contact_search/);

    return unless (
        $c->stash->{zaak} &&
        $c->stash->{zaak}->aanvrager &&
        $c->stash->{zaak}->aanvrager_object->btype eq 'bedrijf'
    );

    $c->stash->{betrokkene} = $c->stash->{zaak}->aanvrager_object;
}

sub randomPassword {
    my $self = shift;

    my $password;
    my $_rand;

    my $password_length = $_[0];
        if (!$password_length) {
            $password_length = 10;
        }

    # Really? What is this... 1987?
    my @chars = split(" ",
        '
        a b c d e f g h i j k l m n o
        p q r s t u v w x y z - _ % # |
        0 1 2 3 4 5 6 7 8 9
        '
    );

    srand;

    for (my $i=0; $i <= $password_length ;$i++) {
        $_rand = int(rand 41);
        $password .= $chars[$_rand];
    }
    return $password;
}


sub wachtwoord_wijzig : Chained('wachtwoord') : PathPart('wijzig'): Args(0) {
    my ($self, $c) = @_;

    $c->assert_any_zaak_permission('zaak_beheer','zaak_edit');

    ### Post
    if (
        %{ $c->req->params } &&
        $c->req->params->{confirmed}
    ) {
        #$c->res->redirect(
        #    $c->uri_for('/')
        #);

        ### Confirmed
        my $newpassword = $self->randomPassword(8);

        $c->stash->{betrokkene}->password($newpassword);

        $c->push_flash_message('Wachtwoord voor bedrijf "'
            . $c->stash->{betrokkene}->naam . '" succesvol gewijzigd'
        );

        $c->res->redirect(
            $c->uri_for(
                '/zaak/' . $c->stash->{zaak}->nr
            )
        );
        $c->detach;
    }

    if ($c->stash->{betrokkene}->has_password) {
        $c->stash->{confirmation}->{message}    =
            'Dit bedrijf heeft eerder een wachtwoord overhandigd gekregen.'
            .' Hiermee zal het bestaande wachtwoord worden gewijzigd.'
    } else {
        $c->stash->{confirmation}->{message}    =
            'Er is nog niet eerder een wachtwoord overhandigd.'
            .' Dit zou niet mogen voorkomen'
            .'<b>Door op bevestigen te klikken geeft u aan de nodige'
            .' identificatie te hebben gecontroleerd.</b>';
    }


    $c->stash->{confirmation}->{type}       = 'yesno';
    $c->stash->{confirmation}->{uri}        =
        $c->uri_for(
            '/zaak/' . $c->stash->{zaak}->nr
            .'/update/bedrijfid/wijzig'
        );


    $c->forward('/page/confirmation');
    $c->detach;
}



sub _zaak_create_security : Private {
    my ($self, $c) = @_;

    if (
        ($c->req->params->{authenticatie_methode} // '') eq 'bedrijfid' ||
        ($c->session->{_zaak_create}->{extern}->{verified} // '')  eq 'bedrijfid'
    ) {
        if (
            $c->model('Plugins::Bedrijfid')->succes ||
            (
                $c->session->{_saml} &&
                $c->session->{_saml}->{uid} &&
                $c->session->{_saml}->{used_profile} =~ m[eherkenning|spoof]
            )
        ) {
            $c->session->{_zaak_create}->{extern} = {};

            ### Check if we are allowed to crate this zaaktype
            $c->session->{_zaak_create}->{extern}->{aanvrager_type}
                = 'niet_natuurlijk_persoon';
            $c->session->{_zaak_create}->{extern}->{verified}
                = 'bedrijfid';

            if ($c->model('Plugins::Bedrijfid')->login) {
                $c->session->{_zaak_create}->{extern}->{id}
                    = $c->model('Plugins::Bedrijfid')->login;
            } elsif ($c->session->{_saml}->{used_profile} =~ m[eherkenning|spoof]) {
                $c->session->{_zaak_create}->{extern}->{id}
                    = $c->session->{_saml}->{uid};
            }

            $c->stash->{aanvrager_type} = 'niet_natuurlijk_persoon'
        } else {
            my $arguments = {};
            $arguments->{'authenticatie_methode'} = 'bedrijfid' if ($c->req->params->{authenticatie_methode});
            $arguments->{'ztc_aanvrager_type'} = 'niet_natuurlijk_persoon' if ($c->req->params->{ztc_aanvrager_type});
            $arguments->{'sessreset'} = 1 if ($c->req->params->{sessreset});
            $arguments->{'zaaktype_id'} = $c->req->params->{zaaktype_id} if (
                $c->req->params->{zaaktype_id} &&
                $c->req->params->{zaaktype_id} =~ /^\d+$/
            );

            $c->res->redirect(
                $c->uri_for(
                    '/auth/bedrijfid',
                    {
                        success_endpoint    => $c->uri_for(
                            '/zaak/create/webformulier/',
                            $arguments,
                        )
                    }
                )
            );

            ### Wipe out externe authenticatie
            if (
                $c->session->{_zaak_create}->{extern} &&
                $c->session->{_zaak_create}->{verified} eq 'bedrijfid'
            ) {
                delete($c->session->{_zaak_create}->{extern});
            }

            $c->detach;
        }
    } else {

        ### Geen bedrijfid, stop here
        return;
    }

    ### Save aanvrager data
    $c->forward('_zaak_create_aanvrager');
}

sub _zaak_create_aanvrager : Private {
    my ($self, $c) = @_;

    return unless (
        $c->req->params->{aanvrager_update}
    );

    my $zaaktype = $c->model('DB::Zaaktype')->find($c->session->{_zaak_create}{zaaktype_id});
    my $zaaktype_node = $zaaktype->zaaktype_node_id;

    my $callerclass     = 'Zaaksysteem::Betrokkene::Object::Bedrijf';

    my $external_validation_profile = $c->user_exists
        ? VALIDATION_CONTACT_DATA
        : VALIDATION_EXTERNAL_CONTACT_DATA->($zaaktype_node);

    ### Only validate contact, which are all optional
    my $profile;
    if ($c->req->params->{contact_edit}) {
        $profile = $external_validation_profile;
    } else {
        ### Get profile from Model
        my $oldprofile         = $c->get_profile(
            'method'=> 'create',
            'caller' => $callerclass
        ) or die('Terrible die here');

        $profile = clone($oldprofile);

        my @required_fields = grep {
            $_ ne 'vestiging_postcodewoonplaats' ||
            $_ ne 'vestiging_adres'
        } @{ $profile->{required} };

        push(@required_fields, 'rechtsvorm');

        $profile->{required} = \@required_fields;

        ### MERGE
        my $contact_profile = $external_validation_profile;
        while (my ($key, $data) = each %{ $contact_profile }) {
            if ($key eq 'msgs') {
                $profile->{msgs} = $data;
                next;
            }

            unless ($profile->{$key}) {
                $profile->{$key} = $data;
                next;
            }

            if (UNIVERSAL::isa($data, 'ARRAY')) {
                push(@{ $profile->{$key} }, @{ $data });
                next;
            }

            if (UNIVERSAL::isa($data, 'HASH')) {
                while (my ($datakey, $dataval) = each %{ $data }) {
                    $profile->{$key}->{$datakey} = $dataval;
                }
                next;
            }
        }
    }

    Params::Profile->register_profile(
        method => '_zaak_create_aanvrager',
        profile => $profile,
    );

    if ($c->req->is_xhr) {
        $c->zvalidate;
        $c->detach;
    }

    my $dv      = $c->zvalidate;
    return unless ref($dv);

    return unless $dv->success;

    if ($c->req->params->{aanvrager_edit}) {
        $c->session->{_zaak_create}->{aanvrager_update} = $dv->valid;
    } elsif ($c->req->params->{contact_edit}) {
        for (qw/npc-email npc-telefoonnummer npc-mobiel/) {
            if (defined($c->req->params->{ $_ })) {
                $c->session->{_zaak_create}->{ $_ } =
                    $c->req->params->{ $_ };
            }
        }
    }
}

sub _zaak_create_load_externe_data : Private {
    my ($self, $c) = @_;

    return unless $c->session->{_zaak_create}->{extern}->{verified} eq 'bedrijfid' && $c->session->{_zaak_create}->{aanvrager_update};

    return if ($c->session->{_zaak_create}{ztc_aanvrager_id});

    if($c->req->params->{aanvrager_update}) {
        ### Strip contact data
        my $id = $c->model('Betrokkene')->create(
            'bedrijf',
            {
                %{ $c->session->{_zaak_create}->{aanvrager_update} },
                'authenticated'   => 0,
                'authenticatedby' => ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEDRIJFID,
            }
        );

        $c->session->{_zaak_create}{ztc_aanvrager_id}
                                        = 'betrokkene-bedrijf-' .  $id;
    }
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 VALIDATION_CONTACT_DATA

TODO: Fix the POD

=cut

=head2 VALIDATION_EXTERNAL_CONTACT_DATA

TODO: Fix the POD

=cut

=head2 ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEDRIJFID

TODO: Fix the POD

=cut

=head2 logout

TODO: Fix the POD

=cut

=head2 randomPassword

TODO: Fix the POD

=cut

=head2 wachtwoord

TODO: Fix the POD

=cut

=head2 wachtwoord_wijzig

TODO: Fix the POD

=cut

