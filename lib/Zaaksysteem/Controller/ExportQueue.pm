package Zaaksysteem::Controller::ExportQueue;

use Moose;
use namespace::autoclean;
use Try::Tiny;

BEGIN { extends 'Zaaksysteem::Controller' }

sub base : Chained('/') : PathPart('exportqueue') : CaptureArgs(0) {
    my ( $self, $c ) = @_;

    $Template::Stash::PRIVATE = undef;
    $c->stash->{queue} = $c->model('ExportQueue');
    $c->stash->{template} = 'export_queue.tt';
}

sub list : Chained('base') : PathPart('') : Args(0) {
     my ($self, $c) = @_;
}

sub upload : Chained('base') : PathPart('upload') : Args(0) {
    my ($self, $c) = @_;
    $c->stash->{template} = 'export_queue_upload.tt';
}

sub delete : Chained('base') : PathPart('delete') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{queue}->delete_expired_exports();
}

sub process_upload : Chained('base') : PathPart('process_upload') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{template} = 'export_queue_upload.tt';

    if ($c->request->parameters->{form_submit} ne 'yes') {
        return;
    }

    my $upload = $c->request->upload('my_file');
    die "Unable to complete upload without file" unless $upload;

    try {
        $c->stash->{queue}
            ->add_as_export($upload->tempname, $upload->filename);
    }
    catch {
        die $_;
    };
}

sub download_file : Chained('base') : PathPart('download') : Args(1) {
    my ($self, $c, $token) = @_;

    try {
        my $entry = $c->stash->{queue}->get_export_item_by_token($token);
        $c->serve_filestore_streaming($entry->filestore_id);
        return;
    }
    catch {
        $c->log->info("$_");
        $c->stash->{error} = "$_";
        return;
    };
}

sub expire : Chained('base') : PathPart('expire') : Args(1) {
    my ($self, $c, $token) = @_;

    try {
        my $entry = $c->stash->{queue}->get_export_item_by_token($token);
        $entry->update({expires => \"NOW() - INTERVAL '1h'"});
    }
    catch {
        $c->log->info("$_");
        $c->stash->{error} = "$_";
    };
    return;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
