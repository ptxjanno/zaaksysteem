package Zaaksysteem::Controller::Form;
use Moose;

use BTTW::Tools;
use Carp qw(croak);
use Clone qw/clone/;
use HTML::Entities;
use HTML::TagFilter;
use JSON;
use Zaaksysteem::BR::Subject::Constants 'REMOTE_SEARCH_MODULE_NAME_STUFNP';
use Zaaksysteem::BR::Subject;
use Zaaksysteem::Constants qw/
    BASE_RELATION_ROLES
    RGBZ_LANDCODES
    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_DIGID
    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEDRIJFID
    VALIDATION_CONTACT_DATA
    VALIDATION_RELATEREN_PROFILE
    BETROKKENE_RELATEREN_PROFILE
    BETROKKENE_RELATEREN_MAGIC_STRING_SUGGESTION
/;

BEGIN { extends 'Zaaksysteem::Controller' };

with qw(
    Zaaksysteem::Roles::Controller::Auth::Public
    Catalyst::TraitFor::Controller::reCAPTCHA
    MooseX::Log::Log4perl
);

use constant RELATIE_TABLE_CONFIG => {
    'header'    => [
        {
            label   => 'Betrokkenetype',
            mapping => 'type',
        },
        {
            label   => 'Naam',
            mapping => 'betrokkene_naam',
        },
        {
            label   => 'Rol',
            mapping => 'rol'
        }
    ],
    'options'   => {
        data_source         => '/form/register_relaties',
        search_action       => '/form/register_relaties/add',
        row_identifier      => 'betrokkene_identifier',
        has_delete_button   => 1,
        init                => 1,
        add                 => {
            label   => 'Toevoegen',
            popup   => 1,
        },
    }
};

sub _set_gemeentecode {
    my ($self, $c) = @_;

    my $requested_code = $c->session->{custom_gemeentecode} // $c->req->params->{gemeentecode};
    return unless $requested_code;

    $c->session->{custom_gemeentecode} = $requested_code;

    return;
}

=head2 formbase

Base for /form URLs.

Checks the session/request parameters for template information, and sets the
right one if allowed.

=cut

sub formbase : Chained('/') : PathPart('form') : CaptureArgs(0) {
    my ($self, $c) = @_;
    $self->_set_gemeentecode($c);
}

=head2 aanvraag_base

Base for /aanvraag URLs.

Checks the session/request parameters for template information, and sets the
right one if allowed.

=cut

sub aanvraag_base : Chained('/') : PathPart('aanvraag') : CaptureArgs(0) {
    my ($self, $c) = @_;
    $self->_set_gemeentecode($c);
}

=head2 aanvragen_base

Base for /aanvragen URLs.

Checks the session/request parameters for template information, and sets the
right one if allowed.

=cut

sub aanvragen_base : Chained('/') : PathPart('aanvragen') : CaptureArgs(0) {
    my ($self, $c) = @_;
    $self->_set_gemeentecode($c);
}

sub form : Chained('formbase'): PathPart(''): Args(0) {
    my ($self, $c) = @_;

    if ($c->user_exists) {
        $c->logout();
        $c->delete_session();
        $c->res->redirect($c->uri_for('/form'));
        $c->detach;
    }

    $c->detach('list');
}

sub form_with_id : Chained('formbase'): PathPart(''): Args(1) {
    my ($self, $c, $id) = @_;

    $c->res->redirect($c->uri_for(
        '/zaak/create/webformulier/',
        { zaaktype => $id, sessreset => 1 }
    ));

    $c->detach;
}

sub cancel : Chained('formbase') : PathPart('cancel') : Args(0) {
    my ($self, $c) = @_;

    delete($c->session->{ _zaak_create });

    $c->logout();
    $c->delete_session();

    $c->res->redirect($c->config->{ gemeente }{ gemeente_portal });
    $c->detach;
}

sub casetype_offline : Chained('formbase') : PathPart('casetype_offline') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{ template } = 'form/casetype_offline.tt';
}


sub form_by_zaaktype_afronden : Chained('aanvraag_base') : PathPart(''): Args(3) {
    my ($self, $c, $zaaktype_naam, $type_aanvrager, $afronden) = @_;

    $c->stash->{ afronden } = $afronden;

    $c->forward('form_by_zaaktype', [ $zaaktype_naam, $type_aanvrager ]);
}

=head2 form_by_zaaktype_id

Convert a human-readable url into a redirect, linking to the first step
of the case creation process.

I changed the preample to aanvragen - it's similar enough to aanvraag to not
disrupt, and otherwise I'd need to check for casetype names with only digits.

Arguments:
    casetype_id: numeric
    subject_type: persoon|organisatie|onbekend

=cut

sub form_by_zaaktype_id : Chained('aanvragen_base') : PathPart('') : Args() {
    my ($self, $c, $casetype_id, $subject_type, $afronden) = @_;

    my $retro_aanvrager_type = {
        persoon => 'natuurlijk_persoon',
        organisatie => 'niet_natuurlijk_persoon',
        onbekend => 'unknown'
    };

    throw('controller/form_by_zaaktype_id/invalid_subject_type',
        'Ongeldig aanvrager type: ' . $subject_type)
        unless $subject_type && exists $retro_aanvrager_type->{$subject_type};

    my $zaaktype = $c->model('DB::Zaaktype')->find($casetype_id)
        or throw('controller/form_by_zaaktype_id/casetype_not_found',
            "Zaaktype $casetype_id niet gevonden");

    $c->stash->{ afronden } = $afronden;

    $c->forward('form_by_zaaktype_create', [
        $zaaktype,
        $retro_aanvrager_type->{$subject_type}
    ]);
}

=head2 public_search

A public page containing the HTML.

=cut

sub public_search : Chained('formbase') : PathPart('public_search') : Args(1): DisableACL {
    my ($self, $c, $interface_id) = @_;

    my $interface           = $c->model('DB::Interface')->search_active(
        {
            id          => $interface_id,
            module      => 'publicsearchquery'
        }
    )->first;

    throw('form/public_search/not_found', 'Public search query not found') unless $interface;


    $c->stash->{iface}          = $interface;
    $c->stash->{iface_config}   = $interface->get_interface_config;

    ### Replace query with current query.
    my $search_query_id         = $interface->get_interface_config->{query}->{id};
    if ($search_query_id) {
        my $raw_search_query                = $c->model('Object')->retrieve(uuid => $search_query_id);
        $c->stash->{iface_config}->{query}  = $raw_search_query->TO_JSON if $raw_search_query;
    }

    $c->stash->{template}       = 'form/public_search.tt';
}

=head2 form_by_zaaktype

Legacy support for previously published urls.

=cut

sub form_by_zaaktype : Chained('aanvraag_base') : PathPart(''): Args(2) {
    my ($self, $c, $zaaktype_naam, $type_aanvrager) = @_;

    $zaaktype_naam =~ s/-/ /g;

    my $zaaktype = $c->model('DB::Zaaktype')->find_by_lowercase_title($zaaktype_naam);

    $c->forward('form_by_zaaktype_create', [$zaaktype, $type_aanvrager]);
}


sub form_by_zaaktype_create : Private {
    my ($self, $c, $zaaktype, $type_aanvrager) = @_;

    $c->session->{ _zaak_create }{ ztc_aanvrager_type } = $type_aanvrager;

    my $afronden = $c->stash->{ afronden } ? 1 : 0;

    if ($zaaktype->deleted) {
        $c->log->warn(sprintf("Zaaktype %d is deleted", $zaaktype->id));
        $c->stash->{ casetype_node } = $zaaktype->zaaktype_node_id;
        $c->detach('casetype_offline');
    }
    elsif (!$zaaktype->active) {
        $c->log->warn(sprintf("Zaaktype %d is offline", $zaaktype->id));
        $c->stash->{ casetype_node } = $zaaktype->zaaktype_node_id;
        $c->detach('casetype_offline');
    }

    my $args = {
        ztc_aanvrager_type    => $type_aanvrager,
        sessreset             => 1,
        zaaktype_id           => $zaaktype->id,
        afronden              => $afronden
    };

    my $twofactor = $c->model('DB::Interface')->search_active({ module => 'auth_twofactor' })->first;

    if ($twofactor) {
        my $enable_for = $twofactor->jpath('$.enable_for');

        if (   $enable_for eq 'persons'
            && $type_aanvrager eq 'natuurlijk_persoon')
        {
            $self->log->trace(
                "Two-factor authentication is enabled for persons. Using it.");
            $args->{authenticatie_methode} = 'twofactor';
        }
        elsif ($enable_for eq 'companies'
            && $type_aanvrager eq 'niet_natuurlijk_persoon')
        {
            $self->log->trace(
                "Two-factor authentication is enabled for companies. Using it."
            );
            $args->{authenticatie_methode} = 'twofactor';
        }
        elsif ($enable_for eq 'both') {
            $self->log->trace(
                "Two-factor authentication is enabled for 'both'. Using it.");
            $args->{authenticatie_methode} = 'twofactor';
        }
        else {
            $self->log->trace(
                "Two-factor authentication is not enabled for $type_aanvrager"
            );
        }
    }
    elsif ($type_aanvrager eq 'natuurlijk_persoon') {
        $self->log->trace("Using DigiD authentication.");
        $args->{authenticatie_methode} = 'digid';
    }
    elsif ($type_aanvrager eq 'niet_natuurlijk_persoon') {
        $self->log->trace("Using BedrijfID/eHerkenning authentication.");
        $args->{authenticatie_methode} = 'bedrijfid';
    }
    elsif ($type_aanvrager eq 'unknown') {
        $self->log->trace("Using preset client.");
        my $preset_client = $zaaktype->zaaktype_node_id->zaaktype_definitie_id
            ->preset_client;

        die "need preset_client" unless ($preset_client);

        $args->{aanvrager} = $preset_client;

        # Clear SAML and twofactor login attributes
        delete $c->session->{_saml};
        delete $c->session->{_twofactor};
        delete $c->session->{pip};

        $c->model('Plugins::Bedrijfid')->logout;

    }

    $c->session->{_zaak_create} = {%{$c->session->{_zaak_create} // {}}, %$args};
    $c->res->redirect(
        $c->uri_for(
            '/zaak/create/webformulier',
            $args
        )
    );
}

sub list : Private {
    my ($self, $c) = @_;

    $c->stash->{zaaktypen}  = $c->model('DB::Zaaktype')->search(
        {
            'me.deleted'                        => undef,
            'zaaktype_node_id.trigger'          => [
                'extern', 'internextern'
            ],
            'zaaktype_node_id.webform_toegang'  => 1,
            'me.active'                         => 1,
        },
        {
            'prefetch'      => 'zaaktype_node_id',
        }
    );

    $c->stash->{template}   = 'form/list.tt';
}


sub aanvrager_type : Private {
    my ($self, $c)          = @_;

    $c->stash->{template}   = 'form/aanvrager_type.tt';
}

my $landcodes = [];
{
    my %remap = reverse %{ RGBZ_LANDCODES() };
    for my $country (sort keys %remap) {
        push(@{ $landcodes },
            {
                value   => $remap{$country},
                label   => $country,
            }
        );
    }
}

sub aanvrager : Private {
    my ($self, $c)      = @_;

    $c->stash->{landcodes} = $landcodes;
    $c->stash->{template} = 'form/aanvrager.tt';

    $c->session->{preset_client} = 0;

    my $verified = $c->session->{_zaak_create}->{extern}->{verified} // '';

    $c->session->{verification_method} = $verified;

    my $saml_data = $c->session->{_saml};

    my $betrokkene = $self->get_betrokkene_from_authentication_method($c,
        auth_module => $verified,
        id          => $c->session->{_zaak_create}{extern}{id},
        type        => $c->session->{_zaak_create}{ztc_aanvrager_type},
        saml_data   => $saml_data,
    );

    $c->session->{_zaak_create}{aanvraag_trigger} = 'extern';
    if($verified eq 'preset_client') {
            throw("form/betrokkene",
                "Missing preset_client - what is going on?") if (!$betrokkene);

            $c->stash->{aanvrager}       = $betrokkene;
            $c->session->{preset_client} = 1;
            $c->session->{form}          = 1;

            ### Set ztc_aanvrager_id
            $c->session->{_zaak_create}{ztc_aanvrager_id}
                = $betrokkene->betrokkene_identifier;
            $c->session->{_zaak_create}{aanvraag_trigger} = 'extern';
            $c->forward(
                $c->session->{_zaak_create}{ztc_aanvrager_type} eq 'unknown'
                ? 'webform'
                : 'zaakcontrole');
            $c->detach();
    }

    $c->session->{form} //= 1;
    my $params = $c->req->params;

    if ($verified eq ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEDRIJFID) {
        $c->log->debug('Checking dossiernummer: ' . $c->model('Plugins::Bedrijfid')->login || $c->session->{_saml}{uid});
    }

    if ($betrokkene) {
        my %updates = %{ $c->session->{_zaak_create}{aanvrager_update} // {} };
        if (%updates && ($c->req->param('skip_ahead') || 0) != 1) {
            $c->stash->{aanvrager} = $betrokkene;
            $c->forward('webform');
        }
        else {
        }

        if ($params->{aanvrager_update}) {
            $c->forward('update_aanvrager_contact_data');

            if ($params->{skip_ahead}) {
                delete $c->stash->{_nav_position};
                $c->detach('webform');
            }

            $c->forward('zaakcontrole');
            $c->stash->{_nav_position} = 'zaakcontrole';
        }
    }

    ### person not found? Well...show form or get it from StUF.
    if (
        !$betrokkene
        || (   $betrokkene->btype eq 'natuurlijk_persoon'
            && !$betrokkene->authenticated
            && $betrokkene->authenticated_by ne
            ZAAKSYSTEEM_GM_AUTHENTICATEDBY_DIGID)
        )
    {
        # Redirect to broker loader template as these requests can take a very long time. (And
        # giving some feedback to the user is good.)
        if ($verified eq 'digid' && !$c->req->param('ran_stuf')) {
            my $stuf_config = $self->_search_stuf_allowed($c);

            if ($stuf_config) {
                $c->session->{stuf_config_interface_id} = $stuf_config->id;
                $c->stash->{load_person_from_broker} = 1;
            }
        }

        $self->_check_mogelijke_aanvragers($c, $betrokkene)
            unless $c->stash->{load_person_from_broker};

        # Update the informration so we can find a betrokkene
        # If a betrokkene can't be found, default to displaying a form that can be filled out.
        if (!$betrokkene) {
            if ($verified eq 'digid') {
                $c->stash->{aanvrager_bsn} = $c->session->{_saml}{uid};
            }
            elsif (($verified eq 'bedrijfid' || $verified eq 'eherkenning')) {

                my ($kvk, $ves) = _kvk_en_vestigingsnummer($c->session->{_saml}{uid});

                $c->stash->{aanvrager_kvk_dossiernummer} = $kvk;
                $ves //= $c->session->{_kvk}{vestigingsnummer};
                $c->stash->{aanvrager_kvk_vestigingsnummer} = $ves if $ves;

                # We're showing the "create a company" form.
                # This implies that the vestigingsnummer is properly set, so
                # don't show the "pick a company from this list" page
                # afterwards.
                $c->session->{vestigingsnummer_override_used} = 1;
            }
            elsif ($verified eq 'twofactor') {

                $c->stash->{two_fa_email} = $c->session->{_twofactor}{email};

                my %extern = %{ $c->session->{_zaak_create}{extern} };

                if ($extern{aanvrager_type} eq 'bedrijf') {
                    my ($kvk, $ves) = _kvk_en_vestigingsnummer($extern{id});
                    $c->stash->{aanvrager_kvk_dossiernummer} = $kvk;

                    $ves //= $c->session->{_kvk}{vestigingsnummer};

                    $c->stash->{aanvrager_kvk_vestigingsnummer} = $ves if $ves;
                    $c->session->{vestigingsnummer_override_used} = 1;
                }
            }
            elsif ($verified eq 'eidas') {
                $c->stash->{prefill} = {
                    first_name    => $saml_data->{firstname},
                    surname       => $saml_data->{surname},
                    date_of_birth => $saml_data->{date_of_birth},
                };
            }

            $c->stash->{aanvrager_edit} = 1;
            $c->stash->{_nav_position}  = 'aanvrager';
            $c->forward('_generate_navigation');
            $c->detach;
        }
    }

    $self->_check_mogelijke_aanvragers($c, $betrokkene);


    if ($betrokkene) {
        my $id = $betrokkene->betrokkene_identifier;
        $c->stash->{aanvrager} = $betrokkene;
        $c->session->{pip}{ztc_aanvrager} = $id;
        $c->session->{pip}{user_uuid}     = $betrokkene->uuid;
        $c->session->{_zaak_create}{ztc_aanvrager_id} = $id;
    }

    $c->stash->{_nav_position} = 'aanvrager';
    $c->forward('_generate_navigation');
    $c->forward('webform') if $c->req->param('afronden');

}

my $STUF_MAP = {
    voorletters                 => 'voorletters',
    voornamen                   => 'voornamen',
    voorvoegsel                 => 'tussenvoegsel',
    geslachtsnaam               => 'geslachtsnaam',
    geslachtsaanduiding         => 'geslachtsaanduiding',
    straatnaam                  => 'straatnaam',
    huisnummer                  => 'huisnummer',
    huisnummertoevoeging        => 'huisnummertoevoeging',
    huisletter                  => 'huisletter',
    postcode                    => 'postcode',
    woonplaats                  => 'woonplaats',
};

sub _load_external_requestor : Private {
    my ($self, $c) = @_;

    my $bridge = Zaaksysteem::BR::Subject->new(
        schema              => $c->model('DB')->schema,
        remote_search       => REMOTE_SEARCH_MODULE_NAME_STUFNP,
        config_interface_id => $c->session->{stuf_config_interface_id},
    );

    my ($rs) = $bridge->search(
        {
            'subject_type' => 'person',
            'subject.personal_number' => sprintf("%09s", $c->stash->{aanvrager_bsn}),
        }
    );

    if ($rs) {
        $bridge->remote_import($rs);

        return 1;
    }

    return;
}

sub _parse_stuf_value {
    my $self    = shift;
    my $value   = shift;

    if (UNIVERSAL::isa($value, 'HASH') && defined($value->{_})) {
        $value = $value->{_};
    }

    if ($value eq 'NIL' || $value eq 'NIL:geenWaarde') {
        $value = '';
    }

    return $value;
}


sub update_aanvrager_contact_data : Private {
    my ($self, $c)      = @_;

    my $params          = $c->req->params;

    my %contact_params;
    for (qw/npc-email npc-telefoonnummer npc-mobiel/) {
        my $value;
        if (exists($params->{ $_ })) {
            $value = $params->{$_};
        } else {
            next;
        }

        $contact_params{$_} = $value;
    }

    my $dv = Data::FormValidator->check(
        \%contact_params, VALIDATION_CONTACT_DATA
    );

    my $valid_params = $dv->valid;

    for my $raw_key (qw/npc-email npc-telefoonnummer npc-mobiel/) {
        my $key = $raw_key;
        $key =~ s/^npc-//g;

        $c->log->debug('Update aanvrager: ' . $key . ' -> "' . ( $valid_params->{$raw_key} // "<undef>" ) .'"');
        $c->stash->{aanvrager}->$key($valid_params->{$raw_key} // "")
    }
}

sub _check_mogelijke_aanvragers {
    my ($self, $c, $betrokkene) = @_;

    my $ztc_aanvrager_type  = $c->stash->{ aanvrager_type } // $c->session->{ _zaak_create }{ ztc_aanvrager_type };

    if ($ztc_aanvrager_type eq 'preset_client') {
        return;
    }
    elsif (!$betrokkene) {
        $c->log->debug("No betrokkene to check as requestor!");
        return;
    }
    elsif ($ztc_aanvrager_type =~ /^natuurlijk_persoon/) {
        $self->_check_mogelijke_aanvragers_personen($c, $betrokkene);
    } else {
        $self->_check_mogelijke_aanvragers_bedrijven($c, $betrokkene);
    }
}

sub _check_mogelijke_aanvragers_bedrijven {
    my $self                       = shift;
    my $c                          = shift;
    my $betrokkene                 = shift;

    if (
        !grep(
             /^niet_natuurlijk_persoon$/,
             @{ $c->stash->{type_aanvragers} }
        )
    ) {
        $c->stash->{aanvrager} = $betrokkene;
        $c->log->debug('U bent geen inwoner (natuurlijk persoon) van de gemeente. Deze aanvraag is niet op u van toepassing.');
        $c->stash->{template} = 'form/aanvraag_nvt.tt';
        $c->detach;
    }
}

sub _check_mogelijke_aanvragers_personen {
    my ($self, $c, $betrokkene) = @_;

    if (   !$betrokkene->in_gemeente
        && !grep(/^natuurlijk_persoon_na$/, @{ $c->stash->{type_aanvragers} }))
    {
        $c->stash->{aanvrager} = $betrokkene;
        $c->log->debug(
            'U bent geen inwoner van de gemeente. Deze aanvraag is niet op u van toepassing.'
        );
        $c->stash->{template} = 'form/aanvraag_nvt.tt';
        $c->detach;
    }

    if ($betrokkene->in_gemeente
        && !grep(/^natuurlijk_persoon$/, @{ $c->stash->{type_aanvragers} }))
    {
        $c->stash->{aanvrager} = $betrokkene;
        $c->log->debug(
            'U bent inwoner van de gemeente. Deze aanvraag is
            alleen van toepassing op personen buiten de gemeente.'
        );
        $c->stash->{template} = 'form/aanvraag_nvt.tt';
        $c->detach;
    }
}


=head2 _should_zaakcontrole

Returns 'true' when all 'preconditions' are met to do a 'zaakcontrole'. It does
not check if there is a previous or concept case. Please use L<_get_vorige_zaak>
or L<_get_concept_case> that will return an apropriate object when possible.

=cut

sub _should_zaakcontrole {
    my $c = shift;

    return if $c->session->{ _zaak_create }{ nothing_to_show };
    return if $c->session->{preset_client};

    return unless $c->stash->{aanvrager};

    return if $c->session->{afronden};

    return if $c->stash->{logged_in_by} eq 'preset_client';
    return if $c->session->{ _zaak_create }{ ztc_aanvrager_type } eq 'unknown';

    return 1;

}

=head2 _get_vorige_zaak

Returns the latest 'reusable' C<Zaak> for the current case-type.

Not all casetypes are reusable, they should be set explicitly.

=cut

sub _get_vorige_zaak {
    my $c = shift;

    return unless $c->stash->{zaaktype}->aanvrager_hergebruik;

    return $c->model('DB::Zaak')->search_extended(
        {
            'aanvrager.gegevens_magazijn_id' => $c->stash->{ aanvrager }->ex_id,
            'aanvrager.betrokkene_type'      => $c->stash->{ aanvrager }->btype,
            'me.zaaktype_id'                 => $c->stash->{ zaaktype }->get_column('zaaktype_id'),
            'me.deleted'                     => undef
        },
        {
            order_by    => { -desc => 'me.id' },
            rows        => 1,
        }
    )->first;
}

=head2 _get_concept_zaak

Returns the latest (and only) C<ConceptCase> for the current case-type

=cut

sub _get_concept_zaak {
    my $c = shift;

    my $zaak_create = $c->session->{_zaak_create};

    return if $c->session->{preset_client};

    my $requestor = $c->session->{_zaak_create}{ztc_aanvrager_id};
    return unless $requestor;

    return $c->model('ConceptCase')->find_concept_case(
        $zaak_create->{zaaktype_id},
        $requestor,
    );
}

sub zaakcontrole : Private {
    my ($self, $c) = @_;

    $c->detach('webform') unless _should_zaakcontrole($c);

    $c->stash->{ ztc_aanvrager_type } = $c->session->{ _zaak_create }{ ztc_aanvrager_type };

    my $params = $c->req->params();
    my $copy_gegevens = exists($params->{copy_gegevens}) ? $params->{copy_gegevens} : '';

    # copy 'vorige_zaak' gegevens if there is one ... then do so, and detach
    if (
        $copy_gegevens eq 'vorige_zaak'
        and my $vorige_zaak = _get_vorige_zaak($c)
    ) {
        my $registratie_fase = $vorige_zaak->registratie_fase;
        my $field_values = $vorige_zaak->field_values({
            fase => $registratie_fase->status
        });

        my $bib_kenmerk_rs = $c->model('DB::BibliotheekKenmerken')->search(
            {
                'me.id' => [keys %$field_values],
                'me.value_type' => ['option', 'checkbox', 'select'],
            },
            {
                prefetch => 'bibliotheek_kenmerken_values'
            }
        );

        while (my $bk = $bib_kenmerk_rs->next) {
            next if not ref($field_values->{ $bk->id });

            my @allowed = map { $_->value } grep { $_->active } $bk->bibliotheek_kenmerken_values;
            my @values = @{ $field_values->{ $bk->id } };

            $field_values->{ $bk->id } = [
                grep { my $x = $_; grep { $_ eq $x } @allowed  } @values
            ];
        }

        $c->session->{_zaak_create}->{form}->{kenmerken} = $field_values;

        $c->detach('webform');

    }

    # copy 'concept_zaak' gegevens if there is one ... then do so, and detach
    if (
        $copy_gegevens eq 'concept_zaak'
        and my $concept_zaak = _get_concept_zaak($c)
    ) {
        my $concept_case_data = $concept_zaak->zaak_gegevens() || {} ;
        my $zaak_create = $c->session->{_zaak_create};
        $c->session->{_zaak_create} = { %$zaak_create, %$concept_case_data };

        $c->detach('webform');

    }

    if ( exists $params->{copy_gegevens} ) {
        $c->detach('webform');
    } # what the hack were you trying to do? nevermind!

    my $vorige_zaak  = _get_vorige_zaak($c);
    my $concept_zaak = _get_concept_zaak($c);

    unless ( $concept_zaak || $vorige_zaak ) {
        $c->session->{ _zaak_create }{ nothing_to_show } = !undef;
        $c->detach('webform');
    } # apperently, we have nothing to do at 'zaak controle'

    $c->stash->{vorige_zaak}     = $vorige_zaak;
    $c->stash->{concept_zaak}    = $concept_zaak;
    $c->stash->{template}        = 'form/zaakcontrole.tt';
    $c->stash->{ _nav_position } = 'zaakcontrole';
    $c->forward('_generate_navigation');

}

=head2 submit_to_pip

Save the case creation blob to the database, ready for continuation
when the user decides to do so.

=cut

sub submit_to_pip : Private {
    my ($self, $c, $options) = @_;
    $options ||= {};

    if ($c->user_exists || $c->stash->{logged_in_by} eq 'preset_client') {
        $self->log->trace("Cannot save concepts for logged in user or preset_client cases");
        return;
    }

    my $params = $c->req->params;

    my $process_step_index = $params->{process_step_index} || 0;

    my $steps = $c->stash->{kenmerken_groups_keep_sort};
    return unless $steps;

    unless ($process_step_index < scalar @$steps && $process_step_index >= 0 ) {
        if ($options->{skip_confirmation}) {
            return;
        }

        throw("form/submit_to_pip", "illegal step index");
    }

    $c->model("ConceptCase")->submit_concept_case($c->session->{_zaak_create});

    unless ($options->{skip_confirmation}) {
        $c->stash->{template} = 'form/boodschap_onafgeronde_zaak.tt';
    }
}


sub resume_from_pip : Private {
    my ($self, $c) = @_;

    return if ($c->user_exists || $c->stash->{logged_in_by} eq 'preset_client');

    my $zaak_create = $c->session->{_zaak_create};

    my $case_data = $c->model('ConceptCase')->get_concept_case_data(
        $zaak_create->{zaaktype_id},
        $zaak_create->{ztc_aanvrager_id}
    );

    return unless $case_data;
    $c->session->{_zaak_create} = { %{$zaak_create}, %$case_data};
}


sub webform : Private {
    my ($self, $c) = @_;

    my $params = $c->req->params;

    my $is_last_step = $c->req->params->{afronden} ? 1 : 0;
    $c->forward('/form/resume_from_pip') if $is_last_step;

    $c->stash->{ table_config } = RELATIE_TABLE_CONFIG;
    $c->stash->{ table_config }{ rows } = $c->session->{ _zaak_create }{ betrokkene_relaties };

    $c->stash->{ template }             = $c->forward('_preprocess_webform');

    $c->stash->{ ztc_aanvrager_type }   = $c->session->{ _zaak_create }{ ztc_aanvrager_type };

    $c->forward('_process_stap', $is_last_step);

    my $betrokkene = $c->session->{_zaak_create}{ztc_aanvrager_id};

    if ($params->{remove_concept}) {
        $c->model('ConceptCase')->delete_concept_case(
            $c->session->{_zaak_create}{zaaktype_id},
            $betrokkene,
        );
        $c->res->redirect($c->uri_for('/pip'));
        $c->detach();
    }

    my ($betrokkene_type, $betrokkene_id) = $betrokkene =~ m|betrokkene-(\w+)-(\d+)|;
    my $betrokkene_obj = $c->model('Betrokkene')->get({}, $betrokkene);

    if (
        $betrokkene_obj &&
        $betrokkene_obj->can('messages_as_flash_messages') &&
        scalar(@{ $betrokkene_obj->messages_as_flash_messages })
    ) {
        $c->push_flash_message(@{ $betrokkene_obj->messages_as_flash_messages });
    }
    ### /BETROKKENE DATA

    ### REGELS
    my $status      = $c->req->param('fase') || 1;
    my $kenmerken   = $c->session->{_zaak_create}->{form}->{kenmerken} ||= {};

    my $rules = $c->stash->{zaaktype}->rules({ status => $status });

    my $rules_result = $c->stash->{regels_result} = $rules->execute({
        kenmerken               => $kenmerken,
        casetype                => $c->stash->{zaaktype},
        aanvrager               => $betrokkene_obj,
        contactchannel          => $c->session->{_zaak_create}->{contactkanaal},
        payment_status          => undef, # satisfy interface,
    });

    # Load new style rules
    $c->stash->{rules} = Zaaksysteem::Backend::Rules->generate_object_params(
        {
            'case.casetype.node'           => $c->stash->{zaaktype},
            'case.channel_of_contact'      => $c->stash->{contactchannel},
            'case.number_status'           => 1,
            'case.requestor'               => $betrokkene_obj,
            'case.requestor.preset_client' => $c->session->{preset_client} ? "Ja" : "Nee",
        },
        {
            engine => 1,
            include_kenmerken =>
                $c->session->{_zaak_create}{form}{kenmerken},
            validation => 1,
        }
    );

    ### Check kenmerken, and regenerate params
    $c->session->{_zaak_create}->{form}->{kenmerken} = $c->stash->{rules}->{validation}->return_active_data(
        $c->session->{_zaak_create}->{form}->{kenmerken}, $c->stash->{rules}->{rules}->_attribute_mapping
    );

    ### Reload rules, kenmerken could be changed
    $c->stash->{rules} = Zaaksysteem::Backend::Rules->generate_object_params(
        {
            'case.casetype.node'        => $c->stash->{ zaaktype },
            'case.channel_of_contact'   => $c->stash->{contactchannel},
            'case.number_status'        => 1,
            'case.requestor'            => $betrokkene_obj,
            'case.requestor.preset_client' => $c->session->{preset_client} ? "Ja" : "Nee",
        },
        {
            engine              => 1,
            include_kenmerken   => $c->session->{_zaak_create}->{form}->{kenmerken},
            validation          => 1,
        }
    );

    ### Reprocess webform
    $c->forward('_preprocess_webform');
    $c->forward('_process_stap_wizard');

    $c->stash->{ page_title } = $c->stash->{ zaaktype }->titel;

    ### /REGELS
    if ($c->stash->{rules}) {
        my $rules = $c->stash->{rules}{rules}->validate->changes;
        if (my $price = $rules->{'case.price'}) {
            $c->stash->{ rules_payment_amount } = sprintf("%.02f", $price);
        }
    }
    elsif (exists $rules_result->{ price }) {
        my $price = $rules_result->{ price }{ expression } || $rules_result->{ price }{ value };

        unless (ref $price) {
            $price =~ s|,|.|gis;
        }

        $c->stash->{ rules_payment_amount } = $price;
    }

    $c->session->{_zaak_create}->{streefafhandeldatum_data} =
        $rules_result->{wijzig_afhandeltermijn};

    $c->session->{regel_sjablonen} =
        $rules_result->{templates};

    if(my $toewijzing = $rules_result->{toewijzing}) {
        $c->session->{create_case_allocation_rule} = $toewijzing;
    } else {
        ### Make sure we empty it again, when not set...
        delete $c->session->{create_case_allocation_rule};
    }

    unless ($c->stash->{template}) {
        $c->stash->{template} = 'foutmelding.tt';
    }

    # last step
    my $steps = $c->stash->{kenmerken_groups_keep_sort};

    $c->forward(
        'submit_to_pip',
        [{ skip_confirmation => ($c->req->params->{submit_to_pip} ? 0 : 1) }]
    );

    delete $c->stash->{ _nav_position };

    ### Save current step in session
    if (
        !$c->session->{ _zaak_create }->{last_posted_step} ||
        $c->stash->{ process }{ step } > $c->session->{ _zaak_create }->{last_posted_step}
    ) {
        $c->session->{ _zaak_create }->{last_posted_step} = $c->stash->{ process }{ step };
    }

    if ($c->stash->{ process }{ step } < $c->session->{ _zaak_create }->{last_posted_step}) {
        $c->stash->{in_previous_step} = 1;
    }

    $c->forward('_generate_navigation');

    if($c->req->is_xhr && $c->req->params->{update_kenmerken}) {
        $c->stash->{template} = "form/form_inner.tt";
    }

    if($c->stash->{ process }{ step } == ($c->stash->{ process }{ total_step_count } - 1)) {
        if(!$c->user_exists && $c->session->{ _zaak_create }{ ztc_aanvrager_type } eq 'unknown' && $c->stash->{ zaaktype }->zaaktype_definitie_id->preset_client) {
            $c->forward('captcha_get');
        }
    }

    # Bij de laatste stap zet publish_zaak
    if ($c->req->params->{update_kenmerken} &&
        ($c->req->param('process_step_index')+1) == @$steps &&
        $c->req->param('submit_to_next')) {

        $c->stash->{ publish_zaak } = 1;
    } else {
        $c->detach();
    }
}

sub _process_stap : Private {
    my ($self, $c, $is_last_step) = @_;

    $c->forward('_process_stap_wizard');
    $c->forward('_process_stap_load_values');
    $c->forward('_process_stap_handle_post', $is_last_step);

    $c->stash->{form} = $c->session->{_zaak_create}->{form};

    $c->forward('/form/upload/display_uploads');

    $self->_get_default_values($c);

    my $zaak_create = $c->session->{_zaak_create};
    my $extra_auth_info = {
        aangevraagd_via => $zaak_create->{aangevraagd_via},
    };

    if($zaak_create->{milestone} eq '1') {
        $c->stash->{allow_cheat} = $self->_allow_cheat($c);
    }
}


=head2 _allow_cheat

In certain situations, required fields may be bypassed. Inform browser
that the cheat option may be presented.

=cut

sub _allow_cheat {
    my ($self, $c) = @_;
    if ($c->check_any_user_permission('zaak_create_skip_required')) {
        return 1;
    }
    elsif ($c->check_any_user_permission('zaak_beheer')) {
        # deprecated, thus warn
        $c->log->warn("zaak_beheer permissions for allow cheat");
        return 1;
    }
    return 0;
}



sub _get_default_values {
    my ($self, $c) = @_;

    # only once please
    return if $c->session->{_zaak_create}->{default_values_set}++;

    my $zaaktype_id             = $c->session->{_zaak_create}->{zaaktype_id};

    my $zaaktype_node = $c->stash->{ zaaktype };

    unless ($zaaktype_node) {
        $zaaktype_node = $c->model('DB::Zaaktype')->find($zaaktype_id, {
            prefetch => 'zaaktype_node_id'
        })->zaaktype_node_id;
    }

    my $zaaktype_kenmerkens = $zaaktype_node->zaaktype_kenmerkens;

    ### Given a hash of parameters, mangle_defaults will combine the defaults
    ### given in our library with our parameters. Since we have none yet, we
    ### just get all the default parameters by giving an empty hash
    ### (perldoc Zaaksysteem::DB::ResultSet::ZaaktypeKenmerken)
    $c->session->{_zaak_create}->{form}->{kenmerken} =
        $zaaktype_kenmerkens->mangle_defaults(
            (
                defined($c->session->{_zaak_create}->{form}->{kenmerken}) &&
                $c->session->{_zaak_create}->{form}->{kenmerken}
            ) ? $c->session->{_zaak_create}->{form}->{kenmerken} : {}
        );
}

sub _generate_navigation : Private {
    my ($self, $c) = @_;

    my @steps =
      ( $c->stash->{_navigation_steps} && scalar @{ $c->stash->{_navigation_steps} } )
      ? @{ $c->stash->{_navigation_steps} }
      : $c->stash->{zaaktype}->get_steps(1);
    my $cur_step = $c->req->param('process_step_index') // 0;

    if($c->req->param('afronden') && $c->session->{_zaak_create}->{afronden_goto_step}) {
        $cur_step = $c->session->{_zaak_create}->{afronden_goto_step};
    }

    my $origin = $c->session->{ _zaak_create }{ aangevraagd_via };
    my $iter_adjust = 0;

    if (
        _should_zaakcontrole($c)
        and
        _get_vorige_zaak($c) || _get_concept_zaak($c)
    ) {
        unshift @steps, {
            label => 'Zaakcontrole',
            disabled => 1
        };

        $iter_adjust++;
    }

    if($origin ne 'balie' && $c->session->{ _zaak_create }{ ztc_aanvrager_type } ne 'unknown') {
        my $url = $c->uri_for(
            '/zaak/create/webformulier/aanvrager',
            { skip_ahead => ($c->stash->{ _nav_position} // '') ne 'zaakcontrole' }
        );

        unshift @steps, {
            label => 'Contactgegevens',
            url => $url
        };

        $c->stash->{ navigation_links_start } = $url;

        $iter_adjust++;
    }

    if(exists $c->stash->{ _nav_position }) {
        if($c->stash->{ _nav_position } eq 'aanvrager') {
            $cur_step = 0;
        } elsif ($c->stash->{ _nav_position } eq 'zaakcontrole') {
            $cur_step = $iter_adjust - 1;
        }
    } else {
        $cur_step += $iter_adjust;
    }

    if($c->req->param('submit_to_next')) {
        $cur_step++;
    }

    if($c->req->param('submit_to_previous')) {
        $cur_step--;
    }

    push(@steps, { label => 'Afronden' });

    # online payment can depend on rules or casetype
    my $online_betaling = $c->forward('/zaak/_get_payment_amount');

    if($origin eq 'webformulier' && $online_betaling) {
        push(@steps, { label => 'Betalen' });
    }

    my $iter = 0;
    my $last_step = $c->session->{ _zaak_create }->{last_posted_step} // 0;
    for my $step (@steps) {
        if ($step->{hidden} && $iter <= $cur_step) {
            $cur_step++;
        }


        $step->{ classes } = join(' ', (
            'stap',
            $iter < 1 ? 'first' : (),
            $iter <= ($last_step && $last_step+$iter_adjust) ? 'done' : (),
            $iter == $cur_step ? 'active' : (),
            $iter == ($cur_step + 1) ? 'next-to-active': ()
        ));

        $step->{active} = $iter == $cur_step ? 1 : 0;

        if($step->{kenmerk_id}) {
            $step->{attributes} = {
                'data-zs-case-webform-field-group'          => '',
                'data-zs-case-webform-field-group-form'     => '',
                'data-field-group-id'                       => $step->{kenmerk_id},
                'data-ng-class'                             => '{ \'stap-disabled\': !caseWebformFieldGroup.isVisible() }'
            };
        }

        if(($last_step && $iter <= $cur_step) && !$step->{ disabled } && !$step->{ url }) {
            $step->{ url } = $c->req->uri_with({
                ztc_aanvrager_type => $c->session->{ _zaak_create }{ ztc_aanvrager_type },
                process_step_index => $iter - $iter_adjust,
                submit_to_next => undef,
                submit_to_previous => undef,
                process_previous_stap => 1,
            });

            ### Make sure URL's contain webform instead of aanvrager
            my $path = $step->{url}->path();
            $path =~ s|zaak/create/(\w+)/aanvrager|zaak/create/$1/webform|;

            $step->{url}->path($path);
        }

        $iter++;
    }

    $c->stash->{ navigation_links } = scalar(@steps) ? \@steps : undef;
}

sub _requires_recaptcha {
    my ($c, $casetype_node, $process) = @_;
    return 0 if $process->{step} != $process->{total_step_count};

    return 0 if $c->user_exists;
    return 0 if ($c->stash->{logged_in_by} // '') ne 'preset_client';

    return 0 if !$casetype_node->zaaktype_definitie_id->preset_client;
    return 0 if $casetype_node->properties->{no_captcha};

    return 0 if exists $c->session->{_zaak_create}{_captcha_validated} && $c->session->{_zaak_create}{_captcha_validated};

    return 1;
}

sub _process_stap_handle_post : Private {
    my ($self, $c, $is_last_step) = @_;

    my $params = $c->req->params();
    if (!$params->{update_kenmerken}) {
        return 1;
    }

    ### Validation
    my $registratie_fase;
    if ($c->stash->{zaak_status}) {
        $registratie_fase    = $c->stash->{zaak_status};
    } else {
        $registratie_fase = $c->stash->{zaaktype}
            ->zaaktype_statussen->search_rs({ status => 1 })->first;
    }

    # This is VERY defensive programming - how can this situation happen?
    throw(
        "case/create/registration_phase/missing",
        "Unable to create case with missing registration phase"
    ) if !$registratie_fase;


# put files in the params just before validating, otherwise the validator doesn't know
# files have been uploaded.
    my $session_uploads = $c->session->{_zaak_create}->{uploads} || {};

    foreach my $upload_kenmerk_id (keys %$session_uploads) {
        my $kenmerk = 'kenmerk_id_' . $upload_kenmerk_id;

        if (ref $params->{$kenmerk} ne 'ARRAY') {
            $params->{$kenmerk} = [];
        }

        if (defined $session_uploads->{$upload_kenmerk_id}) {
            for my $file (@{$session_uploads->{$upload_kenmerk_id}}) {
                my $filename;
                if (defined $file->{upload}) {
                    $filename = $file->{upload}->filename;
                }
                elsif ($file->{filestore_id} && defined $file->{name}) {
                    $filename = $file->{name};
                }
                push @{$params->{ $kenmerk }}, $filename;
            }
        } else {
            delete($params->{$kenmerk});
        }
    }


# hack - to make checkboxes and options defined, to enable search for required fields
    my @defined_kenmerken = ($c->req->param('defined_kenmerk'));
    foreach my $defined_kenmerk (@defined_kenmerken) {
        $params->{$defined_kenmerk} //= '';
    }

    {
        my $dv = $registratie_fase->validate_kenmerken(
            $params,
            {
                ignore_undefined => 1,
                with_prefix      => 1,
            }
        );

        my $process = $c->stash->{ process };
        my $ztc_aanvrager_type = $c->session->{ _zaak_create }{ ztc_aanvrager_type } // $params->{ztc_aanvrager_type} || '';

        if (_requires_recaptcha($c, $registratie_fase->zaaktype_node_id, $process)) {
            my $captcha_result = $c->forward('captcha_check');

            if ($captcha_result) {
                $c->session->{_zaak_create}->{_captcha_validated} = 1;
            } else {
                $c->log->info("Captcha error: " . $c->stash->{recaptcha_error});
                $dv->{ invalid }{ recaptchatable } = [ $c->stash->{recaptcha_error} ];
            }
        }

        if ($c->req->is_xhr && $c->req->params->{do_validation}) {
            $c->zvalidate($dv);
            $c->detach;
        }
    }

    $self->uploadfile($c);

    my $session_kenmerken = $c->session->{_zaak_create}->{form}->{kenmerken} ||= {};

    my %req_kenmerken   = map {
            my $key = $_;
            $key    =~ s/kenmerk_id_//g;
            $key    => $c->req->params->{ $_ }
        } grep(/^kenmerk_id_(\d+)$/, keys %{ $c->req->params });

    for my $kenmerk (keys %req_kenmerken) {

        if (UNIVERSAL::isa($req_kenmerken{$kenmerk}, 'ARRAY')) {
            $session_kenmerken->{ $kenmerk } = [];

            foreach my $value (@{ $req_kenmerken{$kenmerk} }) {
                push @{$session_kenmerken->{ $kenmerk }}, $self->_make_value_secure($value);
            }
        } else {
            $session_kenmerken->{ $kenmerk } = $self->_make_value_secure($req_kenmerken{$kenmerk});
        }
    }

    # remove kenmerken that are in the current step but not in cgi params
    # - to get rid of the last checkbox
    # first find out which step the post is about - which data are we replacing here
    my $submitted_step_index = $c->req->param('process_step_index');
    my $steps = $c->stash->{kenmerken_groups_keep_sort};
    my $submitted_step = $steps->[$submitted_step_index];

    # then get a list of kenmerken for this submitted step. for each of them, if there's
    # no information for one of them, delete it.
    if (!$c->req->params->{process_previous_stap} && !$is_last_step) {
        my $current_stap_kenmerken = $c->stash->{kenmerken_groups}->{$submitted_step};

        foreach my $current_stap_kenmerk (@$current_stap_kenmerken) {
            my $bibliotheek_kenmerk_id = $current_stap_kenmerk->get_column(
                'bibliotheek_kenmerken_id'
            );

            # ZS-3772 Objecttype fields are special...
            next unless $bibliotheek_kenmerk_id;

            unless (exists $req_kenmerken{ $bibliotheek_kenmerk_id }) {
                delete $session_kenmerken->{ $bibliotheek_kenmerk_id };
            }
        }
    }
}

sub _make_value_secure {
    my ($self, $value) = @_;


    my $tf  = HTML::TagFilter->new(allow => {
        p   => { all => [] },
        h1  => { all => [] },
        h2  => { all => [] },
        h3  => { all => [] },
        strong => { all => [] },
        em   => { all => [] },
        ul  => { all => [] },
        li  => { all => [] },
        ol  => { all => [] },
        a   => { 'href' => ['any'], 'title'=> ['any'] },
        img => { 'src' => ['any'],  'alt'  => ['any'],  'title' => ['any'],},
    });

    ### HTML::TagFilter has the annoying problem that it returns en empty
    ### string when given a 0. So we make sure we do not run tagfilter when
    ### string is empty or contains a 0 (when it tests 'false').
    ### Also, we recode entities to Unicode
    unless (defined($value) && !$value) {
        $value = $tf->filter($value);
        decode_entities($value);
    }

    return $value;
}

sub uploadfile {
    my ($self, $c) = @_;

    my $uploaded_files = {};
    foreach my $upload_param (keys %{$c->req->uploads}) {
        my $upload = $c->req->upload($upload_param);

        my $options = {
          'filename' => $upload->filename,
          'id'       => '0',
          'naam'     => $upload->filename,
        };

        my $file_id = $c->req->param('file_id');

        # For oldschool uploading (IE)
        unless($file_id) {
            ($file_id) = $upload->headers()->header('content-disposition') =~ m|name="(.*?)"|;
        }

        die "need file id" unless($file_id);

        my ($kenmerk) = $file_id =~ m|(\d+)$|;

        my $params = {
            uploads => {
                $kenmerk => {'upload' => $upload}
            }
        };

        my $filestore = $c->model('DB::Filestore')->filestore_create({
            original_name => $upload->filename,
            file_path     => $upload->tempname,
        });

        push @{$c->session->{_zaak_create}->{uploads}->{$kenmerk}}, {
            upload       => $c->req->upload($upload_param),
            filestore_id => $filestore->id,
        };
        $uploaded_files->{$filestore->id} = $upload;
    }

    return $uploaded_files; #obsolete
}

sub _process_stap_load_values : Private {
    my ($self, $c)          = @_;


    return 1 if (
        $c->session->{_zaak_create}->{form} &&
        scalar(keys %{ $c->session->{_zaak_create}->{form} })
    );

    $c->session->{_zaak_create}->{form} = {
        kenmerken   => {}
    };

}

#
# determine the screenflow of the webform.
# the webform can be submitted through a submit button - in which case there's a variable
# present in $c->req->params(), or through AJAX. default behaviour is to stay on the same
# step
#
# input:
# - current step index (process_step_index)
# - CGI param submit to next
# - CGI param submit to prev
#
# output:
# - new current step
#
sub _process_stap_wizard : Private {
    my ($self, $c)          = @_;
    $c->stash->{process}    = {};

    push(@{ $c->stash->{ kenmerken_groups_keep_sort } }, 'verify');

    my $steps = $c->stash->{kenmerken_groups_keep_sort};
    my $process_step_index = $c->req->param('process_step_index') || 0;

    my $s = scalar @$steps;
    if (!$s) {
        throw(
            '/Controller/Form/_process_stap_wizard',
            'Deze zaak heeft geen kenmerken!'
        );
    }

    do {
        throw(
            '/Controller/Form/_process_stap_wizard',
            sprintf(
                'Illegal step index (tried step %d of %d)', $process_step_index,
                $s
            ));

    } unless ($process_step_index < $s && $process_step_index >= 0);

    if($c->req->param('submit_to_previous')) {
        $process_step_index--;
    } elsif($c->req->param('submit_to_next')) {
        $process_step_index++;
    }

    if ($c->stash->{afronden_goto_step}) {
        $process_step_index = $c->stash->{afronden_goto_step};
        $c->stash->{afronden_goto_step} = undef;
    }

    $c->stash->{ process_step_index } = $process_step_index;

    my $process = {
        current_stap => $steps->[$process_step_index],
        previous_stap => $process_step_index - 1,
        step         => $process_step_index,
    };

    if(scalar @$steps > $process_step_index + 1) {
        $process->{'next_stap'} = $process_step_index + 1;
    }

    $process->{ total_step_count } = scalar @$steps;
    $c->stash->{process} = $process;
}



sub finish : Private {
    my ($self, $c) = @_;

    $c->forward('/zaak/finish');
}

sub _preprocess_webform : Private {
    my ($self, $c) = @_;

    ### Form fields
    {
        $c->stash->{kenmerken_groups}           = {};
        $c->stash->{kenmerken_groups_keep_sort} = [];
        $c->stash->{kenmerken_groups_only}      = {};
        $c->stash->{_navigation_steps}          = [];
        my $fields                              = $c->stash->{fields};

        my $current_group;
        $fields->reset;

        my %hidden_groups = map { $_ => 1 } @{$c->stash->{rules}{rules}{_validate}{hidden_groups}};

        my $firstgroupfound = 0;
        while (my $kenmerk = $fields->next) {
            if ($kenmerk->is_group) {
                $firstgroupfound++;
                $current_group = $kenmerk->label;

                $c->stash->{kenmerken_groups_only}->{$current_group} = $kenmerk;

                my $hidden = 0;
                if (!exists $hidden_groups{$kenmerk->id}) {
                    push(
                        @{ $c->stash->{kenmerken_groups_keep_sort} },
                        $current_group
                    );
                } else {
                    $hidden = 1;
                }

                push (
                    @{ $c->stash->{_navigation_steps} },
                    { label => $kenmerk->label, kenmerk_id => $kenmerk->id, hidden => $hidden }
                );

                next;
            } else {
                ### Geen group, show default
                if (!$firstgroupfound) {
                    $firstgroupfound++;
                    $current_group = 'Benodigde gegevens';
                    $c->stash->{kenmerken_groups_keep_sort}->[0]
                        = 'Benodigde gegevens';
                    $c->stash->{kenmerken_groups_only}->{'Benodigde gegevens'}
                        = {
                            label   => 'Benodigde gegevens',
                            help    => undef,
                        };
                }
            }

            $c->stash->{kenmerken_groups}->{$current_group} ||= [];

            push(
                @{ $c->stash->{kenmerken_groups}->{$current_group} },
                $kenmerk
            );
        }
    }

    return 'form/intake.tt';
}




sub register_relaties_in_session_suggestion : Chained('/') : PathPart('form/register_relaties/suggestion'): Args(0) {
    my ($self, $c) = @_;

    my @columns;
    my $suggestion = BETROKKENE_RELATEREN_MAGIC_STRING_SUGGESTION->(
        \@columns,
        $c->req->params->{magic_string_prefix},
        $c->req->params->{rol}
    );

    unless ($suggestion) {
        $c->res->body('NOK');
        return;
    }

    $c->res->body($suggestion);
}

Params::Profile->register_profile(
    'method'    => 'register_relaties_in_session',
    'profile'   => BETROKKENE_RELATEREN_PROFILE,
);

sub register_relaties_in_session : Chained('/') : PathPart('form/register_relaties'): Args() {
    my ($self, $c, $action)          = @_;


    my $params = $c->req->params();

    $c->stash->{nowrapper}  = 1;

    if ($c->req->is_xhr) {
        if ($params->{do_validation}) {
            $c->zvalidate;
        }
    }

    if ($action && $action eq 'add') {
        $c->stash->{rollen} = [
            @{ BASE_RELATION_ROLES() },
            @{ $c->model('DB::Config')->get_value('custom_relation_roles') || [] }
        ];
        $c->stash->{template}   = 'widgets/betrokkene/create_relatie.tt';
        $c->detach;
    }

    my $relaties = $c->session->{_zaak_create}->{betrokkene_relaties} ||= [];

    if ($params->{action} && $params->{action} eq 'remove') {

        my $remove_id = $params->{remove_id};
        my @new = ();
        foreach my $relatie (@$relaties) {
            unless($relatie->{id} eq $remove_id) {
                push @new, $relatie;
            }
        }
        $relaties = \@new;
    }

    $c->stash->{table_config} = RELATIE_TABLE_CONFIG;
    $c->stash->{template}       = 'widgets/general/simple_table.tt';

    if (uc($c->req->method) eq 'POST') {

        my $relatie_profile     = BETROKKENE_RELATEREN_PROFILE;

        my $relatie = {};
        $relatie->{ $_ } = $params->{ $_ } for (
            qw/
                type
                betrokkene_naam
            /,
            @{ $relatie_profile->{required} },
            @{ $relatie_profile->{optional} }
        );

        $relatie->{id} = $self->_get_new_insert_id($relaties);

        push @$relaties, $relatie;
    };

    $c->stash->{table_config}->{rows} = $c->session->{_zaak_create}->{betrokkene_relaties} = $relaties;
}


sub _get_new_insert_id {
    my ($self, $object_list) = @_;

    my $max_id = 1;

    foreach my $object (@$object_list) {
        if($object->{id} && $object->{id} > $max_id) {
            $max_id = $object->{id};
        }
    }

    return $max_id + 1;
}

=head2 _search_stuf_allowed

Simple helper to check whether or not broker searching is enabled or not.

=cut

sub _search_stuf_allowed {
    my ($self, $c) = @_;

    $self->log->trace('Checking if we can retrieve users from the selected BRP servicebus');

    my $stufconfig;
    if ($c->session->{custom_gemeentecode}) {
        my @stufconfigs = $c->model('DB::Interface')->search_active(
            { module => 'stufconfig' }
        )->all;

        @stufconfigs = grep {
            $_->jpath('$.gemeentecode') == $c->session->{custom_gemeentecode},
        } @stufconfigs;

        throw('form/gemeentecode_not_found', 'No configuration found for that gemeentecode')
            unless @stufconfigs;
        throw('form/gemeentecode_too_many', 'More than one configuration found for specified gemeentecode')
            unless @stufconfigs == 1;

        $stufconfig = $stufconfigs[0];
    }
    else {
        $stufconfig = $c->model('DB::Interface')->find_by_module_name('stufconfig');
    }

    if (
        $stufconfig && $stufconfig->active &&
        $stufconfig->module_object->can_search_sbus($stufconfig)
    ) {
        $self->log->trace('Servicebus enabled, request user via servicebus');
        return $stufconfig;
    }

    $self->log->trace('Servicebus disabled, let user enter their own credentials');
    return;
}

=head2 stuf_integrale_zoekvraag_import

Voert een integrale zoekvraag uit om een gebruiker uit een makelaar te importeren. Redirect vervolgens naar
een ingevuld formulier.

=cut

sub stuf_integrale_zoekvraag_import : Chained('/') : PathPart('form/integrale_zoekvraag_import') {
    my ($self, $c) = @_;

    if (!$c->req->params->{url}) {
        die 'Need URL to redirect to after completion';
    }

    my $url = sprintf "%s&ran_stuf=1", $c->req->params->{url};

    ### Generate "voorloopnul", happens in spoof mode only
    if (length($c->session->{ _saml }{ uid }) != 9) {
        $c->session->{ _saml }{ uid } = sprintf("%09d", $c->session->{ _saml }{ uid });
    }

    $c->stash->{aanvrager_bsn} = $c->session->{ _saml }{ uid };

    my $result = $c->forward('_load_external_requestor');

    $c->stash->{json} = $c->view('JSON')->prepare_json_row($url);
    $c->forward('View::JSON');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 BETROKKENE_RELATEREN_PROFILE

TODO: Fix the POD

=cut

=head2 RELATIE_TABLE_CONFIG

TODO: Fix the POD

=cut

=head2 RGBZ_LANDCODES

TODO: Fix the POD

=cut

=head2 VALIDATION_CONTACT_DATA

TODO: Fix the POD

=cut

=head2 ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEDRIJFID

TODO: Fix the POD

=cut

=head2 ZAAKSYSTEEM_GM_AUTHENTICATEDBY_DIGID

TODO: Fix the POD

=cut

=head2 aanvrager

TODO: Fix the POD

=cut

=head2 aanvrager_type

TODO: Fix the POD

=cut

=head2 cancel

TODO: Fix the POD

=cut

=head2 casetype_offline

TODO: Fix the POD

=cut

=head2 finish

TODO: Fix the POD

=cut

=head2 form

TODO: Fix the POD

=cut

=head2 form_by_zaaktype_afronden

TODO: Fix the POD

=cut

=head2 form_by_zaaktype_create

TODO: Fix the POD

=cut

=head2 form_with_id

TODO: Fix the POD

=cut

=head2 get_vorige_zaak

TODO: Fix the POD

=cut

=head2 list

TODO: Fix the POD

=cut

=head2 register_relaties_in_session

TODO: Fix the POD

=cut

=head2 register_relaties_in_session_suggestion

TODO: Fix the POD

=cut

=head2 resume_from_pip

TODO: Fix the POD

=cut

=head2 update_aanvrager_contact_data

TODO: Fix the POD

=cut

=head2 uploadfile

TODO: Fix the POD

=cut

=head2 webform

TODO: Fix the POD

=cut

=head2 zaakcontrole

TODO: Fix the POD

=cut

