package Zaaksysteem::Controller::Event;

use Moose;
use namespace::autoclean;

use BTTW::Tools;
use XML::Simple;

use Data::Visitor::Callback;

BEGIN { extends 'Zaaksysteem::Controller' }

=head1 Event querying

This controller enables paged event querying. This URL should always be called with the request method B<GET>

=head2 URL construction

B</event/list/[event-type]>

Querying events can be done in serveral modes. All event-mode, in which case B<[event-type]> in the URL will be empty. Everything after </event/list> is interpreted as a (partial) event-type. For example, querying all events related to cases can be done like this

    /event/list/case

Or with more specificity:

    /event/list/case/attribute/update

=head2 Query Parameters

=over 4

=item rows (I<default = 10>)

The maximum amount of rows to be returned by the query. The resultset can, of
course, contain less rows.

=item page (I<default = 1>)

The page number of the query. Integer number >= 1.

=item interval

The interval, in seconds, of events that should be retrieved. For example, set
this to 5 if you only want to retrieve events that happened in the last 5
seconds.

=item case_id

Filter events based on the value of the C<zaak_id> field in the Logging table.

=item created_by

Filter events based on the value of the C<created_by> or
C<betrokkene_id> field in the Logging table. (This implies the subject by which
the event was triggered).

=item for

Filter events that happend 'for' a specific subject. This can mean a note on
a subject, a case created with the specified subject as applicant, and so on.

Implicitly sets C<case_related_ident> to this parameter's value, so that
subjects related to cases also get involved. Don't mix this parameter with
either C<case_related_ident> or the use of C<case_relation>.

=item category

Filter events by category. Possible categories are C<document>, C<case>,
C<contactmoment> and C<note>.

=item case_related_ident

Filter events that have an associated case which itself is related to the
subject specified in this parameter. Uses the C<case_relation> parameter to
determine the relation between the subject and the case.

=item case_relation

Additional parameter used by C<case_related_ident>. Defaults to 'aanvrager'.

=back

=head2 Response body

The response should always be HTTP 200, unless something went wrong deeper in
Zaaksysteem. The results of the query are wrapped in a standard JSON paging
block (see L<API::Paging|Zaaksysteem::Manual::API::Paging>). Every event has
at least these properties:

    {
        "id": <integer>,
        "event_type": <string>,
        "timestamp": <iso8601>,
        "description": <string>     # This is the textual representation
    }                               # of the event, to be displayed directly

=head2 Validation

This URL requires XSRF token validation, and will fail if a proper token is not
presented with the X-XSRF-TOKEN header.

=cut

sub events : Chained('/') : PathPart('event') : CaptureArgs(0) {
    my ($self, $c) = @_;

    my $model = $c->model('DB::Logging');

    my $event_rs = $model->{ events } || $model->search({});
    my $applicant;

    my $context = $c->req->params->{'context'} || $c->req->params->{'category'};

    $event_rs = $event_rs->search_events_by_context($context) if $context;

    if($c->req->params->{'interval'}) {
        $event_rs = $event_rs->search({
            'me.created' => { '>', DateTime->now(time_zone => 'UTC')->subtract(seconds => $c->req->params->{'interval'}) }
        });
    }

    my $case_id;

    if($c->req->params->{for}) {
        if ($c->req->params->{category} eq 'case') {
            $case_id = $c->req->params->{for};
            $event_rs = $event_rs->search({ 'me.zaak_id' => $case_id || undef });
        }
        elsif ($c->req->params->{'category'} eq 'object'
            && $c->req->params->{for})
        {
            ### event/download (timeline logboek downloaden) for objects
            $event_rs
                = $event_rs->search({ object_uuid => $c->req->params->{for} });

        }
        else {
            $applicant = $c->req->params->{for};
        }
    }

    # Setup ACL's for searching in cases
    my $acl = $c->model("DB::Zaak")->get_acl_subquery($c->user, 'read');
    if ($acl) {
        $event_rs = $event_rs->search_rs(
            {
                -or => [
                    # Freely be able to look at logs which are not linked to a
                    # case
                    { zaak_id => undef },

                    # Or you must obey the ACL's of cases
                    {
                        zaak_id => { '!=' => undef },
                        -exists => $acl->search_rs(
                            { 'acl.case_id' => { '=' => \'me.zaak_id' } }
                        )->as_query,
                    },
                ]
            }
        );
    }

    if ($c->req->params->{'case_related_ident'} || $applicant) {
        my $identifier = $c->req->params->{'case_related_ident'} || $applicant;
        my $relation = $c->req->params->{'case_relation'} || 'aanvrager';

        my $case_rs = $c->model("DB::Zaak")->search(
            {
                'me.deleted'          => undef,
                'me.registratiedatum' => { '<' => \"NOW()" },
            },
            {
                join => [qw(aanvrager zaak_betrokkenen)]
            }
        );

        my $related = $self->_related_case_ids($case_rs, $relation, $identifier);

        my $q = $related->get_column('me.id')->as_query;

        $event_rs = $event_rs->search_rs(
            {
                -or => [
                    { 'me.created_for' => $applicant },
                    { 'me.zaak_id' => { -in => $q } },
                ],
            }
        );
    }
    elsif ($case_id) {
        $event_rs = $event_rs->search_rs(
            {
                zaak_id => $case_id,
            },
        );
    }

    $c->stash->{events} = $event_rs->search_rs(
        undef,
        {
            order_by => [
                { -desc => 'created' },
                { -desc => 'id' },
            ]
        },
    );
}

sub _related_case_ids {
    my ($self, $case_rs, $attribute, $for) = @_;

    return $case_rs unless $for;

    my ($type, $id) = $for =~ m/^betrokkene-(.*?)-(.*?)$/;

    if ($attribute eq 'aanvrager') {
        return $case_rs->search_rs(
            {
                'aanvrager.gegevens_magazijn_id' => $id,
            },
            {
                columns => 'me.id',
                join    => 'aanvrager',
            }
        );
    }
    elsif ($attribute eq 'behandelaar') {
        return $case_rs->search_rs(
            {
               'behandelaar.gegevens_magazijn_id' => $id,
            },
            {
                columns => 'me.id',
                join    => 'behandelaar'
            }
        );
    }
}

sub list : JSON : Chained('events') : PathPart('list') : DB('RO') {
    my ($self, $c, @type) = @_;

    my $event_rs = $c->stash->{ events };

    if(scalar(@type) && $c->req->param('category')) {
        throw('event/ambiguous_request', "Either supply an event-type after the URI path or supply a category parameter. These constraints are mutually exclusive.");
    }

    # Default to plain event search
    unless($event_rs) {
        $c->stash->{ events } = $c->model('DB::Logging')->search_events(join('/', @type) || ());

        # Revisit events base sub to re-apply parameters on deviant resultset
        $c->forward('events');

        $event_rs = $c->stash->{ events };
    }

    if(join('/', @type) eq 'kcc/call') {
        $event_rs = $event_rs->search({
            'me.event_data' => { like => '{"extension":"%","phonenumber":"%"}' },
            'me.created' => { '>', DateTime->now(time_zone =>
                    'UTC')->subtract(seconds => 600) }
        });
    }

    # Apply paging
    $event_rs = $event_rs->search({}, {
        page => $c->req->param('page') || 1,
        rows => $c->req->param('rows') || 10
    });

    my $pager = $event_rs->pager;

    if($pager->next_page) {
        $c->stash->{ next_url } = $c->req->uri_with({
            before_id => undef,
            since_id => undef,
            page => $pager->next_page
        });
    }

    if($pager->previous_page) {
        $c->stash->{ prev_url } = $c->req->uri_with({
            before_id => undef,
            since_id => undef,
            page => $pager->previous_page,
        });
    }

    $c->stash->{ json } = $event_rs;
    $c->detach('Zaaksysteem::View::JSON');
}

=head1 download

Download the XML representation of a timeline.

=head2 Response body

    <events>
        <event>
            <id>
            <description>
            .
            .
            .
        </event
    </events>

=cut

sub download : Chained('events') : PathPart('download') : Args(0) : DB('RO') {
    my ($self, $c) = @_;

    my $fn = sprintf('zaaksysteem-zaak-logboek-%s.xml', DateTime->now(time_zone => 'local')->datetime());

    $c->res->body(XML::Simple::XMLout(
        { event => [ map { $self->_filter_json($_->TO_JSON) } $c->stash->{ events }->all ] },
        RootName => 'events',
        KeyAttr => { event => 'id' },
        NoAttr => 1
    ));

    $c->res->content_type('application/xml');
    $c->res->header('Content-Disposition', qq[attachment; filename="$fn"]);
}

sub _filter_json {
    my $self = shift;
    my $hashref = shift;

    # Beat the booleans into submission
    Data::Visitor::Callback->new(
        'JSON::XS::Boolean' => sub { $_ = int }
    )->visit($hashref);

    return $hashref;
}

=head1 get / view event

A simple controller that retrieves one event and dumps it in JSON format.

=head2 URL construction

B</event/[id]>

=head2 Response body

Same as in the L<list|Zaaksysteem::Controller::Event#Event_querying> view of events.

=head2 Validation

This URL requires XSRF token validation, and will fail if a proper token is not
presented with the X-XSRF-TOKEN header.

=cut

sub get : JSON : Chained('/') : PathPart('event') : Args(1) : DB('RO') {
    my ($self, $c, $event_id) = @_;

    $c->stash->{ json } = $c->model('DB::Logging')->find($event_id);

    $c->detach('Zaaksysteem::View::JSON');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 download

TODO: Fix the POD

=cut

=head2 events

TODO: Fix the POD

=cut

=head2 get

TODO: Fix the POD

=cut

=head2 list

TODO: Fix the POD

=cut

