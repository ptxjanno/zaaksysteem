package Zaaksysteem::Controller::Medewerker;

use Moose;

use Moose::Util::TypeConstraints qw[enum];

use BTTW::Tools;
use Zaaksysteem::Constants qw/LOGGING_COMPONENT_USER/;
use Zaaksysteem::Constants::Rights qw(:all);

BEGIN { extends 'Zaaksysteem::Controller' }

sub behandelaar : Global {
    my ($self, $c, $zaaktype_node_id) = @_;
    my ( $aanvrager_keuze );

    $c->stash->{template}               = 'form/list.tt';
    $c->session->{zaaksysteem}->{mode}  = $c->stash->{layout_type} = 'simple';


    $c->stash->{behandelaar_form}       = $c->session->{behandelaar_form} = 1;

    ### When id given, redirect to first step
    if ($zaaktype_node_id) {
        $c->res->redirect(
            $c->uri_for(
                '/zaak/create',
                {
                    mode                => 'behandelaar',
                    zaaktype            => $zaaktype_node_id,
                    create              => 1,
                    ztc_trigger         => 'intern',
                    betrokkene_type     => 'medewerker',
                    ztc_contactkanaal   => 'post',
                    ztc_aanvrager_id    => 'betrokkene-medewerker-' .  $c->user->uidnumber
                }
            )
        );
        $c->detach;
    }

    ### Let's get a list of zaaktypen
    $c->stash->{zaaktypen}  = $c->model('Zaaktype')->list(
        {
            'zaaktype_trigger'  => 'intern',
        },
    );
}

sub base : Chained('/') : PathPart('medewerker') : CaptureArgs(1) {
    my ($self, $c, $userid)  = @_;

}

sub index : Chained('/') : PathPart('medewerker') : Args(0) {
    my ($self, $c)      = @_;

    $c->assert_any_user_permission('useradmin');

    if ($c->req->params->{set_read}) {
        if (my $notify = $c->model('DB::Message')->find($c->req->params->{set_read})) {
            $notify->is_read(1);
            $notify->update;
        }
    }

    my $active_entities = $c->model('DB::UserEntity')->search({ date_deleted => undef })->get_column('subject_id')->as_query;
    $c->stash->{inbox}  = $c->model('DB::Subject')->search(
        {
            subject_type    => 'employee',
            '-or'           => [
                { group_ids       => undef },
                { group_ids       => '{}' },
            ],
            id              => { 'in' => $active_entities }
        },
        {
            order_by        => { '-asc' => 'username' },
        }
    );

    if ($c->req->params->{parent_identifier}) {
        my $group_id;
        if (($group_id) = $c->req->params->{parent_identifier} =~ /^group-(\d+)$/) {
            $c->stash->{people_entries}         = [
                $c->model('DB::Subject')->search({ subject_type => 'employee' }, { order_by => { '-asc' => 'username'}})
                    ->search_by_group_id($group_id)
                    ->search({ id   => { 'in' => $active_entities }})
            ];
        } elsif (
            $c->req->params->{parent_identifier} eq 'inbox'
        ) {
            $c->stash->{people_entries}         = [ $c->stash->{inbox}->all ];
        }
    }

    $c->stash->{ou_entries}     = $c->model('DB::Groups')->get_as_tree($c->stash);
    $c->stash->{template}       = 'medewerker/index.tt';
}

define_profile move_to_ou => (
    required => {
        entry_dn => 'Str',
        ou_dn => 'Str',
    },
    optional => {
        authorization_scope => enum([qw[full app]])
    }
);

sub move_to_ou : Chained('/') : PathPart('medewerker/move_to_ou') : Args(0) {
    my ($self, $c, $userid)     = @_;

    $c->assert_any_user_permission('useradmin');

    my $params = assert_profile($c->req->params)->valid;

    my ($user_id) = $params->{entry_dn}   =~ /^user-(\d+)$/;
    my ($group_id) = $params->{ou_dn}      =~ /^group-(\d+)$/;
    my $auth_scope = $params->{ authorization_scope };

    unless ($user_id && $group_id) {
        $c->stash->{json} = {
            'succes'    => 0,
            'bericht'   => 'Opgegeven parameters zijn incorrect.'
        };

        $c->forward('Zaaksysteem::View::JSONlegacy');
        $c->detach;
    }

    my $subject                 = $c->model('DB::Subject')->find($user_id);

    unless ($subject) {
        $c->stash->{json} = {
            'succes'    => 0,
            'bericht'   => 'Afdeling kan niet gevonden worden. [' . $user_id . ']'
        };

        $c->forward('Zaaksysteem::View::JSONlegacy');
        $c->detach;
    }

    my $group                   = $c->model('DB::Groups')->find($group_id);

    unless ($group) {
        $c->stash->{json} = {
            'succes'    => 0,
            'bericht'   => 'Medewerker kan niet gevonden worden. [' . $group_id . ']'
        };

        $c->forward('Zaaksysteem::View::JSONlegacy');
        $c->detach;
    }

    my $role;

    if ($auth_scope) {
        my $role_name = $auth_scope eq 'app' ? 'App gebruiker' : 'Behandelaar';

        $role = $c->model('DB::Roles')->search({
            name => $role_name,
            system_role => 1
        })->first;
    }

    if (
        $role &&
        (!$subject->role_ids || !grep({ $_ == $role->id } @{ $subject->role_ids }))
    ) {
        my $roles = (ref $subject->role_ids ? $subject->role_ids : []);
        push (@$roles, $role->id);

        $subject->role_ids($roles);
        $subject->make_column_dirty('role_ids');
    }

    $subject->group_ids([$group_id]);

    my $props = $subject->properties;
    delete($props->{preferred_group_id});
    $subject->properties($props);

    if ($subject->update) {
        $c->stash->{json} = {
            'newdn'     => $group->id,
            'succes'    => 1,
            'bericht'   => 'Medewerker "' . $subject->username . '" succesvol toegevoegd aan "'
                . $group->name . '"',
        };

        $c->model('DB::Logging')->trigger('user/update/ou', {
            component => LOGGING_COMPONENT_USER,
            data => {
                user_id             => $subject->id,
                user_displayname    => $subject->displayname,
                group_id            => $group_id,
                group_name          => $group->name,
            }
        });
    } else {
        $c->stash->{json} = {
            'newdn'     => $group->id,
            'succes'    => 0,
            'bericht'   => 'Medewerker "' . $subject->username . '" kon niet toegevoegd worden aan "'
                . $group->name . '"',
        };
    }

    $c->forward('Zaaksysteem::View::JSONlegacy');
    $c->detach;
}

=head2 notify_user

Arguments: $c, \%PARAMS

Return value: none

    $self->notify_user(
        $c,
        {
            initials    => 'MK',
            sn          => 'Doedels',
            givenname   => 'Michael',
            telephonenumber => '123',
            mail        => 'test@example.com',
            displayname => 'MK Doedels'
        }
    );

Sends a mail to the e-mailadress given in the C<mail> param. Mail can be set from
configurationwindows, called "gebruikerstemplate" (or something).

=cut

my $mapping = {
    'user.initials'     => 'initials',
    'user.last_name'    => 'sn',
    'user.first_names'  => 'givenname',
    'user.telephone'    => 'telephonenumber',
    'user.email'        => 'mail',
    'user.name'         => 'displayname',
    'user.title'        => 'title',
};

sub notify_user {
    my $self      = shift;
    my $c         = shift;
    my $subject   = shift;

    my $notify_id = $c->model('DB::Config')->get('new_user_template');

    unless ($notify_id) {
        $c->log->warn('Cannot send e-mail, new_user_template not set in configuration');

        return;
    }

    if (my $template = $c->model('DB::BibliotheekNotificaties')->find($notify_id)) {
        my $params = { map { $_ => $subject->properties->{ $mapping->{$_} } } keys %$mapping };

        $params->{'user.department'} = $subject->primary_group->name if $subject->primary_group;

        try {
            $template->send_mail({
                to          => $params->{'user.email'},
                ztt_context => $params,
                request_id  => $c->stash->{request_id},
            });
        } catch {
            $c->log->error("Could not send email, because: '$_'");
        };
    } else {
        $c->log->warn('Cannot send e-mail, bibliotheek_notificatie not found');
    }
}

=head2 add_user

Arguments: via c.req.params

Return value: redirect to /medewerker

    URL: /medewerker/add/user

Adds a user to zaaksysteem. Reads C<<$c->req->params>> for input parameters. Redirects on
success.

B<Options>

=over 4

=item sn [required]

Surname, the lastname of a user.

=item initials [required]

Initials, the initials of a person

=item givenname [required]

Givenname, the givenname of a person

=item displayname [required]

Displayname: the name you would like to display on any overviews or below a letterhead.

E.g: M.K. Ootjers

=item mail [required]

E-mailaddress of user

=item title [optional]

Job title of the user.

=item group_id [required]

Parent group ID of user. The group ID the user belongs to

=item username [required]

Username of the user

=item password [required]

Password of the user.

=item telephonenumber [optional]

The telephonenumber of this user 

=back

=cut

define_profile 'add_user' => (
    required    => [qw/
        sn
        initials
        givenname
        displayname
        mail

        group_id
        username
        password
    /],
    optional    => [qw/
        telephonenumber
        title
        schema
    /],
    dependency_groups => {
        password_group  => [qw/password password_confirmation/],
    },
    field_filters => {
        username => ['lc']
    },
    constraint_methods => {
        password    => sub {
            my ($dfv, $val) = @_;

            return unless $val && $dfv->{__INPUT_DATA}->{password_confirmation};

            return unless ($dfv->{__INPUT_DATA}->{password_confirmation} eq $val);

            return if length($val) < 6;

            return 1;
        },
        username    => sub {
            my ($dfv, $val) = @_;
            my $schema      = $dfv->{__INPUT_DATA}->{schema};

            return unless ($val && lc($val) =~ /^[a-z0-9-]{3,32}$/);

            if ($schema) {
                my $users       = $schema->resultset('Subject')->search(
                    {
                        username => lc($val)   
                    }
                );

                return if $users->count;
            }

            return 1;
        }
    }
);

sub add_user : Chained('/') : PathPart('medewerker/add/user') : Args(0) {
    my ($self, $c)             = @_;
    my $entry;
    my $params                      = $c->req->params;

    $c->assert_any_user_permission('useradmin');

    if ($c->req->is_xhr && $c->req->params->{do_validation}) {
        my $dv = Params::Profile->check(
            params => {
                %$params,
                schema => $c->model('DB'),
            },
            method => __PACKAGE__ . '::add_user'
        );

        $c->zvalidate($dv);
        $c->detach;
    }

    if ($c->req->params->{confirmed}) {
        ### Get authldap interface
        $params = assert_profile(
            { %$params, schema => $c->model('DB')}
        )->valid;

        my $authldap = $c->model('DB::Interface')->search_active({ module => 'authldap'})->first;
        my $user_entity;

        $c->model('DB')->txn_do(
            sub {
                $user_entity = $c->model('DB::Subject')->create_user(
                    {
                        interface => $authldap,
                        %$params,
                        set_behandelaar => 1,
                    }
                );
            }
        );

        if ($user_entity) {
            $c->push_flash_message('Medewerker: ' . $user_entity->subject_id->displayname . ' succesvol toegevoegd');

            $self->notify_user($c, $user_entity->subject_id);

            $c->res->redirect(
                $c->uri_for('/medewerker')
            );
            $c->detach;
        }
    }

    $c->stash->{params}         = $params;
    $c->stash->{nowrapper}      = 1;
    $c->stash->{template}       = 'medewerker/edit_user.tt';

    $c->stash->{tree}           = $c->model('DB::Groups')->get_as_tree($c->stash);
    $c->detach;
}

=head2 edit_user

Arguments: $USERID, the rest of the params via C<<$c->req->params>>

Return value: redirect to /medewerker

    URL: /medewerker/edit/user/$USERID

Updates a users credentials

B<Options>

=over 4

=item sn [required]

Surname, the lastname of a user.

=item initials [required]

Initials, the initials of a person

=item givenname [required]

Givenname, the givenname of a person

=item displayname [required]

Displayname: the name you would like to display on any overviews or below a letterhead.

E.g: M.K. Ootjers

=item mail [required]

E-mailaddress of user

=item title [optional]

Job title of the user.

=item group_id [required]

Parent group ID of user. The group ID the user belongs to

=item username [required]

Username of the user

=item password [optional]

Password of the user. Only needed when changing the password

=item telephonenumber [optional]

The telephonenumber of this user 

=back

=cut

define_profile 'edit_user' => (
    required    => [qw/
        sn
        initials
        givenname
        displayname
        mail

        group_id
        username
    /],
    optional    => [qw/
        telephonenumber
        title
        schema

        password
    /],
    dependency_groups => {
        password_group  => [qw/password password_confirmation/],
    },
    field_filters => {
        username => ['lc']
    },
    constraint_methods => {
        password    => sub {
            my ($dfv, $val) = @_;

            return unless $val && $dfv->{__INPUT_DATA}->{password_confirmation};

            return unless ($dfv->{__INPUT_DATA}->{password_confirmation} eq $val);

            return if length($val) < 6;

            return 1;
        },
        username    => sub {
            my ($dfv, $val) = @_;
            my $schema      = $dfv->{__INPUT_DATA}->{schema};
            my $current     = $dfv->{__INPUT_DATA}->{entry};

            return 1 if $current->username eq $val;

            return unless ($val && lc($val) =~ /^[a-z0-9-]{3,32}$/);

            if ($schema) {
                my $users = $schema->resultset('Subject')->search(
                    {
                        username => lc($val)
                    }
                );

                return if $users->count;
            }

            return 1;
        }
    }
);

sub edit_user : Chained('/') : PathPart('medewerker/edit/user') : Args(1) {
    my ($self, $c, $id)             = @_;
    my $entry;
    my $params                      = $c->req->params;

    $c->assert_any_user_permission('useradmin');

    if ($id !~ /^\d+$/) {
        $c->res->redirect($c->uri_for('/medewerker'));
        $c->detach;
    }

    $c->stash->{entry}  = $c->model('DB::Subject')->find($id);

    if ($c->req->is_xhr && $c->req->params->{do_validation}) {
        my $dv = Params::Profile->check(
            params => {
                %$params,
                schema => $c->model('DB'),
                entry  => $c->stash->{entry}
            },
            method => __PACKAGE__ . '::edit_user'
        );

        $c->zvalidate($dv);
        $c->detach;
    }


    if ($c->req->params->{confirmed}) {
        ### Get authldap interface
        $params = assert_profile(
            { %$params, schema => $c->model('DB'), entry  => $c->stash->{entry}}
        )->valid;

        my $authldap                = $c->model('DB::Interface')->search_active({ module => 'authldap'})->first;

        my $entity                  = $c->stash->{entry}->user_entities->search->first;

        my $subject;
        $c->model('DB')->txn_do(
            sub {
                $subject = $c->stash->{entry}->update_user(
                    {
                        %$params,
                        set_behandelaar => 1,
                    },
                    $entity
                );
            }
        );

        if ($subject) {
            $c->push_flash_message('Medewerker: ' . $subject->displayname . ' succesvol bijgewerkt');
            $c->res->redirect(
                $c->uri_for('/medewerker')
            );
            $c->detach;
        }
    } else {
        $params = { $c->stash->{entry}->get_all_columns };
    }

    $c->stash->{params}         = $params;
    $c->stash->{nowrapper}      = 1;
    $c->stash->{template}       = 'medewerker/edit_user.tt';

    $c->stash->{tree}           = $c->model('DB::Groups')->get_as_tree($c->stash);
    $c->detach;
}


my $ADD_MAP = {
    'ou'    => {
        'required'  => [qw/name/],
        'optional'  => [qw/parent/],
        'constraint_methods' => {
            'ou'    => qr/^[\w\d\s-]+$/,
        }
    },
    'role'  => {
        'required'  => [qw/name/],
        'optional'  => [qw/parent external_assignee/],
        'constraint_methods' => {
            'role'    => qr/^[\w\d\s-]+$/,
        }
    }
};

sub add : Chained('/') : PathPart('medewerker/add') : Args(1) {
    my ($self, $c, $type ) = @_;

    $c->assert_any_user_permission('useradmin');

    unless ($type && $ADD_MAP->{$type}) {
        $c->log->warn('Ongeldig type voor add');
        $c->res->redirect($c->uri_for('/medewerker'));
        $c->detach;
    }

    $c->stash->{type} = $type;

    if ($c->req->params->{confirmed}) {
        Params::Profile->register_profile(
            method  => 'add',
            profile => $ADD_MAP->{$type}
        );

        my $dv = Params::Profile->check(
            params  => $c->req->params,
        );

        my $opts        = $dv->valid;

        ### Everything checked, now get the root group
        my $rootgroup   = $c->model('DB::Groups')->search(\'array_length(me.path, 1) = 1')->first;

        my $parent      = $opts->{parent} || $rootgroup->id;

        if ($dv->success) {

            my $success;
            if ($type eq 'ou') {
                $success = $c->model('DB::Groups')->create_group(
                    {
                        name                => $opts->{name},
                        parent_group_id     => $parent,
                    }
                );
            } else {
                $success = $c->model('DB::Roles')->create_role(
                    {
                        name                => $opts->{name},
                        parent_group_id     => $parent,
                    }
                );

                # hand stitch the correct rights for now
                if ($opts->{external_assignee}) {
                    $success->set_db_right(USER);
                    $success->set_db_right(DASHBOARD);
                }
            }

            if ($success) {
                $c->log->info(
                    'Users->add: Created ' . $type . ': ' . $success->name
                );

                $c->push_flash_message('Succesvol aangemaakt: ' . $success->name);

                my $msg;
                if ($type eq 'role') {
                    $msg    = 'Rol ' . $success->name .
                            ' succesvol toegevoegd';
                } else {
                    $msg    = 'Afdeling ' . $success->name .
                            ' succesvol toegevoegd';
                }

                $c->model('DB::Logging')->trigger($type . '/create', {
                    component => LOGGING_COMPONENT_USER,
                    data => $opts
                })
            }
        } else {
            $c->log->warn('Invalid fields: ' . dump_terse($dv));
            $c->push_flash_message('Aanmaken mislukt, naam van '
            . $type . ' niet correct');
        }
    } else {
        $c->stash->{nowrapper} = 1;
        $c->stash->{template} = 'medewerker/add.tt';

        if ($type eq 'role' || $type eq 'ou') {
            $c->stash->{tree} = $c->model('DB::Groups')->get_as_tree($c->stash)
        }

        $c->detach;
    }

    $c->res->redirect($c->uri_for('/medewerker'));
}

=head2 edit

Arguments: via C<<$c->req->params>>

Return value: redirect to /medewerker

    URL: /medewerker/edit

Edits a Role or Group. Or better said, a Role or Organization Unit.

B<Params>

=over 4

=item name

Name of OU or Parent

=item parent

ID of parent group. The ID of the parent Group row.

=back

=cut


sub edit : Chained('/') : PathPart('medewerker/edit') : Args(0) {
    my ($self, $c)  = @_;

    $c->assert_any_user_permission('useradmin');

    my $mapping     = {
        group   => {
            table   => 'Groups',
            method  => 'update_group',
        },
        role    => {
            table   => 'Roles',
            method  => 'update_role',
        }
    };

    my $params      = $c->req->params;

    ### Make sure description is always the name
    if ($params->{name}) {
        $params->{description} = $params->{name};
    }

    my ($type, $id) = $c->req->params->{dn} =~ /^(\w+)-(\d+)$/;

    if (!$type || $id !~ /^\d+$/) {
        $c->res->redirect($c->uri_for('/medewerker'));
        $c->detach;
    }

    $c->stash->{entry}  = $c->model('DB::' . $mapping->{$type}->{table})->find($id);

    if (!$params->{confirmed}) {
        $c->stash->{params}     = { $c->stash->{entry}->get_columns };
        $c->stash->{nowrapper}  = 1;
        $c->stash->{template}   = 'medewerker/add.tt';

        $c->stash->{tree}       = $c->model('DB::Groups')->get_as_tree($c->stash);
        $c->detach;
    }

    if ($type eq 'role' && $c->stash->{entry}->system_role) {

        $c->log->info("Trying to edit system role '$id', unable");
        $c->push_flash_message({
            message     => "Kan geen systeemrollen bewerken",
            type        => 'error'
        });
        $c->res->redirect($c->uri_for('/medewerker'));
        $c->detach;
    }
    my $method = $mapping->{$type}->{method};

    my $success;
    try {
        $c->model('DB')->txn_do(sub {
            $success = $c->stash->{entry}->$method($params);

            if ($type eq 'role') {
                my $external_assignee = $success->has_db_rights(USER, DASHBOARD);

                if ($params->{external_assignee} && !$external_assignee) {
                    $success->set_db_right(USER);
                    $success->set_db_right(DASHBOARD);
                    $success->clean_redis_for_related_subjects;
                }
                elsif (!$params->{external_assignee} && $external_assignee) {
                    $success->unset_db_right(USER);
                    $success->unset_db_right(DASHBOARD);
                    $success->clean_redis_for_related_subjects;
                }
            }
        });
    }
    catch {
        my $errmsg = sprintf("Kan %s niet bewerken",
            $type eq 'role' ? 'rol' : 'afdeling', $_);

        $c->log->warn("$errmsg: $_");

        $c->push_flash_message({
            message     => $errmsg,
            type        => 'error'
        });
        $c->res->redirect($c->uri_for('/medewerker'));
        $c->detach;
    };

    $c->log->info(
        $mapping->{$type}->{table} . '->edit: updated entry: ' . $success->name
    );

    $c->push_flash_message( $mapping->{$type}->{table} . ' succesvol bijgewerkt: ' . $success->name);
    $c->res->redirect($c->uri_for('/medewerker'));

}

=head2 get_entry_from_identifier

Arguments: $c, $identifier_string

Return value: $ROW_OF_ENTRY

    my $entry = $self->get_entry_from_identifier($c, 'group-33');

    # Returns: blessed(Zaaksysteem::DB::Group[33]);

Returns the entry according to given identifier, possible identifier options below. Entry
is a DBIx::Class row.

B<entries>

=over 4

=item group

    group-44

A L<Zaaksysteem::Backend::Group::Component> row.

=item role

    group-44

A L<Zaaksysteem::Backend::Role::Component> row.

=item subject

    subject-44

A L<Zaaksysteem::Backend::Subject::Component> row.

=back

=cut

sub get_entry_from_identifier {
    my ($self, $c, $ident) = @_;

    my ($type, $id) = $ident =~ /^(role|group|user)-(\d+)$/;

    return unless $type && $id;

    my $entry;
    my %source_map  = (
        role    => 'DB::Roles',
        group   => 'DB::Groups',
        user    => 'DB::Subject',
    );

    return unless $source_map{$type};

    return $c->model($source_map{$type})->find($id);
}

=head2 assert_entry_deletion

Arguments: $SUBJECT_OR_GROUP_OR_ROLE_ROW

Return value: $TRUE_ON_SUCCESS or Exception on error

    $self->assert_entry_deletion($c, $c->model('DB::Subject')->search->first);

    ### Throws: Cannot delete a subject, unsupported

Will check if the given entry can be deleted, by checking for child groups on
groups, prevents deletion of subjects.

Throws an exception when deletion is not possible, returns when everything is ok.

=cut

sub assert_entry_deletion {
    my ($self, $c, $entry)  = @_;

    unless ($entry && $entry->result_source->source_name =~ /^(?:Roles|Groups|Subject)$/) {
        throw('medewerker/delete/invalid_identifier', 'Cannot delete entry, don\'t know identifier');
    }

    my $errmsg;
    if ($entry->result_source->source_name eq 'Subject') {
        $errmsg = 'Systeemrollen kunnen niet verwijderd worden.';
    }

    if ($entry->result_source->source_name eq 'Roles') {
        if ($entry->system_role) {
            $errmsg = 'Systeemrollen kunnen niet verwijderd worden.';
        }

        if ($entry->subjects->search_active->count) {
            $errmsg = 'Actieve gebruikers hebben deze rol. Rol kan niet verwijderd worden.';
        }
    }

    if ($entry->result_source->source_name eq 'Groups') {
        if (!$entry->path || scalar(@{ $entry->path }) < 2) {
            $errmsg = 'Systeemafdelingen kunnen niet verwijderd worden.';
        }

        if ($entry->subjects->search_active->count) {
            $errmsg = 'Afdeling kan niet verwijderd worden, het bevat actieve gebruikers.';
        }
    }

    throw('medewerker/delete/not_permitted', $errmsg) if $errmsg;

    return 1;
}

sub delete : Chained('/') : PathPart('medewerker/delete') : Args(0) {
    my ($self, $c) = @_;

    my $entry       = $self->get_entry_from_identifier($c, $c->req->params->{dn});

    my $error;
    try {
        $self->assert_entry_deletion($c, $entry);
    }
    catch {
        $error = $c->format_error($_);
    };

    if(!$c->req->params->{confirmed} || $error) {
        if ($error) {
            $c->stash->{confirmation}->{msgonly}    = 1;
            $c->stash->{confirmation}->{message}    = $error->{messages}->[0];


            $c->stash->{confirmation}->{uri}        =
                $c->uri_for(
                    '/medewerker'
                );
        } else {
            $c->stash->{confirmation}->{message}    =
                'Weet u zeker dat u "'
                . ($entry->can('displayname') ? $entry->displayname : $entry->name)
                . '" wilt verwijderen?'
                . (!$entry->can('displayname') ? ' Let op: Verwijder deze rol alleen wanneer deze niet meer gebruikt wordt door zaaktypen.' : '');

            $c->stash->{confirmation}->{type}       = 'yesno';
            $c->stash->{confirmation}->{params}     = {
                dn  => $c->req->params->{dn}
            };

            $c->stash->{confirmation}->{uri}        =
                $c->uri_for(
                    '/medewerker/delete'
                );
        }


        $c->res->status(200);
        $c->forward('/page/confirmation');
        $c->detach;
    }

    if ($entry->set_deleted) {
        $c->push_flash_message(
            'Entry "' . ($entry->can('displayname') ? $entry->displayname : $entry->name)  . '" succesvol verwijderd'
        );

        $c->model('DB::Logging')->trigger('role/remove', {
            component => LOGGING_COMPONENT_USER,
            data => {
                role_id         => $entry->id,
                role_name       => ($entry->can('displayname') ? $entry->displayname : $entry->name),
            }
        });
    }

    $c->res->redirect($c->uri_for('/medewerker'));
}

sub delete_role_from : Chained('/') : PathPart('medewerker/delete_role_from') : Args(0) {
    my ($self, $c, $userid)     = @_;

    $c->assert_any_user_permission('useradmin');

    my ($user_id)               = $c->req->params->{entry_dn}   =~ /^user-(\d+)$/;
    my ($role_id)               = $c->req->params->{role_dn}    =~ /^role-(\d+)$/;

    unless ($user_id && $role_id) {
        $c->stash->{json} = {
            'succes'    => 0,
            'bericht'   => 'Opgegeven parameters zijn incorrect.'
        };

        $c->forward('Zaaksysteem::View::JSONlegacy');
        $c->detach;
    }

    my $subject                 = $c->model('DB::Subject')->find($user_id);

    unless ($subject) {
        $c->stash->{json} = {
            'succes'    => 0,
            'bericht'   => 'Medewerker kan niet gevonden worden. [' . $user_id . ']'
        };

        $c->forward('Zaaksysteem::View::JSONlegacy');
        $c->detach;
    }

    my $role                    = $c->model('DB::Roles')->find($role_id);

    unless ($role) {
        $c->stash->{json} = {
            'succes'    => 0,
            'bericht'   => 'Rol kan niet gevonden worden. [' . $role_id . ']'
        };

        $c->forward('Zaaksysteem::View::JSONlegacy');
        $c->detach;
    }

    my @subject_roles = grep { $_ != $role_id } @{ $subject->role_ids };
    $subject->role_ids(\@subject_roles);

    if ($subject->update) {
        $c->stash->{json} = {
            'succes'    => 1,
            'role_name' => $role->name,
            'bericht'   => 'Rol "' . $role->name . '" verwijderd van "'
                . $subject->displayname . '"',
        };

        $c->model('DB::Logging')->trigger('user/update/ou', {
            component => LOGGING_COMPONENT_USER,
            data => {
                user_id         => $subject->id,
                displayname     => $subject->displayname,
                role_id         => $role_id,
                role_name       => $role->name,
                action          => 'delete',
            }
        });
    } else {
        $c->stash->{json} = {
            'succes'    => 0,
            'bericht'   => 'Rol "' . $role->name . '" kon niet verwijderd worden van "'
                . $subject->displayname . '"',
        };
    }

    $c->forward('Zaaksysteem::View::JSONlegacy');
    $c->detach;
}

sub add_role_to : Chained('/') : PathPart('medewerker/add_role_to') : Args(0) {
    my ($self, $c, $userid)     = @_;

    $c->assert_any_user_permission('useradmin');

    my ($user_id)               = $c->req->params->{entry_dn}   =~ /^user-(\d+)$/;
    my ($role_id)               = $c->req->params->{role_dn}    =~ /^role-(\d+)$/;

    unless ($user_id && $role_id) {
        $c->stash->{json} = {
            'succes'    => 0,
            'bericht'   => 'Opgegeven parameters zijn incorrect.'
        };

        $c->forward('Zaaksysteem::View::JSONlegacy');
        $c->detach;
    }

    my $subject                 = $c->model('DB::Subject')->find($user_id);

    unless ($subject) {
        $c->stash->{json} = {
            'succes'    => 0,
            'bericht'   => 'Medewerker kan niet gevonden worden. [' . $user_id . ']'
        };

        $c->forward('Zaaksysteem::View::JSONlegacy');
        $c->detach;
    }

    my $role                    = $c->model('DB::Roles')->find($role_id);

    unless ($role) {
        $c->stash->{json} = {
            'succes'    => 0,
            'bericht'   => 'Rol kan niet gevonden worden. [' . $role_id . ']'
        };

        $c->forward('Zaaksysteem::View::JSONlegacy');
        $c->detach;
    }

    my @subject_roles = @{ $subject->role_ids || [] };
    push(@subject_roles, $role_id);

    $subject->role_ids(\@subject_roles);

    if ($subject->update) {
        $c->stash->{json} = {
            'succes'    => 1,
            'role_name' => $role->name,
            'bericht'   => 'Rol "' . $role->name . '" toegevoegd aan "'
                . $subject->displayname . '"',
        };

        $c->model('DB::Logging')->trigger('user/update/ou', {
            component => LOGGING_COMPONENT_USER,
            data => {
                user_id         => $subject->id,
                displayname     => $subject->displayname,
                role_id         => $role_id,
                role_name       => $role->name,
                action          => 'add',
            }
        });
    } else {
        $c->stash->{json} = {
            'succes'    => 0,
            'bericht'   => 'Rol "' . $role->name . '" kon niet toegevoegd worden aan "'
                . $subject->displayname . '"',
        };
    }

    $c->forward('Zaaksysteem::View::JSONlegacy');
    $c->detach;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 LOGGING_COMPONENT_USER

TODO: Fix the POD

=cut

=head2 add

TODO: Fix the POD

=cut

=head2 add_role_to

TODO: Fix the POD

=cut

=head2 base

TODO: Fix the POD

=cut

=head2 behandelaar

TODO: Fix the POD

=cut

=head2 delete

TODO: Fix the POD

=cut

=head2 delete_role_from

TODO: Fix the POD

=cut

=head2 index

TODO: Fix the POD

=cut

=head2 move_to_ou

TODO: Fix the POD

=cut

=head2 update

TODO: Fix the POD

=cut

