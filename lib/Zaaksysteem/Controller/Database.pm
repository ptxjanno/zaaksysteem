package Zaaksysteem::Controller::Database;

use Moose;
use namespace::autoclean;

use Zaaksysteem::SQL;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::Database - A DBIx::Class controller

=head1 SYNOPSIS

=head1 DESCRIPTION

Zaaksysteem Controller which dumps the database schema of the current instance.

=head1 METHODS

=head2 database

Returns the database SQL

=cut

sub index : Chained('/') : PathPart('database') {
    my ($self, $c) = @_;

    $c->assert_any_user_permission('admin');

    my $schema = $c->model('DB')->schema;
    my $generator = Zaaksysteem::SQL->new(
        schema  => $schema,
        version => $c->config->{ZS_VERSION},
    );

    $c->stash->{zapi}   = [ $generator->sql ];
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 index

TODO: Fix the POD

=cut

