package Zaaksysteem::Controller::File;

use Moose;

use BTTW::Tools;
use File::stat;
use MIME::Lite;
use Moose::Util::TypeConstraints;
use Zaaksysteem::Backend::Tools::FilestoreMetadata qw(get_document_categories);
use Zaaksysteem::Constants qw(MAX_CONVERSION_FILE_SIZE);
use Zaaksysteem::Object::Reference::Instance;

with 'MooseX::Log::Log4perl';

BEGIN { extends 'Zaaksysteem::Controller' }

=head1 NAME

Zaaksysteem::Controller::File - Frontend library for the Zaaksysteem
file structure.

=head1 METHODS

=head2 instance_base

Reserves the C</file/[id]> namespace

=cut

sig instance_base => 'Zaaksysteem, Int';

sub instance_base : Chained('/') : PathPart('file') : CaptureArgs(1) {
    my ($self, $c, $id) = @_;

    $c->stash->{ file_id } = $id;
    $c->stash->{ file } = $c->model('DB::File')->find($id);

    unless (defined $c->stash->{ file }) {
        throw('file/not_found', sprintf(
            'Could not find file with ID "%s"', $id
        ));
    }
}

=head2 create

Create a new file.

=head3 Arguments

=over

=item case_id [optional]

Assign this file to a case. When this parameter is ommitted, the file
will show up in the global file queue for later processing.

=item publish_pip [optional]

Boolean, decides whether this file gets shown on the PIP or not.

=item publish_website [optional]

Boolean, decides whether this file may be exported outside of Zaaksysteem.

=item return_content_type

Option to override the content-type this call returns. Mostly for IE-compatibility.

=back

=head3 Location

POST: /file/create

=head3 Returns

A JSON structure containing file properties.

=cut

sub create : Local {
    my ($self, $c) = @_;

    $c->stash->{json_content_type} = $c->req->params->{return_content_type};

    $c->stash->{ json } = try {
        my %optional;
        if ($c->req->params->{case_id}) {
            $optional{case_id} = $c->req->params->{case_id};
        }
        my $upload  = $c->req->upload('file');
        my $subject = $c->user->as_object->old_subject_identifier;

        # Create the DB-entry
        my $result = $c->model('DB::File')->file_create({
            db_params => {
                created_by   => $subject,
                %optional,
            },
            name              => $upload->filename,
            file_path         => $upload->tempname,
        });

        return $result->discard_changes;
    } catch {
        return $c->process_error($_);
    };

    $c->detach('Zaaksysteem::View::JSON');
}

=head2 file_create_sjabloon

Create a new file based on a sjabloon and add it to the case accept queue.

=head3 Arguments

=over

=item name [required]

The name this file will get.

=item case_id [required]

The case_id this document will be assigned to.

=item zaaktype_sjabloon_id [required]

The sjabloon_id of the sjabloon that will be used.

=item target_format [optional]

Override for zaaktype_sjablonen.target_format (which is given with
zaaktype_sjabloon_id). Defaults to what is set in
zaaktype_sjablonen.target_format or .odt if it isn't set at all.

=back

=head3 Returns

A JSON structure containing file properties.

=cut

sub file_create_sjabloon : JSON : Local {
    my ($self, $c) = @_;

    my $params     = $c->req->params;
    my $subject = $c->user->as_object;

    $c->stash->{ json } = try {
        my $case = $c->model('DB::Zaak')->find($params->{case_id});

        if (!$case) {
            throw('file/file_create_sjabloon/case_not_found', sprintf(
                'Case with ID "%d" not found',
                $params->{ case_id }
            ));
        }

        if ($case->is_afgehandeld) {
            throw('file/file_create_sjabloon/case_resolved', sprintf(
                'Cannot process template for resolved case "%d"',
                $params->{ case_id }
            ));
        }

        my $zaaktype_sjabloon = $c->model('DB::ZaaktypeSjablonen')
            ->search({id => $params->{zaaktype_sjabloon_id}})->single;

        unless (defined $zaaktype_sjabloon) {
            throw('file/file_create_sjabloon/zaaktype_sjabloon_not_found', sprintf(
                'ZaaktypeSjabloon "%d" not found',
                $params->{ zaaktype_sjabloon_id }
            ));
        }

        my %optional;
        if (defined $params->{case_document_ids}) {
            $optional{case_document_ids} = [$params->{case_document_ids}];
        }

        my $template = $zaaktype_sjabloon->bibliotheek_sjablonen_id;

        my $result;
        if ($template->interface_id) {
            my $rv = $zaaktype_sjabloon->bibliotheek_sjablonen_id->create_file_from_external_template({
                name    => $params->{name},
                case    => $case,
                subject => $subject,
                target_format => 'odt',
                %optional,
            });

            if ($template->interface_id->module eq 'xential') {
                # All other status are considered errors
                my $type = 'error';
                if (ref($rv) && $rv->{status} eq 'done') {
                    $type = 'pending';
                }
                elsif (ref($rv) && $rv->{status} eq 'complex_build') {
                    $type = 'redirect';
                }

                $result = {
                    id   => undef,
                    type => $type,
                    data => $rv,
                };
            }
            elsif ($template->interface_id->module eq 'stuf_dcr') {
                $result = $rv;
            }
        }
        else {
            my $file = $zaaktype_sjabloon->bibliotheek_sjablonen_id->file_create({
                name    => $params->{name},
                case    => $case,
                subject => $subject,
                target_format => $params->{target_format} || $zaaktype_sjabloon->target_format,
                %optional,
            });
            $result = {
                id   => $file->filestore_id->uuid,
                type => 'created',
                data => $file,
            }

        }

        return $result;
    } catch {
        return $c->process_error($_);
    };

    $c->detach('Zaaksysteem::View::JSON');
}

=head2 update

Update properties of a file or replace the file itself.

=head3 Arguments

=over

=item file_id [required]

=item name [optional]

=item case_id [optional]

=item directory_id [optional]

Setting this to (JSON/JS unquoted) null will reset it to the root.

=item case_document_ids [optional]

=item case_document_clear_old [optional with case_document_id]

When this boolean is set to true, a case_document which is already assigned
to another document gets cleared.

=item publish_pip [optional]

Boolean, decides whether this file gets shown on the PIP or not.

=item publish_website [optional]

Boolean, decides whether this file may be exported outside of Zaaksysteem.

=item accepted [optional]

Boolean. When set to false, it requires a rejection_reason. This results in
the document being placed back in the global document queue. No longer bound to
the case.

=item rejection_reason [required with accepted]

A string detailing why the document was rejected. See the accepted parameter.

=item reject_to_queue [optional with accepted]

Boolean-ish value, if defined uses the value otherwise uses whatever is set in the database

=item metadata [optional]

A hash containing any of the following metadata columns:
 description
 document_category_parent
 document_category_child
 trust_level -- Valid: 'Openbaar', 'Beperkt openbaar', 'Intern', 'Zaakvertrouwelijk', 'Vertrouwelijk', 'Confidentieel', 'Geheim', 'Zeer geheim'
 origin -- Valid: 'Inkomend', 'Uitgaand', 'Intern'
 origin_date -- Valid: L<Postgres date format|http://www.postgresql.org/docs/9.1/static/datatype-datetime.html#DATATYPE-DATETIME-DATE-TABLE>

All of these columns are optional.

=item deleted

Sets the document to deleted. Note: files are never truly removed, just disabled.

Important: as this is called from a javascript environment it NEEDS 'true' or 'false'.

=back

=head3 Location

POST: /file/update

=head3 Returns

A JSON structure containing the updated file properties.

=cut

sub update : JSON : Local {
    my ($self, $c) = @_;
    my $params     = $c->req->params;

    $params->{ subject } = $c->user->as_object;

    $c->stash->{ json } = try {
        my $file_id = $params->{file_id};
        my $file = $c->model('DB::File')->search_rs({
            'me.id' => $file_id,
        })->first;

        unless (defined $file) {
            my $warning = sprintf('File with ID "%s" not found', $file_id);
            $c->log->info($warning);
            throw('file/update/file_not_found', $warning);
        }

        # destroying is only allowed when the user has zaak_beheer
        # permissions. since the permissions engine is dependant on
        # the controller layer, determine permissions here.
        # Since this call is somewhat expensive (potential LDAP traffic,
        # db lookups) only do it if necessary.
        # BTW, the idea is that backend layer should be able to tell
        # permissions like $schema->current_user->check_any_zaak_permission('zaak_beheer'),
        # which will make this patch obsolete.
        if ($params->{destroyed}) {
            $params->{user_has_zaak_beheer_permissions} =
                $c->check_any_given_zaak_permission($file->case_id, 'zaak_beheer');
        }

        return $file->update_properties($params);
    } catch {
        return $c->process_error($_);
    };

    $c->detach('Zaaksysteem::View::JSON');
}

=head2 move_case_document

Moves a case document assignment from one file to another file.

=head3 Arguments

=over

=item from [required

=item to [required]

=item case_document_id [required]

=back

=head3 Location

POST: /file/move_case_document

=head3 Returns

The file it was assigned to.

=cut

sub move_case_document : JSON : Local {
    my ($self, $c) = @_;
    my $params     = $c->req->params;

    $c->stash->{ json } = try {
        my $subject = $c->user->as_object;

        my $file_id = $params->{to};
        my ($file) = $c->model('DB::File')->search({
            id => $file_id,
        });

        unless (defined $file) {
            my $warning = sprintf('File with ID "%s" not found', $file_id);

            $c->log->warn($warning);

            throw('file/move_case_document/file_not_found', $warning);
        }

        return $file->move_case_document({
            from             => $params->{from},
            subject          => $subject,
            case_document_id => $params->{case_document_id},
        });
    } catch {
        return $c->process_error($_);
    };

    $c->detach('Zaaksysteem::View::JSON');
}


=head2 search_queue

Returns all files currently in the document queue. (Accepted false)

=head3 Arguments

=over

=item visibility [required]

Restricts the output to files belonging to the subject or simply returning all files.

=item subject_id [optional]

Limit the search to a certain subject.

=item case_id [optional]

Get all files related to the given case id.

=back

=head3 Location

/file/search_queue/(subject_id|case_id)/1234

=head3 Returns

A list containing one or more JSON structures containing the file
properties.

=cut

define_profile search_queue => (
    required => {
        visibility => 'Str'
    }
);

sub search_queue : JSON : Chained('/') : PathPart('file/search_queue') : Args {
    my ($self, $c) = @_;

    my $params = assert_profile($c->req->params)->valid;

    my $visibility = $params->{visibility};

    $c->stash->{json} = try {

        # Only queued documents + default to no case_id/subject_id/date_deleted
        my %search_params = (
            'me.accepted'       => 0,
            'me.case_id'        => undef,
            'me.subject_id'     => undef,
            'me.date_deleted'   => undef,
            'me.queue'          => 1,
            'me.active_version' => 1,
        );

        # Restrict context
        if ($params->{case_id}) {
            $search_params{case_id} = $params->{case_id};
        }
        elsif ($params->{subject_id}) {
            $search_params{subject_id} = $params->{subject_id};
        }

        # Visibility 'all' is a default by not restricting the intake_owner. With subject it needs to be set explicitly.
        if ($visibility eq 'subject' && $c->check_any_user_permission('documenten_intake_subject')) {
            $search_params{'me.intake_owner'} = $c->user->as_object->old_subject_identifier;
        }
        # When 'all' is set, make sure the user executing this call is allowed to. No other parameters are required.
        elsif ($visibility eq 'all' && !$c->check_any_user_permission('documenten_intake_all')) {
            throw('file/search_queue/not_allowed', sprintf(
                'Current user "%s" not permitted to view all intake files',
                $c->user->as_object->display_name
            ));
        }

        return $c->model('DB::File')->search(\%search_params)
            ->with_related_data(case_context => 0);
    } catch {
        return $c->process_error($_);
    };

    $c->detach('Zaaksysteem::View::JSON');
}

=head2 verify

Checks if the database's md5sum matches the filesystem md5sum.

=head3 Arguments

=over

=item file_id [required]

=back

=head3 Location

/file/verify/file_id/1234

=head3 Returns

Returns a JSON hash with 1 upon succes, 0 for a failure.

=cut

sub verify : JSON : Chained('/') : PathPart('file/verify/file_id') : Args(1) {
    my ($self, $c, $file_id) = @_;

    $c->stash->{ json } = try {
        my ($file) = $c->model('DB::File')->search({id => $file_id});

        unless (defined $file) {
            my $warning = sprintf('File with ID "%s" not found', $file_id);

            $c->log->warn($warning);

            throw('file/verify/file_not_found', $warning);
        }

        return { result => $file->filestore->verify_integrity };
    } catch {
        return $c->process_error($_);
    };

    $c->detach('Zaaksysteem::View::JSON');
}

=head2 get_thumbnail

Returns the thumbnail.

=head3 Arguments

=over

=item file_id [required]

=back

=head3 Parameters

=over 4

=item max_height

Preferred height (in pixels) for the thumbnail

=item max_width

Preferred width (in pixels) for the thumbnail

=back

=head3 Location

/file/thumbnail/file_id/1234

=head3 Returns

Returns the thumbnail data.

=cut

sub get_thumbnail : Chained('/') : PathPart('file/thumbnail/file_id') : Args(1) {
    my ($self, $c, $file_id) = @_;

    try {
        my $t0 = Zaaksysteem::StatsD->statsd->start;

        my $file = $c->model('DB::File')->search_rs({ 'me.id' => $file_id },
            { prefetch => 'filestore_id' })->first;

        unless (defined $file) {
            my $warning = sprintf('File with ID "%s" not found', $file_id);

            $c->log->warn($warning);

            throw('file/get_thumbnail/file_not_found', $warning);
        }

        my $path;
        if ($file->confidential && !$file->case_id->confidential_access) {
            $path = $file->thumbnail_unavailable;
        }
        elsif ($file->filestore_id->size > MAX_CONVERSION_FILE_SIZE) {
            $path = $file->thumbnail_unavailable_size;
        }
        else {
            my $thumbnail = $file->get_thumbnail($c->req->params);

            if ($thumbnail) {
                $c->serve_filestore($thumbnail->filestore_id);

                Zaaksysteem::StatsD->statsd->end('serve_file.get_thumbnail.time', $t0);
                Zaaksysteem::StatsD->statsd->increment('serve_file.get_thumbnail', 1);

                return 1;
            }

            $path = $file->may_preview_as_pdf
                ? $file->thumbnail_waiting
                : $file->thumbnail_unavailable;
        }

        $c->serve_static_file($path);
        $c->res->headers->content_type('image/png');

        Zaaksysteem::StatsD->statsd->end('serve_file.get_thumbnail.time', $t0);
        Zaaksysteem::StatsD->statsd->increment('serve_file.get_thumbnail', 1);
    } catch {
        $c->stash->{ json } = $c->process_error($_);
    };

    $c->detach unless $c->stash->{ json };
    $c->detach('Zaaksysteem::View::JSON');
}

=head2 request_signature

Triggers a request-signature event and dispatches it to the configured (if
any) signing service.

=head3 URI

C</file/[id]/request_signature>

=head3 Response

    {
        result => ...
    }

=cut

sub request_signature : Chained('instance_base') : PathPart('request_signature') : Args(0) {
    my ($self, $c) = @_;

    my $valid_sign = $c->model('DB::Interface')->find_by_module_name('valid_sign');
    unless ($valid_sign) {
        throw(
            "file/request_signature/interface_not_found",
            "Interface for signing documents is not found",
        );
    }

    my $params = {
        file_id       => $c->stash->{file_id},
        case_id       => $c->stash->{file}->get_column('case_id'),
        subject_uuid  => $c->user->uuid,
    };

    my $transaction = $valid_sign->process_trigger(
        'start_signing_procedure',
        $params
    );

    my $id = $transaction->id;
    if ($transaction->success_count) {
        $c->stash->{json} = { id => $id };
        $c->detach('Zaaksysteem::View::JSON');
    }
    throw("file/request_signature/error",
        "Undefined error, please see the transactions logs: $id");
}

=head2 generate_edit_invitation

Generates a new L<Zaaksysteem::Object::Types::Session::Invitation> object as
a session invitation for editing the referenced file.

=head3 URI

C</file/[id]/generate_edit_invitation>

=head3 Response

    {
        result => 'zaaksysteem://...'
    }

=cut

sub generate_edit_invitation : Chained('instance_base') : PathPart('generate_edit_invitation') : Args(0) {
    my ($self, $c) = @_;

    my $filestore_entry = $c->stash->{ file }->filestore_id;

    my $invitation = $c->model('Session::Invitation')->create({
        subject => $c->user->as_object,
        date_expires => DateTime->now->add(minutes => 30),
        action_path => '/api/v1/file',
        object => Zaaksysteem::Object::Reference::Instance->new(
            id => $filestore_entry->uuid,
            type => 'file',
            preview => $filestore_entry->original_name
        )
    });

    my $auth_interface = $c->model('DB::Interface')->find_by_module_name('authtoken');
    unless ($auth_interface) {
        throw(
            "file/generate_edit_invitation/interface_not_found",
            "Interface for token-based authentication not found",
        );
    }

    my $session_invitation_version = $auth_interface->get_interface_config->{session_invitation_version};

    my $invitation_uri;
    if ($session_invitation_version eq 'v2') {

        # Because some customers cannot upgrade for another quarter
        if ($auth_interface->get_interface_config->{documentwatcher_downgrade}) {
            $invitation_uri = $invitation->as_uri_documentwatcher_16(
                $c->uri_for_action('/api/v1/file/get', [$filestore_entry->uuid]),
            );
        }
        else {
            $invitation_uri = $invitation->as_uri(
                $c->uri_for_action('/api/v1/file/get', [$filestore_entry->uuid]),
                $c->stash->{file}->get_column('case_id'),
            );
        }

    }
    elsif ($session_invitation_version eq 'v1') {
        # To be removed in early 2019
        $invitation_uri = $invitation->as_v1_uri(
            $c->uri_for_action('/api/v1/file/get', [$filestore_entry->uuid]));
    }
    else {
        throw(
            "file/generate_edit_invitation/unknown_version",
            sprintf(
                "Unsupported version configured for session invitations: '%s'",
                $session_invitation_version),
        );
    }

    $c->stash->{ json } = {
        result => $invitation_uri
    };

    $c->detach('Zaaksysteem::View::JSON');
}

=head2 update_file

Replace a file. This results in a new file being created with the properties
of the current (/soon to be 'old') file. The only difference is that it will
have a higher version and updated file properties (md5, size, mimetype, etc.).

=head3 Arguments

=over

=item file_id [required]

=item file [required - mutex with existing_file_id]

The newly uploaded file.

=item existing_file_id [required - mutex with file]

An existing file entry can replace a file instead of uploading a new one.

The new file's data.

=item return_content_type

Option to override the content-type this call returns. Mostly for IE-compatibility.

=back

=head3 Location

POST: /file/update_file

=head3 Returns

A JSON structure containing the new file properties.

=cut

sub update_file : Local {
    my ($self, $c) = @_;

    $c->stash->{json_content_type} = $c->req->params->{return_content_type};

    $c->stash->{ json } = try {
        my %params  = %{ $c->req->params };
        my $subject = $c->user->as_object;
        my $file_id = $params{file_id};
        my $file_uuid = $params{file_uuid};

        my $file;
        if ($file_id) {
            ($file)     = $c->model('DB::File')->search({'me.id' => $file_id});
        } else {
            ($file) = $c->model('DB::File')->search(
                {
                    'filestore_id.uuid' => $file_uuid,
                },
                {
                    join    => 'filestore_id'
                }
            );
        }

        unless (defined $file) {
            my $warning = sprintf('File with ID "%s" not found', $file_id);

            $c->log->warn($warning);

            throw('file/update_file/file_not_found', $warning);
        }

        my $result;
        if ($params{existing_file_id}) {
            $result = $file->update_existing({
                subject          => $subject,
                existing_file_id => $params{existing_file_id}
            });
        }
        elsif ($params{file_id} || $params{file_uuid}) {
            my $upload  = $c->req->upload('file');

            $result  = $file->update_file({
                subject       => $subject,
                original_name => $upload->filename,
                new_file_path => $upload->tempname,
            });
        }

        return $result;
    } catch {
        return $c->process_error($_);
    };

    $c->detach('Zaaksysteem::View::JSON');
}

=head2 edit_file

    URL: /file/edit_file

    # Post Data (JSON Format)
    {
        file_id             => 4902,        # required
        interface_module    => 'xential'    # required
    }

    # Returns:

B<Return value>: JSON

Requests an edit from zaaksysteem. Will return the file for editing. This call was first introduced for
Xential.

=cut

define_profile edit_file => (
    required    => {
        interface_module    => subtype('Str' => where { $_ =~ /^(?:xential)$/}),
        file_id             => 'Int',
    }
);

sub edit_file : Local {
    my ($self, $c)  = @_;
    my $params      = assert_profile($c->req->params)->valid;
    my ($interface);

    $interface = $c->model('DB::Interface')->search_active({ 'module' => $params->{interface_module} })->first;

    unless (defined $interface) {
        throw('file/edit_file/editor_not_configured', sprintf(
            'Editor "%s" is not configured',
            $params->{ interface_module }
        ));
    }

    $c->stash->{ json } = try {
        my $rv  = $interface->process_trigger('request_edit_file',
            {
                file_id             => $params->{file_id},
                subject             => $c->user->id,
            }
        );

        return {
            success          => 1,
            redirect_url     => $rv->{redirect_url},
        };
    } catch {
        return $c->process_error($_);
    };

    $c->detach('Zaaksysteem::View::JSON');
}

=head2 version_info

Get a version history tree for this file.

=head3 Arguments

=over

=item file_id [required]

=back

=head3 Location

/file/version_info/file_id/1234

=head3 Returns

Returns a JSON hash keyed on version IDs.

=cut

sub version_info : JSON : Chained('/') : PathPart('file/version_info/file_id') : Args(1) {
    my ($self, $c, $file_id) = @_;

    $c->stash->{ json } = try {
        my ($file) = $c->model('DB::File')->search({id => $file_id});

        unless (defined $file) {
            my $warning = sprintf('File with ID "%s" not found', $file_id);

            $c->log->warn($warning);

            throw('file/version_info/file_not_found', $warning);
        }

        my $root_file = $file->get_root_file;
        my @files     = $root_file->files;

        # Add the starter file to the results. The files method only lists files
        # pointing to it.
        push @files, $root_file;

        my @loglines = $c->model('DB::Logging')->search({
            component    => 'document',
            component_id => [ map ({ $_->id } @files) ]
        });

        my @result;
        for my $f (@files) {
           push @result, {
                file_id         => $f->id,
                case_id         => $f->get_column('case_id'),
                version         => $f->version,
                name            => $f->name,
                extension       => $f->extension,
                creation_reason => $f->creation_reason,
                created_by      => $f->created_by,
                log             => [ grep { $f->id eq $_->component_id } @loglines ],

                # any reason why not use the TO_JSON pattern?
                date_created    => $f->date_created->set_time_zone('UTC')->set_time_zone('Europe/Amsterdam')->iso8601,
                date_modified   => $f->date_modified->set_time_zone('UTC')->set_time_zone('Europe/Amsterdam')->iso8601,
            }
        }

        return { result => \@result };
    } catch {
        return $c->process_error($_);
    };

    $c->detach('Zaaksysteem::View::JSON');
}

=head2 revert_to

Revert to a previous version of a file. This sets the version you
revert to as the last version.

Example:

File.jpg has 5 versions. The last 2 versions had a fault in them and
the user wishes to revert to version 3. Version_id is set to 3 and
in the return value you will find the restored file with version 6.

=head3 Arguments

=over

=item file_id [required]

The file you wish to revert to.

=back

=head3 Location

POST: /file/revert_to/

=head3 Returns

The reverted file as a JSON hash.

=cut

sub revert_to : JSON : Local {
    my ($self, $c) = @_;

    $c->stash->{ json } = try {
        my %params  = %{ $c->req->params };
        my $file_id = $params{file_id};
        my ($file) = $c->model('DB::File')->search({id => $file_id});

        unless (defined $file) {
            my $warning = sprintf('File with ID "%s" not found', $file_id);

            $c->log->warn($warning);

            throw('file/revert_to/file_not_found', $warning);
        }

        return $file->make_leading($c->user->as_object);
    } catch {
        return $c->process_error($_);
    };

    $c->detach('Zaaksysteem::View::JSON');
}

=head2 document_categories

Get a list of document categories and their allowed values.

=head3 Location

/file/document_categories

=head3 Returns

The document categories in a list of hashes.

=cut

sub document_categories : JSON : Local {
    my ($self, $c) = @_;

    my @categories = get_document_categories();
    $c->stash->{json} = \@categories;
    $c->forward('Zaaksysteem::View::JSON');
}

=head2 help

Returns a HTMLified version of the POD in this library.

=cut

sub help : Local {
    my ($self, $c) = @_;

    require Pod::Simple::HTML;
    my $p = Pod::Simple::HTML->new;
    $p->output_string(\$c->stash->{pod_output});
    $p->parse_file('lib/Zaaksysteem/Controller/File.pm');
    $c->stash->{template} = 'pod.tt';
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
