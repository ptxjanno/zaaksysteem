package Zaaksysteem::Controller::Beheer::Zaaktypen;
use Moose;
use namespace::autoclean;

use Hash::Merge::Simple qw( clone_merge );
use JSON qw(decode_json);
use List::Util qw(first);
use List::MoreUtils qw(distinct);
use Params::Profile;
use BTTW::Tools;

use Zaaksysteem::Constants qw(
    BASE_RELATION_ROLES
    BETROKKENE_RELATEREN_MAGIC_STRING_SUGGESTION
);

BEGIN { extends 'Zaaksysteem::Controller' }

with 'MooseX::Log::Log4perl';

use constant ZAAKTYPEN              => 'zaaktypen';
use constant ZAAKTYPEN_MODEL        => 'DB::Zaaktype';
use constant CATEGORIES_DB          => 'DB::BibliotheekCategorie';

sub base : Chained('/') : PathPart('beheer/zaaktypen'): CaptureArgs(1) {
    my ( $self, $c, $zaaktype_id ) = @_;

    $c->assert_any_user_permission(qw[beheer beheer_zaaktype_admin]);

    $c->response->headers->header('Cache-Control', 'No-store');

    if($zaaktype_id && $zaaktype_id ne 'create') {
        my $zaaktype;
        if ($zaaktype_id =~ /^[0-9]+$/) {
            $zaaktype = $c->model('DB::Zaaktype')->find($zaaktype_id, {
                prefetch => 'zaaktype_node_id'
            });
        } else {
            my $object_data = $c->model('DB::ObjectData')->search(
                {
                    uuid => $zaaktype_id,
                    object_class => "casetype",
                },
            )->single;

            $zaaktype = $c->model('DB::Zaaktype')->find($object_data->object_id, {
                prefetch => 'zaaktype_node_id'
            });
        }

        if (not defined $zaaktype) {
            throw(
                "beheer/zaaktypen/not_found",
                "No case type found with id or UUID '$zaaktype_id'",
            );
        }

        my $zaaktype_node = $zaaktype->zaaktype_node_id;

        $c->stash->{zaaktype_id}            = $zaaktype->id;
        $c->stash->{zaaktype_node_id}       = $zaaktype_node->id;
        $c->stash->{zaaktype_node_title}    = $zaaktype_node->titel;

    } else {
        $c->stash->{zaaktype_id}        = 'create';
        $c->stash->{zaaktype_node_id}   = 0;
    }
}


sub betrokkene_magic_string_prefix : Chained('/'): PathPart('beheer/zaaktypen/magic_string') {
    my ($self, $c) = @_;

    my $rol    = $c->req->params->{rol};
    my $prefix = $c->req->params->{prefix};

    my $magic_string = BETROKKENE_RELATEREN_MAGIC_STRING_SUGGESTION->([], $prefix, $rol);
    $c->res->body($magic_string);
    $c->detach();
}


sub zaaktypen_flush : Chained('/'): PathPart('beheer/zaaktypen/flush'): Args(1) {
    my ($self, $c, $zaaktype_id, $category_id) = @_;

    my @uri_args;

    if (defined $category_id) {
        push @uri_args, $category_id;
    }

    $c->assert_any_user_permission(qw[beheer beheer_zaaktype_admin]);

    die 'need zaaktype_id' unless($zaaktype_id);

    delete($c->session->{zaaktypen}->{$zaaktype_id});

    $c->v2_redirect_catalog($category_id, @uri_args);

    $c->res->body('');
    $c->detach;
}


sub flush : Chained('base'): PathPart('flush') {
    my ($self, $c) = @_;

    $c->assert_any_user_permission(qw[beheer beheer_zaaktype_admin]);

    my $zaaktype_id = $c->stash->{zaaktype_id};
    die 'need zaaktype_id' unless($zaaktype_id);

    delete($c->session->{zaaktypen}->{$zaaktype_id});

    $c->res->body("flushed zaaktype from your session: " . $zaaktype_id);
    $c->detach;
}


sub zaaktypen_clone : Chained('base'): PathPart('clone'): Args(0) {
    my ($self, $c)   = @_;

    my $zaaktype_id = $c->stash->{zaaktype_id} or die 'need zaaktype_id';

    # the cloned version will, once being re-commited, yield a copy
    $c->session->{zaaktypen}->{$zaaktype_id} = $c->model('Zaaktypen')->retrieve(
        id              => $zaaktype_id,
        as_session      => 1,
        as_clone        => 1,
    );

    my %request_params;
    if (my $url = $c->req->params->{return_url}) {
        $c->log->debug("Setting casetype_edit_return_url to '$url'");
        $request_params{return_url} = $url;
    }

    $c->res->redirect(
        $c->uri_for("/beheer/zaaktypen/$zaaktype_id/bewerken", \%request_params)
    );
}

=head2 _zaaktype_error

Helper function to bounce users into an error.

=cut

sub _zaaktype_error : Private {
    my ($self, $c, $msg, $id ) = @_;
    $c->push_flash_message({
        message => $c->stash->{error_flash_message},
        type    => 'error',
    });
    $c->res->redirect($c->stash->{uri_for});
    $c->detach;
}


sub zaaktypen_verwijder : Chained('base'): PathPart('verwijder'): Args(0) {
    my ($self, $c) = @_;

    my $zt_node = $c->model('Zaaktypen')->retrieve(nid => $c->stash->{zaaktype_node_id},);

    if (!$zt_node) {
        $c->log->warn("Unable to find case type node with ID: " . $c->stash->{zaaktype_node_id});
        $c->stash->{error_flash_message} = "Zaaktype kon niet gevonden worden";
        $c->forward('_zaaktype_error');
    }


    my $zaaktype = $zt_node->zaaktype_id;

    if (!$zaaktype) {
        $c->log->warn("Unable to find case type with node ID: " . $c->stash->{zaaktype_node_id});
        $c->stash->{error_flash_message} = "Zaaktype kon niet gevonden worden";
        $c->forward('_zaaktype_error');
    }

    if (!$c->req->params->{confirmed}) {
        $c->stash->{confirmation}->{message} =
            'Weet u zeker dat u zaaktype "'
            . $zt_node->titel . '"  wilt verwijderen?'
            . ' Deze actie kan niet ongedaan gemaakt worden';

        $c->stash->{confirmation}->{type}           = 'yesno';
        $c->stash->{confirmation}->{uri}            = $c->req->uri;
        $c->stash->{confirmation}->{commit_message} = 1;

        $c->forward('/page/confirmation');
        $c->detach;
    }

    # I want this code to be above the confirmed status, so you cannot even
    # type a reason before receiving an error. It triggers some kind of JS error
    # which I do not want to investigate now.

    my $bibliotheek_categorie_id = $zaaktype->get_column('bibliotheek_categorie_id');
    my $action_path = '/beheer/bibliotheek';

    if ($bibliotheek_categorie_id) {
        $action_path = sprintf('%s/%s', $action_path, $bibliotheek_categorie_id);
    }

    $c->stash->{ uri_for } = $c->uri_for($action_path);

    eval {
        $zaaktype->assert_delete;
    };

    if ($@) {
        $c->log->error(sprintf("Unable to delete casetype '%s' with ID: %d, cases still refer to it", $zt_node->titel, $zaaktype->id));
        $c->stash->{error_flash_message} = sprintf('Zaaktype "%s" kan niet worden verwijderd, er refereren nog zaken naar', $zt_node->titel);
        $c->forward('_zaaktype_error');
    }

    my $event = $c->model('Zaaktypen')->verwijder(
        nid            => $zt_node->id,
        commit_message => $c->req->params->{commit_message},
    );
    if ($event) {
        $c->push_flash_message($event->onderwerp);
    }

    $c->res->redirect($c->stash->{uri_for});
    $c->detach;
}




sub zaaktypen_bewerken : Chained('base'): PathPart('bewerken'): CaptureArgs(0) {
    my ($self, $c)   = @_;

    my $params = $c->req->params();

    if (defined $params->{return_url}) {
        $c->log->debug("Setting casetype_edit_return_url to '$params->{return_url}'");
        $c->session->{api}{v2}{casetype}{url} = $params->{return_url};
    }

    if (defined $params->{folder_uuid}) {
        $c->session->{api}{v2}{casetype}{folder} = $params->{folder_uuid};
    }

    my $zaaktype_id = $c->stash->{zaaktype_id};
    # existing zaaktype
    if($zaaktype_id && $zaaktype_id ne 'create') {

        # load the zaaktype into the session, unless it's already there
        unless($c->session->{zaaktypen}->{$zaaktype_id}) {

            $c->session->{zaaktypen}->{$zaaktype_id} = $c->model('Zaaktypen')->retrieve(
                    id              => $zaaktype_id,
                    as_session      => 1,
                );
        }

    } else {
        # no zaaktype_id - we're creating one
        unless($c->session->{zaaktypen}->{create}) {

            $c->session->{zaaktypen}->{create} = {
                'create'    => 1,
                'node'      => {
                    'id'        => 0,
                    'version'   => 1,
                },
                active      => 0,
            };
        }

        if ($params->{folder_uuid}) {
            my $category = $c->model('DB::BibliotheekCategorie')->search(
                { uuid => $params->{folder_uuid} }
            )->single;

            $c->stash->{categorie_id} = $category->id;
        } else {
            $c->stash->{categorie_id} = $params->{bibliotheek_categorie_id};
        }
    }

    $c->stash->{baseaction} = $c->stash->{formaction}   = $c->uri_for(
        '/beheer/zaaktypen/' .
        ( $c->stash->{zaaktype_id} || 0)
        . '/bewerken'
    );

    $c->stash->{params} = $c->stash->{zaaktype} = $c->session->{zaaktypen}->{$zaaktype_id};

    $c->stash->{categorie_id} ||= $c->stash->{zaaktype}->{zaaktype}->{bibliotheek_categorie_id};

    $c->stash->{categorie_id} ||= $c->session->{categorie_id};

    if ($c->stash->{params}->{definitie}->{oud_zaaktype}) {
        $c->stash->{flash} = 'LET OP: Dit is een oud zaaktype uit versie 1.1.'
            . ' Bij publicatie worden alle oude zaaktypen bijgewerkt met de'
            . ' nieuwe fasenamen';
    }

    if (my $id = $c->session->{zaaktypen}{$zaaktype_id}{zaaktype}{bibliotheek_categorie_id}) {
        $c->session->{api}{v2}{casetype}{category} = $id;
    }

    $c->forward('_load_session_and_params');

    if($c->req->is_xhr && $c->req->params->{autosave}) {
        $c->stash->{json} = {success => 1};
        $c->forward('Zaaksysteem::View::JSONlegacy');
        $c->detach;
    }
}




sub zaaktypen_start : Chained('zaaktypen_bewerken'): PathPart(''): Args(0) {
    my ($self, $c) = @_;

    $c->forward('algemeen');
}




sub zaaktypen_view : Chained('base'): PathPart('view'): Args(0) {
    my ($self, $c) = @_;

    $c->stash->{catalogus} = $c->model('Zaaktype')->retrieve(
        nid => $c->stash->{zaaktype_node_id}
    );

    $c->stash->{template} = 'beheer/zaaktypen/view.tt';
}


sub get_steps {
    my ($self, $c) = @_;

    my @stappen_plan = qw/
        algemeen
        relaties
        acties
        milestone_definitie
        milestones
        auth
    /;

    if ($c->stash->{zaaktype}->{node}->{properties}->{is_casetype_mother}) {
        push @stappen_plan, 'children';
    }

    push @stappen_plan, 'finish';

    return @stappen_plan;
}

sub generate_steps : Private {
    my ($self, $c)   = @_;
    my $huidige_stap;

    my $stapnaam     = $c->stash->{huidige_stapnaam};
    my @stappen_plan = $self->get_steps($c);

    my $zaaktype_id = $c->stash->{zaaktype_id} or die 'need zaaktype_id';
    my $params = $c->req->params();

    my $vorige_stap;
    while (@stappen_plan && !$huidige_stap) {
        my $stap_data   = shift @stappen_plan;

        if ($stapnaam eq $stap_data) {
            if ($vorige_stap) {
                $c->stash->{vorige_stap}    = $vorige_stap;
            } elsif ($params->{goback}) {
                $c->forward('zaaktypen_flush', [$zaaktype_id]);
            }

            $huidige_stap = $stap_data;

            $c->stash->{volgende_stap}  = shift @stappen_plan;
        }

        $vorige_stap = $stap_data;
    }

    if ($params->{goback}) {
        if ($c->stash->{vorige_stapurl}) {
            $c->res->redirect(
                $c->stash->{vorige_stapurl}
            );
        } else {
            $c->res->redirect(
                $c->stash->{baseaction} . '/' .
                     $c->stash->{vorige_stap}
            );
        }
        $c->detach;
    }
}

sub _validate_part : Private {
    my ($self, $c)   = @_;

    $c->forward('generate_steps');

    # Submit
    if (
        $c->req->params && $c->req->params->{zaaktype_update} &&
        $c->stash->{validation}->{validation_profile}->{success} &&
        !($c->req->is_xhr && $c->req->params->{do_validation})
    ) {
        $self->merge_stash_into_session($c);

        my $destination = $c->req->param('destination') || '';
        if($destination eq 'finish') {
            $destination = '/beheer/zaaktypen/'.$c->stash->{zaaktype_id}.'/bewerken/finish';
        }
        if($destination) {
            $c->res->redirect(
                $c->uri_for($destination)
            );
        } else {
            $c->res->redirect(
                (
                    $c->stash->{volgende_stapurl} ||
                    $c->uri_for(
                        '/beheer/zaaktypen/'
                        . $c->stash->{zaaktype_id} . '/bewerken/'
                        . $c->stash->{volgende_stap}
                    )
                )
            );
        }
        $c->detach;
    }
}


sub merge_stash_into_session {
    my ($self, $c) = @_;

    delete($c->session->{params});

    my $tomerge = {};
    if ($c->stash->{params}->{params}) {
        $tomerge = $c->stash->{params}->{params};
    } else {
        $tomerge = $c->stash->{params};
    }

    # clean up temporary
    # should they be here in the first place?
    my $milestone_number = $c->stash->{milestone_number};
    if($milestone_number) {
        if(exists $tomerge->{statussen}->{ $c->stash->{milestone_number} }->{elementen}->{regels}) {
            my $regels = $tomerge->{statussen}->{ $c->stash->{milestone_number} }->{elementen}->{regels};
            foreach my $regel (values %$regels) {
                foreach my $key (keys %$regel) {
                    delete $regel->{$key} if($key =~ m|_previous|);
                }
            }
        }
    }

    my $zaaktype_id = $c->stash->{zaaktype_id} or die "need zaaktype_id";

    my $sessionmerged = clone_merge(
        $c->session->{zaaktypen}->{$zaaktype_id},
        $tomerge
    );


    ### Delete tmp session data
    $c->session->{zaaktypen}->{$zaaktype_id} = $sessionmerged;
}


sub _load_params_status_update : Private {
    my ($self, $c)  = @_;

    ### We need to reshake the params a bit, just to make it easy for
    ### javascript to work with the dynamic tables

    my $mnumber             = $c->req->params->{milestone_number};
    my $zaaktype_id = $c->stash->{zaaktype_id} or die 'need zaaktype_id';

    ### Load all data according to template
    #my $zaaktype_template   = $c->model('Zaaktypen')->session_template;

    ### Session parameters
    $c->stash->{milestone_number}   = $mnumber;


    #### Default parameters
    my %found_elementdata   = ();
    my $newreqparams        = {};

    ### Let's make sure we got all the checkboxes
    my @given_checkboxes = ();
    if ($c->req->params->{ezra_checkboxes}) {
        if (UNIVERSAL::isa($c->req->params->{ezra_checkboxes}, 'ARRAY')) {
            @given_checkboxes = @{ $c->req->params->{ezra_checkboxes} };
        } else {
            push(@given_checkboxes,
                $c->req->params->{ezra_checkboxes}
            );
        }
    }

    for my $paramkey (keys %{ $c->stash->{req_params} }) {
        my $paramvalue          = $c->stash->{req_params}->{$paramkey};

        next unless $paramkey   =~ /^params\.status\./;

        my $paramkeyoriginal = $paramkey;

        ### Are we able to work with this param?
        if (
            $paramkey !~ /^params\.status\.[\w\d_]+\.[\w\d_]+\.\d+$/
        ) {
            ### Remove bogus keys
            next;
        }

        ### Make param zaaksysteem readable
        {
            for my $given_checkbox (@given_checkboxes) {
                if (
                    !$c->stash->{req_params}->{$given_checkbox} &&
                    $given_checkbox eq $paramkey
                ) {
                    $paramvalue = undef;
                }
            }

            ### CHANGE: params.status.notificatie.label.1
            ### INTO: params.statussen.notificatie.1.label
            $paramkey   =~
            s/^params\.status(\.[\w\d_]+)(\.[\w\d_]+)(\.\d+)$/params\.statussen\.$mnumber\.elementen$1$3$2/g;

            ### Delete deprecated entry and set new one
            $newreqparams->{$paramkey} = (
                $paramvalue ? $paramvalue : undef
            );
        }

        ### Load kenmerk dialog data into params
        {
            my ($element, $elementnumber)   = $paramkey =~
                /^params\.statussen\.$mnumber\.elementen\.([\w\d_]+)\.(\d+)/;

            if (! defined($found_elementdata{$element . $elementnumber}) ) {
                my $elementdata = $c->session->{zaaktypen}
                    ->{$zaaktype_id}
                    ->{statussen}
                    ->{ $mnumber }
                    ->{ 'elementen' }
                    ->{ $element }
                    ->{ $elementnumber };

                if ($elementdata) {
                    for my $key (keys %{ $elementdata }) {
                        next if exists $newreqparams->{
                            'params.statussen.' . $mnumber
                            . '.elementen.' . $element
                            . '.' . $elementnumber . '.' . $key
                        };

                        $newreqparams->{
                            'params.statussen.' . $mnumber
                            . '.elementen.' . $element
                            . '.' . $elementnumber . '.' . $key
                        } = $elementdata->{$key};
                    }
                }

                $found_elementdata{$element . $elementnumber} = 1;
            }
        }
    }

    $c->stash->{req_params} = $newreqparams;

}

sub _handle_deleted_entries {
    my ($self, $c) = @_;

    ### Only for milestone handling
    return unless $c->stash->{milestone_number};

    my $zaaktype_id = $c->stash->{zaaktype_id} or die "need zaaktype_id!";
    my $milestone_number = $c->stash->{milestone_number} or die "need milestone number";


    my $old_params = $self->_get_session_zaaktype_status($c, {
        zaaktype_id => $zaaktype_id,
        milestone   => $milestone_number,
    })->{elementen};


    my $new_params  = $c->stash
        ->{new_params}
        ->{params}
        ->{statussen}
        ->{ $milestone_number }
        ->{ elementen };

    for my $element ( keys %{ $old_params }) {
        unless ($new_params->{ $element }) {

            delete $old_params->{$element};

            next;
        }
        for my $i (keys %{ $old_params->{ $element } }) {
            unless ($new_params->{ $element }->{ $i }) {

                delete $old_params->{$element}->{$i};
            }
        }
    }
}



# TODO every sub should use this or similar to retrieve zaaktype data from session.
# session police.
Params::Profile->register_profile(
    method  => '_get_session_zaaktype_status',
    profile => {
        required => [qw/zaaktype_id milestone/],
    }
);
sub _get_session_zaaktype_status {
    my ($self, $c, $params) = @_;

    my $dv = Params::Profile->check(params  => $params);
    die "invalid options for _get_session_zaaktype_status" unless $dv->success;

    my $zaaktype_id = $params->{zaaktype_id};
    my $milestone   = $params->{milestone};

    return $c->session->{zaaktypen}->{$zaaktype_id}->{params}->{statussen}->{$milestone} ||= {};
}



{
    sub _load_session_and_params : Private {
        my ($self, $c)   = @_;

        return unless $c->req->params && $c->req->params->{zaaktype_update};



        ### Clone parameters
        $c->stash->{req_params} = { %{ $c->req->params } };

        my $params = $c->req->params();

        if(exists $params->{'zaaktype_betrokkenen.betrokkene_type'}) {

            my $betrokkene_checkboxes = $params->{'zaaktype_betrokkenen.betrokkene_type'};

            $betrokkene_checkboxes = [$betrokkene_checkboxes]
                unless(
                    ref $betrokkene_checkboxes &&
                    ref $betrokkene_checkboxes eq 'ARRAY'
                );
            my $checkbox_map = {map {$_, 1} @$betrokkene_checkboxes};

            if(exists $checkbox_map->{preset_client}) {
                my $preset_client = $c->req->params->{preset_client};
                $c->stash->{req_params}->{'definitie.preset_client'} = $preset_client;
            } else {
                $c->stash->{req_params}->{'definitie.preset_client'} = undef;
            }
        }

        if (exists $params->{'default_directory_marker'}) {
            if (not exists $c->stash->{req_params}{'node.properties.default_directories'}) {
                $c->stash->{req_params}{'node.properties.default_directories'} = [];
            }
            elsif (ref $c->stash->{req_params}{'node.properties.default_directories'} ne 'ARRAY') {
                $c->stash->{req_params}{'node.properties.default_directories'} = [
                    $c->stash->{req_params}{'node.properties.default_directories'}
                ];
            }
        }

        ### Let's make sure we got all the checkboxes
        if ($c->stash->{req_params}->{ezra_checkboxes}) {
            my @given_checkboxes = ();
            if (UNIVERSAL::isa($c->stash->{req_params}->{ezra_checkboxes}, 'ARRAY')) {
                @given_checkboxes = @{ $c->stash->{req_params}->{ezra_checkboxes} };
            } else {
                push(@given_checkboxes,
                    $c->stash->{req_params}->{ezra_checkboxes}
                );
            }

            for my $given_checkbox (@given_checkboxes) {
                if (!$c->stash->{req_params}->{$given_checkbox}) {
                    $c->stash->{req_params}->{$given_checkbox} = undef;
                }
            }

            delete($c->stash->{req_params}->{ezra_checkboxes});
        }

        ### Reorden 'special' params status update
        if ($c->req->params->{status_update}) {
            $c->forward('_load_params_status_update');
        }

        ### Strip whitespace from numeric fields
        for my $key (qw(definitie.afhandeltermijn definitie.servicenorm)) {
            next unless exists $c->stash->{req_params}{$key};

            $c->stash->{req_params}{$key} =~ s/^\s+|\s+$//g;
        }

        ### Update params
        $c->stash->{new_params} = {};
        for my $param (keys %{ $c->stash->{req_params} }) {
            # Security, only test.bla[.bla.bla]
            next unless $param  =~ /^[\w\d\_]+\.[\w\d\_\.]+$/;

            my $eval = '$c->stash->{new_params}->{';
            $eval   .= join('}->{', split(/\./, $param)) . '}';

            $eval   .= ' = $c->stash->{params}->{';
            $eval   .= join('}->{', split(/\./, $param)) . '}';

            $eval   .= ' = $c->stash->{req_params}->{\'' . $param . '\'}';
            eval($eval);
        }


        $self->clean_empty_rows($c->stash->{new_params});

        $self->_handle_deleted_entries($c);

        my $zaaktype_id = $c->stash->{zaaktype_id} or die 'need zaaktype_id';
        ### Resort params
        if ($c->req->params->{status_update}) {

            foreach my $elementnaam (
                keys %{ $c->stash->{params}->{params}->{statussen}->{
                        $c->stash->{milestone_number}
                    }->{elementen}
                }
            ) {
                my $sorted_params = {
                    map {
                        $_->{mijlsort} => $_
                    } values %{
                        $c->stash->{params}->{params}->{statussen}->{
                            $c->stash->{milestone_number}
                        }->{elementen}->{$elementnaam}
                    }
                };
                $c->stash->{params}->{params}->{statussen}->{
                    $c->stash->{milestone_number}
                }->{elementen}->{$elementnaam} = $sorted_params;
            }

            ### OK...finalize
            $c->session->{zaaktypen}->{$zaaktype_id}->{statussen}->{
                $c->stash->{milestone_number}
            }->{elementen} = $c->stash->{params}->{params}->{statussen}->{
                $c->stash->{milestone_number}
            }->{elementen};
        }


        if (exists($c->stash->{req_params}->{'zaaktype_betrokkenen.betrokkene_type'})) {
            my @betrokkenen = ();
            if ($c->stash->{req_params}{'node.trigger'} eq 'intern') {
                # When setting a casetype to "intern only", only allow medewerkers
                @betrokkenen = ('medewerker');
            }
            else {
                if (
                    UNIVERSAL::isa(
                        $c->req->params->{'zaaktype_betrokkenen.betrokkene_type'},
                        'ARRAY'
                    )
                ) {
                    push(@betrokkenen,
                        @{ $c->req->params->{'zaaktype_betrokkenen.betrokkene_type'} }
                    );
                } elsif ($c->req->params->{'zaaktype_betrokkenen.betrokkene_type'}) {
                    push(@betrokkenen,
                        $c->req->params->{'zaaktype_betrokkenen.betrokkene_type'}
                    );
                }
            }

            my $counter = 0;
            $c->stash->{params}->{betrokkenen} = {};
            for my $betrokkene (distinct @betrokkenen) {
                $c->stash->{params}->{betrokkenen}->{
                    ++$counter
                }   = {
                    betrokkene_type => $betrokkene,
                }
            }
        }

        ### Validate params
        $c->stash->{validation} = $c->model('Zaaktypen')->validate_session(
            'session'       => $c->stash->{params},
            'zs_fields'     => [ keys %{ $c->req->params } ],
        );

        ### Give JSON some feedback
        if ($c->req->is_xhr && $c->req->params->{do_validation}) {
                $c->zcvalidate($c->stash->{validation}->{validation_profile});
                $c->detach;
        }
    }
}
=head2 clean_empty_rows

The javascript frontend is programmed in such a way that when you cancel
the component creation dialog you are left with an empty row. This
can be easily removed but also easily forgotten. This routine does cleanup
getting rid of a useless rows that would otherwise cause a die.

=cut

sub clean_empty_rows {
    my ($self, $params) = @_;

    # anti auto-vivification
    return unless my $statussen = $params->{params}->{statussen};

    ### Filters
    my $filterkeys = {
        notificaties            => ['bibliotheek_notificaties_id'],
        regels                  => ['naam'],
        kenmerken               => [qw(naam label)],
        checklists              => ['label'],
        relaties                => ['relatie_zaaktype_id'],
        resultaten              => ['resultaat'],
        standaard_betrokkenen   => ['betrokkene_identifier'],
    };

    my $filter = sub {
        my ($elementen, $element, $checkkey) = @_;

        # once again - programmed this way to stop auto-vivification
        my $tocheck = $elementen->{$element} or return;

        foreach my $k (keys %$tocheck) {
            my $v = $tocheck->{$k};
            if (!grep {length($v->{$_})} @$checkkey) {
                delete $tocheck->{$k};
            }
        }
    };

    for my $filterkey (keys %$filterkeys) {
        $filter->($_->{elementen}, $filterkey, $filterkeys->{$filterkey}) for values %$statussen;
    }
}

sub algemeen : Chained('zaaktypen_bewerken'): PathPart('algemeen') {
    my ($self, $c) = @_;

    $c->stash->{bib_cat}        = $c->model(CATEGORIES_DB)->search(
        {
            'system'    => { 'is' => undef },
            'pid'       => undef,
        },
        {
            order_by    => ['pid','naam']
        }
    );

    $c->stash->{huidige_stapnaam}   = 'algemeen';

    $c->stash->{formaction}         .= '/algemeen';

    $c->forward('_validate_part');

    ### Speciale webformulieren
    {
        $c->stash->{speciale_webformulieren} = [];
        if (-d $c->config->{root} . '/tpl/zaak_v1/nl_NL/form/custom') {
            opendir(my $DIR, $c->config->{root} . '/tpl/zaak_v1/nl_NL/form/custom');
            while (my $file = readdir($DIR)) {
                next unless $file =~ /\.tt$/;

                $file =~ s/\.tt$//;

                push(@{ $c->stash->{speciale_webformulieren} },
                    $file
                );
            }
        }
    }

    $c->stash->{template}   = 'beheer/zaaktypen/algemeen/edit.tt';
}

sub relaties : Chained('zaaktypen_bewerken'): PathPart('relaties') {
    my ($self, $c) = @_;

    $c->stash->{huidige_stapnaam}   = 'relaties';

    $c->stash->{formaction}         .= '/relaties';

    ## Custom
    {
        $c->stash->{zaaktype_betrokkenen} = {};

        while (
            my ($bid, $betrokkene) = each %{
                $c->stash->{zaaktype}->{betrokkenen}
            }
        ) {
            $c->stash->{zaaktype_betrokkenen}->{
                $betrokkene->{betrokkene_type}
            } = 1;
        }
    }

    $c->forward('_validate_part');

    $c->stash->{template}   = 'beheer/zaaktypen/relaties/edit.tt';
}

sub acties : Chained('zaaktypen_bewerken'): PathPart('acties') {
    my ($self, $c) = @_;

    $c->stash->{huidige_stapnaam}   = 'acties';

    $c->stash->{formaction}         .= '/acties';

    $c->forward('_validate_part');

    $c->forward('case_action_checkboxes');
    $c->stash->{template}   = 'beheer/zaaktypen/acties/edit.tt';
}

sub children : Chained('zaaktypen_bewerken'): PathPart('children') {
    my ($self, $c) = @_;

    $c->stash->{huidige_stapnaam}   = 'children';
    $c->stash->{formaction}         .= '/children';

    $c->forward('_validate_part');
    $c->forward('children_titles');
    $c->stash->{template}   = 'beheer/zaaktypen/children.tt';
}


sub children_titles : Private {
    my ($self, $c) = @_;

    my $children = $c->stash->{zaaktype}->{node}->{properties}->{child_casetypes};

    # consolidate database query
    my @children_ids = map { $_->{casetype}{id} } @$children;

    my %titles = map { $_->id, $_->zaaktype_node_id->titel }
        $c->model('DB::Zaaktype')->search({ id => \@children_ids})->all;

    # supply frontend with handy structure
    foreach my $casetype (@$children) {
        my $id = $casetype->{casetype}{id};
        $casetype->{casetype} = { id => $id, title => $titles{$id} };
    }

    $c->stash->{child_casetypes} = $children;
}


sub auth : Chained('zaaktypen_bewerken'): PathPart('auth') {
    my ($self, $c) = @_;

    $c->stash->{huidige_stapnaam}   = 'auth';
    $c->stash->{formaction}         .= '/auth';

    $c->forward('_validate_part');
    $c->stash->{template}   = 'beheer/zaaktypen/auth/edit.tt';
}


sub finish : Chained('zaaktypen_bewerken'): PathPart('finish') {
    my ($self, $c) = @_;

    $c->stash->{huidige_stapnaam}   = 'finish';
    $c->stash->{formaction}         .= '/finish';

    $c->forward('_validate_part');

    $c->detach('publish') if $c->req->params->{commit};

    $c->stash->{template}   = 'beheer/zaaktypen/finish/view.tt';
}


=head2 assert_component_change

At least one checked checkbox is required

=cut

sub assert_component_change {
    my ($self, $c) = @_;

    my @components = $c->req->param('components');
    die "At least one component should be changed to warrant a change" unless @components;
    $c->stash->{commit_components} = \@components;
    return;
}


sub publish : Private {
    my ($self, $c) = @_;

    $self->assert_component_change($c);
    $c->stash->{commit_message} = $c->req->params->{commit_message};

    my $casetype = $c->stash->{zaaktype};

    $c->detach('publish_single') unless $casetype->{node}->{properties}->{is_casetype_mother};

    my $casetype_display = {
        casetype => {
            id    => $casetype->{zaaktype}->{id},
            title => $casetype->{node}->{titel}
        }
    };

    my @children = $c->model('Zaaktypen')->list_child_casetypes({ mother => $casetype });

    $c->stash->{publish_queue} = [$casetype_display, @children];
    $c->stash->{template} = 'beheer/zaaktypen/finish/publish.tt';
}


sub publish_single : Private {
    my ($self, $c) = @_;

    # undef category is fine for zaaktypen_flush
    my $category_id = eval {
        $c->stash->{ zaaktype }{ zaaktype }{ bibliotheek_categorie_id }
    };

    my $zaaktype_id;
    $c->model('DB')->schema->txn_do(
        sub {
            $c->model('Zaaktypen')->commit_session(
                session           => $c->stash->{zaaktype},
                commit_message    => $c->stash->{commit_message},
                commit_components => $c->stash->{commit_components},
            );

            $zaaktype_id = $c->stash->{zaaktype_id};

            return if !$zaaktype_id or $zaaktype_id eq 'create';

            # Cleanup old children when needed
            my $rs = $c->model('DB::ZaaktypeNode')
                ->search_rs({ moeder_zaaktype_id => $zaaktype_id });
            $rs->update({ moeder_zaaktype_id => undef });
        }
    );

    $c->push_flash_message('Zaaktype succesvol bijgewerkt');

    # zaaktypen_flush detaches
    $c->forward('zaaktypen_flush', [ $zaaktype_id, $category_id ]);
}


sub confirm_finish : Chained('') : PathPart('beheer/zaaktypen/finish_confirm') {
    my ($self, $c) = @_;

    $c->stash->{nowrapper} = 1;
    $c->stash->{template} = 'widgets/confirm.tt';
}


sub _add_milestone : Private {
    my ($self, $c) = @_;

    my @statusnums  = sort { $a <=> $b } keys %{ $c->stash->{zaaktype}->{statussen} };
    my $laststatus  = pop @statusnums;

    ### Reroute
    $c->stash->{zaaktype}->{statussen}->{ $laststatus }->{definitie}->{status} =
        ($laststatus + 1);

    $c->stash->{last_milestone} = $laststatus + 1;

    $c->stash->{zaaktype}->{statussen}->{ ($laststatus + 1) } =
        $c->stash->{zaaktype}->{statussen}->{ $laststatus };

    $c->stash->{zaaktype}->{statussen}->{ $laststatus } = {
        'elementen' => {},
        'definitie' => {
            status  => $laststatus,
            create  => 1,
            ou_id   => $c->stash->{zaaktype}->{statussen}->{ $laststatus }
                ->{definitie}->{ou_id},
            role_id => $c->stash->{zaaktype}->{statussen}->{ $laststatus }
                ->{definitie}->{role_id}
        }
    };
}

sub _verify_milestone_definitie : Private {
    my ($self, $c) = @_;

    if ($c->req->params->{definitie_update}) {
        {
            my %availablestatussen = ();
            my $statussen;
            for my $key ( grep { /statussen\./ } keys %{ $c->req->params } ) {
                my ($statusid) = $key =~ /statussen\.(\d+)\./;

                if (! $availablestatussen{$statusid} ) {
                    if (
                        $c->req->params->{
                            'statussen.' . $statusid . '.definitie.naam'
                        }
                    ) {
                        $statussen->{$statusid} = $c->stash->{zaaktype}
                            ->{statussen}->{ $statusid };
                    }
                }

                $availablestatussen{$statusid} = 1;
            }

            $c->stash->{zaaktype}->{statussen} = $statussen;
        }

        ### Update data
        {
            my $params = $c->req->params;
            my @statusnums     = sort { $a <=> $b } keys %{ $c->stash->{zaaktype}->{statussen} };
            my ($role_id,$ou_id);
            for my $statusnr (@statusnums) {


                # Naam
                $c->stash->{zaaktype}->{statussen}->{ $statusnr }
                    ->{definitie}->{naam}   = $c->req->params->{
                        'statussen.' . $statusnr . '.definitie.naam'
                    };

                my $term = $params->{ "statussen.$statusnr.definitie.termijn" };
                if ($term) {
                    $c->stash->{zaaktype}->{statussen}->{$statusnr}
                        ->{definitie}->{termijn} = $term;
                }
                else {
                    delete $c->stash->{zaaktype}->{statussen}->{$statusnr}
                        ->{definitie}->{termijn};
                }

                # Fase
                $c->stash->{zaaktype}->{statussen}->{ $statusnr }
                    ->{definitie}->{fase}   = $c->req->params->{
                        'statussen.' . $statusnr . '.definitie.fase'
                    };

                my $role_set = $c->req->params->{
                    'statussen.' . $statusnr .  '.definitie.role_set'
                };

                # ou_id
                if (
                    $role_set || $statusnr == 1
                ) {
                    $ou_id      = $c->req->params->{
                        'statussen.' . $statusnr . '.definitie.ou_id'
                    };

                    $role_id    = $c->req->params->{
                        'statussen.' . $statusnr . '.definitie.role_id'
                    };
                }


                $c->stash->{zaaktype}->{statussen}->{ $statusnr }
                    ->{definitie}->{ou_id}   = $ou_id;

                $c->stash->{zaaktype}->{statussen}->{ $statusnr }
                    ->{definitie}->{role_set}   = $role_set;

                # scope_id
                $c->stash->{zaaktype}->{statussen}->{ $statusnr }
                    ->{definitie}->{role_id}   = $role_id;

            }
        }
    }

    ### Resort data
    {
        my @statusnums     = sort { $a <=> $b } keys %{ $c->stash->{zaaktype}->{statussen} };
        my ($statussen, $counter) = ({}, 0);
        my ($role_id, $ou_id) = @_;
        for my $statusnr (@statusnums) {
            ++$counter;
            my $statusdata  = $c->stash->{zaaktype}->{statussen}->{$statusnr};

            ### Make DAMN sure counter is the same as status
            $statusdata->{definitie}->{status} = $counter;

            $statussen->{$counter} = $statusdata;

            ### XXX Dit is pre-2.1 code, wat alleen gebruikt wordt bij
            ### bestaande zaaktypen van voor 2.1 die bewerkt worden. Het vinkje:
            ### toewijziging activeren staat standaard uit, en moet even aangezet
            ### worden voor afwijkende roles/ou's
            ### {
            if (
                !$c->req->params->{definitie_update} &&
                (
                    $statusdata->{definitie}->{ou_id} ne $ou_id ||
                    $statusdata->{definitie}->{role_id} ne $role_id
                )
            ) {
                $ou_id      = $statusdata->{definitie}->{ou_id};
                $role_id    = $statusdata->{definitie}->{role_id};
                $statusdata->{definitie}->{role_set} = 1;
            }
            ### }
        }

        $c->stash->{zaaktype}->{statussen} = $statussen;
    }
}

sub milestone_definitie : Chained('zaaktypen_bewerken'): PathPart('milestone_definitie') {
    my ($self, $c) = @_;

    $c->stash->{huidige_stapnaam}   = 'milestone_definitie';

    $c->stash->{formaction}         .= '/milestone_definitie';

    $c->forward('_verify_milestone_definitie');
    ### Ajax action
    if ($c->req->is_xhr && $c->req->params->{action}) {
        $c->forward('_add_milestone');

        $c->stash->{nowrapper} = 1;
        $c->stash->{template} =
            'beheer/zaaktypen/milestone_definitie/ajax_table.tt';
        $c->detach;
    }

    $c->forward('_validate_part');

    if (
        !$c->stash->{zaaktype}->{statussen} ||
        scalar(keys %{ $c->stash->{zaaktype}->{statussen} }) < 2
    ) {
        $c->stash->{zaaktype}->{statussen} = {
            1   => {
                'definitie' => {
                    'naam'      => 'Geregistreerd',
                    'fase'      => 'Registreren',
                    'status'    => 1,
                },
            },
            2   => {
                'definitie' => {
                    'naam'      => 'Afgehandeld',
                    'fase'      => 'Afhandelen',
                    'status'    => 2,
                }
            },
        };
    }

    if ($c->stash->{zaaktype}->{definitie}->{oud_zaaktype}) {
        my $laatste_status = scalar(
            keys(%{ $c->stash->{zaaktype}->{statussen} })
        );

        $c->stash->{zaaktype}->{statussen}->{1}
            ->{definitie}->{fase} = 'Registreren';
        $c->stash->{zaaktype}->{statussen}->{$laatste_status}
            ->{definitie}->{fase} = 'Afhandelen';
    }

    $c->stash->{template}   = 'beheer/zaaktypen/milestone_definitie/edit.tt';
}


sub milestones_base : Chained('zaaktypen_bewerken'): PathPart('milestones'): CaptureArgs(1) {
    my ($self, $c, $milestone_number) = @_;

    $c->stash->{milestone_number}   = $milestone_number;
    $c->stash->{page_title} = "Fase $milestone_number";

    if ($c->stash->{milestone_number} < 2) {
        $c->stash->{milestone_first} = 1;
    }

    if (
        $c->stash->{zaaktype}->{statussen} &&
        scalar(keys %{
            $c->stash->{zaaktype}->{statussen}
            }
        ) == $c->stash->{milestone_number}
    ) {
        $c->stash->{milestone_last} = 1;
    }

    $c->stash->{milestone} = $c->stash->{zaaktype}->{statussen}->{
        $c->stash->{milestone_number}
    };

    $c->stash->{milestoneurl}       .= $c->stash->{formaction} . '/milestones';
    $c->stash->{formaction}         .= '/milestones/'
        . $c->stash->{milestone_number};

    ### Define volgende stap bypass:
    if ($c->stash->{milestone_number} > 1) {
        $c->stash->{vorige_stapurl} =
            $c->stash->{milestoneurl} . '/' .
            ($c->stash->{milestone_number} - 1)
    }


    if ($c->req->params->{status_update}) {
        if (
            $c->stash->{zaaktype}->{statussen}->{
                ($c->stash->{milestone_number} + 1)
            }
        ) {
            $c->stash->{volgende_stapurl} =
                $c->stash->{milestoneurl} . '/' .
                ($c->stash->{milestone_number} + 1)
        }
    }

    $c->stash->{huidige_stapnaam}   = 'milestones';

    $c->forward('_validate_part');
}


sub milestones_start : Chained('zaaktypen_bewerken'): PathPart('milestones'): Args(0) {
    my ($self, $c) = @_;

    ### Forward to milestone number 1
    $c->forward('milestones_base', [1]);
    $c->forward('milestones');
}


sub milestones : Chained('milestones_base'): PathPart(''): Args(0) {
    my ($self, $c) = @_;

    my $zaaktype_id = $c->stash->{zaaktype_id};

    $c->stash->{integrity}->{rules} = Zaaksysteem::Backend::Rules->integrity(
        {
            from_session    => $c->session->{zaaktypen}->{$zaaktype_id}
        },
        $c->model('DB')
    )->for_status_number($c->stash->{milestone_number});

    $c->stash->{template} = 'beheer/zaaktypen/milestones/edit.tt';
}


sub _dialog_process_post : Private {
    my ($self, $c) = @_;

    return unless $c->req->params->{update};

    my $zaaktype_id = $c->stash->{zaaktype_id} or die 'need zaaktype_id';

    my $element     = $c->stash->{zaaktypen_tmp_store_element};

    my $dialog_data = $c->session->{zaaktypen}
        ->{$zaaktype_id}
        ->{statussen}
        ->{ $c->stash->{milestone_number} }
        ->{ 'elementen' }
        ->{ $element }
        ->{ $c->stash->{rownumber} };

    my $elemdata    = {
        map {
            my $key = $_;
            $key    =~ s/^${element}_//;
            $key    => $c->req->params->{ $_ }
        } grep {
            $_ =~ /^${element}_/
        } %{ $c->req->params } };

    if ($c->req->params->{update_regel_editor}) {
        # Not interested in the past. Deletion of a condition or action should not keep
        # old data. So just get rid of old stuff.
        $dialog_data = $elemdata;
    }

    if ($elemdata->{required_permissions}) {
        # if our JS wasn't executed, we try to insert "[<getJSON>] or
        # similar. Check if it is really JSON
        $self->_assert_json($elemdata->{required_permissions});
    }

    if ($element eq 'standaard_betrokkenen') {
        if (my $id = $elemdata->{betrokkene_identifier}) {
            my $betrokkene = $c->model('DB')->schema->betrokkene_model->get({}, $id);
            if (!$betrokkene) {
                throw("zaaktype/beheer/betrokkene_id/invalid", "No betrokkene found with id $id");
            }

            $dialog_data->{naam} = $betrokkene->display_name;
        }
    }

    $self->_inflate_date_limit_options($elemdata);

    my $s = Zaaksysteem::Backend::Rules::Serializer->new(
        schema => $c->model('DB')->schema,
    );

    $elemdata = $s->sanitize_rule($elemdata);

    # NEXT, drop empty checkboxes
    my $checkboxes = $c->req->params->{dialog_checkboxes};
    if ($checkboxes) {
        my @dialog_checkboxes  = ref $checkboxes eq 'ARRAY' ? @{$checkboxes} : ($checkboxes);

        for (@dialog_checkboxes) {
            my $key = $_;
            $key    =~ s/^${element}_//;
            if (!$elemdata->{ $key }) {
                $elemdata->{ $key } = undef;
            }
        }
    }

    if ($c->stash->{objecttype}) {
        for my $key (keys %{ $elemdata }) {
            my $val = ($key =~ m/_label$/ ? $elemdata->{ $key } : int($elemdata->{ $key }));

            $dialog_data->{ object_metadata }{ $key } = $val;
        }
    } else {
        $dialog_data->{ $_ } = $elemdata->{ $_ } for keys %{ $elemdata };
        $self->_set_attribute_group_pip($dialog_data, $elemdata);
    }

    my @properties_fields = qw[
        map_case_location
        map_wms_layer_id
        map_wms_feature_attribute_id
        map_wms_feature_attribute_label
        skip_change_approval
        text_content
    ];

    for (@properties_fields) {
        $dialog_data->{ properties }{ $_ } = delete $dialog_data->{ $_ };
    }

    # database chokes on an empty string value, but it likes undefs
    $dialog_data->{bibliotheek_kenmerken_id} ||= undef;

    $c->session->{zaaktypen}
        ->{$zaaktype_id}
        ->{statussen}
        ->{ $c->stash->{milestone_number} }
        ->{ 'elementen' }
        ->{ $c->stash->{zaaktypen_tmp_store_element} }
        ->{ $c->stash->{rownumber} } = $dialog_data;

    return 1;
}

sub _assert_json {
    my ($self, $data) = @_;

    try {
        decode_json($data);
    }
    catch {
        $self->log->warn("This is not JSON: $_");
        throw('zaaktypen/required_permissions/not_json', "You did not submit JSON content");
    }
    return 1;

}

=head2 _set_attribute_group_pip

Set the PIP enabled booleanish value for groups.

=cut

sub _set_attribute_group_pip {
    my ($self, $d, $e) = @_;
    if ($d->{is_group}) {
        $d->{pip} = $e->{pip} // undef;
        return 1;
    }
    return 0;
}

sub _deflate_date_limit_options {
    my ($self, $params) = @_;

    if (($params->{type} // '') eq 'date') {
        my $dl = $params->{properties}->{date_limit};
        foreach my $s (qw(start end)) {
            foreach (keys %{$dl->{$s}}) {
                my $key = "date_limit_" . $s . "_". $_;
                $params->{$key} = $dl->{$s}{$_};
            }
        }
        return 1;
    }
    return 0;
}

sub _inflate_date_limit_options {
    my ($self, $data) = @_;

    my @keys = grep { $_ =~ /^date_limit/ } keys %$data;
    return 0 if !@keys;

    my %properties = ();
    foreach my $k (@keys) {
        my (undef, undef, $key, $subkey) = split(/_/, $k);
        $properties{date_limit}{$key}{$subkey} = $data->{$k};
    }

    $data->{properties} = { %{$data->{properties} // {}}, %properties };
    return 1;
}

sub _dialog_load_kenmerken : Private {
    my ($self, $c) = @_;

    my $params = $c->req->params();

    if (
        $c->stash->{zaaktypen_tmp_store_element} eq 'kenmerken' &&
        !exists($c->stash->{params}->{id}) &&
        !exists($c->stash->{params}->{label}) &&
        $params->{edit_id} &&
        $params->{kenmerk_type} eq 'kenmerk'
    ) {
        my $bibliotheek_kenmerk =
            $c->model('DB::BibliotheekKenmerken')->find($params->{edit_id});

        if ($bibliotheek_kenmerk) {
            my @columns = $bibliotheek_kenmerk->result_source->columns;

            my %data;
            for my $column (@columns) {
                my $columnname      = $column;

                $columnname         = 'type'
                    if ($column eq 'value_type');

                $data{$columnname}  = $bibliotheek_kenmerk->$column;
            }

            $c->stash->{params} = \%data;
        }
    }

}

sub dialog_bewerken_base : Private {
    my ($self, $c) = @_;

    my $zaaktype_id             = $c->stash->{zaaktype_id} or die 'need zaaktype_id';

    $c->stash->{rownumber}      = $c->req->params->{rownumber} // 1;

    $c->forward('_dialog_process_post');

    $c->stash->{params} = $c->session->{zaaktypen}->{$zaaktype_id}->{statussen}
        ->{ $c->stash->{milestone_number} }->{elementen}
        ->{ $c->stash->{zaaktypen_tmp_store_element} }
        ->{ $c->stash->{rownumber} };

    $self->_deflate_date_limit_options($c->stash->{params});

    $c->forward('_dialog_load_kenmerken');
    $c->stash->{nowrapper}  = 1;
}

=head2 kenmerk_bewerken


=cut

sub kenmerk_bewerken : Chained('milestones_base'): PathPart('kenmerk'): Args() {
    my ($self, $c) = @_;

    my $params = $c->req->params;

    my $id = $c->req->params->{edit_id};
    throw( "ztb/kenmerk/404", "Unable to find kenmerk with id $id") unless $id;

    $c->stash->{zaaktypen_tmp_store}         = 'milestones';
    $c->stash->{zaaktypen_tmp_store_element} = 'kenmerken';
    $c->stash->{template} = 'beheer/zaaktypen/milestones/edit_kenmerk.tt';

    my $kenmerk = $c->model('DB::BibliotheekKenmerken')->search_rs({ id => $id})->first;
    throw("ztb/kenmerk/404", "Unable to find kenmerk with id $id") unless $kenmerk;

    $c->stash->{bibliotheek_kenmerk} = $kenmerk;

    # Only allow date limitations in the registration phase
    if ($kenmerk->value_type eq 'date' && $c->stash->{milestone_number} == 1 && $c->stash->{zaaktype_id} ne 'create') {
        my $zt = $c->model('DB::Zaaktype')->find($c->stash->{zaaktype_id});
        my $ztn = $zt->zaaktype_node_id;
        my $zts = $ztn->zaaktype_statussen->search({ status => 1 })->first;
        my $kenmerken = $zt->zaaktype_node_id->zaaktype_kenmerken->search(
            { 'bibliotheek_kenmerken_id.value_type' => 'date',
              zaak_status_id => $zts->id,
            },
            {
                distinct => 'bibliotheek_kenmerken_id.id',
                join     => 'bibliotheek_kenmerken_id',
                order_by => 'me.id'
            }
        );
        $c->stash->{datelimit_kenmerken} = $kenmerken;
    }

    if ($kenmerk->value_type eq 'geolatlon') {
        my $if = $c->model('DB::Interface')->find_by_module_name('map');

        if (defined $if) {
            $c->stash->{ map_layer_settings } = $if->module_object->get_ol_settings(undef, $if);
        }
    }

    $c->forward('dialog_bewerken_base');
}

sub objecttype_bewerken : Chained('milestones_base') : PathPart('objecttype') : Args() {
    my ($self, $c) = @_;
    my $params = $c->req->params;

    $c->stash->{zaaktypen_tmp_store_element}    = 'kenmerken';
    $c->stash->{objecttype} = $c->model('Object')->retrieve(uuid => $c->req->params->{edit_id});

    $c->stash->{template}   = 'beheer/zaaktypen/milestones/edit_objecttype.tt';
    $c->forward('dialog_bewerken_base');
}

sub text_block_bewerken : Chained('milestones_base'): PathPart('text_block'): Args() {
    my ($self, $c) = @_;

    $c->stash->{zaaktypen_tmp_store}            = 'milestones';
    $c->stash->{zaaktypen_tmp_store_element}    = 'kenmerken';

    $c->stash->{template}   = 'beheer/zaaktypen/milestones/edit_text_block.tt';

    $c->forward('dialog_bewerken_base');
}

sub kenmerkgroup_bewerken : Chained('milestones_base'): PathPart('kenmerkgroup'): Args() {
    my ($self, $c) = @_;

    $c->stash->{zaaktypen_tmp_store}            = 'milestones';
    $c->stash->{zaaktypen_tmp_store_element}    = 'kenmerken';

    $c->stash->{template}   = 'beheer/zaaktypen/milestones/edit_kenmerk_group.tt';

    $c->forward('dialog_bewerken_base');
}

sub regelgroup_bewerken : Chained('milestones_base'): PathPart('regelgroup'): Args() {
    my ($self, $c) = @_;

    $c->stash->{zaaktypen_tmp_store}            = 'milestones';
    $c->stash->{zaaktypen_tmp_store_element}    = 'regels';

    $c->stash->{template}   = 'beheer/zaaktypen/milestones/edit_regel_group.tt';

    $c->forward('dialog_bewerken_base');
}


sub sjabloon_bewerken : Chained('milestones_base'): PathPart('sjabloon'): Args() {
    my ($self, $c) = @_;

    $c->stash->{zaaktypen_tmp_store}            = 'milestones';
    $c->stash->{zaaktypen_tmp_store_element}    = 'sjablonen';

    $c->stash->{template}   = 'beheer/zaaktypen/milestones/edit_sjabloon.tt';

    $c->forward('dialog_bewerken_base');
}


sub checklist_bewerken : Chained('milestones_base'): PathPart('checklist'): Args() {
    my ($self, $c) = @_;

    $c->stash->{zaaktypen_tmp_store}            = 'milestones';
    $c->stash->{zaaktypen_tmp_store_element}    = 'checklists';

    $c->stash->{template}   = 'beheer/zaaktypen/milestones/edit_checklist.tt';

    $c->forward('dialog_bewerken_base');
}

sub _get_user_roles {
    my ($self, $c) = @_;
    $c->stash->{rollen} = $c->get_user_roles;
}

sub notificatie_bewerken : Chained('milestones_base'): PathPart('notificatie'): Args() {
    my ($self, $c) = @_;

    my $params = $c->req->params();

    $c->stash->{zaaktypen_tmp_store}            = 'milestones';
    $c->stash->{zaaktypen_tmp_store_element}    = 'notificaties';

    $self->_get_user_roles($c);

    $c->stash->{template}   = 'beheer/zaaktypen/milestones/edit_notificatie.tt';

    $c->forward('dialog_bewerken_base');
}

sub betrokkene_bewerken : Chained('milestones_base'): PathPart('betrokkene'): Args() {
    my ($self, $c) = @_;

    my $params = $c->req->params();

    $c->stash->{zaaktypen_tmp_store}            = 'milestones';
    $c->stash->{zaaktypen_tmp_store_element}    = 'standaard_betrokkenen';

    $self->_get_user_roles($c);

    $c->stash->{template}   = 'beheer/zaaktypen/milestones/edit_betrokkene.tt';

    $c->forward('dialog_bewerken_base');
}



sub resultaat_bewerken : Chained('milestones_base'): PathPart('resultaat'): Args() {
    my ($self, $c) = @_;

    my $params = $c->req->params();

    $c->stash->{zaaktypen_tmp_store}            = 'milestones';
    $c->stash->{zaaktypen_tmp_store_element}    = 'resultaten';

    $c->stash->{template}   = 'beheer/zaaktypen/milestones/edit_resultaat.tt';

    $c->forward('dialog_bewerken_base');
}


sub regel_bewerken : Chained('milestones_base'): PathPart('regel'): Args() {
    my ($self, $c) = @_;

    $c->stash->{zaaktypen_tmp_store}            = 'milestones';
    $c->stash->{zaaktypen_tmp_store_element}    = 'regels';


    my $params = $c->req->params();

    my $qr = qr/^regels_voorwaarde_\d+_kenmerk$/;
    if(first { $_ =~ /$qr/ && $params->{$_} eq 'case_result' } keys %$params) {
        $c->stash->{is_regel_type} = 1;
    }

    if($params->{'add_voorwaarde'}) {
        my $voorwaarden = $params->{'regels_voorwaarden'};
        $voorwaarden ||= '1';
        unless(ref $voorwaarden && ref $voorwaarden eq 'ARRAY') {
            $voorwaarden = [$voorwaarden];
        }
        # find a free slot
        my $lookup = {map {$_ => 1} @$voorwaarden};
        my $i = 0;
        while($lookup->{++$i}) {}
        $c->stash->{new_voorwaarde} = "$i";
    } elsif($params->{'add_actie'}) {
        my $acties = $params->{'regels_acties'};
        $acties ||= '1';
        unless(ref $acties && ref $acties eq 'ARRAY') {
            $acties = [$acties];
        }
        # find a free slot
        my $lookup = {map {$_ => 1} @$acties};
        my $i = 0;
        while($lookup->{++$i}) {}
        $c->stash->{new_actie} = "$i";
    } elsif($params->{'add_anders'}) {
        my $anders = $params->{'regels_anders'};
        $anders ||= '1';
        unless(ref $anders && ref $anders eq 'ARRAY') {
            $anders = [$anders];
        }
        # find a free slot
        my $lookup = {map {$_ => 1} @$anders};
        my $i = 0;
        while($lookup->{++$i}) {}
        $c->stash->{new_anders} = "$i";
    }

    # provide last_milestone
    my $zaaktype_id = $c->stash->{zaaktype_id};
    my $statussen = $c->session->{zaaktypen}->{$zaaktype_id}->{statussen};
    my ($last_milestone_id) = sort { $b <=> $a } keys %$statussen;
    $c->stash->{last_milestone} = $last_milestone_id;
    $c->stash->{zaaktype_resultaten} = $statussen->{$last_milestone_id}->{elementen}->{resultaten};


    $c->stash->{params} = $c->req->params();

    # the checkbox filters need the structure with active fields now
    $c->stash->{add_active_structure} = sub {
        my $values = shift;
        return [] unless $values && @$values;

        # only add the active structure if it isn't there already
        return [map { ref $_ ? $_ : {value => $_, active => 1} } @$values];
    };

    my $rs = $c->model('DB::Interface');

    my @interfaces = qw[
        buitenbeter
        mijnoverheid
        stuf_zkn_client
        zorginstituut
        key2burgerzakenverhuizing
    ];

    $c->stash->{ ext_interfaces } = {
        map { $_->id => $_ } $rs->search_active({ module => \@interfaces })->all
    };

    $c->stash->{ api_interfaces } = {
        map { $_->id => $_->name }
        grep { $_->jpath('$.is_trigger') } $rs->search_active({ module => 'api' })->all
    };

    $c->stash->{template}   = 'beheer/zaaktypen/milestones/edit_regel.tt';

    $c->forward('dialog_bewerken_base');
}

sub relatie_bewerken : Chained('milestones_base'): PathPart('relatie'): Args() {
    my ($self, $c) = @_;

    $c->stash->{zaaktypen_tmp_store}            = 'milestones';
    $c->stash->{zaaktypen_tmp_store_element}    = 'relaties';

    $self->_get_user_roles($c);
    $c->stash->{template}   = 'beheer/zaaktypen/milestones/edit_relatie.tt';

    $c->forward('dialog_bewerken_base');
}



sub overzicht_milestones : Chained('milestones_base'): PathPart('overzicht') : Args() {
    my ($self, $c) = @_;

    $c->stash->{template} =
        'beheer/zaaktypen/milestones/overzicht_milestones.tt';
}



Params::Profile->register_profile(
    method  => '_session_zaaktype',
    profile => {
        required => [ qw/zaaktype_id/ ],
    }
);

sub _session_zaaktype {
    my ($self, $c, $params) = @_;

    my $dv = Params::Profile->check(params => $params);
    die "invalid options for _session_zaaktype" unless $dv->success;

    my $zaaktype_id = $params->{zaaktype_id};
    return $c->session->{zaaktypen}->{$zaaktype_id} ||= {};
}


sub GET : JSON : Chained('base'): PathPart('GET') {
    my ($self, $c) = @_;

    my $zaaktype_id = $c->stash->{zaaktype_id};

    $c->stash->{json} = $c->session->{zaaktypen}->{$zaaktype_id} || {};

    $c->forward('Zaaksysteem::View::JSON');
}

sub POST : JSON : Chained('base'): PathPart('POST') {
    my ($self, $c) = @_;

    my $params = $c->req->params;

    my $zaaktype_id = $c->stash->{zaaktype_id};

    my $zaaktype = $c->session->{zaaktypen}->{$zaaktype_id} = {};
    $zaaktype->{definitie}->{pdc_tarief} = $params->{pdc_tarief};

    $c->forward('Zaaksysteem::View::JSON');
}

sub case_action_checkboxes : Private {
    my ($self, $c) = @_;

    my $checkboxes = $c->model('Zaaktypen')->casetype_actions;

    # this works around some TT/Zaaksysteem legacy, could be avoided altogether by just
    # using a different interface to get info to the browser.
    # specifically talking to the params hash that is used to populate the html
    foreach my $checkbox (@$checkboxes) {
        next if $checkbox->{ head };

        my @path = split /\./, $checkbox->{field};
        my $field = pop @path;

        my $pointer = $c->stash->{params};
        map { $pointer = $pointer->{$_} } @path;
        $checkbox->{value} = $pointer->{$field};
    }
    $c->stash->{case_action_checkboxes} = $checkboxes;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 CATEGORIES_DB

TODO: Fix the POD

=cut

=head2 GET

TODO: Fix the POD

=cut

=head2 LOGGING_COMPONENT_ZAAKTYPE

TODO: Fix the POD

=cut

=head2 POST

TODO: Fix the POD

=cut

=head2 ZAAKTYPEN

TODO: Fix the POD

=cut

=head2 ZAAKTYPEN_MODEL

TODO: Fix the POD

=cut

=head2 acties

TODO: Fix the POD

=cut

=head2 algemeen

TODO: Fix the POD

=cut

=head2 auth

TODO: Fix the POD

=cut

=head2 base

TODO: Fix the POD

=cut

=head2 case_action_checkboxes

TODO: Fix the POD

=cut

=head2 checklist_bewerken

TODO: Fix the POD

=cut

=head2 children

TODO: Fix the POD

=cut

=head2 children_titles

TODO: Fix the POD

=cut

=head2 confirm_finish

TODO: Fix the POD

=cut

=head2 dialog_bewerken_base

TODO: Fix the POD

=cut

=head2 finish

TODO: Fix the POD

=cut

=head2 flush

TODO: Fix the POD

=cut

=head2 generate_steps

TODO: Fix the POD

=cut

=head2 get_steps

TODO: Fix the POD

=cut

=head2 kenmerk_bewerken

TODO: Fix the POD

=cut

=head2 kenmerkgroup_bewerken

TODO: Fix the POD

=cut

=head2 merge_stash_into_session

TODO: Fix the POD

=cut

=head2 milestone_definitie

TODO: Fix the POD

=cut

=head2 milestones

TODO: Fix the POD

=cut

=head2 milestones_base

TODO: Fix the POD

=cut

=head2 milestones_start

TODO: Fix the POD

=cut

=head2 notificatie_bewerken

TODO: Fix the POD

=cut

=head2 objecttype_bewerken

TODO: Fix the POD

=cut

=head2 overzicht_milestones

TODO: Fix the POD

=cut

=head2 publish

TODO: Fix the POD

=cut

=head2 publish_single

TODO: Fix the POD

=cut

=head2 regel_bewerken

TODO: Fix the POD

=cut

=head2 regelgroup_bewerken

TODO: Fix the POD

=cut

=head2 relatie_bewerken

TODO: Fix the POD

=cut

=head2 relaties

TODO: Fix the POD

=cut

=head2 resultaat_bewerken

TODO: Fix the POD

=cut

=head2 sjabloon_bewerken

TODO: Fix the POD

=cut

=head2 zaaktypen_bewerken

TODO: Fix the POD

=cut

=head2 zaaktypen_clone

TODO: Fix the POD

=cut

=head2 zaaktypen_flush

TODO: Fix the POD

=cut

=head2 zaaktypen_start

TODO: Fix the POD

=cut

=head2 zaaktypen_verwijder

TODO: Fix the POD

=cut

=head2 zaaktypen_view

TODO: Fix the POD

=head2 clean_empty_rows

TODO: Fix the POD

=cut

