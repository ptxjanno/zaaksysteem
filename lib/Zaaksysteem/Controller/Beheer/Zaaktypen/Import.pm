package Zaaksysteem::Controller::Beheer::Zaaktypen::Import;
use Moose;

use namespace::autoclean;

use Archive::Extract;
use Clone qw(clone);
use Encode;
use File::Spec::Functions;
use File::Temp;
use Hash::Merge::Simple qw( clone_merge );
use XML::Dumper;
use XML::Simple;
use Zaaksysteem::Constants;
use BTTW::Tools;

BEGIN { extends 'Zaaksysteem::Controller' }

use constant ZAAKTYPEN              => 'zaaktypen';
use constant ZAAKTYPEN_MODEL        => 'DB::Zaaktype';
use constant CATEGORIES_DB          => 'DB::BibliotheekCategorie';

has importer => (
    'is'    => 'rw',
);

sub base : Chained('/') : PathPart('beheer/zaaktypen/import') {
    my ( $self, $c ) = @_;

    my $params = $c->req->params();
    if($params->{upload}) {
        $c->detach("upload");
    }

    my $importer = $self->_importer($c);

    my $zaaktype = $importer->imported_zaaktype;

    if($zaaktype) {
        eval {
            $self->_session($c)->{problems} = [];
            $importer->check_dependencies();

            $c->stash->{import_problems}        = $importer->problems;
            $c->stash->{zaaktype}               = $zaaktype;
            $c->stash->{import_dependencies}    = $self->_session($c)->{import_dependencies};
            $c->stash->{dependency_config}      = ZAAKTYPE_DEPENDENCIES;

        };
        if($@) {
            $c->log->error("import error: " .$@);
            $c->stash->{dependency_error} = $@;
        }
    }

    $c->stash->{error} = $self->_session($c)->{'zaaktype_import_error'};

    $c->stash->{only_import_active_fieldvalues} = $self->_session($c)->{only_import_active_fieldvalues};
    $c->stash->{template} = 'beheer/zaaktypen/import.tt';
    if($params->{import} && @{$importer->problems} == 0) {
        try {
            $c->stash->{template} = 'beheer/zaaktypen/import/finish.tt';

            $c->stash->{zaaktype_node} = $importer->import_zaaktype($c->model('Zaaktypen'));

            my $category = $c->stash->{zaaktype_node}->zaaktype_id->bibliotheek_categorie_id;

            $c->stash->{category_name} = $category ? $category->naam : "de Zaaksysteem catalogus";

            if ($c->session->{import_return_url}) {
                my $category_uuid = $category ? $category->uuid : '';
                ($c->stash->{return_url} = $c->session->{import_return_url}) =~ s/\{\}/$category_uuid/g;

                $c->log->debug(sprintf("Found import_return_url = %s; final return_url = %s", $c->session->{import_return_url}, $c->stash->{return_url}));
            }
            else {
                $c->stash->{return_url} = $c->uri_for("/beheer/bibliotheek/" . $category->id);
            }

            $self->_session($c, 'flush');
        }
        catch {
            $c->stash->{import_error} = $_;
        }
    }
}


sub settings : Chained('/') : PathPart('beheer/zaaktypen/import/settings') {
    my ($self, $c) = @_;

    my $params = $c->req->params();
    my $importer = $self->_importer($c);

    map { $self->_session($c)->{$_} = $params->{$_} } qw/only_import_active_fieldvalues/;

    $c->response->body('OK');
    $c->detach;
}


=head2 upload

Receive an uploaded .ztb file. Unzip this file in a temporary directory and store the
zip information in the session.

For IE, the uploaded file is send to this controller, for spanky browsers we receive
a UUID with which a Filestore obj can be obtained.

=cut

sub upload : Chained('/') : PathPart('beheer/zaaktypen/import/upload') {
    my ( $self, $c ) = @_;

    my $filestore_row;
    try {
        $self->_session($c, 'flush');

        my $params = $c->req->params();

        my $uuid = $params->{filestore_uuid};
        unless($uuid) {
            # IE
            $c->forward('/upload/index');

            $uuid = $c->stash->{uuid};
        }

        throw("import/filestore_uuid/missing", "UUID missing") unless $uuid;

        $filestore_row = $c->model('DB::Filestore')->find_by_uuid($uuid);
        if (!$filestore_row) {
            throw("import/filestore_uuid/not_found", "Unable to find UUID $uuid");
        }

        unless($filestore_row->original_name =~ m|\.ztb$|) {
            throw("import/extension/invalid", "Not a .ztb file");
        }

        $self->_session($c)->{'import_filename'} = $filestore_row->original_name;
        $self->_session($c)->{'import_filestore_id'} = $filestore_row->id;

        my $importer = $self->_importer($c);

        my $archive = $importer->extract_import_file($filestore_row);
        $self->_session($c)->{upload} = $archive;

        my $zaaktype_xml_file = catfile($importer->filepath, 'zaaktype.xml');

        my $xml_doc = XML::LibXML->load_xml(location => $zaaktype_xml_file);
        $importer->filter_timestamp_xml($xml_doc);

        my $zaaktype = XML::Dumper::xml2pl($xml_doc->toString);
        $zaaktype->{filename} = $filestore_row->original_name;

        $importer->decode_notificatie_newlines($zaaktype);
        $importer->remove_obsolete_parts($zaaktype);
        $importer->unwrap_timestamps($zaaktype);

        $importer->imported_zaaktype($zaaktype);
    }
    catch {
        $c->res->redirect($c->uri_for('/beheer/object/import/0'));
        $c->log->warn("Ongeldig zaaktype import bestand: $_");
        $c->push_flash_message('Ongeldig Zaaktype importbestand');
        $c->detach;
    };

    $c->res->redirect($c->uri_for('/beheer/zaaktypen/import'));
    $c->detach();
}

sub flush : Chained('/') : PathPart('beheer/zaaktypen/import/flush') {
    my ( $self, $c ) = @_;

    $self->_session($c, 'flush');
    $c->res->redirect($c->uri_for('/beheer/zaaktypen/import'));
}

# show adjustment form
#
sub adjust : Chained('/') : PathPart('beheer/zaaktypen/import/adjust') {
    my ( $self, $c ) = @_;

    my $params          = $c->req->params();
    my $id              = $c->stash->{id}               = $params->{id};
    my $dependency_type = $c->stash->{dependency_type}  = $params->{dependency_type};

    my $importer = $self->_importer($c);

    $c->stash->{options}         = $importer->dependency_options($dependency_type, {remote_id => $id});
    $c->stash->{dependency_item} = $importer->dependency_item({dependency_type => $dependency_type, id => $id});

    $c->stash->{bib_cat} = $c->model(CATEGORIES_DB)->search({
        'system'    => [ undef, 0 ],
        'pid'       => undef,
    }, {
        order_by    => ['pid','naam']
    });

    if ($dependency_type eq 'BibliotheekKenmerken') {
        $self->determine_field_options($c, $importer, $id);
    }

    $c->stash->{only_import_active_fieldvalues} = $self->_session($c)->{only_import_active_fieldvalues};
    $c->stash->{dependency_config} = ZAAKTYPE_DEPENDENCIES;
    $c->stash->{nowrapper}         = 1;
    $c->stash->{template}          = 'beheer/zaaktypen/import/adjust.tt';
}


sub determine_field_options {
    my ($self, $c, $importer, $id) = @_;

    my $solution = $c->stash->{dependency_item}->{solution};

    $c->stash->{local_field_options} = $c->model('DB::BibliotheekKenmerkenValues')->search({
        bibliotheek_kenmerken_id => $solution->{id}
    }, {
        order_by => 'sort_order'
    });

    my $remote_record = $importer->lookup_remote_record('BibliotheekKenmerken', $id);

    if ($solution->{id}) {
        $c->stash->{remote_field_options} = $importer->determine_field_options($remote_record, $solution->{id});
    }
}


sub validate : Chained('/') : PathPart('beheer/zaaktypen/import/validate') {
    my ( $self, $c ) = @_;

    my $params          = $c->req->params();
    my $new_name        = $params->{new_name};
    my $dependency_type = $params->{dependency_type};

    my $importer        = $self->_importer($c);

    $new_name ||= $importer->dependency_item({
        dependency_type => $dependency_type,
        id              => $params->{id}
    })->{name};

    my $option = $importer->dependency_options($dependency_type, {name => $new_name});

    my $json = {
        success => 1
    };
    if($option) {
        $json->{success} = 0;
        $json->{error} = 'Geef een andere naam';
    }

    # validation for category items
    my $dependency_config = ZAAKTYPE_DEPENDENCIES->{$dependency_type};
    if($dependency_config->{has_category}) {
        unless($params->{bibliotheek_categorie_id}) {
            $json->{success} = 0;
            $json->{categorie_error} = 'Geef een categorie';
        }
        # child already exists
        my $child_count = $c->model(CATEGORIES_DB)->search({
            pid     => $params->{bibliotheek_categorie_id} || 0,
            naam    => $params->{sub_categorie},
        })->count();
        if($child_count) {
            $json->{success} = 0;
            $json->{sub_categorie_error} = 'Subcategorie bestaat al';
        }
    }

    $c->stash->{json} = $json;
    $c->forward('Zaaksysteem::View::JSONlegacy');
}


sub approve : Chained('/') : PathPart('beheer/zaaktypen/import/approve') {
    my ( $self, $c ) = @_;

    my $params          = $c->req->params();

    my $id              = $c->stash->{id}               = $params->{id};
    my $dependency_type = $c->stash->{dependency_type}  = $params->{dependency_type};
    my $action          = $params->{action};

    my $dependency_config = ZAAKTYPE_DEPENDENCIES->{$dependency_type};
    my $importer = $self->_importer($c);

    my $dependency_item = $importer->dependency_item({dependency_type => $dependency_type, id =>$id});
    my $solution = $dependency_item->{solution} ||= {};
    $solution->{action} = $action;

    if($action eq 'add') {
        # validate the new name. if it exists, show an error and reshow the adjustment page
        $solution->{name} = $params->{new_name} || $dependency_item->{name};

        # Don't delete the ID for standaard betrokkene, we need it
        # elsewhere.
        delete $solution->{id} unless $dependency_type eq 'ZaaktypeStandaardBetrokkenen';


        if($dependency_config->{has_category}) {
            my $remote_record = $importer->lookup_remote_record($dependency_type, $id);

            my $bibliotheek_categorie_id = $remote_record->{bibliotheek_categorie_id};

            if($bibliotheek_categorie_id) {
                 my $categorie_dependency_item = $importer->dependency_item({
                     dependency_type => 'BibliotheekCategorie',
                     id              =>  $bibliotheek_categorie_id,
                 });

                 $categorie_dependency_item->solution({
                     action  => 'use_existing',
                     id      => $params->{bibliotheek_categorie_id},
                 });

                $solution->{bibliotheek_categorie_id} = $params->{bibliotheek_categorie_id};
            }
        }

        if($params->{multi_cat} && $dependency_config->{has_category}) {
            # look for other elements in the same group that also need to be placed in a category
            my $dependency_type_items = $self->_session($c)->{import_dependencies}->{$dependency_type};

            foreach my $other_id (keys %$dependency_type_items) {

                my $dependency = $importer->dependency_item({
                    dependency_type => $dependency_type,
                    id              => $other_id
                });
                my $solution = $dependency->solution;
                unless($solution && %$solution) {


                    $dependency->bibliotheek_categorie_id(
                        $params->{bibliotheek_categorie_id}
                    );
                }
            }
        }

        if ($dependency_type eq 'ZaaktypeStandaardBetrokkenen') {
            $dependency_item->{solution}->{changed} = 1;
            $dependency_item->{solution}->{betrokkene_id} = $params->{betrokkene_id};
            $dependency_item->{solution}->{naam} = $params->{betrokkene_naam};
        }
        elsif ($solution->{name} ne $dependency_item->{name} || $params->{bibliotheek_categorie_id}) {
            $dependency_item->{solution}->{changed} = 1;
            if($dependency_type eq 'BibliotheekKenmerken') {
                $dependency_item->{solution}->{magic_string} = $c->model('DB::BibliotheekKenmerken')->
                    generate_magic_string(
                        $solution->{name}
                    );
            }
        }


    } elsif($action eq 'use_existing') {
        $solution->{id} = $params->{new_id};

        my $option = $importer->dependency_options($dependency_type, {id => $solution->{id}});

        $solution->{name} = $option->{name};

        if ($dependency_type eq 'BibliotheekKenmerken') {
            my $remote_record = $importer->lookup_remote_record('BibliotheekKenmerken', $id);

            # this regexp should match with the one in Component::BibliotheekKenmerken
            if ($remote_record->{value_type} =~ m/^(option|checkbox|select)$/) {

                my $row = $c->model('DB::BibliotheekKenmerken')->find($solution->{id});

                if ($row->uses_options) {
                    $solution->{use_remote_field_options} = $params->{use_remote_field_options};
                } else {
                    delete $solution->{use_remote_field_options};
                }
            } else {
                delete $solution->{use_remote_field_options};
            }
        }

        $dependency_item->{solution}->{changed} = 1;
    } elsif($action eq 'revert') {
        delete $dependency_item->{solution};
    } else {
        die "incorrect action $action";
    }


    $c->stash->{nowrapper} = 1;

    $importer->check_dependencies();
    $c->stash->{dependency_config} = ZAAKTYPE_DEPENDENCIES;
    $c->stash->{dependency_item}   = $dependency_item;
    $c->stash->{template}          = 'beheer/zaaktypen/import/item.tt';
}


sub importall : Chained('/') : PathPart('beheer/zaaktypen/import/bulkimport') {
    my ($self, $c) = @_;

    my $importer = $self->_importer($c);
    my $tmp_dir = Temp::File->newdir();

    my $upload = $c->request->upload('filename');
    die('No file submitted') if !$upload;
    $upload->fh;
    my $a = $upload->tempname;

    my $res = $importer->import_from_ztb_tarball($a);
    foreach (keys %$res) {
        if ($res->{$_}{error}) {
            $c->log->error("Fout opgetreden bij het importeren van $_: $res->{$_}{error}");
            $c->stash->{import_error} = $res->{$_}{error};
        }
        else {
            $c->log->info("Zaaktype bestand $_ is succesvol geimporteerd");
        }
    }
    $c->stash->{template} = 'beheer/zaaktypen/import/finish.tt';

};


# ---------------------------- only friends can see private parts ---------------------- #



sub _importer {
    my ($self, $c) = @_;

    if ($c->stash->{importer}) {
        return $c->stash->{importer};
    }

    my $importer = $c->model('Zaaktypen::Import');
    $importer->initialize({
        groups      => $c->model('DB::Groups'),
        session     => $self->_session($c)
    });

    $c->stash->{importer} = $importer;
    return $c->stash->{importer};
}



sub _session : Private {
    my ($self, $c, $flush) = @_;

    die "need c" unless($c);

    if ($flush) {
        if (exists $c->stash->{zaaktype_node}) {
            my $zaaktype_id = $c->stash->{zaaktype_node}->get_column('zaaktype_id');
            delete $c->session->{zaaktypen}{ $zaaktype_id };
        }
        return $c->session->{zaaktype_import} = {};
    }
    return $c->session->{zaaktype_import} ||= {};
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 CATEGORIES_DB

TODO: Fix the POD

=cut

=head2 ZAAKTYPEN

TODO: Fix the POD

=cut

=head2 ZAAKTYPEN_MODEL

TODO: Fix the POD

=cut

=head2 ZAAKTYPE_DEPENDENCIES

TODO: Fix the POD

=cut

=head2 adjust

TODO: Fix the POD

=cut

=head2 approve

TODO: Fix the POD

=cut

=head2 base

TODO: Fix the POD

=cut

=head2 determine_field_options

TODO: Fix the POD

=cut

=head2 flush

TODO: Fix the POD

=cut

=head2 importall

TODO: Fix the POD

=cut

=head2 settings

TODO: Fix the POD

=cut

=head2 validate

TODO: Fix the POD

=cut

