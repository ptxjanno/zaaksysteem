package Zaaksysteem::General::Authentication;

use strict;
use warnings;

=head1 NAME

Zaaksysteem::General::Authentication - Helper methods for user authentication
and authorization checks.

=head1 DESCRIPTION

This package is applied on the L<Zaaksysteem> instance by L<Catalyst> via
L<Zaaksysteem::General>.

Most methods here are in the process of being rewritten and exist mostly to
keep existing codepaths functional.

=cut

use BTTW::Tools qw(throw);
use List::Util qw[any];
use Scalar::Util qw/blessed/;

use Zaaksysteem::Constants qw/
    ZAAKSYSTEEM_AUTHORIZATION_ROLES
    ZAAKSYSTEEM_AUTHORIZATION_PERMISSIONS
    ZAAKSYSTEEM_CONSTANTS
/;

use Zaaksysteem::Constants::Users qw(:all);

=head1 METHODS

=head2 assert_user

Asserts if the user is a C<PIP>, C<API> or C<REGULAR> user. You can
import these values from L<Zaaksysteem::Constants::Users>

    $c->assert_user(); # Defaults to a REGULAR user

    $c->assert_user(PIP) # user must be a PIP user

    $c->assert_user(PIP|REGULAR) user must be PIP or REGULAR

=cut

sub assert_user {
    my ($c, $scope) = @_;

    unless ($scope) {
        $scope = REGULAR;
    }

    return if $c->check_user_mask($scope);

    throw(
        'zaaksysteem/unauthorized_user',
        'Unauthorized: authentication required',
        { http_code => 401 }
    );
}

=head2 check_user_mask

Checks if the user, if existant and logged in, has specific authorization
properties.

    if ($c->check_user_mask(API)) {
        # User is an API delegate
    }

    # Bail out unless user is any kind of admin
    return unless $c->check_user_mask(ADMIN);

=cut

sub check_user_mask {
    my ($c, $scope) = @_;

    return unless defined $scope;

    # Ensure proper bit context
    $scope |= 0;

    if ($scope & FORM) {
        return 1 if !$c->user_exists && $c->session->{form};
    }

    if ($scope & PIP) {
        return 1 if !$c->user_exists && $c->session->{pip};
    }

    if ($scope & API) {
        return 1 if $c->user_exists && $c->user->is_external_api;
    }

    if ($scope & REGULAR) {
        return 1 if $c->user_exists && !$c->user->is_external_api;
    }

    if ($scope & ADMIN) {
        return 1 if $c->user_exists && $c->check_any_user_permission('admin');
    }

    return;
}

### XXX BELOW IS NEW STYLE

=head2 check_any_user_permission

Given a list of permissions, find out if the user
has at least one of these, hence 'any'

Implies 'admin' as a given permission to check, since users with that role may
perform any action.

Implementation details of the check can be found in
L<Zaaksysteem::Backend::Subject::Component/has_legacy_permission>.

=cut

sub check_any_user_permission {
    my ($c, @check_permissions) = @_;

    return unless $c->user_exists;

    return any {
        $c->user->has_legacy_permission($_)
    } ('admin', @check_permissions);
}

=head2 assert_any_user_permission

Assertive variant of L</check_any_user_permission>. If that method returns
false-ish, a debug message will be appended to the log and the request will be
redirected to C</forbidden>.

=cut

sub assert_any_user_permission {
    my ($c, @permissions) = @_;

    return 1 if $c->check_any_user_permission(@permissions);

    $c->log->debug("assert_any_user_permission: denied");

    $c->detach('/forbidden');
}

=head2 check_any_zaak_permission

cached version, to lessen traffic to LDAP server.

alternative caching strategy is to get all permissions and then consult
the cached lookup. the penalty is for smaller json requests that only call
this function once.

=cut

sub check_any_zaak_permission {
    my ($c, @check_permissions) = @_;

    return $c->check_any_given_zaak_permission($c->stash->{zaak}, @check_permissions);
}

=head2 check_any_given_zaak_permission

need to be able to call this from lists of cases. so per case the whole has to
executed.

the historic function signature makes it a bit hard to overload, we could look
if @_[1] is a hashref or something but i rather fix it with a different function
name - much less debugging nightmares.

'given' - indicates the zaak is specifically passed to the function instead
being taken from the stash, so it refers to the case rather than to the permission.
anybody got a catchier name?

=cut

sub check_any_given_zaak_permission {
    my ($c, $zaak, @check_permissions) = @_;

    # stash is cleared after each request
    my $cache_key = '_cache_check_any_zaak_permission - zaak: ' . $zaak->id . ' - ' . join ",", sort @check_permissions;

    return $c->stash->{$cache_key} if exists $c->stash->{$cache_key};
    return $c->stash->{$cache_key} = $c->_retrieve_any_zaak_permission($zaak, @check_permissions);
}

=head2 retrieve_list_of_zaak_permissions

Will return a list of permissions for the given user

=cut

sub retrieve_list_of_zaak_permissions {
    my ($c, $zaak) = @_;
    my %rv;

    my $map = {
        admin   => {
            zaak_read   => 1,
            zaak_edit   => 1,
            zaak_beheer => 1,
            zaak_search => 1,

            case_allocate => 1,
        },
        edit   => {
            zaak_read   => 1,
            zaak_edit   => 1,
            zaak_search => 1,

            case_allocate => 1,
        },
        read   => {
            zaak_read   => 1,
            zaak_search => 1,
        },
        search   => {
            zaak_search => 1,
        },
        allocate => {
            case_allocate => 1,
        }
    };

    return [] unless ($zaak && $c->user_exists);

    ### First, check if user is allowed to view zaken
    unless ($c->check_any_user_permission('gebruiker')) {
        $c->push_flash_message('Geen toegang: u heeft geen rechten om zaken te bekijken [!gebruiker]');
        return [];
    }

    return [ keys %{ $map->{admin} } ] if $c->check_any_user_permission('admin');

    my $subject = $c->user or return;

    if ($zaak->ddd_user_is_staff($subject)) {
        %rv = (%rv, %{ $map->{read} });
    }

    if ($zaak->ddd_user_is_assignee($subject)) {
        %rv = (%rv, %{ $map->{edit} });
    }

    if ($zaak->ddd_user_is_coordinator($subject)) {
        %rv = (%rv, %{ $map->{edit} });
    }

    my $authorisations  = $c->get_cached_zaaktype_authorisations($zaak->get_column('zaaktype_id'));

    my $user_role_ids   = [ map { $_->id } @{ $c->user->roles } ];

    my $parent_ou_ids   = [ map { $_->id } @{ $c->user->primary_groups }, @{ $c->user->inherited_groups } ];

    foreach my $authorisation (@$authorisations) {

        # make distinction for authorisations for confidential cases
        next unless ($zaak->confidentiality eq 'confidential') == $authorisation->{confidential};

        # if any of the parent_ou_ids (which includes mine) has rights,
        # i'm able to access this.
        if ($authorisation->{ou_id} && !grep {$authorisation->{ou_id} eq $_ } @$parent_ou_ids) {
            next;
        }

        # if this is an authorisation that applies to us,
        # then see if it is a right that we were wondering about.
        my $role_permission = grep { $authorisation->{role_id} eq $_ } @$user_role_ids;

        $rv{ $authorisation->{recht} } = 1 if ($role_permission);
    }

    my %permission_map = (
        'read'      => 'zaak_read',
        'search'    => 'zaak_search',
        'write'     => 'zaak_edit',
        'manage'    => 'zaak_beheer',
    );

    ### Case authorisations
    my $custom_case_authorisation = $zaak->zaak_authorisations->search_rs(
        {
            '-or' => [
                {
                    entity_id   => $subject->position_matrix,
                    entity_type => 'position',
                },
                {
                    entity_id   => $subject->username,
                    entity_type => 'user',
                }
            ],
            scope      => 'instance',
        },
        { columns => 'capability', distinct => 1 }
    );
    while (my $acl = $custom_case_authorisation->next) {
        $rv{$permission_map{$acl->capability}} = 1;
    }
    ### Edits only allowed when you are a behandelaar or when behandelaar is set and you are coordinator
    if (
        !$rv{zaak_beheer} && !$zaak->behandelaar && $rv{zaak_edit}
    ) {
        delete($rv{zaak_edit});
        %rv = (%rv, %{ $map->{allocate} });
    }

    return [ keys %rv ];
}

=head2 get_cached_zaaktype_authorisations

Cached (request scope) variant of L</_retrieve_zaaktype_authorisations>.

=cut

sub get_cached_zaaktype_authorisations {
    my ($c, $zaaktype_id) = @_;

    my $cache_key = '_zaaktype_authorisations_cache - zaaktype_id: ' . $zaaktype_id;

    return $c->stash->{$cache_key} ||= $c->_retrieve_zaaktype_authorisations($zaaktype_id);
}

=head2 assert_any_zaak_permission

Assertive variant of L</check_any_zaak_permission>.

If the check fails, appends a debug log message and redirects the request to
C</forbidden>.

=cut

sub assert_any_zaak_permission {
    my ($c, @permissions) = @_;

    return 1 if $c->check_any_zaak_permission(@permissions);

    $c->log->debug("assert_any_zaak_permission: denied");
    $c->forward('/forbidden');
    $c->detach();

    return;
}

=head2 check_field_permission

Check if the user has permission to work this field. algorithm:

- Check case permissions (we have those, otherwise we wouldn't have landed here)
- Check if this kenmerk is guarded by an additional role that we need to have.
- If so, check if we play this role. (do we wear this hat?)

=cut

sub check_field_permission {
    my ($c, $zaaktype_kenmerk) = @_;

    my $required_permissions = $zaaktype_kenmerk->required_permissions_decoded;

    return 1 unless @$required_permissions;
    return 1 if $c->user->has_legacy_permission('admin');

    my $user_roles = $c->user->roles;
    my $parent_groups = $c->user->parent_groups;

    my $all_groups = $c->model('DB::Groups')->get_all_cached($c->stash);
    my $all_roles = $c->model('DB::Roles')->get_all_cached($c->stash);

    # populate this with labels
    foreach my $required (@$required_permissions) {
        # cache in stash so we only have to do it once per request
        # we could do better by doing this in the get_ou_by_id sub
        my $ou_id   = $required->{org_unit_id};
        my $role_id = $required->{role_id};

        next unless $ou_id && $role_id;

        my ($group) = grep { $_->id == $ou_id } @$all_groups;
        my ($role)  = grep { $_->id == $role_id } @$all_roles;

        unless ($group && $role) {
            next;
        }

        $zaaktype_kenmerk->{ou_names}->{$ou_id} = $group->name;
        $zaaktype_kenmerk->{role_names}->{$role_id} = $role->name;
    }

     # any of these will suffice
    foreach my $required (@$required_permissions) {
        return 1 if
            (any { $required->{org_unit_id} eq $_->id } @$parent_groups) &&
            (any { $required->{role_id}     eq $_->id } @$user_roles);
    }

    # explicit return to refuse access
    return;
}

=head2 assert_field_permission

Performs L</check_field_permission> and throws up a "Forbidden" reply if the
check fails.

=cut

sub assert_field_permission {
    my $c = shift;

    return 1 if $c->check_field_permission(@_);

    $c->log->debug("assert_field_permission: denied");
    $c->forward('/forbidden');
    $c->detach();

    return;
}

=head1 PRIVATE METHODS

=head2 _retrieve_any_zaak_permission

Emulation of the original C<retrieve_any_zaak_permission> method. The API is
a stable reflection of that heritage, but the actual logic has been moved to
L<Zaaksysteem::Zaken::Roles::Authentication/check_user_permissions>.

=cut

sub _retrieve_any_zaak_permission {
    my ($c, $zaak, @check_permissions) = @_;

    return unless $zaak && $c->user_exists;

    ### First, check if user is allowed to view zaken
    unless ($c->check_any_user_permission('gebruiker')) {
        $c->push_flash_message('Geen toegang: u heeft geen rechten om zaken te bekijken [!gebruiker]');
        return;
    }

    return $zaak->check_user_permissions($c->user, @check_permissions);
}

sub _retrieve_zaaktype_authorisations {
    my ($c, $zaaktype_id) = @_;

    my $rs = $c->model('DB::ZaaktypeAuthorisation');
    my @authorisations = $rs->search({zaaktype_id => $zaaktype_id});

    return [map { {$_->get_columns} } @authorisations];
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, 2019, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
