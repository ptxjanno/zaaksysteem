package Zaaksysteem::Search::ZQL::Literal::Function;

use Moose;

use Zaaksysteem::Search::Term::Function;

extends 'Zaaksysteem::Search::ZQL::Literal';

has '+value' => ( isa => 'Str' );
has arguments => ( is => 'ro', isa => 'Zaaksysteem::Search::ZQL::Literal::Set', required => 1 );

override new_from_production => sub {
    my $class = shift;

    return $class->new(value => shift, arguments => shift);
};

override dbixify => sub {
    my $self = shift;
    my $cmd = shift;

    return Zaaksysteem::Search::Term::Function->new(
        object_type => $cmd->object_type,
        function => $self->value,
        arguments => $self->arguments->dbixify
    );
};

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

