package Zaaksysteem::Search::ZQL::Command;

use Moose;

=head1 NAME

Zaaksysteem::Search::ZQL::Command - Baseclass for ZQL command parse productions

=head1 DESCRIPTION

=head1 METHODS

=head2 apply_to_resultset

Apply the command to a L<DBIx::Class::ResultSet> instance.

This is a default implementation that returns the resultset unmodified.

=cut

sub apply_to_resultset {
    my $self = shift;
    my $rs = shift;

    return $rs;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

