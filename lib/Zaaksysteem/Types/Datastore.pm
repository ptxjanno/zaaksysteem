package Zaaksysteem::Types::Datastore;
use warnings;
use strict;

use MooseX::Types -declare => [qw(
    DatastoreType
)];

use List::Util 'any';
use MooseX::Types::Moose qw(Str Int Num Bool ArrayRef HashRef Item);
use Zaaksysteem::BR::Subject::Constants ':remote_search_module_names';

=head1 NAME

Zaaksysteem::Types::Datastore - Custom types for Zaaksysteem datastore

=head1 SYNOPSIS

    package MyClass;
    use Moose;
    use Zaaksysteem::Types::Datastore qw(TYPE1 TYPE2);

    has attr => (
        isa => TYPE1,
        is => 'ro',
    );

=head1 AVAILABLE TYPES

=head2 DatastoreTypes

=cut

=head2 CustomerType

A type (subtype of Str) that only allows syntactically valid Customer Types

=over

=item NatuurlijkPersoon

=item Bedrijf

=item BagLigplaats

=item BagNummeraanduiding

=item BagOpenbareruimte

=item BagPand

=item BagStandplaats

=item BagVerblijfsobject

=item BagWoonplaats

=back

=cut

subtype DatastoreType, as enum(
    [
        qw(
            NatuurlijkPersoon
            Bedrijf
            BagLigplaats
            BagNummeraanduiding
            BagOpenbareruimte
            BagPand
            BagStandplaats
            BagVerblijfsobject
            BagWoonplaats
            )
    ]
    ),
    message { "'$_' is not a valid datastore type" };

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

