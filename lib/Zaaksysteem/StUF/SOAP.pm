package Zaaksysteem::StUF::SOAP;

use Moose;

use BTTW::Tools;
use Zaaksysteem::StUF::Parser;

extends qw/Zaaksysteem::SOAP/;

use FindBin qw/$Bin/;

has '+wsdl_file'     => (
    'lazy'      => 1,
    'default'   => sub {
        return shift->home .  '/share/wsdl/stuf/bg0204/bg0204.wsdl';
    }
);

has '+reader_writer_config'     => (
    'lazy'      => 1,
    'default'   => sub {
        my $self                    = shift;
        my $compiler                = Zaaksysteem::StUF::Parser->new(
            home    => $self->home,
        );

        return $compiler->reader_writer_config;
    }
);

has '+xml_definitions'     => (
    'lazy'      => 1,
    'default'   => sub {
        my $self                    = shift;
        my $compiler                = Zaaksysteem::StUF::Parser->new(
            home    => $self->home,
        );

        return $compiler->xml_definitions;
    }
);

has 'home'      => (
    is      => 'rw',
    lazy    => 1,
    default => sub {
        return $Bin . '/..';
    }
);

has 'soap_endpoint'     => (
    'is'        => 'rw',
);

has 'soap_port'         => (
    'is'        => 'rw',
);

has 'soap_service'      => (
    'is'        => 'rw',
);

has 'soap_action'       => (
    'is'        => 'rw',
);

has 'is_test'      => (
    'is'        => 'rw',
);

# around 'dispatch'   => sub {
#     my $orig                = shift;
#     my $self                = shift;

#     my ($answer, $trace)    = $self->$orig(@_);

#     if ($answer) {
#         for my $key (keys %{ $answer }) {
#             next unless UNIVERSAL::isa($answer->{$key}, 'XML::LibXML::Element');

#             my $value = $answer->{$key};

#             ### VICREA PATCH, REMOVE ATTRS
#             {
#                 my $nodes   = $value->getElementsByTagName('PRSIDB');

#                 if ($nodes->size) {
#                     $_->parentNode->removeChild($_) for $nodes->get_nodelist;
#                 }
#             }

#             ### Define the reader
#             my $reader         = $self->wsdl->compile(
#                 READER  => $key
#             );

#             return $reader->($value);
#         }
#     }

#     return ($answer, $trace);
# };

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

