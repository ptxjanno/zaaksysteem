package Zaaksysteem::StUF::0301::Processor::BG0310;
use Moose;
use Moose::Util::TypeConstraints;

use Zaaksysteem::XML::Compile;
use HTML::Entities;
use BTTW::Tools;
use Data::Random::NL qw(generate_bsn);
use Zaaksysteem::Types qw/UUID/;
use Zaaksysteem::Constants qw/RGBZ_GEMEENTECODES/;

use String::Random;

use Zaaksysteem::BR::Subject::Utils;

with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::StUF::0301::Processor::BG0310 - Implementations of STUF-BG 0310 SOAP calls

=head1 DESCRIPTION

StUF-BG is a specification for an API to create and manage BG objects.

=head1 TESTS 

Tests can be found in L<TestFor::General::StUF::0301::Processor::BG0310>

=head1 CONSTANTS

=head2 BG0310_PARAMETER_CONFIGURATION

B<Type>: HashRef

A hashref containing the mapping and configuration for this processor

=cut

use constant BG0310_PARAMETER_CONFIGURATION => {
    'NPS'   => {
        mapping => {
            ### TYPE            => STUF PARAM
            personal_number     => 'inp.bsn',
            personal_number_a   => 'inp.a-nummer',
            family_name         => 'geslachtsnaam',
            prefix              => 'voorvoegselGeslachtsnaam',
            initials            => 'voorletters',
            first_names         => 'voornamen',
            date_of_birth       => 'geboortedatum',
            gender              => 'geslachtsaanduiding',
            noble_title         => 'adellijkeTitelPredikaat',
        },
        address_mapping => {
            street                  => 'gor.straatnaam',
            street_number           => 'aoa.huisnummer',
            street_number_suffix    => 'aoa.huisnummertoevoeging',
            street_number_letter    => 'aoa.huisletter',
            zipcode                 => 'aoa.postcode',
            city                    => 'wpl.woonplaatsNaam',
        },
        partner_mapping => {
            personal_number     => 'inp.bsn',
            personal_number_a   => 'inp.a-nummer',
            family_name         => 'geslachtsnaam',
            prefix              => 'voorvoegselGeslachtsnaam',
        },
        stuf_sort => [qw/
            inp.bsn
            inp.a-nummer
            geslachtsnaam
            voorvoegselGeslachtsnaam
            voorletters
            voornamen
            adellijkeTitelPredikaat
            geslachtsaanduiding
            geboortedatum

            wpl.woonplaatsNaam
            gor.straatnaam
            aoa.postcode
            aoa.huisnummer
            aoa.huisletter
            aoa.huisnummertoevoeging
        /],
        stuf_filters => {
            geboortedatum   => \&date_filter
        },
        search_settings => {
            'inp.bsn'           => {
                exact   => 'true',
            },
            'a-nummer'          => {
                exact   => 'true',
            },
            'voorvoegselGeslachtsnaam'     => {
                exact   => 'false',
            },
            'voorletters'     => {
                exact   => 'false',
            },
            'geslachtsnaam'     => {
                exact   => 'false',
            },
            'a-nummer'          => {
                exact   => 'true',
            },
            'prefix'            => {
                exact   => 'true',
            },
            'geboortedatum'     => {
                exact   => 'true',
                filter  => \&date_filter,
            },
            'geslachtsaanduiding' => {
                exact   => 'true',
            },
            'wpl.woonplaatsNaam' => {
                exact   => 'false',
            },
            'gor.straatnaam'     => {
                exact   => 'false',
            },
            'aoa.postcode'       => {
                exact   => 'true',
                filter  => sub { return uc(shift) },
            },
            'aoa.huisnummer'     => {
                exact   => 'true',
            },
            'aoa.huisletter'     => {
                exact   => 'true',
            },
            'aoa.huisnummertoevoeging'  => {
                exact   => 'false',
            },
        },
        xpath   => {
            personal_number     => '/bg:inp.bsn',
            personal_number_a   => '/bg:inp.a-nummer',
            family_name         => '/bg:geslachtsnaam',
            prefix              => '/bg:voorvoegselGeslachtsnaam',
            initials            => '/bg:voorletters',
            first_names         => '/bg:voornamen',
            noble_title         => '/bg:adellijkeTitelPredikaat',
            use_of_name         => '/bg:aanduidingNaamgebruik',
            gender              => '/bg:geslachtsaanduiding',
            date_of_birth       => '/bg:geboortedatum',
            date_of_death       => '/bg:overlijdensdatum',
            is_secret           => '/bg:inp.indicatieGeheim',
            municipality        => '/bg:inp.gemeenteVanInschrijving'
        },
        partner_xpath   => {
            personal_number     => '/bg:inp.bsn',
            personal_number_a   => '/bg:inp.a-nummer',
            family_name         => '/bg:geslachtsnaam',
            prefix              => '/bg:voorvoegselGeslachtsnaam',
            initials            => '/bg:voorletters',
        },
        address_xpath   => {
            street                  => '/bg:gor.straatnaam',
            street_number           => '/bg:aoa.huisnummer',
            street_number_letter    => '/bg:aoa.huisletter',
            street_number_suffix    => '/bg:aoa.huisnummertoevoeging',
            city                    => '/bg:wpl.woonplaatsNaam',
            zipcode                 => '/bg:aoa.postcode',
        },
        correspondence_address_xpath   => {
            street                  => '/bg:gor.straatnaam',
            street_number           => '/bg:aoa.huisnummer',
            street_number_letter    => '/bg:aoa.huisletter',
            street_number_suffix    => '/bg:aoa.huisnummertoevoeging',
            city                    => '/bg:wpl.woonplaatsNaam',
            zipcode                 => '/bg:postcode',
        },
        foreign_address_xpath   => {
            foreign_address_line1   => '/bg:sub.adresBuitenland1',
            foreign_address_line2   => '/bg:sub.adresBuitenland2',
            foreign_address_line3   => '/bg:sub.adresBuitenland3',
        }
    },
};

=head1 ATTRIBUTES

=head2 xml_backend

The L<Zaaksysteem::XML::Compile> instance to use. Defaults to a new
C<Zaaksysteem::XML::Compile> instance, with the StUF-0301-BG-0310 bits loaded, which
should be fine for most use cases.

=cut

has xml_backend => (
    is => 'ro',
    lazy => 1,
    isa => 'Zaaksysteem::XML::Compile::Backend',
    default => sub {
        my $xml_backend = Zaaksysteem::XML::Compile->xml_compile;
        $xml_backend->add_class('Zaaksysteem::XML::Generator::StUF0301::BG0310');
        return $xml_backend;
    },
);

=head2 schema

L<DBIx::Class> handle to use for accessing the database.

=cut

has schema => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Schema',
    required => 1,
);

=head2 sender

B<Type>: HashRef of sender (or receiver, depending on the context) data (applicatie, organisatie)

=cut

has our_party  => (
    is          => 'rw',
    isa         => 'HashRef',
    required    => 1,
    default     => sub {
        return {
            applicatie  => 'ZSNL',
            organisatie => 'Zaaksysteem',
        };
    },
);

=head2 receiver

B<Type>: HashRef of receiver (or sender, depending on the context) data (applicatie, organisatie)

=cut

has other_party  => (
    is          => 'rw',
    isa         => 'HashRef',
    required    => 1,
    default     => sub {
        return {
            applicatie  => 'DDS',
            organisatie => 'Centric',
        };
    },
);

=head2 reference_id

B<Type>: Num

The reference_id to work with in this session

=cut

has reference_id  => (
    is          => 'rw',
    isa         => 'Num',
    required    => 1,
);

=head2 interface_uuid

B<Type>: UUID

The UUID of the interface this StUF instance is linked to. Mainly used to determine our
own id.

=cut

has interface_uuid  => (
    is          => 'rw',
    isa         => UUID,
);

=head2 other_party_primary_key

B<Type>: ENUM (C<sleutelGegevensbeheer|sleutelVerzendend>)

The other party primary key type

=cut

has other_party_primary_key  => (
    is          => 'rw',
    isa         => subtype('Str' => where { $_ =~ m/^(?:sleutelGegevensbeheer|sleutelVerzendend)$/ }),
    required    => 1,
    default     => sub { 'sleutelGegevensbeheer'; }
);

=head2 municipality_code

B<Type>: Integer

Our local municipality code, to find out wether we are a local resident or not

=cut

has municipality_code  => (
    is          => 'rw',
    isa         => 'Num',
    required    => 1,
);

=head1 GENERATING XML

=head2 import_np

    $processor->import_np(
        {
            subject                     => {
                personal_number             => '985658785',
            },
            internal_identifier         => '83',
            external_identifier         => 587854589,
        },
    );

Imports a 'NPS' entity by sending a "afnemerindicatie" message to the other party.

=cut

define_profile import_np => (
    required    => {
        internal_identifier     => 'Str',
    },
    optional    => {
        external_identifier     => 'Str',
        subject                 => 'HashRef',
    },
    require_some => {
        ext_id_or_subject => [1, qw(external_identifier subject)],
    }
);

sub import_np {
    my $self    = shift;
    my $params  = assert_profile(shift || {})->valid;

    my $values  = '';
    if (!$params->{external_identifier}) {
        my $schema  = $self->_generate_stuf3_schema_from_entity($params->{subject});
        $values     = $self->_gen_stuf3_structure($schema);
    }

    ### Generate XML
    my $xml = $self->xml_backend->bg0310->import_entity(
        'writer',
        {
            stuurgegevens   => {
                $self->_build_stuurgegevens(),
                entiteittype => 'NPS',
            },
            parameters      => {
                mutatiesoort => 'T',
            },
            object          => {
                'entiteittype'      => 'NPS',
                'values'            => $values,
                ($params->{internal_identifier} ? ('sleutelVerzendend' => $params->{internal_identifier}) : ()),
                ($params->{external_identifier} ? ('sleutelOntvangend' => $params->{external_identifier}) : ()),
            },
        }
    );

    return $xml;
}

=head2 search_np

    $processor->search_np(
        {
            personal_number => 556589785,
            address_residence.street => 'Donker',
            address_residence.number => 44,
        },
    );

    ## Will generate the XML with a C<gelijk> block with the given search parameters

Generates the XML for a person search. It will generate the question parameters and a scope block
containing all the attributes we would like to retrieve as a response.

=cut

sub search_np {
    my ($self, $params) = @_;
    $params             = Zaaksysteem::BR::Subject::Utils->_inflate_key_value_to_hash($params);

    ### Searching with primary parameters
    my $native_params   = $self->_map_params($params, BG0310_PARAMETER_CONFIGURATION->{'NPS'}->{mapping}, 'type');
    $native_params      = $self->_apply_search_settings($native_params, 'NPS');

    ### Searching with address parameters
    my $adr_params;
    if ($params->{address_residence}) {
        $adr_params      = $self->_map_params($params->{address_residence}, BG0310_PARAMETER_CONFIGURATION->{'NPS'}->{address_mapping}, 'type');
        $adr_params      = $self->_apply_search_settings($adr_params, 'NPS');
    }

    my $gelijk_xml  = '';
    $gelijk_xml     .= $self->_gen_search_xml($_) for (
        @{ $native_params },
        (
            $adr_params
                ? {
                    'group'     => 'verblijfsadres',
                    'search'    => $adr_params,
                } : ()
        )
    );

    ### Generate XML
    my $xml = $self->xml_backend->bg0310->search_nps(
        'writer',
        {
            stuurgegevens => {
                $self->_build_stuurgegevens(),
            },
            gelijk  => $gelijk_xml
        }
    );

    return $xml;
}

=head2 disable_entity

    $processor->disable_entity(
        {
            internal_identifier         => '83',
            external_identifier         => 587854589,
        },
    );

Imports a 'NPS' entity by sending a "afnemerindicatie" message to the other party.

=cut

define_profile disable_entity => (
    required    => {
        internal_identifier     => 'Str',
    },
    optional    => {
        external_identifier     => 'Str',
    }
);

sub disable_entity {
    my $self    = shift;
    my $params  = assert_profile(shift || {})->valid;

    ### Generate XML
    my $xml = $self->xml_backend->bg0310->disable_entity(
        'writer',
        {
            stuurgegevens   => {
                $self->_build_stuurgegevens(),
                entiteittype => 'NPS',
            },
            parameters      => {
                mutatiesoort => 'T',
            },
            object          => {
                'entiteittype'      => 'NPS',
                'value'             => {},
                'sleutelVerzendend' => $params->{internal_identifier},
                ($params->{external_identifier} ? ('sleutelOntvangend' => $params->{external_identifier}) : ()),
            },
        }
    );

    return $xml;
}

=head2 confirm_message

    $processor->confirm_message()

Response to the other party telling we succesfully receiver their message

=cut

sub confirm_message {
    my ($self, $params) = @_;

    ### Generate XML
    my $xml = $self->xml_backend->bg0310->confirm_message(
        'writer',
        {
            stuurgegevens => {
                $self->_build_stuurgegevens({as_receiver => 1}),
            },
        }
    );

    ### Manually replace to match the selected search queries

    ### Manually upgrade the objects to 

    return $xml;
}

=head2 error_message

    $processor->error_message(
        {
            errortype   => 'Fo01',
            code        => 'StUF001',
            message     => 'Random message',
        }
    )

Response to the other party telling we succesfully receiver their message

=cut

define_profile error_message => (
    required => {
        errortype   => subtype('Str' => where { $_ =~ m/^Fo0[1-3]$/ }),
        code        => 'Str',
        message     => 'Str',
    }
);

sub error_message {
    my $self    = shift;
    my $params  = assert_profile(shift || {})->valid;

    ### Generate XML
    my $xml = $self->xml_backend->bg0310->error_message(
        'writer',
        {
            stuurgegevens => {
                $self->_build_stuurgegevens({as_receiver => 1}),
            },
            %$params,
        }
    );

    return $xml;
}

=head1 PARSING XML

=head2 parse_np

    $processor->parse_np($xml)

Will parse the given xml into a NP entry. Will throw an error when we are unable to perform due
to invalid xml, invalid entity etc

=cut

sub parse_np {
    my ($self, $xml)    = @_;
    my $parser          = $self->xml_backend->bg0310->parser($xml);

    if (! $parser->findnodes('//bg:npsLk01')) {
        throw('processor/bg0310/invalid_message', 'Unable to process this xml message, parsing supported: npsLk01');
    }

    my $object_prefix   = "//bg:npsLk01/bg:object[1]";
    if (my ($node)  = $parser->findnodes('//bg:npsLk01/bg:parameters/st:mutatiesoort')) {
        if ($node->textContent ne 'T') {
            $object_prefix   = "//bg:npsLk01/bg:object[2]";

        }
    } else {
        throw('processor/bg0310/invalid_mutatiesoort', 'Cannot handle this message, need a mutatiesoort');
    }

    return $self->_parse_np_object($parser, $object_prefix);
}

=head2 parse_multiple_np

    $processor->parse_multiple_np($xml)

Will parse the given xml into a list of NP entries. Will throw an error when we are unable to perform due
to invalid xml, invalid entity etc

=cut

sub parse_multiple_np {
    my ($self, $xml)    = @_;
    my $parser          = $self->xml_backend->bg0310->parser($xml);

    if (! $parser->findnodes('//bg:npsLa01')) {
        throw('processor/bg0310/invalid_message', 'Unable to process this xml message, parsing supported: npsLa01');
    }

    my @nodes = $parser->findnodes('//bg:npsLa01/bg:antwoord/bg:object');

    my @nps;
    for (my $i = 1; $i <= @nodes; $i++) {
        my $object_prefix = "//bg:npsLa01/bg:antwoord/bg:object[$i]";

        push(@nps, $self->_parse_np_object($parser, $object_prefix));
    }

    return \@nps;
}


=head2 parse_stuf_error

    my $hashref = $processor->parse_stuf_error($xml)

    # Returns, e.g.
    {
        code => STUF005,
        message => 'StUF Error, not authorized',
        is_stuf_error => 1
    }

Will return an error when there is one. In case of a StUF error, C<is_stuf_error> is set to true.

=cut

sub parse_stuf_error {
    my $self        = shift;
    my $xml         = shift;
    my $expected    = shift;
    my $rv;

    my $parser      = $self->xml_backend->bg0310->parser($xml);

    if ($expected) {
        my @expected   = $parser->findnodes('//' . $expected);

        ### Everything A OK
        return if @expected;

        ### Not what we expected....
        $rv = {
            is_stuf_error   => 0,
            code            => 'unknown',
            message         => "Unknown message, expected " . $expected
        };
    }

    my @fault       = $parser->findnodes('//SOAP:Fault');
    my @stuffault   = $parser->findnodes("//*[local-name()='Fo01Bericht' or local-name()='Fo02Bericht' or local-name()='Fo03Bericht']");

    return $rv unless (@fault || @stuffault);

    if (@fault) {
        $rv->{message} = $self->get_text_content($parser, '//faultstring') // 'Unknown SOAP error';
        $rv->{code} = $self->get_text_content($parser, '//faultcode') // '';
    }

    if (@stuffault) {
        $rv->{is_stuf_error} = 1;
        $rv->{message} = $self->get_text_content($parser, '//st:omschrijving') // 'Unknown StUF error';
        $rv->{code} = $self->get_text_content($parser, '//st:code') // '';   
    }

    return $rv;
}

=head1 SPOOFING

=head2 answer_question_np

    $processor->answer_question_np(\%params)

Answers an np question (spoof usage)

=cut

sub answer_question_np {
    my ($self, $params) = @_;
    $params             = Zaaksysteem::BR::Subject::Utils->_inflate_key_value_to_hash($params || {});

    ### Generate XML
    my $xml = $self->xml_backend->bg0310->response_search_nps(
        'writer',
        {
            stuurgegevens => {
                $self->_build_stuurgegevens({as_receiver => 1}),
            },
            $self->_gen_dummy_data($params),
        }
    );

    ### Manually replace to match the selected search queries

    ### Manually upgrade the objects to 
    return $xml;
}

=head2 spoof_ack_from_other_party

    $processor->spoof_ack_from_other_party(\%params)

Answers an np question (spoof usage)

=cut

sub spoof_ack_from_other_party {
    my ($self, $params) = @_;
    $params             = Zaaksysteem::BR::Subject::Utils->_inflate_key_value_to_hash($params || {});

    ### Generate XML
    my $xml = $self->xml_backend->bg0310->spoof_lk01_np(
        'writer',
        {
            stuurgegevens => {
                $self->_build_stuurgegevens({as_receiver => 1}),
                mutatiesoort => 'T',
            },
            parameters      => {
                mutatiesoort => 'T',
            },
            $self->_gen_dummy_data($params),
        }
    );

    ### Manually replace to match the selected search queries

    ### Manually upgrade the objects to 
    return $xml;
}

=head1 HELPER METHODS

=head2 escape_value

    $self->escape_value('< this is a & test>');

Same as C< | XML > in L<Template::Toolkit>, filters the given value and makes sure unsafe entities are converted
to their xml entities.

=cut

sub escape_value {
  my $self = shift;
  my $data = shift;

  $data =~ s/&/&amp;/sg;
  $data =~ s/</&lt;/sg;
  $data =~ s/>/&gt;/sg;
  $data =~ s/"/&quot;/sg;
  $data =~ s/'/&apos;/sg;

  return $data;
}


=head2 search_to_string

    my $vars    = {
        'personal_number'           => '123456789',
        'initials'                  => 'D.',
        'first_names'               => 'Don',
        'family_name'               => 'Fuego',
        'prefix'                    => 'The',
        'address_residence.street'                  => 'Muiderstraat',
        'address_residence.street_number'           => 42,
    };
    
    $string = $self->search_to_string($vars)

    # Returns, e.g.:
    $VAR1 = '123456789, The, Fuego'

Returns a stringified version of the first three search options in order of importance.

E.g: BSN and family_name are the most identifying parameters, start with returning these.

=cut

sub search_to_string {
    my $self        = shift;
    my $params      = shift;

    my @search      = qw/
        personal_number
        prefix
        family_name
        date_of_birth
        address_residence.zipcode
        address_residence.street
        address_residence.street_number
    /;

    my @values;
    for my $key (@search) {
        last if @values > 2;
        push (@values, $params->{$key}) if length $params->{$key};
    }

    return join(', ', @values);
}

=head2 get_text_content

    $self->get_text_content($parser, '//omschrijving');

Returns the text content of the given xpath

=cut

sub get_text_content {
    my ($self, $parser, $xpath) = @_;

    my ($node)  = $parser->findnodes($xpath);
    return unless $node;
    return $node->textContent;
}


=head1 PRIVATE METHODS

=head2 _parse_np_object

Parses a single StUF3 object containing NP data

=cut

sub _parse_np_object {
    my ($self, $parser, $object_prefix) = @_;

    my $values = {
        subject_type => 'person',
    };

    $self->_load_np_native      ($parser, $values, $object_prefix);
    $self->_load_np_partner     ($parser, $values, $object_prefix);
    $self->_load_np_addresses   ($parser, $values, $object_prefix);
    $self->_load_np_subscription($parser, $values, $object_prefix);

    return $values;
}

=head2 _load_np_native

Load the native attributes for an NP, like burgerservicenumber etc

=cut

sub _load_np_native {
    my ($self, $parser, $values, $prefix) = @_;

    my $subject_values = {};
    for my $key (keys %{ BG0310_PARAMETER_CONFIGURATION->{NPS}->{xpath} }) {
        my $xpath   = BG0310_PARAMETER_CONFIGURATION->{NPS}->{xpath}->{$key};

        $self->_load_value_from_xpath($parser, $subject_values, $key, "$prefix/" . $xpath);
    }

    if ($subject_values->{municipality}) {
        if (int($subject_values->{municipality}) == int($self->municipality_code)) {
            $subject_values->{is_local_resident} = 1;
        }
    }

    $values->{subject} = $subject_values;
}

=head2 _load_np_partner

Load the partner attributes for an NP.

=cut

sub _load_np_partner {
    my ($self, $parser, $values, $prefix) = @_;

    # Grab partner info directly from the the xpath
    my %partner = ();

    $self->_load_value_from_xpath(
        $parser,
        \%partner,
        'family_name',
        "$prefix/bg:geslachtsnaamPartner",
    );

    $self->_load_value_from_xpath(
        $parser,
        \%partner,
        'prefix',
        "$prefix/bg:voorvoegselGeslachtsnaamPartner",
    );

    if (%partner) {
        $values->{subject}{partner} = \%partner;
        return;
    }

    # Get the active partner:
    #
    # The list is ordered by name, datumSluiting. we need to add somethings in
    # the list..
    my ($partner) = $parser->findnodes("$prefix/bg:inp.heeftAlsEchtgenootPartner");
    if ($partner) {
        my $xpath_mapping = BG0310_PARAMETER_CONFIGURATION->{NPS}{partner_xpath};
        for my $key (keys %{$xpath_mapping}) {

            next if $partner{$key};

            my $xpath = $xpath_mapping->{$key};

            $self->_load_value_from_xpath($parser, \%partner, $key,
                "$prefix/bg:inp.heeftAlsEchtgenootPartner/" . $xpath);
        }
        $values->{subject}{partner} = \%partner;
    }

    return;
}

=head2 _load_np_subscription

Load the native subscription attributes for an NP, like sleutels etc

=cut

sub _load_np_subscription {
    my ($self, $parser, $values, $prefix) = @_;

    unless ($self->interface_uuid) {
        throw(
            'processor/stuf/bg0310/subscription/no_interface_uuid_set',
            'Code error: no interface_uuid set on processor'
        );
    }

    my ($object)    = $parser->findnodes($prefix);
    my $external_id = $object->getAttributeNS('http://www.egem.nl/StUF/StUF0301', $self->other_party_primary_key);

    return unless $external_id;

    $values->{external_subscription} = {
        external_identifier => $external_id,
        interface_uuid      => $self->interface_uuid,
    };
}

=head2 _load_np_addresses

Load the addresses of a NP. Like address_residence, address_correspondence etc

=cut

sub _load_np_addresses {
    my ($self, $parser, $rv, $prefix) = @_;
    my $address;

    my $value_loader = sub {
        my ($mapper, $values, $adr_prefix) = @_;
        for my $key (keys %{ $mapper }) {
            my $xpath   = $mapper->{$key};

            $self->_load_value_from_xpath($parser, $values, $key, "$adr_prefix/" . $xpath );
        }
    };

    if (($address) = $parser->findnodes("$prefix/bg:sub.verblijfBuitenland")) {
        $rv->{subject}->{address_residence} = {};

        $value_loader->(BG0310_PARAMETER_CONFIGURATION->{NPS}->{foreign_address_xpath}, $rv->{subject}->{address_residence}, "$prefix/bg:sub.verblijfBuitenland");

        my ($lndcode)  = $parser->findnodes("$prefix/bg:sub.verblijfBuitenland/bg:lnd.landcode");
        if ($lndcode) {
            $rv->{subject}->{address_residence}->{country} = {
                dutch_code  => $lndcode->textContent
            }
        } else {
            throw(
                'processor/bg0310/load_foreign_address/no_country_code',
                'Foreign address, but no country code in response message'
            );
        }
    } elsif (($address) = $parser->findnodes("$prefix/bg:verblijfsadres")) {
        $rv->{subject}->{address_residence} = {};
        $value_loader->(BG0310_PARAMETER_CONFIGURATION->{NPS}->{address_xpath}, $rv->{subject}->{address_residence}, "$prefix/bg:verblijfsadres");

        $rv->{subject}->{address_residence}->{country} = {
            dutch_code  => '6030',
        };
        if (defined $rv->{subject}{municipality}) {
            $rv->{subject}{address_residence}{municipality}{dutch_code} = $rv->{subject}{municipality};
        }
    }

    if (($address) = $parser->findnodes("$prefix/bg:sub.correspondentieAdres")) {
        $rv->{subject}->{address_correspondence} = {};
        $value_loader->(BG0310_PARAMETER_CONFIGURATION->{NPS}->{correspondence_address_xpath}, $rv->{subject}->{address_correspondence}, "$prefix/bg:sub.correspondentieAdres");

        $rv->{subject}->{address_correspondence}->{country} = {
            dutch_code  => '6030',
        };

        if (defined $rv->{subject}{municipality}) {
            $rv->{subject}{address_correcepondence}{municipality}{dutch_code} = $rv->{subject}{municipality};
        }
    }

}

=head2 _gen_dummy_data

=cut

sub _gen_dummy_data {
    my $self    = shift;
    my $params  = shift;

    my $sleutelVerzendend = int(rand(400) * 1000);
    if ($params->{external_id}) {
        if ($params->{external_id} ne 'IN_PROGRESS') {
            $sleutelVerzendend = $params->{external_id};
        }
    }
    else {
        $sleutelVerzendend = undef if ($params->{personal_number} && (substr($params->{personal_number}, 0,1) == 9));
    }

    my $woonplaats = $params->{'address_residence'}->{'city'};
    my $gemeentecode;
    if (!$woonplaats) {
        $woonplaats = sub { ucfirst(lc(String::Random->new()->randpattern('CCCCCCCCC'))) };

        if ($params->{config_interface_id}) {
            my $config_interface = $self->schema->resultset('Interface')->find($params->{config_interface_id});
            $gemeentecode = $config_interface->jpath('$.gemeentecode');

            $woonplaats = RGBZ_GEMEENTECODES()->{$gemeentecode};
        }
    }
    if (!$gemeentecode) { 
        my $config_interface = $self->schema->resultset('Interface')->search_active({module => 'stufconfig'})->first;
        $gemeentecode = $config_interface->jpath('$.gemeentecode');

        $woonplaats = RGBZ_GEMEENTECODES()->{$gemeentecode};
    }

    return (
        object => {
            ($sleutelVerzendend ? (sleutelVerzendend => $sleutelVerzendend) : ()),
            ($params->{local_object_id} ? (sleutelOntvangend => $params->{local_object_id}) : ()),
            'bsn' => ($params->{personal_number} || sub { generate_bsn() }),
            'anummer' => ($params->{personal_number_a} || sub { '1' . String::Random->new()->randpattern('nnnnnnnnn') }),
            'geslachtsnaam' => ($params->{family_name} || sub { String::Random::random_string('0',[
                'Bakker',
                'Kool',
                'Puree',
                'Fleppenstein',
                'Springintveld',
                'Janssen',
                'Dekker',
                'Brouwer',
                'Muler',
                'Bos',
                'Vos',
                'Visser',
                'Smits',
                'Schouten',
                'Jacobs',
                'Kuipers',
                'Post',
                'Visser'
                ]) }
            ),
            'voorletters' => ($params->{initials} || sub { String::Random->new()->randpattern('CC') }),
            'voornamen' => ($params->{first_names} || sub { String::Random::random_string('0',[
                'Wim',
                'Piet',
                'Frits',
                'Jasper',
                'Jaap',
                'Gerard',
                'Koos',
                'Jan',
                'Delajah',
                'Marco',
                'Mieke',
                'Else',
                'Jonas',
                'Niels',
                'Pim',
                'Theo',
                'Chris',
                'Bert',
                'Sam',
                'Noah',
                'Max',
                'Sven',
                'Siem',
                'Gijs',
                'Teun'
            ]) }),
            'geboortedatum' => ($params->{date_of_birth} || sub { String::Random->new()->randregex('19\d\d051\d') }),
            'geslachtsaanduiding' => ($params->{gender} || sub { ['M', 'V']->[int(rand(1))] }),
            'datumoverlijden' => (sub { [undef,  String::Random->new()->randregex('19\d\d051\d' )]->[int(rand(1))] }),
            'gemeentecode' => int($gemeentecode),
        },
        verblijfsadres => {
            'straatnaam' => ($params->{'address_residence'}->{'street'} || sub { ucfirst(lc(String::Random->new()->randpattern('CCCCCCCCCCCCCCCCC'))) }),
            'huisnummer' => ($params->{'address_residence'}->{'street_number'} || sub { String::Random->new()->randpattern('nn') }),
            'huisnummertoevoeging' => ($params->{'address_residence'}->{'street_number_suffix'} || sub { String::Random->new()->randpattern('CC') }),
            'huisletter' => ($params->{'address_residence'}->{'street_number_letter'} || sub { uc(String::Random->new()->randpattern('C')) }),
            'postcode' => (uc(($params->{'address_residence'}->{'zipcode'} || '')) || sub { '1' . String::Random->new()->randpattern('nnnCC') }),
            'woonplaatsNaam' => $woonplaats, 
        },
    );
}


=head2 _gen_search_xml

Generates the XML for a search

TODO: Move this to template

=cut

sub _gen_search_xml {
    my $self    = shift;
    my $param   = shift;

    my $name    = $param->{group} || $param->{name};

    my $rv = '      ';
    $rv         .= '<bg:' . $name;
    if ($param->{group}) {
        $rv         .= ">\n";
        $rv         .= "   " . $self->_gen_search_xml($_) for @{ $param->{search} };
        $rv         .= '      </bg:' . $name . ">\n";
    } else {
        $rv         .= ($param->{exact} ? ' st:exact="' . $param->{exact} . '">' : '>');
        $rv         .= $self->escape_value($param->{value});
        $rv         .= '</bg:' . $name . ">\n";
    }


    return $rv;
}

=head2 _gen_stuf3_structure

Will generate a stuf3 structure.

=cut

sub _gen_stuf3_structure {
    my $self    = shift;
    my $list    = shift;
    my $depth   = shift || 1;
    my $map     = shift;

    my $ind     = ('  ' x $depth);

    my @response;
    for my $param (@$list) {
        my $name    = $param->{group} || $param->{name};
        $name       = $map->{$name} if $map->{$name};

        my $rv     .= $ind . '<'. $name;
        if ($param->{group}) {
            $rv         .= ' st:entiteittype="' . $param->{entiteittype} . '"' if $param->{entiteittype};
            $rv         .= ' st:verwerkingssoort="' . $param->{verwerkingssoort} . '"' if $param->{verwerkingssoort};
            $rv         .= ">\n";
            $rv         .= $self->_gen_stuf3_structure($param->{values}, ($depth+1), $map);
            $rv         .= "\n" . $ind . '</' . $name . ">";
        } else {
            if (!defined($param->{value})) {
                $rv         .= ' st:noValue="geenWaarde" xsi:nil="true" />';
            } else {
                $rv         .= '>';
                $rv         .= $self->escape_value($param->{value});
                $rv         .= '</' . $name . ">";
            }
        }

        push(@response, $rv);
    }

    return join("\n", @response);
}

=head2 _load_value_from_xpath

Load a value into the values HashRef. Will remove a value when the noValue is set to something
other than geenWaarde

=cut

sub _load_value_from_xpath {
    my ($self, $parser, $values, $key, $xpath) = @_;

    my ($node)  = $parser->findnodes($xpath);
    return unless $node;

    $values->{$key} = $node->textContent;

    ### Got node
    if (my $novalue = $node->getAttributeNS('http://www.egem.nl/StUF/StUF0301', 'noValue')) {
        if ($novalue eq 'geenWaarde') {
            $values->{$key} = undef;
        } else {
            ### No changes, delete the key
            delete $values->{$key}
        }
    }
}

sub _generate_stuf3_schema_from_entity {
    my $self    = shift;
    my $subject = shift;

    my %rev_map = map { BG0310_PARAMETER_CONFIGURATION->{NPS}->{mapping}->{$_} => $_ } keys %{ BG0310_PARAMETER_CONFIGURATION->{NPS}->{mapping} };

    my @schema;
    for my $key (@{ BG0310_PARAMETER_CONFIGURATION->{NPS}->{stuf_sort} }) {
        next unless $rev_map{$key};
        my $value = $subject->{$rev_map{$key}};

        if (BG0310_PARAMETER_CONFIGURATION->{NPS}->{stuf_filters}->{$key}) {
            $value = BG0310_PARAMETER_CONFIGURATION->{NPS}->{stuf_filters}->{$key}->($value);
        }

        push(
            @schema,
            {
                name    => 'bg:' . $key,
                value   => $value,
            }
        );
    }

    my %rev_adr = map { BG0310_PARAMETER_CONFIGURATION->{NPS}->{address_mapping}->{$_} => $_ } keys %{ BG0310_PARAMETER_CONFIGURATION->{NPS}->{address_mapping} };

    my @address;
    for my $key (@{ BG0310_PARAMETER_CONFIGURATION->{NPS}->{stuf_sort} }) {
        next unless $rev_adr{$key};
        my $value = $subject->{'address_residence'}->{$rev_adr{$key}};

        if (BG0310_PARAMETER_CONFIGURATION->{NPS}->{stuf_filters}->{$key}) {
            $value = BG0310_PARAMETER_CONFIGURATION->{NPS}->{stuf_filters}->{$key}->($value);
        }

        push(
            @address,
            {
                name    => 'bg:' . $key,
                value   => $value,
            }
        );
    }

    push(
        @schema,
        {
            group   => 'bg:verblijfsadres',
            values  => \@address,
        }
    );

    if ($subject->{partner}->{family_name}) {
        my %rev_par = map { BG0310_PARAMETER_CONFIGURATION->{NPS}->{partner_mapping}->{$_} => $_ } keys %{ BG0310_PARAMETER_CONFIGURATION->{NPS}->{partner_mapping} };

        my @partner;
        for my $key (@{ BG0310_PARAMETER_CONFIGURATION->{NPS}->{stuf_sort} }) {
            next unless $rev_par{$key};
            my $value = $subject->{'partner'}->{$rev_par{$key}};

            if (BG0310_PARAMETER_CONFIGURATION->{NPS}->{stuf_filters}->{$key}) {
                $value = BG0310_PARAMETER_CONFIGURATION->{NPS}->{stuf_filters}->{$key}->($value);
            }

            push(
                @partner,
                {
                    name    => 'bg:' . $key,
                    value   => $value,
                }
            );
        }

        push(
            @schema,
            {
                group               => 'bg:inp.heeftAlsEchtgenootPartner',
                entiteittype        => 'NPSNPSHUW',
                verwerkingssoort    => 'T',
                values  => [
                    {
                        group               => 'bg:gerelateerde',
                        entiteittype        => 'NPS',
                        verwerkingssoort    => 'T',
                        values              => \@partner
                    }
                ],
            },
        );
    }


    return \@schema;
}


=head2 _apply_search_settings


    my $hashparams = $processor->_apply_search_settings(
        {
            personal_number     => 234234234,
        },
        'NPS',                                  # The type to map to/from, 'NPS' only for now
    );

Returns a HashRef of mapped params with search settings for stuf generator applied

=cut

sub _apply_search_settings {
    my ($self, $params, $object_type, $direction) = @_;

    throw(
        'processor/bg0310/invalid_direction',
        'Please supply "NPS" for second parameter "object_type"'
    ) unless $object_type && $object_type eq 'NPS';

    my $search_settings     = BG0310_PARAMETER_CONFIGURATION->{$object_type}->{search_settings};

    return $params unless $search_settings;

    for my $data (@$params) {
        my $param = $data->{name};
        if ($search_settings->{$param}) {
            for my $sparam (keys %{ $search_settings->{$param} }) {
                ### Apply filter
                if ($sparam eq 'filter') {
                    $data->{value} = $search_settings->{$param}->{$sparam}->($data->{value});
                    next;
                }

                $data->{$sparam} = $search_settings->{$param}->{$sparam};
            }
            
        }
    }

    return $params;
}


=head2 _map_params

    my $hashparams = $processor->_map_params(
        {
            personal_number     => 234234234,
        },
        { personal_number => 'inp.bsn' }        # The type to map to/from, 'NPS' only for now
        'type'                                  # Source of the params, either "type" or "stuf"
    );

Returns a HashRef of mapped params.

=cut

sub _map_params {
    my ($self, $params, $given_mapping) = @_;

    my %mapping         = %{ $given_mapping };
    my @sorting         = @{ BG0310_PARAMETER_CONFIGURATION->{'NPS'}->{stuf_sort} || [] };

    ### Prepare return value from mapped params
    my %mapped_params   = map({ $mapping{$_} => $params->{$_} } grep( { exists $params->{$_} } keys %mapping));
    my @rv;
    for my $key (@sorting) {
        next unless exists $mapped_params{$key};

        push(
            @rv,
            {
                name    => $key,
                value   => $mapped_params{$key}
            }
        );
    }

    return \@rv;
}

=head2 _build_stuurgegevens

    $processor->_build_stuurgegevens(
        {
            as_receiver => 1       # optional: defines whether the "zender" of the stuurgegevens is us or
                                # the other party.
        }
    );

Returns a hash of stuurgegevens

=cut

define_profile _build_stuurgegevens => (
    required    => {},
    optional    => {
        as_receiver    => 'Bool'        # When receiver is set to true, "ontvanger" will be set to "our_party"
    }
);

sub _build_stuurgegevens {
    my $self    = shift;
    my $options = assert_profile(shift || {})->valid;

    return (
        zender              => ($options->{as_receiver} ? $self->other_party : $self->our_party),
        ontvanger           => ($options->{as_receiver} ? $self->our_party : $self->other_party),
        tijdstipBericht     => DateTime->now()->strftime('%Y%m%d%H%M%S%3N'),
        referentienummer    => $self->reference_id,
    );
}


__PACKAGE__->meta->make_immutable();

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
