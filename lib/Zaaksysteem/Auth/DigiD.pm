package Zaaksysteem::Auth::DigiD;
use Moose;
use XML::LibXML::XPathContext;
use XML::LibXML;
use Digest::SHA qw(sha1_base64);

=head1 NAME

Zaaksysteem::Auth::DigiD - A DigiD model for Zaaksysteem

=cut

has redis => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Store::Redis',
    required => 1,
);

has cookiejar => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Store::Redis',
    required => 1,
);

sub _get_xpath {
    my ($self, $xml) = @_;

    my $xp  = XML::LibXML::XPathContext->new(
        XML::LibXML->load_xml(string => $xml)
    );

    $xp->registerNs('samlp', 'urn:oasis:names:tc:SAML:2.0:protocol');
    $xp->registerNs('saml', 'urn:oasis:names:tc:SAML:2.0:assertion');

    return $xp;
}

sub _get_nameid {
    my ($self, $xml) = @_;

    my $xp = $self->_get_xpath($xml);
    my @nodes = $xp->findnodes('//samlp:LogoutRequest/saml:NameID');
    return $nodes[0]->textContent if @nodes;
    return;
}

sub _generate_redis_id {
    my ($self, $id) = @_;
    # Just obfuscation, not security, as this is used as a key. Salted hashes
    # would make the lookup a bit more complex.
    return join(':', 'digid', sha1_base64($id));

}

sub start_federated_session {
    my ($self, $id, $session_id) = @_;

    $id = $self->_generate_redis_id($id);

    $self->redis->set($id, "session:$session_id");
    # DigiD FE sessions expire by default after 3 hrs
    $self->redis->expire($id, 10800);
    return 1;
}

sub end_federated_session {
    my ($self, $xml) = @_;

    my $name_id = $self->_get_nameid($xml);

    return 0 unless defined $name_id;

    $name_id = $self->_generate_redis_id($name_id);

    my $session_id = $self->redis->get($name_id);

    return 0 unless defined $session_id;

    $self->redis->del($name_id);
    $self->cookiejar->del($session_id);
    $self->cookiejar->del("json:$session_id");
    return 1;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
