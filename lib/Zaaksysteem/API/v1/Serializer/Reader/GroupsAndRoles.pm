package Zaaksysteem::API::v1::Serializer::Reader::GroupsAndRoles;

use Moose;

with 'Zaaksysteem::API::v1::Serializer::DispatchReaderRole';

=head1 NAME

Zaaksysteem::API::v1::Serializer::Reader::GroupsAndRoles - Group and Role readers

=head1 DESCRIPTION

This serializer encodes various Group and Role Objects into a valid APIv1 JSON structure.


=head1 METHODS

=head2 dispatch_map

The dispatcher for various Map Objects

=over

=item group

=item role

=back

=cut

sub dispatch_map {
    return (
        'Zaaksysteem::Model::DB::Groups' => sub { __PACKAGE__->read_group(@_) },
        'Zaaksysteem::Model::DB::Roles' => sub { __PACKAGE__->read_role(@_) },
    );
}

=head2 read_group

Read groups

=cut

sub read_group {
    my ($class, $serializer, $object) = @_;

    return {
        type => 'group',
        reference => undef,
        instance => {
            (map({ $_ => $object->$_ } qw/id name/)),
            (map({ $_ => $serializer->read($object->$_) } qw/date_created date_modified/)),
        }
    };
}

=head2 read_role

Read roles

=cut

sub read_role {
    my ($class, $serializer, $object) = @_;

    return {
        type => 'role',
        reference => undef,
        instance => {
            (map({ $_ => $object->$_ } qw/id name/)),
            (map({ $_ => $serializer->read($object->$_) } qw/date_created date_modified/)),
            system_role => ($object->system_role ? JSON::true : JSON::false),

        }
    };
}


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
