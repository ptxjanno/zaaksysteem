package Zaaksysteem::Cache;

use Moose;

use Storable     qw( freeze thaw );
use Digest::MD5  qw( md5_base64 );

use Data::Dumper;
use Data::FormValidator;

has 'storage' => (
    is =>'ro',
);


sub BUILD {
    my $self        = shift;

    die('Missing required storage object') unless $self->storage;
}

=head1 NAME

Zaaksysteem::Cache - Zaaksysteem cache object

=head1 SYNOPSIS

    my $cache = Zaaksysteem::Cache->new(
        storage => $c->stash->{__zs_cache}
    );

    $cache->set({
        key         => 'hello',
        value       => 'world',
        params      => { withthese => 'params'}
    });

    print $cache->get({
        key     => 'hello',
    });

    # Returns: world

    print $cache->get({
        key     => 'hello',
        params  => { withthese => 'params'}
    });

    # Returns: world

    print $cache->get({
        key     => 'hello',
        params  => { withthese => 'invalidparams'}
    });

    # Returns: undef

    $cache->clear();

    # Cache cleared

=head1 DESCRIPTION

Caching object with the added ability to store information dependent
on given parameters.

=head1 METHODS

=head2 $cache->set(\%OPTIONS)

    $cache->set(
        key         => 'say_hoi',
        value       => 'hoi',
        params      => { onlywhenthisparam => 'isset' }
        callback    => sub {
            my ($cache, $id, $zaak) = @_;

            return 1 if $zaak->last_modified < DateTime->now()->subtract(seconds => 1);
        }
    )

Sets the the given content in cache object $IDENTIFIER. Optional $PARAMETERS
can be given, to make sure you can only retrieve this object when the
parameters match.

=cut

sub set {
    my ($self, $options) = @_;

    my $dv = Data::FormValidator->check(
        $options,
        {
            required => [qw/key value/],
            optional => [qw/callback params/],
        }
    );

    die('Missing params: ' . Dumper($dv)) unless $dv->success;

    my $identifier = $options->{key};
    my $content    = $options->{value};
    my $parameters = $options->{params};
    my $callback   = $options->{callback};

    $self->storage->{$identifier} = {
        content => $content,
    };

    if ($parameters) {
        $self->storage->{$identifier}->{hash} = $self->_create_hash($parameters),
    }

    if ($callback) {
        $self->storage->{$identifier}->{callback} = $callback,
    }

    return $content;
}

=head2 $cache->get(\%OPTIONS)

    $cache->get(
        key             => 'say_hoi',
        params          => { onlywhenthisparam => 'isset' }
        callback_args   => [$case],
    );

Gets the content from cache object identifier by $IDENTIFIER. Optional
$PARAMETERS can be given, to make sure you only retrieve this object
when the parameters match.

=cut

sub get {
    my ($self, $options) = @_;

    my $dv = Data::FormValidator->check(
        $options,
        {
            required => [qw/key/],
            optional => [qw/callback_args params/],
        }
    );

    die('Missing params: ' . Dumper($dv)) unless $dv->success;

    my $identifier = $options->{key};
    my $content    = $options->{value};
    my $parameters = $options->{params};
    my $callbackargs = $options->{callback_args} || [];

    return unless (
        exists($self->storage->{$identifier}) &&
        UNIVERSAL::isa($self->storage->{$identifier}, 'HASH')
    );

    my $cacheobj    = $self->storage->{$identifier};

    if ($parameters) {
        return unless (
            $cacheobj->{hash} eq
                $self->_create_hash($parameters)
        );
    }

    if ($cacheobj->{callback}) {
        return unless $cacheobj->{callback}($self, $identifier, @{ $callbackargs });
    }

    return $self->storage->{$identifier}->{content};
}

=head2 $cache->clear([$IDENTIFIER]);

Clears the cache for identifier (key), $IDENTIFIER. When no argument
is given, it will clear the complete cache.

=cut

sub clear {
    my $self        = shift;
    my $identifier  = shift;

    if ($identifier) {
        delete($self->storage->{$identifier})
            if exists($self->storage->{$identifier});

        return 1;
    }

    delete $self->storage->{$_} for keys %{ $self->storage };

    return 1;
}


=head2 PRIVATE METHODS

=head2 $cache->_create_hash($opts)

Created a unique identifier of the given parameters

=cut

sub _create_hash {
    my $self            = shift;
    my $options         = shift;

    return unless $options;

    my $serialized      = freeze($options);

    return md5_base64($serialized);
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 BUILD

TODO: Fix the POD

=cut

