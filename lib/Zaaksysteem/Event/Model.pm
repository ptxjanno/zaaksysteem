package Zaaksysteem::Event::Model;
use Moose;

with 'MooseX::Log::Log4perl';

use UUID::Tiny ':std';

has amqp => (
    isa => 'Net::AMQP::RabbitMQ',
    is => 'ro',
    required => 1,
);

has channel => (
    isa => 'Int',
    is => 'ro',
    required => 1,
);

has exchange => (
    isa => 'Str',
    is => 'ro',
    required => 1,
);

has context => (
    isa => 'Str',
    is => 'ro',
    required => 1,
);

has json => (
    is => 'rw',
    isa => 'JSON::XS',
    default => sub { JSON::XS->new->canonical(1) }
);

sub publish_incoming_email_event {
    my $self = shift;
    my $filestore_uuid = shift;

    $self->log->info(
        sprintf(
            "Broadcasting event for '%s' in context '%s'",
            $filestore_uuid,
            $self->context,
        )
    );

    my $routing_key = 'zsnl.v2.communication.IncomingEmail.EmailReceived';

    my $parameters = {
        id => create_uuid_as_string(UUID_V4),
        created_date => DateTime->now->iso8601 . 'Z',
        correlation_id => create_uuid_as_string(UUID_V4),
        context => $self->context,
        domain => 'communication',
        user_uuid => undef, # System event: not executed by a user
        entity_type => 'IncomingEmail',
        entity_id => create_uuid_as_string(UUID_V4),
        event_name => 'EmailReceived',
        changes => [
            {"key" => "file", "old_value" => undef, "new_value" => "$filestore_uuid"}
        ],
        entity_data => {},
    };

    $self->amqp->publish(
        $self->channel,
        $routing_key,
        $self->json->encode($parameters),
        { exchange => $self->exchange },
        { content_type => "application/json" },
    );
}

__PACKAGE__->meta->make_immutable();

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
