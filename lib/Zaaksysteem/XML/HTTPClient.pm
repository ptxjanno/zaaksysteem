package Zaaksysteem::XML::HTTPClient;

use Moose;
use Moose::Util::TypeConstraints;

class_type('LWP::UserAgent');
class_type('Zaaksysteem::XML::MijnOverheid::HTTPClient::Spoof::UserAgent');

use namespace::autoclean;

use BTTW::Tools::UA;
use BTTW::Tools;

=head1 NAME

Zaaksysteem::XML::HTTPClient - "Bare" XML over HTTP (POST) client

=head1 SYNOPSIS

    my $instance = Zaaksysteem::XML::Compile->mijnoverheid;
    my $client = Zaaksysteem::XML::HTTPClient->new(
        xml_instance => $instance,
        call_mapping => [
            {
                request  => 'has_berichtenbox',
                response => 'has_berichtenbox_response',
                url => 'https://foo.bar.com/has_berichtenbox',
            },
            ...
        ],
    );

    # Any "request" from the URL map will work
    $client->call('has_berichtenbox', {argument1 => value1, etc.});

=head1 ATTRIBUTES

=head2 ua

The L<LWP::UserAgent> instance to use.

Defaults to C<< LWP::UserAgent->new() >>.

=cut

has ua => (
    is      => 'ro',
    isa     => 'LWP::UserAgent | Zaaksysteem::XML::MijnOverheid::HTTPClient::Spoof::UserAgent',
    default => sub {
        return new_user_agent();
    },
);

=head2 xml_instance

The L<Zaaksysteem::XML::Compile::Instance> that's used to generate and parse the XML.

=cut

has xml_instance => (
    is       => 'ro',
    required => 1,
);

=head2 call_mapping

An hash references containing mappings from call names to XML element names and URLs.

They should look like this:

    {
        call_name => {
            request  => 'Name of the request XML method in the Zaaksysteem::XML::Compile::Instance',
            response => 'Name of the response XML method',
            url      => 'URL to POST to',
        },
    }

=cut

has call_mapping => (
    is       => 'ro',
    isa      => 'HashRef',
    lazy     => 1,
    builder  => '_build_call_mapping',
);

sub _build_call_mapping {
    return {};
}

=head1 METHODS

=head2 call

Send the specified request to the configured URL, and process and returns the
response.

This function takes at least one argument: the name of the call to perform.
This should be an existing key in the C<call_mapping>.

Any other arguments are passed to the XML writer, and should most likely be a
has reference.

The return value is a two-member array. The first is the decoded XML as a Perl
data structure, the second the raw XML (for logging).

=cut

sub call {
    my $self = shift;
    my $call_name = shift;

    my $mapping = $self->call_mapping->{$call_name};
    my $request_method  = $mapping->{request};
    my $response_method = $mapping->{response};

    my $call_xml = $self->xml_instance->$request_method('WRITER', @_);

    my $res = $self->ua->post(
        $mapping->{url},
        Content_Type => "text/xml",
        Content      => $call_xml,
    );

    if (!$res->is_success) {
        throw(
            'xmlhttpclient/response',
            'Got a non-success response from HTTP request: ' . $res->code,
            $res,
        );
    }

    my $res_data = try {
        $self->xml_instance->$response_method('READER', $res->decoded_content);
    } catch {
        throw(
            'xmlhttpclient/response',
            'Got invalid XML response from HTTP request: ' . $_
        );
    };

    return (
        $res_data,
        $res->decoded_content,
        $call_xml,
    );
}

__PACKAGE__->meta->make_immutable();

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
