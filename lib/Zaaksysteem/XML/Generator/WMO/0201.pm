package Zaaksysteem::XML::Generator::WMO::0201;
use Moose;

with qw(
    Zaaksysteem::XML::Generator
    Zaaksysteem::XML::Roles::Zorginstituut
);

use BTTW::Tools;
use DateTime::Format::Strptime;

=head1 NAME

Zaaksysteem::XML::Generator::WMO::0201 - Definitions for iWMO 2.1 XML templates

=head1 SYNOPSIS

    use Zaaksysteem::XML::Generator::WMO::0201

    my $generator = Zaaksysteem::XML::Generator::WMO::0201->new(...);

    # Returns and accepts character strings, not byte strings!
    $xml_data = $generator->generate_case_id({
        stuurgegevens => { zender => '...', ontvanger => '...', etc. }
    });

    # Returns and accepts character strings, not byte strings!
    $xml_data = $generator->generate_case_id_return({
        stuurgegevens => { zender => '...', ontvanger => '...', etc. }
        zaak => { identificatie => '' }
    });

    # One method is created for every .xml file in the template directory.

=head1 METHODS

=head2 name

Short name for this module, for use as the accessor name in
L<Zaaksysteem::XML::Compile::Backend>.

=cut

sub name { return 'iwmo_0201' }

=head2 path_prefix

Path (under share/xml-templates) to search for . "stuf0310".

=cut

sub path_prefix {
    return 'zorginstituut/0201/wmo';
}

__PACKAGE__->build_generator_methods();

__PACKAGE__->meta->make_immutable();

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
