package Zaaksysteem::XML::Roles::Zorginstituut;
use Moose::Role;

use BTTW::Tools qw(throw);
use Encode qw(encode);
use List::Util qw(first);
use MIME::Base64 qw(encode_base64);
use Zaaksysteem::Version qw(:version);



around _build_predefines => sub {
    my $orig = shift;
    my $self = shift;

    my $retval = $self->$orig;

    $retval->{convert_boolean}           = \&convert_to_boolean;
    $retval->{convert_dt_to_date}        = \&convert_dt_to_date;
    $retval->{convert_dt_to_time}        = \&convert_dt_to_time;
    $retval->{convert_dt_to_timestamp}   = \&convert_dt_to_timestamp;
    $retval->{validate_product_code}     = \&validate_product_code;
    $retval->{validate_agb_code}         = \&validate_agb_code;
    $retval->{set_int_enum}              = \&set_int_enum;
    $retval->{set_enum}                  = \&set_enum;
    $retval->{encode_to_base64}          = \&encode_to_base64;
    $retval->{format_municipality_code}  = \&format_municipality_code;
    $retval->{zaaksysteem_major_version} = sub { return sprintf("%04d", major_version) };
    $retval->{zaaksysteem_minor_version} = sub { return sprintf("%04d", minor_version) };
    $retval->{zaaksysteem_patch_version} = sub { return sprintf("%04d", patch_version) };

    return $retval;
};

=head2 convert_to_boolean

Convert the perl-like boolean to a WMO like boolean.

=cut

sub convert_to_boolean {
    my $val = shift;
    return 2 if !$val;
    return 1;
}

=head2 convert_dt_to_timestamp

Convert a datetime object or a scalar to a StUF 0301 date.

=cut

sub convert_dt_to_timestamp {
    return _convert_dt_to_custom('%Y%m%d%H%M%S', shift);
}


=head2 convert_dt_to_date

Convert a datetime object or a scalar to a WMO date.

=cut

sub convert_dt_to_date {
    return _convert_dt_to_custom('%F', shift);
}

=head2 convert_dt_to_time

Convert a datetime object or a scalar to a WMO timestamp.
WMO does not like twelve o' clock, so we adjust it to twelve o' clock
plus one second.

=cut

sub convert_dt_to_time {
    my $str = _convert_dt_to_custom('%T', shift);
    # 00:00:00 is not allowed for some reason.
    if ($str eq '00:00:00') {
        return '00:00:01';
    }
    return $str;
}

sub _convert_dt_to_custom {
    my ($pattern, $val) = @_;

    if (!ref $val) {
        my $dtf = DateTime::Format::Strptime->new(pattern => '%d-%m-%Y');
        $val = $dtf->parse_datetime($val);
    }
    my $dtf = DateTime::Format::Strptime->new(
        pattern   => $pattern,
        locale    => 'nl_NL',
        time_zone => 'Europe/Amsterdam',
    );
    return $dtf->format_datetime($val);
}

=head2 validate_product_code

Validates the product code, must be a value with a lenght between 1 and 5.

=cut

sub validate_product_code {
    my $val = shift;
    if (length $val > 5) {
        throw("xml/zorginstituut/product_code/invalid", "Product code is more than 5");
    }
    return $val;
}

=head2 set_int_enum

Converts a product code to a valid value (1 => 01).

=cut

sub set_int_enum {
    my ($val, @options) = @_;
    return unless defined $val;
    return set_enum(sprintf("%02d", int($val)), @options);
}

=head2 set_enum

Set an enumeration

=cut

sub set_enum {
    my ($val, @options) = @_;
    if (first { $val eq $_ } @options) {
        return $val;
    }
    throw("xml/zorginstituut/set_enum/invalid", "Enumeration '$val' is incorrect");
}

=head2 format_municipality_code

Converts a munciplaity code to a valid value (999 => 0999).

=cut

sub format_municipality_code {
    return sprintf("%04d", int(shift));
}


=head2 validate_agb_code

Validates if an AGB code is correct (8 digits).

=cut

sub validate_agb_code {
    my $val = shift;

    if ($val =~ /^[0-9]{8}$/) {
        return $val;
    }
    throw("xml/zorginstituut/agb_code/invalid", "AGB code '$val' is incorrect");
}

sub encode_to_base64 {
    my $xml = shift;
    return encode_base64(encode('UTF-8', $xml, Encode::FB_CROAK));
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
