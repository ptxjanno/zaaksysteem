package Zaaksysteem::Zaaktypen::BaseResultSet;
use Moose;

with 'MooseX::Log::Log4perl';

use BTTW::Tools;

use constant ZAAKTYPE_PREFIX  => 'zaaktype_';


sub _retrieve_columns {
    my ($self, $no_extras)  = @_;

    my @columns     = $self->result_source->columns;
    return @columns if $no_extras;

    ### It is possible we ask a relation with extra information,
    ### when the component exports extra_columns, we can add
    ### these to the columns information
    if ($self->result_source->result_class->can('added_columns')) {
        push(
            @columns,
            @{ $self->result_source->result_class->added_columns }
        );
    }

    return @columns;
}

sub _get_session_template {
    my ($self) = @_;

    my $template = {};
    for my $key ($self->_retrieve_columns) {
        $template->{$key} = undef;
    }

    return $template;
}

sub __validate_session {
    my ($self, $element_session_data, $profile, $single)    = @_;

    my $rv                                                  = {};

    return unless UNIVERSAL::isa($element_session_data, 'HASH');

    Params::Profile->register_profile(
        method => '__validate_session',
        profile => $profile,
    );

    if ($single) {
        $rv = Params::Profile->check(
            params  => $element_session_data
        );
    } else {
        while (my ($counter, $data) = each %{ $element_session_data }) {
            $rv->{$counter} = Params::Profile->check(
                params  => $data
            );
        }
    }

    return $rv;
}


sub _retrieve_as_session {
    my $self            = shift;
    my $extra_options   = shift;

    my @columns     = $self->_retrieve_columns;

    my $counter     = 0;

    my $rv          = {};

    my $search      = {};
    if ($extra_options && $extra_options->{search}) {
        $search     = $extra_options->{search};
    }

    my $rows        = $self->search(
        $search,
        {
            order_by    => 'id'
        }
    );

    while (my $row  = $rows->next) {
        $rv->{++$counter} = {};
        for my $column (@columns) {
            ### When this is a reference to another table, just
            ### retrieve the id
            if (
                UNIVERSAL::can($row->$column, 'isa') &&
                $row->$column->can('id')
            ) {
                $rv->{$counter}->{$column}  = $row->$column->id;
            } elsif (
                !ref($row->$column) ||
                !UNIVERSAL::can($row->$column, 'isa') ||
                $row->$column->isa('DateTime')
            ) {
                $rv->{$counter}->{$column} = $row->$column;
            }
        }
    }

    return $rv;
}

sub _params_to_database_params {
    my ($self, $columns, $params) = @_;

    my %result;
    # Deal with default values from the database
    foreach (@{$columns}) {
        if (!defined $params->{$_}) {
            my $info = $self->result_source->column_info($_);
            next if exists $info->{default_value};
        }
        $result{$_} = $params->{ $_ };
    }
    return \%result;
}

sub _commit_session {
    my ($self, $node, $element_session_data, $options)    = @_;
    my $rv = {};

    return unless UNIVERSAL::isa($element_session_data, 'HASH');

    my @keys    = sort { $a <=> $b } keys %{$element_session_data};
    foreach my $counter (@keys) {
        my $data_params = $element_session_data->{$counter};

        my @columns = $self->_retrieve_columns(1);

        my $data = $self->_params_to_database_params(\@columns, $data_params);

        delete $data->{$_} for qw(id uuid zaaktype_node_id);

        if ($node->can('status')) {
            if (
                $options && $options->{'status_id_column_name'}
            ) {
                $data->{ $options->{'status_id_column_name'} }     = $node->id;
            } else {
                $data->{ 'zaak_status_id' }     = $node->id;
            }

            $data->{zaaktype_node_id}   = $node->get_column('zaaktype_node_id'),
        } elsif (grep({ $_ eq 'zaaktype_node_id' } @columns)) {
            $data->{zaaktype_node_id}   = $node->id,
        }

        if (grep({ $_ eq 'zaaktype_id' } @columns) && $node->can('zaaktype_id')) {
            $data->{zaaktype_id} = $node->get_column('zaaktype_id');
        }

        if ($options->{extra_data}) {
            while (my ($extracol, $extradata) = %{ $options->{extra_data} }) {
                $data->{$extracol} = $extradata;
            }
        }

        #$self->log->debug(dump_terse($data));

        $rv->{$counter} = $self->create($data);
    }

    return $rv;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
