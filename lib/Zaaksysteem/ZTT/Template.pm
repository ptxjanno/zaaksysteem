package Zaaksysteem::ZTT::Template;

use Moose;

use BTTW::Tools;
use Zaaksysteem::StatsD;

use Zaaksysteem::ZTT::Modification;
use Zaaksysteem::ZTT::MagicDirective;

with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::ZTT::Template - Base class for L<ZTT|Zaaksysteem::ZTT> templates

=head1 ATTRIBUTES

=head2 tag_regexp

=cut

has tag_regexp => (
    is => 'rw',
    isa => 'RegexpRef',
    default => sub { return qr/(\[\[(.*?)\]\])/ }
);

=head2 directive_parser

=cut

has directive_parser => (
    is => 'rw',
    isa => 'Zaaksysteem::ZTT::MagicDirective',
    default => sub {
        return Zaaksysteem::ZTT::MagicDirective->new;
    }
);

=head2 modification_count

This counter is meant to keep track of how many modifications have been
applied to the template.

=cut

has modification_count => (
    is => 'rw',
    isa => 'Num',
    traits => [qw[Counter]],
    default => 0,
    handles => {
        increment_modifications => 'inc'
    }
);

=head1 CONSTRUCTORS

=head2 new_from_thing

This constructor attempts to recognize the provided argument as a source of
template data. It is a domain-breaking function, since it knows about the
subclasses that extend this base class.

    my $template = Zaaksysteem::ZTT::Template->new_from_thing($object);

It will throw a C<ztt/template/factory> exception if no suitable subclass could
be determined.

=cut

sub new_from_thing {
    my $class = shift;
    my $thing = shift;

    my $t0 = Zaaksysteem::StatsD->statsd->start;

    # If the thing is not a reference to anything, assume it's at least stringable
    unless (ref $thing) {
        require Zaaksysteem::ZTT::Template::Plaintext;

        my $template = Zaaksysteem::ZTT::Template::Plaintext->new(string => "$thing");

        Zaaksysteem::StatsD->statsd->end("ztt_new_from_thing_plaintext", $t0);

        return $template;
    }

    if (eval { $thing->isa('OpenOffice::OODoc::Document'); }) {
        require Zaaksysteem::ZTT::Template::OpenOffice;

        my $template = Zaaksysteem::ZTT::Template::OpenOffice->new(document => $thing);

        Zaaksysteem::StatsD->statsd->end("ztt_new_from_thing_openoffice", $t0);

        return $template;
    }

    throw('ztt/template/factory', sprintf(
        'Unable to figure out what the supplied template actually is and how to handle it (%s)',
        ref $thing
    ));
}

=head1 ABSTRACT METHODS

=head2 tag_selections

This method returns a list of all L<selections|Zaaksysteem::ZTT::Selection>
found in the template.

    for my $selection ($template->tag_selections) {
        # Do some processing with the data
    }

When called on instances of the L<base class|Zaaksysteem::ZTT::Template> the
method will return nothing.

=cut

sub tag_selections { }

=head2 sections

This method returns the list of all L<selections|Zaaksysteem::ZTT::Selection>
found in the template that are to be interpreted as a seperate section, which
is basically a form of subtemplating. The section is usually processed with a
different context than the parent template.

    for my $section ($template->sections) {
        # Do some processing of the subtemplate / section
    }

When called on instances of the L<base class|Zaaksysteem::ZTT::Template> the
method will return nothing.

=cut

sub sections { }

=head1 METHODS

=head2 modifications

This method will return a list of combined
L<modifications|Zaaksysteem::ZTT::Modification> objects that represent all the
operations needed to process the template into a final document.

It will B<not> try to be smart about recursive modifications that may result
from an initial processing run on the template. This means that if some
modifications result in a 'final' document that contains new tags to be
replaced this method will have to be called again to get modifications derived
from the new template.

    for my $modification ($template->modifications) {
        # Modify the template according to the modification
    }

=cut

sub modifications {
    my $self = shift;

    my @mods;

    for my $selection ($self->sections) {
        if ($selection->is_conditional && not $selection->is_iterable) {
            push @mods, Zaaksysteem::ZTT::Modification->new(
                type => 'show_when',
                selection => $selection
            );
        }

        if ($selection->is_iterable) {
            push @mods, Zaaksysteem::ZTT::Modification->new(
                type => 'iterate',
                selection => $selection
            );
        }
    }

    for my $selection ($self->tag_selections) {
        push @mods, Zaaksysteem::ZTT::Modification->new(
            type => 'replace',
            selection => $selection
        );
    }

    for my $selection ($self->inline_iterators) {
        push @mods, Zaaksysteem::ZTT::Modification->new(
            type => 'iterate_inline',
            selection => $selection
        );
    }

    return @mods;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
