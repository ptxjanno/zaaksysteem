package Zaaksysteem::ZTT::MagicDirective;

use Moose;

use Parse::RecDescent;

use BTTW::Tools;

=head1 NAME

Zaaksysteem::ZTT::MagicDirective - Parse augmented magic-string syntax

=head1 DESCRIPTION

This package abstracts the syntax used in ZTT templates to render
'magic strings'.

    my $parser = Zaaksysteem::ZTT::MagicDirective->new;

    $parser->parse('case.id');
    # {
    #   expression => \'case.id'
    # }

    $parser->parse('case.id | link_to_case');
    # {
    #   expression => \'case.id',
    #   filter => {
    #     name => 'link_to_case',
    #     args => []
    #   }
    # }

    $parser->parse('case.id | link_to(case, "Zaak %d")');
    # {
    #   expression => \'case.id',
    #   filter => {
    #     name => 'link_to',
    #     args => [ 'Zaak %d' ]
    #   }
    # }

For a better description of the return value of this call, see L</parse>.

=head1 ATTRIBUTES

=head2 parser

This attribute holds a reference to the L<Parse::RecDescent> object used to
parse directives.

=cut

has parser => (
    is => 'rw',
    isa => 'Parse::RecDescent',
    default => sub {
        # $::RD_HINT = 1;
        # $::RD_WARN = 1;
        # $::RD_TRACE = 1;

        return Parse::RecDescent->new(MAGIC_DIRECTIVE_GRAMMAR->());
    }
);

=head1 CONSTANTS

=head2 MAGIC_DIRECTIVE_GRAMMAR

This constant is the grammar that will be fed to L<Parse::RecDescent>

=cut

use constant MAGIC_DIRECTIVE_GRAMMAR => <<'EOG';
{
    # Treebuilder helper. This sub will be available only in the grammer
    # below. If someone has a non-nasty way of embedding this
    # programmatically, have at it.
    sub treeify {
        my $t = shift;

        $t = [ shift, $t, shift ] while scalar @_;

        return $t;
    }
}

script : command /\Z/ {
    $return = $item{ command };
}

command : cmd_iterate
        | cmd_show_when

cmd_iterate : /iterate/i bareword expression_block(?) {
    $return = {
        iterate => lc($item{ bareword }),
        ( $item{ 'expression_block(?)' }[0] ?
            (constraint => $item{ 'expression_block(?)' }[0]) :
            ()
        )
    };
}

cmd_show_when : /show_when/i expression_block {
    $return = {
        show_when => $item{ expression_block }
    };
}

# Main production of magic directive grammar
magic_directive : statement filter(?) /\Z/ {
    $return = {
        # Unpack matching expression, prevent needless wrapping
        %{ $item{ statement } },

        filter => $item{ 'filter(?)' }[0],
    };
}

statement : iteration_statement | expression {
    $return = { expression => $item[1] };
}

iteration_statement : /iter(?:eer|ate)\:/i bareword /\:/ bareword {
    $return = {
        # scalarref == variable subsitution, the bareword is interpreted
        # as an expression that dereferences a property of the current
        # context object (from the set referenced by iterate_context)
        expression => \lc($item[4]),
        iterate_context => lc($item[2])
    };
}

expression_block : '{' expression '}' {
    $return = { expression => $item{ expression } };
}

expression : relational_expr

relational_expr : <leftop: binary_expr cmp_op binary_expr> {
    $return = treeify(@{ $item[1] });
}

binary_expr : <leftop: term /([+-])/ term> {
    $return = treeify(@{ $item[1] });
}

term : <leftop: factor /([*\/])/ factor> {
    $return = treeify(@{ $item[1] });
}

factor : number | call | string | sub_expr | constant

factor : '[' relational_expr(s? /,/) ']' {
    $return = {
        call => 'array',
        args => $item[2]
    };
}

constant : /:[\w\.]+/ {
    $return = {
        call => 'constant',
        args => [ substr $item[1], 1 ]
    };
}

factor : bareword {
    $return = \$item[1];
}

sub_expr : '(' <commit> relational_expr ')' {
    $return = $item{ relational_expr };
}

call : bareword '(' relational_expr(s? /,/) ')' {
    $return = {
        call => lc($item[1]),
        args => $item[3]
    }
}

cmp_op : '==' | '!=' | '~=' | '>=' | '<=' | '>' | '<' | 'in'

number : /[+-]?\d+(?:\.\d+)?/

bareword : /[\w\.]+/

# A filter begins with a pipe, followed by a filtername and perhaps arguments
filter : "|" bareword filter_args(?) {
    $return = {
        name => lc($item{ bareword }),
        args => $item{ 'filter_args(?)'}[0] || []
    }
}

# Arguments appear between the '(' and ')' grouping chars, returns an
# arrayref of the arguments
filter_args : '(' arg(s? /,/) ')' {
    $return = $item[2]
}

# An argument can be bareword, or a quoted string (for space inclusion)
# Arguments are positional, no key-value-pair crud here pls
arg : bareword | string

# Non-greedily match all chars between two '"' chars, skip over escaped '\"'
# return the matched string sans quote chars
string : /"(.*?)(?<!\\)"/ {
    $return = $1
}
EOG

=head1 METHODS

=head2 parse

This method takes a string and attempts to parse the string as a magic
directive. The return-value is a hashref with the sanitized data.

    my $directive = Zaaksysteem::ZTT::MagicDirective->new->parse('...');

The returned hashref will look something like this:

    {
        expression => ...,

        # Other keys are optional
        iterate_context => '...',

        filter => {
            name => '...'
            args => [       # Args will always be strings
                '...',
                '...'
            ]
        }
    }

=head3 Expressions

Parsed expressions (1 + 1, my_attr > my_other_atter, etc) are returned in a
possibly nested set of values. Each value's type indicates it's function.
Plain strings are string constants, numbers work the same. Arrayrefs imply a
binary operation (like C<+>, C<==>, etc).

    my $expr = 1;               # Number evaluates to self
    my $expr = 'foo';           # Strings as well
    my $expr = [ '+', 1, 1 ];   # Becomes 2
    my $expr = \'bar';          # Evaluates to the attribute 'bar'

=cut

sub parse {
    my $self = shift;
    my $text = shift;

    my $data;

    # No need to catch, on failure return implicit undef.
    try {
        $data = $self->parser->magic_directive($text);
    };

    # Early silent return on parse-errors
    unless (defined $data && $data->{ expression }) {
        return;
    }

    # Strip empty fields for API compliance
    for my $key (keys %{ $data }) {
        delete $data->{ $key } unless $data->{ $key };
    }

    return $data;
}

=head2 deparse

This method takes a parsed expression and deparses it into a string.

The produced string will be functionally equivalent to the original
expression, but the two expression strings are most likely not equivalent.

Binary operations (+, ==, in, etc) will be wrapped in parenthesis and
syntactic sugar will be lost. For example, literal arrays in source text
will be converted to calls to the 'array' constructor function).

    my $parser = ...;

    my $expression = $parser->parse('1 + 1');
    my $deparse = $parser->deparse($expression->{ expression });

    assert $deparse eq '(1 + 1)';

Optionally, this method can re-sugar the expression when the sweetner flag is
passed.

    my $expression = $parser->parse(':my_constant');
    my $unsweetened = $parser->deparse($expression->{ expression });

    assert $unsweetened eq 'constant("my_constant")';

    my $sweetened = $paser->deparse($expression->{ expression }, 1);

    assert $sweetened eq ':my_constant';

=cut

sub deparse {
    my $self = shift;
    my $expression = shift;
    my $sweetner = shift;

    my $reftype = ref $expression;

    unless ($reftype) {
        return sprintf('"%s"', $expression);
    }

    if ($reftype eq 'SCALAR') {
        return ${ $expression };
    }

    if ($reftype eq 'ARRAY') {
        my ($op, $x, $y) = @{ $expression };

        return sprintf(
            '(%s %s %s)',
            $self->deparse($x, $sweetner),
            $op,
            $self->deparse($y, $sweetner)
        );
    }

    if ($reftype eq 'HASH') {
        if ($sweetner && $expression->{ call } eq 'constant') {
            return sprintf(':%s', $expression->{ args }[0]);
        }

        my $arglist = join ', ', map {
            $self->deparse($_, $sweetner)
        } @{ $expression->{ args } };

        if ($sweetner && $expression->{ call } eq 'array') {
            return sprintf('[ %s ]', $arglist);
        }

        return sprintf('%s(%s)', $expression->{ call }, $arglist);
    }
}

=head2 parse_script

This method takes a "zttscript" string and produces an array of script
directives.

    my @directives = Zaaksysteem::ZTT::MagicDirective->new->parse('...');

Currently, two directives are supported, C<show_when> and C<iterate>.

Input text is split by newline after which each line is individually parsed.

=head3 show_when

The C<show_when> directive is used to make sections in (ODT) documents
conditionally visible. The general structure of the directive is

    show_when { <condition> }

Where a C<condition> is any MagicDirective expression. The condition is met
when the expression has a true-ish value (a number, a non-empty string, etc).

=head3 iterate

C<iterate> is used when a section is meant to be repeated a number of times
within a set context. The set context is specified by the directive's only
argument;

    iterate zaak_relaties

=cut

sub parse_script {
    my $self = shift;
    my $text = shift;

    my @productions;

    for my $line (split m[\n], $text) {
        push @productions, try {
            return $self->parser->script($line);
        } catch {
            return {
                error => sprintf('Could not parse line "%s"', $line)
            }
        }
    }

    return @productions;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 KNOWN BUGS

May summon the Dark Lord, Cthulhu

     Ph'nglui mglw'nafh Cthulhu R'lyeh wgah'nagl fhtagn

                            .....
                       .d$$$$*$$$$$$bc
                    .d$P"    d$$    "*$$.
                   d$"      4$"$$      "$$.
                 4$P        $F ^$F       "$c
                z$%        d$   3$        ^$L
               4$$$$$$$$$$$$$$$$$$$$$$$$$$$$$F
               $$$F"""""""$F""""""$F"""""C$$*$
              .$%"$$e    d$       3$   z$$"  $F
              4$    *$$.4$"        $$d$P"    $$
              4$      ^*$$.       .d$F       $$
              4$       d$"$$c   z$$"3$       $F
               $L     4$"  ^*$$$P"   $$     4$"
               3$     $F   .d$P$$e   ^$F    $P
                $$   d$  .$$"    "$$c 3$   d$
                 *$.4$"z$$"        ^*$$$$ $$
                  "$$$$P"             "$$$P
                    *$b.             .d$P"
                      "$$$ec.....ze$$$"
                          "**$$$**""

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
