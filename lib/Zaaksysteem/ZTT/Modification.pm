package Zaaksysteem::ZTT::Modification;

use Moose;

=head1 NAME

Zaaksysteem::ZTT::Modification - Abstraction for modifications on templates

=head1 DESCRIPTION

This package abstracts the concept of a modification required to process a
template into a final document

=head1 ATTRIBUTES

=head2 type

=cut

has type => (
    is => 'ro',
    isa => 'Str',
    required => 1
);

=head2 selection

=cut

has selection => (
    is => 'ro',
    isa => 'Zaaksysteem::ZTT::Selection',
    required => 1
);

=head2 data

=cut

has data => (
    is => 'rw',
    isa => 'HashRef'
);

=head1 METHODS

=head2 as_string

This method returns a stringified representation of the modification object.
Most useful in debug or logging contexts.

    warn $modification->as_string;

=cut

sub as_string {
    my $self = shift;

    return sprintf(
        "at %s, %s %s",
        $self->selection->as_string,
        $self->type,
        'data'
    );
}

__PACKAGE__->meta->make_immutable;
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
