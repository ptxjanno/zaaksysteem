package Zaaksysteem::External::KvKAPI;
use Moose;
use namespace::autoclean;

=head1 NAME

Zaaksysteem::External::KvKAPI - KvKAPI model for Zaaksysteem

=head1 DESCRIPTION

Glue layer for KvKAPI calls in Zaaksysteem. This module calls the KvK API and
allows you to transform the data from ZS syntax to KvK and transforms KvK
objects to ZS objects.

=head1 SYNOPSIS

    use Zaaksysteem::External::KvKAPI;

    my $api = Zaaksysteem::External::KvKAPI->new(
        api_key => 'foo',
    );

    $api->search;
    $api->search_all;
    $api->search_max;

    $api->api_call;

=cut

use BTTW::Tools;
use WebService::KvKAPI;
use WebService::KvKAPI::Spoof;

use Zaaksysteem::Object::Types::Address;
use Zaaksysteem::Object::Types::Company;
use Zaaksysteem::Object::Types::CountryCode;
use Zaaksysteem::Object::Types::LegalEntityType;
use Zaaksysteem::Object::Types::Subject;
use Zaaksysteem::Object::Types::ExternalSubscription;

with 'MooseX::Log::Log4perl';

=head1 ATTRIBUTES

=head2 api_key

The API key of the KvK API

=cut

has api_key => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head2 spoofmode

Boolean value indicating that the calls should go against the test api
server at KvK.

=cut

has spoofmode => (
    is       => 'ro',
    isa      => 'Bool',
    default  => 0,
);

=head2 api_host

String value indicating the host to be used explicitly, instead of relying
on the host specified in the OpenAPI specification.

=cut

has api_host => (
    is => 'ro',
    isa => 'Str',
    predicate => 'has_api_host'
);

=head2 api

The API object, should be a L<WebService::KvKAPI> kinda object. You can only
set this via the constructor.

It delegates the following actions: C<search>, C<search_all>, C<search_max> and
C<profile>, so you can call C<< $self->search> >>. For more information about
the methods see L<WebService::KvKAPI>.

=cut

has _api => (
    is      => 'ro',
    isa     => 'WebService::KvKAPI',
    builder => '_build_kvk_api',
    lazy    => 1,
    handles => [qw(search search_all search_max profile api_call)],
    init_arg => 'api',
);

=head2 query_map

Map L<Zaaksysteem::Object::Types::Subject> attribute names to KvK API query
parameters. You can override the default values via the constructor.

Defaults to:

    {
        coc_number                          => 'kvkNumber',
        coc_location_number                 => 'branchNumber',
        company                             => 'tradeName',
        rsin                                => 'rsin',

        'address_residence.street'          => 'street',
        'address_residence.street_number'   => 'houseNumber',
        'address_residence.zipcode'         => 'postalCode',
        'address_residence.city'            => 'city',
    }

=cut

has _query_map => (
    is => 'ro',
    isa => 'HashRef',
    lazy => 1,
    default => sub {
        return {
            coc_number                          => 'kvkNumber',
            coc_location_number                 => 'branchNumber',
            company                             => 'tradeName',
            rsin                                => 'rsin',

            'address_residence.street'          => 'street',
            'address_residence.street_number'   => 'houseNumber',
            'address_residence.zipcode'         => 'postalCode',
            'address_residence.city'            => 'city',
        };
    },
    traits => ['Hash'],
    handles => {
        exists_query_key => 'exists',
        get_query_key    => 'get',
    },
    init_arg => 'query_map',
);

=head1 METHODS

Implements (almost) all methods defined by L<WebService::KvKAPI>.

=head2 exists_query_key

Check if a query key can be mapped to a KvK key

=head2 get_query_key

Get the KvK query key based on the ZS key

=cut

sub _build_kvk_api {
    my $self = shift;

    if ($self->spoofmode) {
        return WebService::KvKAPI::Spoof->new(
            api_key => $self->api_key
        );
    }

    my %kvk_args = (
        api_key => $self->api_key
    );

    if ($self->has_api_host) {
        $kvk_args{ api_host } = $self->api_host;
    }

    return WebService::KvKAPI->new(%kvk_args);
}

=head2 map_params_to_kvk_query

Map the ZS internal Zaaksysteem subject fields to a KvK query

=cut

sig map_params_to_kvk_query => 'HashRef';

sub map_params_to_kvk_query {
    my ($self, $params) = @_;

    my %kvk_query;
    foreach (keys %$params) {
        next unless $self->exists_query_key($_);
        $kvk_query{$self->get_query_key($_)} = $params->{$_};
    }

    if (exists $kvk_query{kvkNumber}) {
        $kvk_query{kvkNumber} = sprintf('%08d', $kvk_query{kvkNumber});
    }
    if (exists $kvk_query{branchNumber}) {
        $kvk_query{branchNumber} = sprintf('%012d', $kvk_query{branchNumber});
    }

    return \%kvk_query if keys %kvk_query;

    throw('kvk/params/mapping', 'No KvK query can be build!');
}

=head2 map_kvk_company_to_subject

Map the output of the KvK API to a customer

=cut

sig map_kvk_company_to_subject =>
    'HashRef => Zaaksysteem::Object::Types::Subject';

sub map_kvk_company_to_subject {
    my ($self, $company) = @_;

    my $address_residence = _map_kvk_address_to_subject_address(
        _get_kvk_address_residence($company)
    );

    my $address_correspondence = _map_kvk_address_to_subject_address(
        _get_kvk_address_correspondence($company)
    );

    my $company_type = try {
        Zaaksysteem::Object::Types::LegalEntityType->new_from_label(
            $company->{legalForm} // ''
        )
    }
    catch {
        $self->log->info($_);
        return;
    };

    my $subject = Zaaksysteem::Object::Types::Company->new(
        company => $company->{tradeNames}{businessName}
            // $company->{tradeNames}{shortBusinessName},

        coc_number => $company->{kvkNumber},

        $company->{branchNumber} ?
            (coc_location_number => $company->{branchNumber})
            : (),

        $company->{rsin}   ? (rsin => $company->{rsin}) : (),

        $company_type      ? (company_type => $company_type)
            : (),

        $address_residence ? (address_residence => $address_residence)
            : (),

        $address_correspondence ?
            (address_correspondence => $address_correspondence)
            : (),
    );

    return Zaaksysteem::Object::Types::Subject->new(
        subject_type          => 'company',
        subject               => $subject,

        external_subscription =>
            Zaaksysteem::Object::Types::ExternalSubscription->new(
                external_identifier => _generate_unique_identifier($company)
            )
    );

}

sub _generate_unique_identifier {
    my $company = shift;

    return join( '/' =>
        $company->{kvkNumber},
        $company->{branchNumber} // '',
        $company->{rsin} // '',
    );
}

sub _get_kvk_address_residence {
    return _get_kvk_address_by_type($_[0], 'vestigingsadres', 'bezoekadres');
}

sub _get_kvk_address_correspondence {
    return _get_kvk_address_by_type($_[0], 'correspondentieadres');
}

sub _get_kvk_address_by_type {
    my ($kvk_company, @types) = @_;

    foreach my $type (@types) {
        foreach my $address (@{$kvk_company->{addresses}}) {
            return $address if $address->{type} eq $type;
        }
    }
    return;
}

sub _map_kvk_address_to_subject_address {
    my $kvk_address = shift;

    return unless $kvk_address;

    my $country = Zaaksysteem::Object::Types::CountryCode->new_from_name(
        $kvk_address->{country});

    if ($country->dutch_code eq '6030') {
        return Zaaksysteem::Object::Types::Address->new(
            street               => $kvk_address->{street},
            street_number        => $kvk_address->{houseNumber},
            street_number_suffix => $kvk_address->{houseNumberAddition},
            city                 => $kvk_address->{city},
            zipcode              => $kvk_address->{postalCode},
            country              => $country,
        );
    }
    else {
        # TODO: Look on the CPAN for address mangling modules
        # for now accept dutchy style notations
        return Zaaksysteem::Object::Types::Address->new(
            foreign_address_line1 => join(" ",
                $kvk_address->{street},
                $kvk_address->{houseNumber},
                $kvk_address->{houseNumberAddition}),
            foreign_address_line2 =>
                join(" ", $kvk_address->{postalCode}, $kvk_address->{city}),
            country => $country,
        );
    }
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a
look at the L<LICENSE|Zaaksysteem::LICENSE> file.
