package Zaaksysteem::Roles::Password;
use Moose::Role;
use namespace::autoclean;

use Authen::Passphrase;
use Authen::Passphrase::BlowfishCrypt;
use Digest::SHA qw(sha512);

=head1 NAME

Zaaksysteem::Roles::Password - A role that checks and updates password

=cut

=head2 update_password

Update the password of a user entity

=cut

sub update_password {
    my ($self, $pass) = @_;

    $self->update({password => $self->crypt_password($pass)});
    return 1;
}

=head2 crypt_password

Crypt the password of the user. The password is hashed before we crypt it

=cut

sub crypt_password {
    my ($self, $pass) = @_;
    my $ppr = Authen::Passphrase::BlowfishCrypt->new(
        cost        => 8,
        salt_random => 1,
        passphrase  => $self->_sha_pass($pass),
    );
    return $ppr->as_rfc2307;
}

=head2 check_password

Check if the password matches. In case we receive non-migrated passwords we
crypt them and store them in the DB

=cut

sub check_password {
    my ($self, $pass) = @_;

    my $ppr = Authen::Passphrase->from_rfc2307($self->password);
    return 1 if $ppr->match($self->_sha_pass($pass));
    return 1 if $self->_check_legacy_password($pass);
    return 0;
}

sub _check_legacy_password {
    my ($self, $password) = @_;
    my $ppr = Authen::Passphrase->from_rfc2307($self->password);
    return 0 if !$ppr->match($password);
    $self->update_password($password);
    return 1;
}

sub _sha_pass {
    my ($self, $pass) = @_;
    return sha512($pass);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019 Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

