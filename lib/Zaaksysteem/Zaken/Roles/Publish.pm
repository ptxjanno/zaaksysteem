package Zaaksysteem::Zaken::Roles::Publish;

use Moose::Role;

use Hash::Merge::Simple qw( clone_merge );
use Archive::Zip;
use File::stat;
use XML::Simple qw(:strict);
use Encode;
use XML::LibXML;

use Data::Dumper; $Data::Dumper::Sortkeys = 1;

use Zaaksysteem::Constants;
use BTTW::Tools;

=head1 METHODS

=head2 publish

Return value: file reference

    my $file_reference = $zaak->publish();

=cut

define_profile export_vergaderingen_ede => (
    required => [qw[root_dir publish_dir published_file_ids published_related_ids]]
);

sub export_vergaderingen_ede {
    my $self        = shift;

    my $args = assert_profile(shift || {})->valid;

    my $publish_dir = $args->{ publish_dir };
    my $root_dir    = $args->{ root_dir };

    my $logging = '';

    # create publish dir
    unless(-d $publish_dir) {
        mkdir $publish_dir
            or die "could not create $publish_dir: $!\n";
        $logging .= "Created export directory: $publish_dir.\n";
    }

    my $cases = $args->{ published_related_ids };

    foreach my $relation_view ($self->zaak_relaties) {
        next unless $relation_view->type eq 'related';

        my $related_case = $relation_view->case;

        next unless $cases->{ $related_case->id };

        $logging .= "Related case: " . $related_case->id . "\n";
    }

    $logging .= $self->_export_files_as_pdf({
        published_file_ids => $args->{ published_file_ids },
        publish_dir => $args->{ publish_dir }
    });

    my $xmlStore = {};
    for my $publish ( 0, 1 ) {
        my $document = $self->ede_vergadering_document({
            publish => $publish,
            files => $args->{ published_file_ids },
            cases => $cases
        });

        # write case xml to disk (zero-padded case id + publish / unpublish bit)
        my $number = sprintf("%023d%d", $self->id, $publish);
        my $filename = sprintf('%s.xml', join('/', $publish_dir, $number));

        $logging .= "-------------- XML -------------- \n" . $document->toString(1);

        $logging .= "Writing to $filename.\n";


        $document->toFile($filename, 2); # Write to file pretty printed

        $xmlStore->{$publish ? 'publish' : 'unpublish'} = $document->toString(1);
    }

    $self->add_log_entry($xmlStore);

    return $logging;
}

sub add_log_entry {
    my ($self, $xmlStore) = @_;

    $self->trigger_logging('case/publish', {
        component => 'case',
        component_id  => $self->id,
        zaak_id       => $self->id,
        data          => $xmlStore
    });
}


=head2 published

Searches in logging to figure out the last date this case was published.
If no logging is found, return undef.

=cut

sub published {
    my ($self) = @_;

    my $rs = $self->result_source->schema->resultset('Logging');

    my $most_recent = $rs->search({
        event_type => 'case/publish',
        zaak_id => $self->id
    }, {
        order_by => { '-desc' => 'id' }
    })->first;

    return $most_recent && $most_recent
            ->created
            ->set_time_zone('UTC')
            ->set_time_zone('Europe/Amsterdam')
            ->set_locale('nl_NL')
            ->strftime("%d %B %Y %H:%M:%S");
}



define_profile ede_vergadering_document => (
    required => [qw[publish files cases]],
    typed => {
        files => 'HashRef',
        cases => 'HashRef'
    },
    field_filters => {
        publish => sub { shift() ? 1 : 0 },
    },
);

sub ede_vergadering_document {
    my $self = shift;

    my $args = assert_profile(shift || {})->valid;

    my $doc = XML::LibXML::Document->new('1.0', 'utf-8');
    my $root = $doc->createElement($args->{ publish } ? 'publish' : 'unpublish');

    my $meeting = $doc->createElement('Vergaderingen_Bestuurlijk');
    $meeting->setAttribute('id', $self->uuid);
    $meeting->setAttribute('access', 'public');

    if($args->{ publish }) {
        my $date = $doc->createElement('datum');
        $date->setAttribute('displayValue', $self->value_by_magic_string({
            magic_string => 'vergader_datum',
            required => 1
        }));

        my $time = $doc->createElement('tijdstip');
        $time->appendTextNode($self->value_by_magic_string({
            magic_string => 'vergader_tijdstip',
            required => 1
        }));

        my $location = $doc->createElement('locatie');
        $location->appendTextNode($self->value_by_magic_string({
            magic_string => 'vergader_lokatie',
            required => 1
        }));

        my $meeting_class = $doc->createElement('vergaderklasse');
        $meeting_class->setAttribute('access', 'public');
        $meeting_class->appendTextNode($self->value_by_magic_string({
            magic_string => 'vergader_klasse',
            required => 1
        }));

        $meeting->appendChild($date);
        $meeting->appendChild($time);
        $meeting->appendChild($location);
        $meeting->appendChild($meeting_class);
    }

    my $prop_and_agenda = $doc->createElement('Behandelvoorstel_en_Agendapunten');
    my $orderNr = 0;

    my $meeting_type = $self->value_by_magic_string({
        magic_string => 'vergader_klasse',
        required => 1
    });

    my @hardcoded_items;

    if(exists EDE_PUBLICATION_STRINGS->{ $meeting_type }) {
        @hardcoded_items = @{ EDE_PUBLICATION_STRINGS->{ $meeting_type } };
    }

    for my $item (@hardcoded_items) {
        $self->ede_vergadering_pna($args->{ publish }, $doc, $prop_and_agenda, $orderNr++, {
            subject => $item,
            id => sprintf('%d_%s', $self->id, $item),
            vergadering_case_id => $self->id,
            type => 'Hardcoded link'
        });
    }

    for my $relation ($self->zaak_relaties) {
        my $case = $relation->case;

        next unless exists $args->{ cases }->{ $case->id };

        # scary, here we're jumping to another object, hard to spot.
        $case->ede_vergadering_pna($args->{ publish }, $doc, $prop_and_agenda, $orderNr++, {
            subject => $case->value_by_magic_string({
                magic_string => 'bbv_onderwerp',
                required => 1
            }),

            id => $relation->relation->uuid,
            case_id => $case->id,
            vergadering_case_id => $self->id,
            type => 'Behandelvoorstel',
            files => $args->{ files }
        });
    }

    $meeting->appendChild($prop_and_agenda);

    $root->appendChild($meeting);
    $doc->setDocumentElement($root);

    return $doc; # Return pretty-printed
}

define_profile ede_vergadering_pna => (
    required => [qw[subject id type vergadering_case_id]],
    optional => [qw[files case_id]],
    defaults => {
        files => []
    }
);

sub ede_vergadering_pna {
    my $self = shift;
    my $publish = shift;
    my $doc = shift;  # xml document
    my $pnas = shift; # proposition and agenda container
    my $orderNr = shift;

    my $args = assert_profile(shift || {})->valid;

    my $pna = $doc->createElement('Behandelvoorstel_en_Agendapunten');

    $pna->setAttribute('oId', $args->{ id });
    $pna->setAttribute('orderNr', $orderNr);

    my $registration_date = $doc->createElement('regdatum');
    $registration_date->setAttribute('displayValue', $self->registratiedatum);

    my $registratiekenmerk = $doc->createElement('registratiekenmerk');
    $registratiekenmerk->appendTextNode($args->{case_id} || $args->{ id });

    my $subject = $doc->createElement('onderwerp');
    $subject->appendTextNode($args->{ subject });

    my $id = $doc->createElement('EDMSdocmanid');
    $id->appendTextNode($args->{ id });

    $pna->appendChild($registratiekenmerk);
    $pna->appendChild($registration_date);
    $pna->appendChild($subject);
    $pna->appendChild($id);

    if($args->{ type } eq 'Behandelvoorstel') {
        my $pubdoc = $doc->createElement('document');

        my @pubdocs = grep { $_->has_label('publicatiedocument') } $self->files;

        my $pubdoc_file;

        for my $file (@pubdocs) {
            next if $file->date_deleted || $file->destroyed || !$file->accepted;
            next unless ($file->version == $file->get_last_version->version);

            $pubdoc_file = $file;
            last;
        }

        $pubdoc->setAttribute('file', $args->{ vergadering_case_id } . '-' . sprintf('%s.pdf', $pubdoc_file->filestore_id->uuid));

        my $type = $doc->createElement('SoortAgendaPunt');
        $type->setAttribute('displayValue', $args->{ type });

        $pna->appendChild($pubdoc);
        $pna->appendChild($type);

        my $keyword_value = $self->value_by_magic_string({
            magic_string => 'behandelvoorstel_trefwoord'
        });

        if($keyword_value) {
            my $keyword = $doc->createElement('trefwoord');

            $keyword->appendTextNode($keyword_value);
            $pna->appendChild($keyword);
        }
    }

    my $attachments = $doc->createElement('Bijlagen');

    for my $file ($self->files->all) {
        my $file_id_combi = $args->{ vergadering_case_id } . '-' . $file->id;
        next unless (ref $args->{ files } eq 'HASH' && exists $args->{ files }->{ $file_id_combi });
        next if $file->date_deleted;
        next if grep { $_->case_document->magic_string eq 'publicatiedocument' } $file->case_documents;

        $self->ede_vergadering_doc({
            publish => $publish,
            document => $doc,
            files => $attachments,
            file => $file,
            vergadering_case_id => $args->{ vergadering_case_id }
        });
    }

    $pna->appendChild($attachments);
    $pnas->appendChild($pna);
}

define_profile ede_vergadering_doc => (
    required => [qw[publish document files file vergadering_case_id]],
    field_filters => {
        publish => sub { shift() ? 1 : 0 }
    },
    typed => {
        files => 'XML::LibXML::Element',
        document => 'XML::LibXML::Document',
        file => 'Zaaksysteem::Model::DB::File'
    }
);

sub ede_vergadering_doc {
    my $self = shift;

    my $args = assert_profile(shift || {})->valid;

    my $doc = $args->{ document };
    my $attachments = $args->{ files };
    my $file = $args->{ file };
    my $publish = $args->{ publish };
    my $filename = $args->{ vergadering_case_id } . '-'. $file->filestore_id->uuid;

    my $docnode = $doc->createElement('Documenten');
    $docnode->setAttribute('oId', $filename);
    $docnode->setAttribute('access', 'internal');

    if($publish) {
        my $ref = $doc->createElement('document');
        $ref->setAttribute('file', sprintf('%s.pdf', $filename));

        my $id = $doc->createElement('EDMSdocmanid');
        $id->appendTextNode($filename);

        my $subject = $doc->createElement('onderwerp');
        $subject->appendTextNode(sprintf('%s.pdf', $file->name));

        my $attribute = $doc->createElement('registratiekenmerk');
        $attribute->appendTextNode($file->id);

        $docnode->appendChild($ref);
        $docnode->appendChild($id);
        $docnode->appendChild($subject);
        $docnode->appendChild($attribute);
    }

    $attachments->appendChild($docnode);
}

sub _export_files_as_pdf {
    my ($self, $options) = @_;

    my $published_file_ids  = $options->{published_file_ids}    or die "need published_file_ids";
    my $publish_dir         = $options->{publish_dir}           or die "need publish_dir";

    my $file_model = $self->result_source->schema->resultset('File');

    my $logging = '';
    foreach my $published_file_id_combi (keys %$published_file_ids) {
        my ($vergadering_case_id, $published_file_id) = split /-/, $published_file_id_combi;
        next unless $vergadering_case_id eq $self->id;

        my $file = $file_model->find($published_file_id);
        die "File $published_file_id not found, reload page." unless $file;

        my $pdf_filename = $self->id . '-' . $file->filestore_id->uuid . '.pdf';
        my $pdf_filepath = $publish_dir .'/'. $pdf_filename;

       $logging .= "Generating PDF for published file $published_file_id: " . $file->name . " to '$pdf_filepath'.\n";

        my $convert_filepath = $file->filestore_id->convert({
            target_format   => 'application/pdf',
            target_file     => $pdf_filepath,
        });

        # this is the case with pdf - already a file on disk. we can just use that one.
        # to include it in the export however, it need to go in to the temporary
        # upload directory.
        if($convert_filepath ne $pdf_filepath) {
            system("cp $convert_filepath $pdf_filepath");
        }
    }
    return $logging;
}


sub value_by_magic_string {
    my ($self, $opts) = @_;

    my $magic_string = $opts->{magic_string} or die "need magic string";
    $magic_string    = lc($magic_string);

    my $required     = $opts->{required};

    my $CACHE_KEY    = '__values_by_magic_string_cached';

    unless($self->{$CACHE_KEY}) {

        my $field_values = $self->field_values;

        my $fields = $self->zaaktype_node_id->zaaktype_kenmerken->search(
            { bibliotheek_kenmerken_id => { -not => undef } },
            { prefetch => 'bibliotheek_kenmerken_id' }
        );

        unless($opts->{ plain }) {
            $fields = $fields->search({ publish_public => 1 });
        }

        my $map = {};

        while(my $row = $fields->next()) {
            next unless $row->bibliotheek_kenmerken_id;

            my $bibliotheek_kenmerken_id    = $row->bibliotheek_kenmerken_id->id;
            my $magic_string                = $row->bibliotheek_kenmerken_id->magic_string;

            my $value = $field_values->{$bibliotheek_kenmerken_id};

            if($row->bibliotheek_kenmerken_id->value_type eq 'file') {
                my $fcd = $self->result_source->schema->resultset('FileCaseDocument')->search({
                    case_document_id => $row->id
                })->first;

                next unless $fcd;

                $value = $fcd->get_column('file_id');
            } elsif(ref $value && ref $value eq 'ARRAY') {
                $value = join ",", @$value;
            }

            $map->{$magic_string} = $value || '';

        }

        $self->{$CACHE_KEY} = $map;
    }

    if(exists $self->{$CACHE_KEY}->{$magic_string}) {
        return $self->{$CACHE_KEY}->{$magic_string};
    }

    # else
    my $warning = "Het veld '$magic_string' is niet gedefinieerd voor zaak " .
        $self->id .
        ". Controleer het zaaktype. Klopt de magic string, en staat 'Publieke website' aangevinkt?\n";

    if($required) {
        die $warning;
    }
    #warn $warning;
    return '';
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 EDE_PUBLICATION_STRINGS

TODO: Fix the POD

=cut

=head2 add_log_entry

TODO: Fix the POD

=cut

=head2 ede_vergadering_doc

TODO: Fix the POD

=cut

=head2 ede_vergadering_document

TODO: Fix the POD

=cut

=head2 ede_vergadering_pna

TODO: Fix the POD

=cut

=head2 export_vergaderingen_ede

TODO: Fix the POD

=cut

=head2 value_by_magic_string

TODO: Fix the POD

=cut

