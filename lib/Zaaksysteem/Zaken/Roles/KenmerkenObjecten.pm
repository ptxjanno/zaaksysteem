package Zaaksysteem::Zaken::Roles::KenmerkenObjecten;
use Moose::Role;

use Zaaksysteem::Constants;
use Zaaksysteem::Zaken::AdvanceResult;
use BTTW::Tools;

with 'MooseX::Log::Log4perl';

sub _get_referential_attribute_value {
    my ($self, $attribute, $params) = @_;

    my $case_id = $self->id;

    # if the field is referential, it is taken from the parent.
    # potential recursion hazard - keep track of visited cases.

    # TODO performance hazard - rewrite this sub:
    # - first get zaaktype_kenmerken
    # - limit on phase
    # - use output to get values

    # TODO make bibliotheek_kenmerken_id unique per zaaktype_node_id, or allow multiple instances

    if(exists $params->{recursion_hazard}->{ $case_id }) {
        die "recursion hazard, quitting";
    }

    my $recursion_protection = $params->{recursion_protection} || {};
    $recursion_protection->{ $case_id } = 1;

    my $pid_field_values = $self->pid->field_values({
        recursion_protection => $recursion_protection
    });
    return $pid_field_values->{$attribute->get_column('bibliotheek_kenmerken_id')};
}

{

=head2 field_values

get values of zaak_kenmerken (case fields)

bibliotheek_kenmerken_id:  limit the results on a single field.

=cut

    Params::Profile->register_profile(
        method  => 'field_values',
        profile => {
            optional => [ qw/
                bibliotheek_kenmerken_id
                fase
            / ],
        }
    );
    sub field_values {
        my $self = shift;
        my $params = shift || {};

        my $request = { %$params, zaak_id => $self->id };
        my $cache_key = 'field_values?' . join "=", map { $_ . '=' . $request->{$_} } sort keys %$request;
        if (my $cached = $self->{cached_field_values}->{$cache_key}) {
            return $cached;
        }

        $params ||= {};
        my $dv = Params::Profile->check(params  => $params);
        die "invalid options for field_values" unless $dv->success;

        my $where = {};
        if(my $id = $dv->valid('bibliotheek_kenmerken_id')) {
            $where->{bibliotheek_kenmerken_id} = $id;
        }

        my $options = {
            prefetch        => [
                'bibliotheek_kenmerken_id',
            ],
            order_by        => { '-asc'   => 'me.id' },
        };

        if($params->{fase}) {
            $where->{bibliotheek_kenmerken_id} = $self->_get_phase_field_ids({
                fase => $params->{fase},
            });
        }

        # Skip other items without bibliotheek_kenmerken_id (text blocks)
        if (!exists $where->{bibliotheek_kenmerken_id}) {
            $where->{bibliotheek_kenmerken_id} = { '!=' => undef };
        }

        my @exclude_bibliotheek_attributes;
        my %kenmerk_values;

        if ($self->get_column('pid')) {
            my %prevent_collisions;

            my %where = %{$where};

            # Skip groups
            $where{is_group}    = 0;
            $where{referential} = 1;

            my $ztk = $self->zaaktype_node_id->zaaktype_kenmerken->search(
                \%where,
                {
                    columns  => [qw[bibliotheek_kenmerken_id referential]],
                    distinct => 1
                }
                );

            while (my $zaaktype_kenmerk = $ztk->next) {
                my $bibid = $zaaktype_kenmerk->get_column(
                    'bibliotheek_kenmerken_id');
                push(@exclude_bibliotheek_attributes, $bibid);

                # If a value has already been set, skip the current iteration
                # over the zaaktype_kenmerken. Doubled zaaktype_kenmerken still
                # creep into this loop, because of the required (see below)
                # 'referential' column (see the distinct query above).
                next if exists $kenmerk_values{$bibid};
                $kenmerk_values{$bibid}
                    = $self->_get_referential_attribute_value(
                    $zaaktype_kenmerk, $params);
            }
        }

        my $kenmerken = $self->zaak_kenmerken->search($where, $options);
        if (@exclude_bibliotheek_attributes) {
            $kenmerken = $kenmerken->search_rs({
                bibliotheek_kenmerken_id => { -not_in => \@exclude_bibliotheek_attributes },
            });
        }

        while (my $kenmerk = $kenmerken->next) {
            $kenmerk_values{$kenmerk->get_column('bibliotheek_kenmerken_id')} = $kenmerk->value;
        }

        $self->{cached_field_values}->{$cache_key} = \%kenmerk_values;
        return \%kenmerk_values;
    }
}


#
# to limit the retrieve value to only the values of a given phase, an in-query
# is generated that select that ids of the fields for the phase.
#
sub _get_phase_field_ids {
    my ($self, $params) = @_;

    my $first_status = $self->zaaktype_node_id->zaaktype_statussen->search(
        { status => $params->{fase}, }
    )->first;

    if (!$first_status) {
        return;
    }

    my $zaaktype_kenmerken_rs = $self->zaaktype_node_id->zaaktype_kenmerken->search(
        { zaak_status_id => $first_status->id, },
        {
            prefetch => ['bibliotheek_kenmerken_id', 'zaak_status_id'],
            order_by => 'me.id'
        }
    );

    return {
        -in => $zaaktype_kenmerken_rs->get_column('bibliotheek_kenmerken_id')
            ->as_query };
}


sub required_fields_missing {
    my ($self) = @_;

    ### Check if every kenmerk is filled in this phases and the phases before
    my $goto_status = $self->volgende_fase ?
        $self->volgende_fase->status :
        $self->huidige_fase->status;

    my $statusses = $self->zaaktype_node_id->zaaktype_statussen->search({
        status => { '<=' => $goto_status }
    }, {
        order_by => { '-asc' => 'id' }
    });

    while (my $status = $statusses->next) {
        my $rules_result = $self->phase_fields_complete({
            phase => $status
        });

        # as soon as we find a missing field we're done. outta here.
        if ($rules_result->{required}) {
            return $rules_result->{missing};
        }
    }

    return;
}


=pod

when aanvrager logs in on pip and issues a change request, a scheduled_job
records is created. the behandelaar needs to handle this request before
proceeding to the next phase.

=cut

sub unaccepted_pip_updates {
    my $self   = shift;
    my $rs     = $self->result_source->schema->resultset('ScheduledJobs');

    # a group by query would be more elegant but that is unwieldy since
    # the bibliotheek_kenmerken_id is in a JSON column
    my @rows   = $rs->search_update_field_tasks({case_id => $self->id})->all;
    my %unique = map { $_->parameters->{bibliotheek_kenmerken_id} => 1 } @rows;

    return scalar keys %unique;
}

sub unread_communication_count {
    my $self = shift;

    my $unread_count = $self->threads->search(
        {},
        {
            select => [ { sum => 'unread_employee_count' } ],
            as => [ 'unread_count' ],
        }
    )->first->get_column('unread_count') // 0;

    return $unread_count;
}

around can_volgende_fase => sub {
    my $orig    = shift;
    my $self    = shift;

    my $advance_result = $self->$orig(@_);

    if (my $fields = $self->required_fields_missing) {
        $advance_result->fail('fields_complete');
        $advance_result->missing($fields);
    } else {
        $advance_result->ok('fields_complete');
    }

    if ($self->unaccepted_pip_updates) {
        $advance_result->fail('pip_updates_complete');
    } else {
        $advance_result->ok('pip_updates_complete');
    }

    return $advance_result;
};


=head2 file_field_documents

fields are retrieved using 'field_value', except files, which are
stored somewhere else. this is taken from the template and perlified.
If we are a child, we try to lookup referential documents.

=cut

sub file_field_documents {
    my ($self, $bibliotheek_kenmerken_id) = @_;

    my $files = $self->_get_case_documents(library_id => $bibliotheek_kenmerken_id);

    if ($self->pid) {
        push(
            @$files,
            @{
                $self->_get_case_documents(
                    case       => $self->pid,
                    library_id => $bibliotheek_kenmerken_id
                )
            }
        );
    }

    return $files;
}

=head2 _get_case_documents

    $self->_get_case_documents(
        case       => $case,        # optional
        library_id => $libary_id    #required
    );

Get case documents based on a library ID.

=cut

sub _get_case_documents {
    my $self = shift;
    my %params = @_;

    my $case = $params{case} // $self;

    # ouch, it turns out that people really want to use a field more than once in a casetype.
    # they really, really want it. files are linked with the case to their zaaktype_kenmerken_id
    # (did I mention we need to rectify some semantics :) aka case_document_ids, which means
    # that you only get the document that is linked to the specific phase we looking at. two
    # instance of a document field, and we're properly screwed.
    # dirty solution is to look for all the aliases:
    my $attributes = $case->zaaktype_node_id->zaaktype_kenmerkens->search({
        bibliotheek_kenmerken_id => $params{library_id}
    });

    # the constructive solution is to modify the table 'file_case_document' so that it will
    # use bibliotheek_kenmerken_ids instead of zaaktype_kenmerken.
    my $files = $case->files->search_by_case_document_id($attributes)->search(
        undef,
        { prefetch => 'filestore' }
    );

    my @rv = map {
        {
            filename        => $_->filename,
            file_id         => $_->id,
            mimetype        => $_->filestore->mimetype,
            accepted        => $_->accepted,
            size            => $_->filestore->size,
            md5             => $_->filestore->md5,
            uuid            => $_->filestore->uuid,
            original_name   => $_->filestore->original_name,
            thumbnail_uuid  => $_->filestore->thumbnail_uuid,
            is_archivable     => $_->filestore->is_archivable,
            confidential    => $_->confidential ? \1 : \0,
        }
    } $files->all;
    return \@rv;
}


=head2 unaccepted_file_field_documents

companion function to file_field_documents

=cut

sub unaccepted_file_field_documents {
    my ($self, $file_field_documents) = @_;

    return scalar grep { !$_->{accepted} } @$file_field_documents;
}

=head2 handle_attribute_triggers

Given an event, look up the potential triggers for it and process them.

=cut

sig handle_attribute_triggers => 'Zaaksysteem::Object::Queue::Model, Zaaksysteem::DB::Component::Logging';

sub handle_attribute_triggers {
    my ($self, $queue, $event) = @_;

    return unless $event->does(
        'Zaaksysteem::DB::Component::Logging::Case::Attribute::Update'
    );

    return unless $event->attribute->value_type eq 'geolatlon';

    my $ztk = $self->zaaktype_node_id->zaaktype_kenmerken->find(
        $event->event_data->{ casetype_attribute_id }
    );

    unless (defined $ztk) {
        $self->log->warn(sprintf(
            'Attempted to retrieve zaaktype_kenmerk row for event "%s" (%s), found nothing. Cannot process triggers for this event',
            $event->onderwerp,
            $event->id
        ));

        return;
    }

    return unless $ztk->properties->{ map_case_location };

    my ($lat, $lon) = split m[,], $event->event_data->{ attribute_value };

    my $item = $queue->create_item({
        type => 'update_case_location',
        object_id => $self->get_column('uuid'),
        label => 'Zaaklocatie instellen',
        disable_acl => 1,
        data => {
            latitude => $lat,
            longitude => $lon
        }
    });

    $queue->queue_item($item) if defined $item;

    return;
}

=head2 clear_field_values

Clearer for the field values cache

=cut

sub clear_field_values {
    my $self = shift;
    delete $self->{cached_field_values};
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 ZAAKSYSTEEM_CONSTANTS

TODO: Fix the POD

=cut

=head2 required_fields_missing

TODO: Fix the POD

=cut

=head2 unaccepted_pip_updates

TODO: Fix the POD

=cut

