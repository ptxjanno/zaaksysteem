package Zaaksysteem::Zaken::Subcase;
use Moose;
use namespace::autoclean;

use BTTW::Tools;
with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::Zaken::Subcase - A subcase model

=head1 DESCRIPTION

Play with cases and how to create subcases.
Deals primarly with PIP and how to copy data from
the originating case.

=head1 SYNOPSIS

    # TODO: Fix me

=head1 ATTRIBUTES


=head2 casetype_relation_rs

The Zaaktype Relation Resultset. Required.

=cut

has casetype_relation_rs => (
    is       => 'ro',
    isa      => 'Defined',
    required => 1,
);

=head2 casetype_rs

The Zaaktype Resultset. Required.

=cut


has casetype_rs => (
    is       => 'ro',
    isa      => 'Defined',
    required => 1,
);

=head2 context

Catalyst context. Optional

=cut

has context => (
    is       => 'ro',
    isa      => 'Defined',
    required => 0,
);

=head2 get_subcase_from_case

Get the subcase resultset from a case
This function does not assert the casetype.
You need to do that yourself.

    # Get all sub case creation options
    my $rs = $self->get_subcase_from_case($case);

    # Get only the sub case creation options for one casetype
    my $rs = $self->get_subcase_from_case($case, $casetype_id);

=cut

sig get_subcase_from_case => 'Zaaksysteem::Model::DB::Zaak, ?Int';

sub get_subcase_from_case {
    my ($self, $case, $casetype_id) = @_;

    my $node = $case->get_column('zaaktype_node_id');
    my $phases = $case->fasen->search_rs(
        {
            zaaktype_node_id => $node,
            status           => { '<=' => $case->milestone + 1 },
        }
    );

    return $self->casetype_relation_rs->search_rs(
        {
            show_in_pip      => 1,
            zaaktype_node_id => $node,
            zaaktype_status_id =>
                { in => $phases->get_column('id')->as_query },
            $casetype_id ? (relatie_zaaktype_id => $casetype_id) : (),
        }
    );
}

=head2 get_pip_subcases

Get subcases for the PIP and return them to the requestor.
Builds the URI

=cut

define_profile get_pip_subcases => (
    required => {
        case       => 'Zaaksysteem::Model::DB::Zaak',
        betrokkene => 'Zaaksysteem::Betrokkene::Object',
    },
);

sub get_pip_subcases {
    my $self   = shift;
    my $params = assert_profile({@_})->valid;

    my $case = $params->{case};
    my $rs = $self->get_subcase_from_case($case);

    # because why use a constant?
    my ($btype, $ztb_type, @subcases);
    my $type = $params->{betrokkene}->btype;

    if ($type eq 'natuurlijk_persoon') {
        $btype = 'persoon';
    }
    elsif ($type eq 'bedrijf') {
        $btype = 'organisatie';
    }

    my $uri_path = "/form/subcase/%d/%d/$btype/%s";

    while (my $s = $rs->next) {
        my $subcase = $self->_subcase_for_pip(
            btype    => $btype,
            uri      => $uri_path,
            subcase  => $s,
            case_id  => $case->id,
        );
        push(@subcases, $subcase) if $subcase;
    }
    return unless @subcases;
    return \@subcases;
}

sub _get_casetype  {
    my ($self, $id) = @_;
    my $casetype = $self->casetype_rs->search(
        {
            'me.id'                            => $id,
            'me.deleted'                       => undef,
            'zaaktype_node_id.trigger'         => ['extern', 'internextern'],
            'zaaktype_node_id.webform_toegang' => 1,
            'me.active'                        => 1,
        },
        { 'prefetch' => 'zaaktype_node_id' },
    )->first;
    return $casetype if $casetype;

    throw("zaken/subcase/casetype/invalid",
        "Casetype [$id] is not suited for subcase creation from the PIP");

}

=head2 assert_casetype

Asserts if the casetype is allowed to be created via the PIP for the subject type

    $self->assert_casetype($casetype_id, $betrokkene_type);

=cut

sig assert_casetype => 'Int, Str';

sub assert_casetype {
    my ($self, $id, $type) = @_;

    my $casetype = $self->_get_casetype($id);

    # Constants would have been really handy
    if ($type eq 'persoon') {
        $type = 'natuurlijk_persoon';
    }
    elsif ($type eq 'organisatie') {
        $type = 'niet_natuurlijk_persoon';
    }

    # Check if the betrokkene type matches the allowed requestor type
    my $found = $casetype->zaaktype_node_id->zaaktype_betrokkenen->search(
        { betrokkene_type => $type })->first;
    return $casetype if $found;
    throw("zaken/subcase/casetype/betrokkene/invalid",
        "Casetype [$id] is not suited for subcase creation from the PIP");

}

=head2 duplicate_case_data

Duplicate case values for /form concept cases

=cut

define_profile duplicate_case_data => (
    required => {
        case            => 'Zaaksysteem::Model::DB::Zaak',
        relation        => 'Zaaksysteem::Model::DB::ZaaktypeRelatie',
        betrokkene_id   => 'Str',
        betrokkene_type => 'Str',
    },
);

sub duplicate_case_data {
    my $self = shift;
    my $params = assert_profile({@_})->valid;

    my $case     = $params->{case};
    my $relation = $params->{relation};

    my $values;
    if ($relation->kopieren_kenmerken) {
        # Transform array values to sometimes array-y values
        # the old form still behaves differently
        $values = $case->field_values;
        foreach (keys %$values) {
            my $v = $values->{$_};
            if (@$v == 1) {
                $values->{$_} = $v->[-1];
            }
        }
    }

    my %zaak_create = (
        zaaktype_id        => $relation->get_column('relatie_zaaktype_id'),

        ztc_aanvrager_id   => $params->{betrokkene_id},
        ztc_aanvrager_type => $params->{betrokkene_type},

        form               => { kenmerken => $values },
        raw_kenmerken      => $values,

        milestone          => 1,
        status             => 'new',
        contactkanaal      => 'webformulier',
        aangevraagd_via    => 'webformulier',

        # Hook into zaak_create
        type_zaak          => $relation->get_column('relatie_type'),
        zaak_relatie_id    => $case->id,
    );

    if (my $copy = $relation->copy_subject_role) {
        my $roles = $relation->subject_role;
        if (@$roles) {
            $zaak_create{subject_role} = $roles;
            $zaak_create{copy_subject_role} = $copy;
        }
    }
    return \%zaak_create;
}

=head2 relate_subcase

Relate sub case to original case

=cut

define_profile relate_subcase => (
    required => {
        parent => 'Zaaksysteem::Model::DB::Zaak',
        child  => 'Zaaksysteem::Model::DB::Zaak',
        type   => 'Str',
    },
    optional => {
        required_in_phase      => 'Int',
    }
);

sub relate_subcase {
    my $self = shift;
    my $params = assert_profile({@_})->valid;

    if ($params->{type} eq 'deelzaak') {
        my %args = (
            relation_zaak_id => $params->{child}->id,
            $params->{required_in_phase}
                ? (required => $params->{required_in_phase})
                : (),
        );
        $params->{parent}->zaak_subcases->create(\%args);
    }

}

=head2 _subcase_for_pip

Returns a datasructure for subcase creation on the PIP.

    my $structure = $self->_subcase_for_pip($subcase_component);

    # The datastructure looks like this..
    {
        uri   => 'https://some.uri/foo',
        label => 'Some label',
    }

=cut

sub _subcase_for_pip {
    my ($self, %params) = @_;

    my $btype    = $params{btype};
    my $case_id  = $params{case_id};
    my $uri_path = $params{uri};

    my $casetype_id = $params{subcase}->get_column('relatie_zaaktype_id');
    my $casetype;

    try {
        $casetype = $self->assert_casetype($casetype_id, $btype);
    }
    catch {
        $self->log->info("$_");
    };
    return unless $casetype;

    return {
        uri => $self->context->uri_for(
            sprintf($uri_path, $case_id, $casetype_id, $casetype->seo_friendly_title),
        ),
        label => $params{subcase}->pip_label
    };
}


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
