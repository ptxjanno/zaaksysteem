package Zaaksysteem::Filestore::Engine::Swift;
use Moose;
use namespace::autoclean;

with qw(
    MooseX::Log::Log4perl
    Zaaksysteem::Filestore::Engine
);

use File::Temp qw(:seekable);
use List::Util qw(none);
use Net::OpenStack::Swift;
use URI::Escape qw(uri_escape);
use Zaaksysteem::StatsD;

=head1 NAME

Zaaksysteem::Filestore::Engine::Swift - OpenStack Swift file store backend

=head1 DESCRIPTION

This file store engine writes and reads files to OpenStack Swift.

=head1 ATTRIBUTES

=head2 os_username [required]

The OpenStack username to use for authentication.

Can be found as "OS_USERNAME" in the "v2" OpenStack RC file.

=cut

has os_username => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head2 os_password [required]

Password of the OpenStack account.

=cut

has os_password => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head2 os_auth_url [required]

OpenStack authentication URL. For now, only the "V2" API endpoint is supported.

Can be found as "OS_AUTH_URL" in the "v2" OpenStack RC file.

=cut

has os_auth_url => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head2 os_tenant_name [required]

The "project name" (previously called "tenant") to use for OpenStack
authentication.

Can be found as "OS_TENANT_NAME" in the "v2" OpenStack RC file.

=cut

has os_tenant_name => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head2 os_timeout [optional]

The timeout which is used to determine how long an upload may take.

=cut

has os_timeout => (
    is       => 'ro',
    isa      => 'Int',
    default  => 30,
);

=head2 storage_bucket [required]

Bucket to use for the currently active customer.

=cut

has storage_bucket => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head2 swift

A L<Net::OpenStack::Swift> instance to use. Supplying this is not required, as it's
built lazily from the configuration attributes as needed.

=cut

has swift => (
    is      => 'ro',
    isa     => 'Net::OpenStack::Swift',
    lazy    => 1,
    builder => '_build_swift',
);

=head1 METHODS

=head2 download_path(uuid)

Returns a URL that can be used with the C<X-Accel-Redirect> header in Nginx to
offload file downloads to the web server.

To make this work, requires a configuration similar to the following in Nginx:

    location ~ ^/download/swift/([A-Za-z0-9_]+)/(http|https)/([A-Za-z0-9\.\-]+)/([1-9][0-9]+)/(.*) {
        internal;

        proxy_set_header X-Auth-Token $1;
        proxy_pass $2://$3:$4/$5;

        # Zaaksysteem determines (and hence overrides) the Content-Type.
        proxy_hide_header Content-Type;

        # Disable partial downloads -- it's all or nothing. This prevents the
        # PDF viewer from requesting the same file over and over.
        proxy_hide_header Accept-Ranges;

        # Replace with your DNS resolver (that can resolve the Swift host)
        resolver 8.8.8.8;
    }
    

=cut

sub download_url {
    my $self = shift;
    my $uuid = shift;
    $uuid = lc($uuid);

    my $storage_url = URI->new($self->swift->storage_url);

    return sprintf(
        "/download/swift/%s/%s/%s/%d%s/%s/%s",
        $self->swift->token,
        $storage_url->scheme,
        $storage_url->host,
        $storage_url->port,
        $storage_url->path,
        uri_escape($self->storage_bucket),
        uri_escape($uuid),
    );
}

=head2 get_path(uuid)

Returns a L<File::Temp> instance with a local cache copy of the stored file
(which stringifies to a file name).

=cut

sub get_path {
    my $self = shift;
    return $self->get_fh(@_);
}

=head2 get_fh(uuid)

Returns a L<File::Temp> instance with a local cache copy of the stored file
(which also works as a file handle).

=cut

sub get_fh {
    my $self = shift;
    my $uuid = shift;

    $uuid = lc($uuid);

    my $t0 = Zaaksysteem::StatsD->statsd->start;

    my $tmp = File::Temp->new();
    $self->log->trace(sprintf("Retrieving object '%s' in temporary file '%s'", $uuid, $tmp));

    my $etag = $self->swift->get_object(
        container_name => $self->storage_bucket,
        object_name    => $uuid,
        write_code     => sub {
            my ($status, $message, $headers, $chunk) = @_;

            if ($self->log->is_trace) {
                $self->log->trace(sprintf(
                    "Receiving object '%s'; got a chunk of size '%d'",
                    $uuid,
                    length($chunk),
                ));
            }

            print $tmp $chunk;
        }
    );

    # Reset internal file position
    $tmp->seek(0, SEEK_SET);

    Zaaksysteem::StatsD->statsd->end('swift.get_fh.time', $t0);

    return $tmp;
}

=head2 write($uuid, $fh)

Write an object to the Swift object store.

=cut

sub write {
    my $self = shift;
    my ($uuid, $fh) = @_;

    $uuid = lc($uuid);

    $self->log->trace(sprintf(
        "Writing new object '%s' to bucket '%s'",
        $uuid,
        $self->storage_bucket,
    ));

    my $t0 = Zaaksysteem::StatsD->statsd->start;
    
    my $headers = $self->swift->put_object(
        container_name => $self->storage_bucket,
        object_name    => $uuid,
        content        => $fh,
        content_length => -s $fh,
    );

    Zaaksysteem::StatsD->statsd->end('swift.write.time', $t0);

    return $uuid;
}

=head2 erase($uuid)

Erases a file from the FileStore or Bucket

=cut

sub erase {
    my $self = shift;
    my ($uuid) = @_;

    $uuid = lc($uuid);

    $self->log->info(sprintf(
        "Erasing object '%s' from bucket '%s'",
        $uuid,
        $self->storage_bucket,
    ));

    my $headers = $self->swift->delete_object(
        container_name => $self->storage_bucket,
        object_name    => $uuid,
    );

}

=head2 _build_swift

Build a new L<Net::OpenStack::Swift> instance using the C<os_> attributes. 

=cut

sub _build_swift {
    my $self = shift;

    my $swift = Net::OpenStack::Swift->new(
        auth_version  => '2.0',
        auth_url      => $self->os_auth_url,
        user          => $self->os_username,
        password      => $self->os_password,
        tenant_name   => $self->os_tenant_name,
        agent_options => {
            timeout => $self->os_timeout // 30,
        },
    );

    $swift->get_auth();
    my ($header, $containers) = $swift->get_account();

    if (none { $_->{name} eq $self->storage_bucket } @{$containers}) {
        $self->log->info(
            sprintf("Container '%s' does not exist yet. Creating.", $self->storage_bucket)
        );
        $swift->put_container(container_name => $self->storage_bucket);
    }

    return $swift;
}

__PACKAGE__->meta->make_immutable();

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

