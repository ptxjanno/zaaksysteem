package Zaaksysteem::Model::Subcase;

use Moose;
use namespace::autoclean;

extends 'Catalyst::Model::Factory::PerRequest';

__PACKAGE__->config(
    class       => 'Zaaksysteem::Zaken::Subcase',
    constructor => 'new'
);

=head1 NAME

Zaaksysteem::Model::Subcase - Catalyst model factory for
L<Zaaksysteem::Zaken::Subcase> instances.

=head1 SYNOPSIS

    my $model = $c->model('Subcase');

=head1 METHODS

=head2 prepare_arguments

Prepares the constructor arguments for L<Zaaksysteem::Zaken::Subcase>.

=cut

sub prepare_arguments {
    my ($self, $c, $args) = @_;

    my $ret = {
        casetype_rs          => $c->model('DB::Zaaktype'),
        casetype_relation_rs => $c->model('DB::ZaaktypeRelatie'),
        context              => $c,
    };

    return $ret;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
