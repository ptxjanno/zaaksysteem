package Zaaksysteem::Metarole::ObjectRelation;

use Moose::Role;
use namespace::autoclean;

with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::Metarole::ObjectAttribute - Hijack L<Moose::Meta::Attribute>s for
L<Zaaksysteem::Object::Relation> infrastructure

=head1 DESCRIPTION

This L<role|Moose::Role> is meant to be applied on Moose attributes in classes
that are intended to behave like Objects.

    package Zaaksysteem::Object::Types::SomeType;

    use Moose;

    extends 'Zaaksysteem::Object';

    has attr => (
        is => 'ro',
        isa => 'Str',
        traits => [qw[OR]],

        label => 'Relation',
        documentation => 'Relation description',
        required => 1,

        cardinality => 'one-to-one'
    );

=cut

use Zaaksysteem::Object::Attribute;
use Zaaksysteem::Object::ValueModel;

use BTTW::Tools;

use Moose::Util::TypeConstraints qw[
    enum
    class_type
    role_type
    subtype
    find_type_constraint
];

use List::Util qw[first all];

# OR as initialism, we're probably going to be using this one all the thyme,
# so it'd better be short.
Moose::Util::meta_attribute_alias('OR');

=head1 ATTRIBUTES

=head2 label

=cut

has label => (
    isa => 'Str',
    is => 'ro',
    required => 1,
    default => sub {
        shift->name
    }
);

=head2 isa_set

Declares whether the relation is 1:1 or 1:N

If true, the value of the attribute is assumed to be an C<ArrayRef>
of object-like objects.

=cut

has isa_set => (
    is => 'ro',
    isa => 'Bool',
    required => 1,
    default => 0
);

=head2 embed

Declares whether the relation should be embedded. This has the effect of
storing the related object within the main object, instead of relying on
storage layer references.

=cut

has embed => (
    is => 'ro',
    isa => 'Bool',
    required => 1,
    default => 0
);

=head2 type

Declares the L<Zaaksysteem::Object/type> of the related object. This
parameter is not required, but setting it with a specific value is B<highly>
recommended.

=cut

has type => (
    is => 'ro',
    isa => 'Str',
    required => 0,
    predicate => 'has_type'
);

=head1 METHODS

=cut

# Moose 'has' keyword magic
before _process_options => sub {
    my ($self, $name, $options) = @_;

    # Assume caller knows best when injecting an explicit isa.
    return if exists $options->{ isa };

    my $embed = $options->{ embed };
    
    my $value_constraint = $embed
        ? class_type('Zaaksysteem::Object')
        : role_type('Zaaksysteem::Object::Reference');

    my $type_constraint = $value_constraint;

    my $type = $options->{ type };

    if ($type) {
        $type_constraint = subtype({
            as => $value_constraint,
            where => sub { return $_->type eq $type },
            message => sub { return sprintf(
                'Value not a "%s"-type reference: %s',
                $type,
                $_->type
            ) }
        });
    }

    if ($options->{ isa_set }) {
        $type_constraint = find_type_constraint('ArrayRef')->parameterize(
            $type_constraint
        );
    }

    $options->{ isa } = $type_constraint;

    return;
};

=head2 zss_dereference

Returns a L<Zaaksysteem::Object::Value> (with type set to
L<Zaaksysteem::Object::ValueType::ObjectRef>) for the relation.

    my $val = $object->meta->find_attribute_by_name('name')->zss_dereference($object);

=cut

sub zss_dereference {
    my ($self, $object) = @_;

    my $reader = $self->get_read_method;

    return Zaaksysteem::Object::ValueModel->new_value(
        'object_ref',
        $object->$reader()->_ref
    );
}

=head2 zss_set

Sets the attribute in the context of an object using a
L<Zaaksysteem::Object::Value> instance.

=cut

sub zss_set {
    my ($self, $object, $value) = @_;

    my $writer = $self->get_write_method;

    $object->$writer($value->value);

    return;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

