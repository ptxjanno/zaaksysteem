package Zaaksysteem::BR::Subject::ResultSet::Employee;

use Moose::Role;
use BTTW::Tools;

with qw/Zaaksysteem::BR::Subject::Utils/;

=head1 NAME

Zaaksysteem::BR::Subject::ResultSet::Employee - This is a specific bridge role for L<DBIx::Class::ResultSet> classes.

=head1 DESCRIPTION

Applies logic for the L<Zaaksysteem::BR::Subject> bridge within L<DBIx::Class::ResultSet> classes

=head1 METHODS

=head2 search_from_bridge

=cut

sub search_from_bridge {
    my $self        = shift;
    my $params      = $self->map_search_params((shift || {}), 'employee')->{subject};

    my $query = { subject_type => 'employee' };
    if ($params && keys %{ $params }) {
        $query->{'me.properties'} = [];
        for my $param (keys %$params) {
            my $value = $params->{$param};

            push(
                @{ $query->{'me.properties'} },
                { 'like' => "%$value%" }
            );
        }
    }

    return $self->search_active($query, @_)->search_rs;
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
