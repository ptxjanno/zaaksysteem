package Zaaksysteem::BR::Subject::Iterator::Company;

use Moose::Role;

=head1 NAME

Zaaksysteem::BR::Subject::Iterator::Company - This is a iterator extension to cache addresses for company subjects

=head1 DESCRIPTION

This Iterator extension will provide the iterator with the logic to cache C<BedrijfAuthenticatie> tables
in search results. This way, we can reduce the amount of queries substantionaly.

=head1 ATTRIBUTES

=head2 _cached_addresses

Private store of cached addresses

=cut

has _cached_authentication => (
    is      => 'rw',
    default => 1,
);

=head1 METHODS

=head2 _can_cache_authentication

Returns true when caching of authentication data for this iterator is allowed/possible

=cut


sub _can_cache_authentication {
    my $self    = shift;

    if ($self->_cache_is_setup && $self->rs->result_source->name eq 'bedrijf') {
        return 1;
    }

    return;
}

=head1 AROUND METHODS

=head2 _cache_related_rows

Caches the related rows, C<BedrijfAuthenticatie> in this case, into the iterator

=cut


around '_cache_related_rows' => sub {
    my $method  = shift;
    my $self    = shift;

    my $ok      = $self->$method(@_);

    return $ok if (!$self->_can_cache_authentication || $self->_in_cache_loop->{company});

    my $b_query    = $self->rs->search()->get_column('bedrijf.id')->as_query;

    ### Get all addresses
    my @entries   = $self->rs->result_source->schema->resultset('BedrijfAuthenticatie')->search({
        'gegevens_magazijn_id'      => { in => $b_query },
    })->all;

    my %cached_entries;
    for my $entry (@entries) {
        my $entry_id                 = $entry->get_column('gegevens_magazijn_id');
        $cached_entries{ $entry_id } ||= [];

        push(@{ $cached_entries{ $entry_id } }, $entry);
    }

    $self->_in_cache_loop->{company} = 1;
    $self->_cached_authentication(\%cached_entries);

    return $ok;
};

=head2 _apply_cache_on_row

Applies the cached data, from C<BedrijfAuthenticatie> in this case, into the returned row

=cut

around '_apply_cache_on_row' => sub {
    my $method  = shift;
    my $self    = shift;
    my ($row)   = @_;

    my $ok      = $self->$method(@_);

    ### Only when subject_type = person
    return $ok if (!$self->_can_cache_authentication);

    my $b_id       = $row->get_column('id');
    $row->cached_authentication(
        ($self->_cached_authentication->{$b_id} ? [ $self->_cached_authentication->{$b_id}->[0] ] : []),
    );

    return 1;
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

