package Zaaksysteem::Backend::Mailer;
use Moose;

=head2 send

Wrap around a selected sendmail function. This way we can easily
pass the mailer as an object, which allows us to pass a mock object
in the same way.

=cut

sub send {
    my $self = shift;

    require Email::Sender::Simple;
    Email::Sender::Simple->import('sendmail');

    return sendmail(@_);
}

__PACKAGE__->meta->make_immutable();



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

