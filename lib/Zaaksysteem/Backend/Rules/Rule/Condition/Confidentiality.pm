package Zaaksysteem::Backend::Rules::Rule::Condition::Confidentiality;

use Moose::Role;
use BTTW::Tools;

=head1 NAME

Zaaksysteem::Backend::Rules::Rule::Condition::Confidentiality - Handles payment_status

=head1 SYNOPSIS

=head1 DESCRIPTION

=cut

after 'BUILD' => sub {
    my $self        = shift;

    if ($self->attribute eq 'confidentiality') {
        $self->attribute('case.confidentiality');
    }
};

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Backend::Rules> L<Zaaksysteem::Manual>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
