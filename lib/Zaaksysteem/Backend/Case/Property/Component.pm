package Zaaksysteem::Backend::Case::Property::Component;

use Moose;

extends 'Zaaksysteem::Backend::Component';

=head1 NAME

Zaaksysteem::Backend::Case::Property::Component

=head1 DESCRIPTION

This package adds table-specific behaviors to the C<case_property> table and
result source code.

=cut

use List::Util qw[first];

=head1 METHODS

=head2 upsert

Insert or update feature for efficient changes to the table's state using
PostgreSQL's C<INSERT ... ON CONFLICT ...> feature.

    my $row = $c->model('DB::CaseProperty')->new_result({ ... })->upsert;

This method is distinct from L<DBIx::Class::Row/update_or_insert>, which uses
the C<in_storage> state of the in-memory row to determine if an update or
insert is required.

The conflict resolution for this upsert method is to set the C<value> and
C<value_v0> columns provided in the insert's C<VALUES> listing in the
conflicted (existing) row, making it an update-set statement.

=cut

sub upsert {
    my $self = shift;

    my $source = $self->result_source;
    my $maker = $source->schema->storage->sql_maker;

    my $data = { $self->get_columns };

    # Build 'INSERT INTO' syntax as ->insert() usually would.
    my ($insert_sql, @bind) = $maker->insert($source->name, $data);

    # Builds 'value = EXCLUDED.value, ...' sql for the on-conflict action
    # EXCLUDED is a funky postgres extension which acts like a table with
    # a single row, filled with the values provided in the INSERT statement.
    # Bind parameters are ignored, since there aren't any for the escaped
    # sql 'values' in the listing.
    my ($set_sql) = $maker->_update_set_values({
        value    => \'EXCLUDED.value',
        value_v0 => \'EXCLUDED.value_v0'
    });

    # Bit of back and forward between the schema and postgres, we want to
    # perform the upsert query such that postgresql can infer the relevant
    # constraint for the conflict to trigger on. We could also instruct
    # postgres to trigger on this constraint specifically, but the syntax
    # form is discouraged. So we do it the, relatively, long way around.
    my @inference_columns = $source->unique_constraint_columns(
        'case_property_name_ref_idx'
    );

    my $sql = sprintf(
        '%s ON CONFLICT (%s) DO UPDATE SET %s',
        $insert_sql,
        join(', ', @inference_columns),
        $set_sql
    );

    # dbh_do retries our query after disconnects. This doesn't usually
    # happen within the context of a Catalyst request, but better safe
    # than sorry.
    $source->storage->dbh_do(sub {
        my ($storage, $dbh) = @_;

        # Bind parameters returned by SQL::Abstract, as configured by
        # DBIx::Class, come in the array-of-array form, where the second level
        # arrays contain a column reference and the value itself. Plain DBI
        # expects bare bind parameters, so we need to extract those from the
        # augmented DBIx::Class bind parameters.
        $dbh->do($sql, undef, map { $_->[1] } @bind);
    });

    # DBIx::Class::Row internals cleanup, upsert must behave like
    # insert/update so later calls to those functions don't get confused
    # about the 'dirty' state.
    $self->in_storage(1);
    $self->{ _dirty_columns } = {};
    $self->{ related_resultsets } = {};
    delete $self->{ _column_data_in_storage };

    return $self;
}

=head2 get_v0_field

Convenience getter for v0-style fields contained in the property.

    my $v0_attribute = $property->get_v0_field('case.casetype');

Returns the first v0 attribute definition that matches on the given name, as
stored in the C<value_v0> table column.

=cut

sub get_v0_field {
    my $self = shift;
    my $name = shift;

    return first { $_->{ name } eq $name } @{ $self->value_v0 };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
