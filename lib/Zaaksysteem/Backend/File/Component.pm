package Zaaksysteem::Backend::File::Component;

use Moose;

=head1 NAME

Zaaksysteem::Backend::File::Component - A Zaaksysteem file DB component

=cut

use BTTW::Tools;
use BTTW::Tools::File qw(sanitize_filename);
use BTTW::Tools::Text;
use BTTW::Tools::Text::PDFHack;
use BTTW::Tools::Text::OpenDocument;
use BTTW::Tools::Text::OOXML;
use BTTW::Tools::Text::HTML;
use File::Basename;
use File::MimeInfo::Magic;
use File::Spec::Functions;
use File::Temp;
use List::Util qw[first any uniq];
use Moose::Util::TypeConstraints qw[enum];
use Params::Profile;
use UUID::Tiny ':std';
use Zaaksysteem::Constants qw(
    MIMETYPES_ALLOWED
    MAX_CONVERSION_FILE_SIZE
);
use Zaaksysteem::Constants::Locks qw(ADD_QUEUE_ITEM_LOCK);
use Zaaksysteem::Object::Reference::Instance;
use Zaaksysteem::Object::Types::Document;
use Zaaksysteem::Object::Types::File::Lock;
use Zaaksysteem::Types qw(DocumentStatus);
use Zaaksysteem::Object::Attribute;

extends 'DBIx::Class';

with qw(
    MooseX::Log::Log4perl
);

use FindBin qw/$Bin/;

=head2 home

The homedir, used to find the static files which download_info uses

=cut

has home => (
    is => 'ro',
    isa => 'Str',
    lazy => 1,
    default => sub {
        return $Bin . '/..';
    }
);

=head2 thumbnail_unavailable

Filepath to the thumbnail_unavailable file

=cut

has thumbnail_unavailable => (
    is => 'ro',
    isa => 'Str',
    lazy => 1,
    default => sub {
        my $self = shift;
        return catfile($self->home, qw(share img thumbnail_unavailable.png));
    }
);

=head2 thumbnail_unavailable_size

Filepath to the thumbnail_unavailable_size file

=cut

has thumbnail_unavailable_size => (
    is => 'ro',
    isa => 'Str',
    lazy => 1,
    default => sub {
        my $self = shift;
        return catfile($self->home, qw(share img thumbnail_unavailable_size.png));
    }
);

=head2 thumbnail_waiting

Filepath to the thumbnail_waiting file

=cut

has thumbnail_waiting => (
    is => 'ro',
    isa => 'Str',
    lazy => 1,
    default => sub {
        my $self = shift;
        return catfile($self->home, qw(share img thumbnail_waiting.png));
    }
);

use Exception::Class (
    'Zaaksysteem::Backend::File::Component::Exception' => { fields => 'code' },
    'Zaaksysteem::Backend::File::Component::Exception::General' => {
        isa         => 'Zaaksysteem::Backend::File::Component::Exception',
        description => 'General exception',
        alias       => 'throw_general_exception',
    },
    'Zaaksysteem::Backend::File::Component::Exception::Parameter' => {
        isa         => 'Zaaksysteem::Backend::File::Component::Exception',
        description => 'Parameter exception',
        alias       => 'throw_parameter_exception',
    },
    'Zaaksysteem::Backend::File::Component::Exception::Logic' => {
        isa         => 'Zaaksysteem::Backend::File::Component::Exception',
        description => 'Logic error',
        alias       => 'throw_logic_exception',
    },
);

=head2 $file->update_properties

Updates the properties of a File database entry.

=head3 Arguments

=over

=item subject [required]

The subject executing this call.

=item deleted [optional]

Boolean. When set to true it will set date_deleted to now() and deleted_by to
the given subject. The opposite, setting to false, will 'undelete' the file.

Undeleted files will always get placed in the root. Any directories set before
deletion are no longer present.

=item metadata [optional]

Update or create a file's metadata. See update_metadata POD for columns.

=item directory_id [optional]

Place this file in a directory.

=item accepted [optional]

Boolean. When set to false a rejection_reason is required detailing why this
file is being sent back to the queue. Setting to false will remove any case or
subject that is currently set.

=item rejection_reason [required if accepted set to false]

String detailing the reason why a document was not accepted.

=item publish_pip [optional]

Boolean, decides whether this file gets shown on the PIP or not.

=item publish_website [optional]

Boolean, decides whether this file may be exported outside of Zaaksysteem.

=item case_document_ids

List of zaaktype_kenmerken IDs

=item reject_to_queue

Boolean, if defined rejects the file regardless of what the DB entry
says about queue rejection

=back

=head3 Returns

Updated version of the File object.

=cut

Params::Profile->register_profile(
    method  => 'update_properties',
    profile => {
        required => [qw/
            subject
        /],
        optional => [qw/
            name
            case_id
            publish_pip
            publish_website
            accepted
            rejection_reason
            directory_id
            version
            root_file_id
            destroyed
            pip_owner
            user_has_zaak_beheer_permissions
            document_status
            rejected
            reject_to_queue
        /],
        constraint_methods => {
            document_status => sub { return DocumentStatus->check(pop) },
            case_id         => qr/^[\d]+$/,
            accepted        => qr/^(0|1)$/,
        },
    }
);

sub update_properties {
    my $self = shift;
    my $opts = $_[0];

    my $dv = Params::Profile->check(
        params  => $opts,
    );

    # Various parameter contraints/checks
    my $valid = $dv->valid;

    my $user_has_zaak_beheer_permissions = delete $valid->{user_has_zaak_beheer_permissions};

    if ($dv->has_invalid) {
        my @invalid = join ',',$dv->invalid;
        throw_parameter_exception(
            code  => '/file/update_properties/invalid_parameters',
            error => "Invalid options given: @invalid",
        );
    }

    my $subject = delete $valid->{subject};
    my $betrokkene_identifier = defined $subject ? $subject->old_subject_identifier : "";

    # Forbid altering deleted files unless the file is being restored or destroyed
    if ($self->date_deleted && !exists $opts->{deleted} && !exists $opts->{destroyed}) {
        throw_parameter_exception(
            code  => '/file/update_properties/cannot_alter_deleted',
            error => "Cannot alter a deleted file",
        );
    }

    # case_document_ids is expected to always be a list
    if ($opts->{case_document_ids} && !UNIVERSAL::isa($opts->{case_document_ids}, 'ARRAY')) {
        throw_parameter_exception(
            code  => '/file/update_properties/not_an_array',
            error => "case_document_ids can only be set as an array",
        );
    }

    # When a file is being destroyed, make sure any other existing versions are destroyed as well
    if (exists $opts->{destroyed} && $opts->{destroyed}) {

        throw('case/file/destroy', "Gebruiker heeft geen rechten om de prullenbak te legen")
            unless $user_has_zaak_beheer_permissions;

        my $root = $self->get_root_file;

        # Update root file first, then associated files. Update will throw an
        # exception should the file be locked by another user.
        $root->update(
            { destroyed => $opts->{ destroyed } },
            { subject => $subject }
        );

        $root->files->update({ destroyed => $opts->{ destroyed } });

        if($self->case) {
            $self->case->trigger_logging(
                'case/document/remove', {
                    component => 'document',
                    data => {
                        file_id   => $self->id,
                        file_name => sanitize_filename($self->name),
                        case_id => $self->get_column('case_id'),
                        mimetype => $self->filestore_id->mimetype,
                    }
                }
            );
        } else {
            my @log_args = (
                'file/remove',
                {
                    component => 'documenten',
                    data => {
                        file_id   => $self->id,
                        file_name => sanitize_filename($self->name),
                        mimetype  => $self->filestore_id->mimetype
                    }
                }
            );

            $self->result_source->schema->resultset('Logging')->trigger(@log_args);
        }
    }

    # As some things depend on a case being present we set it first if it is present
    # in the options. (As opposed to various if statements)
    if ($opts->{case_id} && !$self->case_id) {
        $self->update(
            { case_id => $opts->{ case_id } },
            { subject => $subject }
        );

        $self->trigger('assign');
        delete $opts->{case_id};
    }

    # Update metadata
    if ($opts->{metadata}) {
        $self->update_metadata($opts->{metadata});
    }

    # Always add modified info
    $valid->{date_modified} = DateTime->now;
    $valid->{modified_by}   = $betrokkene_identifier;

    # D::FV does not handle undef well. Check if a value is set in
    # valid, if not (but they key is set in the opts hash) set it
    # anyway.
    if (exists $opts->{directory_id} && !$valid->{directory_id}) {
        $valid->{directory_id} = $opts->{directory_id};
    }
    if (exists $opts->{publish_website} && !$valid->{publish_website}) {
        $valid->{publish_website} = 0;
    }
    if (exists $opts->{publish_pip} && !$valid->{publish_pip}) {
        $valid->{publish_pip} = 0;
    }

    # Restore or delete a file
    if (exists $opts->{deleted} && !$opts->{deleted}) {
        $valid->{date_deleted} = undef;
        $valid->{deleted_by}   = undef;

        if($self->case) {
            $self->case->trigger_logging(
                'case/document/restore',
                {
                    component => 'document',
                    data => {
                        case_id   => $self->case->id,
                        file_id   => $self->id,
                        file_name => sanitize_filename($self->name),
                        mimetype  => $self->filestore_id->mimetype
                    }
                }
            );
        } else {
            $self->result_source->schema->resultset('Logging')->trigger(
                'file/restore',
                {
                    component => 'documenten',
                    data => { file_id => $self->id }
                }
            );
        }
    }
    elsif (exists $opts->{deleted} && $opts->{deleted}) {

        $valid->{date_deleted} = DateTime->now;
        $valid->{deleted_by}   = $betrokkene_identifier;
        $valid->{directory_id} = undef;

        if($self->case) {
            if (my @ids = $self->get_case_document_ids) {
                $self->case_documents->delete;
                $self->set_case_documents_in_case_property(@ids);
            }
            if ($self->confidential && !$self->case_id->confidential_access) {
                delete $valid->{$_} for qw(date_deleted deleted_by directory_id);
            }
            else {
                $self->case->trigger_logging(
                    'case/document/trash',
                    {
                        component => 'document',
                        data => {
                            case_id => $self->case->id,
                            file_id => $self->id,
                            file_name => sanitize_filename($self->name),
                            mimetype => $self->filestore_id->mimetype
                        }
                    }
                );
            }
        } else {
            $self->result_source->schema->resultset('Logging')->trigger(
                'file/trash',
                {
                    component => 'documenten',
                    data => { file_id => $self->id }
                }
            );
        }
    }


    my $reject_to_queue = $opts->{reject_to_queue} // $self->reject_to_queue;
    # Reject a file; unset case_id and set accepted to false.
    if ($valid->{rejected} || exists $opts->{accepted} && !$opts->{accepted}) {
        ### ZS-1863: Make sure we do not "deny" a file which is already accepted...
        if ($self->accepted) {
            throw_logic_exception(
                code  => '/file/update_properties/cannot_unaccept',
                error => 'File is already accepted, cannot "unaccept"',
            );
        }

        if($valid->{rejected}) {
            delete $opts->{rejected};
            delete $valid->{rejected};
            $valid->{accepted} = $opts->{accepted} = 0;
            $valid->{rejection_reason} //= sprintf('Geweigerd als documentkenmerk uit zaak %s', $self->get_column('case_id') // "<onbekend>");
            $opts->{rejection_reason}  //= $valid->{rejection_reason};
        }

        $self->reject;

        $self->trigger('reject', {
            rejection_reason => $opts->{rejection_reason},
            reject_to_queue => $reject_to_queue,
            filename => $self->filename,
        });

        # When rejecting a PIP document, handle it as a deletion. There is no
        # requeueing for these types. Same goes for when 'reject_to_queue' is false.

        if (!$reject_to_queue) {
            $valid->{date_deleted} = DateTime->now;
            $valid->{deleted_by}   = $betrokkene_identifier;
            $valid->{directory_id} = undef;
            $self->case_documents->delete;
        }
        # All other documents go back to the queue.
        else {
            if (!$valid->{rejection_reason}) {
                throw_logic_exception(
                    code  => '/file/update_properties/rejection_reason_required',
                    error => 'Rejection reason required when not accepting a file',
                );
            }
            $valid->{case_id}    = undef;
            $valid->{subject_id} = undef;
            $valid->{accepted}   = 'f';
        }
    }

    # Accept a file, unset any rejection_reason that might exist.
    elsif (exists $opts->{accepted}) {
        if (!$self->case_id) {
            throw_logic_exception(
                code  => '/file/update_properties/cannot_accept_unassigned',
                error => 'Cannot accept a document if it is not assigned to a case',
            );
        }
        $valid->{rejection_reason}  = undef;
        $valid->{is_duplicate_name} = 0;
        $valid->{is_duplicate_of}   = undef;

        if (my @ids = $self->get_case_document_ids) {
            $self->set_case_documents_in_case_property(@ids);
        }
    }

    # If a file is already accepted and is getting a name change OR
    # it is currently being accepted: automatically rename the file if there is
    # a duplicate. If a root_file_id is being set it would implicate a new version in
    # a series. Ignore duplicate name if so.
    if ($self->accepted && $valid->{name} || ($valid->{accepted} and !$valid->{root_file_id})) {
        my $name = $valid->{name} ? sanitize_filename($valid->{name}) : $self->name;
        $valid->{name} = $self->get_valid_filename($name);
    }

    if (!$self->accepted && !$valid->{accepted}) {
        $valid->{active_version} = 1;
    }

    # Assign a case
    if ($opts->{case_id}) {
        if ($self->case) {
            throw_logic_exception(
                code  => '/file/update_properties/case_already_defined',
                error => 'Cannot assign a new case when a case is already defined',
            );
        }
        # When assigning a case_id a previous rejection_reason should be cleared.
        $valid->{rejection_reason} = undef;
    }

    # If a case document is being assigned, set the defaults belonging to it.
    # This is done before the actual update so overrides can be passed right
    # away.
    if ($opts->{case_document_ids}) {
        $self->set_or_replace_case_documents(@{$opts->{case_document_ids}});

        # Only set defaults if a single document is being assigned.
        $self->apply_case_document_defaults;
    }

    my $result = $self->update($valid, { subject => $subject })->discard_changes;

    if (exists $valid->{accepted} && $self->get_column('case_id')) {
        $self->case_id->ddd_update_system_attribute('case.num_unaccepted_files');
    }

    if($valid->{ name }) {
        $self->trigger('rename', {
            renamed_to => $valid->{ name }
        });

        ### Check if references files are now "in the clear"
        if ($valid->{name} ne $self->name) {
            my $duplicate_files = $self->file_is_duplicate_ofs;

            while (my $duplicate_file = $duplicate_files->next) {
                $duplicate_file->is_duplicate_of(undef);
                $duplicate_file->is_duplicate_name(0);
                $duplicate_file->update;
            }
        }
    }

    if(exists $valid->{ publish_website }) {
        $self->trigger($valid->{ publish_website } ? 'publish' : 'unpublish', {
            publish_to => 'website'
        });
    }

    if(exists $valid->{ publish_pip }) {
        $self->trigger($valid->{ publish_pip } ? 'publish' : 'unpublish', {
            publish_to => 'pip'
        });
    }

    return $result;
}

=head2 $file->update_metadata

Update a File's metadata.

=head3 Parameters

All parameters are optional.

=over

=item description

=item document_category

=item trust_level

Valid: 'Openbaar', 'Beperkt openbaar', 'Intern', 'Zaakvertrouwelijk', 'Vertrouwelijk', 'Confidentieel', 'Geheim', 'Zeer geheim'

=item origin

Valid: 'Inkomend', 'Uitgaand', 'Intern'

=back

=cut

Params::Profile->register_profile(
    method  => 'update_metadata',
    profile => {
        required => [],
        optional => [qw/
            description
            document_category_parent
            document_category_child
            trust_level
            origin
            origin_date
            creation_date
        /],
    }
);

sub update_metadata {
    my $self = shift;
    my $update = shift;

    my $opts = assert_profile(shift || {}, profile => {
        optional => {
            subject => 'Zaaksysteem::Object::Types::Subject'
        }
    })->valid;

    my $valid = Params::Profile->check(
        params  => $update,
    );
    if (!$valid->success) {
        throw_parameter_exception(
            code  => '/file/update_metadata/invalid_options',
            error => 'Invalid options given',
        );
    }

    # Check whether to create or update
    if (!$self->metadata_id) {

        delete $update->{id};

        $update->{creation_date} //= $self->date_created;
        my $md = $self->result_source->schema->resultset('FileMetadata')->create(
            $update
        );
        $self->update({ metadata => $md }, $opts);
    }
    else {
        $self->metadata->update($update);
    }

    my @secret_levels = (
        'Vertrouwelijk',
        'Confidentieel',
        'Geheim',
        'Zeer geheim'
    );

    if (my $trust_level = $update->{trust_level}) {
        if (any { $trust_level eq $_ } @secret_levels) {
            $self->update({confidential => 1}) unless $self->confidential;
        }
        else {
            $self->update({confidential => 0}) if $self->confidential;
        }
        if (my @ids = $self->get_case_document_ids) {
            $self->set_case_documents_in_case_property(@ids);
        }
    }

    if ($self->case) {
        my %metadata = %$update;

        delete $metadata{id};
        # DateTime vs scalar
        if (ref($metadata{creation_date})) {
            $metadata{creation_date} = $metadata{creation_date}->iso8601;
        }

        my $logging = $self->case->logging;

        my %event_data = (
            case_id   => $self->case->id,
            file_id   => $self->id,
            metadata  => \%metadata,
            file_name => $self->name,
            mimetype  => $self->filestore_id->mimetype
        );

        my $event = do {
            local $event_data{metadata} = {'%' => '%'};
            $logging->find_recent({
                data       => \%event_data,
                event_type => 'case/document/metadata/update'
            });
        };

        if ($event) {
            $event->data(\%event_data);
            $event->update;
        } else {
            # No event found
            $self->case->trigger_logging(
                'case/document/metadata/update',
                {
                    component => 'document',
                    data      => \%event_data,
                }
            );
        }
    }
}

=head2 $file->update_file

Replace a file with a newer version.

=head3 Parameters

=over

=item new_file_path [required]

The new file's location.

=item original_name [required]

The name that will be set in the Filestore for this file. Not to be confused
with the File's name. This will be copied from the current File. This is required
due to not accidently setting the name to the UUID.

=item is_restore

Indicate whether or not this is a file being restored from a previous version.
When set, the version check (if older than latest, throw exception) is bypassed.

=back

=cut

Params::Profile->register_profile(
    method  => 'update_file',
    profile => {
        required => [qr/subject new_file_path original_name/],
        optional => [qr/is_restore name/],
    }
);

sub update_file {
    my $self = shift;
    my $opts = $_[0];

    my $dv = Params::Profile->check(
        params  => $opts,
    );
    if ($dv->has_invalid) {
        my @invalid = join ',',$dv->invalid;
        throw_parameter_exception(
            code  => '/file/update_file/invalid_options',
            error => 'Invalid options given: @invalid',
        );
    }

    # Check if there is already a newer parent than the file being updated
    my $newest = $self->get_last_version;
    if ($newest->version > $self->version && !$opts->{is_restore}) {
        throw_parameter_exception(
            code  => '/file/update_file/not_last_version',
            error => sprintf (
                "File %s is at version %d, can't modify given version %d",
                ($self->name, $newest->version, $self->version)
            ),
        );
    }

    # Create new filestore entry
    my $file_path = $opts->{new_file_path};

    my $fs = $self->result_source->schema->resultset('Filestore')->filestore_create({
        original_name => $opts->{original_name},
        file_path     => $file_path,
    });

    my $props = $opts->{name}
        ? Zaaksysteem::Backend::File::ResultSet::get_file_properties(sanitize_filename($opts->{name}))
        : undef;

    my $copy = $self->_copy_file(
        uuid         => $newest->get_column('uuid'),
        directory_id => $newest->get_column('directory_id'),
        filestore    => $fs,
        version      => $newest->version + 1,
        subject      => $opts->{ subject },
        root_file_id => $self->root_file_id || $self->id, # The first file doesn't point to itself
        defined($props)
            ? ( name => $props->{name}, ext => $props->{ext})
            : ()
    );

    # Unset any foreign keys that may disappear on the old entry. (Directories, for example)
    $self->file_derivatives->delete();

    $self->update(
        { directory_id => undef },
        { subject => $opts->{ subject } }
    );

    my $event;

    # Same for restoring files, except $self isn't always the last active file
    my @log_data;
    if ($opts->{is_restore}) {
        $newest->update(
            { directory_id => undef },
            { subject => $opts->{ subject } }
        );

        @log_data = (
            'case/document/revert',
            {
                component => 'document',
                component_id => $copy->id,
                data => {
                    case_id => $copy->get_column('case_id'),
                    original_file_name => sanitize_filename($self->filename),
                    original_file_version => $self->version,
                    new_file_name => sanitize_filename($copy->filename),
                    new_file_version => $copy->version,
                    file_id   => $self->id,
                    file_name => sanitize_filename($self->name),
                    mimetype => $self->filestore_id->mimetype
                }
            }
        );
    } else {
        @log_data = (
            'case/document/replace',
            {
                component => 'document',
                component_id => $copy->id,
                data => {
                    case_id => $copy->get_column('case_id'),
                    original_file_name => sanitize_filename($newest->filename),
                    original_file_version => $newest->version,
                    new_file_name => sanitize_filename($copy->filename),
                    new_file_version => $copy->version,
                    file_id   => $self->id,
                    file_name => sanitize_filename($self->name),
                    mimetype => $self->filestore_id->mimetype
                }
            }
        );
    }

    if ($self->case_id) {
        $event = $self->case->trigger_logging(@log_data);
    }
    else {
        $event = $self->result_source->schema->resultset('Logging')->trigger(@log_data);
    }

    $copy->update(
        { creation_reason => $event->onderwerp },
        { subject => $opts->{ subject } }
    );

    ### Search all other versions, and unset root_file_id
    $copy->make_active_version($opts->{ subject });
    $self->discard_changes;

    return $copy;
}

sig make_active_version => 'Zaaksysteem::Object::Types::Subject';

sub make_active_version {
    my $self = shift;
    my $subject = shift;

    my $root_file_id = $self->get_column('root_file_id');

    if ($root_file_id) {
        my $previous_versions  = $self->result_source->resultset->search(
            {
                '-or'   => [
                    { id => $root_file_id },
                    {
                        root_file_id    => $root_file_id,
                        id              => { '!=' => $self->id },
                    },
                ],
                active_version => 1,
            }
        );

        while (my $old_version = $previous_versions->next) {
            $old_version->active_version(0);
            $old_version->update(undef, { subject => $subject });
        }
    }

    $self->active_version(1);
    $self->update(undef, { subject => $subject });

    return $self->discard_changes;
}

Params::Profile->register_profile(
    method  => 'update_existing',
    profile => {
        required => [qr/subject existing_file_id/],
    }
);

sub update_existing {
    my $self = shift;
    my $opts = $_[0];

    my $dv = Params::Profile->check(
        params  => $opts,
    );

    if ($dv->has_invalid) {
        my @invalid = join ',',$dv->invalid;
        throw_parameter_exception(
            code  => '/file/update_existing/invalid_options',
            error => 'Invalid options given: @invalid',
        );
    }

    my ($existing) = $self->result_source->resultset->search({id => $opts->{existing_file_id}});
    if (!$existing) {
        throw_parameter_exception(
            code  => '/file/update_existing/file_not_found',
            error => printf('Existing file with id %d not found', $opts->{existing_file_id}),
        );
    }
    if ($existing->accepted) {
        throw_logic_exception(
            code  => '/file/update_existing/existing_already_accepted',
            error => 'When using an existing file, the file cannot already be accepted',
        );
    }

    my $last_version = $self->get_last_version;

    $last_version->assert_no_lock($dv->valid->{ subject });

    my $result = $existing->update_properties({
        subject         => $opts->{ subject },
        version         => $last_version->version+1,
        root_file_id    => $last_version->root_file_id || $last_version->id,
        accepted        => 1,
        publish_pip     => $last_version->publish_pip || $existing->publish_pip,
        publish_website => $last_version->publish_website || $existing->publish_website,
        directory_id    => $last_version->directory_id,
    });

    # Set new file-id on file case documents. History not required.
    if ($last_version->case_documents) {
        my @ids = $self->get_case_document_ids;
        $last_version->case_documents->search(
            { case_document_id => {'not in' => \@ids } })->update({
            file_id => $result->id
        });
        $result->set_case_documents_in_case_property(@ids);
    }

    # Unset any foreign keys that may disappear on the old entry.
    $last_version->update(
        { directory_id => undef },
        { subject => $opts->{ subject } }
    );

    $existing->uuid($last_version->uuid);
    $existing->make_active_version($opts->{ subject });

    return $result->discard_changes;
}

=head2 $file->get_last_version

Returns the last version of a file.

=cut

sub get_last_version {
    my $self = shift;
    my $root_file_id = $self->get_column('root_file_id') || $self->id;

    my $res = $self->result_source->schema->resultset('File')->search_rs(
        { root_file_id => $root_file_id, },
        { order_by => { -desc => 'version' }, rows => 1 },
    )->first;

    return $res ? $res : $self;
}

=head2 $self->get_root_file

Gets the root file of a file. The root file is the first document in a series
of versions. When no root_file_id is set in the file, it is assumed that this
is in fact the first file in the series.

=cut

sub get_root_file {
    my $self = shift;
    my $root_file;
    if ($self->root_file_id) {
        $root_file = $self->root_file_id;
    }
    else {
        $root_file = $self;
    }
    return $root_file;
}

=head2 $self->make_leading

Makes this file the leading one by duplicating it into the last version +1.

=cut

sig make_leading => 'Zaaksysteem::Object::Types::Subject';

sub make_leading {
    my $self = shift;
    my $subject = shift;

    # Simply use update_file to achieve this. The functionality is exactly the
    # same. The only 'bad' part of it is that it will readd the file to the
    # filestore.
    my $file_path = $self->filestore->get_path();

    return $self->update_file({
        subject       => $subject,
        original_name => $self->filestore->original_name,
        new_file_path => $file_path,
        is_restore    => 1,
    })->discard_changes;
}

=head2 $self->get_pip_data

Fetches the data that is approved to show on the PIP.

=cut

sub get_pip_data {
    my $self = shift;

    no strict 'refs';
    *DateTime = sub {shift->iso8601};
    use strict;

    (my $dotless = $self->extension) =~ s/\.//g;
    return {
        extension_dotless => $dotless,
        id                => $self->id,
        destroyed         => $self->destroyed,
        extension         => $self->extension,
        pip_thumbnail_url => '/pip/file/thumbnail/file_id/'.$self->id,
        version           => $self->version,
        name              => $self->name,
        accepted          => $self->accepted,
        publish_pip       => $self->publish_pip,
        date_created      => $self->date_created,
        date_modified     => $self->date_modified,
        date_deleted      => $self->date_deleted,
        deleted_by        => $self->deleted_by,
        created_by        => $self->created_by,
        modified_by       => $self->modified_by,
        filestore_id => {
           mimetype    => $self->filestore->mimetype,
           is_archivable => $self->filestore->is_archivable ? \1 : \0,
        },
        case_id => {
           id => $self->case->id,
        },
    }
}

=head2 TO_JSON

TO_JSON extension. Pretty much only to convert DateTime objects. Consider
moving this to a more global space at some point. Or even better; fix it
properly without the monkey patch.

=cut

sub TO_JSON {
    my $self = shift;

    {
        no warnings 'redefine';
        no strict 'refs';
        *DateTime::TO_JSON = sub {shift->iso8601};
        use strict;
    }

    my %options;

    ### XXX TODO (different call from frontend)
    # $options{log} = [$self->loglines->all];

    # Intake owner
    $options{intake_owner} = $self->intake_owner;

    ### XXX TODO
    # if ($self->is_duplicate_name && $self->accepted == 0 && $self->case) {
    #     my $result = $self->result_source->resultset->search({
    #         id        => {'!=' => $self->id},
    #         name      => $self->name,
    #         case_id   => $self->case->id,
    #         extension => $self->extension,
    #     },
    #     {
    #         prefetch    => [
    #             { case_id => "zaaktype_node_id" },
    #             { case_id => "zaaktype_id" },
    #             { case_id => "aanvrager" },
    #             { case_id => "coordinator" },
    #             { case_id => "behandelaar" },
    #             'filestore_id',
    #             'root_file_id'
    #         ]
    #     }
    #     );
    #     # In case the duplicate vanished in the mean time we simply skip this part.
    #     if ($result->count) {
    #         $options{is_duplicate_of} = $result->first->get_last_version->id;
    #     }
    # }

    my $schema = $self->result_source->schema;

    my $locally_editable = $schema->resultset('Config')->get(
        'files_locally_editable'
    );

    $options{thumbnail_url}      = '/file/thumbnail/file_id/'.$self->id;
    $options{pip_thumbnail_url}  = '/pip/file/thumbnail/file_id/'.$self->id;
    $options{locally_editable}   = $locally_editable ? \1 : \0;
    $options{signable}           = $schema->has_active_module('valid_sign') ? \1 : \0;
    $options{extension_dotless}  = $self->extension =~ s/^\.//r;

    ### XXX TODO (different call from frontend)
    if ($self->case_id) {
        if ($self->has_column_loaded('case_document_id_list')
            && ref $self->get_column('case_document_id_list') eq 'ARRAY')
        {
            $options{case_documents} = $self->get_column('case_document_id_list');
        } else {
            my @case_document_ids = $self->case_documents->search({
                case_document_id => { '!=' => undef }
            })->get_column('case_document_id')->all;

            $options{ case_documents } = \@case_document_ids;
        }
    }

    $options{case_documents} //= [];

    ### Done, annotation count from subquery
    if ($self->has_column_loaded('annotation_count')) {
        $options{annotation_count} = $self->get_column('annotation_count');
    } else {
        $options{annotation_count} = $self->annotation_count;
    }

    %options = $self->_load_plain_columns(
        %options
    );

    %options = $self->_append_custom_columns(
        %options
    );

    unless (defined $options{ lock_timestamp }) {
        $options{ lock_subject_id } = undef;
        $options{ lock_subject_name } = undef;
    }

    return \%options;
}

sub _load_plain_columns {
    my $self            = shift;
    my %options         = @_;

    for my $column ($self->result_source->columns) {
        my $info = $self->result_source->column_info($column);

        # If there's a DateTime inflator, use it. Others (like relationships)
        # should stay un-inflated for performance.
        if (exists $info->{_ic_dt_method}) {
            my $value = $self->$column;

            if ($column eq 'lock_timestamp' && defined $value && $value < DateTime->now) {
                $options{ $column } = undef;
            } elsif (defined $value) {
                $options{$column} = $value->clone->set_time_zone('UTC')->iso8601 . 'Z';
            } else {
                $options{$column} = undef;
            }
        }
        else {
            $options{$column} = $self->get_column($column);
        }

    }

    return %options;
}

sub _append_custom_columns {
    my $self            = shift;
    my %options         = @_;

    $options{case_id}       = $self->get_column('case_id');
    $options{filestore_id}  = $self->filestore_id;
    $options{directory_id}  = $self->directory_id;
    $options{metadata_id}   = ($self->metadata_id ? { $self->metadata_id->get_columns } : undef);

    # Comply with TMLO, all documents share this scope value in ZS.
    unless (defined $options{ aggregation_scope }) {
        $options{ aggregation_scope } = 'Archiefstuk';
    }

    $options{$_}            = $self->$_ for qw/modified_by deleted_by created_by/;

    ### Get relationships
    # my @relationships   = $self->result_source->relationships;

    # for my $column ($self->result_source->columns) {
    #     next if grep { $column eq $_ } @relationships;

    #     $options{$column} = $self->get_column($column);
    # }

    return %options;
}


sub annotation_count {
    my ($self) = @_;

    # don't bother with annotation if there is not case_id set.
    return 0 if !$self->get_column('case_id');

    my $schema = $self->result_source->schema;
    my $rs = $schema->resultset('FileAnnotation')->search({ file_id => $self->id });

    my $current_user = $schema->default_resultset_attributes->{ current_user };
    my $show_public  = $schema->resultset('Config')->get('pdf_annotations_public');
    if (!$current_user) {
        return $show_public ? $rs->count : 0;
    }

    if ($show_public) {
        return $rs->count;
    }
    else {
        return $rs->search({
            subject => 'betrokkene-medewerker-'
                . $current_user->uidnumber
        })->count;
    }
}

=head2 get_download_info

Easy access to oft required fields when serving a static file.

Returns returns path, mimetype, size and filename (composed from the name on
the file record and the extention that is the request format) and the filehandle.

Either:

    my %download_info = $file->get_download_info();

Or:

    my %download_info = $file->get_download_info('pdf');

%download_info will contain download information:

If the file is stored in the "filestore" table, the fields C<filestore>
and C<filename> will exist and contain the filestore item to send to the
user and the filename to give it.

If the file can't be downloaded in the specified format, one of two special
keys will be set: C<download_disabled> or C<download_waiting>. In those cases,
the caller should take care to send the appropriate special static file.

=cut

sub get_download_info {
    my ($self, $target_fmt) = @_;

    if ($self->confidential && !$self->case_id->confidential_access) {
        return (restricted => 1);
    }

    my @DOC_MIMETYPES = qw(
        application/vnd.openxmlformats-officedocument.wordprocessingml.document
        application/vnd.openxmlformats-officedocument.wordprocessingml.template
    );

    my $filestore = $self->filestore;
    my $too_big_to_convert = $filestore->size > MAX_CONVERSION_FILE_SIZE;

    my $virus_status = $filestore->virus_scan_status;

    if ($virus_status =~ /^found/) {
        throw(
            'download/virus_found',
            'File cannot be downloaded: virus found.'
        );
    }
    elsif ($virus_status eq 'pending') {
        return (download_scanning => 1);
    }

    if (!$target_fmt) {
        return (
            filestore => $filestore,
            filename  => $self->filename,
        );
    }

    if ($target_fmt eq 'odt') {
        if ($filestore->extension eq '.odt') {
            return (
                filestore => $filestore,
                filename  => $self->filename,
            );
        }
    }
    elsif ($target_fmt eq 'doc') {
        if (any { $filestore->mimetype eq $_ } @DOC_MIMETYPES) {
            return (
                filestore => $filestore,
                filename  => $self->filename,
            );
        }
        elsif ($self->may_convert_to_doc && !$too_big_to_convert) {
            my $filestore = $self->get_word_document;
            return (
                filestore => $filestore,
                filename  => $filestore->original_name,
            );
        }
    }
    elsif ($target_fmt eq 'pdf') {
        if ($filestore->mimetype eq 'application/pdf') {
            return (
                filestore => $filestore,
                filename  => $self->filename,
            );
        }
        elsif ($self->may_preview_as_pdf && !$too_big_to_convert) {
            my $filestore = $self->get_preview_pdf;
            if ($filestore) {
                return (
                    filestore => $filestore,
                    filename  => $self->name . $filestore->extension,
                );
            }
            else {
                return (download_waiting => 1);
            }
        }
    }

    return (download_disabled => 1);
}

=head2 get_thumbnail

Retrieves (or implicitly generates) a thumbnail indexed on the supplied
maximum height and width definitions.

    my $thumbnail = $file->get_thumbnail({
        max_width => 640,
        max_height => 480
    });

Returns a file_derivative object if found.

=cut

define_profile get_thumbnail => (
    optional => {
        max_width => 'Int',
        max_height => 'Int'
    },
    defaults => {
        max_width => 398,
        max_height => 565,
    }
);

sub get_thumbnail {
    my $self = shift;
    my $params = assert_profile(shift)->valid;

    my $thumbnail = $self->_get_thumbnail($params);
    return $thumbnail if defined $thumbnail;

    if ($self->may_preview_as_pdf) {
        $self->queue_preview_pdf;
        $self->queue_thumbnail(%$params);
    }
    return undef;
}

=head2 generate_thumbnail

Generate a fresh thumbnail for the provided height and width parameters.

    my $thumbnail = $file->generate_thumbnail({
        width => 640,
        height => 480
    });

=cut

define_profile generate_thumbnail => (
    optional => {
        width => 'Int',
        height => 'Int'
    },
);

sub generate_thumbnail {
    my $self = shift;
    my $params = assert_profile(shift)->valid;

    my $t0 = Zaaksysteem::StatsD->statsd->start;

    my $filestore = $self->filestore_id;

    my %geometry = (
        width  => $params->{width}  || 398,
        height => $params->{height} || 565,
    );

    my $thumbnail_tmp = $filestore->generate_thumbnail(%geometry);

    Zaaksysteem::StatsD->statsd->end('file.generate_thumbnail.time', $t0);
    Zaaksysteem::StatsD->statsd->increment('file.generate_thumbnail', 1);

    my $filestore_rs = $self->result_source->schema->resultset('Filestore');

    my $thumbnail_filestore = $filestore_rs->filestore_create({
        original_name => sprintf('tn_%sx%s_%s.png', $geometry{width}, $geometry{height}, $self->name),
        file_path => $thumbnail_tmp->filename
    });

    return $self->file_derivatives->create({
        type         => 'thumbnail',
        filestore_id => $thumbnail_filestore->id,
        max_width    => $geometry{width},
        max_height   => $geometry{height},
    });
}

=head2 may_preview_as_pdf

Can the file be previewed as PDF

=cut

sub may_preview_as_pdf {
    my $self = shift;
    return $self->filestore_id->may_preview_as_pdf;
}

=head2 may_convert_to_doc

Can the file be converter to a Word document

=cut

sub may_convert_to_doc {
    my $self = shift;
    return $self->filestore_id->may_convert_to_doc;
}

=head2 get_preview_pdf

Get the preview PDF. If the file cannot be previewed as PDF
we throw an error.

Then there is no preview PDF available we set a queue item to create
a preview PDF.

=cut

sub get_preview_pdf {
    my $self = shift;

    if (!$self->may_preview_as_pdf) {
        throw('file/preview/pdf', "Unable to show preview PDF for this file");
    }

    my $pdf = $self->_get_preview_pdf;
    return $pdf if $pdf;

    $self->log->trace(
        sprintf("Preview PDF not found. Adding queue item to create (file-id: %d).", $self->id)
    );
    $self->queue_preview_pdf;
    return;
}

=head2 get_word_document

Get a file as a word document

=cut

sub get_word_document {
    my $self = shift;

    if (!$self->may_convert_to_doc) {
        throw('file/convert/doc', "Unable to convert file to a Word document");
    }

    my $doc = $self->_get_word_document;
    return $doc if $doc;

    my $copy = $self->create_copy(
        extension => 'docx',
        type      => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        preview   => 0,
    );

    $self->add_preview($copy, 'docx');

    return $copy;

}

=head2 queue_thumbnail

Generate a queue-item for C<generate_thumbnail>

=cut

sub queue_thumbnail {
    my ($self, %params) = @_;

    return $self->_add_queue_item(
        'generate_thumbnail',
        sprintf('Generate thumbnail for file %d', $self->id),
        %params,
    );
}

=head2 queue_preview_pdf

Generate a queue-item for C<generate_preview_pdf>

=cut

sub queue_preview_pdf {
    my $self = shift;

    return $self->_add_queue_item(
        'generate_preview_pdf',
        sprintf('Generate preview pdf for file %d', $self->id),
    );
}

=head2 queue_set_search_terms

Generate a queue-item for C<set_search_terms>

=cut

sub queue_set_search_terms {
    my $self = shift;

    return $self->_add_queue_item(
        'set_search_terms',
        sprintf('Generate search terms for file %d', $self->id),
    );
}

=head2 generate_pdf

Generate a PDF

=cut

define_profile generate_pdf => (
    required => {
        preview => 'Bool',
    },
);

sub generate_pdf {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    my $copy = $self->create_copy(
        extension => 'pdf',
        type      => 'application/pdf',
        preview   => $opts->{preview},
    );

    if ($opts->{preview}) {
        $self->log->trace(
            sprintf("Generating previews (file-id: %d).", $self->id)
        );

        # Delete all old references
        $self->file_derivatives->search({type => 'pdf'})->delete;

        $self->add_preview($copy, 'pdf');
        $self->queue_set_search_terms;
        $self->queue_thumbnail;
    }
    else {
        $self->log->trace(
            sprintf("Not generating previews (file-id: %d).", $self->id)
        );
    }

    return $copy;
}

=head2 create_copy

Create a copy of the filestore object and return the filestore object

=cut

define_profile create_copy => (
    optional => {
        type      => 'Str',
        extension => 'Str',
        preview   => 'Bool',
        filestore => 'Zaaksysteem::Model::DB::Filestore',
    },
    defaults => {
        extension => 'pdf',
        type      => 'application/pdf',
        preview   => 1,
    },
    field_filters => {
        type => ['lc'],
    },
);

sub create_copy {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    my $t0 = Zaaksysteem::StatsD->statsd->start;

    my $fs = $opts->{filestore} // $self->filestore_id;
    my $case_id = $self->get_column('case_id');

    my $fh = File::Temp->new();

    my $filepath = $fs->convert({
        $case_id ? (case_id => $case_id) : (),
        target_file   => $fh->filename,
        target_format => $opts->{type},
    });

    my $name = $opts->{preview} ? 'preview_' . $self->name : $self->name;

    my $file = $self->result_source->schema->resultset('Filestore')->filestore_create({
        original_name  => sprintf("%s.%s", $name, $opts->{extension}),
        file_path      => "$filepath",
    });

    Zaaksysteem::StatsD->statsd->end('file.create_copy.time', $t0);
    Zaaksysteem::StatsD->statsd->increment('file.create_copy', 1);

    return $file;
}

=head2 may_copy_to_pdf

Tells you if the file may be copied to PDF

=cut

sub may_copy_to_pdf {
    my $self = shift;
    my $filestore = $self->filestore_id;
    return $filestore->may_copy_to_pdf;
}

=head2 save_copy

Save a copy of file as PDF

=cut

define_profile save_copy => (
    required => {
        subject => 'Any',
    },
    optional => {
        extension => enum([qw[pdf]]),
        type    => enum([qw[application/pdf]]),
    },
    defaults => {
        extension => 'pdf',
        type      => 'application/pdf',
    },
    field_filters => {
        type => ['lc'],
    },
);

sub save_copy {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    my $t0 = Zaaksysteem::StatsD->statsd->start;

    my $pdf;
    if ($self->may_preview_as_pdf) {
        $pdf = $self->_get_preview_pdf();
    } elsif (!$self->may_copy_to_pdf) {
        throw("documents/copy/pdf", "Not allowed to copy documents to PDF");
    }

    my $filestore_copy = $pdf;
    if (!$filestore_copy) {
        $filestore_copy = $self->create_copy(
            $pdf ? (filestore => $pdf) : (),
            preview   => 0,
            type      => $opts->{type},
            extension => $opts->{extension},
        );
    }

    my $case = $self->get_column('case_id');
    my $copy;

    if ($case) {
        my $previous = $self->result_source->resultset->search(
            {
                name         => $self->name,
                extension    => '.' . $opts->{extension},
                date_deleted => undef,
                case_id      => $case,
            },
            {
                order_by => { -asc => 'version' },
                limit    => 1,
            }
        );

        my $first_version = $previous->first;

        my $accepted = 0;
        my $behandelaar = $self->case->behandelaar;

        if ($behandelaar) {
            my $case_subject = sprintf "betrokkene-%s-%s", (
                $self->case->behandelaar->betrokkene_type,
                $self->case->behandelaar->betrokkene_id,
            );
            if ($case_subject eq $opts->{subject}->old_subject_identifier) {
                $accepted = 1;
            }
        }

        $copy = $self->_copy_file(
            name      => $self->name,
            ext       => '.' . $opts->{extension},
            filestore => $filestore_copy,
            subject   => $opts->{ subject },
            accepted  => $accepted,
            root_file_id => $first_version ? $first_version->id : undef,
            creation_reason => "Bestand gekopieerd",
            uuid => create_uuid_as_string(UUID_V4),
        );

        if ($first_version && $copy->accepted) {
            $copy->version($copy->get_last_version->version + 1);
            $copy->make_active_version($opts->{ subject });
        }
        elsif ($first_version) {
            $copy->is_duplicate_of($first_version->id)
        }

        $copy->update(undef, { subject => $opts->{ subject } })->discard_changes;
    }
    else {
        $copy = $self->_copy_file(
            name      => $self->name,
            ext       => '.' . $opts->{extension},
            filestore => $filestore_copy,
            subject   => $opts->{ subject },
            creation_reason => "Bestand gekopieerd",
            uuid => create_uuid_as_string(UUID_V4),
        );
    }

    my @log_data = (
        'case/document/copy',
        {
            component    => 'document',
            component_id => $copy->id,
            data         => {
                $case ? (case_id => $case) : (),
                original_filename => sanitize_filename($self->filename),
                original_version  => $self->version,
                copy_filename     => sanitize_filename($copy->filename),
                copy_version      => $copy->version,
            }
        }
    );

    if ($case) {
        $self->case->trigger_logging(@log_data);
    }
    else {
        $self->result_source->schema->resultset('Logging')->trigger(@log_data);
    }

    Zaaksysteem::StatsD->statsd->end('file.save_copy.time', $t0);
    Zaaksysteem::StatsD->statsd->increment('file.save_copy', 1);

    return $copy;
}

=head2 add_preview

Add a filestore object as a preview

=cut

sub add_preview {
    my ($self, $copy, $type) = @_;

    $type //= 'pdf';

    $self->file_derivatives->create(
        {
            filestore_id => $copy->id,
            type         => $type,
            max_width    => 0,
            max_height   => 0,
        }
    );
    return 1;
}


=head2 get_subject_name

Get the display name for any given subject.

=cut

sub get_subject_name {
    my ($self, $subject) = @_;
    return $self->result_source->schema->betrokkene_model->get({}, $subject)->display_name;
}

=head2 $self->get_valid_filename($name, $extension, $case_id)

Returns a valid filename to prevent duplicates.

Example: file.txt already exists for a given case, it then appends (1) to the
name. Every future occurence results in a +1 in this version suffix.

=cut


sub get_valid_filename {
    my $self = shift;
    my ($name) = @_;

    # If there is no case, we cannot say there are duplicate files (yet).
    return $name if !$self->case;

    my $files = $self->result_source->resultset->search({
        id        => {'!=' => $self->id},
        name      => { '~*' => quotemeta($name) . '(\ \(\d+\))?' },
        case_id   => $self->case->id,
        extension => $self->extension,
    });

    # No existing files found, return the name as is
    if ($files->count == 0) {
        return $name;
    }

    # Suffix in this context means the 'version' a file has.
    my $suffix;
    while (my $f = $files->next) {
        # Extract the suffix from the current file
        my ($f_suffix) = ($f->name =~ m/.*\((\d+)\)/);

        # If the current suffix + 1 is greater than the highest registered, it becomes leading.
        if ($f_suffix && $f_suffix+1 > $suffix) {
            $suffix = $f_suffix+1;
        }
        # First file with a suffix
        elsif (!$f_suffix && !$suffix) {
            $suffix = 1;
        }
    }
    return sprintf "%s (%d)", ($name, $suffix);
}

sub trigger {
    my ($self, $subtype, $opts) = @_;
    $opts //= {};

    # Logging outside of the case context is nonsensical,
    # no one will read those events
    return unless $self->get_column('case_id');

    my $event_fields = {
        component    => 'document',
        component_id => $self->id,
        zaak_id      => $self->get_column('case_id'),
        data         => $opts
    };

    my $type = sprintf('case/document/%s', $subtype);

    my $log = $self->case->trigger_logging(
        $type,
        $event_fields
    );

    my $action;
    if ($subtype eq 'create') {
        $action = "aangemaakt";
    }
    elsif ($subtype eq 'accept') {
        $action = "geaccepteerd";
    }
    elsif ($subtype eq 'rename') {
        $action = "hernoemd";
    }
    elsif ($subtype eq 'reject') {
        $action = "geweigerd";
    }
    elsif ($subtype eq 'merged') {
        $action = 'samengevoegd'
    }
    else {
        $action = $subtype;
    }

    if ($opts->{disable_message}) {
        return;
    }

    if ($subtype eq 'create' || $subtype eq 'accept' || $subtype eq 'merged' || !$self->accepted) {
        $self->case_id->create_message_for_behandelaar(
            message    => sprintf("Document '%s' %s", sanitize_filename($self->name), $action),
            event_type => $type,
            log        => $log,
        );
    }
}

=head2 $self->filename

Returns the concatenation of name and extension separated by a comma, e.g
klaymen.gif

=cut

sub filename {
    my $self    = shift;

    return $self->name . $self->extension; # NOTE: extension is including leading dot
}

=head2 is_rejectable

Checks if a file is rejectable. This is only true when a file is not accepted, has a case_id linked to it and is rejectable to the queue.
Otherwise you cannot reject it, but must delete it.

=cut

sub is_rejectable {
    my $self = shift;
    return 1 if (!$self->accepted && $self->get_column('case_id') && $self->reject_to_queue);
    return 0;
}

=head2 filepath

Returns the full 'virtual' path of the file representation in Zaaksysteem. Within a case you can put files in (sub)directories.
This is NOT the path of the file on disk, use L<Zaaksysteem::Backend::Filestore::Component::get_path> for this purpose.

=head3 SYNOPSIS

    my $path_in_case = $file->filepath;
    my $path_on_disk = $file->filestore->get_path;

=cut

sub filepath {
    my $self = shift;

    my $dir = $self->directory;
    return $dir ? catfile($dir->name, $self->filename) : $self->filename;
}

=head2 $self->loglines

Returns a DBIx::Class::Resultset of all loglines related to this file.

=cut

sub loglines {
    my $self    = shift;

    my $search = $self->result_source->schema->resultset('Logging')->search({
        component    => 'document',
        component_id => $self->id,
    });
    return $search;
}

=head2 apply_case_document_defaults

Applies the defaults belonging to the case type document. This method can only
function if there is a single case document set.

=cut

sub apply_case_document_defaults {
    my $self = shift;
    my @cds  = $self->case_documents;

    return unless scalar @cds == 1;

    my $cd = $cds[0]->case_document_id;

    my $library_property = $cd->bibliotheek_kenmerken_id;
    if ($library_property && $library_property->file_metadata_id) {
        my $md = $library_property->file_metadata_id;
        my %opts;
        my @columns = $md->result_source->columns;
        for my $c (@columns) {
            next if $c eq 'id';
            if ($md->$c) {
                $opts{$c} = $md->$c;
            }
        }
        if (my $value = $library_property->value_default) {
            $opts{description} = $value;
        }

        delete $opts{trust_level} if $self->confidential;

        $self->update_metadata({%opts});
    }

    $self->update({
        publish_pip => $cd->pip || 0,
        publish_website => $cd->publish_public || 0
    });

    return $self->discard_changes;
}


sub has_label {
    my $self = shift;
    my $label = shift;

    unless($label) {
        return $self->case_documents->count > 0 ? 1 : 0;
    }

    return grep
        { $_->case_document->library_attribute->magic_string eq $label }
        $self->case_documents;
}

sub labels {
    my $self = shift;

    return map { $_->case_document->label } $self->case_documents;
}

sub get_case_document_ids {
    my $self = shift;
    return uniq
        $self->case_documents->get_column('case_document_id')->all;
}
=head2 set_or_replace_case_documents

Method to set new case documents for a file. Requires a list of case
document IDs to be given. All existing entries will be deleted.

=cut

sub set_or_replace_case_documents {
    my ($self, @case_document_ids) = @_;

    @case_document_ids = uniq map { ref $_ eq 'ARRAY' ? @$_ : $_ }
        @case_document_ids;

    my @old_case_documents = $self->get_case_document_ids;

    my @kenmerken = $self->case_id->zaaktype_node_id->zaaktype_kenmerken->search(
        { 'id' => { in => [@old_case_documents, @case_document_ids] }, },
        { join => 'bibliotheek_kenmerken_id' })->all;

    for my $zid (@old_case_documents, @case_document_ids) {
        my $action;
        my ($object) = grep { $zid == $_->id } @kenmerken;
        if (grep({ $zid == $_ } @old_case_documents) && !grep( { $zid == $_ } @case_document_ids)) {
            $action = 'remove';
        } elsif (!grep({ $zid == $_ } @old_case_documents) && grep( { $zid == $_ } @case_document_ids)) {
            $action = 'add';
        }

        if ($action) {
            $self->trigger_label_logging($action, $object);
        }
    }

    # Delete existing
    $self->case_documents->delete;

    # Create new entries
    my $fcd = $self->result_source->schema->resultset('FileCaseDocument');
    for my $cid (@case_document_ids) {
        $fcd->create({
            case_document_id => $cid,
            file_id          => $self->id,
        });
    }

    $self->set_case_documents_in_case_property(
        @old_case_documents,
        @case_document_ids,
    );

    return $self;
}

sub set_case_documents_in_case_property {
    my ($self, @documents) = @_;

    my $case = $self->case_id;

    my $rs = $case->zaaktype_node_id->zaaktype_kenmerken->search_rs(
        {
            @documents ? ('me.id' => { -in => \@documents }) : (),
            'bibliotheek_kenmerken_id.value_type' => 'file',
        },
        { prefetch => 'bibliotheek_kenmerken_id', },
    );

    my @library_attribute_id;

    while (my $attribute = $rs->next) {
        push(@library_attribute_id, $attribute->get_column('bibliotheek_kenmerken_id'));

        $self->_document_to_case_property($case, $attribute);
    }
    $case->ddd_update_system_attribute('case.case_documents');
    $self->_set_referential_case_documents($case, @library_attribute_id);
}

sub _document_to_case_property {
    my ($self, $case, $casetype_attribute) = @_;

    my $library_attribute  = $casetype_attribute->bibliotheek_kenmerken_id;
    my $oa = Zaaksysteem::Object::Attribute->new(
        name  => "attribute." . $library_attribute->magic_string,
        label => $casetype_attribute->label
            || $library_attribute->label
            || $library_attribute->naam,
        attribute_type => 'text',    # or so it seems
        bwcompat_type  => 'file',
        bwcompat_name  => $library_attribute->magic_string,
        property_name  => $library_attribute->magic_string,
        grouping       => 'case',
        object_row     => $casetype_attribute,
    );

    my $found = $case->file_field_documents($library_attribute->id);
    $oa->value($found);
    $case->update_case_properties($oa);
    return;
}

sub _set_referential_case_documents {
    my ($self, $case, @attribute_id) = @_;

    my $rs = $self->result_source->schema->resultset('Zaak')->search_rs(
        { pid => $case->id }
    );

    while(my $child = $rs->next) {
        my $attribute_rs = $child->zaaktype_node_id->zaaktype_kenmerken->search_rs(
            {
                @attribute_id
                    ? ('me.bibliotheek_kenmerken_id' => { -in => \@attribute_id })
                    : (),
                'me.referential' => 1,
                'bibliotheek_kenmerken_id.value_type' => 'file',
            },
            { prefetch => 'bibliotheek_kenmerken_id', },
        );

        while (my $attribute = $attribute_rs->next) {
            $self->_document_to_case_property($child, $attribute);
        }
        $self->_set_referential_case_documents($child, @attribute_id);
    }
}

=head2 move_case_document

Method to easily move a case document assignment from one document to another.

This should be called on the target file, passing the 'from' file as a parameter.

=cut

Params::Profile->register_profile(
    method  => 'move_case_document',
    profile => {
        required => [qr/subject from case_document_id/],
    }
);

sub move_case_document {
    my $self = shift;
    my $opts = $_[0];

    my $dv = Params::Profile->check(
        params  => $opts,
    );
    my $valid = $dv->valid;

    my $kenmerk = $self->result_source->schema->resultset('ZaaktypeKenmerken')->search(
        {
            'id' => $opts->{case_document_id},
        },
        {
            join => 'bibliotheek_kenmerken_id',
        }
    )->first;

    $self->result_source->schema->resultset('FileCaseDocument')->search({
        file_id => $opts->{from},
        case_document_id => $opts->{case_document_id},
    })->delete;

    $self->result_source->schema->resultset('FileCaseDocument')->find_or_create({
        file_id => $self->id,
        case_document_id => $opts->{case_document_id},
    });

    my $from_file = $self->result_source->resultset->find($opts->{from});
    $from_file->trigger_label_logging('remove', $kenmerk);
    $self->trigger_label_logging('add', $kenmerk);

    $self->apply_case_document_defaults;

    $self->discard_changes;

    $self->$self->set_case_documents_in_case_property(
        $opts->{from},
        $opts->{case_document_id}
    );

    return $self->discard_changes;
}

=head2 trigger_label_logging

Trigger logging on a case document file

    my $action = 'add';    # or remove
    my $attribute
        = $file->result_source->schema->resultset('ZaaktypeKenmerken')
        ->search(
        { 'id' => 42 },
        { join => 'bibliotheek_kenmerken_id' }
        )->first;
    $file->trigger_label_logging($action, $attribute);

=cut

sig trigger_label_logging => 'Str, Zaaksysteem::Model::DB::ZaaktypeKenmerken';

sub trigger_label_logging {
    my ($self, $action, $kenmerk) = @_;

    return unless $self->get_column('case_id');

    $self->case->trigger_logging(
        'case/document/label', {
            component => 'document',
            data => {
                action      => $action,
                label       => $kenmerk->label || $kenmerk->bibliotheek_kenmerken_id->naam,
                file_id     => $self->id,
                file_name   => sanitize_filename($self->filename),
                case_id     => $self->get_column('case_id'),
                mimetype    => $self->filestore_id->mimetype,
            }
        }
    );
    return 1;
}


=head2 reject

This called when the case owner (behandelaar) doesn't allow a file to go in.
If this file is linked to a scheduled job, we give the job a chance to do cleanup

=cut

sub reject {
    my ($self) = @_;

    if (my $scheduled_job = $self->scheduled_jobs_id) {
        $scheduled_job->apply_roles->reject;
    }
}

=head2 condense_content

=cut

sub condense_content {
    my $self = shift;

    return try {
        my $text;

        my $mt = $self->filestore->mimetype;

        # Don't set it yet - just make sure it doesn't go out of scope when the
        # if() tree below ends. ->get_path() is an expensive operation we might
        # not want to do in all cases.
        my $path;

        if($mt eq 'text/plain') {
            $text = BTTW::Tools::Text->new(source => $self->filestore->content);
        }
        elsif ($mt eq 'text/html') {
            $path = $self->filestore->get_path;

            my $html = BTTW::Tools::Text::HTML->new(file => "$path");
            $text = BTTW::Tools::Text->new(source => $html);
        }
        elsif ($mt eq 'application/pdf') {
            $path = $self->filestore->get_path;

            my $pdf = BTTW::Tools::Text::PDFHack->new(file => "$path");
            $text = BTTW::Tools::Text->new(source => $pdf);
        }
        elsif ($mt eq 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
            $path = $self->filestore->get_path;

            my $ooxml = BTTW::Tools::Text::OOXML->new(file => "$path");
            $text = BTTW::Tools::Text->new(source => $ooxml);
        }
        elsif ($mt eq 'application/vnd.oasis.opendocument.text') {
            $path = $self->filestore->get_path;

            my $odt = BTTW::Tools::Text::OpenDocument->new(file => "$path");
            $text = BTTW::Tools::Text->new(source => $odt);
        }
        elsif ($self->may_preview_as_pdf) {
            my $pdf = $self->_get_preview_pdf;
            return undef unless $pdf;

            $path = $pdf->get_path;
            $text = BTTW::Tools::Text->new(source => BTTW::Tools::Text::PDFHack->new(file => "$path"));
        }

        return undef unless blessed $text;

        my @keywords = $text->stemmed_words;

        return undef unless scalar @keywords;

        return join " ", @keywords;
    };
}

sub insert {
    my $self    = shift;

    my $rv      = $self->next::method(@_);
    if (ref($self->case_id)) {
        $self->case_id->touch;
    }

    if ($self->may_preview_as_pdf) {
        $self->get_preview_pdf;
    }
    elsif ($self->extension ne '.mime' and $self->extension ne '.eml') {
        $self->queue_set_search_terms;
    }

    # PDFs are special - they "may preview as PDF", but we need to generate
    # search terms and a thumbnail anyway.
    if($self->filestore->mimetype eq 'application/pdf') {
        $self->queue_set_search_terms;
        $self->queue_thumbnail;
    }

    return $rv;
};

sub update {
    my $self    = shift;
    my $update  = shift;
    my $opts    = shift;

    my $no_update = delete $opts->{no_search_term_update};
    my $subject = delete $opts->{ subject };

    $self->assert_no_lock($subject);

    my $rv = $self->next::method($update, $opts, @_);

    if (ref($self->case_id)) {
        $self->case_id->touch;
    }

    return $rv if $no_update;

    $rv = $rv->discard_changes;

    if (!$rv->date_deleted && !$rv->destroyed && $rv->active_version) {
       if ($rv->may_preview_as_pdf) {
           $rv->get_preview_pdf;
       }

       $rv->queue_set_search_terms;
    }

    return $rv;
};

=head2 set_search_terms

Set the search term on a file

=cut

sub set_search_terms {
    my $self = shift;

    my $terms = $self->condense_content;

    $self->update(
        {
            search_index => $terms // undef,
            search_term  => $self->name // undef,
        },
        { no_search_term_update => 1 }
    );

    return;
}

=head2 assert_no_lock

Assert that the file row is not locked.

Optionally takes a L<Zaaksysteem::Object::Types::Subject> argument to check
against the lock owner, should the file be locked.

=cut

sig assert_no_lock => '?Zaaksysteem::Object::Types::Subject';

sub assert_no_lock {
    my $self = shift;
    my $subject = shift;

    if ($self->is_locked($subject)) {
        throw('file/lock/file_locked', sprintf(
            'Subject "%s" has a lock on file "%s" until "%sZ"',
            $self->lock_subject_name,
            $self->filename,
            $self->lock_timestamp->iso8601
        ));
    }
}

=head2 is_locked

Returns a boolean flag indicating whether the file is actively being locked.

Optionally takes a L<Zaaksysteem::Object::Types::Subject> argument to check
against the lock owner, should the file be locked.

=cut

sig is_locked => '?Zaaksysteem::Object::Types::Subject';

sub is_locked {
    my $self = shift;
    my $subject = shift;

    # If the file is not associated with a case, locking has no meaning.
    return unless $self->get_column('case_id');

    # No timestamp, no lock
    return unless $self->lock_timestamp;

    # Timestamp is *after* now, implies lock expired
    return if DateTime->now > $self->lock_timestamp;

    # No subject supplied, assume current_user is /not/ lock owner
    return 1 unless defined $subject;

    # Lock owner is supplied subject, implies not locked
    return if $self->lock_subject_id eq $subject->id;

    # File is locked
    return 1;
}

=head2 acquire_lock

Acquire a lock on a file for the supplied user. Lock duration is set to
now() + 15 minutes.

=cut

sig acquire_lock => 'Zaaksysteem::Object::Types::Subject, ?Int';

sub acquire_lock {
    my $self = shift;
    my $subject = shift;
    my $minutes = shift || 15;

    return $self->update({
        lock_timestamp => DateTime->now->add(minutes => $minutes),
        lock_subject_id => $subject->id,
        lock_subject_name => $subject->display_name
    }, { subject => $subject });
}

=head2 release_lock

Releases a lock (if one exists) by updating the file row.

=cut

sig release_lock => 'Zaaksysteem::Object::Types::Subject';

sub release_lock {
    my $self = shift;
    my $subject = shift;

    return $self->update({
        lock_timestamp => undef,
        lock_subject_id => undef,
        lock_subject_name => undef
    }, { subject => $subject });
}

=head2 extend_lock

Refreshes the expiration of an existing lock with now() + 15 minutes.

=cut

sig extend_lock => 'Zaaksysteem::Object::Types::Subject';

sub extend_lock {
    my $self = shift;
    my $subject = shift;
    my $minutes = shift || 15;

    return $self->update({
        lock_timestamp => DateTime->now->add(minutes => $minutes)
    }, { subject => $subject });
}

=head2 get_lock_fields

Returns a map of fields with lock data

=cut

sub get_lock_fields {
    my $self = shift;

    my @fields = qw[lock_timestamp lock_subject_id lock_subject_name];

    return map { $_ => undef } @fields unless $self->is_locked;

    return map { $_ => $self->$_ } @fields;
}

=head2 lock_as_object

Returns a L<Zaaksysteem::Object::Types::File::Lock> instance or nothing,
depending on the current lock state of the file.

=cut

sub lock_as_object {
    my $self = shift;

    return unless $self->is_locked;

    return Zaaksysteem::Object::Types::File::Lock->new(
        date_expires => $self->lock_timestamp,
        subject => Zaaksysteem::Object::Reference::Instance->new(
            type => 'subject',
            id => $self->lock_subject_id
        )
    );
}

sub _get_word_document {
    my $self = shift;

    my @ms_word_mimetypes = (
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.template',
    );

    if (any { $self->filestore_id->mimetype eq $_ } @ms_word_mimetypes) {
        return $self->filestore_id;
    }

    my $doc = $self->file_derivatives->search(
        { type => 'docx' },
        { prefetch => 'filestore_id' }
    )->first;

    return $doc ? $doc->filestore_id : undef;
}

sub _get_thumbnail {
    my ($self, $params) = @_;

    return $self->file_derivatives->search(
        {
            %{$params // {}},
            type   => 'thumbnail'
        }
    )->first;
}

sub _add_queue_item {
    my ($self, $type, $label, %data) = @_;

    $self->log->trace(sprintf("Adding queue item. Label: '%s', type: '%s' (file-id: %d)", $label, $type, $self->id));

    # This uses the same ordering of overriding hash keys as the create_item
    # call below ends up with.
    my $queue_data = JSON::XS->new->canonical(1)->utf8(1)->encode(
        {
            %data,
            file_id => $self->id,
        }
    );

    my $schema = $self->result_source->schema;
    $schema->lock(ADD_QUEUE_ITEM_LOCK);

    my $existing = $schema->resultset('Queue')
        ->search(
            {
                status => [ 'pending', 'waiting' ],
                type => $type,
                label => $label,
                data => $queue_data,
            }
        )->first;

    if ($existing) {
        $schema->unlock(ADD_QUEUE_ITEM_LOCK);
        $self->log->trace("Found existing queue-item for this task.");
        return 1;
    }

    # XXX: Domain break, queue model should be injected into this code path.
    my $qrs = $schema->resultset('Queue');

    my $item = $qrs->create_item($type, {
        label => $label,
        metadata => {
            target               => 'backend',
            require_object_model => 0,
        },
        data  => {
            %data,
            file_id => $self->id,
        }
    });

    $schema->unlock(ADD_QUEUE_ITEM_LOCK);

    $qrs->queue_item($item);

    return 1;
}

sub _get_preview_pdf {
    my $self = shift;

    if ($self->filestore_id->mimetype eq 'application/pdf') {
        return $self->filestore_id;
    }

    my $pdf = $self->file_derivatives->search({ type => 'pdf' },
        { prefetch => 'filestore_id' })->first;

    my $rv = $pdf ? $pdf->filestore_id : undef;

    if (!$rv) {
        $self->log->trace(
            sprintf("Preview PDF not found during internal request (file-id: %d).", $self->id)
        );
    }

    return $rv;
}

define_profile _copy_file => (
    required => {
        subject   => 'Zaaksysteem::Object::Types::Subject',
        filestore => 'Zaaksysteem::Model::DB::Filestore',
    },
    optional => {
        case_id               => 'Int',
        accepted              => 'Bool',
        name                  => 'Str',
        ext                   => 'Str',
        root_file_id          => 'Any',
        directory_id          => 'Any',
        version               => 'Int',
        creation_reason       => 'Str',
        date                  => 'DateTime',
        ignore_case_documents => 'Bool',
        ignore_locks          => 'Bool',
        uuid                  => 'Str',
    },
    defaults => {
        version => 1,
        date    => sub { DateTime->now() },
    }
);

sub _copy_file {
    my $self = shift;
    my %arguments = @_;
    my $opts = assert_profile(\%arguments)->valid;

    # Data::FormValidator removes keys with 'undef' values from the argument
    # list.  We really want to be able to pass in an undefined value
    for my $key (qw/root_file_id directory_id/) {
        $opts->{$key} = undef
            if(exists $arguments{$key} and not exists $opts->{$key})
    }

    my $subject               = delete $opts->{subject};
    my $date                  = delete $opts->{date};
    my $ext                   = delete $opts->{ext};
    my $ignore_case_documents = delete $opts->{ignore_case_documents};
    my $ignore_locks          = delete $opts->{ignore_locks};

    my $schema = $self->result_source->schema;
    $date = $schema->format_datetime_object($date);

    # No extension provided, try our best to guesstimate a correct one.
    unless (defined $ext) {
        $ext = $opts->{ filestore }->extension // $self->extension;
    }

    if ($ignore_locks) {
        foreach (qw(lock_timestamp lock_subject_id lock_subject_name)) {
            $opts->{$_} = undef;
        }
    }

    my $copy = undef;
    do {
        local $schema->source('File')->relationship_info('file_case_documents')->{attrs}{cascade_copy} = 0
            if $ignore_case_documents;
        $copy = $self->copy({
            extension     => $ext,
            created_by    => $subject->old_subject_identifier,
            modified_by   => $subject->old_subject_identifier,
            date_created  => $date,
            date_modified => $date,
            metadata_id   => undef,
            %$opts,
        });
    };

    if (my $id = $self->get_column('metadata_id')) {
        my $md_rs = $self->result_source->schema->resultset('FileMetadata');
        my $md = $md_rs->search_rs(
            {
                id => $id
            },
            {
                result_class => 'DBIx::Class::ResultClass::HashRefInflator'
            }
        )->first;

        my $opts = {};

        if (defined $subject) {
            $opts->{ subject } = $subject;
        }

        $copy->update_metadata($md, $opts);
    }

    return $copy;
}

sub as_object {
    my $self = shift;

    my $seq = $self->get_column('root_file_id') // $self->id;

    my %args = (
        name          => $self->name,
        version       => $self->version,
        file          => $self->filestore_id->as_object,
        number        => $seq,
        filename      => $self->filename,
        date_modified => $self->date_modified // $self->date_created,
        date_created  => $self->date_created,
    );

    if (my $case_id = $self->get_column('case_id')) {
        # Quick hack to prevent massive case query
        my $entry = $self->result_source->schema->resultset('ObjectData')->search_rs(
            {
                object_id    => $case_id,
                object_class => 'case',
            }
        )->first;

        throw(
            'document/as_object/case_id/reference/not_found',
            "Unable to find reference to case id $case_id"
        ) unless $entry;

        $args{case} = Zaaksysteem::Object::Reference::Instance->new(
            id   => $entry->get_column('uuid'),
            type => 'case',
        );
    }

    # TODO: Files uploaded via the PIP do not have metadata..
    if ($self->get_column('metadata_id')) {
        $args{metadata} = $self->metadata_id->as_object;
        $args{metadata}->status($self->document_status);
    }

    return Zaaksysteem::Object::Types::Document->new(%args);
}

=head2 is_present_in_case

Check if a file is present in another case by ID

=cut

sig is_present_in_case => 'Int';

sub is_present_in_case {
    my ($self, $id) = @_;
    my $file = $self->filestore_id->files->search_rs(
        {
            case_id        => $id,
            active_version => 1,
            date_deleted   => undef,
            destroyed      => 0
        }
    )->first;
    return $file ? 1 : 0;
}

=head2 copy_to_case

Copies a file into another case and returns a L<Zaaksysteem::Backend::File>.
The caller should check if the case is eligable for writing and/or the user has
access to the case.

=head3 required named arguments

=over

=item case

L<Zaaksysteem::Model::DB::Zaak>

=item subject

L<Zaaksysteem::Object::Types::Subject>

=back

=cut

define_profile copy_to_case => (
    required => {
        case    => 'Zaaksysteem::Model::DB::Zaak',
        subject => 'Zaaksysteem::Schema::Subject',
    },
    optional => {
        directory => 'Defined',
    },
);

sub copy_to_case {
    my $self = shift;
    my %arguments = @_;
    my $opts = assert_profile(\%arguments)->valid;

    my $subject     = delete $opts->{subject};
    my $target_case = delete $opts->{case};
    my $directory   = delete $opts->{directory};

    my $target_id = $target_case->id;
    if ($self->is_present_in_case($target_id)) {
        throw(
            'copy_to_case/already_exists',
            "File already exists on the target case"
        );
    }

    # only if the current user is the same as the behandelaar of the taget_case
    # which can be unassigned
    my $target_user_uuid = $target_case->behandelaar && $target_case->behandelaar->subject_id;
    my $accepted = $target_user_uuid eq $subject->uuid;

    my $copy_params = {
        filestore             => $self->filestore_id,
        subject               => $subject->as_object(),
        root_file_id          => undef,
        directory_id          => $directory ? $directory->id : undef,
        case_id               => $target_id,
        accepted              => $accepted ? 1 : 0,
        ignore_case_documents => 1,
        ignore_locks          => 1,
        uuid                  => create_uuid_as_string(UUID_V4),
    };

    my $copy = $self->_copy_file(%$copy_params);

    my $logging_params = $self->_logging_params(
        target_case => $target_case,
        copy_file   => $copy,
    );

    $self->case->trigger_logging(
        'case/document/copy_case_into' => {
            component    => 'document',
            component_id => $self->id,
            data         => $logging_params,
        }
    );

    $copy->case->trigger_logging(
        'case/document/copy_case_from' => {
            component    => 'document',
            component_id => $copy->id,
            data         => $logging_params,
        }
    );

    return $copy;
}

sub _logging_params {
    my $self = shift;
    my %arguments = @_;

    my $params;

    my $target_case = $arguments{target_case};
    $params->{target_case_id        } = $target_case->id;
    $params->{target_case_subject   } = $target_case->onderwerp;

    my $source_case = $self->case;
    $params->{source_case_id        } = $source_case->id;
    $params->{source_case_subject   } = $source_case->onderwerp;

    my $copy = $arguments{copy_file};
    $params->{copy_file_id          } = $copy->id;
    $params->{copy_filename         } = $copy->filename;
    $params->{copy_version          } = $copy->version;

#   my $original = $self
    $params->{original_file_id      } = $self->id;
    $params->{original_filename     } = $self->filename;
    $params->{original_version      } = $self->version;

    return $params
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 MIMETYPES_ALLOWED

TODO: Fix the POD

=cut

=head2 annotation_count

TODO: Fix the POD

=cut

=head2 has_label

TODO: Fix the POD

=cut

=head2 insert

TODO: Fix the POD

=cut

=head2 labels

TODO: Fix the POD

=cut

=head2 log

TODO: Fix the POD

=cut

=head2 make_active_version

TODO: Fix the POD

=cut

=head2 trigger

TODO: Fix the POD

=cut

=head2 update

TODO: Fix the POD

=cut

=head2 update_existing

TODO: Fix the POD

=cut

