package Zaaksysteem::Backend::Sysin::TransactionRecordToObject::Component;

use strict;
use warnings;

use Moose;

extends 'Zaaksysteem::Backend::Component';

=head1 NAME

Zaaksysteem::Backend::Sysin::TransactionRecordToObject::Component

=head1 SYNOPSIS

=head1 DESCRIPTION

These methods provides transaction specific actions.

=head1 ATTRIBUTES

=head2 _json_data

ISA: HashRef

Defines the json_data for this row.

=cut

has '_json_data'    => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self                    = shift;

        no strict 'refs';
        no warnings 'redefine';
        *DateTime::TO_JSON          = sub { shift->iso8601 };
        use strict;

        my %optional;
        my ($obj_subscription) = $self->result_source->schema->resultset('ObjectSubscription')->search_filtered({
            local_id => $self->local_id,
            local_table => $self->local_table,
        });

        my $transaction_record = $self->transaction_record_id;

        my $pub_info = {
            id          => $self->id,
            local_table => $self->local_table,
            local_id    => $self->local_id,
            mutations   => $self->mutations,
            mutation_type         => $self->mutation_type,
            # Do NOT let this return a full object of either transaction or
            # transaction_record -- it'll loop.
            transaction_record_id => {
                id => $transaction_record->id,
                transaction_id => {
                    id => $transaction_record->get_column('transaction_id')
                },
            },
            object_subscription   => $obj_subscription,
        };

        return $pub_info;
    },
);

=head2 object_subscription_delete()

Marks the related ObjectSubscription as deleted.

=cut

sub object_subscription_delete {
    my $self    = shift;

    my ($obj_subscription) = $self->result_source->schema->resultset('ObjectSubscription')->search({
        local_id => $self->local_id,
        local_table => $self->local_table,
        date_deleted => undef,
    });

    # No point in erroring about something already being deleted
    if (!$obj_subscription) {
        return [];
    }

    $obj_subscription->update({date_deleted => DateTime->now});

    return $self->discard_changes;
}

1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

