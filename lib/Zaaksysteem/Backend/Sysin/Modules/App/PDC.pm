package Zaaksysteem::Backend::Sysin::Modules::App::PDC;

use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
/;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::APP::PDC - Interface to configure a PDC App in zaaksysteem

=head1 DESCRIPTION

This module allows the configuration of a PDC App on zaaksysteem.nl

=cut

use BTTW::Tools;

use constant INTERFACE_ID => 'app_pdc';

use constant INTERFACE_CONFIG_FIELDS => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_title',
        type        => 'text',
        label       => 'Header titel',
        required    => 1,
        description => 'Voer een titel in voor de app.'
    ),

    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_header_bgimage',
        type        => 'text',
        label       => 'Header plaatje',
        description => 'Voer een achtergrondplaatje in. Dit wordt getoond in de header van de app. Let op: het plaatje moet via https geserveerd worden.',
        data        => { pattern => '^https:\/\/.+' },
    ),

    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_header_bgcolor',
        type        => 'text',
        label       => 'Header kleur',
        required    => 0,
        description => 'Voer een achtergrondkleur in die wordt getoond in de header van de app. Gebruik hiervoor een hex kleurcode. Bijvoorbeeld #ff0000 voor rood.'
    ),

    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_accent_color',
        type        => 'text',
        label       => 'Accent kleur',
        required    => 0,
        description => 'Voer een kleur in die wordt gebruikt voor de tabs van de navigatie. Bijvoorbeeld #ff0000 voor rood.'
    ),

    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_objects',
        type => 'multiple',
        label => 'Objecttypes',
        description => 'Geef op welke objecttypes gebruikt moeten worden.',
        data => {
            fields => [
                {
                    name => 'objecttype',
                    type => 'spot-enlighter',
                    label => 'Objecttype',
                    description => '',
                    data => {
                        restrict => 'objecttypes',
                        placeholder => 'Type uw zoekterm',
                        label => 'label'
                    }
                },
                {
                    name => 'objecttype_subject_attribute',
                    type => 'text',
                    label => 'Label',
                    description => 'Geef op welk kenmerk gebruikt moet worden als label'
                },
            ]
        }
    ),

    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_app_uri',
        type => 'display',
        label => 'App URL',
        description => 'De link naar de applicatie',
        data => {
            template => '<a href="<[field.value]>" target="_blank"><[field.value]></a>'
        }
    )
];

use constant MODULE_SETTINGS => {
    name                          => INTERFACE_ID,
    module_type                   => ['apiv1', 'app', 'pdc'],
    label                         => 'App - Producten en Diensten-Catalogus',
    interface_config              => INTERFACE_CONFIG_FIELDS,
    direction                     => 'incoming',
    manual_type                   => ['text'],
    description                   => '<a target="_blank" href="http://wiki.zaaksysteem.nl/wiki/Redirect_app_pdc">Documentatie</a>',
    is_multiple                   => 0,
    is_manual                     => 0,
    retry_on_error                => 0,
    allow_multiple_configurations => 0,
    is_casetype_interface         => 0,
    has_attributes                => 0,
    trigger_definition => {},
    test_interface => 0,
    text_templates => {
        attributes => qq/
            Uw koppeling maakt gebruik van attributen, welke weer
            gekoppeld kunnen worden aan velden binnen het zaaksysteem.
            Door op onderstaande knop te drukken kunt u de standaard velden koppelen
            aan kenmerken uit uw zaaktypen. Gebruik het vinkje om aan te geven dat u dit
            veld naast het detailoverzicht tevens wilt tonen in de lijstweergave van de APP.
        /,
    }
};

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() } );
};

=head1 ATTRIBUTES

=head2 api_supported_scope

Declares the supported scopes.

=cut

has api_supported_scope => (
    is => 'ro',
    default => sub {
        return ['case']
    }
);

=head2 api_full_access

Declares this module to have full access to the API within it's scope.

=cut

has api_full_access => (
    is => 'ro',
    default => 1,
);

=head1 METHODS

=head2 _load_values_into_form_object

=cut

around _load_values_into_form_object => sub {
    my $orig = shift;
    my $self = shift;
    my $opts = $_[1]; # get options

    my $form = $self->$orig(@_);

    $form->load_values({
        interface_app_uri => $opts->{ base_url } . 'pdc'
    });

    return $form;
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
