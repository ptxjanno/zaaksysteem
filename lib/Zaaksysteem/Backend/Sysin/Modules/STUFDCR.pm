package Zaaksysteem::Backend::Sysin::Modules::STUFDCR;
use Moose;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    MooseX::Log::Log4perl
    Zaaksysteem::Backend::Sysin::Modules::Roles::BaseURI
    Zaaksysteem::Backend::Sysin::Modules::Roles::ClientCertificate
    Zaaksysteem::Backend::Sysin::Modules::Roles::SOAPGeneric
    Zaaksysteem::Backend::Sysin::Modules::Roles::Tests
    Zaaksysteem::Backend::Sysin::Modules::Roles::CertificateInfo
/;

use Encode qw(encode_utf8);
use List::MoreUtils qw(any);
use XML::LibXML;
use Zaaksysteem::Backend::Sysin::STUFDCR::Model;
use Zaaksysteem::SOAP::Client;
use BTTW::Tools;
use BTTW::Tools::File qw(fix_file_extension_for_fh);
use Zaaksysteem::Tools::SysinModules qw(:certificates);
use Zaaksysteem::ZAPI::Form::Field;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::STUFDCR - STUF Document Creation Interface

=head1 DESCRIPTION

Interface module that implements the SOAP calls (outgoing and incoming)
described in the STUF-DCR.

This module implements the L<Zaaksysteem::Backend::Sysin::Modules::Roles::SOAPGeneric>
role, making the STUF-DCR return calls available through a SOAP interface.

The StUF-DCR ("Documentcreatieservices") protocol has two "roles":
Documentcreatieapplicatie ("DCA") and Documentcreatieverzoeker ("DCV"). This
interface implements the DCV role in Zaaksysteem.

=cut

my $INTERFACE_ID = 'stuf_dcr';

my @INTERFACE_CONFIG_FIELDS = (
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_recipient_application',
        type        => 'text',
        required    => 1,
        label       => 'Applicatienaam ontvanger',
        description => 'Applicatienaam van de ontvanger, die in de StUF-stuurgegevens wordt meegegeven.',
        data        => {
            placeholder => 'DCA',
        },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_dca_endpoint',
        type        => 'text',
        required    => 1,
        label       => 'Adres van DCA',
        description => 'SOAP endpoint waarop de <abbr title="Documentcreatieapplicatie">DCA</abbr> bereikt kan worden.',
        data        => {
            placeholder => 'https://dca.example.com/soap',
            pattern     => '^https:\/\/.+',
        },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_zender_organisatie',
        type        => 'text',
        label       => 'StUF Organisatie (verzender)',
        description => 'Inhoud van het veld &quot;organisatie&quot; in het &quot;verzender&quot;-deel van de StUF-stuurgegevens in uitgaande berichten.',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_zender_applicatie',
        type        => 'text',
        label       => 'StUF Applicatie (verzender)',
        description => 'Inhoud van het veld &quot;applicatie&quot; in het &quot;verzender&quot;-deel van de StUF-stuurgegevens in uitgaande berichten.',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_zender_administratie',
        type        => 'text',
        label       => 'StUF Administratie (verzender)',
        description => 'Inhoud van het veld &quot;administratie&quot; in het &quot;verzender&quot;-deel van de StUF-stuurgegevens in uitgaande berichten.',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_zender_gebruiker',
        type        => 'text',
        label       => 'StUF Gebruikersnaam (verzender)',
        description => 'Inhoud van het veld &quot;gebruiker&quot; in het &quot;verzender&quot;-deel van de StUF-stuurgegevens in uitgaande berichten.',
    ),
    client_private_key(
        description => 'Private key die Zaaksysteem zal gebruiken bij uitgaande SOAP-verzoeken aan de <abbr title="Documentcreatieapplicatie">DCA</abbr>',
        required => 1
    ),
    ca_certificate(
        description => 'CA-certificaat dat Zaaksysteem zal gebruiken om uitgaande verbindingen met de <abbr title="Documentcreatieapplicatie">DCA</abbr> verifi&euml;ren.',
    ),

    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_api_url',
        type        => 'display',
        label       => 'Adres van DCV',
        description => 'Vul dit adres in als adres van de <abbr title="Documentcreatieverzoeker">DCV</abbr> in uw <abbr title="Documentcreatieapplicatie">DCA</abbr>.',
        required    => 0,
        data        => { template => '<[field.value]>' },
    ),
    client_certificate(
        description => 'Client certificate dat Zaaksysteem verwacht bij binnenkomende SOAP-verzoeken van de <abbr title="Documentcreatieapplicatie">DCA</abbr>',
        required => 1
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_username_selection',
        type        => 'select',
        label       => 'Gebruik gebruikersnaam of elektronisch postadres',
        description => 'Dit veld geeft aan of de betrokkenen medewerkers met'
                        . 'gebruikersnaam of met hun e-mail adres'
                        . 'geindentificeerd moeten worden in de gegenereerde'
                        . 'XML.',
        required    => 0,
        default     => 'username',
        data        => {
            options => [
                { value => 'username', label => 'Gebruik van gebruikersnaam' },
                { value => 'email',    label => 'Gebruik van elektronisch postadres' }
            ]
        }
        ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'attribute_export_uri',
        type => 'display',
        label => 'Kenmerkschema',
        description => 'Het kenmerkschema wordt door de gekoppelde applicatie gebruikt om zaak-kenmerken te koppelen.',
        required => 0,
        data => {
            template => '<a href="<[field.value]>" target="_blank">Exporteer kenmerkschema</a>'
        }
    ),
);

my $INTERFACE_DESCRIPTION = <<'EOT';
<p>
    Deze koppeling configureert <a target="_blank" href="http://www.gemmaonline.nl/index.php/Documentcreatie_services">StUF Documentcreatieservices</a>-aansluiting in de rol <abbr title="Documentcreatieverzoeker">DCV</abbr> op uw Zaaksysteem.
</p>
EOT

my %MODULE_SETTINGS = (
    name                          => $INTERFACE_ID,
    description                   => $INTERFACE_DESCRIPTION,
    label                         => 'StUF-DCR',
    interface_config              => \@INTERFACE_CONFIG_FIELDS,
    direction                     => 'outgoing',
    manual_type                   => [],
    is_multiple                   => 1,
    is_manual                     => 0,
    allow_multiple_configurations => 1,
    is_casetype_interface         => 0,
    has_attributes                => 0,
    attribute_list                => [],
    retry_on_error                => 0,
    sensitive_config_fields       => [qw(
        dca_endpoint
        ca_certificate
        client_private_key
        client_certificate
    )],
    trigger_definition            => {
        create_file_from_template => {
            method  => 'create_file_from_template',
        },
    },
    test_interface                => 1,
    test_definition               => {
        description => qq{
            Om te controleren of de applicatie goed geconfigureerd is, kunt u
            hieronder een aantal tests uitvoeren. Hiermee controleert u of de
            verbinding tussen Zaaksysteem en uw Documentcreatieapplicatie
            tot stand kan worden gebracht.
        },
        tests => [
            {
                id => 1,
                label  => 'Test firewall',
                name   => 'connection_test',
                method => 'test_connection',
                description => 'Kan verbinding maken met de server en poort van de Documentcreatieapplicatie',
            },
            {
                id => 2,
                label  => 'Test CA-certificaat',
                name   => 'secure_connection_test',
                method => 'test_ssl_connection',
                description => 'Verbinding met Documentcreatieapplicatie via geconfigureerd CA-certificaat',
            },
        ],
    },
    interface_update_callback => sub {
        return shift->_update_interface_config(@_);
    },
);

=head2 uri_values

URI values, to be used by the
L<Zaaksysteem::Backend::Sysin::Modules::Roles::BaseURI> role to show customized
URIs in the interface configuration.

=cut

has uri_values => (
    is       => 'ro',
    isa      => 'HashRef',
    default => sub {
        {
            attribute_export_uri => "/beheer/bibliotheek/export/xml",
        }
    }
);

=head2 services_uri_values

URI values, to be used by the
L<Zaaksysteem::Backend::Sysin::Modules::Roles::BaseURI> role to show customized
URIs to the "services." virtual host in the interface configuration.

=cut

has services_uri_values => (
    is => 'ro',
    isa => 'HashRef',
    lazy => 1,
    default => sub {
        {
            interface_api_url  => '/sysin/interface/ID/soap',
        }
    },
);

=head2 BUILDARGS

Configures this interface module (configuration form fields, etc.).

=cut

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig(%MODULE_SETTINGS);
};

=head2 _get_model

Return an instance of L<Zaaksysteem::Backend::Sysin::STUFDCR::Model>, to handle
communication with StUF-DCR remote end.

=cut

sub _get_model {
    my ($self, $opts) = @_;

    my $interface = $opts->{interface};
    my $schema    = $opts->{schema};

    my $config    = $interface->get_interface_config;

    # Certificates by us to connect to external systems
    my $client_key     = $self->_get_cert_path($interface, 'client_private_key');
    my $ca_certificate = $self->_get_cert_path($interface, 'ca_certificate');

    my $model = Zaaksysteem::Backend::Sysin::STUFDCR::Model->new(
        interface => $interface,

        recipient_application => $config->{recipient_application},

        sender_organisation   => $config->{zender_organisatie},
        sender_administration => $config->{zender_administratie},
        sender_user           => $config->{zender_gebruiker},
        sender_application    => $config->{zender_applicatie},

        (($config->{username_selection} // '') eq 'email') ?
        ( use_username => 0): (),

        soap_client => $self->_build_soap_client(
            dca_endpoint => $config->{dca_endpoint},
            client_key   => $client_key,
            ($ca_certificate ? (ca_certificate => $ca_certificate) : ()),
        ),
    );

    return $model;
}

=head2 _build_soap_client

Build a L<Zaaksysteem::SOAP::Client> instance for use by the model.

=cut

sub _build_soap_client {
    my $self = shift;
    my %args = @_;

    return Zaaksysteem::SOAP::Client->new(
        endpoint => $args{dca_endpoint},
        ua       => LWP::UserAgent->new(
            agent    => 'Zaaksysteem/' . $Zaaksysteem::VERSION,
            timeout  => 60,
            ssl_opts => {
                verify_hostname => 1,
                SSL_cert_file => $args{client_key},
                SSL_key_file  => $args{client_key},

                (exists $args{ca_certificate})
                    ? (SSL_ca_file => $args{ca_certificate})
                    : (SSL_ca_path => '/etc/ssl/certs')
            }
        ),
    );
}

=head2 _process_row

Process an incoming SOAP call (in StUF DCR, only "verstrekDocumentcreatieResultaat")

=cut

sub _process_row {
    my $self = shift;
    my ($record, $row) = @_;

    my $transaction = $self->process_stash->{transaction};

    my $schema = $transaction->result_source->schema;

    # The default direction is "outgoing", but this interface defines a "two way" link.
    # This is the incoming (SOAP server) part
    $transaction->direction('incoming');

    my $stuf_dcr = $transaction->interface->model();

    my ($xc, $root) = try {
        $stuf_dcr->parse_incoming_xml($row);
    }
    catch {
        $record->is_error(1);
        $self->set_record_output($record, $_, "Ongeldige XML in SOAP-aanroep");
        return;
    };

    return unless $xc && $root;

    my ($msg, $response_xml) = try {
        my $result = $stuf_dcr->dispatch($xc, $root);

        my $original_transaction = $schema->resultset('Transaction')->search(
            {
                external_transaction_id => $result->{job_id},
                interface_id => $transaction->get_column('interface_id'),
            },
            {
                order_by => { -desc => 'date_created' }
            }
        )->first;

        if (!$original_transaction) {
            throw(
                'stufdcr/unsollicited_document',
                "StUF DCR SOAP-call ontvangen voor onbekend job_id.",
            );
        }

        my $original_pp = $original_transaction->get_processor_params();
        my $case_id = $original_pp->{case};

        my $subject = $self->_get_subject(
            schema => $schema,
            id     => $original_pp->{subject},
        );

        my $processor_params = $transaction->get_processor_params();
        $processor_params->{result} = $self->_process_result(
            schema  => $schema,
            result  => $result,
            case_id => $case_id,
            subject => $subject,

            document_name => $original_pp->{template_external_name},

            # Indicate that this is a return SOAP call -- leave a notification
            asynchronous_response => 1,

            (defined $original_pp->{case_document_ids})
                ? (case_document_ids => $original_pp->{case_document_ids})
                : (),
        );

        $transaction->processor_params($processor_params);
        $transaction->external_transaction_id($result->{job_id});

        return (
            sprintf(
                "Bestand '%s' toegevoegd aan zaak %d voor gebruiker '%s'",
                $result->{document}{filename}, $case_id,
                $subject->username
            ),
            $stuf_dcr->generate_bv03($record->id, $result->{stuurgegevens}),
        );
    }
    catch {
        $record->is_error(1);

        my %stuurgegevens = $stuf_dcr->parse_stuurgegevens($xc, $xc->findnodes('DCr:stuurgegevens'));

        return (
            "Fout bij verwerken binnenkomend bericht",
            $stuf_dcr->generate_fo03($record->id, \%stuurgegevens, $_),
        );
    };

    $response_xml = encode_utf8($response_xml);

    $self->set_record_output($record, $response_xml, $msg);

    return;
}

=head2 create_file_from_template

Start a transaction to create a file from the specified template.

=cut

sub create_file_from_template {
    my ($self, $params, $interface) = @_;

    my $input_data = JSON->new->utf8->pretty->canonical->encode($params);

    return $interface->process({
            external_transaction_id => 'unknown',
            input_data              => $input_data,
            processor_params        => {
                processor => '_create_file_from_template',
                %$params,
            },
        },
    );
}

sub _create_file_from_template {
    my ($self, $record) = @_;

    my $transaction = $self->process_stash->{transaction};
    my $params      = $transaction->get_processor_params();

    $self->log->debug("Creating file from template, using StUF-DCR");

    my $rv;

    try {
        my $interface = $transaction->interface;
        my $stuf_dcr  = $interface->model();
        my $schema    = $transaction->result_source->schema;

        my $case = $self->_get_case(
            schema => $schema,
            case   => $params->{case}
        );

        my $subject = $self->_get_subject(
            schema => $schema,
            id     => $params->{subject}
        );

        my $username
            = $stuf_dcr->use_username
            ? $subject->username
            : $subject->email_address;

        my $result = $stuf_dcr->create_document_from_template(
            case                   => $case,
            template_external_name => $params->{template_external_name},
            document_title         => $params->{document_title},
            transaction            => $transaction,
            username               => $username,
        );

        $self->set_record(
            $record,

            # This is an outgoing StUF-DCR transaction (SOAP call)
            input  => $result->{request},

            # XXX Maybe remove document content from response?
            output => $result->{response},
        );
        $record->preview_string(
            sprintf(
                "Zaak: %d; Gebruiker: %s; Sjabloon: %s",
                $params->{case}, $username,
                $params->{template_external_name}
            )
        );

        $params->{result} = $self->_process_result(
            schema  => $schema,
            result  => $result->{result},
            case_id => $params->{case},
            subject => $subject,

            (defined $params->{case_document_ids})
                ? (case_document_ids => $params->{case_document_ids})
                : (),
        );

        $transaction->processor_params($params);
        $transaction->external_transaction_id($result->{result}{job_id});

        return;
    } catch {
        $record->is_error(1);
        $transaction->success_count(0);
        $transaction->error_count(1);
        $transaction->error_fatal(1);
        $transaction->error_message(
            "Fout bij aanmaken document: $_"
        );
        $self->log->error("Error creating document: $_ / transaction_id: " . $transaction->id);

        $transaction->processor_params->{result} = {
            type => 'error',
        };
    };
}

sub _process_result {
    my $self = shift;
    my %args = @_;

    my $result = $args{result};

    my %rv;

    if(any { $result->{status} eq $_ } qw(ontvangen bezig onderbroken)) {
        # Your document is being created. Please wait for a(nother) SOAP call for it to finish.

        if ($result->{resume_url}) {
            $rv{type} = 'redirect';
            $rv{data}{document}{resume_url} = $result->{resume_url};
        }
        else {
            $rv{type} = 'pending';
        }
    }
    elsif ($result->{status} eq 'klaar') {

        # TODO: Use the filename of the template if no filename is
        # provided or default to something else;
        if (!$result->{document}{filename}) {
            $result->{document}{filename} = $args{document_name} // 'Undefined filename';
        }

        $result->{document}{filename} = fix_file_extension_for_fh(
            $result->{document}{content},
            $result->{document}{filename}
        );

        my $file = $args{schema}->resultset('File')->file_create({
            name       => $result->{document}{filename},
            file_path  => $result->{document}{content}->filename,
            db_params => {
                case_id    => $args{case_id},
                created_by => $args{subject}->betrokkene_identifier,
                generator  => 'stuf_dcr',
            },
        });

        if ($args{case_document_ids}) {
            $file->set_or_replace_case_documents(@{ $args{case_document_ids} });
        }

        # Don't create a notification
        if ($args{asynchronous_response}) {
            $args{schema}->resultset('Message')->message_create(
                {
                    event_type => 'stufdcr/sjabloon',
                    case_id    => $args{case_id},
                    subject_id => $args{subject}->betrokkene_identifier,
                    message    => "Sjabloon toegevoegd aan zaak: $result->{document}{filename}",
                    data       => {
                        file_ids => $file->id,
                        filename => $result->{document}{filename},
                    }
                }
            );
        }

        $rv{type} = 'created';
        $rv{data}{id} = $file->id;
    }
    else {
        throw(
            'stufdcr/document_create_error',
            sprintf(
                "Fout bij aanmaken van document. Status van DCA: '%s'",
                $result->{status},
            ),
        );
    }

    return \%rv;
}

sub _get_case {
    my ($self, %opts) = @_;

    my $case = $opts{schema}->resultset('Zaak')->search_extended({'me.id' => $opts{case}})->first;
    return $case if $case;

    throw('stuf_dcr/case/not_found', "No case with id $opts{case} found");
}

sub _get_subject {
    my ($self, %opts) = @_;

    my $s = $opts{schema}->resultset('Subject')->find($opts{id});
    return $s if $s;

    throw('stuf_dcr/subject/not_found', "No subject with id $opts{id} found");
}

=head2 test_connection

Test if a connection to the configured endpoint can be established.

=cut

sub test_connection {
    my $self = shift;
    my $interface = shift;

    $self->test_host_port($interface->jpath('$.dca_endpoint'));

    return;
}

=head2 test_ssl_connection

Test if an SSL connection to the configured endpoint can be established, using
the configured CA certificate.

=cut

sub test_ssl_connection {
    my $self = shift;
    my $interface = shift;

    my $ca          = $self->_get_cert_path($interface, 'ca_certificate');
    my $client_cert = $self->_get_cert_path($interface, 'client_private_key');

    $self->test_host_port_ssl(
        $interface->jpath('$.dca_endpoint'),
        $ca,
        $client_cert,
        $client_cert,
    );

    return;
}

sub _get_cert_path {
    my $self = shift;
    my $interface = shift;
    my $key = shift;

    my $id = $interface->jpath(sprintf('$.%s[0].id', $key));

    return unless $id;

    my $filestore = $interface->result_source->schema->resultset('Filestore');

    return $filestore->find($id)->get_path;
}

sub _update_interface_config {
    my ($module, $interface, $model) = @_;

    my $config = $interface->get_interface_config;

    $module->get_certificate_info(
        $interface,
        $config,
        qw(ca_certificate client_certificate)
    );

    $interface->update_interface_config($config);
}


1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
