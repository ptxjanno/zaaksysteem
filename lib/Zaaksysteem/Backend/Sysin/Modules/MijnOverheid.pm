package Zaaksysteem::Backend::Sysin::Modules::MijnOverheid;
use Moose;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::MijnOverheid - Interface module for MijnOverheid/Lopende Zaken

=cut

use JSON;
use List::Util ('sum', 'all');
use BTTW::Tools;
use Zaaksysteem::Constants::Users qw(:all);
use Zaaksysteem::Types qw(UUID);
use Zaaksysteem::XML::MijnOverheid::HTTPClient;
use Zaaksysteem::XML::MijnOverheid::HTTPClient::Spoof::UserAgent;
use Zaaksysteem::XML::MijnOverheid::SpoofClient;
use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

extends 'Zaaksysteem::Backend::Sysin::Modules';
with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
    Zaaksysteem::Backend::Sysin::Modules::Roles::Tests
    Zaaksysteem::Backend::Sysin::Modules::Roles::CertificateInfo
/;

use constant INTERFACE_ID => 'mijnoverheid';

use constant ATTACHMENTS_MAX_NUMBER     => 2;
use constant ATTACHMENTS_MAX_TOTAL_SIZE => 256_000;

use constant INTERFACE_CONFIG_FIELDS => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_oin',
        type => 'text',
        label => 'Overheidsidentificatienummer',
        required => 1,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_esb',
        type => 'select',
        label => 'ESB-provider',
        data => {
            options => [
                {
                    value => 'opentunnel',
                    label => 'OpenTunnel',
                }
            ],
        },
        default => 'opentunnel',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_endpoint_bb_check',
        type => 'text',
        label => 'Endpoint-URL van de "Berichtenbox Check"-service',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_endpoint_bb',
        type => 'text',
        label => 'Endpoint-URL van de "Berichtenbox"-service',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_allow_attachments',
        type => 'checkbox',
        label => 'Versturen van documenten',
        description => 'Deze optie maakt het mogelijk om documenten mee te sturen als bijlage van een bericht, of ze direct naar mijnoverheid te versturen.',
        when => '(interface_endpoint_bb !== "")',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_endpoint_lz',
        type => 'text',
        label => 'Endpoint-URL van de "Lopende Zaken"-service',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_ca_cert',
        type => 'file',
        label => 'CA-certificaate voor de ESB',
        description => 'Upload hier het certificaat van de <abbr title="Certificate Authority (de partij die de verbinding met de ESB heeft gecertificeerd)">CA</abbr> die het certificaat gebruikt door de ESB heeft ondertekend.',
        required    => 0,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_spoofmode',
        type => 'checkbox',
        label => 'Spoof-modus',
        description => qq{
            Met deze "spoof"-modus worden geen berichten naar de
            geconfigureerde ESB gestuurd,, zodat de functionaliteit getest kan
            worden zonder afhankelijkheid van de ESB.<br>
            <b>LET OP</b>: Niet aanzetten op een productie-omgeving. Deze optie is alleen voor
            test-doeleinden en levert geen juiste/echte gegevens.
        },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_spoofmode_has_berichtenbox',
        type => 'checkbox',
        label => 'Spoof-modus: Berichtenbox geactiveerd',
        description => 'Deze bepaald de mogelijkheid in documenten tab berichten te kunnen versturen',
        when => 'interface_spoofmode',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_spoofmode_accepted_by_provider',
        type => 'checkbox',
        label => 'Spoof-modus: XML transactie geaccepteerd',
        description => 'Deze bepaald of de transactie is geaccepteerd door de tussenpartij',
        when => 'interface_spoofmode',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_spoofmode_server_error',
        type => 'checkbox',
        label => 'Spoof-modus: External Server Error',
        description => 'Deze bepaald of de tussenpartij een error geeft',
        when => 'interface_spoofmode',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_base_uri',
        type => 'display',
        label => 'Basis-URI',
        description => 'De genoteerde URI wordt gebruikt om de link naar de PIP in MijnOverheid-berichten samen te stellen',
        data => {
            template => '<[field.value]>'
        }
    )
];

use constant MODULE_SETTINGS => {
    name                          => INTERFACE_ID,
    label                         => 'MijnOverheid',
    interface_config              => INTERFACE_CONFIG_FIELDS,
    direction                     => 'outgoing',
    sensitive_config_fields       => ['oin', 'ca_cert'],
    is_multiple                   => 0,
    is_manual                     => 0,
    retry_on_error                => 1,
    allow_multiple_configurations => 0,
    is_casetype_interface         => 0,
    has_attributes                => 0,
    test_interface                => 1,
    test_definition               => {
        description => qq{
            Hier kunt u de verbinding met MijnOverheid testen.
        },
        tests => [
            {
                id    => 1,
                label => 'Configuratie',
                name  => 'test_configuration',
                method => 'test_configuration',
                description => qq{
                    Deze test doet een basale test op de instellingen van deze configuratie.
                },
            },
            {
                id    => 2,
                label => 'Secure Connection',
                name  => 'test_secure_connection',
                method => 'test_secure_connection',
                description => qq{
                    Deze test probeert verbinding te maken met de server(s) van MijnOverheid.
                },
            },
            {
                id    => 3,
                label => 'Spoof Modus',
                name  => 'test_spoofmode',
                method => 'test_spoofmode',
                description => qq{
                    Deze test controleert of de "Spoof Modyus" uit staat geschakeld.
                },
            },
        ],
    },
    trigger_definition => {
        PostStatusUpdate => {
            method => 'PostStatusUpdate',
            update => 1
        },
        check_subscription => {
            method => 'check_subscription',
            update => 1,
            api    => 1,
            api_is => 'ro',
            api_allowed_users => REGULAR,
        },
        send_berichtenbox_message => {
            method => 'send_berichtenbox_message',
            update => 1,
            api    => 1,
            api_is => 'ro',
            api_allowed_users => REGULAR,
        },
    },
    interface_update_callback => sub {
        return shift->_update_interface_config(@_);
    },
};

around BUILDARGS => sub {
    my $orig = shift;
    my $class = shift;

    $class->$orig( %{ MODULE_SETTINGS() } );
};

=head2 _load_values_into_form_object

=cut

around _load_values_into_form_object => sub {
    my $orig = shift;
    my $self = shift;
    my $opts = $_[1]; # get options

    my $form = $self->$orig(@_);

    $form->load_values({
        interface_base_uri => $opts->{ base_url }
    });

    return $form;
};


=head2 PostStatusUpdate

=cut

define_profile PostStatusUpdate => (
    required => [qw/
        case_id
        kenmerken
        message
    /],
);

sub PostStatusUpdate {
    my $self      = shift;
    my $params    = assert_profile(shift)->valid;
    my $interface = shift;

    $interface->process({
        external_transaction_id => 'unknown',
        input_data => JSON->new->pretty->encode($params->{message}),
        processor_params => {
            %$params,
            processor => '_process_external_system_message',
        },
    });
}

=head2 _process_external_system_message

Perform the calls to OpenTunnel to handle "MijnOverheid" messaging.

=cut

sub _process_external_system_message {
    my $self   = shift;
    my $record = shift;

    my $params    = $self->_processor_params;
    my $interface = $self->_transaction->interface;
    my $schema    = $interface->result_source->schema;
    my $case      = $schema->resultset('Zaak')->find($params->{case_id});
    my $base_uri  = _interface__base_uri($interface);
    my $requestor = $case->aanvrager_object;

    try {
        if ($requestor->btype ne 'natuurlijk_persoon') {
            throw('mijnoverheid/requestor_type', "MijnOverheid wordt alleen ondersteund voor natuurlijke personen");
        }
        my $bsn = sprintf("%09d", $requestor->burgerservicenummer);

        my $api = $self->_mijnoverheid_client($interface);

        my $subject = "Bericht verstuurd";
        my $message = "";
        my ($result, $xml, $xml_out);
        my @attachments;
        if ($params->{message}{message_type} eq 'Lopende Zaken') {
            ($result, $xml, $xml_out) = $api->lopende_zaak(
                bsn     => $bsn,
                case_id => $case->id,

                date_of_registration => $case->registratiedatum,
                date_target          => $case->streefafhandeldatum,
                casetype_name        => $case->zaaktype_node_id->titel,
                explanation          => $case->onderwerp_extern,
                status               => $params->{message}{status},
                status_name          => $params->{message}{status_name},
                url                  => "${base_uri}pip/zaak/" . $case->id,
                (defined $case->afhandeldatum)
                    ? (date_of_completion => $case->afhandeldatum)
                    : (),
                (defined $case->resultaat)
                    ? (result             => $case->resultaat)
                    : (),
            );

            $self->log->trace("LopendeZaken XML:\n" . $xml);
        }
        elsif($params->{message}{message_type} eq 'Berichtenbox') {

            my ($check_result, $check_xml, $request_xml) = $api->has_berichtenbox(
                bsn => sprintf("%09d", $bsn)
            );

            $record->input($request_xml);
            $record->output($check_xml);

            if (!$check_result->{has_berichtenbox}) {
                throw(
                    'mijnoverheid/berichtenbox/not_available',
                    'Betrokkene heeft geen MijnOverheid berichtenbox'
                );
            }

            my $ztt = Zaaksysteem::ZTT->new();
            $ztt->add_context($case);

            $message = $ztt->process_template( $params->{message}{message} )->string // '';
            $subject = $ztt->process_template( $params->{message}{subject} )->string // '';

            my @file_attachment_ids;
            if ( $params->{message}{attachments} ) {
                @file_attachment_ids = __PACKAGE__->_search_file_attachment_ids_for_bibliotheek_kenmerken_ids(
                    $schema, $case->id, $params->{message}{attachments}
                )
            }

            @attachments = __PACKAGE__->_load_attachments_by_ids($schema, @file_attachment_ids);

            ($result, $xml, $xml_out) = $api->berichtenbox_message(
                bsn => $bsn,
                subject   => $subject,
                content   => $message,
                reference => $case->id,
                attachments => \@attachments,
            );

            $self->log->trace("BerichtenBox XML:\n" . $xml);
        }
        else {
            throw("mijnoverheid/unknown_message_type", "Onbekend berichttype: " . $params->{message}{message_type});
        }

        $record->output($record->output . "\n\n" . $xml);
        $record->input($record->input . "\n\n" . $xml_out);

        if (!$result->{accepted}) {
            throw(
                "mijnoverheid/not_accepted",
                "Bericht niet geaccepteerd door MijnOverheid: " . $result->{message} // '[geen melding]',
            );
        }

        my $logging = $schema->resultset('Logging');
        my $logging_data = {
            component    => 'case',
            component_id => $params->{case_id},
            zaak_id      => $params->{case_id},
            data         => {
                destination => $params->{message}{message_type},
                subject => $subject,
                message => $message,
            }
        };
        if ( scalar @attachments ) {
            my @logging_attachments = map { delete $_->{content}; $_ } @attachments;
            $logging_data->{data}{attachments} = \@logging_attachments;
        }
        $logging->trigger( 'case/send_mijnoverheid_message' => $logging_data );

    }
    catch {
        my $message = "Algemene fout";
        if (blessed($_)) {
            my $error_set;
            if ($_->can('object')) {
                my $object = $_->object;

                if (blessed($object) && $object->isa('HTTP::Message')) {
                    $error_set = 1;
                    $self->_transaction->error_message($object->as_string);
                }
            }

            if ($_->can('message')) {
                $message = $_->message;
                $self->_transaction->error_message($message) unless $error_set;
            }
        }

        # Log error $_ in case. *Then* fail the transaction.
        my $logging = $schema->resultset('Logging');
        $logging->trigger(
            'case/send_mijnoverheid_message/error', {
            component    => 'case',
            component_id => $params->{case_id},
            zaak_id      => $params->{case_id},
            data         => {
                destination => $params->{message}{message_type},
                subject => 'Bericht kon niet verzonden worden',
                message => $message,
                error   => 1,
            }
        });
    
    # okay, lets fail indeed!
    $record->is_error(1);
    };

    return;
}

define_profile check_subscription => (
    required => {
        request_params => 'HashRef',
        uploads        => 'HashRef' ,
        headers        => 'HTTP::Headers',
        body           => 'Str' ,
        method         => 'Str',
    },
    optional => {
        object_model   => 'Zaaksysteem::Object::Model',
    },
);

sub check_subscription {
    my $self      = shift;
    my $params    = shift;
    my $interface = shift;

    assert_profile(
        $params
    )->valid;
    my $request_params = $params->{request_params};
    assert_profile(
        $request_params => 
        profile => {
            required => {
                subject_uuid => UUID
            }
        }
    )->valid;

    my $response = try { 
        return $self->_check_subscription($interface, $request_params, undef);
    } catch {
        my $exception = $_;
        
        # Special case for bad_subject_type exception; this is essentially a
        # fault condition at the caller's side, but technically the
        # berichtenbox does not exist.

        return {
            has_berichtenbox => 0
        } if
            blessed($exception)
            &&
            $exception->isa('BTTW::Exception::Base')
            &&
            $exception->type eq 'mijnoverheid/subject/bad_subject_type';

        $self->log->warn("$exception");

        throw(
            'mijnoverheid/error/general',
            'Mijnoverheid algemene of configuratie fout. Neem contact op met uw beheerder'
        )
    };

    return _sysin_types_mijnoverheid_check_message_box_exists($response);
}

sub _check_subscription {
    my $self      = shift;
    my $interface = shift;
    my $params    = shift;
    my $record    = shift;

    my $schema = $interface->result_source->schema;
    my $subject_uuid = $params->{subject_uuid};
    my $subject = __PACKAGE__->_get_subject_by_uuid( $schema, $subject_uuid);
    my $bsn = $subject->subject->personal_number;

    my $client = __PACKAGE__->_mijnoverheid_client($interface);
    my $response = __PACKAGE__->_mijnoverheid_client_call(
        $client,
        has_berichtenbox => {
             bsn => sprintf( "%09d", $bsn ),
        },
        $record
    );

    return $response;
}

define_profile send_berichtenbox_message => (
    required => {
        request_params => 'HashRef',
        uploads        => 'HashRef' ,
        headers        => 'HTTP::Headers',
        body           => 'Any' ,
        method         => 'Str',
    },
    optional => {
        object_model   => 'Zaaksysteem::Object::Model',
    },
);

sub send_berichtenbox_message {
    my $self      = shift;
    my $params    = shift;
    my $interface = shift;

    assert_profile(
        $params
    )->valid;
    my $request_params = $params->{request_params};
    assert_profile(
        $request_params =>
        profile => {
            required => {
                subject_uuid     => UUID,
                subject          => 'Str',
                body             => 'Str',
                case_id          => 'Int',
            },
            optional => {
                file_attachments => 'Int', # ArrayRef of
            },
        }
    )->valid;

    # we are being called outside normal transactions, so we create one here
    my $transaction = $interface->create_related('transactions',
        { input_data => JSON->new->pretty->encode($request_params), direction => 'outgoing' }
    );
    my $record = $transaction->new_related('records', {} );

    my $response = $self->_send_berichtenbox_message( $interface, $request_params, $record );

    $record->insert();
    $transaction->update(
        {
            processed     => 1,
            success_count => $response->{accepted} ? 1 : 0,
            error_count   => $response->{accepted} ? 0 : 1,
            error_message => $response->{accepted} ? '' : 'Berichtenbox boodschap is niet afgeleverd', 
        }
    );

    return _sysin_types_mijnoverheid_send_berichtenbox_message_accepted($response);

}

sub _send_berichtenbox_message {
    my $self      = shift;
    my $interface = shift;
    my $params    = shift;
    my $record    = shift;

    assert_profile(
        $params =>
        profile => {
            required => {
                subject_uuid     => UUID,
                case_id          => 'Int',
            },
            optional => {
                body             => 'Str', # may contain magicstrings
                subject          => 'Str', # the subject line, not the 'object'
                file_attachments => 'Int', # ArrayRef off file id's
            },
        }
    )->valid;

    my $schema       = $interface->result_source->schema;
    my $subject_uuid = $params->{subject_uuid};
    my $subject      = __PACKAGE__->_get_subject_by_uuid( $schema, $subject_uuid);
    my $bsn          = $subject->subject->personal_number;
    my $case         = $schema->resultset('Zaak')->find($params->{case_id});

    my $ztt = Zaaksysteem::ZTT->new();
    $ztt->add_context($case);

    my $message_body = $ztt->process_template( $params->{body} )->string // '';
    my $subject_line = $ztt->process_template( $params->{subject} )->string // '';

    my $file_attachments = $params->{file_attachments};
    my @file_attachment_ids = @$file_attachments;

    my @attachments = __PACKAGE__->_load_attachments_by_ids($schema, @file_attachment_ids);

    my $client = __PACKAGE__->_mijnoverheid_client($interface);
    my $response = __PACKAGE__->_mijnoverheid_client_call(
        $client,
        berichtenbox_message => {
             bsn       => sprintf( "%09d", $bsn ),
             subject   => $subject_line,
             content   => $message_body,
             reference => $params->{case_id},
             attachments => \@attachments,
        },
        $record
    );

    return $response if not $response->{accepted};

    my @logging_attachments = map { delete $_->{content}; $_ } @attachments;
    my $logging_data = {
        component => 'berichtenbox',
        data => {
            case_id             => $params->{case_id},
            attachments         => \@logging_attachments,
            content             => $message_body,
            message_subject     => $subject_line,
        }
    };

    $case->trigger_logging( 'case/send_berichtenbox' => $logging_data );

    return $response;
}

sub _mijnoverheid_client_call {
    my $class     = shift;
    my $client    = shift;
    my $call      = shift;
    my $params    = shift;
    my $record    = shift;

    my( $result, $xml_resp, $xml_rqst ) = $client->$call(%$params);

    $record->input($xml_rqst)  if $record;
    $record->output($xml_resp) if $record;

    return $result;
}

sig _mijnoverheid_client => 'Zaaksysteem::Model::DB::Interface';

sub _mijnoverheid_client {
    my $self = shift;
    my $interface = shift;

    my $instance = _interface__allow_attachments($interface) ?
        Zaaksysteem::XML::Compile
        ->xml_compile
        ->add_class('Zaaksysteem::XML::MijnOverheid::InstanceV2')
        ->mijnoverheid_v2
        :
        Zaaksysteem::XML::Compile
        ->xml_compile
        ->add_class('Zaaksysteem::XML::MijnOverheid::Instance')
        ->mijnoverheid;

    my $ua;
    if ( _interface__spoofmode($interface) ) {
        my $has_berichtenbox     = _interface__spoofmode_has_berichtenbox($interface);
        my $accepted_by_provider = _interface__spoofmode_accepted_by_provider($interface);
        my $allow_attachments    = _interface__allow_attachments($interface);
        my $server_error         = _interface__spoofmode_server_error($interface);
        $ua = Zaaksysteem::XML::MijnOverheid::HTTPClient::Spoof::UserAgent->new(
            spoofmode => {
                has_berichtenbox     => $has_berichtenbox,
                accepted_by_provider => $accepted_by_provider,
                allow_attachments    => $allow_attachments,
                server_error         => $server_error,
            }
        )
    } else {
        $ua = LWP::UserAgent->new();
        if ( my $ca_file = _interface__ca_file($interface) ) {
            $ua->ssl_opts(
                verify_hostname => 1,
                SSL_ca_file     => $ca_file->get_path,
            );
        }
    }

    my $call_urls = _interface__spoofmode($interface) ?
        {
            has_berichtenbox     => '/spoof/has_berichtenbox',
            berichtenbox_message => '/spoof/berichtenbox_message',
            lopende_zaak         => '/spoof/lopende_zaak',
        }
        :
        {
            has_berichtenbox     => _interface__endpoint_bb_check($interface),
            berichtenbox_message => _interface__endpoint_bb($interface),
            lopende_zaak         => _interface__endpoint_lz($interface),
        };
    my $client = Zaaksysteem::XML::MijnOverheid::HTTPClient->new(
        xml_instance => $instance,
        ua           => $ua,
        sender       => _interface__oin($interface),
        call_urls    => $call_urls,
    );

    return $client;
}

=head2 test_configuration

Pass this test when minimal configuration is setup

=cut

sub test_configuration {
    my ($self, $interface) = @_;

    throw(
        'sysin/modules/test/error/missing_oin',
        'Geen Overheidsidentificatienummer ingevoerd'
    ) unless _interface__oin($interface);

    throw(
        'sysin/modules/test/error/missing_endpoint_bb_check',
        'Geen endpoint ingevoerd voor "Berichtenbox Check"'
    ) unless _interface__endpoint_bb_check($interface);

    throw(
        'sysin/modules/test/error/missing_ca_file',
        'CA certificate is not present.'
    ) unless _interface__ca_file($interface);

    return
}

=head2 test_secure_connection

Pass this test when a connection can be made to MijnOverheid.

=cut

sub test_secure_connection {
    my $self = shift;
    my $interface = shift;

    try {
        $self->test_configuration($interface)
    } catch {
        throw(
            'sysin/modules/test/error/bad_config',
            'Configuratie fout, zie separate test'
        )
    };

    my $ca_file = _interface__ca_file($interface);

    $self->test_host_port_ssl(
        _interface__endpoint_bb_check($interface),
        $ca_file->get_path,
    ) if $ca_file;
}

=head2 test_sppofmode

Pass this test when spoofmode is not enabled

=cut

sub test_spoofmode {
    my $self = shift;
    my $interface = shift;

    throw(
        'sysin/modules/test/error/spoofmode',
        'Deze koppeling is ingeschakelt op "Spoof Modus"'
    ) if _interface__spoofmode($interface);

}

sub _transaction {
    my $self = shift;
    return $self->process_stash->{transaction};
}

sub _processor_params {
    my $self = shift;
    return $self->_transaction->get_processor_params();
}

sub _get_subject_by_uuid {
    my $class = shift;
    my $schema = shift;
    my $subject_uuid = shift;

    use Zaaksysteem::BR::Subject;
    my $subject_bridge = Zaaksysteem::BR::Subject->new(
        schema => $schema,
    );

    my $subject = $subject_bridge->find($subject_uuid) or
        throw(
            'mijnoverheid/subject/not_found',
            sprintf(
                'Betrokkene kan niet worden gevonden voor opgegeven uuid: %s',
                $subject_uuid,
            )
        );

    if ($subject->subject_type ne 'person') {
        throw(
            'mijnoverheid/subject/bad_subject_type',
            sprintf(
                'Betrokkene is geen natuurlijke persoon, uuid: %s',
                $subject_uuid,
            )
        );
    }

    return $subject;
}

sub _search_file_attachment_ids_for_bibliotheek_kenmerken_ids {
    my $class = shift;
    my $schema = shift;
    my $case_id = shift;
    my @bibliotheek_kenmerken_ids = @_;

    return unless scalar @bibliotheek_kenmerken_ids;

    my $rs_zaaktype_kenmerken = $schema
        ->resultset('ZaaktypeKenmerken')
        ->search_rs(
        {
            bibliotheek_kenmerken_id => { -in => [ @bibliotheek_kenmerken_ids ] },
        },
        {
            columns  => ['id' ],
            distinct => 1
        }
    );
    my $rs_files = $schema->resultset('File')->search(
        {
            'case_documents.case_document_id' => { -in => $rs_zaaktype_kenmerken->as_query },
            case_id                           => $case_id,
            date_deleted                      => undef,
            accepted                          => 1,
            active_version                    => 1,
        },
        {
            join => { case_documents => 'file_id' },
            columns => [ 'id' ]
        }
    );
    
    my @file_attachment_ids = map { $_->id } $rs_files->all;

    return @file_attachment_ids
}

sub _load_attachments_by_ids {
    my $class = shift;
    my $schema = shift;
    my @file_ids = @_;

    return unless scalar @file_ids;

    throw(
        'mijnoverheid/too_many_attachments',
        sprintf ( 'Het aantal bijlagen van %d is te veel, MijnOverheid accepteert maximaal %d bijlagen',
            scalar @file_ids,
            ATTACHMENTS_MAX_NUMBER,
        )
    ) if scalar @file_ids > ATTACHMENTS_MAX_NUMBER;

    my @files = $schema
        ->resultset('File')
        ->search(
            {
                'me.id' => [ @file_ids ],
            },
            {   
                prefetch => 'filestore'
            }
        );

    throw(
        'mijnoverheid/mimetype/not_application_pdf',
        "MijnOverheid accepteert alleen bestanden van het type PDF"
    ) unless all {
        $_->filestore->mimetype eq 'application/pdf'
    } @files;

    my $total_size = sum map { $_->filestore->size } @files;
    throw(
        'mijnoverheid/size/exceeds',
        sprintf ("MijnOverheid accepteert alleen bestanden met een gezamenlijke grootte kleiner dan %d bytes [%d]",
            ATTACHMENTS_MAX_TOTAL_SIZE,
            $total_size,
        )
    ) if $total_size > ATTACHMENTS_MAX_TOTAL_SIZE;

    my @attachments = map {
        +{
            mimetype => $_->filestore->mimetype,
            size     => $_->filestore->size,
            filename => $_->filename,
            md5      => $_->filestore->md5,
            content  => $_->filestore->content(),
            file_id  => $_->id,
        }
    } @files;

    return @attachments
}

sub _sysin_types_mijnoverheid_check_message_box_exists{
    my $response = shift;
    
    my $message = "Berichtenbox is" . ( $response->{has_berichtenbox} ? " " : " niet "  ) . "beschikbaar";

    return {
        type      => 'result',
        reference => undef,
        preview   => $message,
        instance  => {
            type               => 'sysin/mijnoverheid/check_message_box_exists',
            ok                 => $response->{has_berichtenbox} ? \1 : \0,
            message            => $message,
        }
    };
}

sub _sysin_types_mijnoverheid_send_berichtenbox_message_accepted{
    my $response = shift;
    
    my $message = "Berichtenbox boodschap is" . ( $response->{accepted} ? " " :  "niet "  ) . "afgeleverd";

    return {
        type => 'result',
        reference => undef,
        preview => $message,
        instance => {
            type => 'sysin/mijnoverheid/send_berichtenbox_message_accepted',
            ok => $response->{accepted} ? \1 : \0,
            message => $message,
        }
    };
}

# hide implementation details that we use XPath querieste get some data
#
sub _interface__allow_attachments {
    return $_[0]->jpath('$.allow_attachments')
}

sub _interface__base_uri {
    return $_[0]->jpath('$.base_uri')
}

sub _interface__ca_cert_id {
    return $_[0]->jpath('$.ca_cert[0].id')
}

sub _interface__ca_file{
    my $interface = shift;

    my $ca_cert_id = _interface__ca_cert_id($interface);
    return unless $ca_cert_id;

    my $schema = $interface->result_source->schema;
    my $ca_file = $schema->resultset('Filestore')->find($ca_cert_id);

    return $ca_file;
}

sub _interface__endpoint_bb {
    return $_[0]->jpath('$.endpoint_bb')
}

sub _interface__endpoint_bb_check {
    return $_[0]->jpath('$.endpoint_bb_check')
}

sub _interface__endpoint_lz {
    return $_[0]->jpath('$.endpoint_lz')
}

sub _interface__oin {
    return $_[0]->jpath('$.oin')
}

sub _interface__spoofmode {
    return $_[0]->jpath('$.spoofmode')
}

sub _interface__spoofmode_accepted_by_provider {
    return $_[0]->jpath('$.spoofmode_accepted_by_provider')
}

sub _interface__spoofmode_has_berichtenbox {
    return $_[0]->jpath('$.spoofmode_has_berichtenbox')
}

sub _interface__spoofmode_server_error {
    return $_[0]->jpath('$.spoofmode_server_error')
}

sub _update_interface_config {
    my ($module, $interface, $model) = @_;

    my $config = $interface->get_interface_config;

    $module->get_certificate_info(
        $interface,
        $config,
        qw(ca_cert)
    );

    $interface->update_interface_config($config);
}


1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
