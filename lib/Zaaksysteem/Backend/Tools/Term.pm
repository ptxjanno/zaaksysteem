package Zaaksysteem::Backend::Tools::Term;

use strict;
use warnings;

use DateTime;

use Zaaksysteem::Constants;
use BTTW::Tools;
use Zaaksysteem::Backend::Tools::WorkingDays qw/add_working_days/;

use Exporter 'import';
our @EXPORT_OK = qw/calculate_term/;


=head2 calculate_term

Add an interval to a given date, return new date.

start:  the start datetime to calculate an interval from
amount: number of units
type:   unit type, e.g. weeks, days, working days etc.

=cut

define_profile calculate_term => (
    #TODO: Move bits and pieces to ZS::Constants
    required => [qw/start amount type/],
    constraint_methods => {
        start => sub {
            my ($dfv, $value) = @_;
            return ref $value && ref $value eq 'DateTime';
        },
        amount => sub {
            my ($dfv, $value) = @_;

            # to allow swift testing, a special mode. 'test' will trigger a
            # preset short time. Otherwise the minimum time would be 1 day.
            return 1 if $value eq 'test';

            if ($dfv->{__INPUT_DATA}->{type} eq ZAAKSYSTEEM_NAMING->{TERMS_TYPE_EINDDATUM}) {
                return $value =~ m/^(\d{2})-(\d{2})-(\d{4})$/;
            }
            return $value =~ m|^\d+$|;
        },
        type => sub {
            my ($dfv, $value) = @_;
            my @valid_types = (
                ZAAKSYSTEEM_NAMING->{TERMS_TYPE_WERKDAGEN},
                ZAAKSYSTEEM_NAMING->{TERMS_TYPE_KALENDERDAGEN},
                ZAAKSYSTEEM_NAMING->{TERMS_TYPE_WEKEN},
                ZAAKSYSTEEM_NAMING->{TERMS_TYPE_EINDDATUM}
            );
            return grep { $value eq $_ } @valid_types;
        }
    }
);

sub calculate_term {
    my $arguments = assert_profile(shift)->valid;

    my $start  = $arguments->{start}->clone;
    my $amount = $arguments->{amount};
    my $type   = $arguments->{type};

    if ($amount eq 'test') {
        # special test keyword to allow a short interval feasible for
        # feature verification
        return DateTime->now->add(minutes => 3);
    } elsif ($type eq ZAAKSYSTEEM_NAMING->{TERMS_TYPE_WERKDAGEN}) {
        return add_working_days({
            datetime     => $start,
            working_days => $amount
        });
    } elsif ($type eq ZAAKSYSTEEM_NAMING->{TERMS_TYPE_KALENDERDAGEN}) {
        return $start->add(days => $amount);
    } elsif ($type eq ZAAKSYSTEEM_NAMING->{TERMS_TYPE_WEKEN}) {
        return $start->add(weeks => $amount);
    } elsif ($type eq ZAAKSYSTEEM_NAMING->{TERMS_TYPE_EINDDATUM}) {
        my ($day, $month, $year) = $amount =~ /^(\d{2})-(\d{2})-(\d{4})$/;
        return DateTime->new(
            year    => $year,
            month   => $month,
            day     => $day
        );
    }
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

