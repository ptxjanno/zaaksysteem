package Zaaksysteem::Backend::Config::ResultSet;

use Moose;

with 'MooseX::Log::Log4perl';

BEGIN { extends 'DBIx::Class::ResultSet'; }

use JSON::XS;
use List::Util qw(none uniq any);

use constant STANDARD_PARAMETERS => qw/
    users_can_change_password
    signature_upload_role
    allocation_notification_template_id
    pdf_annotations_public
    requestor_search_extension_active
    requestor_search_extension_href
    requestor_search_extension_name
/;

use constant ADVANCED_PARAMETERS => qw/
    bag_spoof_mode
    jodconverter_url
    public_manpage
    enable_stufzkn_simulator
    disable_dashboard_customization
/;

my @ARRAY_PARAMETERS = qw(allowed_templates custom_relation_roles bag_priority_gemeentes);
my @BOOLEAN_PARAMETERS = qw(files_locally_editable bag_spoof_mode bag_local_only);

sub _decode_config_item {
    my ($self, $name, $item, $for_json) = @_;

    if ($for_json && any { $name eq $_ } @BOOLEAN_PARAMETERS) {
        return $item ? \1 : \0;
    }

    if (any { $name eq $_ } @ARRAY_PARAMETERS) {
        my $decoded = JSON::XS->new->utf8(0)->decode($item || '[]');

        # This is processed by "veldoptie.tt", which is very picky about input values
        return $decoded if (@$decoded || $for_json);
        return "";
    }

    return $item;
}

sub get_value {
    my ($self, $parameter, $for_json) = @_;

    die "need parameter" unless $parameter;

    my $row = $self->search({ parameter => $parameter })->first;

    if ($row) {
        my $val = $row->value;

        return $self->_decode_config_item($parameter, $val, $for_json);
    }

    return;
}

sub get {
    my $self = shift;
    my $retval = undef;

    eval {
        $retval = $self->get_value(@_);
    };

    return $retval;
}

sub save {
    my ($self, $params, $advanced, $apply_defaults) = @_;

    $apply_defaults //= 1;

    my $config;
    if ($apply_defaults) {
        $config = $self->apply_defaults($params, $advanced);
    }
    else {
        $config = $params;
    }

    while (my ($key, $value) = each %$config) {
        next if $key eq 'advanced'; # advanced thingy needs to go bye bye

        # List::Util::first clashes with DBIx::Class::first, so we can't import it.
        if (List::Util::first { $key eq $_ } @ARRAY_PARAMETERS) {
            if (ref($value) ne 'ARRAY') {
                $value = [$value];
            }

            $value = [ grep { $_ } uniq @$value ];

            $value = JSON::XS->new->utf8(0)->encode($value);
        }

        my $row = $self->find_or_create({parameter => $key, advanced => $advanced});

        if ($row) {
            $row->value($value);
            $row->update;
        }
    }
}

sub get_all {
    my ($self, $advanced) = @_;

    my @settings = $self->search({advanced => $advanced})->all;

    my $config = { map { $_->parameter => $_->value } @settings };

    for my $param (@ARRAY_PARAMETERS) {
        next unless exists $config->{ $param };

        $config->{ $param } = $self->_decode_config_item(
            $param,
            $config->{ $param }
        );
    }

    return $self->apply_defaults($config, $advanced);
}

sub apply_defaults {
    my ($self, $config, $advanced) = @_;

    $config->{$_} ||= '' for ($advanced ? ADVANCED_PARAMETERS : STANDARD_PARAMETERS);

    return $config;
}

=head2 get_customer_config

Convenience method to get all customer_info data from the database.

=cut

sub get_customer_config {
    my $self = shift;

    return $self->result_source->schema->redis->get_or_set_json(
        'customer_config',
        sub {
            my $rs = $self->search({ parameter => { 'ILIKE' => 'customer_info_%' } });

            my %config;
            while (my $item = $rs->next) {
                my $key = $item->parameter;
                $key =~ s/^customer_info_//;
                $config{$key} = $self->_decode_config_item(
                    $item->parameter,
                    $item->value
                );
            }
            return \%config;
        }
    );
}

=head2 get_bag_config

Convenience method to get all bag data from the database.

=cut

sub get_bag_config {
    my $self = shift;

    return $self->result_source->schema->redis->get_or_set_json(
        'bag_config',
        sub {
            my $rs = $self->search({ parameter => { 'ILIKE' => 'bag_%' } });

            my %config;
            while (my $item = $rs->next) {
                my $key = $item->parameter;
                $config{$key} = $self->_decode_config_item($key, $item->value, 1);
            }
            return \%config;
        },
        {
            expire => 14400
        }
    );
}

=head2 get_email_template_by_parameter

Convenience method to get an e-mail template from the configuration

=cut

sub get_email_template_by_parameter {
    my ($self, $param) = @_;

    my $id = $self->get($param);
    if (!$id) {
        throw(
            "config/email_template/not_configured",
            "No e-mail template configured for '$param'"
        );
    }

    my $template = $self->result_source->schema->resultset(
        'BibliotheekNotificaties')->find($id);
    return $template if $template;
    throw(
        "config/email_template/not_found",
        "No e-mail template is found for '$param' with id '$id'"
    );
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 apply_defaults

TODO: Fix the POD

=cut

=head2 get

TODO: Fix the POD

=cut

=head2 get_all

TODO: Fix the POD

=cut

=head2 get_value

TODO: Fix the POD

=cut

=head2 save

TODO: Fix the POD

=cut

