package Zaaksysteem::Backend::Object::Roles::ObjectResultSet;

use Moose::Role;
use Moose::Util qw/ensure_all_roles/;
use Scalar::Util qw/blessed/;

use constant ZQL_BLESS_OBJECT => sub {
    my $method                      = shift;
    my $self                        = shift;

    my $row                         = $self->$method(@_);

    $self->_apply_zql_roles($row);

    return $row;
};

=head1 NAME

Z::B::Object::Roles::ObjectResultSet - Special ResultSet for ZQL Objects, blesses returned
rows with L<Zaaksysteem::Backend::Object::Roles::ObjectComponent>

=head1 SYNOPSIS

    my $rs      = $object->from_type('case');

    ok($rs->count, 'Found objects from type "case"');

    my $case    = $rs->first;

    ## $case has role: Z::B::Object::Roles::ObjectResultSet

=head1 DESCRIPTION

This class will ensure every returned row from the database has the ZQL Object Component role
applied L<Zaaksysteem::Backend::Object::Roles::ObjectComponent>


=cut

# ###
# ### _zql_options
# ###
# ### Options set by our ZQL handling, like notifying we would like to describe our object etc.

# has '_zql_options'         => (
#     'is'            => 'rw',
#     'lazy'          => 1,
#     'default'       => sub {
#         return {
#             describe_rows   => undef,
#         }
#     }
# );

=head1 ATTRIBUTES

=head2 describe_rows(BOOLEAN)

When set, it will describe the rows on JSON output

=cut

has 'describe_rows'         => (
    'is'            => 'rw',
    'isa'           => 'Bool',
    'trigger'       => sub {
        my $self                    = shift;
        my $val                     = shift;

        $self->{attrs}->{_zql_options}->{describe_rows} = $val;
    }
);

has hydrate_actions => (
    is => 'rw',
    isa => 'Bool',
    trigger => sub {
        my $self = shift;
        my $val = shift;

        $self->{ attrs }{ _zql_options }{ include_row_actions } = $val;
    }
);

=head2 object_requested_attributes($ARRAY)

Will let TO_JSON know which attributes to return

=cut

has 'object_requested_attributes'         => (
    'is'            => 'rw',
    # 'isa'           => 'Bool',
    'trigger'       => sub {
        my $self                    = shift;
        my $val                     = shift;

        $self->{attrs}->{_zql_options}->{requested_attributes} = $val;
    }
);

=head1 METHODS

=head2 all

Return value: @rows

Interferes with C<< $rs->all >> to return objects with the role
L<Zaaksysteem::Backend::Object::Roles::ObjectComponent> applied

=cut

around 'all'    => sub {
    my $method                      = shift;
    my $self                        = shift;

    my @rows                        = $self->$method(@_);

    $self->_apply_zql_roles(@rows);

    return @rows;
};

=head2 find

Return value: $rows

Interferes with C<< $rs->find >> to return objects with the role
L<Zaaksysteem::Backend::Object::Roles::ObjectComponent> applied

=cut

around 'find'   => sub {
    return ZQL_BLESS_OBJECT->(@_);
};

=head2 next

Return value: $rows

Interferes with C<< $rs->next >> to return objects with the role
L<Zaaksysteem::Backend::Object::Roles::ObjectComponent> applied

=cut

around 'next'   => sub {
    return ZQL_BLESS_OBJECT->(@_);
};

=head2 single

Return value: $rows

Interferes with C<< $rs->single >> to return objects with the role
L<Zaaksysteem::Backend::Object::Roles::ObjectComponent> applied

=cut

around 'single' => sub {
    return ZQL_BLESS_OBJECT->(@_);
};

=head1 INTERNAL METHODS

=head2 _apply_roles(@rows)

Return value: @rows

Applies role L<Zaaksysteem::Backend::Object::Roles::ObjectComponent> to every given
row in C<@rows>

=cut

sub _apply_zql_roles {
    my $self                        = shift;

    for my $row (grep { blessed($_) } @_) {
        ensure_all_roles(
            $row,
            'Zaaksysteem::Backend::Object::Roles::ObjectComponent'
        );

        $row->_initialize_zql($self->{attrs}->{_zql_options} || {});
    }
}

=head2 search

=head2 search_rs

Wrappers around "search" on the resultset that preserve the "describe_rows" flag.

=cut

around search => sub {
    my $orig = shift;
    my $self = shift;

    my $rs = $self->search_rs(@_);

    if (wantarray) {
        return $rs->all;
    }

    return $rs;
};

around search_rs => sub {
    my $orig = shift;
    my $self = shift;

    my $rv = $self->$orig(@_);

    $rv->describe_rows($self->describe_rows);

    return $rv;
};

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 ZQL_BLESS_OBJECT

TODO: Fix the POD

=cut

