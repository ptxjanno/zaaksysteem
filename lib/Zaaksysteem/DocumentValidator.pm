package Zaaksysteem::DocumentValidator;
use Moose;

with 'MooseX::Log::Log4perl';

use Data::Dumper;
use Params::Profile;
use File::Basename;
use File::MMagic;
use HTTP::Request::Common;
use LWP::UserAgent;
use Zaaksysteem::Constants qw/MIMETYPES_ALLOWED/;


#
# validate documents before they can be added to Zaaksysteem.
#
# In het kader van de nen2082 moet er een beperking worden ingevoerd
# voor het aantal bestandsformaten. Uitgangspunt voor de formaten die
# wel worden toegestaan is dat ze door ImageMagick of OpenOffice
# geconverteerd kunnen worden. De formaten die worden geaccepteerd zijn:
# Office:
#   .rtf
#   .doc
#   .dot
#   .docx
#   .xls
#   .xlsx
#   .ppt
#   .pptx
#   .odt
#   .ods
#   .odp
#   .pdf
#
#   Afbeeldingen:
#   .bmp
#   .gif
#   .jpeg
#   .jpg
#   .png
#   .tiff
#   .tif
#
#   Overige:
#   .htm
#   .html
#   .xml
#


Params::Profile->register_profile(
    method  => 'validate_path',
    profile => {
        required => [ qw/filepath/ ]
    }
);

sub validate_path {
    my ($self, $params) = @_;

    my $dv = Params::Profile->check(params => $params);
    die "invalid options for validate_path" unless $dv->success;

    my $filepath = $params->{filepath};

    my($filename, $directories, $suffix) = fileparse($filepath, qr/\.[^.]*/);
    my $suffix_lc = lc $suffix;

    my $mm       = new File::MMagic;
    my $mimetype = $mm->checktype_filename($filepath);

    my $determined_mimetype = $self->_mimetype_allowed({
            suffix_lc       => $suffix_lc,
            mimetype        => $mimetype,
            upload_mimetype => $mimetype,
    });

    return $suffix_lc && $mimetype && $determined_mimetype;
}


Params::Profile->register_profile(
    method  => 'validate_upload',
    profile => {
        required => [ qw/upload filename/ ]
    }
);

sub validate_upload {
    my ($self, $params) = @_;

    my $dv = Params::Profile->check(params => $params);
    die "invalid options for validate_upload" unless $dv->success;

    my $filename    = $params->{filename};
    my $upload      = $params->{upload};

    my $mm            = File::MMagic->new();

    my $mimetype = $mm->checktype_filehandle($upload->fh);
    my $mimetype_2 = $mm->checktype_filename($upload->tempname);

    # If we don't do this, $upload->fh will continue to exist, and code higher
    # up will try to put it in the session. But GLOBs are not storable.
    $upload->meta->find_attribute_by_name('fh')->clear_value($upload);

    $self->log->debug(
        "Checking document: [$filename], mimetype: [$mimetype], mimetype (by filename): [$mimetype_2], mimetype (by uploader): [" . $upload->type . "]"
    );

    my($filename_parsed, $directories, $suffix) = fileparse($filename, qr/\.[^.]*/);
    my $suffix_lc = lc $suffix;

    my $determined_mimetype = $self->_mimetype_allowed({
        suffix_lc => $suffix_lc,
        mimetype  => $mimetype,
        upload_mimetype => $upload->type,
    });

    unless($suffix_lc && $mimetype && $determined_mimetype) {
        $self->log->warn("Invalid suffix ($suffix_lc) or mimetype ($mimetype, $determined_mimetype, " . $upload->type . ")");
        return undef;
    }

    return $determined_mimetype;
}


Params::Profile->register_profile(
    method  => '_mimetype_allowed',
    profile => {
        required => [ qw/suffix_lc mimetype upload_mimetype/ ]
    }
);

sub _mimetype_allowed {
    my ($self, $params) = @_;

    my $dv = Params::Profile->check(params => $params);
    die "invalid options for _mimetype_allowed" unless $dv->success;

    my $suffix_lc = $params->{suffix_lc};
    my $mimetype = $params->{mimetype};
    my $upload_mimetype = $params->{upload_mimetype};

    my $mimetypes_allowed = MIMETYPES_ALLOWED->{$suffix_lc};

    my %mimetypes = ($mimetypes_allowed->{mimetype} => 1);
    if(my $alternate_mimetypes = $mimetypes_allowed->{alternate_mimetypes}) {
        foreach my $mimetype (@$alternate_mimetypes) {
            $mimetypes{$mimetype} = 1;
        }
    }

    if (exists $mimetypes{$mimetype} || exists $mimetypes{$upload_mimetype}) {
        return $mimetypes_allowed->{mimetype};
    }
    return undef;

}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
