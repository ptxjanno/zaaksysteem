package Zaaksysteem::View::ZAPI::CSV;
use Moose;

BEGIN { extends 'Catalyst::View'; }

use BTTW::Tools;
use Encode qw(encode_utf8);
use File::Temp;
use Scalar::Util qw/blessed/;
use Text::CSV;
use Zaaksysteem::DocumentConverter;
use List::Util qw(any);

with 'MooseX::Log::Log4perl';

__PACKAGE__->config(
    'stash_key'             => 'csv',
    'quote_char'            => '"',
    'escape_char'           => '"',
    'sep_char'              => ',',
    'eol'                   => "\n",
    'binary'                => 1,
    'allow_loose_quotes'    => 1,
    'allow_loose_escapes'   => 1,
    'allow_whitespace'      => 1,
    'always_quote'          => 1
);

use constant EXPORT_FILETYPE_CONFIG => {
    xls         => { mimetype => 'application/vnd.ms-excel', extension => 'xls' },
    calc        => { mimetype => 'application/vnd.oasis.opendocument.spreadsheet', extension => 'ods' },
    csv         => { mimetype => 'text/csv', extension => 'csv' },
};

has converter => (
    isa     => 'Zaaksysteem::DocumentConverter',
    is      => 'ro',
    lazy    => 1,
    default => sub { return Zaaksysteem::DocumentConverter->new(); },
);

=head1 METHODS

=head2 $csv->process($c, \%OPTIONS)

Return value: $STRING_CONTENT

    $csv->process($c, { format => 'csv', zapi_object => [{beer => 'heineken'}] });

Called from catalyst when processing this view.

B<Options>

=over 4

=item zapi_object (required)

The ZAPI object to be converted to CSV

=item format (required)

The format of the output: instead of C<csv>, you could use the formats C<csv> or C<calc>

=back

=cut

define_profile 'process'    => (
    required            => [qw/zapi_object format/],
    constraint_methods  => {
        format      => qr/csv|xls|calc/,
    },
    field_filters       => {
        format      => ['lc'],
    }
);

sub process {
    my $self                = shift;
    my $c                   = shift;
    my $options             = assert_profile(shift || {})->valid;

    my $zapi_object         = $options->{zapi_object};
    my $format              = $options->{format};

    my $blacklist = $self->_setup_blacklist($c);

    my $processed_result    = $self->_process_zapi_result(
        $zapi_object,
        $c->stash->{zapi}->{result},
        $blacklist,
    );

    $c->stash->{csv}        = $processed_result->{rows};

    my $content = $self->render( $c, undef, $zapi_object, $c->stash );

    $self->post_process_content($c, $format, $content, $processed_result);
}

=head2 $csv->post_process_content($c, $format, $string_content)

Return value: $STRING_CONTENT

    $csv->post_process_content($c, 'xls',"case.number,casetype.name\n1,Testzaaktype");

Post processor for C<process>. Will convert the default CSV format to the given format.

Will use the L<Zaaksysteem::DocumentConverter> to create C<xls> and C<calc>,
also will set the correct content_type and content-disposition.

B<Options>

=over 4

=item $format

The format of the output: instead of C<csv>, you could use the formats C<csv> or C<calc>

=back

=cut

sub post_process_content {
    my $self                = shift;
    my $c                   = shift;
    my $format              = shift;
    my $raw_content         = shift;
    my $processed_content   = shift;

    my $content;
    $c->res->headers->header(
        'Content-Disposition'  =>
            "attachment; filename=\"zaaksysteem-" . time()
            . "." . EXPORT_FILETYPE_CONFIG->{ $format }->{extension} . "\""
    );

    if ($format eq 'csv') {
        $c->res->headers->header( "Content-Type" => "text/csv" )
            unless ( $c->res->headers->header("Content-Type") );

        $content    = $raw_content;
    } else {
        $c->res->headers->header( 'Content-Type'  => 'application/x-download' );

        my $column_types = $processed_content->{header_data}->{column_types};
        $content = $self->converter->convert_scalar(
            destination_type => EXPORT_FILETYPE_CONFIG->{$format}{'mimetype'},
            source           => encode_utf8($raw_content),
            filter_options   => {
                column_types    => $column_types,
                force_from_type => 'text/csv',
                locale          => 'nl_NL',
            }
        );
    }

    $c->res->body($content);
}

sub _load_csv_header {
    my $self            = shift;
    my $zapi_object     = shift;
    my $result          = shift;


    my $rv              = {
        column_order => [],
        column_types => [],
    };

    if (
        $zapi_object->options &&
        $zapi_object->options->{csv}->{column_order}
    ) {
        return $zapi_object->options->{csv};
    }

    my $first_row;
    if (blessed($result)
        && (   $result->isa('DBIx::Class::ResultSet')
            || $result->isa('Zaaksysteem::Object::Iterator')
        )
    ) {
        $first_row = $result->first;
    }
    else {
        $first_row = $result->[0];
    }

    if (blessed($first_row) && $first_row->isa('DBIx::Class')) {
        if ($first_row->can('csv_header')) {
            my $header = $first_row->csv_header;

            if (
                ref($header) eq 'ARRAY' && ref $header->[0] eq 'HASH'
            ) {
                $rv->{column_types} = $self->_format_column_types($header);
                $rv->{column_order} = [ map { $_->{label} } @$header ];
                $rv->{attr_order}   = [ map { $_->{ attribute_name } } @$header ];
                return $rv;
            } else {
                $rv->{column_order}     = $header;
                return $rv;
            }
        } else {
            my %columns = $first_row->get_columns;
            $rv->{column_order} = [ keys %columns ];
            return $rv;
        }
    } elsif (blessed($first_row) && $first_row->isa('Zaaksysteem::Object')) {
        $rv->{column_order} = [ $first_row->attribute_names ];
        return $rv;
    }

    return $rv;
}

sub _format_column_types {
    my $self   = shift;
    my $header = shift;

    my @column_format = ();
    for (my $i = 0; $i < scalar(@$header); $i++) {
        if (
            $header->[$i]->{attribute_type} &&
            (
                $header->[$i]->{attribute_type} eq 'date' ||
                $header->[$i]->{attribute_type} eq 'timestamp' ||
                $header->[$i]->{attribute_type} eq 'timestamp_or_text'
            )
        ) {
            push @column_format, 'datetime';
        }
        else {
            push @column_format, 'default';
        }
    }

    return \@column_format;
}

=head2 $csv->_process_zapi_result($zapi_object, $result_from_zapi)

Return value: \@ROWS

    $csv->post_proce_process_zapi_result($zapi_object, $c->stash->{zapi}->{result});

Will turn the rows given in C<<$c->stash->{zapi}->{result} into a CSV format. By finding out
the header for the CSV, and places the output just below it.

B<Options>

=over 4

=item $zapi_object

Raw zapi_object. An instance of L<<Zaaksysteem::ZAPI::Response>>

=item $result_from_zapi

Because the ZAPI system can put bounds to our resultset, by setting pagination for instance,
this flag is ARRAYREF of rows to convert.

=back

=cut

sub _process_zapi_result {
    my ($self, $zapi_object, $result, $blacklist) = @_;

    my @rows;

    throw(
        'zaaksysteem/view/zapi/csv',
        'No zapi attribute found, cannot generate csv'
    ) unless $result;

    my $header_data     = $self->_load_csv_header($zapi_object, $result);

    my $column_order    = $header_data->{column_order};

    unless (
        $zapi_object->options &&
        $zapi_object->options->{csv}{no_header}
    ) {
        push(@rows, $column_order) if $column_order;
    }

    # loop through results, use the column order
    if (blessed($result)
        && (   $result->isa('DBIx::Class::ResultSet')
            || $result->isa('Zaaksysteem::Object::Iterator')
        )
    ) {
        $result->reset;

        while (defined(my $entry = $result->next)) {
            push(@rows, $self->_parse_csv_from_entry($entry, $header_data, $blacklist));
        }
    } else {
        foreach my $entry (@$result) {
            push(@rows, $self->_parse_csv_from_entry($entry, $header_data, $blacklist));
        }
    }

    return {
        rows        => \@rows,
        header_data => $header_data
    };
}

sub _setup_blacklist {
    my $self  = shift;
    my $c     = shift;

    my @mapping;
    if ($c->user->has_legacy_permission('view_sensitive_data')) {
        return \@mapping;
    }
    my $hide = $c->stash->{zapi_hide_mappings} // {};
    @mapping = map { $_ } keys %$hide;
    return \@mapping;
}

sub is_empty_attr_label {
    my $val = shift;
    return 1 if (!defined $val || !length($val));
    return 0;
}

sub _parse_csv_from_entry {
    my ($self, $entry, $header_data, $blacklist) = @_;

    my $column_order = $header_data->{ column_order };

    my $header_empty = any { is_empty_attr_label($_) } @$column_order;

    my $row;
    if (blessed($entry) && $entry->isa('DBIx::Class')) {
        if ($entry->can('csv_data')) {
            # It seems cases are now exported by csv_data
            $entry->blacklisted_columns($blacklist);

            # This is gonna be a performance hog..
            if ($header_empty) {
                my $header_new = $entry->csv_header;
                for (my $i = 0; $i < @$column_order; $i++) {
                    next unless is_empty_attr_label($column_order->[$i]);
                    my $label = $header_new->[$i]{label};
                    next if is_empty_attr_label($label);
                    $column_order->[$i] = $label;
                }
            }

            $row = $entry->csv_data;
        } else {
            my $rowdata    = { $entry->get_columns };
            $row    = [
                map { $rowdata->{ $_ } } @$column_order
            ];
        }
    }
    elsif(blessed($entry) && $entry->isa('Zaaksysteem::Object')) {
        my @row;

        for my $attr_name (@{ $header_data->{ attr_order } }) {
            my $attr = $entry->attribute_instance($attr_name);

            my $value = $attr ? $attr->value : undef;

            if (ref $value eq 'ARRAY') {
                $value = join(',', @{ $value } );
            }

            push @row, $value;
        }

        return \@row;
    }
    else {
        $row = [
            map { $entry->{$_} } @$column_order
        ];
    }
    return $row;
}



sub csv_options {
    my ($self, $arguments) = @_;

    my $options     = $arguments->{options}    or die "need options";

    my $config = $self->config;

    if(my $csv_options = $options->{csv}) {
        foreach my $key (keys %$config) {
            if(my $override = $csv_options->{$key}) {
                $config->{$key} = $override;
            }
        }
    }
    return $config;
}

=head1 METHODS

=head2 $csv->render($c, $template_name, $zapi_object, $params)

Return value: $STRING_CONTENT

    $csv->render($c, undef, $zapi_object, { 'csv' => $rows });

Called from catalyst when processing this view.

B<Options>

=over 4

=item $template_name

Because every view gets a template in the second argument, we make sure we still are
"interface correct". We do NOTHING with this var. Setting C<undef> has no effect.

=item $zapi_object (required)

The ZAPI object to be converted to CSV/ An instance of L<<Zaaksysteem::ZAPI::Response>>

=item params

Normally the stash, from where it can pick the C<stash_key> defined in our config. Defaults
to C<csv>.

=back

=cut

sub render {
    my $self = shift;
    my ( $c, $template, $zapi_object, $args ) = @_;

    my $config = $self->config;

    if(my $options = $zapi_object->options) {
        $config = $self->csv_options({
            options => $options,
        });
    }

    my $stash_key = $self->config->{'stash_key'};
    return '' unless $args->{$stash_key} && ref($args->{$stash_key}) =~ /ARRAY/;

    $config = { map { $_ => $config->{$_} } qw/
        quote_char
        escape_char
        sep_char
        eol
        binary
        allow_loose_quotes
        allow_loose_escapes
        allow_whitespace
        always_quote/
    };

    my $csv = Text::CSV->new($config);

    my $content = '';
    foreach my $row ( @{ $args->{$stash_key} } ) {
        # Skip empty rows:
        next unless scalar @{ $row };

        ### Blessed objects need to be down translated
        my $status = $csv->combine( @{ $row } );
        Catalyst::Exception->throw(
            "Text::CSV->combine Error: " . $csv->error_diag() )
          if ( !$status );
        $content .= $csv->string();
    }

    return $content;
}

1;




__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 EXPORT_FILETYPE_CONFIG

TODO: Fix the POD

=cut

=head2 csv_options

TODO: Fix the POD

=cut

