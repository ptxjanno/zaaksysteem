package Zaaksysteem::Object::Types::Config::Category;

use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Object';

=head1 NAME

Zaaksysteem::Object::Types::Config::Category - Data wrapper for
C<config/category> objects

=head1 DESCRIPTION

=cut

use BTTW::Tools;
use Syzygy::Types qw[Name];

=head1 METHODS

=head2 type

Returns the object type string C<config/category>.

=cut

override type => sub { return 'config/category' };

=head1 ATTRIBUTES

=head2 name

Localized name for the category

=cut

has name => (
    is => 'rw',
    isa => 'Str',
    label => 'Naam van de categorie',
    traits => [qw[OA]],
    required => 1,
);

=head2 slug

Internal slug/shortname for the category. This name is used for
cross-referencing categories in C<config/definition> objects.

=cut

has slug => (
    is => 'rw',
    isa => Name,
    label => 'Interne referentienaam voor de categorie',
    traits => [qw[OA]],
    required => 1
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
