package Zaaksysteem::Object::Types::Case::Result;

use Moose;

use Zaaksysteem::Types qw[Datestamp CaseResult];

extends 'Zaaksysteem::Object';

=head1 NAME

Zaaksysteem::Object::Types::Case::Result - Data carrier object for case
result instances

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 name

Name of the result.

=cut

has name => (
    is => 'rw',
    isa => 'Str',
    label => 'Naam',
    traits => [qw[OA]],
    required => 1
);

=head2 result

The actual result selected in casetype panel

=cut

has result => (
    is => 'rw',
    isa => CaseResult,
    label => 'Resultaat',
    traits => [qw[OA]],
    required => 1
);

=head2 dossier_type

Defines the type of the dossier, pyshical or digital.

=cut

has dossier_type => (
    is => 'rw',
    isa => 'Str',
    label => 'Dossiertype',
    traits => [qw[OA]]
);

=head2 archival_type

Defines what should happen with the dossier after the L</retention_period>
has expired.

=cut

has archival_type => (
    is => 'rw',
    isa => 'Str',
    label => 'Archiefnominatie',
    traits => [qw[OA]]
);

=head2 retention_period

Retention period of the dossier, expressed in days after completion of the
dossier.

=cut

has retention_period => (
    is => 'rw',
    isa => 'Int',
    label => 'Bewaartermijn',
    traits => [qw[OA]]
);

=head2 selection_list

Name of the selection list which covers the result in accordance with the
municipal process description.

=cut

has selection_list => (
    is => 'rw',
    isa => 'Str',
    label => 'Selectielijst',
    traits => [qw[OA]]
);

=head2 selection_list_start

Start date of the selection list.

=cut

has selection_list_start => (
    is => 'rw',
    isa => Datestamp,
    coerce => 1,
    label => 'Selectielijst brondatum',
    traits => [qw[OA]]
);

=head2 selection_list_end

End date of the selection list.

=cut

has selection_list_end => (
    is => 'rw',
    isa => Datestamp,
    coerce => 1,
    label => 'Selectielijst einddatum',
    traits => [qw[OA]]
);

=head1 METHODS

=head2 type

Overrides L<Zaaksysteem::Object/type>, always returns the string C<case/result>.

=cut

override type => sub { 'case/result' };

=head2 TO_STRING

Overrides L<Zaaksysteem::Object/TO_STRING>, returns the L</name> of the
result.

=cut

override TO_STRING => sub {
    return shift->name;
};

__PACKAGE__->meta->make_immutable

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
