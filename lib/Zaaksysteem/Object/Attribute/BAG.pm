package Zaaksysteem::Object::Attribute::BAG;
use Moose::Role;

=head1 NAME

Zaaksysteem::Object::Attribute::BAG - Role for an attribute of type 'BAG'

=head1 METHODS

=head2 _build_human_value

Return the "human-readable" (in this case, "front-end parseable") value for
this field.

=cut

sub _build_human_value {
    my $self = shift;
    return unless $self->value;

    if (ref($self->value) eq 'ARRAY') {
        return [
            map { $_->{human_identifier} } @{$self->value}
        ];
    }

    return $self->value->{human_identifier};
}

=head2 _build_index_value

Return the "indexable" (searchable) value for a BAG field: its (like
"openbareruimte-123456")

=cut

sub _build_index_value {
    my $self = shift;
    my ($value) = @_;
    return unless $value;

    if (ref($value) eq 'ARRAY') {
        return [
            map { $_->{bag_id} } @{$value}
        ];
    }

    return $value->{bag_id};
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

