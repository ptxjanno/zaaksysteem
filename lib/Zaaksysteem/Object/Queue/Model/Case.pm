package Zaaksysteem::Object::Queue::Model::Case;

use Moose::Role;

use Zaaksysteem::Constants qw(
    LOGGING_COMPONENT_ZAAK
);

use BTTW::Tools;
use Zaaksysteem::Types qw[UUID CaseNumber];

use Zaaksysteem::Object::Types::Address;
use Zaaksysteem::Object::Types::Location;
use Zaaksysteem::Zaken::Model;
use Zaaksysteem::Backend::Sysin::Auth::Alternative;

=head1 NAME

Zaaksysteem::Object::Queue::Model::Case - Case queue item handler

=head1 DESCRIPTION

=head1 METHODS

=head2 create_case_subcase

=cut

sig create_case_subcase => 'Zaaksysteem::Backend::Object::Queue::Component';

sub create_case_subcase {
    my $self = shift;
    my $item = shift;

    my $case_model;
    if($item->subject) {
        $case_model = $self->case_model($item->subject->id);
    } else {
        $case_model = $self->case_model();
    }

    my %start_subcase_args = (
        action_data      => $item->data,
        object_model     => $self->object_model,
        betrokkene_model => $self->betrokkene_model,
        case_model       => $case_model,
    );
    $start_subcase_args{current_user} = $item->subject if $item->subject;

    $item->object_data->get_source_object->start_subcase(%start_subcase_args);

    return;
}

=head2 create_case_document

=cut

sig create_case_document => 'Zaaksysteem::Backend::Object::Queue::Component';

sub create_case_document {
    my $self = shift;
    my $item = shift;

    my %template_action_args = (
        action_data => $item->data
    );

    if ($item->subject) {
        $template_action_args{ current_user } = $item->subject;
    }

    my $file = $item->object_data->get_source_object->template_action(
        %template_action_args
    );

    ### Detected an external template, act accordingly
    if (ref $file eq 'HASH' && exists $file->{type}) {
        $item->data($file);
    } else {
        my $data = $item->data;

        if ($data->{interface_id}) {
            # In case of external templates we have no clue what the file id
            # is or will be, posted on a later time to ZS
            $data->{result} = {
                file_id         => undef,
                file_store_uuid => undef,
            };

        }
        elsif (blessed $file) {
            $data->{result} = {
                file_id        => $file->id,
                filestore_uuid => $file->filestore_id->uuid,
            };
        }
        else {
            throw('qitem/create_case_document/state/unknown',
                "Not an external template and not a file object, unknown failure condition"
            );
        }

        $item->data($data);
    }

    return;
}

=head2 update_case_location

Handler for C<update_case_location> queue item instances.

=cut

sig update_case_location => 'Zaaksysteem::Backend::Object::Queue::Component';

sub update_case_location {
    my $self = shift;
    my $item = shift;

    my $zaak = $item->object_data->get_source_object;
    my $location = Zaaksysteem::Object::Types::Location->new($item->data);
    $zaak->update_location($location);
    $zaak->touch;

    return;
}

=head2 allocate_case

=cut

sig allocate_case => 'Zaaksysteem::Backend::Object::Queue::Component';

sub allocate_case {
    my $self = shift;
    my $item = shift;

    $item->object_data->get_source_object->allocation_action($item->data);

    return;
}

=head2 transition_case_phase

=cut

sub transition_case_phase {
    my $self = shift;
    my $item = shift;

    my $subject_id = $item->subject_id;

    unless ($subject_id) {
        throw('queue/create/case/subject', sprintf(
            'Subject is missing, unable to process queue item "%s"',
            $item->stringify
        ));
    }

    my $model = $self->case_model($subject_id);
    my $zaak = $model->rs_zaak->find($item->data->{ case_number });

    $model->execute_phase_actions($zaak, $item->data->{ transition_args });

    $zaak->touch;

    return;
}

=head2 case_event_trampoline

Allows the indirection of C<Zaaksysteem::Zaken::Model::emit_case_event()>
calls, for the places in Zaaksysteem where Catalyst context is lost.

Calls C<emit_case_event> for the given C<case_number> and C<event_name>.

=cut

sub case_event_trampoline {
    my $self = shift;
    my $item = shift;

    my $case_model = $self->case_model($item->subject_id);

    my $case = $case_model->rs_zaak->find($item->data->{ case_number });

    $self->emit_case_event({
        case => $case,
        event_name => $item->data->{ event_name },
    });

    return;
}

=head2 add_case_subject

Add a new subject to a case, in a specified role.

If the (role, subject) combination already exists for this case, this queue
item will do nothing.

=cut

sub add_case_subject {
    my $self = shift;
    my $item = shift;

    my $case = $item->object_data->get_source_object;
    my $schema = $case->result_source->schema;

    my $subject = $schema->betrokkene_model->get_by_string($item->data->{betrokkene_identifier});

    if (
        !$subject &&
        (
            $subject->can('gm_extern_np') &&
            $subject->gm_extern_np &&
            $subject->gm_extern_np->can('deleted_on') &&
            $subject->gm_extern_np->deleted_on
        )
    ) {
        $self->log->warn(sprintf("Subject with identifier %s not found", $item->data->{betrokkene_identifier}));
        return;
    }

    my $rv = $case->betrokkene_relateren({
        betrokkene_identifier  => $item->data->{betrokkene_identifier},
        magic_string_prefix    => $item->data->{magic_string_prefix},
        rol                    => $item->data->{rol},
        pip_authorized         => $item->data->{gemachtigd},
        send_auth_confirmation => $item->data->{notify},
    });

    if ($rv && $item->data->{notify} && $item->data->{gemachtigd}) {
        try {
            my $template_id = $schema->resultset('Config')->get(
                'subject_pip_authorization_confirmation_template_id'
            );

            unless($template_id) {
                my $message = 'Kon geen e-mail notificatie template vinden, geen notificatie verstuurd naar ingestelde betrokkene';
                $self->log->error($message);

                return;
            }

            my $template = $schema->resultset('BibliotheekNotificaties')->find($template_id);

            unless($template) {
                my $message = 'Notificatie template kon niet gevonden worden, geen e-mail notificatie verstuurd';
                $self->log->warn($message);
                return;
            }

            unless($subject->email) {
                my $message = sprintf(
                    'Geen e-mail adres gevonden voor "%s", geen notificatie verstuurd',
                    $subject->display_name
                );
                $self->log->warn($message);
                return;
            }

            $case->mailer->send_case_notification({
                notification => $template,
                recipient => $subject->email
            });


        } catch {
            $self->log->warn('Sending e-mail to authorized subject for subcase failed: ' . $_);
        };
    }

    if (!$rv) {
        $case->trigger_logging(
            'case/subject/exists',
            {
                component => LOGGING_COMPONENT_ZAAK,
                data => {
                    case_id      => $case->id,
                    subject_name => $item->data->{naam},
                    role         => $item->data->{rol},
                },
            },
        );
    }

    return 1;
}

=head2 touch_case

Handler for asynchronous case touches.

=cut

define_profile touch_case => (
    optional => {
        case_object_id => UUID,
        case_number => CaseNumber
    },
    require_some => {
        case_identifier => [ 1, qw[case_object_id case_number] ]
    }
);

sub touch_case {
    my $self = shift;
    my $item = shift;

    my $args = assert_profile($item->data)->valid;

    my $zaak;

    if (exists $args->{ case_object_id }) {
        my $case = $self->object_model->new_resultset->find(
            $args->{ case_object_id }
        );

        $zaak = $case->get_source_object;
    } else {
        $zaak = $self->_rs_zaak->find($args->{ case_number });
    }

    unless (defined $zaak) {
        throw('queue/case/touch/not_found', sprintf(
            'Could not find case by identifier "%s"',
            $args->{ case_object_id } || $args->{ case_number }
        ));
    }

    $zaak->_touch(no_db_lock => 1);

    return;
}

=head2 touch_case_v0

Be able to synchronise a case to both v0 and v1. This unit is only for manual
touches to make sure both api/v0 and api/v1 are synched after development
changes

=cut

sub touch_case_v0 {
    my $self = shift;
    my $item = shift;

    my $zaak = $self->_rs_zaak->find($item->data->{ case_number });

    my $case_property = $self->schema->resultset('CaseProperty')->search_rs(
        { case_id => $zaak->id }
    );

    if ($zaak->is_deleted) {
        $case_property->delete;
        $zaak->object_data;
        return;
    }

    $case_property->search_rs({namespace => 'case'})->delete;
    $zaak->ddd_update_system_attributes();
    $zaak->touch();
}

=head2 create_case

Create a case via the queue model.

=cut

sig create_case => 'Zaaksysteem::Backend::Object::Queue::Component';

sub create_case {
    my ($self, $item) = @_;

    my $subject_id = $item->subject_id;

    unless ($subject_id) {
        throw('queue/create/case/subject', sprintf(
            'Subject is missing, unable to process queue item "%s"',
            $item->stringify
        ));
    }

    my $model = $self->case_model($subject_id);

    my $create_arguments = $model->prepare_case_arguments(
        $item->data->{ create_args }
    );

    $create_arguments->{ case_id } = $item->data->{ case_id };

    $model->create_case($create_arguments);
    return;
}

sig create_case_form => 'Zaaksysteem::Backend::Object::Queue::Component';

sub create_case_form {
    my ($self, $item) = @_;

    my %data = %{$item->data->{ create_args }};
    $data{override_zaak_id} = 1;
    $data{id} = $item->data->{ case_id };

    my $uploads = delete $data{uploads};
    my $zaak = $self->_rs_zaak->create_zaak(\%data);

     $self->case_model->execute_phase_actions($zaak,
        { phase => 1 });

    my $schema = $zaak->result_source->schema;

    if ($uploads) {
        my @attributes = keys %$uploads;
        my $zaaktype_kenmerken = $zaak->zaaktype_node_id->zaaktype_kenmerken->search(
            {
                'bibliotheek_kenmerken_id.id'         => { -in => \@attributes },
                'bibliotheek_kenmerken_id.value_type' => 'file',
            },
            { join => 'bibliotheek_kenmerken_id' }
            );

        while (my $ztk = $zaaktype_kenmerken->next) {
            my $bieb = $ztk->get_column('bibliotheek_kenmerken_id');
            my $id = $ztk->id;

            my @files = @{$uploads->{$bieb}};
            my $file_rs = $schema->resultset('File')->search_rs({
                'me.id' => { -in => \@files }
            });
            $file_rs->update({case_id => $zaak->id});

            $file_rs->reset;

            while (my $file = $file_rs->next) {
                $file->set_or_replace_case_documents($id);
                $file->apply_case_document_defaults;
            }
        }
    }

    # mimic linking auth two fa accounts to a subject via /form
    if (my $id = $item->data->{auth_two_fa_subject_id}) {
        if (my $subject = $schema->resultset('Subject')->find($id)) {

            my $interface = $schema->resultset('Interface')->search_active(
                {module => 'auth_twofactor'}
            )->first;


            my $model = Zaaksysteem::Backend::Sysin::Auth::Alternative->new(
                schema            => $schema,
                interface         => $interface,
                sms_sender        => $interface->jpath('$.sms_sender'),
                sms_endpoint      => $interface->jpath('$.sms_endpoint'),
                sms_product_token => $interface->jpath('$.sms_product_token'),
            );

            if (!$model->get_subject_link($subject)) {
                if (my $betrokkene = $model->find_betrokkene_by_subject($subject)) {
                    $model->set_subject_link($subject, $betrokkene);
                    return 1;
                }
            }
        }

    }

    return;
}

sub case_update_deadline {
    my ($self, $item) = @_;

    my $zaak_rs = $self->_rs_zaak->search_rs({
        status => { -in => [qw(open new)] }
    });

    # RAW SQL:
    # update zaak set current_deadline = jsonb_set (
    #     jsonb_set (
    #         current_deadline,
    #         '{current}',
    #         to_jsonb(
    #         (current_deadline - >> 'current')::int + 1)::text
    #     ),
    #    '{last_modified}',
    #    to_jsonb ( NOW ()),
    #    true
    # )
    # WHERE ( current_deadline->> 'start')::timestamp::date <= NOW() - INTERVAL '24h'
    # AND ( ( current_deadline->> 'last_modified')::timestamp <= NOW() - INTERVAL '24h'
    #      OR current_deadline->> 'last_modified' is null)
    # ;
    #
    my $updated_cases = $zaak_rs->search_rs(
        {
            "(current_deadline->>'start')::timestamp::date" => {
                '<=' => \
                    "NOW()- INTERVAL '24h' and ((current_deadline->>'last_modified')::timestamp <=  NOW() - INTERVAL '24h' or current_deadline->>'last_modified' is null)"
            }
        }
    );

    my @ids = $updated_cases->get_column('id')->all;

    my $sql = qq{
    jsonb_set(
        jsonb_set(current_deadline,
                  '{current}',
                  ((current_deadline->>'current')::int + 1)::text::jsonb
        )
    ,
    '{last_modified}',
    to_jsonb(NOW()),
    true)
};

    $updated_cases->reset;
    $updated_cases->update({current_deadline => \$sql });

    my $cases = $zaak_rs->search_rs({id => { '-in' => \@ids }});
    while (my $case = $cases->next) {
        $case->touch;
    }

}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
