package Zaaksysteem::Object::Queue::Model::File;
use Moose::Role;

use BTTW::Tools;
use File::Temp;

=head1 NAME

Zaaksysteem::Object::Queue::Model::File - File queue item handler

=head1 DESCRIPTION

=head1 METHODS

=head2 generate_preview_pdf

=head2 set_search_terms

=head2 generate_thumbnail

Generate a PDF from a document

=cut

sig generate_preview_pdf => 'Zaaksysteem::Backend::Object::Queue::Component';

sub generate_preview_pdf {
    my ($self, $item) = @_;

    my ($file) = $self->_process_item($item);

    if ($file->_get_preview_pdf) {
        return 1;
    }

    $file->generate_pdf(preview => 1);
    return 1;
}

sig set_search_terms => 'Zaaksysteem::Backend::Object::Queue::Component';

sub set_search_terms {
    my ($self, $item) = @_;

    my ($file) = $self->_process_item($item);
    $file->set_search_terms;
    return 1;
}

sig generate_thumbnail => 'Zaaksysteem::Backend::Object::Queue::Component';

sub generate_thumbnail {
    my ($self, $item) = @_;

    my ($file, $data) = $self->_process_item($item);

    delete $data->{ target };

    if ($file->_get_thumbnail($data)) {
        return 1;
    }

    # Ensure width/height defaults are used if none exist in the queue-item
    my %args;
    $args{width}  = $data->{max_width}  if $data->{max_width};
    $args{height} = $data->{max_height} if $data->{max_height};

    $file->generate_thumbnail(\%args);
    return 1;
}


sig _process_item => 'Zaaksysteem::Backend::Object::Queue::Component';

sub _process_item {
    my ($self, $item) = @_;

    my $schema = $item->result_source->schema;
    my $data   = $item->data;
    my $id     = delete $data->{file_id};
    my $file   = $schema->resultset('File')->find($id, {
        prefetch => 'filestore_id'
    });

    return ($file, $data);
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
