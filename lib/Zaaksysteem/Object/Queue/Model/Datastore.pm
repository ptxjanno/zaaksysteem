package Zaaksysteem::Object::Queue::Model::Datastore;

use Moose::Role;

use BTTW::Tools;
use Zaaksysteem::Datastore::Model;
use Zaaksysteem::Export::Model;

=head1 NAME

Zaaksysteem::Object::Queue::Model::Datastore - Datastore queue item handler

=head1 DESCRIPTION

=head1 METHODS

=head2 export

=cut

sig datastore_export => 'Zaaksysteem::Backend::Object::Queue::Component';

sub datastore_export {
    my $self = shift;
    my $item = shift;

    my $data = $item->data;

    my $model = Zaaksysteem::Datastore::Model->new(
        schema => $self->schema,
        type   => $data->{class},
    );

    my $user = $self->find_subject($data->{user_id});

    my $fh = $model->export($data->{export_params});

    my $export = Zaaksysteem::Export::Model->new(
        schema     => $self->schema,
        user       => $user,
        request_id => $item->metadata->{request_id},
        uri        => URI->new_abs('/exportqueue', $self->base_uri),
    );

    my $time     = DateTime->now()->set_time_zone('Europe/Amsterdam');
    my $filename = sprintf("Export-gegevensmagazijn-%s-%s.csv",
        $model->type, join('T', $time->ymd(''), $time->hms('')));

    $export->add_as_export($fh, $filename);
    return;
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
