package Zaaksysteem::Object::AttributeRole::DaysPercentage;

use Moose::Role;

use DateTime;
use List::Util qw(max);
use BTTW::Tools;

with 'Zaaksysteem::Object::AttributeRole';

=head1 NAME

Zaaksysteem::Object::AttributeRole::DaysPercentage - "Percentage of days left" calculated attribute

=head1 SYNOPSIS

    my $attr = Zaaksysteem::Object::Attribute->new(
        dynamic_class => 'DaysPercentage',
        parent_object => $some_obj,
        ...
    );
    print $attr->value . "% of allotted time has passed";

=head1 METHODS

See L<Zaaksysteem::Object::AttributeRole> for the inherited wrapper around C<value>.

=cut

sub _calculate_value {
    my $self = shift;

    my $target       = $self->_get_attribute('case.date_target')->value;
    my $completion   = $self->_get_attribute('case.date_of_completion')->value;
    my $registration = $self->_get_attribute('case.date_of_registration')->value;

    # The target can be a string ("Opgeschort"), in that "percentage" is nonsensical.
    if (!blessed($target)) {
        return undef;
    }

    if (blessed($completion)) {
        $completion = $completion->epoch - $registration->epoch;
    }
    else {
        $completion = DateTime->now()->epoch - $registration->epoch;
    }

    my $percentage = sprintf("%d", 100 * ($completion / max($target->epoch - $registration->epoch, 1)));

    return $percentage + 0;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

