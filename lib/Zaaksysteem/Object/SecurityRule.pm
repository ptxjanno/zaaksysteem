package Zaaksysteem::Object::SecurityRule;

use Moose;
use namespace::autoclean;

use Moose::Util::TypeConstraints qw[enum duck_type];

use BTTW::Tools;

=head1 NAME

Zaaksysteem::Object::SecurityRule - Encode security rules as objects

=head1 DESCRIPTION

This class encodes the data for a security rule.

    my $rule = Zaaksysteem::Object::SecurityRule->new(
        entity      => $object_implementing_security_identity,
        verdict     => 'permit',
        capability  => 'read'
    );

    if ($rule->compare($rule2)) {
        print "Rules are the same!";
    }

=head1 ATTRIBUTES

=head2 entity

This attribute holds a reference to a L<Zaaksysteem::Object::SecurityIdentity>
object.

=cut

has entity => (
    is => 'rw',
    isa => duck_type([qw[security_identity]]),
    required => 1
);

=head2 capability

This attribute encodes the capability the rule applies to. It is a plain string
and has no further validation or constraint. When not set, the capability
is assumed to mean 'any'.

=cut

has capability => (
    is => 'rw',
    isa => 'Str',
    predicate => 'has_capability'
);

=head2 groupname

=cut

has groupname => (
    is => 'rw',
    isa => 'Maybe[Str]',
);

=head1 METHODS

=head2 compare

This method (which can also be called as a function) compares two rules and
returns true if they are logically equivalent, so different instances with the
same rule attributes will be considered equal.

=cut

sub compare {
    my ($a, $b) = @_;

    for my $side ($a, $b) {
        unless (blessed $side && $side->isa(__PACKAGE__)) {
            throw(
                'object/security_rule',
                'Unable to compare security rules, argument not a SecurityRule instance'
            );
        }
    }

    # Sometimes, $a or $b are database objects instead of "proper" objects
    # That means they won't have a "compare" method. Hence the slightly ugly
    # calling style:
    return unless Zaaksysteem::Object::SecurityIdentity::compare($a->entity, $b->entity);

    # If either rule has no capability, assume wildcard.
    if ($b->has_capability && $b->has_capability) {
        return unless $a->capability eq $b->capability;
    }

    if (defined $a->groupname) {
        return unless defined $b->groupname;

        return unless $a->groupname eq $b->groupname;
    } else {
        return if defined $b->groupname;
    }

    return 1;
}

=head2 TO_JSON

Convenience method for serializer infrastructure.

=cut

sub TO_JSON {
    my $self = shift;

    my $entity_id = $self->entity->entity_id;
    my $entity_type = $self->entity->entity_type;

    if ($entity_type eq 'group') {
        $entity_id = sprintf('%s|0', $entity_id);
        $entity_type = 'position';
    }

    if ($entity_type eq 'role') {
        $entity_id = sprintf('0|%s', $entity_id);
        $entity_type = 'position';
    }

    return {
        entity_type => $entity_type,
        entity_id   => $entity_id,
        groupname   => $self->groupname,
        capability  => $self->capability
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

