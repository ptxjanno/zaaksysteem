package Zaaksysteem::Object::Query::Expression::MemberRelation;

use Moose;

use Moose::Util::TypeConstraints qw[role_type];
use BTTW::Tools;

with 'Zaaksysteem::Object::Query::Expression';

=head1 NAME

Zaaksysteem::Object::Query::Expression::MemberRelation - Abstracts the
'is member of' logic of IN conditionals.

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 expression

The expression for which membership in L</set> is tested.

=cut

has expression => (
    is => 'rw',
    isa => role_type('Zaaksysteem::Object::Query::Expression'),
    required => 1
);

=head2 set

The set for which membership of L</expression> is tested.

=cut

has set => (
    is => 'rw',
    isa => role_type('Zaaksysteem::Object::Query::Expression'),
    required => 1
);

=head1 METHODS

=head2 stringify

Implements logic required by
L<Zaaksysteem::Object::Query::Expression/stringify>.

    # "my_field is member of [ text('abc'), text('def'), ... ]
    qb_in('my_field', [qw[abc, def]])->stringify

=cut

sub stringify {
    my $self = shift;

    return sprintf(
        '%s is member of %s',
        $self->expression->stringify,
        $self->set->stringify
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
