package Zaaksysteem::Object::Query::Expression::Disjunction;

use Moose;

use Moose::Util::TypeConstraints qw[find_type_constraint role_type];
use BTTW::Tools;

with 'Zaaksysteem::Object::Query::Expression';

=head1 NAME

Zaaksysteem::Object::Query::Expression::Disjunction - Abstracts logical OR
queries.

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 expressions

Collection of L<Zaaksysteem::Object::Query::Expression> instances to check.

=cut

has expressions => (
    is => 'rw',
    isa => find_type_constraint('ArrayRef')->parameterize(
        role_type('Zaaksysteem::Object::Query::Expression')
    ),
    traits => [qw[Array]],
    required => 1,
    handles => {
        all_expressions => 'elements'
    }
);

=head1 METHODS

=head2 stringify

Implements logic required by
L<Zaaksysteem::Object::Query::Expression/stringify>.

    # "field:my_field equal to 'abc' or field:other_field is member of [ text(1), text(2), text(3) ]"
    qb_or(qb_eq('my_field', 'abc'), qb_in('other_field', [qw[1 2 3]))

=cut

sub stringify {
    my $self = shift;

    return join ' or ', map { $_->stringify } $self->all_expressions;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
