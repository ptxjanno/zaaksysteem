package Zaaksysteem::Geo::BAG::Connection;
use Moose::Role;

=head1 NAME

Zaaksysteem::Geo::BAG::Connection - Role for BAG searching connections

=head1 REQUIRED METHODS

=head2 search

Execute a (sort of full-text) query for BAG objects, of a specified type.

=head2 get_exact

Perform a full-text search for a single BAG object.

=head2 find_nearest

Find the BAG objects nearest to the specified latitude and longitude.

=head2 get

Retrieve a single BAG object by its identifier.

=cut

requires 'search', 'get_exact', 'find_nearest', 'get';

=head2 priority_gemeentes

List of municipality names that should get priority in search results.

Used to prevent a "Kerkstraat" on the other side of the country from
coming up high in the search results.

=cut

has priority_gemeentes => (
    is       => 'ro',
    isa      => 'ArrayRef[Str]',
    required => 0,
    default  => sub { [] },
);

=head2 priority_only

Search B<only> in the priority_gemeentes, instead of using priority_gemeentes
to increase search score (and having "local" results be at the top).

=cut

has priority_only => (
    is       => 'ro',
    isa      => 'Bool',
    required => 0,
    default  => 0,
);

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
