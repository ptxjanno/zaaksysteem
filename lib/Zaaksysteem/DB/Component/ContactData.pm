package Zaaksysteem::DB::Component::ContactData;

use Moose;
use namespace::autoclean;

extends 'DBIx::Class';

=head1 NAME

Zaaksysteem::DB::Component::ContactData - Row instance behaviors for
C<contact_data>.

=head1 DESCRIPTION

This L<DBIx::Class> component augments the base class for C<contact_data> rows
and adds some useful shorthand/convenience methods.

=cut

use BTTW::Tools;
use Zaaksysteem::Constants qw[
    LOGGING_COMPONENT_BETROKKENE
];

=head1 METHODS

=head2 TO_JSON

Implements serializable 'role' for L<JSON> processing.

Returns a hashref representing the instance on a higher level than row/column
data.

=cut

sub TO_JSON {
    my $self = shift;

    my $data = {
        id => $self->id,
        email_addresses => [
            $self->email
        ],
        #created => $self->created->datetime,
        #last_modified => $self->last_modified->datetime,
    };

    my @phonenumbers = ();

    for my $key ('mobiel', 'telefoonnummer') {
        if($self->$key) {
            push(@phonenumbers, {
                type => $key eq 'mobiel' ? 'mobile' : 'landline',
                number => $self->$key
            });
        }
    }

    $data->{ phonenumbers } = [ @phonenumbers ];
    $data->{ identifier } = $self->identifier;

    if($self->natural_person) {
        $data->{ natural_person } = $self->natural_person;
    }

    if($self->non_natural_person) {
        $data->{ non_natural_person } = $self->non_natural_person;
    }

    return $data;
}

=head2 identifier

Returns an 'old betrokkene identifier'-style string used for the subject
datawarehousing.

    'betrokkene-natuurlijk_persoon-123'
    'betrokkene-bedrijf-987'

=cut

sub identifier {
    my $self = shift;

    if($self->natural_person) {
        return sprintf('betrokkene-natuurlijk_persoon-%d', $self->gegevens_magazijn_id);
    }

    if($self->non_natural_person) {
        return sprintf('betrokkene-bedrijf-%d', $self->gegevens_magazijn_id);
    }

    return;
}

=head2 natural_person

Returns the L<Zaaksysteem::DB::Component::NatuurlijkPersoon> associated with
the contact data if L</betrokkene_type> is C<1> (natuurlijk_persoon).

=cut

sub natural_person {
    my $self = shift;

    if($self->betrokkene_type == 1) {
        return $self->result_source->schema->resultset('NatuurlijkPersoon')->find($self->gegevens_magazijn_id);
    }

    return;
}

=head2 bedrijf

Returns the L<Zaaksysteem::DB::Component::Bedrijf> associated with the contact
data if L</betrokkene_type> is C<2> (bedrijf).

=cut

sub non_natural_person {
    my $self = shift;

    if($self->betrokkene_type == 2) {
        return $self->result_source->schema->resultset('Bedrijf')->find($self->gegevens_magazijn_id);
    }

    return;
}

=head2 update

Overrides L<DBIx::Class::Row/update> by triggering
L<Zaaksysteem::DB::Component::NatuurlijkPersoon/update> or
L<Zaaksysteem::DB::Component::Bedrijf/update> after the original update method
has executed.

=cut

sub update {
    my $self = shift;

    my $result = $self->next::method(@_);
    $self->update_betrokkene;
    return $result;
}

=head2 insert

Overrides L<DBIx::Class::Row/insert> by triggering L</update_betrokkene> after
the original methods has executed.

=cut

sub insert {
    my $self = shift;

    my $result = $self->next::method(@_);
    $self->update_betrokkene;
    return $result;
}

=head2 update_betrokkene

Calls L<Zaaksysteem::DB::Component::NatuurlijkPersoon/update> or
L<Zaaksysteem::DB::Component::Bedrijf/update>, depending on our
L</betrokkene_type>.

=cut

sub update_betrokkene {
    my $self = shift;

    if (my $betrokkene = $self->natural_person || $self->non_natural_person) {
        $betrokkene->update;
    }
}

=head2 exec_and_log_update

Calls L</update> when the row has dirty columns (changed since fetch), and
inserts a C<subject/update_contact_data> event if succesful.

Returns the created event or C<undef>.

=cut

sig exec_and_log_update => 'CodeRef';

sub exec_and_log_update {
    my $self = shift;
    my $logger = shift;

    # Gets fieldnames of dirty columns
    my @fields = $self->is_changed;

    return unless scalar @fields;

    return $self->result_source->schema->txn_do(sub {
        $self->update;

        return $logger->('subject/update_contact_data', {
            component => LOGGING_COMPONENT_BETROKKENE,
            component_id => $self->get_column('gegevens_magazijn_id'),
            created_for => $self->identifier,
            data => {
                subject_id => $self->identifier,
                fields => \@fields
            }
        });
    });
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, 2018 Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
