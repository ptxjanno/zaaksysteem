package Zaaksysteem::DB::Component::Logging::Attribute::Remove;

use Moose::Role;

has attribute => (
    is => 'ro',
    lazy => 1,
    builder => '_add_magic_attributes_builder'
);

sub _python_subject {
    my $self = shift;
    if ($self->data->{ attribute_name }) {
            return sprintf(
                'Kenmerk "%s" (magic string "%s") verwijderd: %s',
                $self->data->{ attribute_name },
                $self->data->{ magic_string },
                $self->data->{ reason }
            );
    }
    return;
}

sub onderwerp {
    my $self = shift;

    if (my $subject = $self->_python_subject) {
        return $subject;
    }

    return $self->get_column('onderwerp') unless $self->attribute;

    sprintf(
        'Kenmerk "%s" verwijderd: %s',
        $self->attribute->naam,
        $self->data->{ reason }
    );
}

sub _add_magic_attributes {
    return;
}

sub _add_magic_attributes_builder {
    my $self = shift;
    return $self->result_source->schema->resultset('BibliotheekKenmerken')->find($self->data->{ attribute_id });
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2019, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 onderwerp

TODO: Fix the POD

=cut

