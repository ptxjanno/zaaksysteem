package Zaaksysteem::DB::Component::Logging::Object::Create;
use Moose::Role;

use JSON;

=head1 NAME

Zaaksysteem::DB::Component::Logging::Object::Create - Event subject
for object creation mutations.

=head1 METHODS

=head2 onderwerp

Overrides L<Zaaksysteem::Schema::Logging/onderwerp> and provides a
contextualized summary of the event.

=cut

sub onderwerp {
    my $self = shift;

    return sprintf(
        '%s "%s" aangemaakt',
        $self->object_description,
        $self->data->{ object_label },
    );
}

around TO_JSON => sub {
    my $orig = shift;
    my $self = shift;

    my $data = $self->$orig(@_);

    my @value_rows = map {
        sprintf(
            "%s: %s",
            $_->{field_label},
            $_->{new_value} // "geen waarde",
        ),
    } @{ $self->data->{changes} };

    $data->{ content } = join("\n", @value_rows);
    $data->{ expanded } = JSON::false;

    return $data;
};
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
