package Zaaksysteem::DB::Component::Logging::Case::Update::Milestone;

use Moose::Role;

has phase => ( is => 'ro', lazy => 1, default => sub {
    my $self = shift;

    $self->result_source->schema->resultset('ZaaktypeStatus')->find($self->data->{ phase_id });
});

sub onderwerp {
    my $self = shift;

    if($self->data->{ admin }) {
        return sprintf('Fase aangepast naar "%s"', $self->phase->fase);
    } else {
        return sprintf('%s afgerond', $self->phase->fase);
    }
}

sub event_category { 'case-mutation'; }

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 event_category

TODO: Fix the POD

=cut

=head2 onderwerp

TODO: Fix the POD

=cut

