package Zaaksysteem::DB::Component::Logging::Case::Subject::Update;
use Moose::Role;

=head2 onderwerp

Builds a human-readable subject for this log line.

=cut

sub onderwerp {
    my $self = shift;

    return sprintf('Betrokkene "%s" van zaak %s bijgewerkt',
        $self->data->{ subject_name },
        $self->data->{ case_id },
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
