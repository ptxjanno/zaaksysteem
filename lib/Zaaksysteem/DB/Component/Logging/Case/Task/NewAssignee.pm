package Zaaksysteem::DB::Component::Logging::Case::Task::NewAssignee;

use Moose::Role;

sub onderwerp {
    my $self = shift;
    return $self->get_column('onderwerp');
}

sub event_category { 'contactmoment' };


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
