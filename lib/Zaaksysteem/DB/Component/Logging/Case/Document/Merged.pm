package Zaaksysteem::DB::Component::Logging::Case::Document::Merged;

use Moose::Role;
use HTML::Entities qw(encode_entities);

=head2 onderwerp

Display the subject line of the event

=cut

sub onderwerp {
    my $self = shift;

    my @names = map { "'$_'" } @{$self->data->{ names }};
    my $last = pop @names;
    my $names = join(", ", @names);
    $names = join(' en ', $names, $last);

    return sprintf(
        "Document '%s' samengevoegd vanuit %s",
        encode_entities($self->data->{filename}),
        encode_entities($names),
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
