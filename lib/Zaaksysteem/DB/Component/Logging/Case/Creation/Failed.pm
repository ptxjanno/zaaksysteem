package Zaaksysteem::DB::Component::Logging::Case::Creation::Failed;
use Moose::Role;

sub onderwerp {
    my $self = shift;

    my $caseid = $self->data->{zaak_id};

    return sprintf "Fout bij aanmaken van zaak (%s): %s", $caseid // '<geen zaakid>', $self->data->{request_id};
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
