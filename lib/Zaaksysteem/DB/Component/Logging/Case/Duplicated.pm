package Zaaksysteem::DB::Component::Logging::Case::Duplicated;

use Moose::Role;

has duplicated_from => ( is => 'ro', lazy => 1, default => sub {
    my $self = shift;

    $self->rs('Zaak')->find($self->data->{ duplicated_from });
});

sub onderwerp {
    my $self = shift;

    sprintf(
        'Zaak gekopie&euml;rd van zaak %d (%s)',
        $self->duplicated_from->id,
        $self->duplicated_from->zaaktype_node_id->titel
    );
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 onderwerp

TODO: Fix the POD

=cut

