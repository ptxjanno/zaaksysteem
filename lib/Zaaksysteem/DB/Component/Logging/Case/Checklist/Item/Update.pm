package Zaaksysteem::DB::Component::Logging::Case::Checklist::Item::Update;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    sprintf(
        'Status van taak "%s" gewijzigd naar: %s',
        $self->data->{ checklist_item_name },
        $self->data->{ checklist_item_value }
    );
}

sub event_category { 'case-mutation'; }

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 event_category

TODO: Fix the POD

=cut

=head2 onderwerp

TODO: Fix the POD

=cut

