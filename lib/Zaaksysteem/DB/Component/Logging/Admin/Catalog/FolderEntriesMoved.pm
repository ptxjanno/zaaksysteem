package Zaaksysteem::DB::Component::Logging::Admin::Catalog::FolderEntriesMoved;
use Moose::Role;

=head2 onderwerp

Generate the main description for this event.

=cut

sub onderwerp {
    my $self = shift;

    return $self->get_column('onderwerp');
}

=head2 event_category

Category this event is in.

=cut

sub event_category { 'system' }

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
