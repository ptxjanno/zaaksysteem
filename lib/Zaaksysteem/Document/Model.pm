package Zaaksysteem::Document::Model;

use Moose;
use namespace::autoclean;

=head1 NAME

Zaaksysteem::Document::Model - A Zaaksysteem document model

=head1 SYNOPSIS

    use Zaaksysteem::Document::Model;
    my $model = Zaaksysteem::Document::Model->new(
        rs_file => $schema->resultset('File'),
        rs_filestore => $schema->resultset('Filestore'),
    );

    my $id          = $model->generate_serial_number;
    my $file        = $model->get_by_serial_number($id);
    my $new_version = $model->create_new_version(
        $file,
        path     => $path,
        filename => $filename
    );
    my $file = $model->update_metadata($file, %metadata);

=head1 DESCRIPTION

This model tries to abstract the File/Filestore logic in the current
Zaaksysteem implemenation into a document oriented approach.

Currently the document is just a
L<File|Zaaksysteem::Backend::File::Component> but this could change in
the (near) future to a Zaaksysteem Document. There is already a
L<document object|Zaaksysteem::Object::Types::Document> for API/v1
endpoints. Consumers of this model should not need to change their
logic after such a change.

=cut

use BTTW::Tools;
use Zaaksysteem::Object::Types::Serial;

with 'MooseX::Log::Log4perl';

=head1 ATTRIBUTES

=head2 rs_file

A L<Zaaksysteem::Backend::File::ResultSet> object.

=cut

has rs_file => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Backend::File::ResultSet',
    required => 1,
);

=head2 rs_filestore

A L<Zaaksysteem::Backend::Filestore::ResultSet> object.

=cut

has rs_filestore => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Backend::Filestore::ResultSet',
    required => 1,
);

=head2 storage

A L<DBIx::Class::Storage> object

=cut

has storage => (
    is       => 'ro',
    isa      => 'DBIx::Class::Storage',
    required => 1,
);

=head2 subject

A L<Zaaksysteem::Schema::Subject> object.

=cut

has subject => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Schema::Subject',
    required => 0,
);

=head2 generate_serial_number

Generate a serial number for a document. Returns a
L<Zaaksysteem::Object::Types::Serial> object.

=cut

sub generate_serial_number {
    my $self = shift;

    my $id = $self->rs_file->generate_file_id;
    return Zaaksysteem::Object::Types::Serial->new(
        object_class => 'document',
        serial       => $id,
        name         => 'number',
    );
}

=head2 assert_document_serial_number

Assert if the serial number is does not exceed the last value given to a file

=cut

sig assert_document_serial_number => 'Int';

sub assert_document_serial_number {
    my ($self, $id) = @_;

    my $val = $self->storage->dbh_do(
        sub {
            my ($storage, $dbh, @args) = @_;
            my $sth = $dbh->prepare('select last_value from file_id_seq');
            $sth->execute();
            my @result = $sth->fetchrow_array;
            return $result[0];
        }
    );

    if ($id > $val) {
        throw("document/serial/too_high",
            "Unable to find document with $id, serial error");
    }

    return 1;
}

=head2 get_by_serial_number

Get a document by serial number

=cut

sig get_by_serial_number => 'Int';

sub get_by_serial_number {
    my ($self, $id) = @_;
    my $file = $self->_get_by_serial_number($id);

    return $file unless $file;
    return $file unless $file->get_column('case_id');

    my $case = $file->case_id;
    try {
        $case->assert_read_permission($self->subject);
    }
    catch {
        $self->log->debug($_);
        throw("document/permissions/read",
            "Unauthorized to view file due to case restrictions");
    };
    return $file;
}

=head2 list

Get a complete listing of all the files in the database

=cut

sub list {
    my $self = shift;
    return $self->rs_file->search_rs(
        {
            date_deleted => undef,
        },
        {
            order_by => { -desc => ['me.id', 'root_file_id'], },
            prefetch => [qw(filestore_id metadata_id)],
        },
    );
}


=head2 list_active

List all the active versions of files.

=cut

sub list_active {
    my $self = shift;
    my $list = $self->list;
    return $list->search_rs(
        {
            active_version => 1,
        }
    );
}

=head2 create_new_version

Create a new version of a file

    my $file = $model->create_new_version(
        $old_file,
        path     => '/tmp/foo',
        filename => 'Myfile.txt',
    );

=cut

define_profile create_new_version => (
    required => {
        path     => 'Str',
        filename => 'Str',
    },
    optional => {
        case_id => 'Int',
        id      => 'Int',
    },
);

sig assert_mutation => 'Zaaksysteem::Backend::File::Component';

sub assert_mutation {
    my ($self, $file) = @_;

    throw(
        'document/create/version/case/missing',
        "Unable to update the document to a newer version, not assigned to a case",
    ) unless $file->get_column('case_id');

    my $case = $file->case_id;
    $case->assert_write_permission($self->subject);
    return 1;
}

sig create_new_version => '?Zaaksysteem::Backend::File::Component => Zaaksysteem::Backend::File::Component';

sub create_new_version {
    my ($self, $file, %opts) = @_;
    my $opts = assert_profile(\%opts)->valid;

    if ($file) {
        $self->assert_mutation($file);

        my $new_file = $file->update_file({
            subject       => $self->subject->as_object,
            new_file_path => $opts->{path},
            original_name => $opts->{filename},
            name          => $opts->{filename},
            is_restore    => 0,
        });

        $new_file->update_properties(
            {
                subject      => $self->subject,
                accepted     => 1,
                root_file_id => $opts->{id},
            }
        );
        return $new_file;
    }
    else {
        my %create_args = (
            name      => $opts->{filename},
            file_path => $opts->{path},
            db_params => {
                $opts->{id} ? (id => $opts->{id}) : (),
                $opts->{case_id} ? (case_id => $opts->{case_id}) : (),
                created_by => $self->subject->old_subject_identifier,
            },
        );

        my $file = $self->rs_file->file_create( \%create_args );

        $file->update_properties({
            subject  => $self->subject->as_object,
            accepted => 1,
        }) if $opts->{case_id};

        return $file;
    }
}

=head2 update_metadata

Update the metadata of a file

    $self->update_metadata(
        appearance
        description
        origin
        origin_date
        pronom_format
        structure
        trust_level
        category
    );

=cut

# TODO: Figure out a way to define a profile for DFV and allow undef
# values. Perhaps make a ZS::Type for things like this?

sig update_metadata => 'Zaaksysteem::Backend::File::Component => Zaaksysteem::Backend::File::Component';

my @_metadata_keys = qw(
    appearance
    description
    origin
    origin_date
    pronom_format
    structure
    trust_level
    creation_date
);

sub update_metadata {
    my ($self, $document, %metadata) = @_;

    my $status = delete $metadata{status};

    if ($status) {
        $document->update_properties(
            {
                subject => $self->subject,
                defined $status ? (document_status => $status) : (),
            }
        );
    }

    if (keys %metadata) {
        my %md;
        foreach (@_metadata_keys) {
            $md{$_} = $metadata{$_} if exists $metadata{$_};
        }

        if (exists $metadata{category}) {
            $md{document_category} = $metadata{category};
        }

        if (keys %md) {
            $document->update_metadata(\%md);
            $document->metadata->discard_changes;
        }
    }

    $document->discard_changes;
    return $document;
}

=head2 update_case_id

Assign the file to a case. If the file is already assigned to a case, we
die.

=cut

sig update_case_id => 'Zaaksysteem::Backend::File::Component, Int => Zaaksysteem::Backend::File::Component';

sub update_case_id {
    my ($self, $document, $id)  = @_;

    my $case = $document->get_column('case_id');
    if ($case && $id != $case) {
        throw("document/case_id/exists", "Unable to update document, it is already assigned to a case");
    }
    elsif ($case && $id == $case) {
        return $document;
    }

    $document->update({case_id => $id});
    return $document->discard_changes;
}

=head2 copy_to_case

Copy a directory including files to a another case.

In case a directory is supplied all file ID's that are given are retrieved from
the directory in the source case. Otherwise the file is taken from the case and
inserted into the root of the file. In case a parent directory is given, the
directory is placed into this parent in the destination case.

=cut

define_profile copy_to_case => (
    required => {
        source      => 'Zaaksysteem::Zaken::ComponentZaak',
        destination => 'Zaaksysteem::Zaken::ComponentZaak',
        subject     => 'Defined',
    },
    optional => {
        parent_directory => 'Zaaksysteem::Backend::Directory::Component',
        directory        => 'Zaaksysteem::Backend::Directory::Component',
        file_ids         => 'Int',
    }
);

sub _assert_not_same_case {
    my ($src, $dst) = @_;

    return if $src->id ne $dst->id;

    throw(
        "document/model/case/equals",
        "Case is the same, unable to comply"
    );
}

sub copy_to_case {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    _assert_not_same_case($opts->{source}, $opts->{destination});

    my $dir;
    if ($opts->{directory}) {
        $dir = $opts->{directory}->copy_to_case(
            case    => $opts->{destination},
            $opts->{parent_directory}
                ? (parent => $opts->{parent_directory})
                : (),
        );
    }

    my $files = $opts->{source}->search_active_files($opts->{file_ids})
        if $opts->{file_ids};

    _copy_to_case($opts->{destination}, $opts->{subject}, $dir, $files)
        if $files;

    return $dir if $dir;
    return;
}

sub _copy_to_case {
    my ($case, $subject, $directory, $files) = @_;

    return unless $files;

    my $case_id = $case->id;
    while (my $file = $files->next) {

        # Don't break if the file already exists as this is a bulk action.
        # We can't inform the user, but soit.
        next if $file->is_present_in_case($case_id);

        $file->copy_to_case(
            case    => $case,
            subject => $subject,
            $directory ? (directory => $directory) : (),
        );
    }

    return;
}

sub _get_documents_by_uuid {
    my ($self, $case, $uuids) = @_;

    return $case->search_active_files()
        ->search_rs({ uuid => { -in => $uuids } },
        { prefetch => 'metadata_id' });
}

sub _fix_uuid_order {
    my @uuids = @_;
    my @order;
    my $count = 1;
    foreach (@uuids) {
        push(@order, "when '$_' then $count");
        $count++;
    }
    return join(" ", 'case uuid', @order, 'end');
}

sub _get_files_for_merge {
    my $resultset = shift;

    my ($level, @files, @names);
    my $confidential = 0;

    while (my $file = $resultset->next) {

        my $pdf = $file->get_preview_pdf;

        throw("document/merge/pdf/unavailable",
            "Unable to merge documents, PDF does not exist yet!") unless $pdf;


        if (!$confidential && $file->confidential) {
            $confidential = 1;
            $level = $file->metadata_id->trust_level;
        }
        elsif (($level // '') eq 'Zeer geheim') {
            # Do nothing
        }
        elsif ($confidential && $file->confidential) {
            my $cur = $file->metadata_id->trust_level;
            if ($cur eq 'Zeer geheim') {
                $level = $cur;
            }
            elsif ($cur eq 'Geheim') {
                $level = $cur;
            }
            elsif ($level ne 'Geheim' && $cur eq 'Confidentieel') {
                $level = $cur;
            }
            elsif ($level ne 'Geheim' or $level ne 'Confidentieel' && $cur eq 'Vertrouwelijk') {
                $level = $cur;
            }
        }

        push(@files, $pdf->get_path);
        push(@names, $file->filename);
    }
    return {
        files        => \@files,
        confidential => $confidential,
        trust_level  => $level,
        names        => \@names,
    };
}

=head2 merge_documents

Merge documents into one PDF file

=cut

sub merge_documents {
    my ($self, %args) = @_;

    my $fs = $self->_get_documents_by_uuid($args{case}, $args{file_uuids});
    my $order = _fix_uuid_order(@{$args{file_uuids}});
    $fs = $fs->search_rs(undef, { order_by => \$order });

    my $data = _get_files_for_merge($fs);
    my @files = @{$data->{files}};
    return 1 unless @files;

    my $fh = _merge_to_one_pdf(@files);
    my $filename = join('.', $args{filename}, 'pdf');

    my %create_args = (
        name      => $filename,
        file_path => $fh->filename,
        db_params => {
            case_id    => $args{case}->id,
            created_by => $args{subject}->old_subject_identifier,
        },
        disable_logging => 1,
    );

    my $file = $self->rs_file->file_create( \%create_args );
    if ($data->{confidential}) {
        $file->update_metadata({ trust_level => $data->{trust_level} })
    }

    $file->trigger('merged',
        { filename => $filename, names => $data->{names} });

    return 1;
}

sub _merge_to_one_pdf {
    my @files = @_;

    my $fh = File::Temp->new(UNLINK => 1, SUFFIX => '.pdf');
    close($fh);

    my @pdftk = (
        '/usr/bin/pdftk',
        @files,
        'cat',
        'output',
        $fh->filename,
    );

    system(@pdftk);
    if ($?) {
        throw("document/merge/system",
            "Unable to merge documents, unknown error!");
    }
    return $fh;
}

sub _get_by_serial_number {
    my ($self, $id) = @_;

    my $rs = $self->list->search_rs({ 'me.id' => $id });

    my $file = $rs->first;
    if (!$file) {
        if ($self->assert_document_serial_number($id)) {
            return undef;
        }
        throw('file/get_by_serial/id', "Unable to retreive file by id $id");
    }
    return $file->get_last_version;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
