package Zaaksysteem::Zaken;

use Moose;
use namespace::autoclean;

use Params::Profile;
use Zaaksysteem::Constants;

use BTTW::Tools;

use Scalar::Util qw(blessed);

### Roles
use constant ZAAK_TABEL => 'Zaak';



has [qw/config prod log dbic z_betrokkene rt/] => (
    'is'    => 'rw',
);



sub find {
    my $self    = shift;

    $self->dbic->resultset(ZAAK_TABEL)->find(@_);
}

sub search {
    my $self    = shift;

    $self->dbic->resultset(ZAAK_TABEL)->search_extended(@_);
}

sub create {
    my $self    = shift;

    $self->dbic->resultset(ZAAK_TABEL)->create_zaak(@_);
}

#
# this provides the logic for the filter boxes on the top of any Zaken overview.
# it needs an existing resultset, and narrows that down based on user input
#
Params::Profile->register_profile(
    method  => 'filter',
    profile => {
        required        => [ qw/
            resultset
        /],
        optional        => [ qw/
            dropdown
            textfilter
        /],
        constraint_methods    => {
            resultset    => sub {
                my ($profile, $value) = @_;
                return $value->isa('Zaaksysteem::Zaken::ResultSetZaak')
            },
            dropdown         => qr/^\w*$/,
#            textfilter       => qr/^\w*$/,
        },
    }
);

sub filter {
    my ($self, $options) = @_;

    my $dv = assert_profile($options);

    my $additional_filtering = [];

    my $filter = $options->{'dropdown'} || '';
    my $additional_filter;
    if($filter) {
        if($filter eq 'urgent') {
            $additional_filter->{'urgentie'} = 'high';
        } else {
            $additional_filter->{'me.status'} = $filter;
        }
        push @$additional_filtering, $additional_filter;
    }

    my $textfilter_where;
    my $textfilter = $options->{'textfilter'};
    if($textfilter) {
        my $textfilters = [];

        push @$textfilters, { 'me.onderwerp' => { 'ilike' => '%'. $textfilter. '%' }};

# todo match on zaaktype_node_id.titel
# todo string match on ID
        if($textfilter =~ m|^\d+$|) {
               push @$textfilters,\[ 'TEXT(me.id) LIKE ?', [ plain_value => '%' . $textfilter . '%' ] ],
        }

        my $betrokkenen = $self->dbic->resultset('ZaakBetrokkenen')->search({
            'naam' => {'ilike' => '%'. $textfilter. '%' },
        });

        my $zaaktypen = $self->dbic->resultset('ZaaktypeNode')->search({
            'titel' => {'ilike' => '%'. $textfilter. '%' },
        });

        push @$textfilters, { 'zaaktype_node_id' => { -in => $zaaktypen->get_column('id')->as_query }};

        push @$textfilters, { 'aanvrager' => { -in => $betrokkenen->get_column('id')->as_query }};

        push @$additional_filtering, ['-or' => $textfilters ];
    }

    if(@$additional_filtering) {
        $self->log->debug(sprintf('Added %d additional filters', scalar @$additional_filtering));

        return $options->{'resultset'}->search('-and' => $additional_filtering);
    }
    return $options->{'resultset'};
}





#
# central query to retrieve all openstaande_zaken
#
Params::Profile->register_profile(
    method  => 'openstaande_zaken',
    profile => {
        required        => [ qw/
            page
            rows
            uidnumber
        /],
        optional        => [ qw/
            sort_direction
            sort_field
        /],
        defaults        => {
            sort_direction  => 'DESC',
            sort_field      => 'me.id',
        },
        constraint_methods    => {
            page    => qr/^\d+$/,
            rows    => qr/^\d+$/,
        },
    }
);

sub openstaande_zaken {
    my $self = shift;
    my $options = assert_profile(shift)->valid;

    my $where = { 'me.status' => 'open' };

    $where->{'me.behandelaar_gm_id'} = $options->{'uidnumber'};
    $where->{'me.deleted'} = undef;

    my $extra_params = {
        page    => $options->{'page'},
        rows    => $options->{'rows'},
    };

    $extra_params->{order_by} = {
        '-' . ($options->{sort_direction} || 'asc') => $options->{sort_field}
    } if $options->{sort_field};

    return $self->dbic->resultset('Zaak')->search_extended(
        $where,
        $extra_params
    );
}

#
# central query to retrieve all openstaande_zaken
#
Params::Profile->register_profile(
    method  => 'adres_zaken',
    profile => {
        required        => [ qw/
            page
            rows
            nummeraanduiding_id
        /],
        optional        => [ qw/
            sort_direction
            sort_field
            status
        /],
        constraint_methods    => {
            page                => qr/^\d+$/,
            rows                => qr/^\d+$/,
            nummeraanduiding_id => qr/^\d+$/,
        },
    }
);

sub adres_zaken {
    my ($self, $options) = @_;
    my $where = {};

    my $dv = assert_profile($options);

    #my $where = { 'me.status' => 'open' };
    $where->{'me.status'} = $options->{status} if $options->{status};

    my $locaties = $self->dbic->resultset('ZaakBag')->search({
        'me.bag_nummeraanduiding_id::bigint' => int($options->{nummeraanduiding_id}),
    });

    $where->{'locatie_zaak'} = {-in => $locaties->get_column('id')->as_query};
    $where->{'me.deleted'} = undef;

    #$self->log->debug('adres zaken: ' . Data::Dumper::Dumper($where));

    my $extra_params = {
        page    => $options->{'page'},
        rows    => $options->{'rows'},
    };

    $extra_params->{order_by} = {
        '-' . ($options->{sort_direction} || 'asc') => $options->{sort_field}
    } if $options->{sort_field};

    return $self->dbic->resultset('Zaak')->search_extended(
        $where,
        $extra_params
    );
}

#
# central query to retrieve all openstaande_zaken
#
Params::Profile->register_profile(
    method  => 'zaken_pip',
    profile => {
        required        => [ qw/
            betrokkene_type
            gegevens_magazijn_id
            type_zaken
        /],
        optional        => [ qw/
            page
            rows
            sort_direction
            sort_field
            show_all_betrokkene_cases
        /],
        constraint_methods    => {
            page    => qr/^\d+$/,
            rows    => qr/^\d+$/,
        },
    }
);

sub zaken_pip {
    my ($self, $options)    = @_;
    my $where               = {};

    my $db = assert_profile($options);

    if ($options->{type_zaken} eq 'open') {
        $where->{'me.status'} = [ 'open','new', 'stalled' ];
    }
    elsif (ref $options->{type_zaken} eq 'ARRAY') {
        $where->{'me.status'} =  $options->{type_zaken};
    }
    else {
        $where->{'me.status'} =  [ $options->{type_zaken} ];
    }

    my $betrokkene_where = {
        betrokkene_type      => $options->{betrokkene_type},
        gegevens_magazijn_id => $options->{gegevens_magazijn_id},
        deleted              => undef,
    };

    if ($options->{betrokkene_type} eq 'medewerker') {
        $betrokkene_where->{'-or'} = [
            { authorisation => { '!=' => 'none' } },
            { authorisation => undef }
        ];
    }

    my $betrokkenen = $self->dbic->resultset('ZaakBetrokkenen')->search(
        $betrokkene_where
    );

    my $gemachtigden;

    if ($options->{all_cases}) {
        $options->{show_all_betrokkene_cases} = 1;
        $gemachtigden = $betrokkenen->search({ pip_authorized => [1, 0] });
    }
    elsif ($options->{as_gemachtigde}//1) {
        $gemachtigden = $betrokkenen->search({
            pip_authorized => 1
        });
    }
    else {
        $options->{as_betrokkene} = 1;
        $options->{as_aanvrager}  = 0;
        $options->{show_all_betrokkene_cases} //= 1;

        $gemachtigden = $betrokkenen->search({ pip_authorized => 0 });
    }

    if (defined($options->{nummeraanduiding})
        && ref($options->{nummeraanduiding}))
    {
        my $locaties = $self->dbic->resultset('ZaakBag')->search(
            {
                'bag_nummeraanduiding_id' =>
                    $options->{nummeraanduiding}->identificatie,
            }
        );
        $where->{'locatie_zaak'}
            = { -in => $locaties->get_column('id')->as_query };
    }

    if ($options->{as_betrokkene}) {
        $where->{'me.id'}
            = { -in => $gemachtigden->get_column('zaak_id')->as_query };
        $where->{'aanvrager'}
            = { -not_in => $betrokkenen->get_column('id')->as_query };
    }
    elsif ($options->{as_aanvrager}) {
        $where->{'aanvrager'}
            = { -in => $betrokkenen->get_column('id')->as_query };
    }
    else {
        $where->{'me.id'}
            = { -in => $betrokkenen->get_column('zaak_id')->as_query };
    }

    $where->{'me.deleted'} = undef;

    # ZS-2208: this subroutine, zaken_pip, is used on the pip, but unfortunatly also
    # on the betrokkene page where we want to see all cases of this betrokkene.
    if (!$options->{show_all_betrokkene_cases}) {
        $where->{ -or } = [
            'zaaktype_node_id.prevent_pip' => undef,
            'zaaktype_node_id.prevent_pip' => 0
        ];
    }

    my $extra_params = {
        join      => 'zaaktype_node_id',
        page      => $options->{'page'},
        rows      => $options->{'rows'},
        order_by  => { '-desc'   => 'me.id' }
    };

    if($options->{ sort_field }) {
        my $direction = $options->{ sort_direction } || 'asc';

        $extra_params->{ order_by } = {
            "-$direction" => $options->{ sort_field }
        };
    }

    return $self->dbic->resultset('Zaak')->search_extended($where, $extra_params);
}

=head2 resume_temporary_stalled_cases

cases can be stalled with a term, after this term they need to
be resumed. this function looks for cases that are due for resumption

=cut

sub resume_temporarily_stalled_cases {
    my ($self, $queue) = @_;

    my $dt = $self->dbic->schema->format_datetime_object(DateTime->today->add(days => 1));
    my $rs = $self->dbic->resultset('Zaak')->search_extended({
        status => 'stalled',
        stalled_until => { '<' => $dt },
    });

    while (my $case = $rs->next) {
        $case->wijzig_status({
            status => 'open',
            reason => 'Zaak automatisch hervat na ingestelde termijn'
        });

        $queue->emit_case_event({
            case => $case,
            event_name => 'CaseUnstalled',
            changes => [
                { key => 'status', old_value => 'stalled', new_value => 'open' }
            ]
        });
    }
}

sub update_phase_deadline_for_cases {
    my ($self, $queue) = @_;

    my $item = $queue->create_item({
        label => "Update phase deadlines",
        type => 'case_update_deadline',
    });
    return;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, 2019 Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 ZAAK_TABEL

TODO: Fix the POD

=cut

=head2 adres_zaken

TODO: Fix the POD

=cut

=head2 create

TODO: Fix the POD

=cut

=head2 filter

TODO: Fix the POD

=cut

=head2 find

TODO: Fix the POD

=cut

=head2 openstaande_zaken

TODO: Fix the POD

=cut

=head2 resume_temporarily_stalled_cases

TODO: Fix the POD

=cut

=head2 search

TODO: Fix the POD

=cut

=head2 zaken_pip

TODO: Fix the POD

=cut

