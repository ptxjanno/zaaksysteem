package Zaaksysteem::Betrokkene::Object::Medewerker;

use Moose;

use BTTW::Tools;
use Zaaksysteem::Constants;

use constant BOBJECT    => 'Zaaksysteem::Betrokkene::Object';
use constant BRSOBJECT  => 'Zaaksysteem::Betrokkene::ResultSet';

extends BOBJECT;

my $CLONE_MAP = {
    'voorletters'    => 'initials',
    'voornamen'      => 'givenname',
    'email'          => 'mail',
    'geslachtsnaam'  => 'sn',
    'display_name'   => 'displayname',
    'telefoonnummer' => 'telephonenumber',
    'username'       => 'username',
    'title'          => 'title',
    'uuid'           => 'uuid',
};
my $UNIFORM = {
    'voorvoegsel'       => 0,
    'straatnaam'        => 'straatnaam',
    'huisnummer'        => 'huisnummer',
    'postcode'          => 'postcode',
    'geslachtsaanduiding'          => 0,
    'woonplaats'        => 'plaats',
};

my $SEARCH_MAP = {
    %{ $CLONE_MAP }
};

has 'intern'    => (
    'is'    => 'rw',
);

has 'ldap_rs' => (
    'is'      => 'rw',
    'handles' => {
        'get_assignee_role' => 'get_assignee_role',
        'is_active'         => 'is_active',
    },
);

has 'ldapid'   => (
    'is'    => 'rw',
);

### DUMMY:
has [qw/
    huisletter
    huisnummertoevoeging
    mobiel
/] => (
    'is'   => 'ro',
);

### Convenience method containing some sort of display_name
has 'naam' => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my ($self) = @_;

        return $self->display_name;
    },
);

has 'afdeling' => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my ($self) = @_;

        return $self->org_eenheid->name
            if $self->org_eenheid;
    },
);

has 'org_eenheid' => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my ($self) = @_;

        if ($self->ldap_rs) {
            return $self->ldap_rs->primary_group;
        }
    },
);

sub search {
    my $self                = shift;
    my $dispatch_options    = shift;
    my $opts                = shift;
    my ($searchr)           = @_;

    ### Will search our LDAP directory containing the asked users
    die('M::B::NP->search() only possible call = class based')
        unless !ref($self);

    # Default: only search for active medewerkers.
    $searchr->{active_search}   = 1 unless exists $searchr->{active_search};
    $searchr->{inactive_search} = 0 unless exists $searchr->{inactive_search};

    my $search_query      = {};

    $search_query->{'me.properties'} = { 'ILIKE' => '%' . $searchr->{freetext} . '%' }
        if $searchr->{freetext};

    # my $subjects            = $c->model()
    my $page                = $dispatch_options->{stash}->{paging_total} || 25;

    ### Paging
    my $rowlimit    = $dispatch_options->{stash}->{paging_rows} =
        $dispatch_options->{stash}->{paging_rows} || 40;
    my $rowpage     = $dispatch_options->{stash}->{paging_page} =
        $dispatch_options->{stash}->{paging_page} || 1;
    my $roworder    = $dispatch_options->{stash}->{order} =
        $dispatch_options->{stash}->{order} || 'user_entities.source_identifier';
    my $roworderdir = $dispatch_options->{stash}->{order_direction} =
        $dispatch_options->{stash}->{order_direction} || 'asc';

    if($opts->{rows_per_page}) {
        $rowlimit = $opts->{rows_per_page};
    }

    $search_query->{subject_type} = SUBJECT_TYPE_EMPLOYEE;

    my $search_options = {
        prefetch    => 'user_entities',
        'page'      => $rowpage,
        'rows'      => $rowlimit,
        'order_by'  => { '-' . $roworderdir => $roworder }
    };

    my $resultset;
    if ($searchr->{inactive_search} && !$searchr->{active_search}) {
        $resultset = $dispatch_options->{dbic}->resultset('Subject')->search(
            {
                %{ $search_query },
                'user_entities.date_deleted'       => {
                    '!=' => undef,
                },
            },
            $search_options,
        );
    }
    elsif($searchr->{inactive_search} && $searchr->{active_search}) {
        $resultset = $dispatch_options->{dbic}->resultset('Subject')->search(
            $search_query,
            $search_options,
        );
    }
    else {
        # active_search && !inactive_search (i.e. the default)
        $resultset = $dispatch_options->{dbic}->resultset('Subject')->search_active(
            $search_query,
            $search_options,
        );
    }

    ### Paging info
    $dispatch_options->{stash}->{paging_total}       = $resultset->pager->total_entries;
    $dispatch_options->{stash}->{paging_lastpage}    = $resultset->pager->last_page;

    return BRSOBJECT->new(
        'class'     => __PACKAGE__,
        'dbic_rs'   => $resultset,
        'opts'      => $opts,
        %{ $dispatch_options },
    );
}



Params::Profile->register_profile(
    method  => '_search_specific',
    profile => {
        required => [ qw/searchr/ ],
        optional => [ qw/org_eenheid/ ],
    }
);

sub _search_specific {
    my ($self, $params) = @_;
    my $filter = '';

    my $dv = Params::Profile->check(params => $params);
    die "invalid options for _search_specific" unless $dv->success;

    my $searchr     = $params->{searchr};
    my $org_eenheid = $params->{org_eenheid};

    foreach my $attr (keys %{ $SEARCH_MAP }) {
        # Usernames should be an exact match, all others can be wildcarded after the string.
        if (exists($searchr->{$attr}) &&
            $searchr->{$attr} &&
            $attr eq 'username' ) {
            $filter .= '(' . $SEARCH_MAP->{$attr} . '=' . $searchr->{$attr} . ')';

        }
        elsif (exists($searchr->{$attr}) &&
            $searchr->{$attr}
        ) {
             $filter .= '(' . $SEARCH_MAP->{$attr} . '=' . $searchr->{$attr} . '*)';
        }
    }

    return '(|' . $filter . ')' if $filter;
    return '';
}


Params::Profile->register_profile(
    method  => '_search_freetext',
    profile => {
        required => [ qw/query/ ]
    }
);
sub _search_freetext {
    my ($self, $params) = @_;
    my $filter = '';

    my $dv = Params::Profile->check(params => $params);
    die "invalid options for _search_freetext" unless $dv->success;

    my $query   = $params->{query};

    for my $value (values %{ $SEARCH_MAP }) {
        $filter .= '(' . $value . '=' . $query . '*)';
    }

    return '(|' . $filter . ')' if $filter;
    return '';
}



sub BUILD {
    my ($self) = @_;

    ### Nothing to do if we do not know which way we came in
    return unless ($self->trigger && $self->trigger eq 'get' && $self->id);

    ### It depends on the 'intern' option, weather we retrieve
    ### our data from our our snapshot DB, or GM. When there is
    ### no intern defined, we will look at the id for a special string
    if ($self->id =~ /\-/) {
        ### Special string, no intern defined, go to intern default
        if (!defined($self->{intern})) {
            $self->{intern} = 1;
        }

        my ($ldapid, $id) = $self->id =~ /^(\d+)\-(\d+)$/;

        $self->id($id);
        $self->ldapid($ldapid);
    }

    if (!$self->intern) {

        ### Get id is probably gmid, it is an external request, unless it is
        ### already set of course
        if (!$self->ldapid) {
            $self->ldapid($self->id);
            $self->id(undef);
        }
    }

    ### All set, let's rock and rolla. Depending on where we have to get the
    ### data from, fill in the blanks
    if ($self->{intern}) {
        $self->_load_intern or die('Failed loading M::B::NP Object');
    } else {
        $self->_load_extern or die('Failed loading M::B::NP Object: ' . $self->ldapid);
    }

    ### Some defaults, should move to Object
    $self->btype('medewerker');
}

sub _load_extern {
    my ($self) = @_;
    my (@entries);

    my $subject;
    if ($self->record) {
        $subject = $self->record;
    } else {
        $subject         = $self->_dispatch_options->{dbic}->resultset('Subject')->find(
            $self->ldapid
        );
    }

    if(!$subject) {
        return;
    }

    $self->ldap_rs($subject);
    $self->id( $self->ldap_rs->id);
    $self->identifier($self->ldapid . '-' . $self->id);

    ### We are loaded external, now let's set up some triggers and attributes
    $self->_load_attributes;

    return 1;
}

sub _load_intern {
    my ($self) = @_;
    my ($bo);

    (
        $self->log->debug(
            'M::B::MW->load: Could not find internal betrokkene by id ' . $self->id
        ),
        return
    ) unless $bo = $self->dbic->resultset('ZaakBetrokkenen')->find($self->id);

    #warn('Loading: ' . $bo->naam . ':' . $bo->id);
    ### TODO : NO idea yet if I really need this object
    $self->bo($bo);

    ### Retrieve data from internal GM
    return unless $bo->betrokkene_id;

    $self->ldapid($bo->betrokkene_id);

    ### Get external data
    $self->_load_extern or return;

    return 1;
}


=head2

Only used in the contact details page, there are duplicates of this.
Refactored in to lazy moose attribute because this borked up a code path
in test context. And it only seems to be eating resources in other contexts.

Constructive solution seems to be delegate this to Backend::LDAP routine.
Very low priority so I'm leaving this be.

=cut

has 'roles' => (
    is => 'ro',
    lazy => 1,
    default => sub {
        my ($self)  = @_;

        return unless $self->ldap_rs;

        return $self->ldap_rs->roles;
    }
);

sub _load_attributes {
    my ($self) = @_;

    for my $meth (keys %{ $CLONE_MAP }) {
        $self->meta->add_attribute($meth,
            'is'        => 'rw',
            'lazy'      => 1,
            ### On update, add custom field back to RT
#            'trigger'   => sub {
#                my ($self, $new, $old) = @_;
#
#                ## Do not update anything when new is the same
#                if ($new eq $old) { return $new; }
#
#                # And definetly do not update the adres_id
#                if ($meth eq 'adres_id') { return; }
#
#                ### Update object
#                $self->gm_np->$meth($new);
#                $self->gm_np->update;
#            },
            ### Load custom fields from RT
            'default'   => sub {
                my ($self) = @_;

                my $key = $CLONE_MAP->{$meth};
                my $val = $self->ldap_rs->$key // '';
                utf8::decode($val);
                return $val;
            }
        );
    }

    ### Uniformiteit, attributes known to every object, but does not have
    ### a trigger :P
    for my $meth (keys %{ $UNIFORM }) {
        my $localmeth = $UNIFORM->{$meth};
        $self->meta->add_attribute($meth,
            'is'        => 'rw',
            'lazy'      => 1,
            ### On update, add custom field back to RT
            ### Load custom fields from RT
            'default'   => sub {
                my ($self) = @_;

                return '' unless $localmeth;
                return '' unless $self->config;
                return '' unless $self->config->{ gemeente };

                return $self->config->{gemeente}->{$localmeth};
            }
        );
    }
}

sub set {
    my ($self, $dispatch_options, $external_id) = @_;

    ### Here we get a medewerk uidNumber. We presume medewerkers will get
    ### deleted from the system, so we create a betrokkene.
    my $identifier = $external_id . '-';


    my $subject = $dispatch_options->{dbic}->resultset('Subject')
                    ->find($external_id) or return;

    my $bo = $dispatch_options->{dbic}->resultset('ZaakBetrokkenen')->create({
        betrokkene_type           => 'medewerker',
        betrokkene_id             => $subject->id,
        gegevens_magazijn_id      => $subject->id,
        naam                      => $subject->displayname,
        subject_id                => $subject->uuid
    });

    $identifier .= $bo->id;

    return 'medewerker-' . $identifier;
}

sub security_identity {
    my $self = shift;

    return 'user', $self->ldap_rs->username;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 BOBJECT

TODO: Fix the POD

=cut

=head2 BRSOBJECT

TODO: Fix the POD

=cut

=head2 BUILD

TODO: Fix the POD

=cut

=head2 SUBJECT_TYPE_EMPLOYEE

TODO: Fix the POD

=cut

=head2 search

TODO: Fix the POD

=cut

=head2 security_identity

TODO: Fix the POD

=cut

=head2 set

TODO: Fix the POD

=cut

