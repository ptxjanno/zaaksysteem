package Zaaksysteem::ZAPI::Form;

use Moose;

use BTTW::Tools;

use Zaaksysteem::ZAPI::Form::FieldSet;

with 'Zaaksysteem::JSON::SerializerRole';

=head1 NAME

Zaaksysteem::ZAPI::Form - Construct a form object, suitable for angular forms.

=head1 SYNOPSIS

    my $form    = Zaaksysteem::ZAPI::Form->new(
        name        => 'hello-world',
        fieldsets   => [
            Zaaksysteem::ZAPI::Form::FieldSet->new(
                name        => 'fieldset-gegevens',
                title       => 'Benodigde gegevens',
                description => 'Lorem Ipsum Larieda',
                fields      => []
                    Zaaksysteem::ZAPI::Form::Field->new(
                        name        => 'subject_type',
                        label       => 'Subject Type',
                        type        => 'radio',
                        required    => 1,
                        options     => [
                            {
                                value   => 'extern',
                                label   => 'Extern',
                            },
                            {
                                value   => 'intern',
                                label   => 'Intern'
                            }

                        ]
                        description => 'Select a subject type'
                    ),
                    Zaaksysteem::ZAPI::Form::Field->new(
                        name        => 'subject',
                        label       => 'Subject',
                        type        => 'text',
                        description => 'Select a subject'
                    ),
                ]
            )
        ]
    );

    $form->TO_JSON;

=head1 DESCRIPTION

This generates a readable form for the Angular Forms goodness.

=head1 ATTRIBUTES

=head2 name

The identifier name of this form

=cut

has name => (
    traits   => [qw[OA]],
    label    => 'name',
    is       => 'rw',
    isa      => 'Str',
    required => 1
);

=head2 fieldsets

The fieldsets for this form. For more info, see
L<Zaaksysteem::ZAPI::Form::FieldSet>

=cut

has fieldsets => (
    traits  => [qw[Array OA]],
    label   => 'fieldsets',

    is      => 'rw',
    isa     => 'ArrayRef[Zaaksysteem::ZAPI::Form::FieldSet]',
    default => sub { return []; },

    handles => {
        all_fieldsets => 'elements',
        add_fieldset  => 'push'
    }
);

=head2 options

The options for this form.

=cut

has options => (
    traits  => [qw[OA]],
    label   => 'options',

    is      => 'rw',
    isa     => 'HashRef',
    default => sub { return {}; }
);

=head2 actions

The fieldsets for this form. For more info, see
L<Zaaksysteem::ZAPI::Form::Actions>

=cut

has actions => (
    traits  => [qw[Array OA]],
    label   => 'actions',

    is       => 'rw',
    isa      => 'ArrayRef[Zaaksysteem::ZAPI::Form::Action]',
    required => 1,
    default  => sub {
        return [];
    },
    handles => {
        all_actions => 'elements',
        add_action => 'push'
    }
);


=head1 METHODS

=head2 validate

Validates this object params

=cut

define_profile validate => (
    required => [qw[name]],
    optional => [qw[fieldsets options actions]]
);

sub validate {
    my $self            = shift;

    assert_profile($self->serialize);

    return 1;
}

=head2 load_values

Loads values from a hash into this form object

=cut

sub load_values {
    my $self            = shift;
    my $values          = shift;

    for (my $i = 0; $i < scalar(@{ $self->fieldsets }); $i++) {
        for (my $fi = 0; $fi < scalar(@{ $self->fieldsets->[$i]->fields }); $fi++) {
            next unless exists($values->{ $self->fieldsets->[$i]->fields->[$fi]->name });
            $self->fieldsets->[$i]->fields->[$fi]->value($values->{ $self->fieldsets->[$i]->fields->[$fi]->name });
        }
    }
}

=head2 TO_JSON

Creates a Angular readable JSON hash containing form information

=cut

sub TO_JSON {
    my $self        = shift;

    $self->validate;

    return $self->serialize;
}

=head2 auto_fieldset

This convenience method auto-vivifies fieldsets based on a name for it. If
such a fieldset already exists, that fieldset is returned. Otherwise a new
fieldset with that name will be created and added to the current
collection.

    $form->auto_fieldset('')

=cut

sub auto_fieldset {
    my $self = shift;
    my $name = shift;

    unless($name) {
        throw('zapi/form', 'Unable to auto-vivify/retrieve nameless fieldset');
    }

    my %params = @_;

    for my $fieldset ($self->all_fieldsets) {
        next unless $fieldset->name eq $name;

        return $fieldset;
    }

    my $fieldset = Zaaksysteem::ZAPI::Form::FieldSet->new(
        name => $name,
        title => ucfirst($name), # XXX A bit of a cludge
        %params
    );

    $self->add_fieldset($fieldset);

    return $fieldset;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

