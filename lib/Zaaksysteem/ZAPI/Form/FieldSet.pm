package Zaaksysteem::ZAPI::Form::FieldSet;

use Moose;

use constant NAME_CONSTRAINT    => qr/[a-zA-Z0-9_-]+/;

use constant FIELDSET_PROFILE          => {
    required            => [qw/
        name
        title
    /],
    optional            => [qw/
        description
        actions
        fields
    /],
    constraint_methods  => {
        name            => NAME_CONSTRAINT,
    }
};

=head1 NAME

Zaaksysteem::ZAPI::Form::FieldSet - Construct a container (FieldSet) for fields

=head1 SYNOPSIS

    my $fieldset    = Zaaksysteem::ZAPI::Form::FieldSet->new(
        name        => 'fieldset-gegevens',
        title       => 'Benodigde gegevens',
        description => 'Lorem Ipsum Larieda',
        fields      => [
            Zaaksysteem::ZAPI::Form::Field->new(
                name        => 'subject_type',
                label       => 'Subject Type',
                type        => 'radio',
                required    => 1,
                options     => [
                    {
                        value   => 'extern',
                        label   => 'Extern',
                    },
                    {
                        value   => 'intern',
                        label   => 'Intern'
                    }

                ]
                description => 'Select a subject type'
            )
        ]
    );

    $fieldset->TO_JSON;

=head1 DESCRIPTION

This generates a readable form fieldset for the Angular Forms goodness. Please
don't use this module directly, but use: L<Zaaksysteem::ZAPI::Form>.

=head1 ATTRIBUTES

=head2 name (required)

Input "name" for this fields.

=cut

has 'name'  => (
    'is'        => 'rw',
    'isa'       => 'Str',
    'required'  => 1,
);

=head2 title (required)

Title for this fieldset

=cut

has 'title'  => (
    'is'        => 'rw',
    'isa'       => 'Str',
    'required'  => 1,
);


=head2 description (optional)

Description for this fieldset

=cut

has 'description'  => (
    'is'        => 'rw',
    'isa'       => 'Str',
);

=head2 fields

The fields for this form. For more info, see L<Zaaksysteem::ZAPI::Form::Field>

=cut

has 'fields'    => (
    traits      => [qw[Array]],
    'is'        => 'rw',
    'isa'       => 'ArrayRef',
    'lazy'      => 1,
    'default'   => sub { return []; },
    handles     => {
        add_field => 'push',
        all_fields => 'elements'
    }
);

=head2 actions

=cut

has 'actions'    => (
    'is'        => 'rw',
    'isa'       => 'ArrayRef',
    'lazy'      => 1,
    'default'   => sub { return []; }
);

=head1 METHODS

=head2 TO_JSON

Returns a Angular readable json representation of this field

=cut

sub TO_JSON {
    my $self        = shift;

    $self->validate;

    return $self->_object_params;
}

=head2 validate

Validates this object, to prove it is complete and contains valid attributes

=cut

sub validate {
    my $self            = shift;

    my $dv              = Data::FormValidator->check(
        $self->_object_params,
        FIELDSET_PROFILE
    );

    die(
        'Cannot validate form fieldset, invalid or missing: '
        . join(',', $dv->invalid, $dv->missing)
    ) unless $dv->success;

    return 1;

}


=head1 INTERNAL METHODS

=head2 _object_params

Returns a HASHRef containing this object parameters according to this object
profile

=cut

sub _object_params {
    my $self        = shift;

    return { map {
        $_ => $self->$_
    } (
        @{ FIELDSET_PROFILE->{required} },
        @{ FIELDSET_PROFILE->{optional} }
    ) };
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::ZAPI::Form::Field> L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 FIELDSET_PROFILE

TODO: Fix the POD

=cut

=head2 NAME_CONSTRAINT

TODO: Fix the POD

=cut

