package Zaaksysteem::Export::Model;
use Moose;
with qw(MooseX::Log::Log4perl);

=head1 NAME

Zaaksysteem::Export::Model - An model for exporting data from Zaaksysteem

=head1 DESCRIPTION

This model hides the logic for developers to export data from Zaaksysteem as
background task

=head1 SYNOPSIS

    use Zaaksysteem::Export::Model;

    my $model = Zaaksysteem::Export::Model->new(
        schema => $zaaksysteem_schema,
    );

    my $item = $model->add_as_export($user, $filehandle, $filename);

    $model->send_email_for_export($item);
    $model->delete_expired_exports();
    $model->update_expired_for_item($item);
    $model->list_exports();

=cut

use BTTW::Tools;
require Crypt::OpenSSL::Random;

has schema => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Schema',
    required => 1,
);

has uri => (
    is       => 'ro',
    isa      => 'URI',
    required => 1,
);

has request_id => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has user => (
    is        => 'ro',
    isa       => 'Defined',
    predicate => 'has_user',
);

has subject_rs => (
    is       => 'ro',
    isa      => 'Defined',
    lazy     => 1,
    builder  => '_build_subject_rs',
    init_arg => undef,
);

has export_queue => (
    is       => 'ro',
    isa      => 'Defined',
    lazy     => 1,
    builder  => '_build_export_queue',
    init_arg => undef,
);

has filestore => (
    is       => 'ro',
    isa      => 'Defined',
    lazy     => 1,
    builder  => '_build_filestore',
    init_arg => undef,
);

has config => (
    is       => 'ro',
    isa      => 'Defined',
    lazy     => 1,
    builder  => '_build_config',
    init_arg => undef,
);

has expire_in_days => (
    is      => 'ro',
    isa     => 'Int',
    default => 3,
);

sub _build_export_queue {
    my $self = shift;
    return $self->schema->resultset('ExportQueue');
}

sub _build_subject_rs {
    my $self = shift;
    return $self->schema->resultset('Subject')
        ->search_rs({ subject_type => 'employee' });
}

sub _build_filestore {
    my $self = shift;
    return $self->schema->resultset('Filestore');
}

sub _build_config {
    my $self = shift;
    return $self->schema->resultset('Config');
}


=head1 METHODS

=head2 assert_user

Check if the user is set on the model

=cut

sub assert_user {
    my $self = shift;

    return 1 if $self->has_user && $self->user->subject_type eq 'employee';

    throw("export/model/user/missing",
        "Unable to determine user for this action!");
}


=head2 add_as_export

Add a filehandle for the user to the filestore and create an Export Queue item.
Please note that this export queue item differs from an
L<Zaaksysteem::Queue::Component> item.

=cut

sub add_as_export {
    my ($self, $filehandle, $filename) = @_;

    $self->assert_user;

    my $filestore = $self->_add_to_filestore($filehandle, $filename);
    my $item      = $self->_create_export_queue_item($filestore);
    $self->send_export_confirmation_to_user($item);

    return 1;
}

sub _create_export_queue_item {
    my ($self, $filestore) = @_;

    return $self->export_queue->create(
        {
            subject_id   => $self->user->id,
            subject_uuid => $self->user->uuid,

            filestore_id   => $filestore->id,
            filestore_uuid => $filestore->uuid,

            token   => $self->_generate_token(),
            expires => $self->_generate_expires(),
        }
    );
}

sub _generate_token {
    my $self = shift;
    return unpack 'H*', Crypt::OpenSSL::Random::random_pseudo_bytes(32);
}

sub _generate_expires {
    my $self = shift;
    my $now  = DateTime->now()->add(days => $self->expire_in_days);
    return $self->schema->format_datetime_object($now);
}

sub _add_to_filestore {
    my ($self, $filehandle, $filename) = @_;

    return $self->filestore->filestore_create(
        {
            file_path     => "$filehandle",
            original_name => $filename,
        }
    );
}

=head2 send_export_confirmation_to_user

=cut

sub send_export_confirmation_to_user {
    my ($self, $item) = @_;

    my $address = $self->user->email_address;
    if (!defined $address) {
        $self->log->info(
            "Unable to send message to the user, no e-mail defined");
        return 0;
    }

    try {
        my $template = $self->config->get_email_template_by_parameter(
            'export_queue_email_template_id'
        );

        my @paths = $self->uri->path_segments;
        push(@paths, 'download', $item->token);

        my $token_uri = $self->uri->clone;
        $token_uri->path_segments(@paths);

        $template->send_mail(
            {
                to          => $address,
                ztt_context => { token_uri => $token_uri },
                request_id  => $self->request_id,
            }
        );
        return 1;
    }
    catch {
        $self->log->info("$_");
        return 0;
    };
}

=head2 add_export_for_user

=cut

sub add_export_for_user {
    my ($self) = @_;

    return 1;
}

=head2 update_expired_for_item

=cut

sub update_expired_for_item {
    my ($self, $item) = @_;

    return 1;
}

=head2 list_exports

=cut

sub list_exports {
    my ($self, $user) = @_;

    $user = $self->assert_optional_user($user);

    return $self->export_queue->search_rs(
        {
            subject_id => $user->id,
            expires    => {
                '>' => $self->schema->format_datetime_object(DateTime->now()),
            },
        },
        {
            prefetch => 'filestore_id',
        }
    );
}

sub assert_optional_user {
    my ($self, $user) = @_;
    $self->assert_user unless $user;
    $user //= $self->user;
    return $user;
}

sub get_export_item_by_token {
    my ($self, $token, $user) = @_;

    my $rs = $self->list_exports($user);
    my $entry = $rs->search_rs({token => $token})->first;
    return $entry if $entry;

    throw(
        "export/queue/invalid/token",
        "Unable to find entry with token '$token'"
    );

}

sub delete_expired_exports {
    my ($self) = @_;

    my $entries = $self->export_queue->search_rs(
        {
            expires    => {
                '<' => $self->schema->format_datetime_object(DateTime->now()),
            },
        },
        {
            prefetch => 'filestore_id',
        }
    );

    while (my $item = $entries->next) {
        $self->_delete_item($item);
    }
    return;
}

sub _delete_item {
    my ($self, $item) = @_;

    my $filestore = $item->filestore_id;
    $filestore->erase;
    $item->delete;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
