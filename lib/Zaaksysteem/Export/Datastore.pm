package Zaaksysteem::Export::Datastore;
use Moose;

=head1 NAME

Zaaksysteem::Export::Datastore - An model for exporting data from the Gegevens
magazijn

=head1 DESCRIPTION

This model extends the L<Zaaksysteem::Export::Model>.

=head1 SYNOPSIS

    use Zaaksysteem::Export::Datastore;

    my $model = Zaaksysteem::Export::Datastore->new(
        schema => $zaaksysteem_schema,
    );

    my $queue_item  = $model->create_queue_item($params);
    my $export_item = $model->export_datastore($queue_item);


=cut

use BTTW::Tools;
use Zaaksysteem::Export::Model;

has schema => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Schema',
    required => 1,
);

has uri => (
    is        => 'ro',
    isa       => 'URI',
    required  => 1,
);

has user => (
    is        => 'ro',
    isa       => 'Defined',
    required  => 1,
);

has export_queue => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Export::Model',
    lazy     => 1,
    builder  => '_build_export_queue',
    init_arg => undef,
);

sub _build_export_queue {
    my $self = shift;

    return Zaaksysteem::Export::Model->new(
        schema => $self->schema,
        user   => $self->user,
        uri    => $self->uri,
    );
}

sub export {
    my ($self, $item) = @_;

}


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
