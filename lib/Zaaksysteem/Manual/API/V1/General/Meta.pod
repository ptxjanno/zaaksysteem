=head1 NAME

Zaaksysteem::Manual::API::V1::General::Meta - Metadata about Zaaksysteem

=head1 Description

Get a list of metadata from Zaaksysteem

=head2 API

This document is based on the V1 API of Zaaksysteem, more information about the default format
of this API can be found in L<Zaaksysteem::Manual::API::V1>. Please make sure you read this
document before continuing.

=head2 URL

The base URL for this API is:

    /api/v1/general/meta

Make sure you use the HTTP Method C<GET> for retrieving.

You need to be logged in to Zaaksysteem to speak to this API.

=head1 Retrieve data

=head2 List all configuration items

   /api/v1/general/meta

This API differs from most APIs as the configuration items do not have a UUID.

B<Response JSON>

=begin javascript

{
   "api_version" : 1,
   "status_code" : 200,
   "request_id" : "mintlab_sprint-5127cf-02890e",
   "result" : {
      "instance" : {
         "pager" : {
            "next" : null,
            "prev" : null,
            "total_rows" : 4,
            "pages" : 1,
            "page" : 1,
            "rows" : 4
         },
         "rows" : [
            {
               "reference" : null,
               "instance" : {
                  "date_created" : "2016-04-11T10:37:00Z",
                  "label" : "Version",
                  "date_modified" : "2016-04-11T10:37:00Z",
                  "value" : "3.25.0.1"
               },
               "type" : "meta"
            },
            {
               "type" : "meta",
               "reference" : null,
               "instance" : {
                  "date_modified" : "2016-04-11T10:37:00Z",
                  "label" : "Appserver",
                  "date_created" : "2016-04-11T10:37:00Z",
                  "value" : "reserved-vcloud.mintlab.cyso.net"
               }
            },
            {
               "type" : "meta",
               "instance" : {
                  "value" : {
                     "email" : "servicedesk@mintlab.nl",
                     "latitude" : "51.278",
                     "naam_kort" : "Sprntlb",
                     "zaak_email" : "rianne@mintlab.nl",
                     "woonplaats" : "Amsterdam",
                     "longitude" : "5.563",
                     "adres" : "Donker Curtiusstraat 7 - 521",
                     "postbus_postcode" : "",
                     "faxnummer" : "",
                     "postcode" : "1051 JL",
                     "naam_lang" : "Sprintlab B.V.",
                     "naam" : "Sprintlab",
                     "postbus" : "",
                     "huisnummer" : "7-521",
                     "gemeente_id_url" : "http://www.mintlab.nl/gemeente-id-url",
                     "straatnaam" : "Donker Curtiusstraat",
                     "gemeente_portal" : "http://www.mintlab.nl/portal-url",
                     "website" : "http://www.mintlab.nl",
                     "telefoonnummer" : "0207370005"
                  },
                  "date_modified" : "2016-04-11T10:37:00Z",
                  "date_created" : "2016-04-11T10:37:00Z",
                  "label" : "Customer information"
               },
               "reference" : null
            },
            {
               "reference" : null,
               "instance" : {
                  "value" : "dbi:Pg:dbname=zaaksysteem_sprint_dev;host=db101.cyso.zaaksysteem.nl",
                  "label" : "Database",
                  "date_created" : "2016-04-11T10:37:00Z",
                  "date_modified" : "2016-04-11T10:37:00Z"
               },
               "type" : "meta"
            }
         ]
      },
      "type" : "set",
      "reference" : null
   }
}

=end javascript

=head2 get

The get call for individual items is not implemented at this time.

=head1 Mutate data

Mutations for meta items are not supported at this moment.

=head1 Objects

The objects which are returned by this API are currently general configuration items, specific the to the instance.
The C<Database>, C<Customer information> and C<Version> items are not likely to be removed. The C<Appserver> may become obsolete
once we will dockerize Zaaksysteem in the future. The objects are mainly used by the C<About> box in the GUI.

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
