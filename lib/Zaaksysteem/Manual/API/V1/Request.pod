=head1 NAME

Zaaksysteem::Manual::API::V1::Request - API v1 request

=head1 ABSTRACT

This page documents the request format for the Zaaksysteem v1 API.

=head1 QUICKSTART

=begin bash

    curl --digest -u "api_user:secret_key" \
         -H "API-Interface-Id: 42" \
         -H "content-Type: application/json" \
         https://zs.instance.tld/api/v1/some/endpoint

=end bash

=head1 OVERVIEW

API requests are HTTP-based, and rely on the semantics described in
L<rfc2616|https://tools.ietf.org/html/rfc2616>.

Data retrieval requests B<MUST> be made using the C<GET> verb. This ensures
the requests are inherently cacheable and safe (idempotent, no side effects).

Update requests and process triggers B<MUST> be made using the C<POST> verb.

=head1 AUTHENTICATION

Requests may require authentication, which Zaaksysteem provides through an
implementation of
L<HTTP Digest Authentication|https://tools.ietf.org/html/rfc2617>.

Since Zaaksysteem supports multiple API interfaces, which share namespaces and
endpoints, it may be necessary to include a non-standard HTTP request header
in order to identify the interface which should be used to connect through.

The only time such an interface identifier is not required for each request is
when only one API interface is active at the time of request. Since this state
may not be stable over time, it is highly recommended to include the interface
identifier header regardless of current requirements.

=head1 POST REQUESTS

With exception of file upload requests, all C<POST> requests to Zaaksysteem
must be performed using the following rules

  * Request body contains valid JSON data
  * Content-Type header is set to application/json

=head1 RESPONSES

See L<Zaaksysteem::Manual::API::V1::Response>.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
