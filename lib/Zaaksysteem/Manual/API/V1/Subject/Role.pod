=head1 NAME

Zaaksysteem::Manual::API::V1::Subject::Role - List "subject roles"

=head1 Description

Subject roles indicate the "kind" of role a subject has in the case. It
includes C<Advocaat>, C<Bewindvoerder>, etc.

This API returns the list of built-in and custom roles.

=head2 API

This document is based on our version 1 API of Zaaksysteem, more information
about the default format of this API can be found in
L<Zaaksysteem::Manual::API::V1>. Please make sure you read this document before
continuing.

=head2 URL

The base URL for this API is:

    /api/v1/subject/role

Make sure you use the HTTP Method C<GET> for retrieving.

=head1 Retrieve data

=head2 get

This API supports only one operation: retrieval of the list of configured and
built-in subject roles.

To retrieve the list of subject roles, you can perform a C<GET> on
C</api/v1/subject/role>. You will get the response in the C<result> parameter.
It will only contain one single result. The C<instance> property will contain
the contents of this subject.

B<Example call>

    curl --anyauth -H "Content-Type: application/json" --digest -u "username:password" https://localhost/api/v1/subject/role

B<Request JSON>

Request bodies are not supported for C<GET> calls.

B<Response JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "development-02a845-06f225",
   "status_code" : 200,
   "result" : {
      "instance" : {
         "pager" : {
            "rows" : 14,
            "total_rows" : 14,
            "next" : null,
            "prev" : null,
            "page" : 1,
            "pages" : 1
         },
         "rows" : [
            {
               "type" : "role",
               "reference" : null,
               "instance" : {
                  "label" : "Advocaat",
                  "date_created" : "2016-12-14T14:38:51Z",
                  "date_modified" : "2016-12-14T14:38:51Z",
                  "is_builtin" : true
               }
            },
            {
               "reference" : null,
               "type" : "role",
               "instance" : {
                  "date_created" : "2016-12-14T14:38:51Z",
                  "label" : "Auditor",
                  "is_builtin" : true,
                  "date_modified" : "2016-12-14T14:38:51Z"
               }
            },
            {
               "instance" : {
                  "date_modified" : "2016-12-14T14:38:51Z",
                  "is_builtin" : false,
                  "label" : "test",
                  "date_created" : "2016-12-14T14:38:51Z"
               },
               "reference" : null,
               "type" : "role"
            }
         ]
      },
      "reference" : null,
      "type" : "set"
   }
}

=end javascript

=head1 Objects

=head2 Subject Role

The of the call in this document returns instances of type C<subject/role>.
This is a simple object with two fields:

=over 4

=item label

B<Type>: String

Label to display for the custom subject role.

=item is_builtin

B<Type>: Boolean

If the subject role is a built-in one, this is true. If it is a custom one,
configured by the customer, it is false.

=back

=head1 Support

The data in this document is supported by the following test. Please make sure you use the API as described
in this test. Any use of this API outside the scope of this test is B<unsupported>.

L<TestFor::Catalyst::API::V1::Subject::Role>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
