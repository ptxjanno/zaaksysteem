=head1 NAME

Zaaksysteem::Manual::API::V1::Types::Case::Milestone - Type definition for case milestones

=head1 DESCRIPTION

This page documents the serialization of C<case/milestone> objects.

=head1 JSON

=begin javascript

{
    "type": "case/milestone",
    "reference": null,
    "instance": {
        "last_sequence_number": 2,
        "milestone_label": "Geregistreerd",
        "milestone_sequence_number": 1,
        "phase_label": "Afhandelen",
        "phase_sequence_number": 2,
        "date_created": "2017-05-16T10:10:17Z",
        "date_modified": "2017-05-16T10:10:17Z"
    }
}

=end javascript

=head1 INSTANCE ATTRIBUTES

=head2 last_sequence_number E<raquo> L<C<sequence_number>|Zaaksysteem::Manual::API::V1::ValueTypes/sequence_number>

Last number in the sequence of phases.

=head2 milestone_label E<raquo> L<C<string>|Zaaksysteem::Manual::API::V1::ValueTypes/string>

Label of the currently attained milestone.

=head2 milestone_sequence_number E<raquo> L<C<sequence_number>|Zaaksysteem::Manual::API::v1::ValueTypes/sequence_nubmer>

Sequence number of the currently attained milestone.

=head2 phase_label E<raquo> L<C<string>|Zaaksysteem::Manual::API::V1::ValueTypes/string>

Label of the current phase.

=head2 phase_sequence_number L<C<sequence_number>|Zaaksysteem::Manual::API::V1::ValueTypes/sequence_number>

Sequence number of the current phase.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
