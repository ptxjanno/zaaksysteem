use utf8;
package Zaaksysteem::Schema::ChecklistItem;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::ChecklistItem

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<checklist_item>

=cut

__PACKAGE__->table("checklist_item");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'checklist_item_id_seq'

=head2 checklist_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 label

  data_type: 'text'
  is_nullable: 1

=head2 state

  data_type: 'boolean'
  default_value: false
  is_nullable: 0

=head2 sequence

  accessor: 'column_sequence'
  data_type: 'integer'
  is_nullable: 1

=head2 user_defined

  data_type: 'boolean'
  default_value: true
  is_nullable: 0

=head2 deprecated_answer

  data_type: 'varchar'
  is_nullable: 1
  size: 8

=head2 uuid

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 0
  size: 16

=head2 due_date

  data_type: 'date'
  is_nullable: 1
  timezone: 'UTC'

=head2 description

  data_type: 'text'
  is_nullable: 1

=head2 assignee_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "checklist_item_id_seq",
  },
  "checklist_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "label",
  { data_type => "text", is_nullable => 1 },
  "state",
  { data_type => "boolean", default_value => \"false", is_nullable => 0 },
  "sequence",
  { accessor => "column_sequence", data_type => "integer", is_nullable => 1 },
  "user_defined",
  { data_type => "boolean", default_value => \"true", is_nullable => 0 },
  "deprecated_answer",
  { data_type => "varchar", is_nullable => 1, size => 8 },
  "uuid",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 0,
    size => 16,
  },
  "due_date",
  { data_type => "date", is_nullable => 1, timezone => "UTC" },
  "description",
  { data_type => "text", is_nullable => 1 },
  "assignee_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<checklist_item_uuid_key>

=over 4

=item * L</uuid>

=back

=cut

__PACKAGE__->add_unique_constraint("checklist_item_uuid_key", ["uuid"]);

=head1 RELATIONS

=head2 assignee_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Subject>

=cut

__PACKAGE__->belongs_to(
  "assignee_id",
  "Zaaksysteem::Schema::Subject",
  { id => "assignee_id" },
);

=head2 checklist_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Checklist>

=cut

__PACKAGE__->belongs_to(
  "checklist_id",
  "Zaaksysteem::Schema::Checklist",
  { id => "checklist_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-12-02 10:13:13
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:rriz7Q0E7HiBxzjiW+E4tg

__PACKAGE__->load_components(
    '+Zaaksysteem::DB::Component::ChecklistItem',
    __PACKAGE__->load_components()
);

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

