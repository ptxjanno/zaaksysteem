use utf8;
package Zaaksysteem::Schema::Adres;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::Adres

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<adres>

=cut

__PACKAGE__->table("adres");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'adres_id_seq'

=head2 straatnaam

  data_type: 'text'
  is_nullable: 1

=head2 huisnummer

  data_type: 'bigint'
  is_nullable: 1

=head2 huisletter

  data_type: 'char'
  is_nullable: 1
  size: 1

=head2 huisnummertoevoeging

  data_type: 'text'
  is_nullable: 1

=head2 nadere_aanduiding

  data_type: 'varchar'
  is_nullable: 1
  size: 35

=head2 postcode

  data_type: 'varchar'
  is_nullable: 1
  size: 6

=head2 woonplaats

  data_type: 'text'
  is_nullable: 1

=head2 gemeentedeel

  data_type: 'text'
  is_nullable: 1

=head2 functie_adres

  data_type: 'char'
  is_nullable: 0
  size: 1

=head2 datum_aanvang_bewoning

  data_type: 'date'
  is_nullable: 1
  timezone: 'UTC'

=head2 woonplaats_id

  data_type: 'varchar'
  is_nullable: 1
  size: 32

=head2 gemeente_code

  data_type: 'smallint'
  is_nullable: 1

=head2 hash

  data_type: 'varchar'
  is_nullable: 1
  size: 32

=head2 import_datum

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 deleted_on

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 adres_buitenland1

  data_type: 'text'
  is_nullable: 1

=head2 adres_buitenland2

  data_type: 'text'
  is_nullable: 1

=head2 adres_buitenland3

  data_type: 'text'
  is_nullable: 1

=head2 landcode

  data_type: 'integer'
  default_value: 6030
  is_nullable: 1

=head2 natuurlijk_persoon_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "adres_id_seq",
  },
  "straatnaam",
  { data_type => "text", is_nullable => 1 },
  "huisnummer",
  { data_type => "bigint", is_nullable => 1 },
  "huisletter",
  { data_type => "char", is_nullable => 1, size => 1 },
  "huisnummertoevoeging",
  { data_type => "text", is_nullable => 1 },
  "nadere_aanduiding",
  { data_type => "varchar", is_nullable => 1, size => 35 },
  "postcode",
  { data_type => "varchar", is_nullable => 1, size => 6 },
  "woonplaats",
  { data_type => "text", is_nullable => 1 },
  "gemeentedeel",
  { data_type => "text", is_nullable => 1 },
  "functie_adres",
  { data_type => "char", is_nullable => 0, size => 1 },
  "datum_aanvang_bewoning",
  { data_type => "date", is_nullable => 1, timezone => "UTC" },
  "woonplaats_id",
  { data_type => "varchar", is_nullable => 1, size => 32 },
  "gemeente_code",
  { data_type => "smallint", is_nullable => 1 },
  "hash",
  { data_type => "varchar", is_nullable => 1, size => 32 },
  "import_datum",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "deleted_on",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "adres_buitenland1",
  { data_type => "text", is_nullable => 1 },
  "adres_buitenland2",
  { data_type => "text", is_nullable => 1 },
  "adres_buitenland3",
  { data_type => "text", is_nullable => 1 },
  "landcode",
  { data_type => "integer", default_value => 6030, is_nullable => 1 },
  "natuurlijk_persoon_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 natuurlijk_persoon_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::NatuurlijkPersoon>

=cut

__PACKAGE__->belongs_to(
  "natuurlijk_persoon_id",
  "Zaaksysteem::Schema::NatuurlijkPersoon",
  { id => "natuurlijk_persoon_id" },
);

=head2 natuurlijk_persoons

Type: has_many

Related object: L<Zaaksysteem::Schema::NatuurlijkPersoon>

=cut

__PACKAGE__->has_many(
  "natuurlijk_persoons",
  "Zaaksysteem::Schema::NatuurlijkPersoon",
  { "foreign.adres_id" => "self.id" },
  undef,
);


# Created by DBIx::Class::Schema::Loader v0.07048 @ 2018-02-08 13:35:23
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:gmAlJV4EpWVeZb9Spw+VKg

__PACKAGE__->add_columns('import_datum',
    { %{ __PACKAGE__->column_info('import_datum') },
    set_on_create => 1,
});

__PACKAGE__->load_components(
    "+Zaaksysteem::DB::Component::Adres",
    __PACKAGE__->load_components()
);

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

