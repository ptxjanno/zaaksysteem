use utf8;
package Zaaksysteem::Schema::CaseAcl;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::CaseAcl

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';
__PACKAGE__->table_class("DBIx::Class::ResultSource::View");

=head1 TABLE: C<case_acl>

=cut

__PACKAGE__->table("case_acl");
__PACKAGE__->result_source_instance->view_definition(" SELECT z.id AS case_id,\n    z.uuid AS case_uuid,\n    cam.key AS permission,\n    s.id AS subject_id,\n    s.uuid AS subject_uuid,\n    z.zaaktype_id AS casetype_id\n   FROM (((zaak z\n     JOIN zaaktype_authorisation za ON (((za.zaaktype_id = z.zaaktype_id) AND (za.confidential = z.confidential))))\n     JOIN (subject_position_matrix spm\n     JOIN subject s ON ((spm.subject_id = s.id))) ON (((spm.role_id = za.role_id) AND (spm.group_id = za.ou_id))))\n     JOIN case_authorisation_map cam ON ((za.recht = cam.legacy_key)))\nUNION ALL\n SELECT z.id AS case_id,\n    z.uuid AS case_uuid,\n    za.capability AS permission,\n    s.id AS subject_id,\n    s.uuid AS subject_uuid,\n    z.zaaktype_id AS casetype_id\n   FROM ((zaak z\n     JOIN zaak_authorisation za ON (((za.zaak_id = z.id) AND (za.entity_type = 'position'::text))))\n     JOIN (subject_position_matrix spm\n     JOIN subject s ON ((spm.subject_id = s.id))) ON ((spm.\"position\" = za.entity_id)))\nUNION ALL\n SELECT z.id AS case_id,\n    z.uuid AS case_uuid,\n    za.capability AS permission,\n    s.id AS subject_id,\n    s.uuid AS subject_uuid,\n    z.zaaktype_id AS casetype_id\n   FROM ((zaak z\n     JOIN zaak_authorisation za ON (((za.zaak_id = z.id) AND (za.entity_type = 'user'::text))))\n     JOIN subject s ON (((s.username)::text = za.entity_id)))\nUNION ALL\n SELECT z.id AS case_id,\n    z.uuid AS case_uuid,\n    unnest(ARRAY['read'::text, 'write'::text, 'search'::text]) AS permission,\n    s.id AS subject_id,\n    s.uuid AS subject_uuid,\n    z.zaaktype_id AS casetype_id\n   FROM ((zaak z\n     JOIN zaak_betrokkenen zb ON (((zb.id = z.aanvrager) OR (zb.id = z.behandelaar) OR ((zb.id = z.coordinator) AND ((zb.betrokkene_type)::text = 'medewerker'::text)))))\n     JOIN subject s ON ((zb.betrokkene_id = s.id)))");

=head1 ACCESSORS

=head2 case_id

  data_type: 'integer'
  is_nullable: 1

=head2 case_uuid

  data_type: 'uuid'
  is_nullable: 1
  size: 16

=head2 permission

  data_type: 'text'
  is_nullable: 1

=head2 subject_id

  data_type: 'integer'
  is_nullable: 1

=head2 subject_uuid

  data_type: 'uuid'
  is_nullable: 1
  size: 16

=head2 casetype_id

  data_type: 'integer'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "case_id",
  { data_type => "integer", is_nullable => 1 },
  "case_uuid",
  { data_type => "uuid", is_nullable => 1, size => 16 },
  "permission",
  { data_type => "text", is_nullable => 1 },
  "subject_id",
  { data_type => "integer", is_nullable => 1 },
  "subject_uuid",
  { data_type => "uuid", is_nullable => 1, size => 16 },
  "casetype_id",
  { data_type => "integer", is_nullable => 1 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-03-27 09:48:34
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:Y8TT84LdLReA32VyoJJthg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
