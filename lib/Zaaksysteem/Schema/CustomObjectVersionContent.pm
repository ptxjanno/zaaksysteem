use utf8;
package Zaaksysteem::Schema::CustomObjectVersionContent;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::CustomObjectVersionContent

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<custom_object_version_content>

=cut

__PACKAGE__->table("custom_object_version_content");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'custom_object_version_content_id_seq'

=head2 archive_status

  data_type: 'enum'
  extra: {custom_type_name => "custom_object_version_content_archive_status",list => ["archived","to destroy","to preserve"]}
  is_nullable: 1

=head2 archive_ground

  data_type: 'text'
  is_nullable: 1

=head2 archive_retention

  data_type: 'integer'
  is_nullable: 1

=head2 custom_fields

  data_type: 'jsonb'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "custom_object_version_content_id_seq",
  },
  "archive_status",
  {
    data_type => "enum",
    extra => {
      custom_type_name => "custom_object_version_content_archive_status",
      list => ["archived", "to destroy", "to preserve"],
    },
    is_nullable => 1,
  },
  "archive_ground",
  { data_type => "text", is_nullable => 1 },
  "archive_retention",
  { data_type => "integer", is_nullable => 1 },
  "custom_fields",
  { data_type => "jsonb", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 custom_object_versions

Type: has_many

Related object: L<Zaaksysteem::Schema::CustomObjectVersion>

=cut

__PACKAGE__->has_many(
  "custom_object_versions",
  "Zaaksysteem::Schema::CustomObjectVersion",
  { "foreign.custom_object_version_content_id" => "self.id" },
  undef,
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-03-10 08:46:23
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:ZzDTAu4Ndr5aQ84nt+/YvQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2020, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut