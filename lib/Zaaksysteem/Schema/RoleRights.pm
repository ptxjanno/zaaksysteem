use utf8;
package Zaaksysteem::Schema::RoleRights;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::RoleRights

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<role_rights>

=cut

__PACKAGE__->table("role_rights");

=head1 ACCESSORS

=head2 rights_name

  data_type: 'text'
  is_foreign_key: 1
  is_nullable: 0

=head2 role_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "rights_name",
  { data_type => "text", is_foreign_key => 1, is_nullable => 0 },
  "role_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</rights_name>

=item * L</role_id>

=back

=cut

__PACKAGE__->set_primary_key("rights_name", "role_id");

=head1 RELATIONS

=head2 rights_name

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Rights>

=cut

__PACKAGE__->belongs_to(
  "rights_name",
  "Zaaksysteem::Schema::Rights",
  { name => "rights_name" },
);

=head2 role_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Roles>

=cut

__PACKAGE__->belongs_to("role_id", "Zaaksysteem::Schema::Roles", { id => "role_id" });


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-10-11 09:39:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:YI4h/jiFOB+bMHIzzWZghw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
