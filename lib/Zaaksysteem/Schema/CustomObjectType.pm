use utf8;
package Zaaksysteem::Schema::CustomObjectType;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::CustomObjectType

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<custom_object_type>

=cut

__PACKAGE__->table("custom_object_type");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'custom_object_type_id_seq'

=head2 uuid

  data_type: 'uuid'
  is_nullable: 0
  size: 16

=head2 catalog_folder_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 custom_object_type_version_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 authorization_definition

  data_type: 'jsonb'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "custom_object_type_id_seq",
  },
  "uuid",
  { data_type => "uuid", is_nullable => 0, size => 16 },
  "catalog_folder_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "custom_object_type_version_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "authorization_definition",
  { data_type => "jsonb", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<custom_object_type_uuid_key>

=over 4

=item * L</uuid>

=back

=cut

__PACKAGE__->add_unique_constraint("custom_object_type_uuid_key", ["uuid"]);

=head1 RELATIONS

=head2 catalog_folder_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::BibliotheekCategorie>

=cut

__PACKAGE__->belongs_to(
  "catalog_folder_id",
  "Zaaksysteem::Schema::BibliotheekCategorie",
  { id => "catalog_folder_id" },
);

=head2 custom_object_type_version_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::CustomObjectTypeVersion>

=cut

__PACKAGE__->belongs_to(
  "custom_object_type_version_id",
  "Zaaksysteem::Schema::CustomObjectTypeVersion",
  { id => "custom_object_type_version_id" },
);

=head2 custom_object_type_versions

Type: has_many

Related object: L<Zaaksysteem::Schema::CustomObjectTypeVersion>

=cut

__PACKAGE__->has_many(
  "custom_object_type_versions",
  "Zaaksysteem::Schema::CustomObjectTypeVersion",
  { "foreign.custom_object_type_id" => "self.id" },
  undef,
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-04-02 16:17:32
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:dgrUu0szM6w4DrNJUUoKMA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2020, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut