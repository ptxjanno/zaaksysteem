use utf8;
package Zaaksysteem::Schema::ThreadMessageAttachmentDerivative;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::ThreadMessageAttachmentDerivative

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<thread_message_attachment_derivative>

=cut

__PACKAGE__->table("thread_message_attachment_derivative");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'thread_message_attachment_derivative_id_seq'

=head2 thread_message_attachment_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 filestore_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 max_width

  data_type: 'integer'
  is_nullable: 0

=head2 max_height

  data_type: 'integer'
  is_nullable: 0

=head2 date_generated

  data_type: 'timestamp'
  default_value: current_timestamp
  is_nullable: 0
  original: {default_value => \"now()"}
  timezone: 'UTC'

=head2 type

  data_type: 'text'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "thread_message_attachment_derivative_id_seq",
  },
  "thread_message_attachment_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "filestore_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "max_width",
  { data_type => "integer", is_nullable => 0 },
  "max_height",
  { data_type => "integer", is_nullable => 0 },
  "date_generated",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 0,
    original      => { default_value => \"now()" },
    timezone      => "UTC",
  },
  "type",
  { data_type => "text", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 filestore_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Filestore>

=cut

__PACKAGE__->belongs_to(
  "filestore_id",
  "Zaaksysteem::Schema::Filestore",
  { id => "filestore_id" },
);

=head2 thread_message_attachment_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ThreadMessageAttachment>

=cut

__PACKAGE__->belongs_to(
  "thread_message_attachment_id",
  "Zaaksysteem::Schema::ThreadMessageAttachment",
  { id => "thread_message_attachment_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-09-30 12:56:24
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:8rR7/7ypFUwVfAHU1ZEPow


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__


=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
