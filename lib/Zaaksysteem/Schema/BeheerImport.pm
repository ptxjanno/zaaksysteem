use utf8;
package Zaaksysteem::Schema::BeheerImport;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::BeheerImport

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<beheer_import>

=cut

__PACKAGE__->table("beheer_import");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'beheer_import_id_seq'

=head2 importtype

  data_type: 'varchar'
  is_nullable: 1
  size: 256

=head2 succesvol

  data_type: 'integer'
  is_nullable: 1

=head2 finished

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 import_create

  data_type: 'integer'
  is_nullable: 1

=head2 import_update

  data_type: 'integer'
  is_nullable: 1

=head2 error

  data_type: 'integer'
  is_nullable: 1

=head2 error_message

  data_type: 'text'
  is_nullable: 1

=head2 entries

  data_type: 'integer'
  is_nullable: 1

=head2 created

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "beheer_import_id_seq",
  },
  "importtype",
  { data_type => "varchar", is_nullable => 1, size => 256 },
  "succesvol",
  { data_type => "integer", is_nullable => 1 },
  "finished",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "import_create",
  { data_type => "integer", is_nullable => 1 },
  "import_update",
  { data_type => "integer", is_nullable => 1 },
  "error",
  { data_type => "integer", is_nullable => 1 },
  "error_message",
  { data_type => "text", is_nullable => 1 },
  "entries",
  { data_type => "integer", is_nullable => 1 },
  "created",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "last_modified",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 beheer_import_logs

Type: has_many

Related object: L<Zaaksysteem::Schema::BeheerImportLog>

=cut

__PACKAGE__->has_many(
  "beheer_import_logs",
  "Zaaksysteem::Schema::BeheerImportLog",
  { "foreign.import_id" => "self.id" },
  undef,
);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-28 09:26:43
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:fuOCudgdg5zHNy9o/9DSCw

# You can replace this text with custom content, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

