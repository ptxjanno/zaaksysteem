use utf8;
package Zaaksysteem::Schema::ZaaktypeKenmerken;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::ZaaktypeKenmerken

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<zaaktype_kenmerken>

=cut

__PACKAGE__->table("zaaktype_kenmerken");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'zaaktype_kenmerken_id_seq'

=head2 bibliotheek_kenmerken_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 value_mandatory

  data_type: 'integer'
  is_nullable: 1

=head2 label

  data_type: 'text'
  is_nullable: 1

=head2 help

  data_type: 'text'
  is_nullable: 1

=head2 created

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 zaaktype_node_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 zaak_status_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 pip

  data_type: 'integer'
  is_nullable: 1

=head2 zaakinformatie_view

  data_type: 'integer'
  default_value: 1
  is_nullable: 1

=head2 bag_zaakadres

  data_type: 'integer'
  is_nullable: 1

=head2 value_default

  data_type: 'text'
  is_nullable: 1

=head2 pip_can_change

  data_type: 'boolean'
  is_nullable: 1

=head2 publish_public

  data_type: 'integer'
  is_nullable: 1

=head2 is_systeemkenmerk

  data_type: 'boolean'
  default_value: false
  is_nullable: 1

=head2 required_permissions

  data_type: 'text'
  is_nullable: 1

=head2 version

  data_type: 'integer'
  is_nullable: 1

=head2 help_extern

  data_type: 'text'
  is_nullable: 1

=head2 object_id

  data_type: 'uuid'
  is_foreign_key: 1
  is_nullable: 1
  size: 16

=head2 object_metadata

  data_type: 'text'
  default_value: '{}'
  is_nullable: 0

=head2 label_multiple

  data_type: 'text'
  is_nullable: 1

=head2 properties

  data_type: 'text'
  default_value: '{}'
  is_nullable: 1

=head2 is_group

  data_type: 'boolean'
  default_value: false
  is_nullable: 0

=head2 referential

  data_type: 'boolean'
  default_value: false
  is_nullable: 0

=head2 uuid

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 0
  size: 16

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "zaaktype_kenmerken_id_seq",
  },
  "bibliotheek_kenmerken_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "value_mandatory",
  { data_type => "integer", is_nullable => 1 },
  "label",
  { data_type => "text", is_nullable => 1 },
  "help",
  { data_type => "text", is_nullable => 1 },
  "created",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "last_modified",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "zaaktype_node_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "zaak_status_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "pip",
  { data_type => "integer", is_nullable => 1 },
  "zaakinformatie_view",
  { data_type => "integer", default_value => 1, is_nullable => 1 },
  "bag_zaakadres",
  { data_type => "integer", is_nullable => 1 },
  "value_default",
  { data_type => "text", is_nullable => 1 },
  "pip_can_change",
  { data_type => "boolean", is_nullable => 1 },
  "publish_public",
  { data_type => "integer", is_nullable => 1 },
  "is_systeemkenmerk",
  { data_type => "boolean", default_value => \"false", is_nullable => 1 },
  "required_permissions",
  { data_type => "text", is_nullable => 1 },
  "version",
  { data_type => "integer", is_nullable => 1 },
  "help_extern",
  { data_type => "text", is_nullable => 1 },
  "object_id",
  { data_type => "uuid", is_foreign_key => 1, is_nullable => 1, size => 16 },
  "object_metadata",
  { data_type => "text", default_value => "{}", is_nullable => 0 },
  "label_multiple",
  { data_type => "text", is_nullable => 1 },
  "properties",
  { data_type => "text", default_value => "{}", is_nullable => 1 },
  "is_group",
  { data_type => "boolean", default_value => \"false", is_nullable => 0 },
  "referential",
  { data_type => "boolean", default_value => \"false", is_nullable => 0 },
  "uuid",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 0,
    size => 16,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<zaaktype_kenmerken_uuid_key>

=over 4

=item * L</uuid>

=back

=cut

__PACKAGE__->add_unique_constraint("zaaktype_kenmerken_uuid_key", ["uuid"]);

=head1 RELATIONS

=head2 bibliotheek_kenmerken_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::BibliotheekKenmerken>

=cut

__PACKAGE__->belongs_to(
  "bibliotheek_kenmerken_id",
  "Zaaksysteem::Schema::BibliotheekKenmerken",
  { id => "bibliotheek_kenmerken_id" },
);

=head2 file_case_documents

Type: has_many

Related object: L<Zaaksysteem::Schema::FileCaseDocument>

=cut

__PACKAGE__->has_many(
  "file_case_documents",
  "Zaaksysteem::Schema::FileCaseDocument",
  { "foreign.case_document_id" => "self.id" },
  undef,
);

=head2 object_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ObjectData>

=cut

__PACKAGE__->belongs_to(
  "object_id",
  "Zaaksysteem::Schema::ObjectData",
  { uuid => "object_id" },
);

=head2 zaak_status_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ZaaktypeStatus>

=cut

__PACKAGE__->belongs_to(
  "zaak_status_id",
  "Zaaksysteem::Schema::ZaaktypeStatus",
  { id => "zaak_status_id" },
);

=head2 zaaktype_node_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ZaaktypeNode>

=cut

__PACKAGE__->belongs_to(
  "zaaktype_node_id",
  "Zaaksysteem::Schema::ZaaktypeNode",
  { id => "zaaktype_node_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-04-20 12:22:27
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:/pvEz4xG5vN+CWdogxORZQ

use JSON::XS qw();

#__PACKAGE__->add_columns(value_default => {
#    data_type => "text",
#    default_value => undef,
#    is_nullable => 1,
#    size => undef,
#    accessor => '_value_default',
#});

__PACKAGE__->resultset_class('Zaaksysteem::DB::ResultSet::ZaaktypeKenmerken');

__PACKAGE__->load_components(
    "+Zaaksysteem::DB::Component::ZaaktypeKenmerken",
    "+Zaaksysteem::Helper::ToJSON",
    __PACKAGE__->load_components()
);

__PACKAGE__->belongs_to(
  "bibliotheek_kenmerken_id",
  "Zaaksysteem::Schema::BibliotheekKenmerken",
  { id => "bibliotheek_kenmerken_id" },
  { join_type   => 'left' },
);

__PACKAGE__->belongs_to(
  "library_attribute",
  "Zaaksysteem::Schema::BibliotheekKenmerken",
  { id => "bibliotheek_kenmerken_id" },
);

__PACKAGE__->belongs_to(
  "zaaktype_sjablonen",
  "Zaaksysteem::Schema::ZaaktypeSjablonen",
  { 'zaaktype_node_id' => "zaaktype_node_id" },
  { is_foreign_key_constraint => 0 },
);

__PACKAGE__->belongs_to(
  "zaaktype_node",
  "Zaaksysteem::Schema::ZaaktypeNode",
  { 'id' => "zaaktype_node_id" },
);

__PACKAGE__->add_columns("+label", { is_serializable => 1});

__PACKAGE__->inflate_column('object_metadata', {
    inflate => sub { JSON::XS->new->decode(shift // '{}') },
    deflate => sub { JSON::XS->new->encode(shift // {}) },
});

__PACKAGE__->inflate_column('properties', {
    inflate => sub { JSON::XS->new->decode(shift // '{}') },
    deflate => sub { JSON::XS->new->encode(shift // {}) },
});

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

