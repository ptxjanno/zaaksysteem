use utf8;
package Zaaksysteem::Schema::Transaction;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::Transaction

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<transaction>

=cut

__PACKAGE__->table("transaction");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'transaction_id_seq'

=head2 interface_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 external_transaction_id

  data_type: 'varchar'
  is_nullable: 1
  size: 250

=head2 input_data

  data_type: 'text'
  is_nullable: 1

=head2 input_file

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 automated_retry_count

  data_type: 'integer'
  is_nullable: 1

=head2 date_created

  data_type: 'timestamp'
  default_value: current_timestamp
  is_nullable: 0
  original: {default_value => \"now()"}
  timezone: 'UTC'

=head2 date_last_retry

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 date_next_retry

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 processed

  data_type: 'boolean'
  default_value: false
  is_nullable: 1

=head2 date_deleted

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 error_count

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 direction

  data_type: 'varchar'
  default_value: 'incoming'
  is_nullable: 0
  size: 255

=head2 success_count

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 total_count

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 processor_params

  data_type: 'text'
  is_nullable: 1

=head2 error_fatal

  data_type: 'boolean'
  is_nullable: 1

=head2 preview_data

  data_type: 'text'
  default_value: '{}'
  is_nullable: 0

=head2 error_message

  data_type: 'text'
  is_nullable: 1

=head2 text_vector

  data_type: 'tsvector'
  is_nullable: 1

=head2 uuid

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 0
  size: 16

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "transaction_id_seq",
  },
  "interface_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "external_transaction_id",
  { data_type => "varchar", is_nullable => 1, size => 250 },
  "input_data",
  { data_type => "text", is_nullable => 1 },
  "input_file",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "automated_retry_count",
  { data_type => "integer", is_nullable => 1 },
  "date_created",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 0,
    original      => { default_value => \"now()" },
    timezone      => "UTC",
  },
  "date_last_retry",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "date_next_retry",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "processed",
  { data_type => "boolean", default_value => \"false", is_nullable => 1 },
  "date_deleted",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "error_count",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "direction",
  {
    data_type => "varchar",
    default_value => "incoming",
    is_nullable => 0,
    size => 255,
  },
  "success_count",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "total_count",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "processor_params",
  { data_type => "text", is_nullable => 1 },
  "error_fatal",
  { data_type => "boolean", is_nullable => 1 },
  "preview_data",
  { data_type => "text", default_value => "{}", is_nullable => 0 },
  "error_message",
  { data_type => "text", is_nullable => 1 },
  "text_vector",
  { data_type => "tsvector", is_nullable => 1 },
  "uuid",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 0,
    size => 16,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 input_file

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Filestore>

=cut

__PACKAGE__->belongs_to(
  "input_file",
  "Zaaksysteem::Schema::Filestore",
  { id => "input_file" },
);

=head2 interface_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Interface>

=cut

__PACKAGE__->belongs_to(
  "interface_id",
  "Zaaksysteem::Schema::Interface",
  { id => "interface_id" },
);

=head2 transaction_records

Type: has_many

Related object: L<Zaaksysteem::Schema::TransactionRecord>

=cut

__PACKAGE__->has_many(
  "transaction_records",
  "Zaaksysteem::Schema::TransactionRecord",
  { "foreign.transaction_id" => "self.id" },
  undef,
);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-28 09:26:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:/VDdvddkC/uP1orT/mpHfw

use JSON::XS qw();

__PACKAGE__->belongs_to(
  "interface",
  "Zaaksysteem::Schema::Interface",
  { id => "interface_id" },
  { join_type   => 'left' },
);

__PACKAGE__->belongs_to(
  "input_file",
  "Zaaksysteem::Schema::Filestore",
  { id => "input_file" },
  { join_type   => 'left' },
);

__PACKAGE__->has_many(
  "records",
  "Zaaksysteem::Schema::TransactionRecord",
  { 'foreign.transaction_id' => 'self.id' },
);

__PACKAGE__->resultset_class('Zaaksysteem::Backend::Sysin::Transaction::ResultSet');

__PACKAGE__->load_components(qw/
    +Zaaksysteem::Backend::Sysin::Transaction::Component
    +Zaaksysteem::Helper::ToJSON
/);

__PACKAGE__->add_columns('date_created',
    { %{ __PACKAGE__->column_info('date_created') },
    set_on_create => 1,
});

__PACKAGE__->inflate_column('processor_params', {
    inflate => sub { JSON::XS->new->decode(shift // '{}') },
    deflate => sub { JSON::XS->new->allow_blessed->convert_blessed->encode(shift // {}) },
});

__PACKAGE__->inflate_column('preview_data', {
    inflate => sub { JSON::XS->new->decode(shift // '{}') },
    deflate => sub { JSON::XS->new->allow_blessed->convert_blessed->encode(shift // {}) },
});

__PACKAGE__->inflate_column('text_vector', {
    inflate => sub { split(' ', (shift() || '')); },
    ### Turn an array in a space seperated string, only when the words are at least 3 characters
    ### wide. It also splits the arrays on whitespace
    deflate => sub { join(' ', map({ s/[^\w\s\.\@]//g; split(/\s/); } grep({ length > 2 } @{ shift() || [] }))) },
});

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

