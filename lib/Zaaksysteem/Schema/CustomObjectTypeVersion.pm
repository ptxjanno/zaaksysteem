use utf8;
package Zaaksysteem::Schema::CustomObjectTypeVersion;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::CustomObjectTypeVersion

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<custom_object_type_version>

=cut

__PACKAGE__->table("custom_object_type_version");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'custom_object_type_version_id_seq'

=head2 uuid

  data_type: 'uuid'
  is_nullable: 0
  size: 16

=head2 name

  data_type: 'text'
  is_nullable: 1

=head2 title

  data_type: 'text'
  is_nullable: 1

=head2 status

  data_type: 'enum'
  default_value: 'active'
  extra: {custom_type_name => "custom_object_type_status",list => ["active","inactive"]}
  is_nullable: 0

=head2 version

  data_type: 'integer'
  is_nullable: 0

=head2 custom_field_definition

  data_type: 'jsonb'
  is_nullable: 1

=head2 authorizations

  data_type: 'jsonb'
  is_nullable: 1

=head2 relationship_definition

  data_type: 'jsonb'
  is_nullable: 1

=head2 date_created

  data_type: 'timestamp'
  default_value: current_timestamp
  is_nullable: 1
  original: {default_value => \"now()"}
  timezone: 'UTC'

=head2 last_modified

  data_type: 'timestamp'
  default_value: current_timestamp
  is_nullable: 1
  original: {default_value => \"now()"}
  timezone: 'UTC'

=head2 date_deleted

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 custom_object_type_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "custom_object_type_version_id_seq",
  },
  "uuid",
  { data_type => "uuid", is_nullable => 0, size => 16 },
  "name",
  { data_type => "text", is_nullable => 1 },
  "title",
  { data_type => "text", is_nullable => 1 },
  "status",
  {
    data_type => "enum",
    default_value => "active",
    extra => {
      custom_type_name => "custom_object_type_status",
      list => ["active", "inactive"],
    },
    is_nullable => 0,
  },
  "version",
  { data_type => "integer", is_nullable => 0 },
  "custom_field_definition",
  { data_type => "jsonb", is_nullable => 1 },
  "authorizations",
  { data_type => "jsonb", is_nullable => 1 },
  "relationship_definition",
  { data_type => "jsonb", is_nullable => 1 },
  "date_created",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 1,
    original      => { default_value => \"now()" },
    timezone      => "UTC",
  },
  "last_modified",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 1,
    original      => { default_value => \"now()" },
    timezone      => "UTC",
  },
  "date_deleted",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "custom_object_type_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<custom_object_type_version_uuid_key>

=over 4

=item * L</uuid>

=back

=cut

__PACKAGE__->add_unique_constraint("custom_object_type_version_uuid_key", ["uuid"]);

=head1 RELATIONS

=head2 custom_object_type_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::CustomObjectType>

=cut

__PACKAGE__->belongs_to(
  "custom_object_type_id",
  "Zaaksysteem::Schema::CustomObjectType",
  { id => "custom_object_type_id" },
);

=head2 custom_object_types

Type: has_many

Related object: L<Zaaksysteem::Schema::CustomObjectType>

=cut

__PACKAGE__->has_many(
  "custom_object_types",
  "Zaaksysteem::Schema::CustomObjectType",
  { "foreign.custom_object_type_version_id" => "self.id" },
  undef,
);

=head2 custom_object_versions

Type: has_many

Related object: L<Zaaksysteem::Schema::CustomObjectVersion>

=cut

__PACKAGE__->has_many(
  "custom_object_versions",
  "Zaaksysteem::Schema::CustomObjectVersion",
  { "foreign.custom_object_type_version_id" => "self.id" },
  undef,
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-03-17 12:57:57
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:PkLJPyZdkIaWxdc273z0FQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2020, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut