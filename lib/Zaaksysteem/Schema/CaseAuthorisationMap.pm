use utf8;
package Zaaksysteem::Schema::CaseAuthorisationMap;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::CaseAuthorisationMap

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<case_authorisation_map>

=cut

__PACKAGE__->table("case_authorisation_map");

=head1 ACCESSORS

=head2 key

  data_type: 'text'
  is_nullable: 0

=head2 legacy_key

  data_type: 'text'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "key",
  { data_type => "text", is_nullable => 0 },
  "legacy_key",
  { data_type => "text", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</key>

=item * L</legacy_key>

=back

=cut

__PACKAGE__->set_primary_key("key", "legacy_key");

=head1 UNIQUE CONSTRAINTS

=head2 C<case_authorisation_map_key_key>

=over 4

=item * L</key>

=back

=cut

__PACKAGE__->add_unique_constraint("case_authorisation_map_key_key", ["key"]);

=head2 C<case_authorisation_map_legacy_key_key>

=over 4

=item * L</legacy_key>

=back

=cut

__PACKAGE__->add_unique_constraint("case_authorisation_map_legacy_key_key", ["legacy_key"]);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-03-27 09:48:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:nOupVT7hlF81DThzOASY8w


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
