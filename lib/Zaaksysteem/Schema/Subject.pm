use utf8;
package Zaaksysteem::Schema::Subject;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::Subject

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<subject>

=cut

__PACKAGE__->table("subject");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'subject_id_seq'

=head2 uuid

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 1
  size: 16

=head2 subject_type

  data_type: 'text'
  is_nullable: 0

=head2 properties

  data_type: 'text'
  default_value: '{}'
  is_nullable: 0

=head2 settings

  data_type: 'text'
  default_value: '{}'
  is_nullable: 0

=head2 username

  data_type: 'text'
  is_nullable: 0
  original: {data_type => "varchar"}

=head2 last_modified

  data_type: 'timestamp'
  default_value: current_timestamp
  is_nullable: 1
  original: {default_value => \"now()"}
  timezone: 'UTC'

=head2 role_ids

  data_type: 'integer[]'
  is_nullable: 1

=head2 group_ids

  data_type: 'integer[]'
  is_nullable: 1

=head2 nobody

  data_type: 'boolean'
  default_value: false
  is_nullable: 0

=head2 system

  data_type: 'boolean'
  default_value: false
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "subject_id_seq",
  },
  "uuid",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 1,
    size => 16,
  },
  "subject_type",
  { data_type => "text", is_nullable => 0 },
  "properties",
  { data_type => "text", default_value => "{}", is_nullable => 0 },
  "settings",
  { data_type => "text", default_value => "{}", is_nullable => 0 },
  "username",
  {
    data_type   => "text",
    is_nullable => 0,
    original    => { data_type => "varchar" },
  },
  "last_modified",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 1,
    original      => { default_value => \"now()" },
    timezone      => "UTC",
  },
  "role_ids",
  { data_type => "integer[]", is_nullable => 1 },
  "group_ids",
  { data_type => "integer[]", is_nullable => 1 },
  "nobody",
  { data_type => "boolean", default_value => \"false", is_nullable => 0 },
  "system",
  { data_type => "boolean", default_value => \"false", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<subject_uuid_key>

=over 4

=item * L</uuid>

=back

=cut

__PACKAGE__->add_unique_constraint("subject_uuid_key", ["uuid"]);

=head1 RELATIONS

=head2 alternative_authentication_activation_links

Type: has_many

Related object: L<Zaaksysteem::Schema::AlternativeAuthenticationActivationLink>

=cut

__PACKAGE__->has_many(
  "alternative_authentication_activation_links",
  "Zaaksysteem::Schema::AlternativeAuthenticationActivationLink",
  { "foreign.subject_id" => "self.uuid" },
  undef,
);

=head2 checklist_items

Type: has_many

Related object: L<Zaaksysteem::Schema::ChecklistItem>

=cut

__PACKAGE__->has_many(
  "checklist_items",
  "Zaaksysteem::Schema::ChecklistItem",
  { "foreign.assignee_id" => "self.id" },
  undef,
);

=head2 export_queue_subject_ids

Type: has_many

Related object: L<Zaaksysteem::Schema::ExportQueue>

=cut

__PACKAGE__->has_many(
  "export_queue_subject_ids",
  "Zaaksysteem::Schema::ExportQueue",
  { "foreign.subject_id" => "self.id" },
  undef,
);

=head2 export_queue_subject_uuids

Type: has_many

Related object: L<Zaaksysteem::Schema::ExportQueue>

=cut

__PACKAGE__->has_many(
  "export_queue_subject_uuids",
  "Zaaksysteem::Schema::ExportQueue",
  { "foreign.subject_uuid" => "self.uuid" },
  undef,
);

=head2 gegevensmagazijn_subjectens

Type: has_many

Related object: L<Zaaksysteem::Schema::GegevensmagazijnSubjecten>

=cut

__PACKAGE__->has_many(
  "gegevensmagazijn_subjectens",
  "Zaaksysteem::Schema::GegevensmagazijnSubjecten",
  { "foreign.subject_uuid" => "self.uuid" },
  undef,
);

=head2 object_mutations

Type: has_many

Related object: L<Zaaksysteem::Schema::ObjectMutation>

=cut

__PACKAGE__->has_many(
  "object_mutations",
  "Zaaksysteem::Schema::ObjectMutation",
  { "foreign.subject_id" => "self.id" },
  undef,
);

=head2 session_invitations

Type: has_many

Related object: L<Zaaksysteem::Schema::SessionInvitation>

=cut

__PACKAGE__->has_many(
  "session_invitations",
  "Zaaksysteem::Schema::SessionInvitation",
  { "foreign.subject_id" => "self.uuid" },
  undef,
);

=head2 user_entities

Type: has_many

Related object: L<Zaaksysteem::Schema::UserEntity>

=cut

__PACKAGE__->has_many(
  "user_entities",
  "Zaaksysteem::Schema::UserEntity",
  { "foreign.subject_id" => "self.id" },
  undef,
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-04-10 16:23:54
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:5qV+h1YP+MBroeNsdAP+CQ

__PACKAGE__->resultset_class('Zaaksysteem::Backend::Subject::ResultSet');

use JSON::XS qw();

__PACKAGE__->load_components(
    '+Zaaksysteem::Backend::Subject::Component',
    __PACKAGE__->load_components()
);

__PACKAGE__->inflate_column('properties', {
    inflate => sub { JSON::XS->new->decode(shift // '{}') },
    deflate => sub { JSON::XS->new->encode(shift // {}) },
});

__PACKAGE__->inflate_column('settings', {
    inflate => sub { JSON::XS->new->decode(shift // '{}') },
    deflate => sub { JSON::XS->new->encode(shift // {}) },
});

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

