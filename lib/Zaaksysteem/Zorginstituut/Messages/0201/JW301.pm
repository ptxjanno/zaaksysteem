package Zaaksysteem::Zorginstituut::Messages::0201::JW301;
use Moose;

extends 'Zaaksysteem::Zorginstituut::Message';
with qw(
    Zaaksysteem::Zorginstituut::Roles::301
);

=head1 NAME

Zaaksysteem::Zorginstituut::Messages::0201::JW301 - JW 2.1 301 message

=head1 DESCRIPTION

A sending module for JW301 messages

=head1 SYNOPSIS

    use Zaaksysteem::Zorginstituut::Messages::0201::JW301;


    my $jw301 = Zaaksysteem::Zorginstituut::Messages::0201::JW301->new(
        case => $case
    );

    my $xml = $jw301->encode();

=cut

use BTTW::Tools;
use Zaaksysteem::XML::Compile;

has '+message_version' => ( default => 2 );
has '+message_sub_version' => ( default => 1);
has '+code' => ( default => 436 );


=head1 PRIVATE METHODS and BUILDERS

=head2 _build_encoder

Builds the encoder for this type of message, used by the C<encoder> attribute

=cut

sub _build_encoder {
    my $self = shift;

    return Zaaksysteem::XML::Compile->xml_compile->add_class(
        'Zaaksysteem::XML::Generator::JW::0201'
    )->ijw_0201;

}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
