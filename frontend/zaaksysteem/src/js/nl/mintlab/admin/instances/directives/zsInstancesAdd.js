/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.admin.instances')
		.directive('zsInstancesAdd', [ 'instancesService', 'zsApi', 'formService', 'systemMessageService', function ( instancesService, api, formService, systemMessageService ) {
			
			return {
				require: [ 'zsInstancesAdd', '^zsInstancesView' ],
				controller: [ '$scope', '$attrs', function ( $scope, $attrs ) {
					
					var ctrl = this,
						loading = false,
						config = null,
						zsInstancesView;
					
					function getCustomerId ( ) {
						return zsInstancesView ? zsInstancesView.getCustomerId() : null;
					}
					
					config = angular.copy(instancesService.getControlPanelForm());

					ctrl.link = function ( controllers ) {
						var unwatch,
							templateField = _.find(config.fields, { name: 'template' });

						zsInstancesView = controllers[0];

						unwatch = $scope.$watch(function ( ) {
							return $scope.$eval($attrs.zsInstancesAddTemplates);
						}, function ( templates ) {
							templateField.data.options =
								_.map(templates, function ( tplId ) {
									return {
										value: tplId,
										label: tplId
									};
								});

							if(templates) {
								unwatch();

								if(!templates.length) {
									_.pull(config.fields, templateField);
								}
							}

						});


					};
					
					ctrl.getFormConfig = function ( ) {
						if (zsInstancesView.isPip()) {
							_.remove(config.fields, { name: 'software_version' });
							_.remove(config.fields, { name: 'customer_type' });
						} else {
							var customer_type = _.find(config.fields, { name: 'customer_type' });

							if (zsInstancesView) {
								customer_type.value = zsInstancesView.getCustomerType();
							}
						}

						return config;
					};
					
					ctrl.isLoading = function ( ) {
						return loading;
					};
					
					ctrl.handleFormSubmit = function ( values, closePopup ) {
						var customerId = getCustomerId(),
							vals = _(values)
								.extend({
									owner: customerId
								})
								.value();
						
						loading = true;
						
						zsInstancesView.createInstance(vals)
							.then(function ( ) {
								closePopup();
							})
							['catch'](function ( error ) {
								var form = formService.get(config.name);
								
								if(error && error.type === 'validationexception') {
									form.setValidity(api.getLegacyFormValidations(config.fields, error));
									systemMessageService.emitValidationError();
								} else {
									systemMessageService.emitSaveError();
								}
							})
							['finally'](function ( ) {
								loading = false;
							});
						};
					
					return ctrl;
					
				}],
				controllerAs: 'instancesAdd',
				link: function ( scope, element, attrs, controllers ) {
					
					controllers[0].link(controllers.splice(1));
					
				}
			};
			
		}]);
	
})();
