/*global angular,console*/
(function () {
    'use strict';
    angular.module('Zaaksysteem.admin')
        .controller('nl.mintlab.admin.INavigatorController', [ '$scope', '$window', 'smartHttp', 'translationService', function ($scope, $window, smartHttp, translationService) {

            $scope.loading = false;
            $scope.selectedCasetype = null;

            $scope.settings = $scope.settings || {};
            $scope.bewaartermijnKeys = [];

            function findArchiefnominatie (result) {
                return _.find(_.keys($scope.archiefnominatieOptions), function (key) {
                    return $scope.archiefnominatieOptions[key] === result.existingResult.archiefnominatie;
                });
            }

            $scope.toggleCasetype = function () {
                if ($scope.action && $scope.selectedCasetype) {
                    $scope.collapsed = !$scope.collapsed;
                }
            };

            $scope.post = function (url, data, onSuccess) {
                $scope.loading = true;
                return smartHttp.connect({
                    method: 'POST',
                    url: url,
                    data: data
                })
                    .success(function (response) {
                        $scope.$emit('systemMessage', {
                            type: 'info',
                            content: response.result[0].message
                        });
                        if (onSuccess) {
                            onSuccess(response);
                        }
                    })
                    .error(function (response) {
                        var messages = response.result[0].messages.join(",");
                        $scope.$emit('systemMessage', {
                            type: 'error',
                            content: messages
                        });
                    })
                    ['finally'](function ( ) {
                        $scope.loading = false;
                    });
            };

            $scope.setCasetype = function (code) {
                // clone, so the original settings are preserved when we start changing
                $scope.casetype = angular.copy($scope.casetypes[code]);
            };

            $scope.preset = function (code) {
                $scope.code = code;
                $scope.setCasetype(code);

                $scope.matches = _.filter($scope.existingCasetypes, function (element) {
                    return element.code === $scope.casetype.general['node.code'];
                });

                $scope.action = $scope.matches.length ? 'update' : 'create';

                if ($scope.action === 'create') {
                    $scope.casetype.general.modify_title = true;
                }
            };

            $scope.init = function (code) {
                $scope.preset(code);

                if ($scope.matches.length === 1) {
                    $scope.selectedCasetypeId = $scope.matches[0].id;
                    $scope.setSelectedCasetype();
                } else {
                    $scope.restoreSettings($scope.code);
                }

                $scope.bewaartermijnKeys = _.keys($scope.bewaartermijnOptions);
            };

            $scope.restoreSettings = function (code) {
                var templateCasetypes = $scope.settings.templateCasetypes;

                if (templateCasetypes && templateCasetypes[code]) {
                    // restore settings
                    var saved = $scope.settings.templateCasetypes[code];
                    if (saved && saved.id) {
                        $scope.selectedCasetypeId = saved.id;
                        $scope.templateCasetype = {
                            id: saved.id,
                            searchable_object_label: saved.name
                        };
                        $scope.setSelectedCasetype();
                    }
                }
            };

            $scope.saveCategory = function (bibliotheek_categorie_id) {
                if (!$scope.settings.categories) {
                    $scope.settings.categories = {};
                }

                if ($scope.settings.categories[$scope.code] !== bibliotheek_categorie_id) {
                    $scope.settings.categories[$scope.code] = bibliotheek_categorie_id;

                    _.each($scope.casetype.documents, function (document) {
                        if (!document.existingKenmerk && !document.enabled) {
                            document.bibliotheek_categorie_id = bibliotheek_categorie_id;
                        }
                    });

                    $scope.saveSettings();
                }
            };

            $scope.validate = function () {
                var documentError = _.find($scope.casetype.documents, function (document) {
                    return document.enabled && (
                        (document.kenmerk_action === 'create' && !document.bibliotheek_categorie_id) ||
                        (document.kenmerk_action === 'update' && !document.bibliotheek_kenmerken_id)
                    );
                });
                return $scope.action && $scope.selectedCasetype && $scope.casetype.general['definitie.grondslag'] && !documentError;
            };

            $scope.prefillResults = function () {
                var statussen = $scope.selectedCasetype.statussen,
                    afhandelfase = _.max(_.keys(statussen));

                _.each($scope.casetype.results, function (result) {
                    // keep the original so we know what inavigator meant
                    result.inavigatorResult = result.resultaat;

                    // try to map the resulttype on a local type.
                    result.zaaksysteemResultaattypeFound = _.find($scope.resultTypes, function (type) {
                        return type.toLowerCase() === result.resultaat.toLowerCase();
                    });

                    if (result.zaaksysteemResultaattypeFound) {
                        result.resultaat = result.zaaksysteemResultaattypeFound;
                    }
                });
            };

            $scope.prefillDocumentPhases = function () {
                var statussen = $scope.selectedCasetype.statussen,
                    afhandelfase = _.max(_.keys(statussen)) - 1;

                _.map($scope.casetype.documents, function (document) {
                    var foundKenmerk, status = _.find(statussen, function (status) {
                        var kenmerk = _.find(status.elementen.kenmerken, function (kenmerk) {
                            return kenmerk.naam === document.label && kenmerk.type === 'file';
                        });
                        if (kenmerk) {
                            foundKenmerk = kenmerk;
                            return true;
                        }
                        return false;
                    });

                    if (foundKenmerk) {
                        document.value_mandatory = !!foundKenmerk.value_mandatory;
                        document.publish_pip = !!foundKenmerk.pip;
                        document.publish_website = !!foundKenmerk.publish_public;

                        document.phase = $scope.statusOptions[status.definitie.status - 1];
                        document.existingKenmerk = foundKenmerk;
                        document.action = 'update';
                    } else {
                        document.phase = $scope.statusOptions[afhandelfase];
                        document.action = 'create';
                        document.kenmerk_action = 'create';
                        // default all required checkbox should be off
                        document.value_mandatory = false;
                        // restore saved setting
                        $scope.setDocumentCategory(document);
                    }
                });
            };

            $scope.setDocumentCategory = function (document) {
                var id;

                if ($scope.settings.categories && $scope.settings.categories[$scope.code] && $scope.settings.categories[$scope.code].id) {
                    id = $scope.settings.categories[$scope.code].id;

                    document.bibliotheek_categorie_id = _.find($scope.existingCategories, function (category) {
                        return String(category.id) === id;
                    });
                }
            };

            $scope.prefillChecklistPhases = function () {
                var statussen = $scope.selectedCasetype.statussen,
                    afhandelfase = _.max(_.keys(statussen)) - 1;

                _.map($scope.casetype.checklistitems, function (checklistItem) {
                    var foundItem, status = _.find(statussen, function (status) {
                        var existing = _.find(status.elementen.checklists, function (item) {
                            return item.external_reference === checklistItem.id;
                        });
                        if (existing) {
                            foundItem = existing;
                            return true;
                        }
                        return false;
                    });

                    if (foundItem) {
                        checklistItem.existing = foundItem;
                        checklistItem.phase = $scope.statusOptions[status.definitie.status - 1];
                    } else {
                        checklistItem.phase = $scope.statusOptions[afhandelfase];
                    }
                });
            };


            $scope.openPreview = function ($event) {
                $event.stopPropagation();
                $scope.openPopup();
            };

            // given a casetype, setup the controls to reflect the settings
            $scope.loadCasetype = function (result) {
                $scope.selectedCasetype = result.casetype;

                // angular select boxes work different with objects, so
                // prep an array.
                // needs to happen before prefills.
                $scope.statusOptions = _.map(_.keys($scope.selectedCasetype.statussen), function (key) {
                    var status = $scope.selectedCasetype.statussen[key];
                    return {
                        label: status.definitie.fase,
                        value: status.definitie.status
                    };
                });

                $scope.existingCasetypes = result.existing_casetypes;
                $scope.existingKenmerken = result.existing_kenmerken;
                $scope.existingCategories = result.existing_categories;
                $scope.preset($scope.code);

                $scope.prefillDocumentPhases();
                $scope.prefillChecklistPhases();
                $scope.prefillResults();

                $scope.inited = true;
            };

            $scope.setSelectedCasetype = function () {
                return smartHttp.connect({
                    method: 'GET',
                    url: '/api/casetype/inavigator/casetype_info/' + $scope.selectedCasetypeId
                })
                    .success(function (response) {
                        $scope.loadCasetype(response.result[0]);
                    })
                    .error(function (response) {
                        var messages = response.result[0].messages.join(",");
                        $scope.$emit('systemMessage', {
                            type: 'error',
                            content: messages
                        });
                    });
            };

            $scope.importCasetype = function (event) {
                event.stopPropagation();

                return $scope.post('/api/casetype/inavigator/do_import', {
                    action: $scope.action,
                    settings: $scope.casetype,
                    casetype_id: $scope.selectedCasetype.zaaktype.id
                }, function (response) {
                    $scope.loadCasetype(response.result[0]);
                    $scope.imported = true;
                    $scope.closePopup();
                });
            };

            $scope.saveSettings = function () {
                var template = {};

                template[$scope.casetype.general['node.code']] = {
                    id: $scope.selectedCasetypeId,
                    name: $scope.selectedCasetype.node.titel
                };

                return $scope.post('/api/casetype/inavigator/save_settings', {
                    templateCasetypes: template,
                    categories: $scope.settings.categories
                });
            };

            $scope.$watch('templateCasetype', function ( nwVal/*, oldVal, scope*/ ) {
                if (nwVal && nwVal.id) {
                    $scope.selectedCasetypeId = nwVal.id;
                    $scope.setSelectedCasetype().then(function () {
                        $scope.saveSettings();
                    });
                } else {
                    $scope.selectedCasetype = null;
                }
            });

            $scope.$watch('selectedCasetypeId', function (nwVal /*, oldVal, scope */) {
                if ($scope.inited) {
                    $scope.collapsed = false;
                }
            });

        }]);


}());
