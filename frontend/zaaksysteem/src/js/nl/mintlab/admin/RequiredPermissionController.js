/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.admin')
		.controller('nl.mintlab.admin.RequiredPermissionController', [ '$scope', 'translationService', 'smartHttp', function ( $scope, translationService, smartHttp ) {
			
			$scope.units = [];
			$scope.roles = [];
			
			$scope.loading = true;
			
			function throwDataError ( ) {
				$scope.$emit('systemMessage', {
					type: 'error',
					content: translationService.get('Er ging iets fout bij het ophalen van de organisatie-units. Probeer het later opnieuw.')
				});
			}
			
			function getUnits ( ) {
				smartHttp.connect({
					method: 'GET',
					url: '/api/authorization/org_unit?zapi_no_pager=1'
				})
					.success(function ( response ) {
						$scope.units = response.result;
					})
					.error(function ( ) {
						throwDataError();	
					});
			}
			
			$scope.add = function ( ) {
				var units = $scope.selectedUnits || [],
					unit = $scope.getUnits()[0],
					unitId = unit.org_unit_id,
					roleId = $scope.getRolesForUnit(unit)[0].role_id;
					
				units.push({
					org_unit_id: unitId,
					role_id: roleId
				});
				
				$scope.selectedUnits = units;
			};
			
			$scope.remove = function ( unit ) {
				var index = _.indexOf($scope.selectedUnits, unit);
				if(index !== -1) {
					$scope.selectedUnits.splice(index, 1);
				}
			};
			
			$scope.getUnits = function ( ) {
				return _.filter($scope.units, function ( unit ) {
					return unit.depth > 0;
				});
			};
			
			$scope.getRolesForUnit = function ( unit ) {
				var unitObj = _.find($scope.units, function ( u ) {
					return u.org_unit_id === unit.org_unit_id;
				});
				return unitObj ? unitObj.roles : [];
			};
			
			$scope.getRoleName = function ( role ) {
				return (!role.system ? '* ' : '') + role.name;
			};
			
			$scope.getJson = function ( ) {
				if ($scope.selectedUnits && $scope.selectedUnits.length) {
					return JSON.stringify({
						selectedUnits: $scope.selectedUnits
					});
				} else {
					return JSON.stringify({});
				}
			};
			
			$scope.handleUnitChange = function ( unit ) {
				var roleId = unit.role_id,
					roles = $scope.getRolesForUnit(unit),
					roleIsAvailable = !!_.find(roles, function ( r ) {
						return roleId === r.role_id;
					});
				
				if(!roleIsAvailable) {
					unit.role_id = roles[0].role_id;
				}
				
			};
			
			getUnits();
			
		}]);
	
})();
