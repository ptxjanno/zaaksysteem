(function ( ) {
	
	angular.module('Zaaksysteem.object.view')
		.directive('zsObjectRelation', [ function ( ) {
			
			var DEFAULT_LIMIT = 5;
			
			return {
				require: [ 'zsObjectRelation', '^zsObjectRelationList' ], 
				controller: [ '$scope', '$element', '$attrs', function ( $scope, $element, $attrs ) {
					
					var ctrl = this,
						limit = DEFAULT_LIMIT,
						zsObjectRelationList,
						objectType = $scope.$eval($attrs.objectType),
						objectTypeName = objectType.id;
						
					function getObjectLength ( ) {
						var objects = ctrl.getRelatedObjects(),
							length = 0;
							
						if(objects) {
							length = objects.length;
						}
						return length;
					}
					
					ctrl.link = function ( controllers ) {
						zsObjectRelationList = controllers[0];
					};
					
					ctrl.getRelatedObjects = function ( ) {
						return zsObjectRelationList ? zsObjectRelationList.getRelatedObjects(objectType)  : null; 
					};
					
					ctrl.hasMore = function ( ) {
						return limit < getObjectLength();
					};
					
					ctrl.toggleShowMore = function ( ) {
						if(ctrl.hasLimit()) {
							limit = -1;
						} else {
							limit = DEFAULT_LIMIT;
						}
					};
					
					ctrl.getLimit = function ( ) {
						return ctrl.hasLimit() ? limit : getObjectLength();	
					};
					
					ctrl.hasLimit = function ( ) {
						return limit !== -1;	
					};
					
					
					return ctrl;
					
				}],
				controllerAs: 'objectRelation',
				link: function ( scope, element, attrs, controllers ) {
					controllers[0].link(controllers.slice(1));
				},
				templateUrl: '/html/object/view/relation-list-item.html'
				
			};
			
		}]);
	
})();
