/*global angular*/
(function() {
    angular.module('Zaaksysteem.case').controller(
        'nl.mintlab.case.FormBootstrapController',
        [
            '$scope',
            'contextualActionService',
            function ($scope, contextualActionService) {
                $scope.openFormBootstrapPopup = function() {
                    contextualActionService.openAction(
                        contextualActionService.findActionByName('zaak'),
                        {
                            casetypeId: $scope.casetype_id,
                            requestor: {
                                type: $scope.requestor_type,
                                label: $scope.requestor_name,
                                data: {
                                    id: $scope.identifier,
                                    uuid: $scope.requestor_id
                                }
                            }
                        }
                    );
                };
            }
        ]
    );
})();
