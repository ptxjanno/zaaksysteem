/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.case')
		.directive('zsCaseWebformFieldGroup', [ function ( ) {
			
			return {
				scope: true,
				require: [ 'zsCaseWebformFieldGroup', '^zsCaseWebformRuleManager', '^zsCaseWebform' ],
				controller: [ '$attrs', function ( $attrs ) {
					
					var ctrl = this,
						zsCaseWebformRuleManager,
						zsCaseWebform;
					
					ctrl.link = function ( controllers ) {
						zsCaseWebformRuleManager = controllers[0];
						zsCaseWebform = controllers[1];
					};
					
					ctrl.getGroupId = function ( ) {
						return parseInt($attrs.fieldGroupId, 10);
					};
					
					ctrl.isVisible = function ( ) {
						var childrenVisible,
							children,
							groupVisible,
							visible;
							
						groupVisible = zsCaseWebformRuleManager && zsCaseWebformRuleManager.isFieldVisible(ctrl.getGroupId());
							
						if(groupVisible) {
							children = _.filter(zsCaseWebform.getControls(), function ( field ) {
								return field.getGroupId() === ctrl.getGroupId();
							});
							
							childrenVisible = !children.length || _.some(children, function ( child ) {
								return child.isVisible();
							});
						}
						
						visible = groupVisible && childrenVisible;
						
						return visible;
					};
					
					return ctrl;
					
				}],
				controllerAs: 'caseWebformFieldGroup',
				link: function ( scope, element, attrs, controllers ) {
					
					controllers[0].link(controllers.slice(1));
					
				}
			};
			
		}]);
	
})();
