/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.case')
		.directive('zsCaseRelationObjectList', [ 'translationService', 'objectRelationService', 'systemMessageService', 'capitalizeFilter', function ( translationService, objectRelationService, systemMessageService, capitalizeFilter ) {
			
			return {
				require: [ 'zsCaseRelationObjectList', '^zsCaseView' ],
				controller: [ function ( ) {
					
					var ctrl = this,
						zsCaseView;
					
					ctrl.setControls = function ( ) {
						zsCaseView = arguments[0];
					};
					
					ctrl.getList = function ( ) {
						var caseObj,
							relations;
							
						if(zsCaseView && zsCaseView.isLoaded()) {
							caseObj = zsCaseView.getCase();
							relations = _.filter(caseObj.related_objects, function ( object ) {
								return [ 'scheduled_job', 'case' ].indexOf(object.related_object_type) === -1;
							});
						}
						
						return relations;
					};
					
					ctrl.getObjectLabel = function ( relation ) {
						return relation.related_object.label;
					};
					
					ctrl.getObjectTypeName = function ( relation ) {
						return relation.related_object.type;
					};
					
					ctrl.getObjectTypeLabel = function ( relation ) {
						var objTypeName = ctrl.getObjectTypeName(relation),
							label;
						
						switch(objTypeName) {
							default:
							label = capitalizeFilter(objTypeName);
							break;
							
							case 'case':
							label = translationService.get('Zaak');
							break;
						}
						
						return label;
					};
					
					ctrl.handleObjectSelect = function ( $object ) {
						var caseObj = zsCaseView.getCase(),
							obj = $object.object,
							objRelation = objectRelationService.createRelationTo(obj),
							caseRelation = objectRelationService.createRelationTo(caseObj);
							
						if(objectRelationService.isRelated(caseObj, obj)) {
							systemMessageService.emitError('Dit object is al gerelateerd aan deze zaak.');
						} else if(obj.type === 'case' && obj.id === caseObj.id) {
							systemMessageService.emitError('U kunt de zaak niet aan zichzelf relateren.');
						} else {
							
							// FIXME: doesn't work at all
							// doesn't work for cases, so we use the object-to-relate
							objectRelationService.addRelation(obj, caseRelation)
								['catch'](function ( ) {
									_.pull(caseObj.related_objects, objRelation);
								});
							
							caseObj.related_objects.push(objRelation);
							
							ctrl.newRelatedObject = null;
							
						}
					};
					
					return ctrl;
					
				}],
				controllerAs: 'caseRelationObjectList',
				link: function ( scope, element, attrs, controllers ) {
					
					controllers[0].setControls.apply(controllers[0], controllers.slice(1));
					
				}
			};
			
		}]);
	
})();
