/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.case')
		.directive('zsCaseActionList', [ '$http', 'translationService', function ( $http, translationService ) {
			
			return {
				require: [ 'zsCaseActionList', '^zsCaseSidebar', '^zsCasePhaseView', '^zsCaseView' ],
				controller: [ '$scope', function ( $scope ) {
					
					var ctrl = this,
						zsCaseSidebar,
						zsCasePhaseView,
						zsCaseView;
					
					ctrl.setControllers = function ( ) {
						zsCaseSidebar = arguments[0];
						zsCasePhaseView = arguments[1];
						zsCaseView = arguments[2];
					};
					
					ctrl.getActions = function ( ) {
						return zsCaseSidebar.getActions();
					};
					
					ctrl.updateItem = function ( item ) {
						
						$http({
							method: 'POST',
							url: '/zaak/' + zsCaseView.getCaseId() + '/action/update',
							data: {
								id: item.id,
								automatic: item.automatic
							}
						})
							.success(function ( data ) {
								
								var itemData = data.result[0];
								
								for(var key in itemData) {
									item[key] = itemData[key];
								}
								
							})
							.error(function ( /*data*/ ) {
								item.automatic = !item.automatic;
								
								$scope.$emit('systemMessage', {
									type: 'error',
									content: translationService.get('Actie kon niet worden gewijzigd.')
								});
							});
							
					};
					
					ctrl.handleCheckboxClick = function ( event ) {
						event.stopPropagation();
					};
					
					ctrl.resetAction = function ( event, item ) {
						var wasTainted = item.tainted;
						
						item.tainted = false;
						
						$http({
							method: 'POST',
							url: '/zaak/' + zsCaseView.getCaseId() + '/action/untaint',
							data: {
								id: item.id
							}
						})
							.success(function ( data ) {
								var itemData = data.result[0];
								
								for(var key in itemData) {
									item[key] = itemData[key];
								}
								
								zsCaseView.reloadData();
							})
							.error(function ( /*data*/ ) {
								item.tainted = wasTainted;
								$scope.$emit('systemMessage', {
									type: 'error',
									content: translationService.get('Actie kon niet hersteld worden')
								});
							});
						
						event.stopPropagation();
					};
					
					ctrl.isDisabled = function ( action ) {
						var disabled;
						
						disabled =
							ctrl.isClosed()
							|| (ctrl.isLastMilestone() && action.data && action.data.relatie_type === 'deelzaak')
							|| (!!action.data.interface_id);

						return disabled;
					};
					
					ctrl.isClosed = function ( ) {
						return zsCaseSidebar.isClosed();
					};
					
					ctrl.isLastMilestone = function ( ) {
						return zsCasePhaseView.isLastMilestone();
					};
					
					ctrl.getActionInfoTemplateType = function (action ) {
						var type;
						
						switch(action.type) {
							case 'template':
							case 'email':
							type = action.type;
							break;
							
							default:
							type = 'other';
							break;
						}
						return type;
					};
					
					return ctrl;
					
				}],
				controllerAs: 'caseActionList',
				link: function ( scope, element, attrs, controllers ) {
					
					var zsCaseActionList = controllers[0];
					
					zsCaseActionList.setControllers.apply(zsCaseActionList, controllers.slice(1));
					
				}
			};
			
		}]);
	
})();
