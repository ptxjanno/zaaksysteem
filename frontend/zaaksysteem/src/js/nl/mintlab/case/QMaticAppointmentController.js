 /*global angular,fetch*/
(function () {
    "use strict";

    angular.module('Zaaksysteem.case')
        .controller('nl.mintlab.case.QMaticAppointmentController', [ '$scope', 'smartHttp', 'translationService', 'dateFilter', function ($scope, smartHttp, translationService, dateFilter ) {

            var safeApply = window.zsFetch('nl.mintlab.utils.safeApply');

            $scope.dates = [];
            $scope.times = [];

            function getDates ( ) {
                smartHttp.connect({
                    method: 'GET',
                    url: '/api/qmatic/index',
                    params: {
                        action: 'getAvailableAppointmentDays',
                        productLinkID: $scope.productLinkID,
                        zapi_no_pager: 1
                    }
                })
                    .success(function ( response ) {
                        $scope.dates = response.result;
                    })
                    .error(function ( response ) {
                        broadcastError(response);
                    });
            }

            function getTimes ( ) {
                smartHttp.connect({
                    method: 'GET',
                    url: '/api/qmatic/index',
                    params: {
                        action: 'getAvailableAppointmentTimes',
                        productLinkID: $scope.productLinkID,
                        appDate: $scope.appointmentDate,
                        zapi_no_pager: 1
                    }
                })
                    .success(function ( response ) {
                        $scope.times = response.result;
                    })
                    .error(function ( response ) {
                        broadcastError(response);
                    });
            }
            
            function broadcastError ( response ) {
                var message = {
                        type: 'error',
                        content: translationService.get('Er ging iets fout bij het ophalen van de beschikbare data. Probeer het later opnieuw.')
                    },
                    type = response.result && response.result[0] ? response.result[0].type : null;
                
                if(type === 'qmatic/config_missing') {
                    message.content = translationService.get('Er is nog geen QMatic koppeling geconfigureerd.');
                }
                
                $scope.$emit('systemMessage', message);
            }

            $scope.setAppointmentTime = function ( time ) {
                $scope.appointmentTime = time;
            };

            $scope.setAppointmentDate = function ( date ) {
                $scope.appointmentDate = date;
            };

            $scope.deleteAppointment = function () {
                smartHttp.connect({
                    method: 'POST',
                    url: '/api/qmatic/index',
                    params: {
                        action: 'deleteAppointment',
                        appointmentId: $scope.appointmentId
                    }
                })
                    .success(function ( /*response*/ ) {
                        $scope.confirmedAppointmentDate = null;
                        $scope.confirmedAppointmentTime = null;

                        $scope.$emit('systemMessage', {
                            type: 'info',
                            content: translationService.get('Uw afspraak is verwijderd')
                        });
                        $scope.appointment = null;
                        $scope.closePopup();
                    })
                    .error(function ( /*response*/ ) {

                    });

            };

            $scope.book = function ( ) {
                smartHttp.connect({
                    method: 'POST',
                    url: '/api/qmatic/index',
                    params: {
                        action: 'bookAppointment',
                        appDate: $scope.appointmentDate,
                        appTime: $scope.appointmentTime,
                        productLinkID: $scope.productLinkID,
                        aanvrager: $scope.aanvrager,
                        previous_appointment_id: $scope.appointmentId
                    }
                })
                    .success(function ( response ) {
                        $scope.confirmedAppointmentDate = $scope.appointmentDate;
                        $scope.confirmedAppointmentTime = $scope.appointmentTime;
                        $scope.$emit('systemMessage', {
                            type: 'info',
                            content: translationService.get('Uw afspraak is geboekt op ' + dateFilter($scope.appointmentDate, 'longDate') + ' om ' + dateFilter($scope.appointmentTime, 'shortTime'))
                        });
                        $scope.appointment = [$scope.confirmedAppointmentDate, 
                            $scope.confirmedAppointmentTime, response.result[0].appointmentId].join(';');

                        $scope.closePopup();
                    })
                    .error(function ( /*response*/ ) {

                    });
            };
            
            $scope.getTimeLabel = function ( time, appointmentLength ) {
                var appointmentMs = appointmentLength * 1000 * 60,
                    ms = new Date(time).getTime(),
                    from,
                    to;
                    
                if(isNaN(ms)) { 
                    // we're dealing with an old browser here,
                    // unable to parse iso formats
                    ms = new Date(time.replace(/-/g, '/').replace('00.000', '').replace(/(.*):/, '$1')).getTime();
                }
                    
                from = dateFilter(ms, 'HH:mm');
                to = dateFilter(ms + appointmentMs, 'HH:mm');
                    
                return from + ' - ' + to;
            };

            $scope.$on('popupopen', function ( /*event*/ ) {
                
                $scope.appointmentDate = null;
                $scope.appointmentTime = null;
                
                safeApply($scope, function ( ) {
                    if ($scope.productLinkID) {
                        getDates();
                    } else {
                        $scope.$emit('systemMessage', {
                            type: 'error',
                            content: translationService.get('Er is iets misgegaan. Neem contact op met de gemeente.')
                        });
                        $scope.closePopup();
                    }
                });
            });

            $scope.$watch('appointmentDate', function ( ) {
                if($scope.appointmentDate) {
                    getTimes();
                } else {
                    $scope.times = [];
                }
            });

            $scope.$watch('appointment', function ( ) {
                var parts = ($scope.appointment || '').split(';');

                $scope.confirmedAppointmentDate = parts[0];
                $scope.confirmedAppointmentTime = parts[1];
                $scope.appointmentId = parts[2];
            });

        }]);
})();
