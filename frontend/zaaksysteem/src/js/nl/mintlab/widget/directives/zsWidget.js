/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.widget')
		.directive('zsWidget', [ function ( ) {
			
			return {
				templateUrl: '/html/widget/widget.html',
				transclude: true,
				scope: true,
				link: function ( scope, element, attrs ) {

					function setTitle ( ) {
						scope.widgetTitle = attrs.zsWidgetTitle;
					}

					if (attrs.zsWidgetTitle === undefined) {
						setTitle();
					} else {
						attrs.$observe('zsWidgetTitle', setTitle);	
					}
				}
			};
			
		}]);
	
})();
