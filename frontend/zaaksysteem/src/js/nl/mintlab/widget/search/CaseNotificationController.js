/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.widget.search')
		.controller('nl.mintlab.widget.search.CaseNotificationController', [ '$scope', function ( $scope ) {
			
			$scope.getVal = function ( ) {
				return (
					parseInt($scope.item.values['case.num_unaccepted_files'], 10) +
					parseInt($scope.item.values['case.num_unaccepted_updates'], 10) +
					parseInt($scope.item.values['case.num_unread_communication'], 10)
				);
			};
		}]);
	
})();
