/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.widget.search')
		.controller('nl.mintlab.widget.search.FilterPositionController', [ '$scope', 'translationService', 'objectService', 'organisationalUnitService', function ( $scope, translationService, objectService, organisationalUnitService ) {
			
			var capabilities = [
					{
						label: translationService.get('wijzigen'),
						capability: 'write'
					},
					{
						label: translationService.get('beheren'),
						capability: 'manage'
					}
				];
				
			function getRule ( capability ) {
				var entityId = $scope.position.entity_id,
					entityType = $scope.position.entity_type;
				
				return _.find($scope.filter.security_rules, function ( rule ) {
					return rule.entity_id === entityId && rule.entity_type === entityType && rule.capability === capability;
				});
			}
			
			$scope.getRoleLabel = function ( ) {
				var ids = $scope.position.entity_id.split('|'),
					orgUnitId = ids[0],
					roleId = ids[1],
					orgUnit = organisationalUnitService.getOrgUnitById(orgUnitId) || {},
					role = _.find(orgUnit.roles, { 'role_id': roleId }),
					label = role ? role.name : '';
					
				return label;
			};
			
			$scope.getOrgLabel = function ( ) {
				var orgUnitId = $scope.position.entity_id.split('|')[0],
					orgUnit = organisationalUnitService.getOrgUnitById(orgUnitId),
					label = orgUnit ? orgUnit.name : '';
					
				return label;	
			};
			
			$scope.getCapabilities = function ( ) {
				return capabilities;	
			};
			
			$scope.hasCapability = function ( capability ) {
				return !!getRule(capability);
			};
			
			$scope.toggleCapability = function ( capability ) {
				var entityId = $scope.position.entity_id,
					entityType = $scope.position.entity_type,
					rule = getRule(capability);
				
				if(rule) {
					objectService.removeSecurityRule($scope.filter, rule);
				} else {
					rule = objectService.createSecurityRule(entityId, entityType, capability);
					objectService.addSecurityRule($scope.filter, rule);
				}
			};
			
			$scope.removePosition = function ( ) {
				objectService.revokeAllRules($scope.filter, $scope.position);
			};
			
		}]);
	
})();