/*global angular*/
(function ( ) {

    angular.module('Zaaksysteem.docs')
        .controller('nl.mintlab.docs.FilePreviewController', [ '$scope', '$window', 'smartHttp', function ( $scope, $window, smartHttp ) {

            $scope.$on('popupclose', function ( /*event, key, value*/ ) {
                $scope.reloadData();
            });

            $scope.onNameClick = function ( event ) {
                if ($scope.entity.getEntityType() === 'folder') {
                    $scope.onFolderNameClick(event);
                } else {
                    $scope.deselectAll();
                    $scope.selectEntity($scope.entity);

                    $scope.openPopup();
                }
                
                event.stopPropagation();
            };

        }]);
})();
