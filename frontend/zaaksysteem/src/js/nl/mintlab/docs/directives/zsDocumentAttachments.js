(function () {
  angular.module('Zaaksysteem.docs')
    .directive('zsDocumentAttachments', [function () {
      return {
        restrict: 'E',
        template: (
          '<div class="row row-email-content row-email-attachment" data-ng-show="attachments.length">' +
            '<div class="column large-4 td-label">%%Bijlagen%%</div>' +
            '<div class="column large-8">' +
              '<div class="td-field" data-ng-repeat="attachment in attachments">' +
                '<i class="mdi mdi-attachment"></i>' +
                '<label>' +
                '<input type="checkbox" data-ng-checked="isAttached(attachment)" data-ng-click="detach(attachment)"/>' +
                '{{ getName(attachment) }}' +
                '</label>' +
                '<input type="text" data-ng-show="false" data-ng-model="newAttachment" data-zs-placeholder="\'%%Voeg een zaakdocument toe%%\'" data-zs-spot-enlighter data-zs-spot-enlighter-restrict="\'case_type_document\'" data-zs-spot-enlighter-label="naam"/>' +
              '</div>' +
            '</div>' +
          '</div>'
        )
      };
    }]);
})();
