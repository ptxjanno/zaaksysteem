/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.pip')
		.directive('zsPipYearlyPaymentView', [ '$window', '$http', 'translationService', 'systemMessageService', function ( $window, $http, translationService, systemMessageService ) {
			
			var crudConfig = {
				options: {
					select: 'single',
					pagination: false
				},
				actions: [ 
					{
						id: 'download',
						type: 'click',
						data: {
							click: 'pipYearlyPaymentView.downloadPdf($selection[0])'
						},
						label: translationService.get('Download')
					}
				],
				columns: [
					{
						id: 'regeling',
						label: translationService.get('Regeling'),
						resolve: 'SpecificatieJaarOpgave[0].Regeling',
						locked: true,
						sort: false
					},
					{	
						id: 'year',
						label: translationService.get('Jaar'),
						resolve: 'SpecificatieJaarOpgave[0].Dienstjaar',
						locked: true,
						sort: false
					},
					{
						id: 'actions',
						label: 'Acties',
						locked: true,
						templateUrl: '/html/core/crud/crud-item-action-menu.html',
						dynamic: true,
						sort: false
					}
				]
			};
			
			return { 
				scope: true,
				require: [ 'zsPipYearlyPaymentView', '^zsPipPaymentView' ],
				controller: [ function ( ) {
					
					var ctrl = this,
						zsPipPaymentView,
						years = [],
						items = [];
						
					function getYearData ( ) {
						$http({
							method: 'GET',
							url: '/pip/gws4all/jaaropgave?year=' + ctrl.year
						})
							.success(function ( response ) {
								items = response.result[0].JaarOpgave;
							})
							.error(function ( ) {
								systemMessageService.emitLoadError('uw specificaties');
							});
					}
					
					ctrl.link = function ( controllers ) {
						zsPipPaymentView = controllers[0];
						years = zsPipPaymentView.getYears();
						
						ctrl.year = years[0];
						
						if(ctrl.year) {
							getYearData();
						}
					};
					
					ctrl.getYears = function ( ) {
						return years;
					};
					
					ctrl.getCrudConfig = function ( ) {
						return crudConfig;
					};
					
					ctrl.getItems = function ( ) {
						return items;
					};
					
					ctrl.handleYearChange = function ( ) {
						getYearData();
					};
					
					ctrl.downloadPdf = function ( item ) {
						$window.open('/pip/gws4all/jaaropgave/pdf?year=' + ctrl.year + '&index=' + _.indexOf(items, item));
					};
					
					return ctrl;
				}],
				controllerAs: 'pipYearlyPaymentView',
				link: function ( scope, element, attrs, controllers ) {
					controllers[0].link(controllers.slice(1));
				}
			};
			
		}]);
	
})();
