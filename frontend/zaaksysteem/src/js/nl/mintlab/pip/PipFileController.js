/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.pip')
		.controller('nl.mintlab.pip.PipFileController', [ '$scope', '$window', function ( $scope, $window) {
			
			$scope.downloadFile = function ( event ) {
				var url = ('/pip/zaak/' + $scope.caseId + '/document/' + $scope.doc.id + '/download');
				$window.open(url);
				event.stopPropagation();
			};
			
		}]);
		
})();