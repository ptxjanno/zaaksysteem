/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.kcc')
		.service('callService', [ 'smartHttp', '$rootScope', '$filter', 'callPollService', 'subjectService', function ( smartHttp, $rootScope, $filter, callPollService, subjectService ) {
			
			var callService = {},
				available = true,
				enabled = false,
				ready = !!enabled,
				calls = [];
			
			function setEnabled ( en ) {
				if(enabled !== en) {
					enabled = en;
					broadcastEnabledChange(); 
					if(!en) {
						ready = false;
						broadcastReadyChange();
						callPollService.disable();
						calls.length = 0;
						broadcastListUpdate();
					}
					smartHttp.connect({
						method: 'POST',
						url: '/api/kcc/user/' + (enabled ? 'enable' : 'disable')
					})
						.success(function ( ) {
							if(en) {
								ready = true;
								broadcastReadyChange();
								callPollService.enable();
							}
						})
						.error(function ( ) {
							enabled = false;
						});
				}
			}
			
			function setAvailable ( av ) {
				if(available !== av) {
					available = av;
					broadcastAvailableChange();
				}
			}
			
			function removeFromQueue ( call ) {
				calls = _.reject(calls, function ( c ) {
					return c.id === call.id;
				});
				broadcastListUpdate();
			}
			
			function broadcastAvailableChange ( ) {
				$rootScope.$broadcast('call.available.change', available);
			}
				
			function broadcastEnabledChange ( ) {
				$rootScope.$broadcast('call.enabled.change', enabled);
			}
			
			function broadcastReadyChange ( ) {
				$rootScope.$broadcast('call.ready.change', enabled);
			}
			
			function broadcastListUpdate ( ) {
				$rootScope.$broadcast('call.list.update', calls);
			}
			
			callService.isEnabled = function ( ) {
				return enabled;	
			};
			
			callService.enable = function ( ) {
				setEnabled(true);
			};
			
			callService.disable = function ( ) {
				setEnabled(false);
			};
			
			callService.addCalls = function ( newCalls ) {
				calls = newCalls;
			};
			
			callService.getCalls = function ( ) {
				return calls;
			};
			
			callService.isAvailable = function ( ) {
				return available;
			};
			
			callService.isReady = function ( ) {
				return ready;
			};
			
			callService.acceptCall = function ( id ) {
				var call = _.find(calls, function ( c) {
						return c.id === id;
					}),
					contact = call ? call.betrokkene[0] : null,
					currentSubject = subjectService.getSubject(),
					subject;
					
				// FIXME: this is because it's impractical to
				// return the same contact data from /api/kcc/call/list
				// as in /api/kcc/call/accept, so we'll port the structure
				// from the former to the latter.
				
				if(contact) {
					subject = contact;
				}
				
				subjectService.setSubject(subject);
				
				removeFromQueue(call);
				
				return smartHttp.connect( {
					method: 'POST',
					url: '/api/kcc/call/accept',
					data: {
						call_id: id
					}
				})
					.success(function ( /*response*/ ) {				
						$rootScope.$broadcast('call.accept', call);
					})
					.error(function ( ) {
						subjectService.setSubject(currentSubject);
						calls.push(call);
					});
			};
			
			callService.rejectCall = function ( id ) {
				return smartHttp.connect({
					method: 'POST',
					url: '/api/kcc/call/reject',
					data: {
						call_id: id
					}
				})
					.success(function ( response ) {
						var call = response.result[0];
						removeFromQueue(call);
						$rootScope.$broadcast('call.reject', call);
					})
					.error(function ( ) {
						
					});	
			};
			
			$rootScope.$on('call.incoming', function ( $event, incomingCalls ) {
				calls = $filter('orderBy')(incomingCalls, 'id', false);
				broadcastListUpdate();
			});
			
			var serviceAvailableUnwatch = $rootScope.$watch('kcc_service_available', function ( serviceAvailable ) {
				if(serviceAvailable !== undefined) {
					setAvailable(serviceAvailable);
					serviceAvailableUnwatch();
				}
			});
			
			var userAvailableUnwatch = $rootScope.$watch('kcc_user_available', function ( userAvailable ) {
				if(userAvailable !== undefined) {
					setEnabled(userAvailable);
					userAvailableUnwatch();
				}
			});
				
			return callService;
			
		}]);
	
})();