/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsBreadcrumbItem', [ function ( ) {
			
			return {
				require: [ '^zsBreadcrumb', 'zsBreadcrumbItem' ],
				scope: true,
				transclude: true,
				replace: true,
				template: '<li class="breadcrumb-item<[breadcrumbItem.isSelected() ? \' breadcrumb-item-active\' : \'\']>"><button class="breadcrumb-item-button" data-ng-click="breadcrumbItem.handleClick()" data-ng-transclude></button></li>',
				controller: [ '$scope', '$attrs', function ( $scope, $attrs ) {
					
					var ctrl = this,
						parent;
						
					function getValue ( ) {
						return $scope.$eval($attrs.zsBreadcrumbItem);
					}
						
					ctrl.setParent = function ( p ) {
						parent = p;
					};
					
					ctrl.isSelected = function ( ) {
						return parent && parent.isSelected(getValue());
					};
					
					ctrl.handleClick = function ( ) {
						parent.select(getValue());	
					};
					
					ctrl.getClass = function ( ) {
						return ctrl.isSelected() ? ' breadcrumb-item-active' : '';
					};
					
					return ctrl;
					
				}],
				controllerAs: 'breadcrumbItem',
				link: function ( scope, element, attrs, controllers ) {
					
					var zsBreadcrumb = controllers[0],
						zsBreadcrumbItem = controllers[1];
						
					zsBreadcrumbItem.setParent(zsBreadcrumb);
					
				}
				
			};
			
		}]);
	
})();
