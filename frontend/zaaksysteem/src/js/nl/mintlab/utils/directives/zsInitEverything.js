/*global angular,initializeEverything,$*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsInitEverything', [ function ( ) {
			
			return {
				priority: -1,
				link: function ( scope, element/*, attrs*/ ) {
					
					initializeEverything();
					
				}
			};
			
		}]);
	
})();