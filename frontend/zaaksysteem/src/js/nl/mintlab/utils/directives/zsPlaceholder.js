/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.directives')
		.directive('zsPlaceholder', [ '$document', function ( $document ) {
			
			var doc = $document[0],
				hasSupport = "placeholder" in $document[0].createElement('input');
			
			return {
				link: function ( scope, element, attrs ) {
					
					var clone,
						placeholder = scope.$eval(attrs.zsPlaceholder);

					function setVisibility ( ) {
						var hasFocus = doc.activeElement === element[0] || doc.activeElement === clone[0],
							visibleElement;
						
						if(element.val()) {
							showSource();
							visibleElement = element;
						} else {
							showClone();
							visibleElement = clone;
						}
						
						if(hasFocus) {
							visibleElement[0].focus();
						}
					}
					
					function onCloneFocus ( ) {
						showSource();
						element[0].focus();
					}
					
					function showSource ( ) {
						var parent = clone.parent ? clone.parent() : null;
						if(parent && parent.length) {
							clone.after(element);
							clone[0].parentNode.removeChild(clone[0]);
						}
					}
					
					function showClone ( ) {
						var parent = element.parent ? element.parent() : null;
						if(parent && parent.length) {
							element.after(clone);
							element[0].parentNode.removeChild(element[0]);
						}
					}
					
					function onBlur ( ) {
						setVisibility();
					}
					
					function onModelChange ( ) {
						if(doc.activeElement !== element[0]) {
							setVisibility();
						}
					}
					
					
					if(hasSupport) {
						element.attr('placeholder', placeholder);
					} else {

						clone = element.clone(true);
						
						clone.removeAttr('ng-model');
						clone.removeAttr('zs-placeholder');
						clone.addClass('zs-placeholder-clone');
						
						clone.bind('focus', onCloneFocus);
						element.bind('blur', onBlur);
						
						clone.val(placeholder);
						
						if(attrs.ngModel) {
							scope.$watch(attrs.ngModel, onModelChange);
						}
					}

					scope.$watch(attrs.zsPlaceholder, function (placeholder) {
						if (clone) {
							clone.val(placeholder);
						}
						element.attr('placeholder', placeholder);
					});
				}
			};
		}]);
	
})();
