/*global angular*/
(function ( ) {

	angular.module('Zaaksysteem')
		.directive('ngEscape', function ( ) {
			return function ( scope, element, attrs ) {
				element.bind('keyup', function ( event ) {
					if(event.keyCode === 27) {
						scope.$eval(attrs.ngEscape);
					}
				});
				
			};
		});
})();