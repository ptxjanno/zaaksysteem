/*global angular,_,fetch*/
(function () {
	
	angular.module('Zaaksysteem')
		.directive('zsFileFormField', [ 'fileUploader', 'translationService', function ( fileUploader, translationService ) {
			
			var indexOf = _.indexOf;			
			return {
				scope: false,
				require: [ 'ngModel' ],
				compile: function ( ) {
					
					return function link ( scope, element, attrs, controllers ) {
						
						var ngModel = controllers[0];
						
						function getMax ( ) {
							return !!scope.$eval(attrs.zsFileFormFieldMultiple) ? Number.MAX_VALUE : 1;
						}
						
						scope.$on('upload.start', function ( ) {
							
						});
						
						scope.$on('upload.progress', function ( ) {
							
						});
						
						scope.$on('upload.end', function ( ) {
							
						});
						
						scope.$on('upload.complete', function ( event, upload ) {
							scope.$apply(function ( ) {
								var result = upload.getData().result,
									max = getMax(),
									fileList = ngModel.$viewValue ? ngModel.$viewValue.concat() : [],
									i,
									l,
									file;
									
								for(i = 0, l = result.length; i < l; ++i) {
									file = result[i];
									while(fileList.length >= max) {
										fileList.shift();
									}
									fileList.push(file);
								}
								
								ngModel.$setViewValue(fileList);
							});
						});
						
						scope.$on('upload.error', function (  ) {
							scope.$apply(function ( ) {
								scope.$emit('systemMessage', {
									type: 'error',
									content: translationService.get('Er ging iets fout bij het uploaden van het bestand')
								});
							});
						});
						
						scope.removeFile = function ( file ) {
							var files = (ngModel.$viewValue || []).concat(),
								index = indexOf(files, file);
								
							if(index !== -1) {
								files.splice(index, 1);
								ngModel.$setViewValue(files);
							}
                        };

                        scope.canShowFileInfo = function (file) {
                            return Boolean(file && file.certificate_info);
                        }
                        
                        scope.showFileInfo = function (file) {
                            const info = file.certificate_info;
                            alert([
                                'subject: ' + info.subject, 
                                'issuer: ' + info.issuer,
                                'serial: ' + info.serial,
                                'notBefore: ' + info.start,
                                'notAfter: ' + info.end,
                                'version: ' + info.version,
                                'fingerprint_md5: ' + info.md5,
                                'fingerprint_sha1: ' + info.sha1,
                                'fingerprint_sha256: ' + info.sha256
                            ].join('\n'))
						};
						
					};
				}
			};
			
		}]);
	
})();
