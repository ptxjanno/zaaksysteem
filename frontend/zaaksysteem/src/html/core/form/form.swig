<div data-ng-form name="<[getFormName()]>" class="ezra_no_waitstart" data-ng-controller="nl.mintlab.core.form.FormController">
	
	<fieldset class="form-fieldset form-fieldset-name-<[fieldset.name]>" data-ng-repeat="fieldset in zsForm.fieldsets" data-ng-controller="nl.mintlab.core.form.FormFieldsetController" data-ng-class="{'form-fieldset-collapsed': fieldset.collapsed, 'form-fieldset-changed': isChanged() }">
		<div class="form-fieldset-meta" data-ng-show="fieldset.title||fieldset.description">
			<div class="form-fieldset-header clearfix" data-ng-show="fieldset.title" data-ng-click="toggleCollapse()">
				<button type="button" class="form-fieldset-collapse mdi" data-ng-class="{ 'mdi-chevron-down': fieldset.collapsed, 'mdi-chevron-up': !fieldset.collapsed }">
				</button>
				<span class="form-fieldset-title"><[fieldset.title]></span>
				<div class="form-fieldset-revert">
					<button type="button" class="btn btn-flat btn-round btn-small" data-ng-click="revert($event)" data-zs-title="Terug naar standaardwaarde">
						<i class="mdi mdi-undo"></i>
					</button>
				</div>
			</div>
			<div class="form-fieldset-desc" data-ng-show="fieldset.description" data-zs-template="<[fieldset.description]>">
			</div>		
		</div>
		<div class="form-fieldset-children" data-ng-if="!fieldset.collapsed">
			<div class="form-field form-field-name-<[field.name]>" data-ng-repeat="field in getFields(fieldset) track by field.name" data-ng-include="'/html/form/form-field.html'" data-ng-class="{'form-field-required': getRequired(field), 'form-field-valid': isValid(field), 'form-field-invalid': !isValid(field), 'form-field-pristine': isPristine(field), 'form-field-empty': isFieldEmpty(field), 'form-field-filled': !isFieldEmpty(field) }">
			</div>
		</div>
		<div class="form-fieldset-actions" data-ng-show="hasVisibleActions(fieldset.actions)">
			<button class="form-action form-action-<[action.name]>" data-ng-repeat="action in fieldset.actions" data-ng-click="action.type=='popup'&&openPopup()||handleActionClick(action, $event)" type="<[action.type=='submit'&&'submit'||'button']>" data-zs-popup="action.type=='popup'&&'/html/form/form-modal.html'||''" data-ng-class="{ 'button button-primary': action.importance != 'secondary', 'button button-secondary': action.importance == 'secondary' }" data-ng-show="isVisible(action)">
				<[action.label]>
			</button>
		</div>
		<div class="form-fieldset-template" data-ng-show="fieldset.templateUrl" data-ng-include="fieldset.templateUrl">
		</div>
	</fieldset>
	
	<div class="form-field-button-list" data-ng-show="hasVisibleActions(zsForm.actions)">
		<button class="form-action form-action-<[action.name]>" data-ng-repeat="action in zsForm.actions" data-ng-click="action.type=='popup'&&openPopup()||handleActionClick(action, $event)" data-ng-disabled="isActionDisabled(action)" type="<[action.type=='submit'&&'submit'||'button']>" data-zs-popup="action.type=='popup'&&'/html/form/form-modal.html'||''" data-ng-class="{ 'button button-primary': action.importance != 'secondary', 'button button-secondary': action.importance == 'secondary' }" data-ng-show="isVisible(action)">
			<[action.label]>
		</button>
	</div>
	
	<div class="form-status" data-ng-class="{ 'form-status-submitting': submitting }">
		<div class="form-status-indicator"></div>
		<div class="form-status-last-saved" data-ng-show="lastSaved">Opgeslagen om <[lastSaved|date:'HH:mm']></div>
	</div>
	
</div>

<script type="text/ng-template" id="/html/form/form-field.html">
	<div class="form-field_label" data-ng-class="{'form-field-label-empty': !field.label, 'form-field-label-description-visible':field.description}">
		<label for="<[getFieldId(field)]>">
			<[field.label]>
			<span class="form-field-required-indicator" data-ng-show="getRequired(field)"> *</span>
		</label>
	</div>

	<div class="form-field-input-wrapper">

		<div class="form-field-input" data-ng-include="field.template||('/html/form/form-field-type-' + field.type + '.html')">
		
		</div>

		<div class="form-field-status" data-ng-class="{'form-field-status-valid': isValid(field), 'form-field-status-invalid': !isValid(field), 'form-field-status-pristine': isPristine(field) }">
			<div class="form-field-status-indicator"></div>
			<div class="form-field-status-message" data-ng-show="field.statusMessage"><[field.statusMessage]></div>
		</div>

	</div>
	
	
	
	<div class="form-field-revert" data-ng-show="field.default!=undefined&&field.default!=null">
		<button type="button" class="form-field-revert-button" data-ng-click="revertField(field)">
			%%Herstel%%
		</button>
	</div>
	
	<div class="form-field-description" data-ng-show="field.description" tabindex="0">
		<div class="form-field-description-icon icon-font-awesome icon-question-sign"></div> 
		<div class="form-field-description-message"  data-zs-template="<[field.description]>"></div>
	</div>
</script>

<!-- TYPES -->
<script type="text/ng-template" id="/html/form/form-field-type-text.html">
	<input 
		type="text" 
		name="<[field.name]>" 
		data-zs-form-field 
		data-ng-model="scope[field.name]" 
		data-ng-required="getRequired(field)" 
		data-ng-pattern="field.data.pattern" 
		id="<[getFieldId(field)]>" 
		data-zs-placeholder="field.data.placeholder||''" 
		data-zs-validation="<[field.data.validation]>" 
		data-validators="field.data.validators" 
		data-ng-disabled="field.disabled" 
	/>
</script>

<script type="text/ng-template" id="/html/form/form-field-type-list.html">
	<input type="text" name="<[field.name]>" data-zs-form-field data-ng-model="scope[field.name]" ng-list data-ng-required="getRequired(field)" data-ng-pattern="field.data.pattern" id="<[getFieldId(field)]>" data-zs-placeholder="field.data.placeholder||''" data-zs-validation="<[field.data.validation]>" data-validators="field.data.validators"/>
</script>

<!-- TYPE NAME == 'richtext' -->
<script type="text/ng-template" id="/html/form/form-field-type-richtext.html">
	<zs-quill-rich-text-editor
		name="<[field.name]>"
		data-zs-form-field
		data-ng-model="scope[field.name]"
		data-ng-required="getRequired(field)" 
		data-editor-id="field.name"
		data-editor-disabled="field.disabled" 
		data-editor-features="field.data.features"
	>
	</zs-quill-rich-text-editor>
</script>

<script type="text/ng-template" id="/html/form/form-field-type-textarea.html">
	<textarea data-ng-model="scope[field.name]" name="<[field.name]>" data-ng-required="getRequired(field)" data-zs-form-field data-ng-pattern="field.data.pattern" id="<[getFieldId(field)]>" data-validators="field.data.validators" data-zs-validation="<[field.data.validation]>" data-zs-placeholder="field.data.placeholder||''" data-ng-disabled="field.disabled" data-validators="field.data.validators"></textarea>
</script>

<script type="text/ng-template" id="/html/form/form-field-type-display.html">
	<span data-zs-template="<[field.data.template]>"></span>
</script>

<script type="text/ng-template" id="/html/form/form-field-type-number.html">
	<input type="number" name="<[field.name]>" data-zs-form-field data-ng-model="scope[field.name]" data-ng-required="getRequired(field)" id="<[getFieldId(field)]>" data-zs-placeholder="field.data.placeholder||''"  data-validators="field.data.validators"/>
</script>

<script type="text/ng-template" id="/html/form/form-field-type-email.html">
	<input type="email" name="<[field.name]>" data-zs-form-field data-ng-model="scope[field.name]" data-ng-required="getRequired(field)" id="<[getFieldId(field)]>" data-zs-placeholder="field.data.placeholder||''"  data-validators="field.data.validators"/>
</script>

<script type="text/ng-template" id="/html/form/form-field-type-password.html">
	<input type="password" name="<[field.name]>" data-zs-form-field data-ng-model="scope[field.name]" data-ng-required="getRequired(field)" data-ng-pattern="field.data.pattern" id="<[getFieldId(field)]>" data-zs-placeholder="field.data.placeholder||''" data-zs-validation="<[field.data.validation]>"  data-validators="field.data.validators"/>
		
		<zs-password-strength-indicator password="<[scope[field.name]]>" data-ng-if="field.data.showPasswordStrength">
		</zs-password-strength-indicator>
	
</script>

<script type="text/ng-template" id="/html/form/form-field-type-url.html">
	<input type="url" name="<[field.name]>" data-zs-form-field data-ng-model="scope[field.name]" data-ng-required="getRequired(field)" id="<[getFieldId(field)]>" data-zs-placeholder="field.data.placeholder||''" data-zs-validation="<[field.data.validation]>"  data-validators="field.data.validators" />
	<a target="_blank" rel="noopener" class="ezra_url_field_goto button button-secondary button-small" data-ng-href="<[isValid(field)?scope[field.name]:'']>" data-zs-title="Ga naar de url" data-ng-show="!isFieldEmpty(field)&&isValid(field)">
		<i class="icon-font-awesome icon icon-external-link"></i>
	</a>
</script>

<script type="text/ng-template" id="/html/form/form-field-type-radio.html">
	<label data-ng-repeat="option in getOptions(field)">
		<input type="radio" name="<[field.name]>" data-zs-form-field data-ng-model="scope[field.name]" value="<[option.value]>"/>
		<[option.label]>
	</label>
</script>

<script type="text/ng-template" id="/html/form/form-field-type-checkbox.html">
	<label data-zs-form-field-checkbox data-ng-model="scope[field.name]" name="<[field.name]>" data-zs-true-value="getTrueValue(field)" data-zs-false-value="getFalseValue(field)">
		<input type="checkbox" name="val" data-zs-form-field data-ng-model="val"/>
		<span class="checkbox-label" data-ng-show="field.data.checkboxlabel">
			<[field.data.checkboxlabel]>
		</span>
	</label>
</script>

<script type="text/ng-template" id="/html/form/form-field-type-checkbox-list.html">
	<div class="checkbox-list" data-zs-checkbox-list data-zs-checkbox-list-options="getOptions(field, true)" data-zs-checkbox-list-max-visible="<[field.data.maxVisible]>" data-ng-model="$parent.scope[field.name]" name="<[field.name]>" data-zs-form-field data-zs-checkbox-list-less-label="<[field.data.labels.less]>" data-zs-checkbox-list-more-label="<[field.data.labels.more]>" data-ng-required="getRequired(field)">
		<ul class="checkbox-list">
			<li class="checkbox-list-item" data-ng-class="{'checkbox-list-item-always-visible': option.always_visible }" data-ng-repeat="option in getVisibleOptions(getOptions(field, true))">
				<label>
					<input type="checkbox" data-ng-checked="scope[option.value]" data-ng-click="toggleOption(option.value)"/>
					<span class="checkbox-label">
						<[option.label]>
					</span>
				</label>
			</li>
		</ul>
		
		<button type="button" class="btn-show-more" type="button" data-ng-click="toggleExpanded()" data-ng-show="hasInvisibleOptions(getOptions(field,true))">
			<i data-ng-show="zsExpanded" class="mdi mdi-chevron-up"></i><span data-ng-show="zsExpanded"> <[getLessOptionsLabel()]></span>
			<i data-ng-show="!zsExpanded" class="mdi mdi-chevron-down"></i><span data-ng-show="!zsExpanded"> <[getMoreOptionsLabel()]></span>
		</button>
	</div>
	
</script>

<script type="text/ng-template" id="/html/form/form-field-type-select.html">
	<select data-ng-model="scope[field.name]" name="<[field.name]>" data-zs-form-field data-ng-options="option.value as option.label for option in getOptions(field)" data-ng-required="getRequired(field)">
		<option data-ng-show="field._empty" value=""><[field._empty.label]></option>
	</select>
</script>

<script type="text/ng-template" id="/html/form/form-field-type-spot-enlighter.html">
	<div class="form-field-spot-enlighter-component" data-ng-model="scope[field.name]" data-zs-spot-enlighter-form-field data-zs-spot-enlighter-form-field-multiple="<[field.data.multi]>" data-zs-spot-enlighter-label="<[field.data.label]>" data-zs-form-field data-zs-spot-enlighter-resolve="<[field.data.resolve]>" data-ng-required="getRequired(field)" data-zs-spot-enlighter-form-field-transform="zsSpotEnlighterFormField.transform($object)">
		<ul class="form-field-spot-enlighter-list" data-ng-show="field.data.multi">
			<li class="form-field-spot-enlighter-list-item" data-ng-repeat="object in scope[field.name]">
				<span class="form-field-spot-enlighter-list-item-label">
					<[getObjLabel(object)]>
				</span>
				<span class="form-field-spot-enlighter-list-item-options">
					<button type="button" class="form-field-spot-enlighter-list-item-remove mdi mdi-close" data-ng-click="remove(object)">
					</button>
				</span>
			</li>
		</ul>
		<div class="spot-enlighter-wrapper">
			<input type="text" data-ng-model="obj" data-zs-spot-enlighter data-zs-spot-enlighter-restrict="getRestrict(field)" data-zs-spot-enlighter-label="<[field.data.label]>" data-zs-spot-enlighter-params="zsSpotEnlighterFormField.getParams()" data-zs-placeholder="field.data.placeholder||'Begin te typen&hellip;'" />
		</div>
		<label class="spot-enlighter-inactive" data-ng-show="zsSpotEnlighterFormField.isInactiveToggleVisible()" >
			<input type="checkbox" data-ng-model="zsSpotEnlighterFormField.includeInactive" />Toon inactief
		</label>
	</div>
</script>

<script type="text/ng-template" id="/html/form/form-field-type-date-range.html">
	<div class="form-field-date-range-component" name="<[field.name]>" data-ng-model="scope[field.name]" data-zs-date-range-form-field data-zs-form-field data-zs-popup="'/html/form/date-range-picker.html'">
		<div class="form-field-date-range-component-select" data-zs-popup-menu>

			<button class="btn btn-secondary btn-dropdown">
				<span><[getDateRangeLabel()]> </span>
				<i class="mdi mdi-menu-down"></i>
			</button>
			<ul class="popup-menu date-range-list">
				<li class="date-range-list-item" data-ng-repeat="option in getOptions(field, true)" data-ng-class="{'date-range-list-item-active': option.value == dateRangeType }">
					<button type="button" class="date-range-list-item popup-menu-item-button" data-ng-click="onDateRangeClick(option.value, $event);closePopupMenu();">
						<i class="icon-font-awesome icon-ok"></i>
						<[option.label]>
					</button>
				</li>
			</ul>
		</div>
		
	</div>
</script>

<script type="text/ng-template" id="/html/form/form-field-type-date.html">
	<div class="form-field-date-range-component" name="<[field.name]>" data-ng-model="scope[field.name]" data-zs-date-form-field data-zs-form-field data-zs-popup="'/html/form/date-picker.html'" data-ng-required="getRequired(field)">
		<div class="form-field-date-range-component-select">
            <button 
                ng-repeat="dateItem in getDateItems()"
                ng-if="showDynamicDates"
                type="button" 
                class="button button-secondary button-xsmall button-dynamic-date"
                ng-class="{ 'button-date-selected': isDynamicDateSelected(dateItem) }"
                data-ng-click="$emit('date.range.select', dateItem.dateSymbol)"
            ><[dateItem.label]></button>
            <hr ng-if="showDynamicDates" />
			<button 
                type="button" 
                class="button button-secondary button-xsmall"
                ng-class="{ 'button-date-selected': showDynamicDates && isStaticDateSelected() }"
                data-ng-click="openPopup()"
            >
                <span><[getDateLabel()]></span>
			</button>
		</div>
	</div>
</script>

<script type="text/ng-template" id="/html/form/form-field-type-file.html">
	<div data-zs-form-field data-zs-file-form-field data-ng-model="scope[field.name]" data-zs-array-required="getRequired(field)" data-zs-file-form-field-multiple="field.data.multi">
		
		<ul class="form-file-list" data-ng-repeat="file in scope[field.name]">
			<li class="form-file-list-item">
				<span class="form-file-list-item-name"><[file.original_name]></span>
				<button type="button"class="form-field-description-icon icon-font-awesome icon-question-sign" data-ng-if="canShowFileInfo(file)" data-ng-click="showFileInfo(file)"></button>
				<button type="button"class="form-file-list-item-remove icon-font-awesome icon-remove" data-ng-click="removeFile(file)"></button>
			</li>
		</ul>
		
		<div class="form-file-upload-button" data-zs-upload data-zs-upload-multiple="<[field.data.multi]>">
			<button type="button" class="button button-secondary button-small">
				%%Upload%%
			</button>
		</div>
	</div>
</script>

<script type="text/ng-template" id="/html/form/form-field-type-price.html">
	<input type="text" name="<[field.name]>" data-zs-form-field data-zs-price-form-field data-ng-model="scope[field.name]" data-ng-required="getRequired(field)" data-zs-restrict="[0-9\.]" id="<[getFieldId(field)]>" data-zs-placeholder="field.data.placeholder||''"/>
</script>

<script type="text/ng-template" id="/html/form/form-field-type-bankaccount.html">
	<div zs-scope data-ng-init="enforce=true">
		<input type="text" name="<[field.name]>" data-zs-form-field data-zs-iban-validate data-ng-model="scope[field.name]" data-ng-required="getRequired(field)" id="<[getFieldId(field)]>" data-zs-placeholder="'Bijv. NL91ABNA0417164300'"/>
	</div>
</script>

<script type="text/ng-template" id="/html/form/form-field-type-org-unit.html">
	<div data-zs-form-field data-zs-org-unit-form-field data-ng-model="scope[field.name]">
		<ul class="form-field-type-org-unit-list">
			<li class="form-field-type-org-unit-list-item" data-ng-repeat="item in scope[field.name]">
				<div class="form-field-type-org-unit-list-item-picker" data-ng-model="item.orgUnit" data-zs-org-unit-picker>
				</div>
				<button type="button" class="form-field-type-org-unit-list-item-picker-remove" data-ng-click="removeOrgUnit(item)">
					<i class="mdi mdi-close"></i>
				</button>
			</li>
		</ul>
		<button type="button" class="form-field-type-org-unit-add-role-button btn btn-secondary" data-ng-click="addOrgUnit()" data-ng-show="!isLimitReached(field.data.limit)">
			%%Toevoegen%%
		</button>
	</div>
</script>

<script type="text/ng-template" id="/html/form/form-field-type-map.html">
	<div data-zs-form-field data-zs-map-form-field data-ng-model="scope[field.name]" data-zs-ezra-map>

		<div class="map-wrap ezra_map ezra_map-smallfixed ezra_map-fixedwidth ezra_map-latlononly">
			<div class="map-header">
				<input
	 					type="hidden"
						class="ezra_map-center"/>
				<input
					type="hidden"
					class="ezra_map-address ezra_map-kenmerkfield"/>
			</div>
			<div class="map">
				<div class="ezra_map-viewport" tabindex="0">
					<div class="spinner-groot"><div></div></div>
				</div>
			</div>
			<div class="map-balloon ezra_map-template" style="display: none">
				<h2 class="map-balloon-title">\%\%address\%\%</h2>
			</div>
		</div>

	</div>
</script>

<script type="text/ng-template" id="/html/form/form-field-type-multiple.html">

	<div 
		class="form-field-type-multiple"
		data-zs-multiple-form-field
		data-zs-form-field
		data-ng-model="scope[field.name]"
		data-fields="field.data.fields"
		name="<[field.name]>"
	>
		<ul>
			<li data-ng-repeat="value in getValues() track by $index">
				<div
					data-zs-form-form-field
					data-zs-form-template-parser
					data-config="getFormConfig($index)"
					data-ng-model="getValues()[$index]"
					data-ng-change="onViewChange()"
					data-fields="field.data.fields"
					name="<[field.name + '-' + $index]>"
				>
				</div>
				<button data-ng-click="removeField($index)" class="form-field-type-multiple-remove">
					<i class="mdi mdi-close"></i>
				</button>
			</li>
		</ul>

		<button ng-click="addField()" ng-show="!hasReachedLimit()" class="btn btn-primary">
			Voeg toe
		</button>
	</div>

</script>

<script type="text/ng-template" id="/html/form/form-field-type-form.html">

	<div data-zs-form-field data-zs-form-template-parser data-config="getFormConfig()" data-zs-form-form-field data-ng-model="scope[field.name]" data-fields="field.data.fields" name="<[field.name]>">
	
	</div>

</script>

<script type="text/ng-template" id="/html/form/form-modal.html">
	<div data-zs-modal data-ng-init="title=action.data.title">
		<div data-ng-include="action.data.template_url">
		</div>
	</div>
</script>

<script type="text/ng-template" id="/html/form/date-range-picker.html">

	<div data-zs-modal data-ng-init="title='%%Kies een periode%%'"><div class="form-date-range-picker" data-zs-date-range-picker data-zs-date-range-picker-from="<[getFrom()]>" data-zs-date-range-picker-to="<[getTo()]>" data-zs-date-range-picker-min="<[getMin()]>" data-zs-date-range-picker-max="<[getMax()]>" data-ng-model="scope[field.name]">
		</div>
		<button type="button" class="button button-primary" data-ng-click="confirmRange();closePopup();" data-ng-disabled="!isValidRange()">
			Selecteer periode
		</button></div>
</script>

<script type="text/ng-template" id="/html/form/date-picker.html">
	<div data-zs-modal data-ng-init="title='%%Kies een datum%%'">
		<div class="form-date-range-picker" data-zs-date-range-picker data-zs-date-range-picker-from="<[getFrom()]>" data-zs-date-range-picker-to="<[getTo()]>" data-zs-date-range-picker-min="<[getMin()]>" data-zs-date-range-picker-max="<[getMax()]>" data-ng-model="scope[field.name]">
		</div>
	</div>
</script>
