<div class="timeline-title-wrap">
    <h2 class="timeline-title big-title-blue">Timeline</h2>
    <div class="timeline-filter">
        <ul>
            <li data-zs-dropdown-menu-decorate>
                <button class="btn btn-flat">
                    %%Filter de timeline%%
                    <i class="mdi mdi-menu-down"></i>
                </button>
                <ul class="timeline-options">
                    <li data-ng-repeat="filter in $parent.availableFilters">
                        <label class="timeline-filter-label">
                            <input 
                                class="timeline-filter-checkbox" 
                                type="checkbox" 
                                data-ng-checked="$parent.activeFilters.length>0&&isFiltered(filter.id)" 
                                data-ng-click="toggleFilter(filter)"
                            />
                            <[filter.label|capitalize]>
                        </label>
                    </li>
                </ul>
            </li>
            <li>
                <button class="btn btn-flat" data-ng-click="downloadLog()">%%Logboek downloaden%%</button>
            </li>
        </ul>
    </div>
</div>

<div class="timeline-content"> 
    <ol class="timeline-feed timeline-inner" data-zs-infinite-scroll>
        <li
            class="timeline-item <[(item.event_category == 'note' || item.event_category == 'contactmoment') && 'timeline-item-extended' || '']> <[(item.event_type == 'case/create' || item.event_type == 'case/close') && 'timeline-item-priority' || '']>"
            data-ng-repeat="item in $parent.items | orderBy:'timestamp':true | filter:$parent.getItemVisibility"
            data-ng-init="expanded=false"
            data-ng-class="{'timeline-item-highlighted': item.id == $parent.$parent.eventId }"
            data-event-type="<[::item.event_type]>"
        >
            <i class="mdi timeline-icon" data-event-type="<[item.event_category]>" data-mimetype="<[item.getMimetype()]>"></i>
            <div class="timeline-item-inner <[item.id==-1&&'timeline-item-new'||'']>">
                <div class="timeline-item-title">
                    <div class="timeline-item-titel">
                        <span class="timeline-item-desc">
                            <span class="timeline-item-desc-meta">
                                <span class="timeline-item-date"><[item.timestamp | date:'dd MMM yyyy HH:mm' ]></span>
                                <span data-ng-show="item.created_by">%%Door%% <[item.created_by]></span>
                                <span data-ng-show="item.contact_channel"> &middot; %%via%%: <[item.contact_channel | capitalize]></span>
                                <span data-ng-if="item.event_type==='email/send'"> &middot; <a href="/zaak/<[item.case_id]>/download_email_pdf/<[item.id]>">Download als PDF</a></span>
                            </span>

                            <span data-ng-bind-html="getItemDescription(item)"></span>

                            <span data-ng-show="item.case_id && item.event_type != 'case/create' && item.event_type != 'case/close'">
                                <a data-ng-hide="$parent.$parent.category=='case'" data-ng-href="/zaak/<[item.case_id]>">%%in zaak%% <[item.case_id]></a>
                            </span>

                            <br/>

                        </span>
                    </div>
                </div>
                
                <div class="timeline-item-content" data-ng-show="item.content.length||item.changes" data-ng-class="{'timeline-item-content-expanded': expanded, 'timeline-item-content-has-diffs': item.changes}">
                    
                    <!-- If this is a type with possible attachments (email, berichtenbox) -->
                    <div data-ng-show="$parent.$parent.isTypeWithAttachments(item)">

                        <pre><[expanded&&item.content||$parent.$parent.getTruncatedText(item)]></pre>
                        <div class="timeline-item-attachments" data-ng-show="item.attachments.length">
                            <button class="timeline-item-attached-file" data-ng-repeat="file in item.attachments" data-ng-click="handleAttachmentClick(item, file)">
                                <span class="timeline-item-attached-file-inner">
                                    <i class="mdi mdi-attachment"></i>
                                    <span><[file.filename]></span>
                                </span>
                            </button>
                        </div>
                    </div>

                    <!-- If this is not a type with possible attachments (email, berichtenbox) -->
                    <div data-ng-show="$parent.$parent.showItemContent(item, expanded)">

                        <pre ng-if="!item.changes"><[item.content]></pre>

                        <ul class="timeline-item-desc-diff" ng-if="item.changes">
                            <li class="timeline-item-desc-diff-item" ng-repeat="change in item.changes">
                                <div class="timeline-item-desc-diff-item-label">
                                    <[::change.field]>
                                </div>
                                <div class="timeline-item-desc-diff-item-value">
                                    <div class="old"><[::change.old_value]></div>
                                    <div class="new"><[::change.new_value]></div>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <button 
                        class="timeline-item-expand" 
                        data-ng-show="
                            $parent.$parent.isTruncated(item) &&
                            (item.event_category!='contactmoment'||$parent.$parent.isTypeWithAttachments(item)) &&
                            item.event_category!='note' &&
                            item.event_category!='case-mutation'" 
                        data-ng-click="expanded=!expanded"
                        >
                        <i class="mdi mdi-<[expanded&&'chevron-up'||'dots-horizontal']>"></i>
                    </button>

                </div>
                
                <div class="timeline-item-important"></div>
            </div>
        </li>
        <li class="timeline-loader <[$parent.loading&&'timeline-loader-loading'||'']>">
            <div class="spinner-google-container">
                <div class="spinner-google"></div>
            </div>
            <button class="btn btn-flat" data-ng-click="$parent.loadMoreData()">%%Meer%%</button>
        </li>
    </ol>
</div>
