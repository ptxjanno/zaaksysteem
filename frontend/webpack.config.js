const { join, resolve } = require('path');
const {
  DefinePlugin,
  ProgressPlugin,
  HotModuleReplacementPlugin
} = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const { readFileSync } = require('fs');

const isDev = process.env.NODE_ENV === 'development';

const read = baseName => readFileSync(`/etc/nginx/ssl/${baseName}`, 'utf8');

const extractStyle = use => {
  return isDev
    ? [{ loader: 'style-loader' }, ...use]
    : ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use
      });
};

/** @type {import('webpack').Configuration} */
module.exports = {
  devtool: 'source-map',
  output: {
    path: join(__dirname, '..', 'root', 'js'),
    publicPath: '/assets/',
    filename: '[name].bundle.js'
  },
  entry: {
    zs: './index.js'
  },
  devServer: isDev
    ? {
        headers: {
          'Access-Control-Allow-Origin': '*'
        },
        host: 'dev.zaaksysteem.nl',
        hot: true,
        https: {
          key: read('server.key'),
          cert: read('server.crt'),
          ca: read('ca.key')
        },
        inline: true,
        port: 9090
      }
    : {},
  module: {
    rules: [
      {
        test: /.*\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          babelrc: false,
          presets: ['babel-preset-es2015'].map(require.resolve),
          plugins: ['babel-plugin-add-module-exports'].map(require.resolve)
        }
      },
      {
        test: /.*\.html$/,
        use: 'html-loader?minimize=false',
        exclude: /node_modules/
      },
      {
        test: /\.(scss|css)$/,
        use: extractStyle([
          {
            loader: 'css-loader',
            options: {
              minimize: false,
              sourceMap: true
            }
          },
          {
            loader: 'sass-loader',
            options: { sourceMap: true }
          }
        ])
      },
      {
        test: /\.(woff(2)?|eot|ttf|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: 'url-loader?name=[name].[ext]&limit=10000'
      },
      {
        include: require.resolve('jquery/jquery'),
        use: 'exports-loader?window.jQuery'
      }
    ]
  },
  resolve: {
    modules: [
      resolve('../client'),
      resolve('./'),
      resolve('node_modules'),
      resolve('../client/node_modules')
    ],
    extensions: ['.js', '.scss', '.html'],
    alias: {
      jquery: 'jquery/jquery',
      angular: join(__dirname, 'angular-index.js')
    }
  },
  plugins: [
    new DefinePlugin({
      ENV: {
        IS_DEV: false
      }
    }),
    isDev
      ? null
      : new UglifyJsPlugin({
          uglifyOptions: { mangle: false },
          sourceMap: true
        }),
    isDev
      ? null
      : new ExtractTextPlugin({
          filename: '../css/[name].css',
          allChunks: true
        }),
    isDev ? new ProgressPlugin() : null,
    isDev ? new HotModuleReplacementPlugin() : null
  ].filter(Boolean),
  stats: {
    assets: true,
    cached: false,
    cachedAssets: false,
    children: false,
    chunks: false,
    chunkModules: false,
    chunkOrigins: false,
    colors: true,
    depth: false,
    entrypoints: false,
    errors: true,
    errorDetails: true,
    hash: false,
    maxModules: 0,
    modules: false,
    moduleTrace: false,
    performance: true,
    providedExports: false,
    publicPath: false,
    reasons: false,
    source: false,
    timings: true,
    usedExports: false,
    version: true,
    warnings: true
  }
};
