const gulp = require('gulp');
const concat = require('gulp-concat');
const rename = require('gulp-rename');
const terser = require('gulp-terser');
const sass = require('gulp-sass');
const swig = require('gulp-swig');
const replace = require('gulp-replace');
const cleanCss = require('gulp-clean-css');

const SRC = './*/src';
const DEST = './../root';
const APP_NAME = 'zaaksysteem';

const config = {
  js: {
    src: [
      `${SRC}/js/**/_*.js`,
      `${SRC}/js/**/*.js`,
      ...[
        'OpenLayers-2.12/OpenLayers.js',
        'jquery.cookie.js',
        'jquery.layout-latest.min.js',
        'jquery.layout.resizePaneAccordions-latest.js',
        'jquery.treeTable.js',
        'zaaksysteem.js',
        'jquery.hoverIntent.js',
        'easing.js',
        'multiselect/plugins/localisation/jquery.localisation-min.js',
        'ui.multiselect.js',
        'highcharts.js',
        'exporting.js',
        'regel_editor.js',
        'jquery.defaultvalue.js',
        'zaaksysteem/WebformTextfieldUpdateRegistry.js',
        'webform.js',
        'validate.js',
        'jquery.fileUploader.js',
        'jquery.qtip.min.js',
        'modernizr.js',
        'mintloader.js',
        'zaaktype_import.js',
        'zaaksysteem/ezra_objectsearch.js',
        'ezra_listactions.js',
        'maps.js',
        'zaaksysteem/wgxpath.install.js',
        'zaaksysteem/ezra_dialog.js',
        'zaaksysteem/ezra_simpletable.js',
        'zaaksysteem/ezra_kennisbank.js',
        'zaaksysteem/ezra_search_box.js',
        'zaaksysteem/ezra_maps.js',
        'zaaksysteem/ezra_import.js',
        'zaaksysteem/ezra_object_import.js',
        'zaaksysteem/import_inavigator.js',
        'zaaksysteem/ezra_search_chart.js',
        'ICanHaz.min.js',
        'zaaksysteem/ezra_woz_photo.js',
        'zaaksysteem/ezra_ztb_notificaties.js',
        'zaaksysteem/ezra_attribute_ie_upload.js',
        'iban.js',
        'quill/quill.min.js'
      ].map(filename => `../root/tpl/zaak_v1/nl_NL/js/${filename}`)
    ],
    dest: `${DEST}/js`,
    concat: {
      name: `${APP_NAME}.min.js`
    },
    components: {
      name: 'components.js'
    }
  },
  css: {
    src: [`${SRC}/css/**/*.scss`, `!${SRC}/css/**/*_.scss`],
    dest: `${DEST}/css`,
    concat: {
      name: `${APP_NAME}.css`
    }
  },
  html: {
    src: [`${SRC}/html/**/*.swig`],
    dest: `${DEST}/html`
  }
};

const copyBowerFiles = () =>
  gulp.src('./components.js').pipe(gulp.dest(config.js.dest));

const copyWebodf = () =>
  gulp.src('./webodf.js').pipe(gulp.dest('../root/webodf'));

const preparePdfViewer = () =>
  gulp
    .src('**/*', { cwd: './node_modules/@mintlab/pdf.js-viewer' })
    .pipe(gulp.dest('../root/pdf.js-with-viewer'));

const minifyJsFiles = () =>
  gulp
    .src(config.js.src)
    .pipe(concat(`${APP_NAME}.min.js`))
    .pipe(terser())
    .pipe(gulp.dest(config.js.dest));

const copyJsFiles = () =>
  gulp
    .src(config.js.src)
    .pipe(concat(`${APP_NAME}.js`))
    .pipe(gulp.dest(config.js.dest));

const buildMinifiedCss = () =>
  gulp
    .src(config.css.src)
    .pipe(sass({ errLogToConsole: true }))
    .pipe(
      rename(p => {
        p.dirname = p.dirname.replace(/(.*?)[\\/]src[\\/]css[\\/]?/, '');
        p.basename += '.min';
      })
    )
    .pipe(cleanCss())
    .pipe(gulp.dest(config.css.dest));

const buildReadableCss = () =>
  gulp
    .src(config.css.src)
    .pipe(sass({ errLogToConsole: true }))
    .pipe(
      rename(p => {
        p.dirname = p.dirname.replace(/(.*?)[\\/]src[\\/]css[\\/]?/, '');
      })
    )
    .pipe(gulp.dest(config.css.dest));

const processSwigTemplates = () =>
  gulp
    .src(config.html.src)
    .pipe(replace(/%%(.*?)%%/g, '$1'))
    .pipe(replace(/\\%/g, '%'))
    .pipe(
      swig({
        defaults: {
          cache: false,
          varControls: ['[[', ']]'],
          tagControls: ['[%', '%]'],
          cmtControls: ['{#', '#}']
        }
      })
    )
    .pipe(
      rename(p => {
        p.dirname = p.dirname.replace(/(.*?)[\\/]src[\\/]html/, 'nl');
      })
    )
    .pipe(gulp.dest(config.html.dest));

gulp.task(
  'build',
  gulp.parallel(
    buildMinifiedCss,
    buildReadableCss,
    preparePdfViewer,
    processSwigTemplates,
    copyWebodf,
    minifyJsFiles,
    copyJsFiles,
    copyBowerFiles
  )
);

gulp.task('default', gulp.series(['build']));

// DEV

const sources = [
  './client/src/**/*',
  './mintjs/src/**/*',
  './zaaksysteem/src/**/*',
  '../client/src/**/*',
  '../root/tpl/zaak_v1/nl_NL/**/*',
  './gulpfile.js'
];

const options = { interval: 500, usePolling: true };

gulp.task('build-dev', gulp.parallel(processSwigTemplates, copyJsFiles));
gulp.task('styles', buildReadableCss);

gulp.task('watch', () =>
  gulp.watch(sources, options, gulp.series(['build-dev']))
);
gulp.task('watch-styles', () =>
  gulp.watch(sources, options, gulp.series(['styles']))
);
