/*

 EzraDialog is the standard way to show a dialog. The html content for the dialog is retrieved
 from the server, then the callback function is executed to enable specific jquery components.

 options:
 - title (default '')
 - url  (required)
 - cgi_params (optional, will be sent as cgi parameters)
 - callback function with jquery inits (will be executed after loading)
 - layout (default 'medium', other options are 'small' and 'large')
 - which dialog (default '#dialog')

 up for discussion:
 - putting callbacks in html using the rel attribute. currently not supported.

 todo
 - verticaal centreren

 */

function debug(object) {
    //console.log(object);
}

$(document).ready(function () {


    $(document).on('click', '.ezra_dialog', function () {
        var obj = $(this);

        // enables dialog togeter with qtip - which replaces the title tag
        var title = obj.attr('title');
        var rel_options = getOptions(obj.attr('rel'));

        // this covers title being `null`, so leave it as it is
        if (title == undefined && rel_options != undefined && rel_options.hasOwnProperty('title')) {
            title = rel_options.title;
        }

        var dialog_options = {
            url: obj.attr('href'),
            title: title
        };

        var rel_options = getOptions(obj.attr('rel'));

        if (rel_options['ezra_dialog_layout']) {
            dialog_options['layout'] = rel_options['ezra_dialog_layout'];
        }

        ezra_dialog(dialog_options);
        return false;
    });

});


function ezra_dialog(options, callback) {

    var start = new Date().getTime();

    if (!options) {
        debug('options missing');
        return;
    }

    var url = options['url'];

    if (!url) {
        debug('url parameter missing');
        return;
    }

    var title = options['title'] || '';
    var cgi_params = options['cgi_params'] || '';
    var layout = options['layout'] || 'medium';
    var which = options['which'] || '#dialog';

    $.ztWaitStart();

    var dialog_element = $(which);

    if (!dialog_element) {
        debug('dialog element ' + which + ' not found');
        return;
    }

    var dialog_content = dialog_element.find('.dialog-content');

    dialog_content.html('');

    // always tidy up, so no confusing information can linger. a precaution.

    try {
        // calling option before init throws in 1.9.x/1.11.4
        dialog_element.dialog(
            'option',
            'beforeclose',
            function () {
                dialog_content.html('');
            }
        );
    } catch (error) {

    }

    // remove classes added
    var classList = dialog_content.attr('class').split(/\s+/);

    for (var i = 0; i < classList.length; i++) {
        if (classList[i] != 'dialog-content') {
            dialog_content.removeClass(classList[i]);
        }
    }

    var $window = $(window);
    var DIALOG_OFFSET = 15;
    var rootElement = document.documentElement;

    function getViewportSize() {
        return {
            width: rootElement.clientWidth,
            height: rootElement.clientHeight,
        };
    }

    function setDialogContentMaxHeight() {
        var $prev = dialog_element.prev();
        var height = getViewportSize().height;
        var maxHeight = (
            height
            - $prev.outerHeight()
            - (DIALOG_OFFSET * 2)
        );

        dialog_element.css({
            'max-height': maxHeight
        });
    }

    /**
     * The top and left offsets are sometimes calculated wrong.
     * Let jQuery UI do it's built-in thing first and always set the dialog's
     * size and position with offsets only in the `open` callback and
     * the window's resize event.
     *
     * @param {number} width
     * @param {boolean} isFullscreen
     */
    function setNormalizedDialogStyle(width, isFullscreen) {
        var viewportSize = getViewportSize();
        var availableWidth = (viewportSize.width - (DIALOG_OFFSET * 2));
        var horizontalOffset = DIALOG_OFFSET;

        if (!isFullscreen && (availableWidth > width)) {
            horizontalOffset = ((viewportSize.width - width) / 2);
        }

        dialog_element
            .parent()
            .css({
                top: DIALOG_OFFSET,
                right: horizontalOffset,
                bottom: isFullscreen ? DIALOG_OFFSET : '',
                left: horizontalOffset,
                width: 'auto'
            });
    }

    dialog_content.load(
        url,
        cgi_params,
        function (responseText, textStatus, XMLHttpRequest) {
            $.ztWaitStop();

            var width = 580;
            var position = ['center', DIALOG_OFFSET];
            var viewportSize = getViewportSize();
            var isFullscreen = false;

            if (layout === 'small') {
                width = 355;
            }

            if (layout === 'mediumsmall') {
                width = 460;
            }

            if (layout === 'mediumlarge') {
                width = 700;
            }

            if (layout === 'large') {
                width = 800;
            }

            if (layout === 'xlarge') {
                width = 960;
            }

            if (layout === 'fullscreen') {
                width = viewportSize.width - (DIALOG_OFFSET * 2);
                isFullscreen = true;
                dialog_element.addClass('dialog-fullscreen');
            }

            if (layout === 'medium-accordion') {
                width = 580;
                dialog_content.addClass('dialog-content-accordion');
            }

            if (layout === 'mediumlarge-accordion') {
                width = 700;
                dialog_content.addClass('dialog-content-accordion');
            }

            if (layout === 'large-accordion') {
                width = 800;
                dialog_content.addClass('dialog-content-accordion');
            }

            if (layout === 'new-layout-large') {
                width = 820;
                dialog_content.addClass('dialog-new-layout');
            }

            function postMessage(message) {
                if (window.parent !== window) {
                    window.parent.postMessage(message, '*');
                }
            }

            var normalizeSize = function () {
                setDialogContentMaxHeight();
                setNormalizedDialogStyle(width, isFullscreen);
            };

            dialog_element.dialog({
                position: position,
                width: width,
                height: 'auto',
                maxHeight: 1000,
                autoOpen: false,
                modal: true,
                resizable: false,
                draggable: true,
                zIndex: 3200,
                title: title,
                open: function () {
                    postMessage('IFRAME:OVERLAY:OPEN');
                    normalizeSize();
                },
                close: function () {
                    postMessage('IFRAME:OVERLAY:CLOSE');
                    $window.off('resize', normalizeSize);
                }
            }).addClass('smoothness').dialog('open');

            $window.resize(normalizeSize);

            if (textStatus !== 'success') {
                $(this).html('Er is een probleem opgetreden, ververs de pagina');
                return;
            }

            $('#accordion').accordion({
                autoHeight: false
            });
            ezra_tooltip_handling();
            initializeEverything(dialog_content);

            if (callback) {
                callback(dialog_content);
            }
        }
    );
}

