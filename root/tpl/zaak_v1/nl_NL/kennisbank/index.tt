[% BLOCK versies %]
    [% USE Scalar %]
    [% USE Dumper %]
    [% versies = entry.scalar.search_versies %]
<table class="kennisbank-versies">
    <thead>
        <tr>
            <th></th>
            <th>Versie</th>
            <th>Wijziging</th>
            <th>Auteur</th>
            <th>Datum</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        [% WHILE (versie = versies.next) %]
        <tr [% versie.id == entry.id ? 'class="huidig"' : '' %] >
            <td style="text-align:center">
                <input
                type="radio"
                name="versie_id"
                value="[% versie.id %]"
                [% versie.id == entry.id ? 'checked="checked"' : '' %]
                />
                <span></span>
            </td>
            <td>[% versie.versie %]</td>
            <td>[% (versie.commit_message || '-') %]</td>
            <td>[% versie.author_name %]</td>
            <td>[% versie.last_modified.strftime('%d-%m-%Y %H:%M') %]</td>
            <td><span class="icon icon-checked"></td>
        </tr>
        [% END %]
    </tbody>
</table>
[% END %]

[% BLOCK management %]
    [% USE Scalar %]

<table class="form">
    <tr>
        <td class="td250"><strong>Product is</strong></td>
        <td>
            <label for="internal">
            <input type="checkbox" name="internal" value="1"[% IF entry.internal %] checked[% END %] />
            Intern</label><br/>
            
            <label for="external">
            <input type="checkbox" name="external" value="1"[% IF entry.external %] checked[% END %] />
            Extern</label>
        </td>
    </tr>

    <tr>
        <td class="td250"><strong>Koppeling</strong></td>
        <td>
            <label>
            <input type="radio" name="coupling" value="default"[% IF entry.coupling == 'default' %] checked[% END %]/>
            Standaard gebruiken</label><br/>
            <label>
            <input type="radio" name="coupling" value="local"[% IF entry.coupling == 'local' %] checked[% END %]/>
            Eigen tekst gebruiken</label>
        </td>
    </tr>

    <tr>
        <td class="td250"><strong>Publicatie website</strong></td>
        <td>
            <label>
            <input type="radio" name="publication" value="published"[% IF entry.publication == 'published' %] checked[% END %]/> Actief</label><br/>
            <label>
            <input type="radio" name="publication" value="depublished"[% IF entry.publication == 'depublished' %] checked[% END %]/> Inactief</label>
        </td>
    </tr>
</table>
[% END %]

[% BLOCK beschrijvingen -%]

<div class="kennisbank">
    <div  class="kennisbank-content block-left">
        <div id="tabinterface" class="ui-tabs">
            <div class="header tabbed header-with-actions">
                [% IF bewerken %]
                    <table>
                        <tr>
                            <td>
                                <div class="kennisbank-titel"><span class="icon-kennisbank-[% entry_type %] icon-titel"></span><input type="text" name="naam" value="[% entry.naam %]" style="width:340px" /></div>
                            </td>
                            <td>
                                [% PROCESS widgets/general/validator.tt %]
                            </td>
                        </tr>
                    </table>
                    
                    <div class="button-group clearfix right">          
                        <button type="submit" class="wymupdate button button-secondary button-small add-tooltip" value="Opslaan" title="Wijziging opslaan">
                            <i class="icon icon-fontastic fontastic-disc-floppy-font"></i>
                        </button>
                    </div>
                    
                    <table>
                        <tr>
                            <td>
                                <div class="kies-bibliotheek-categorie">
                                    <span>Categorie:</span> [% PROCESS widgets/general/categorie.tt 
                                    fieldname = 'bibliotheek_categorie_id' 
                                    categorie_id = bibliotheek_categorie_id %]
                                </div>
                            </td>
                            <td>
                                [% PROCESS widgets/general/validator.tt %]
                            </td>
                        </tr>
                    </table> 
                                   
                [% ELSE %]
                
                    <div class="kennisbank-titel"><span class="icon-kennisbank-[% entry_type %] icon-titel"></span>[% entry.name %]</div>
                    
                [% END %]
            </div>
            <ul class="ui-rounded-corners-off ui-tabs-nav ui-helper-clearfix ui-widget-header">
                [% FOREACH component IN entry_config.components %]
                <li>
                    <a href="#[% component.name %]">
                        [% c.loc(component.name) | ucfirst %]
                    </a>
                </li>
                [% END %]
                [% IF bewerken && entry %]
                <li>
                    <a href="#versies">
                        [% c.loc('versies') | ucfirst %]
                    </a>
                </li>
                    [% IF entry_type == 'product' %]
                <li>
                    <a href='#management'>Beheer</a>
                </li>
                    [% END %]
                [% END %]
            </ul>
            [% FOREACH component IN entry_config.components %]
                [% componentname = component.name %]
                [% componentname_internal = component.name _ '_internal' %]
            <div class="ezra_kennisbank_content" id="[% component.name %]">
                [% IF bewerken || entry.$componentname.length > 1 %]
                <h3 class="kennisbank-content-title">Externe en interne tekst</h3>
                [% END %]

                [% IF bewerken %]
                    <textarea
                    class="kennisbank_textarea ezra_dagobert_editor"
                    name="[% componentname %]"
                    >[% entry.$componentname %]</textarea>
                [% ELSE -%]
                <p>
                    [% column = component.column %]
                    [% IF entry.$column.length > 1 %]
                    [% entry.$column %]
                    [% END %]
                </p>
                [% END %]

                [% IF bewerken || entry.$componentname_internal.length > 1 %]

                <h3 class="kennisbank-content-title">Interne tekst</h3>
                [% END %]

                [% IF bewerken %]
                    <textarea
                    class="kennisbank_textarea ezra_dagobert_editor"
                    name="[% componentname_internal %]"
                    >[% entry.$componentname_internal %]</textarea>
                [% ELSE -%]
                <p>
                    [% entry.$componentname_internal %]
                </p>
                [% END %]
            </div>
            [% END %]

            [% IF bewerken && entry %]
                <div id="versies">
                    [% PROCESS versies %]
                </div>

                [% IF entry_type == 'product'; %]
                <div id="management">
                    [% PROCESS management %]
                </div>
                [% END %]
            [% END %]
        </div>
    </div>
[% END -%]

[% BLOCK auteur %]
    <div class="kennisbank-sidebar block-sidebar">
        <div class="block">
            <div class="header">Info</div>
            <div class="blockcontent">
                <table>
                    <tr>
                        <td>
                            Laatst gewijzigd:
                        </td>
                        <td>
                            [% entry.last_modified.strftime('%d-%m-%Y %H:%M') %]
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Door:
                        </td>
                        <td>
                            [% entry.author_name %]
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Versie:
                        </td>
                        <td>
                            [% entry.versie %]
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
[% END -%]

[% BLOCK relaties -%]
    [% USE Scalar -%]
    <div class="kennisbank-sidebar block-sidebar">
        <div class="block ezra_kennisbank_add_relatie-container">
            <div class="header header-with-actions clearfix">
                <h1 class="left"><span class="icon-relaties">Relaties</span></h1>
                [% IF bewerken %]


                    <div class="ezra_actie_button_handling right">
                        <div class="button-group clearfix ezra_actie_button_handling_navigation">
                            <a class="button-secondary button button-small dropdown-toggle add-tooltip" title="Voeg een relatie toe" href="#relatietoevoegen">
                                <i class="icon icon-font-awesome icon-plus"></i>
                            </a>

                            <div class="ezra_actie_button_handling_popover">
                                <div class="popover-tip ezra_actie_button_handling_popover_tip"></div>
                                <div class="popover" id="popover_relatietoevoegen" style="margin: 10px -3px 0 0;">
                                    <div class="buttons">
                                    [% FOREACH relatie IN entry_config.relaties %]
                                        <a
                                        href="[% c.uri_for(relatie.url) %]"
                                        class="ezra_kennisbank_add_relatie"
                                        title="Kies [% relatie.name %]"
                                        rel="relatie: [% relatie.name %]"
                                        >[% relatie.name | ucfirst %]</a>
                                    [% END %]
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>


                [% END %]
            </div>
            

            <div class="blockcontent">
[% BLOCK disabled %]
                [% FOREACH relatie IN entry_config.relaties %]
                    [% relatiemethod    = 'gerelateerde_' _ relatie.name %]
                    [% relatieitems     = entry.scalar.$relatiemethod %]
    
                   
                    <div class="inner-block">
                        <h3>[% c.loc(relatie.name) | ucfirst %]</h3>
                        <ul class="ezra_kennisbank_add_relatie-relatie ezra_kennisbank_add_relatie-id-[% relatie.name %] kennisbank-relaties-[% c.loc(relatie.name) | lower %]">
                        <li
                        class="ezra_kennisbank_add_relatie-geenrelaties"
                        [% (relatieitems.count ? ' style="display: none;"' : '') %]>Geen relaties gevonden</li>
                        [% WHILE (relatie_item = relatieitems.next) %]
                            [% NEXT IF relatie_item.deleted %]
                            [% # ARGH, NEED TO FIX THIS IN COMPONENT CLASS OF
                               # ZAAKTYPE, SO WE JUST CAN CALL: naam
                            %]
                            <li>
                                [% IF relatie_item.zaaktype_node_id %]
                                [% relatie_item.entry_url %]
                                <a
                                class="ezra_nieuwe_zaak_tooltip-show"
                                href="[% c.uri_for(
                                    '/zaak/create', {
                                        'prefill_zaaktype_id'   => relatie_item.zaaktype_node_id.get_column('zaaktype_id'),
                                        'prefill_zaaktype_name' => relatie_item.zaaktype_node_id.titel
                                    }
                                ) %]">
                                [% ELSE %]
                                <a href="[% c.uri_for(relatie_item.entry_url) %]">
                                [% END %]
                                    [% (
                                        relatie_item.zaaktype_node_id
                                            ? relatie_item.zaaktype_node_id.titel
                                            : relatie_item.naam
                                    ) %]
                                </a>
                                [% IF bewerken %]
                                    <input
                                    type="hidden"
                                    name="[% relatie.name %]"
                                    value="[% relatie_item.id %]" />
                                    <a
                                    href="/noscript"
                                    class="right icon-del icon ezra_kennisbank_add_relatie-remove">
                                    </a>
                                [% END %]
                            </li>
                        [% END %]
                        </ul>
                    </div>
                [% END %]
[% END %]
            </div>
        </div>
    </div>
</div>

[% END -%]

[% PROCESS widgets/general/breadcrumbs.tt %]

[% IF bewerken -%]
    <form
    action="[% c.uri_for('/' _ c.req.path) %]"
    method="post"
    class="ezra_kennisbank_submit"
    >
    <input type="hidden" name="id" value="[% entry.id %]" />
    <input type="hidden" name="external_id" value="[% entry.external_id %]" />
[% END -%]

[% PROCESS beschrijvingen %]
[% PROCESS relaties %]


[% IF bewerken -%]
    </form>
[% END -%]
