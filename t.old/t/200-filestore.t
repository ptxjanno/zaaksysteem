#! perl
use TestSetup;
initialize_test_globals_ok;

use File::Temp qw/tempfile/;
use File::Slurp;

$zs->zs_transaction_ok(sub {
    my $result = $zs->create_filestore_ok(
        original_name => 'KEKEKEKE.docx',
        file_path     => $zs->config->{filestore_test_file_path},
    );

    ok open (my $file, '<', $zs->config->{filestore_test_file_path}), "Read test file";
    my $content = join "", <$file>;
    close $file;
    is $content, $result->content, "Content equals file content";

}, 'Get file content');

# test filestore's zeal to get rid of viruses
$zs->zs_transaction_ok(sub {
    throws_ok(sub {
        my ($fh, $filename) = tempfile(TMPDIR => 1);
        print $fh "i am infected";
        close $fh;
        note "scanner $filename";
        $schema->resultset('Filestore')->clamscan($filename);
    }, qr/Virus aangetroffen in bestand/, 'Fails when a virus is scanned');
}, 'Block infected file');

$zs->zs_transaction_ok(sub {
    lives_ok(sub {
        $schema->resultset('Filestore')->clamscan($zs->config->{filestore_test_file_path});
    }, 'Lives when no virus is scanned');
}, 'Clean file can pass');


$zs->zs_transaction_ok(sub {
    my $result = $schema->resultset('Filestore')->filestore_create({
        original_name => 'KEKEKEKE.docx',
        file_path     => $zs->config->{filestore_test_file_path},
    });
    ok $result, 'Created filestore entry';
    my @set = ('uuid', 'size', 'mimetype', 'md5', 'date_created');
    for my $s (@set) {
        ok $result->$s, "$s is set";
    }
}, 'filestore_create');


$zs->zs_transaction_ok(sub {
    throws_ok(sub {
        my ($fh, $filename) = tempfile(TMPDIR => 1);
        print $fh "i am infected";
        close $fh;

        my $result = $schema->resultset('Filestore')->filestore_create({
            original_name => 'KEKEKEKE.docx',
            file_path     => $filename,
        });
    }, qr/Virus aangetroffen in bestand/, 'Fails when infected file is supplied');
}, 'filestore_create bork out when trying to upload a virus');


$zs->zs_transaction_ok(sub {
    throws_ok sub {
        $schema->resultset('Filestore')->filestore_create({
            original_name => 'No extension',
            file_path     => $zs->config->{filestore_test_file_path},
        });
    }, qr/File does not have an extension/, 'No extension fails';

    throws_ok sub {
        $schema->resultset('Filestore')->filestore_create({
            original_name => 'Invalid extension.wtf',
            file_path     => $zs->config->{filestore_test_file_path},
        });
    }, qr/Bestandstype niet toegestaan: .wtf/, 'Invalid extension fails';
}, 'filestore_create without extension and invalid extension');

$zs->zs_transaction_ok(sub {
    my $no_ext = $schema->resultset('Filestore')->filestore_create({
        original_name    => 'No extension',
        ignore_extension => 1,
        file_path        => $zs->config->{filestore_test_file_path},
    });
    ok $no_ext, 'File without extension with override is allowed';
}, 'filestore_create without extension with override');

$zs->zs_transaction_ok(sub {
    my $no_ext = $schema->resultset('Filestore')->filestore_create({
        original_name    => 'Invalid extension.wtf',
        ignore_extension => 1,
        file_path        => $zs->config->{filestore_test_file_path},
    });
    ok $no_ext, 'File with invalid extension with override is allowed';
}, 'filestore_create with invalid extension with override');

$zs->zs_transaction_ok(sub {
    throws_ok sub {
        $schema->resultset('Filestore')->assert_allowed_filetype('fire.it.up');
    }, qr/Bestandstype niet toegestaan: .up/,
         'Incorrect extension';
}, 'assert_allowed_filetype with invalid file extension');

$zs->zs_transaction_ok(sub {
    my $name = 'ADRIAAAAN';
    throws_ok sub {
        $schema->resultset('Filestore')->assert_allowed_filetype($name);
    }, qr/File does not have an extension: $name/,
         'Incorrect extension';
}, 'assert_allowed_filetype without file extension');

$zs->zs_transaction_ok(sub {
    lives_ok sub {
        $schema->resultset('Filestore')->assert_allowed_filetype('extension.sql');
    }, 'Correct extension: .sql';
}, 'assert_allowed_filetype lives when extension is valid');

$zs->zs_transaction_ok(sub {
    my $file = $zs->create_file_ok;

    throws_ok sub {
        $file->update_properties({
            subject  => $zs->get_subject_ok,
            accepted => 'DIT MAG NIET',
            case_id  => undef,
            case_type_document_id => 'Dit ook niet.',
        });
    }, qr/Invalid options given: accepted/, 'invalid options fail';
}, 'update_properties with invalid options');

$zs->zs_transaction_ok(sub {
    my $filestore = $zs->create_filestore_ok;
    my $store = $zs->get_ustore_ok;
    my $path  = $store->getPath($filestore->uuid);
    ok $path, 'Got path from test ustore';
    is $filestore->get_path, $path, 'Path matches';
}, 'get_path');

$zs->zs_transaction_ok(sub {
    my $filestore = $zs->create_filestore_ok;
    ok $filestore->verify_integrity, 'Verified integrity';
}, 'verify_integrity');

$zs->zs_transaction_ok(sub {

    my $filestore = $zs->create_filestore_ok(file_path => 't/inc/Documents/openoffice_document.pdf', original_name => 'openoffice_document.pdf');
    my $result    = $filestore->generate_thumbnail;
    ok $result->thumbnail_uuid, 'Generated thumbnail';
    is $result->get_thumbnail, '*File::UStore::$fh', 'Got UStore file handle';

    my $fs = $zs->config->{filestore_location};
    like $result->get_thumbnail_path, qr/$fs/, 'Got path';
}, 'thumbnail tests');

$zs->zs_transaction_ok(sub {
    my $filestore = $zs->create_filestore_ok(file_path => 't/inc/Documents/openoffice_document.pdf', original_name => 'openoffice_document.pdf');
    my $result    = $filestore->generate_thumbnail;
    ok $result->thumbnail_uuid, 'Generated thumbnail';
    is $result->ustore->get($result->thumbnail_uuid), '*File::UStore::$fh',
        'Got UStore file handle';
}, 'get_thumbnail');

$zs->zs_transaction_ok(sub {
    my $filestore = $zs->create_filestore_ok(file_path => 't/inc/Documents/testdocument.doc', original_name => 'testdocument.pdf');
    is($filestore->mimetype, 'application/msword', 'Got correct mimetype for word files');
}, 'converts mimetype correctly');

zs_done_testing();
