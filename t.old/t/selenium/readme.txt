The testscenario of a functionality is located where it's initiated from.

For example: The testfile for 'Referential attributes' is located under 'Manager - Casetypes', and not under 'Users - Case'.
For example: The testfile for 'Email at PIP feedback' is located under 'Manager - Configuration', and not under 'Customer - PIP'.

The structure is as follows:
- code
- customer
	- pip
		- case
		- mycases
		- mycredentials
	- webform
		- citizen
		- company
		- general
		- prefilledcustomer
- documents
- general
	- authentication
	- basictests
- manager
	- catalog
		- attributes
		- casetypes
		- categories
		- documenttemplates
		- emailtemplates
		- import
		objecttypes
	- configuration
	- interfaces
		- datastore
		- overview
		- subscriptions
		- transactions
	- log
	- users
- user
	- case
	- contactsearch
	- dashboard
	- documentintake
	- greenbutton
	- object
	- objectsearch
	- registrationform
	- spotenlighter
	- useroverview
