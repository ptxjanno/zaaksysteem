package TestFor::General::Case::Suspend;
use base qw(Test::Class);

use TestSetup;

sub case_suspend : Tests {
    $zs->zs_transaction_ok(sub {
        $schema->default_resultset_attributes->{delayed_touch} = Zaaksysteem::Zaken::DelayedTouch->new;
        my $case = $zs->create_case_ok;

        my $initial_registratiedatum = $case->registratiedatum;
        my $new_term = $initial_registratiedatum->clone->add(days => 20);
        $case->set_verlenging($new_term);

        is $case->streefafhandeldatum,
            $new_term,
            "Case has been prolonged to a total of 20 days";

        $case->suspend({});

        # take a margin because this process may run concurrent.
        my $stalled_since_seconds = DateTime->now->epoch - $case->stalled_since->epoch;
        ok $stalled_since_seconds < 60, "Stalled_since is less than a minute ago";
        ok !$case->stalled_until, "No enddate set for suspension period";

        $case->suspend({
            suspension_term_amount => 15,
            suspension_term_type => 'kalenderdagen'
        });

        my $stalled_until_seconds = DateTime->now->add(days => 15)->epoch - $case->stalled_until->epoch;
        ok $stalled_until_seconds < 60, "Enddate set correctly for suspension period";

        $case->resume({ status => 'open' });

        ok !$case->stalled_until, "Stalled_until has been cleared";

    }, 'Case suspend and resume');


    $zs->zs_transaction_ok(sub {
        $schema->default_resultset_attributes->{delayed_touch} = Zaaksysteem::Zaken::DelayedTouch->new;

        # resume may not operate in the future, therefore, start the case in the past
        # so we have some slack.
        my $registratiedatum = DateTime->now->add(days => -20);
        my $case = $zs->create_case_ok(
            registratiedatum => $registratiedatum
        );

        my $streefafhandeldatum = $case->streefafhandeldatum;

        # execute twice: suspend for three days and resume
        # check that the new streefafhandeldatum is 6 days after the original one

        for my $factor (1..2) {
            $case->suspend({});

            my $stalled_since = $registratiedatum->clone->add(days => 4 * $factor)->dmy;
            my $stalled_until = $registratiedatum->clone->add(days => (4 * $factor) + 3)->dmy;

            $case->resume({
                status => 'open',
                stalled_since => $stalled_since,
                stalled_until => $stalled_until
            });
        }

        my $expected = $streefafhandeldatum->add(days => 6)->epoch;

        my $diff_seconds = abs ($case->streefafhandeldatum->epoch - $expected);

        ok $diff_seconds < 60, "New streefafhandeldatum as expected";

    }, 'Case suspend and resume twice');

}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

