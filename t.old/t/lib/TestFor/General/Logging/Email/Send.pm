package TestFor::General::Logging::Email::Send;
use base qw(Test::Class);

use TestSetup;

use Zaaksysteem::DB::Component::Logging::Email::Send;

sub logging_email_send : Tests {
    my $subject = 'ik ben een subject';
    my $recipient = 'ik_ben_een_recipient@ik_ben_een_host.ik_ben_een_top_level_domain';

    $zs->zs_transaction_ok(sub {

        my $logging = $zs->create_logging_ok(
            event_type => 'email/send',
            event_data => {
                subject => $subject,
                recipient => $recipient,
            }
        );

        $logging->_apply_roles;
        my $onderwerp = $logging->onderwerp;

        ok $onderwerp =~ m/$subject/, 'Subject filled in';
        ok $onderwerp =~ m/$recipient/, 'Recipient filled in';

    }, 'Subject and recipient filled in');


    $zs->zs_transaction_ok(sub {
        my $cc = 'ik_ben_een_cc';
        my $bcc = 'ik_ben_een_bcc';

        my $logging = $zs->create_logging_ok(
            event_type => 'email/send',
            event_data => {
                subject => $subject,
                recipient => $recipient,
                cc => $cc,
                bcc => $bcc,
            }
        );

        $logging->_apply_roles;
        my $onderwerp = $logging->onderwerp;

        ok $onderwerp =~ m/$cc/, 'Subject filled in';
        ok $onderwerp =~ m/$bcc/, 'Recipient filled in';

    }, 'CC and BCC filled in');
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
