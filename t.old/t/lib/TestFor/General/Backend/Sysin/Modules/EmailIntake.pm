package TestFor::General::Backend::Sysin::Modules::EmailIntake;
use base qw(ZSTest);
use TestSetup;

=head1 NAME

TestFor::General::Backend::Sysin::Modules::EmailIntake - Email intake tests

=cut

use File::Basename;
use File::Copy;
use File::Spec::Functions qw(catfile);
use File::Temp;

use Mail::Track;

use Zaaksysteem::Backend::Sysin::Modules::EmailIntake;

sub zs_emailintake_documents : Tests {
    $zs->zs_transaction_ok(
        sub {
            my $interface = $zs->create_interface_ok(
                module           => 'emailintake',
                name             => 'Documentintake',
                interface_config => {
                    api_user => 'intake@meuk.nl',
                    type     => Zaaksysteem::Backend::Sysin::Modules::EmailIntake::DOCUMENT_INTAKE,
                },
            );

            my $files = $zs->schema->resultset('File')->search_rs({})->all;
            my $msg = _smaf('marco.mime');

            my $pt = $interface->process_trigger(
                'process_mail', {
                    api_user => 'intake@meuk.nl',
                    message  => $msg,
            });
            isa_ok($pt, 'Zaaksysteem::Model::DB::Transaction');

            my $tr = $zs->get_from_zaaksysteem(
                resultset => 'TransactionRecord',
                search    => {transaction_id => $pt->id},
                options   => {rows => 1},
            )->first;

            like(
                $tr->preview_string,
                qr/Email van '.+' naar '.+' met onderwerp '.+' is verwerkt/,
               "Preview string matches"
            );

            my $fs = $zs->schema->resultset('File')->search_rs({})->all;
            is($fs, $files + 3, "Three files found");
        },
        "Documentintake via mail",
    );

    $zs->zs_transaction_ok(
        sub {
            my $interface = $zs->create_interface_ok(
                module           => 'emailintake',
                name             => 'Documentintake',
                interface_config => {
                    api_user => 'intake@meuk.nl',
                    type     => Zaaksysteem::Backend::Sysin::Modules::EmailIntake::DOCUMENT_INTAKE,
                },
            );

            my $mt = Mail::Track->new();
            my %params = (
                to      => 'intake@meuk.nl',
                from    => 'foobar@example.org',
                subject => 'Foobar',
            );

            my $employee = $zs->create_medewerker_ok();
            my $username = $employee->subject_id->username;

            my $msg = $mt->prepare({ %params, Body => 'This is some content', });
            $msg->add_body(
                {
                    content      => "Some strange characters like Gar\xc3\xa7on",
                    content_type => 'text/plain',
                }
            );

            my $dst = File::Temp->new(UNLINK =>1, SUFFIX => '.pdf');
            my $src = catfile(qw(t inc Documents openoffice_document.pdf));

            my $ok = copy($src, $dst->filename) or die $!;
            ok($ok, "File is copied");

            $msg->add_attachment(
                path     => $dst->filename,
                filename => "$username-iets.pdf",
            );

            my $message;
            my $opts;
            {
                no warnings qw(redefine once);
                local *Email::Sender::Simple::send = sub {
                    my $self = shift;
                    my $entity = shift;
                    $opts  = shift;
                    $message = $entity->stringify();
                };
                $msg->send;
            }

            my $subject = $zs->create_subject_ok();
            my $tmp_mail = File::Temp->new(UNLINK =>1, SUFFIX => '.mime');
            print $tmp_mail $message;
            close($tmp_mail);
            my $file = $zs->schema->resultset('File')->file_create({
                name       => 'tijdelijk.mime',
                file_path  => $tmp_mail->filename,
                ignore_extension => 1,
                db_params  => {
                    created_by => 'betrokkene-medewerker-' . $subject->id,
                    queue      => 0,
                },
            })->id;

            my $pt = $interface->process_trigger(
                'process_mail', {
                    api_user => 'intake@meuk.nl',
                    message  => $file,
            });
            isa_ok($pt, 'Zaaksysteem::Model::DB::Transaction');

            my $frs = $schema->resultset('File')->search_rs({intake_owner => "betrokkene-medewerker-" . $employee->subject_id->id });
            is($frs->count, 1, "One document found");
            my $f = $frs->first;
            is_deeply(
                {
                    filename => $f->filename,
                    extension => $f->extension,
                },
                {
                    filename => 'iets.pdf',
                    extension => '.pdf'
                },
                "File properties are correct"
            );

        },
        "Documentintake via mail with username-document.pdf",
    );
}

sub zs_emailintake_generic : Tests {
    $zs->zs_transaction_ok(
        sub {
            my $zt = $zs->create_zaaktype_predefined_ok();

            my $total_cases = $zs->get_from_zaaksysteem(resultset => 'Zaak')->all;

            my $interface = $zs->create_interface_ok(
                module           => 'emailintake',
                name             => 'Generiek',
                zaaktype         => $zt,
                interface_config => {
                    api_user => 'generiek@meuk.nl',
                    type     => Zaaksysteem::Backend::Sysin::Modules::EmailIntake::GENERIC,
                },
            );

            my %kenmerken = (
                subject => 'onderwerp',
                body    => 'inhoud',
                from    => 'aanvrager'
            );

            $zs->set_attribute_mapping_on_interface_ok(
                interface => $interface,
                casetype => $zt,
                attributes => \%kenmerken,
            );

            $zt->zaaktype_node_id->zaaktype_definitie_id->update({ preset_client => "betrokkene-medewerker-" . $zs->set_current_user->id });

            my $msg = _smaf('marco.mime');

            my $pt = $interface->process_trigger(
                'process_mail', {
                    api_user => 'generiek@meuk.nl',
                    message  => $msg,
            });
            isa_ok($pt, 'Zaaksysteem::Model::DB::Transaction');

            my $tr = $zs->get_from_zaaksysteem(
                resultset => 'TransactionRecord',
                search    => {transaction_id => $pt->id},
                options   => {rows => 1},
            )->first;

            like(
                $tr->preview_string,
                qr/Email van '.+' naar '.+' met onderwerp '.+' is verwerkt/,
                "Preview string matches"
            );

            my $cases = $zs->get_from_zaaksysteem(
                resultset => 'Zaak',
                options   => { order_by => { '-desc' => 'id' } }
            );
            is($cases->all, $total_cases + 1, "One case created");
            my $case = $cases->first();

            is($case->onderwerp, "Banana", "Subject is correct");

            my @zk = sort { $a cmp $b } map { $_->bibliotheek_kenmerken_id->magic_string } $case->zaak_kenmerken;
            my @bk = sort { $a cmp $b } values %kenmerken;
            cmp_deeply(\@zk, \@bk, "All kenmerken found in the case as well");

            my $f = $zs->get_from_zaaksysteem(resultset => 'File', search => { case_id => $case->id });

            is($f->all, 5, "Five files found, we also have the HTML");
            is($zs->get_from_zaaksysteem(resultset => 'File', search => { case_id => $case->id, accepted => 1})->count, 0, "Mails are not accepted as files");


        },
        "Zaak creation via mail",
    );
}

sub zs_emailintake_olo : Tests {
    $zs->zs_transaction_ok(
        sub {
            $zs->stuf_0312;

            my $total_cases = $zs->get_from_zaaksysteem(resultset => 'Zaak')->all;
            my $interface = $zs->create_interface_ok(
                module           => 'emailintake',
                name             => 'OLO',
                interface_config => {
                    api_user => 'olo@meuk.nl',
                    type     => Zaaksysteem::Backend::Sysin::Modules::EmailIntake::OLO,
                },
            );

            my $olo = $zs->create_interface_omgevingsloket_ok();

            $zs->set_fallback_requestors($olo);

            my $msg = _smaf('olo_mail.mime');

            my $pt = $interface->process_trigger(
                'process_mail', {
                    api_user => 'olo@meuk.nl',
                    message  => $msg,
            });

            my $cases = $zs->get_from_zaaksysteem(
                resultset => 'Zaak',
                options   => {
                    order_by => { '-desc' => 'id' }
                }
            );
            is($cases->all, $total_cases + 1, "One case created");

            my $case = $cases->first();

            is($case->onderwerp, "extra info ", "Subject is correct");

            my $f =  $zs->get_from_zaaksysteem(resultset => 'File', search => { case_id => $case->id, accepted => 1});
            is($zs->get_from_zaaksysteem(resultset => 'File', search => { case_id => $case->id, accepted => 1})->count, 1, "One accepted mail");


        },
        "Online omgevingsloket via mail",
    );
}

sub _smaf {
    my $msg = $zs->open_testsuite_mail(shift);
    my $rs  = $schema->resultset('File');

    my $fh = File::Temp->new(UNLINK => 1, SUFFIX => '.mime');
    my $path = $fh->filename;
    print $fh $msg;
    close($fh);
    my ($filename, undef, $ext) = fileparse($path, '\.[^.]*');

    my $subject = $zs->create_subject_ok();

    my $mailfile = $rs->file_create({
        name       => $filename . $ext,
        file_path  => $path,
        ignore_extension => 1,
        db_params  => {
            created_by => 'betrokkene-medewerker-' . $subject->id,
            queue      => 0,
        },
    });
    return $mailfile->id;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
