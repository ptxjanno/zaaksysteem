package TestFor::General::Object::Process;

use base 'ZSTest';

use TestSetup;

use Zaaksysteem::Object;
use Zaaksysteem::Object::Environment;
use Zaaksysteem::Object::Model;

use Test::DummyObjectRelation;

sub setup : Test(startup) {
    my $self = shift;

    $self->{ model } = Zaaksysteem::Object::Model->new(
        schema => $zs->schema
    );
}

sub object_environment : Tests {
    my $self = shift;

    my $env1 = Zaaksysteem::Object::Environment->new;

    ok !$env1->exists('a'), 'env->exists() ok for non-existant var';
    ok !$env1->get('a'), 'env->get() ok for non-existant var';

    lives_ok { $env1->set('a', 1); } 'env->set() lives when binding new var';

    ok $env1->exists('a'), 'env->exists() ok for existant var';

    is $env1->get('a'), 1, 'env->get() ok for existant var (implies env->set is ok too)';

    dies_ok { $env1->set('a', 2) } 'env->set() dies on re-binding var';

    my $env2 = $env1->new_child;

    isa_ok $env2, 'Zaaksysteem::Object::Environment', 'env->new_child() ok';

    ok $env2->exists('a'), 'env->exists() ok for existant var in parent';

    is $env2->get('a'), 1, 'env->get() ok for existant var in parent';

    lives_ok { $env2->set('a', 2 ) } 'env->set() lives when binding var existant in parent';

    is $env2->get('a'), 2, 'env->get() ok for var bound in child and parent';

    dies_ok { $env2->set('a', 3) } 'env->set() dies ok if var is bound in child env';

    is $env1->get('a'), 1, 'child->set() does not interfere with parent vars';
}

sub object_environment_defaults : Tests {
    my $self = shift;

    my $env = Zaaksysteem::Object::Environment->new;

    my $object = Test::DummyObjectRelation->new(attr => 'i haes value');

    for my $pname (qw[get set log]) {
        isa_ok $env->get($pname), 'Zaaksysteem::Object::Process',
            "default environment has '$pname' process";
    }

    is $env->process('get', { object => $object, name => 'attr' }), $object->attr,
        'get-process return value same as object attribute value';

    $env->process('set', { object => $object, name => 'attr', value => 'another' });

    is $object->attr, 'another', 'set-process sets value on object';

    is $env->process('get', { object => $object, name => 'attr' }), $object->attr,
        'get-process returns updated value';
}

sub object_process_log_calls : Tests {
    my $self = shift;

    my $loglevel;
    my $logline;

    my $env = Zaaksysteem::Object::Environment->new;

    my $log_wrapped = $env->new_child(log => Zaaksysteem::Object::Process->new(
        description => 'logwrapper',
        environment_profile => { level => 'Str', message => 'Str' },
        log_call => 0,
        implementation => sub {
            my ($p, $e) = @_;

            $loglevel = $e->get('level');
            $logline = $e->get('message');
        }
    ));

    my $object = Test::DummyObjectRelation->new;

    $log_wrapped->process('set', { object => $object, name => 'attr', value => 'derp' });

    my $oldline = $logline;

    ok $logline =~ m[^Process.*started$], 'Logmessage checks out';
    is $loglevel, 'info', 'Loglevel checks out';

    $log_wrapped->process('get', { object => $object, name => 'attr' });

    is $oldline, $logline, 'Process with log_call=>0 generates no logline';
}

sub object_process_builtins : Tests {
    my $self = shift;

    my $model = $self->{ model };

    my $object = Test::DummyObjectRelation->new(attr => 'abc');

    dies_ok {
        $model->process('i_dont_exist')
    } 'model->process dies when attempting to execute non-existant process';

    lives_ok {
        $model->process('log', { level => 'info', message => 'Some message' })
    } 'log process is available';

    $zs->txn_ok(sub {
        my $new_object = $model->object_environment->process('save', { object => $object });

        my $stack = $model->object_environment->get('stack');

        is ref $stack, 'ARRAY', 'Action stack is an array';
        is scalar @{ $stack }, 1, 'One action pushed on stack';

        my $action = $stack->[0];
        is ref $action, 'HASH', 'Action is a hash';
        is $action->{ name }, 'save_object', 'Correct action pushed';

        isa_ok $new_object, 'Zaaksysteem::Object', 'object model action save retval';

        my $saved_object = Test::DummyObjectRelation->new(attr => 'cba');
        $model->save_object(object => $saved_object);

        my $get = $model->process('get', { name => 'attr', object => $saved_object });
        is $get, $saved_object->attr, 'process "get" baseline';

        my $set = $model->process('set', { name => 'attr', object => $saved_object, value => 'xyz' });

        is $saved_object->attr, 'xyz', 'process "set" baseline';

        my $get2 = $model->process('get', { name => 'attr', object => $saved_object });
        is $get2, 'xyz', 'process "set+get" baseline';
    });
}

sub object_process_tree : Tests {
    my $self = shift;

    my $env = Zaaksysteem::Object::Environment->new;

    my $tempvar;

    my $proc1 = Zaaksysteem::Object::Process->new(
        description => 'proc1',
        implementation => sub {
            my ($p, $e) = @_;

            my $value = $e->get('value');
            my $args = {};

            if ($value) {
                $args->{ value } = $value;
            }

            $e->process('proc2', $args);
        }
    );

    my $proc2 = Zaaksysteem::Object::Process->new(
        description => 'proc2',
        implementation => sub {
            my ($p, $e) = @_;

            $tempvar = $e->get('value');
        }
    );

    my $sub_env = $env->new_child(
        proc1 => $proc1
    )->new_child(
        proc2 => $proc2,
    );

    lives_ok { $proc1->eval($sub_env) } 'Executing process with subprocess in other env';

    ok !defined $tempvar, 'Check variable remains undefined';

    dies_ok { $proc1->eval($env) } 'Executing process with subprocess in empty env dies';

    $env->set('value', 'derp');

    lives_ok { $proc1->eval($sub_env) } 'Executing process with subprocess in other env, mutated';

    is $tempvar, 'derp', 'New root-env variable picked up by processes in subenvs';

    lives_ok { $sub_env->process('proc1', { value => 'herp' }) } 'Executing process via env with masked variable';

    is $tempvar, 'herp', 'Implicit sub-env masks variable set in root-env';
}

sub object_type_constraints : Tests {
    my $self = shift;

    use Moose::Util::TypeConstraints qw[subtype role_type];

    my $object_tester = role_type('Zaaksysteem::Object::Reference');

    my $object = Test::DummyObjectRelation->new;

    lives_ok { $object->object_ref($object) } 'Setup object relation';
    lives_ok { $object->object_typed_ref($object) } 'Setup object/type relation';
    dies_ok { $object->object_other_ref($object) } 'Setup invalid object/type/relation';
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2014, 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
