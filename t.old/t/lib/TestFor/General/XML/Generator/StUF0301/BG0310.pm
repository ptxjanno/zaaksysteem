package TestFor::General::XML::Generator::StUF0301::BG0310;
use base 'ZSTest';

use Encode qw(encode_utf8 decode_utf8);
use File::Basename;
use File::Spec::Functions qw(catfile catdir);
use TestSetup;
use Zaaksysteem::XML::Generator::StUF0301::BG0310;

# ./zs_prove -v t/lib/TestFor/General/XML/Generator/StUF0301/BG0310.pm


sub _dummy_stuurgegevens {
    my $self = shift;

    return (
        stuurgegevens => {
            zender => {
                applicatie => encode_utf8("t\xebst"),
                organisatie => 'Gemeente X',
                gebruiker  => 'thedoors',
            },
            ontvanger => {
                applicatie => 'test2',
                organisatie => 'Gemeente X',
            },
            referentienummer => 31337,
            tijdstipBericht => '20150101060000000',
            @_
        },
    );
}

### Poor mans attribute
my $generator;
sub generator {
    return $generator if $generator;

    return ($generator = Zaaksysteem::XML::Generator::StUF0301::BG0310->new());
}

sub bg0310_search : Tests {
    my $self        = shift;

    my $rv          = $self->generator->search_nps(
        writer => {
            $self->_dummy_stuurgegevens(()),
            gelijk => {
                'inp.bsn' => {
                    exact => 1,
                    value => '562565456'
                },
            },
        }
    );

    diag(explain($rv));
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
