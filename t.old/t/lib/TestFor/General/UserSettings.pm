package TestFor::General::UserSettings;
use base qw(ZSTest);

use Zaaksysteem::UserSettings;

use TestSetup;

=head1 NAME

TestFor::General::UserSettings - Usersettings Tests

=head1 SYNOPSIS

    See USAGE tests

    Quick test of this single file from within vagrant
    ./zs_prove -v t/lib/TestFor/General/UserSettings.pm

=head1 DESCRIPTION

These tests prove the interactions on Subject->settings.

=head1 USAGE

=head2 usettings_usage_favourite_searches

=cut

sub usettings_usage_favourite_searches : Tests {
    my $self        = shift;

    my $settings    = Zaaksysteem::UserSettings->new_from_settings(
        {
          'backwards_compatible_setting' => {
            'setting_uno' => 'value'
          },
          'favourites' => [
            {
              'id' => '2806b1cf-faa0-4304-bdb2-8b24768dcd23',
              'order' => 10,
              'reference_type' => 'casetype',
              'reference_id' => '2806b1cf-faa0-4304-bdb2-8b24768dcdc4'
            },
            {
              'id' => '2806b1cf-faa0-4304-bdb2-8b24768dcd24',
              'order' => 20,
              'reference_type' => 'casetype',
              'reference_id' => '2806b1cf-faa0-4304-bdb2-8b24768dcdc5'
            },
          ]
        },
        schema => $schema
    );

    my $favs        = $settings->favourites;
    my $fav         = $favs->create(
        {
            reference_type    => 'casetype',
            reference_id => '2806b1cf-faa0-4304-bdb2-8b24768dcdc4',
        }
    );

    isa_ok($fav, 'Zaaksysteem::UserSettings::Favourites::Row');
    is($fav->reference_id, '2806b1cf-faa0-4304-bdb2-8b24768dcdc4', 'Got correct reference_id');

    ### Generate a few
    $favs->create({reference_id => $_, reference_type => 'casetype'}) for (
        '2806b1cf-faa0-4304-bdb2-8b24768dcdc5',
        '2806b1cf-faa0-4304-bdb2-8b24768dcdc6',
        '2806b1cf-faa0-4304-bdb2-8b24768dcdc7',
        '2806b1cf-faa0-4304-bdb2-8b24768dcdc8',
        '2806b1cf-faa0-4304-bdb2-8b24768dcdc9',
    );

    $favs->create({reference_id => $_, reference_type => 'case'}) for (
        '2806b1cf-faa0-4304-bdb2-8b24768dcec5',
        '2806b1cf-faa0-4304-bdb2-8b24768dcec6',
        '2806b1cf-faa0-4304-bdb2-8b24768dcec7',
        '2806b1cf-faa0-4304-bdb2-8b24768dcec8',
        '2806b1cf-faa0-4304-bdb2-8b24768dcec9',
    );

    my $last = $favs->create(
        {
            reference_type    => 'casetype',
            reference_id => '2806b1cf-faa0-4304-bdb2-8b24768dcd10'
        }
    );

    my $json = $settings->to_settings;

    is($json->{backwards_compatible_setting}->{setting_uno}, 'value', 'Found backwards compatible setting');
    is(@{ $json->{favourites} }, 14, 'Found 14 favourite search rows');

    ok($json->{favourites}->[0]->{id}, 'Favourite search contains an id');
    ok($json->{favourites}->[0]->{reference_id}, 'Favourite search contains a reference_id');


    ### Search (get objects)
    is($favs->search({ reference_type => 'casetype' }), 9, 'Got 9 rows of searches');
    my ($first) = $favs->search({ reference_type => 'casetype' });
    isa_ok([ $favs->search({ reference_type => 'casetype' }) ]->[0], 'Zaaksysteem::UserSettings::Favourites::Row');

    ### Update a object
    $favs->update(
        $first,
        {
            reference_id         => '2806b1cf-faa0-4304-bdb2-8b24768dcd11',
        }
    );

    is($first->reference_id, '2806b1cf-faa0-4304-bdb2-8b24768dcd11', 'Got new reference_id for updated row');


    ### Delete a object
    $favs->delete($first);

    is($favs->search({ reference_type => 'casetype' }), 8, 'Succesfully deleted a row');

    ### Move a row
    my $fifth = [ $favs->search({ reference_type => 'casetype' }) ]->[4];

    $favs->update($fifth, { order => 2 });

    ### Check for uniqueness of rows, and find occurence
    my $i = 10;
    for my $row ($favs->search({ reference_type => 'casetype' })) {
        fail('Ordering of favourites not valid') unless $i == $row->order;

        if ($fifth->id eq $row->id) {
            is($fifth->order, $i, 'Correctly moved row to place: ' . $i);
        }

        $i += 10;
    }
}

=head2 usettings_usage_labs

=cut

sub usettings_usage_labs : Tests {
    my $self        = shift;

    my $settings    = Zaaksysteem::UserSettings->new_from_settings(
        {
          'backwards_compatible_setting' => {
            'setting_uno' => 'value'
          },
          'labs'                         => {
              'case_version'      => '2',
              'desktop_version'   => '2',
          },
        },
        schema => $schema
    );

    my $labs        = $settings->lab;

    is($labs->case_version, 2, 'Got version 2 of labs: case_version');
    is($labs->desktop_version, 2, 'Got version 2 of labs: desktop_version');

    $labs->case_version(1);
    $labs->desktop_version(1);

    ### Reload settings
    my $newsettings = $settings->to_settings();
    $settings       = Zaaksysteem::UserSettings->new_from_settings($newsettings, schema => $schema);
    $labs           = $settings->lab;

    is($labs->case_version, 1, 'Got version 1 of labs: case_version');
    is($labs->desktop_version, 1, 'Got version 1 of labs: desktop_version');
}

=head1 IMPLEMENTATION

Implementation tests, test on the implementation, and not necessarily to show you how to use
this code.

=head2 usettings_create

Create usersettings

=cut

sub usettings_create : Tests {
    my $anon        = {anonymous_data1 => 'anonymous_value1' };

    my $settings    = Zaaksysteem::UserSettings->new_from_settings(
        {
            anonymouskey1 => $anon,
        },
        schema => $schema
    );

    isa_ok($settings, 'Zaaksysteem::UserSettings', 'Got valid object');
    is_deeply($settings->{anonymouskey1}, $anon, 'Anonymous hash set');
}





1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
