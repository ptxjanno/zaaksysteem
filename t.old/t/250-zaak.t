#! perl
use TestSetup;
initialize_test_globals_ok;

$zs->zs_transaction_ok(sub {

    my $case    = $zs->create_case_ok();
    my $subject = $zs->create_subject_ok();

    $case->wijzig_behandelaar({'betrokkene_identifier' => 'betrokkene-medewerker-' . $subject->id});

    my $behandelaar = $case->behandelaar;
    isa_ok($behandelaar, "Zaaksysteem::Schema::ZaakBetrokkenen");
    is($behandelaar->betrokkene_id, $subject->id, "The correct behandelaar is set");

}, 'Change behandelaar on a case');


$zs->zs_transaction_ok(sub {

        my $resultaat = Zaaksysteem::TestUtils::DEFAULT_ZAAKTYPE_RESULTAAT;
        my $case = $zs->create_case_ok();
        ok($case->is_in_phase('registratie_fase'), "Zit in registratiefase");

        my $subject = $zs->create_subject_ok();
        $case->wijzig_behandelaar({'betrokkene_identifier' => 'betrokkene-medewerker-' . $subject->id});

        $case->advance(
            object_model     => $zs->object_model,
            betrokkene_model => $zs->betrokkene_model,
            current_user     => $zs->set_current_user,
        );
        ok(!$case->is_in_phase('registratie_fase'), "Zit niet in registratiefase");
        ok(!$case->is_in_phase('afhandel_fase'), "Zit niet in afhandelfase");

        $case->wijzig_behandelaar({'betrokkene_identifier' => 'betrokkene-medewerker-' . $subject->id});
        $case->resultaat($resultaat);

        $case->advance(
            object_model     => $zs->object_model,
            betrokkene_model => $zs->betrokkene_model,
            current_user     => $zs->set_current_user,
        );
        ok($case->is_in_phase('afhandel_fase'), "Zit in afhandelfase");

}, 'case->advance');

zs_done_testing();
