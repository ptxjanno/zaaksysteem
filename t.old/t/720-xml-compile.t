#! perl

use TestSetup;
initialize_test_globals_ok;

use Zaaksysteem::XML::Compile::Backend;

my $xmlb    = Zaaksysteem::XML::Compile::Backend->new(
    'home'  => './'
);

$zs->zs_transaction_ok(sub {
    can_ok($xmlb, $_) for qw/
        home
        log

        add_class
    /;
}, 'Chack backend class');

$zs->zs_transaction_ok(sub {
    $xmlb->add_class('Zaaksysteem::StUF::0204::Instance');
    ok($xmlb->can("stuf0204"), 'Found accessor "stuf0204" on xml compile class');

}, 'Added StUF class');

$zs->zs_transaction_ok(sub {
    my $stuf_class = $xmlb->stuf0204;

    #isa_ok($stuf_class, 'Zaaksysteem::XML::Compile::Instance');
    can_ok($stuf_class, $_) for qw/home schema _load_instance name elements _reader_config schemas/;

    is($stuf_class->name, 'stuf0204', 'Found correct name on StUF instance');

    # diag(explain($stuf_class->kennisgevingsbericht('writer', { stuurgegevens => 'bla'})));

    can_ok($stuf_class, 'kennisgevingsbericht');

}, 'Verified StUF class');

zs_done_testing;
