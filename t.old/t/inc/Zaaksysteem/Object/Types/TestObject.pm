package Zaaksysteem::Object::Types::TestObject;
use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Object';
with qw{
    Zaaksysteem::Object::Roles::Log
    Zaaksysteem::Object::Roles::Relation
    Zaaksysteem::Object::Roles::Security
};

use Zaaksysteem::Types qw(IntervalStr PackageElementStr);

=head1 NAME

Zaaksysteem::TestObject - Test object with all bells & whistles

=head1 DESCRIPTION

=head1 ATTRIBUTES

None

=cut

sub new_empty {
    my $self = shift;

    $self->new();
}

=head2 relatable_types

Returns a list of types that can be related to a ScheduledJob

=cut

sub relatable_types {
    return [
        'object',
        'case',
        'casetype',
    ];
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
