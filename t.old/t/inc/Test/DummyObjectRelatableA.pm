package Test::DummyObjectRelatableA;

use Moose;

extends 'Zaaksysteem::Object';

with 'Zaaksysteem::Object::Roles::Relation';

sub relatable_types {
    return qw[dummy_object_relatable_b];
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
