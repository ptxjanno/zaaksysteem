#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;

use Zaaksysteem::Object::Model;

my @REQUIRED_OBJECT_RESULT_PARAMS = qw/
    object_type
    id
    values
/;

use Pg::hstore;

$zs->zs_transaction_ok(sub {
    $zs->create_case_ok;

    my $objects = Zaaksysteem::Object::Model->new(schema => $schema)->rs;

    my $lastcase = $schema->resultset('Zaak')->search({},{order_by => { '-desc' => 'me.id'} })->first;

    my $zql = Zaaksysteem::Search::ZQL->new('SELECT case.number,case.casetype.name, case.date_of_completion FROM case WHERE case.number = ' . $lastcase->id);

    my $rs = $zql->apply_to_resultset($objects);

    is($rs->count, 1, 'Applied ZQL to resultset, found 1 entry');

    my $first = $rs->first;

    is_deeply([map({ $_->{label} } @{ $first->csv_header })], ['Zaaknummer','Zaaktype', 'Afhandeldatum' ], '$row->csv_header: Found all requested attributes in csv_header');
    is(@{ $first->csv_header }, 3 , '$row->csv_header: Exactly three columns in header');

    like($first->human_values->[0], qr/^\d+$/, 'Found zaaknummer');
    like($first->human_values->[1], qr/[a-zA-Z]/, 'Found casetype');

    is(@{ $first->csv_data }, 3 , '$row->csv_data: Exactly three columns in data');
}, 'Test CSV functionality of ObjectData');

zs_done_testing;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

