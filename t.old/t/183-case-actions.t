#! perl
use warnings;
use strict;

use lib 't/inc';
use TestSetup;

use FakeLogger;
use Zaaksysteem::Zaaktypen;

initialize_test_globals_ok;

my $logger = FakeLogger->new();

sub get_zaaktypen_model {
    return new Zaaksysteem::Zaaktypen(dbic => $zs->schema, log => $logger);
}

sub create_action_casetype {
    my $zaaktype_node = $zs->create_zaaktype_node_ok;
    my $casetype = $zs->create_zaaktype_ok(node => $zaaktype_node);
    $zaaktype_node->zaaktype_id($casetype->id);
    $zaaktype_node->update;

    my $zaaktype_status = $zs->create_zaaktype_status_ok(
        status => 1,
        fase   => 'registratiefase',
        node   => $zaaktype_node
    );

    my $afhandelfase = $zs->create_zaaktype_status_ok(
        status => 2,
        fase   => 'afhandelfase',
        node   => $zaaktype_node
    );

    $zs->create_zaaktype_sjabloon_ok(
        status => $zaaktype_status,
        automatisch_genereren => 1
    );

    $zs->create_zaaktype_notificatie_ok(
        status => $zaaktype_status,
        automatic => 1
    );

    return $casetype;
}

# need to investigate why $phase->zaaktype_sjablonens etc don't give results.
# leaving that for later, leaving this code since it'd be a waste throw away
$zs->zs_transaction_ok(sub {
    $schema->default_resultset_attributes->{delayed_touch} = Zaaksysteem::Zaken::DelayedTouch->new;
    my $casetype = create_action_casetype;
    my $case = $zs->create_case_ok(
        casetype => $casetype
    );

    my $phase = $case->zaaktype_node_id->zaaktype_statuses->first;
    my $model = get_zaaktypen_model;
    #diag explain $model->retrieve_session($casetype->id);

    # reset case
    $case = $schema->resultset('Zaak')->find($case->id);
    #map { diag 'sjabloon', explain {$_->get_columns} } $phase->zaaktype_sjablonens->search->all;
    my $rs = $zs->schema->resultset('CaseAction');
    my @actions = $rs->create_from_phase($case, $phase);

    TODO: {
        local $TODO = "Case actions don't seem to yield any results here";
        ok undef, "Allocation actions are executed last";
    }
    #map { diag explain {$_->get_columns} } @actions;

}, 'Order of case actions');



zs_done_testing();
