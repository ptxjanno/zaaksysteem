#!/usr/bin/env bash

set -e

usage() {
    cat <<OEF
$(basename $0) OPTIONS

Build a docker perl base image and sets the correct tag in the Docker
files. Before pushing to the registery be sure to login :). Be advised
that you need to run this from the root of the git repository.

OPTIONS:

-f      Force a rebuild - without cache or tag checking
-p      Push the build to the registry
-t      Set a custom tag
-T      Skip tag checking
-r      Set the registry name

OEF
    exit
}

add_opts() {
    opts="$opts $*"
}

registry_name=registry.gitlab.com/zaaksysteem/zaaksysteem-perl

_docker_perl='docker/Dockerfile.perl'
_docker_backend='docker/Dockerfile.backend'
_cpan=cpanfile

opts=""
push=0
rebuild=0;


for i in $_docker_perl $_docker_backend $_cpan
do
    if [ ! -f "$i" ];
    then

        echo "No $i file found" >&2
        exit 2;
    fi
done

tag=$(cat $_docker_perl $_cpan | shasum -a 256 | cut -b 1-7)
tag_check=1

while getopts "fhpt:r:T" name
do
    case $name in
        f) rebuild=1;;
        p) push=1;;
        t) tag=$OPTARG;;
        T) tag_check=0;;
        h) usage;;
        r) registry_name=$OPTARG;;
    esac
done
shift $((OPTIND - 1))

do_push() {
    if [ $push -eq 1 ]
    then
        docker push $registry
        exit $?
    else
        echo "Not pushing to the registry, supply -p option to do that"
    fi
}

registry=$registry_name:$tag

cur=$(grep "FROM" $_docker_backend |head -1 | grep $registry_name| sed -e 's/^FROM //')
current_tag=$(echo $cur | awk -F: '{print $NF}' | sed -e 's/\s\+as .*//');

if [ $rebuild -eq 0 ] && [ $tag_check -eq 1 ] && [ "$tag" = "$current_tag" ];
then
    echo "Tags ($tag) are equal, will not rebuild the image";
    do_push
    exit 2;
fi

if [ $rebuild -eq 1 ]
then
    add_opts --no-cache
fi

docker build $opts -f "$_docker_perl" -t $registry .

sed -i -e "s/$current_tag/$tag/" $_docker_backend

do_push

# vim: filetype=sh syntax=sh
