#! /bin/bash

set -e

export OS_AUTH_URL=http://swift:35357/v2.0
export OS_USERNAME=demo
export OS_PASSWORD=demo
export OS_TENANT_NAME=test

if ! grep LocalSwift etc/zaaksysteem.conf; then
    echo "No LocalSwift file store found in etc/zaaksysteem.conf."
    echo ""
    echo "Please put it in (copy from etc/zaaksysteem.conf.dist)"
    exit 1
fi

swift.pl put dev

cd /opt/filestore
for file in zaaksysteem/storage/*/*/*/*/*/*; do
    base=$(basename "$file")
    uuid=${base#zs_}

    swift.pl put "dev/${uuid}" "$file"
done

echo "✨ All done! ✨"
echo "You can now execute the following SQL statement to update the database:"
echo ""
echo "    BEGIN;"
echo "    UPDATE filestore SET storage_location = '{LocalSwift}' WHERE storage_location = '{LocalUStore}';"
echo "    COMMIT;"
echo ""
echo "Afterwards you can remove the <Plugin::Zaaksysteem::Filestore> block for"
echo "'LocalUStore' from etc/zaaksysteem.conf"
