#! /bin/bash

set -e

if [ $# -lt "1" ]; then
    echo "USAGE: $0 [LIST OF SQL-UPDATE FILES]"
    echo
    echo "Loads update scripts on test template, and dumps is back"
    exit
fi
GIT_ROOT=$(git rev-parse --show-toplevel)
if [ -z "$GIT_ROOT" ]; then
    echo "Please run this script from a Zaaksysteem git repository"
    exit 1
fi

cd "$GIT_ROOT"

if ! docker-compose ps | grep -q 'Up'; then
    echo "Did not find any running containers. Did you run 'docker-compose up'?"
    exit 1
fi

TEMPLATE="db/test-template.sql"
EMPTYTEMPLATE="db/template.sql"

PROCID=$$
DATABASE="zs_test_${USER}_${PROCID}"

dbexists=$(docker-compose exec database psql -U zaaksysteem -t -c "SELECT 1 FROM pg_database WHERE datname = '$DATABASE'")
dbexists=$(echo "$dbexists" | tr -d '[:space:]')
if [ "$dbexists" != "" ]; then
    echo "Temporary database $DATABASE already (still?) exists. Please try again."
    echo $dbexists | od -x
    exit
fi

UPGRADE_FILE=$(mktemp db/db_upgrade.XXXXXXXX)
for args in $*
do
    cat $args >> $UPGRADE_FILE;
done

echo "Creating test database: $DATABASE"
docker-compose exec database psql -U zaaksysteem -c "CREATE DATABASE $DATABASE ENCODING 'UTF-8';"

echo "Loading '$TEMPLATE' into '$DATABASE'"
docker-compose exec database psql -U zaaksysteem -f "/opt/zaaksysteem/$TEMPLATE" "$DATABASE"

echo "Executing upgrades to $DATABASE"
docker-compose exec database psql -U zaaksysteem -f "/opt/zaaksysteem/$UPGRADE_FILE" "$DATABASE"

echo "Recreating DBIx::Class schema"
perlbackend=backend
if [ "$(docker-compose ps|awk '{ print $1; }'|grep 'perl-api')" ]; then
    perlbackend=perl-api
fi
docker-compose run --rm $perlbackend /opt/zaaksysteem/dev-bin/db_redeploy.sh "dbi:Pg:dbname=${DATABASE};host=database" zaaksysteem zaaksysteem123

echo "Dumping database '$DATABASE' to '$TEMPLATE'"
docker-compose exec -T database pg_dump -U zaaksysteem --no-owner "$DATABASE" > "$TEMPLATE"

echo "Dumping database '$DATABASE' schema to '$EMPTYTEMPLATE'"
docker-compose exec -T database pg_dump -U zaaksysteem --no-owner --schema-only "$DATABASE" > "$EMPTYTEMPLATE"

echo "Cleaning up database: '$DATABASE'"
docker-compose exec database psql -U zaaksysteem -c "DROP DATABASE $DATABASE"

rm "${UPGRADE_FILE}"
