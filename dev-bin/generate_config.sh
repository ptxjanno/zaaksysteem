#! /bin/bash

set -euo pipefail

if [ -t 1 ]; then
    bold=$(echo -en '\033[1m')
    red=$(echo -en '\033[31m')
    default=$(echo -en '\033[0m')
else
    bold=""
    red=""
    default=""
fi

copy_conf() {
    local src=$1
    local dst=$2

    if [ ! -e "$dst" ]; then
        cp -v "$src" "$dst"
    else
        echo "Not creating $dst - file exists."
    fi
}

copy_conf etc/virus_scanner-service.conf.dist etc/virus_scanner-service.conf
copy_conf etc/zaaksysteem.conf.dist etc/zaaksysteem.conf
copy_conf etc/log4perl.conf.dist etc/log4perl.conf
copy_conf etc/default_customer.conf.dist etc/customer.d/default.conf
copy_conf etc/minty_config.conf.dist etc/minty_config.conf

found=0
for config in etc/customer.d/*.conf; do
    if ! grep -q -F "zaaksysteemdb.url" "$config"; then
        echo " 😮${red} Configuration file ${bold}$config${default}${red} does not contain ${bold}zaaksysteemdb.url${default}${red} configuration.${default}"
        found=1
    fi

done
if [ $found -eq 1 ]; then
    echo ""
    echo "Python services won't be able to connect to the database."
    echo "See etc/default_customer.conf.dist for an example"
fi

echo "Done!"
