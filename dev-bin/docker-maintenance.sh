#! /bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/.."

docker_override="$DIR"/docker-compose.override.yml
# Make sure the versions are correct
if [ -e $docker_override ]
then
    version=$(grep "^version:" $DIR/docker-compose.yml)
    version_override=$(grep "^version:" $docker_override)
    if [ "$version" != "$version_override" ]
    then
        echo "Setting $docker_override to $version";
        sed -i -e "s/^version:.*/$version/" $docker_override
    fi
fi

set -e

echo "Stopping and rebuilding containers"
docker-compose rm -svf $@

# Regenerate configs so we don't mount files as directories...?
"$DIR"/dev-bin/generate_config.sh

if [ ${ZS_LOCAL_BUILD:-0} -eq 1 ]
then
    docker-compose build $@
else
    docker-compose pull $@
    docker-compose build --pull $@
fi

if [ ${ZS_DOCKER_START:-0} -eq 1 ]
then
    docker-compose up -d $@
else
    docker-compose up --no-start --remove-orphans $@
fi

if [ ${ZS_DOCKER_CLEANUP:-1} -eq 1 ]
then
    "$DIR"/dev-bin/docker-cleanup.sh
fi

if [ "${ZS_GIT_HOOKS_INSTALL:-1}" -eq 1 ]
then
    read -s -n1 -p \
        "Press any key to install git-hooks. CTRL+C to abort" \
        || exit 0

    "$DIR"/dev-bin/install-git-hooks.sh
fi
