package Zaaksysteem::Test::Filestore::Engine::S3;
use utf8;
use Moose;
extends 'Zaaksysteem::Test::Moose';

=head1 NAME

Zaaksysteem::Test::Filestore::Engine::S3 - Tests for S3 filestore

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::Filestore::Engine::S3

=cut

use BTTW::Tools::RandomData qw(:uuid);
use Zaaksysteem::Filestore::Engine::S3;
use Zaaksysteem::Test;

sub test__build_s3 {
    my %new_args;

    no warnings 'redefine';
    local *Net::Amazon::S3::new = sub {
        my $class = shift;
        %new_args = @_;
        return mock_one();
    };

    my $fs = Zaaksysteem::Filestore::Engine::S3->new(
        name => 'S3Store',
        storage_bucket => 'sub-emmer',
        bucket => 'emmer',
        host => 'foobar.com',
        use_iam_role => 0,
        access_key => '31337',
        secret_key => 's3kr1t',
        secure => 1,
        use_virtual_host => 1,
    );

    my $s3 = $fs->s3;

    cmp_deeply(
        \%new_args,
        {
            host => 'foobar.com',
            aws_access_key_id => '31337',
            aws_secret_access_key => 's3kr1t',
            secure => 1,
            use_virtual_host => 1,
        },
        "S3 instance creation"
    );

    $fs = Zaaksysteem::Filestore::Engine::S3->new(
        name => 'S3Store',
        storage_bucket => 'sub-emmer',
        bucket => 'emmer',
        use_iam_role => 1,
        access_key => '31337',
        secret_key => 's3kr1t',
        secure => 0,
        use_virtual_host => 0,
    );

    $s3 = $fs->s3;

    cmp_deeply(
        \%new_args,
        {
            use_iam_role => 1,
            secure => 0,
            use_virtual_host => 0,
        },
        "S3 instance creation"
    );
}

sub test_download_url {
    my ($bucket, $uuid, $time);
    no warnings 'redefine';
    local *Net::Amazon::S3::new = sub {
        return mock_one(
            bucket => sub {
                $bucket = shift;

                return mock_one(
                    query_string_authentication_uri => sub {
                        $uuid = shift;
                        $time = shift;

                        return "https://somehost/somepath"
                    }
                );
            }
        );
    };

    my $fs = Zaaksysteem::Filestore::Engine::S3->new(
        name => 'S3Store',
        storage_bucket => 'sub-emmer',
        bucket => 'emmer',
        host => 'foobar.com',
        use_iam_role => 0,
        access_key => '31337',
        secret_key => 's3kr1t',
        secure => 1,
        use_virtual_host => 1,
    );
    
    my $file_uuid = generate_uuid_v4();
    my $du = $fs->download_url($file_uuid);

    is($du, "/download/s3/https/somehost/somepath", "S3 download path");
    is($bucket, 'emmer', "Bucket name passed in correctly");
    ok($time > time + 30, "Validity time > 30 seconds into the future");
    ok($time < time + 90, "Validity time < 90 seconds into the future");
    is($uuid, "sub-emmer/$file_uuid", "Correct file requested from S3");
}

sub test_get_path {
    my $mock_fs = mock_one(
        get_fh => sub { return "fh"; }
    );

    is(
        Zaaksysteem::Filestore::Engine::S3::get_path($mock_fs, generate_uuid_v4()),
        "fh",
        "get_path is a thin wrapper around get_fh"
    );
}

sub test_get_fh {
    my ($uuid, $method, $location) = shift;

    no warnings 'redefine';
    local *Zaaksysteem::Filestore::Engine::S3::_get_bucket = sub {
        return mock_one(
            get_key_filename => sub {
                $uuid = shift;
                $method = shift;
                $location = shift;

                return "meta";
            }
        );
    };
    my $fs = Zaaksysteem::Filestore::Engine::S3->new(
        name => 'S3Store',
        storage_bucket => 'sub-emmer',
        bucket => 'emmer',
        host => 'foobar.com',
        use_iam_role => 0,
        access_key => '31337',
        secret_key => 's3kr1t',
        secure => 1,
        use_virtual_host => 1,
    );

    my $file_uuid = generate_uuid_v4();
    my $fh = $fs->get_fh($file_uuid);

    is($uuid, "sub-emmer/$file_uuid", "File UUID passed correctly");
    is($method, "GET", "GET method used to retrieve file");
    isa_ok($fh, "File::Temp", "Temp file handle returned for file access");
    is($fh, $location, "The same temp file handle was given to the download call");
}

sub test_write {
    my ($s3_filename, $filename, $options);

    my $mock_fs = mock_one(
        _get_filename => sub {
            return "filename here";
        },
        _get_bucket => sub {
            return mock_one(
                add_key_filename => sub {
                    $s3_filename = shift;
                    $filename = shift;
                    $options = shift;
                    return 1;
                }
            );
        },
    );

    my $file_uuid = generate_uuid_v4();
    my $store_uuid = Zaaksysteem::Filestore::Engine::S3::write(
        $mock_fs,
        $file_uuid,
        undef,
        "filename"
    );

    is($store_uuid, $file_uuid, "Returned UUID is correct");
    is($s3_filename, "filename here", "UUID passed to upload method correctly");
    is($filename, "filename", "Filename passed correctly");
    cmp_deeply(
        $options,
        { "content_type" => "application/octet-stream" },
        "Options are passed correctly."
    );
}

sub test_write_failure {
    my $mock_fs = mock_one(
        bucket => sub { return "emmer"; },
        _get_filename => sub { return "filename here" },
        _get_bucket => sub {
            return mock_one(
                add_key_filename => sub {
                    return 0;
                }
            );
        },
    );

    my $file_uuid = generate_uuid_v4();
    throws_ok(
        sub {
            Zaaksysteem::Filestore::Engine::S3::write(
                $mock_fs,
                $file_uuid,
                undef,
                "filename"
            );
        },
        qr#Upload of 'filename here' to S3 bucket 'emmer' failed#
    );
}

sub test_erase {

    my $model = Zaaksysteem::Filestore::Engine::S3->new(
        name           => 'Villa testsuite',
        bucket         => 'emmer',
        storage_bucket => 'tuinhuis',
        access_key     => 1,
        secret_key     => 1,
    );

    my $uuid = generate_uuid_v4();
    my $delete_key;

    my $override = override(
        'Net::Amazon::S3::Bucket::delete_key' => sub {
            my $bucket = shift;
            $delete_key = shift;
            return 1;
        }
    );

    $model->erase($uuid);
    is($delete_key, "tuinhuis/$uuid", "We have deleted the right file");

}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
