package Zaaksysteem::Test::Zorginstituut0202;
use Moose;
extends 'Zaaksysteem::Test::Moose';
use Zaaksysteem::Test;

=head1 NAME

Zaaksysteem::Test::Zorginstituut - Test the plugin modules of ZS::Zorginstituut

=head2 SYNOPSIS

    prove -l :: Zaaksysteem::Test::Zorginstituut

=cut

use BTTW::Tools qw(dump_terse);
use BTTW::Tools::RandomData qw(:uuid);

use IO::All;
use List::Util qw(none);

use Zaaksysteem::Test::XML qw(:all);

use Zaaksysteem::XML::Generator::WMO::0202;
use Zaaksysteem::Zorginstituut::Messages::0202::WMO301;
use Zaaksysteem::Zorginstituut::Model::0202::WMO301;
use Zaaksysteem::Zorginstituut::Provider::Generic;;
use Zaaksysteem::Zorginstituut::Reader;

my $di01_endpoint = 'https://localhost:9001';

sub mock_provider {
    return mock_one(
        di01_endpoint => $di01_endpoint,
        send_request  => sub { return 1 },
    );
}

sub test_setup {
    my $test = shift;
    $test->next::method;

    my $test_method = $test->test_report->current_method->name;

    if ($test_method =~ /generic_live/ && none { $_ =~ /^ZS_ZI_GEN_/} keys %ENV) {
        $test->test_skip("Skipping $test_method: live test");
    }
    elsif ($test_method =~ /live_wmo301/ && !$ENV{LIVE_WMO301}) {
        $test->test_skip("Skipping $test_method: live test");
    }
    elsif ($test_method =~ /live_wmo302/ && !$ENV{LIVE_WMO302}) {
        $test->test_skip("Skipping $test_method: live test");
    }
}

sub test_models {

    my @models = Module::Pluggable::Object->new(
        search_path => 'Zaaksysteem::Zorginstituut::Model',
        require     => 1,
        except      => qr/^Zaaksysteem::Zorginstituut::Model::[0-9]+$/,
    )->plugins;

    is(@models, 3, "Found all the models");

    my @methods = qw(code description type version label shortname);
    foreach my $plugin (@models) {
        foreach (@methods) {
            can_ok($plugin, $_);
        }
    }

}

sub test_providers {

    my @models = Module::Pluggable::Object->new(
        search_path => 'Zaaksysteem::Zorginstituut::Provider',
        require     => 1,
    )->plugins;

    is(@models, 3, "Found all the providers");

    my $schema    = mock_dbix_schema;
    my $interface = mock_interface;

    my @methods = qw(shortname label build_request send_request);

    foreach my $plugin (@models) {
        foreach (@methods) {
            can_ok($plugin, $_);
        }
    }
}

sub test_provider_generic {

    my $ca_cert       = 'This is your CA certificate';
    my $client_cert   = 'This is your public key';
    my $client_secret = 'This is your private key';

    open my $ca,     '<', \$ca_cert;
    open my $cert,   '<', \$client_cert;
    open my $secret, '<', \$client_secret;

    my $model = Zaaksysteem::Zorginstituut::Provider::Generic->new(
        di01_endpoint           => $di01_endpoint,
        ca_certificate          => $ca,
        client_public_key       => $cert,
        client_private_key      => $secret,
    );

    isa_ok($model, 'Zaaksysteem::Zorginstituut::Provider::Generic');

    my $ua = $model->ua;

    use Zaaksysteem::Version;
    my $zs_version = "Zaaksysteem/$VERSION";

    is($ua->agent, $zs_version, "Useragent is $zs_version");

    my @keys = qw(verify_hostname SSL_ca_file SSL_cert_file SSL_key_file);

    cmp_deeply(
        [sort $ua->ssl_opts ],
        [sort @keys ],
        "SSL opts passed to BTTW::Tools::UA"
    );

    is($ua->ssl_opts('SSL_ca_file'), $model->ca_certificate,
        "CA certificate set");
    is(
        $ua->ssl_opts('SSL_cert_file'),
        $model->client_private_key,
        "client certificate set"
    );
    is(
        $ua->ssl_opts('SSL_key_file'),
        $model->client_private_key,
        "client key set"
    );

    my $req = $model->build_request(
        endpoint      => 'http://foo.bar.nl',
        'soap-action' => 'washme',
        xml           => 'foo',
    );

    _inspect_header(
        $req,
        "Generic provider header inspection",
        soapaction => 'washme'
    );

    {
        my @bv03 = qw(t data zorginstituut bv03.xml);
        my $override = override(
            'LWP::UserAgent::request' => sub {
                HTTP::Response->new(
                    200, 'OK',
                    ['Content-Type' => 'text/xml'],
                    scalar io->catfile(@bv03)->slurp
                );
            }
        );
        my $res = $model->send_request($req);
        ok($res, "Got a request");
    }
}

sub test_provider_generic_live {

    my $provider = Zaaksysteem::Zorginstituut::Provider::Generic->new(
        di01_endpoint  => $ENV{ZS_ZI_GEN_DI01_ENDPOINT},
    );

    isa_ok($provider, 'Zaaksysteem::Zorginstituut::Provider::Generic');

    my $interface   = mock_interface;
    my $schema      = mock_dbix_schema;

    my $model = Zaaksysteem::Zorginstituut::Model::0202::WMO301->new(
        schema            => $schema,
        municipality_code => $ENV{ZS_ZI_GEN_MUNICIPALITY_CODE},
        interface         => $interface,
        provider          => $provider,
    );

    isa_ok($model, 'Zaaksysteem::Zorginstituut::Model::0202::WMO301');

    my $generator  = Zaaksysteem::XML::Generator::WMO::0202->new();
    my $aanbieder  = $ENV{ZS_ZI_GEN_AANBIEDER};

    my $xml = generate_301(
        $generator,
        afzender  => $model->municipality_code,
        aanbieder => $aanbieder
    );

    $xml = $model->stuf_envelope(
        'ggk0210-di01',
        verzend_applicatie       => 'Zaaksysteem testsuite',
        gemeentenaam             => $model->municipality_name,
        agb_code                 => $aanbieder,
        object_subscription_uuid => generate_uuid_v4,
        remote_subscription_id   => generate_uuid_v4,
        berichttype              => "WMO301",
        date                     => DateTime->now(),
        gemeentecode             => $model->municipality_code,
        xml                      => $xml,

        application_major_version => '0002',
        application_minor_version => '0020',
    );

    $xml = $model->add_soap_envelope($xml);

    my $soap_action = 'http://www.stufstandaarden.nl/koppelvlak/ggk0210/ggk_Di01';
    my $req = $provider->build_request(
        endpoint      => $provider->di01_endpoint,
        'soap-action' => $soap_action,
        xml           => $xml,
    );

    _inspect_header(
        $req,
        "Generic header live inspection",
        soapaction => $soap_action,
    );

    my $res;
    lives_ok(
        sub {
            $res = $provider->send_request($req);
        },
        "WMO301 send to Generic provider at $ENV{ZS_ZI_GEN_DI01_ENDPOINT} for $ENV{ZS_ZI_GEN_MUNICIPALITY_CODE}"
    );
    diag("Request: " . $req->as_string);
    diag("Response: " . $res) if $res;
}

sub _inspect_header {
    my ($req, $testname, %opts) = @_;

    my $headers = $req->headers;
    my %headers;
    foreach (keys %opts) {
        $headers{$_} = $headers->header($_);
    }

    cmp_deeply(\%headers, \%opts, "$testname: Headers validated");
}

sub test_bv03_wmo {
    my $schema    = mock_dbix_schema;
    my $interface = mock_interface;
    my $provider  = mock_provider;

    my $model     = Zaaksysteem::Zorginstituut::Model::0202::WMO301->new(
        schema           => $schema,
        municipality_code => 9999,
        interface        => $interface,
        provider         => $provider,
    );

    isa_ok($model, 'Zaaksysteem::Zorginstituut::Model::0202::WMO301');

    my $xml = $model->stuf_envelope(
        'ggk0210-bv03',

        agb_code        => '01234567',
        date            => DateTime->now(),
        our_reference   => generate_uuid_v4,
        their_reference => generate_uuid_v4,
        gemeentecode    => $model->municipality_code,
    );


    my $xml_schema = XML::Compile::Schema->new(
        [
            qw( share/xsd/zorginstituut/ggk0210_patch_04/ggk0210/0301/stuf0301.xsd)
        ]
    );

    validate_xml(
        schema    => $xml_schema,
        xml       => $xml,
        namespace => 'http://www.egem.nl/StUF/StUF0301',
        type      => 'Bv03Bericht',
        testname  => 'Craft StUF bv03 message',
    );
}

sub test_fo01_wmo {
    my $schema    = mock_dbix_schema;
    my $interface = mock_interface;
    my $provider  = mock_provider;

    my $model     = Zaaksysteem::Zorginstituut::Model::0202::WMO301->new(
        schema           => $schema,
        municipality_code => 9999,
        interface        => $interface,
        provider         => $provider,
    );

    isa_ok($model, 'Zaaksysteem::Zorginstituut::Model::0202::WMO301');

    my $xml = $model->stuf_envelope(
        'ggk0210-fo01',

        agb_code        => '01234567',
        date            => DateTime->now(),
        our_reference   => generate_uuid_v4,
        their_reference => generate_uuid_v4,
        gemeentecode    => $model->municipality_code,
        error_code      => "FooBar",
        error_side      => "client",
        error           => "Some rror",
    );

    my $xml_schema = XML::Compile::Schema->new(
        [
            qw( share/xsd/zorginstituut/ggk0210_patch_04/ggk0210/0301/stuf0301.xsd)
        ]
    );

    validate_xml(
        schema    => $xml_schema,
        xml       => $xml,
        namespace => 'http://www.egem.nl/StUF/StUF0301',
        type      => 'Fo01Bericht',
        testname  => 'Craft StUF Fo03 message',
    );

    # mimic the backend::sysin::module::zorginstituut soap_error call
    my @wmo = qw(share xml-templates zorginstituut 0202 wmo wmo302-minimal.xml);
    $xml = $model->stuf_envelope(
        'ggk0210-du01',

        agb_code                 => '01234567',
        object_subscription_uuid => generate_uuid_v4,
        remote_subscription_id   => generate_uuid_v4,
        berichttype              => "WMO301",
        date                     => DateTime->now(),
        gemeentecode             => $model->municipality_code,
        xml                      => scalar io->catfile(@wmo)->slurp,
    );
    $xml = $model->add_soap_envelope($xml);

    my $du01 = $model->get_stuf_du01($xml);

    lives_ok(
        sub {
            my $xml = $model->stuf_envelope(
                'ggk0210-fo01',

                agb_code        => $du01->findvalue('//s:zender/s:organisatie'),
                date            => DateTime->now(),
                their_reference => $du01->findvalue('//s:referentienummer'),
                gemeentecode    => $model->municipality_code,
                # generated in transaction
                our_reference   => generate_uuid_v4, # Transaction uuid
                error_code      => "FooBar",
                error_side      => "client",
                error           => "Some rror",
            );
        },
        "WMO302 to Fo01 messages"
    );
}

sub test_wmo301 {

    my $schema    = mock_dbix_schema;
    my $interface = mock_interface;
    my $provider  = mock_provider;

    my $model = Zaaksysteem::Zorginstituut::Model::0202::WMO301->new(
        schema           => $schema,
        interface        => $interface,
        municipality_code => 9999,
        provider         => $provider,
    );

    isa_ok($model, 'Zaaksysteem::Zorginstituut::Model::0202::WMO301');

    my @methods = qw(send_301 stuf_envelope);

    foreach (@methods) {
        if (!can_ok($model, $_)) {
            return;
        }
    }

    my $xml = $model->stuf_envelope(
        'ggk0210-di01',

        verzend_applicatie       => 'Zaaksysteem',
        gemeentenaam             => 'Amsterdam',
        agb_code                 => '01234567',
        object_subscription_uuid => generate_uuid_v4,
        berichttype              => "WMO301",
        date                     => DateTime->now(),
        gemeentecode             => $model->municipality_code,
        xml                      => '<here>be</here><xml/>',

        application_major_version => '0002',
        application_minor_version => '0020',
    );

    my $xml_schema = XML::Compile::Schema->new(
        [
            qw(
                share/xsd/zorginstituut/ggk0210_patch_04/ggk0210/ggk0210/ggkberichten/ggk0210_stuf0301_msg_ggkberichten.xsd
                share/xsd/zorginstituut/ggk0210_patch_04/ggk0210/ggk0210/ggkberichten/ggk0210_msg_ggkberichten.xsd
                share/xsd/zorginstituut/ggk0210_patch_04/ggk0210/0301/stuf0301.xsd
                )
        ]
    );

    validate_xml(
        schema    => $xml_schema,
        xml       => $xml,
        namespace => 'http://www.stufstandaarden.nl/koppelvlak/ggk0210',
        type      => 'envelopHeenbericht-ggk_Di01',
        testname  => 'Craft StUF message',
    );

    {
        note("Send 301 with failures");

        my $case      = mock_case;
        my $record    = mock_transaction_record;
        my $interface = mock_interface(
            get_mapped_attributes_from_case => sub { return {} });


        throws_ok(
            sub {
                $model->send_301(
                    case      => $case,
                    record    => $record,
                    interface => $interface,
                );
            },
            qr/No message could be delivered, not a start, or a stop message/,
            "send_301 throws an expected error"
        );
    }

    {
        my $case = mock_case(aanvrager_object => mock_natuurlijk_persoon);
        my $record = mock_transaction_record;

        my $now   = DateTime->now();
        my $start = $now->add(days => 7);
        my $end   = $start->add(days => 14);

        my $mapped_attributes = _mock_mapped_attributes(
            beschikkingsnummer           => [ undef ],
            toegewezen_product_categorie => [ 10 ],
        );

        my $interface = mock_interface(
            get_mapped_attributes_from_case => sub {
                return $mapped_attributes;
            }
        );

        my $model = Zaaksysteem::Zorginstituut::Model::0202::WMO301->new(
            schema           => $schema,
            interface        => $interface,
            municipality_code => 9999,
            provider         => $provider,
        );

        my $args;
        my $override = override(
            'Zaaksysteem::Zorginstituut::Model::create_object_subscription'
                => sub {
                my $self = shift;
                $args = shift;
                return mock_one(external_id => generate_uuid_v4);
            }
        );

        lives_ok(
            sub {
                $model->send_301(
                    case   => $case,
                    record => $record,
                );
            },
            "send_301 sends start messages"
        );

        is($args->id, $case->id, "Case is send to create_object_subscription");

    }

    {
        note("Send 301 stop");
        my $stop
            = mock_case(aanvrager_object => mock_natuurlijk_persoon);
        my $case
            = mock_case(aanvrager_object => mock_natuurlijk_persoon);
        my $record = mock_transaction_record;

        my $mapped_attributes = _mock_mapped_attributes(
            beschikkingsnummer           => [ $stop->id ],
            toegewezen_product_categorie => [ 10 ],
        );

        my $interface = mock_interface(
            get_mapped_attributes_from_case => sub {
                return $mapped_attributes;
            }
        );

        my $model = Zaaksysteem::Zorginstituut::Model::0202::WMO301->new(
            schema           => $schema,
            interface        => $interface,
            municipality_code => 9999,
            provider         => $provider,
        );

        my $args;
        my $override = override(
            'Zaaksysteem::Zorginstituut::Model::create_object_subscription'
                => sub {
                my $self = shift;
                $args = shift;
                return mock_one(external_id => generate_uuid_v4);
            }
        )->override('Zaaksysteem::Zorginstituut::Messages::0202::WMO301::_find_case' => sub {
            return $stop
        })->override('Zaaksysteem::Zorginstituut::Messages::0202::WMO301::_get_data_from_old_case' => sub {
            return {
                %{$mapped_attributes},
                beschikkingsnummer => [ $case->id ],
                aanbieder          => ['01234567'],
            };
        });

        lives_ok(
            sub {
                $model->send_301(
                    case   => $case,
                    record => $record,
                );
            },
            "send_301 sends stop messages"
        );

        is($args->id, $case->id, "Case is send to create_object_subscription");

    }

}

sub test_wmo302 {

    my $schema    = mock_dbix_schema;
    my $interface = mock_interface;
    my $provider  = mock_provider;

    my $model     = Zaaksysteem::Zorginstituut::Model::0202::WMO301->new(
        schema           => $schema,
        municipality_code => 9999,
        interface        => $interface,
        provider         => $provider,
    );

    isa_ok($model, 'Zaaksysteem::Zorginstituut::Model::0202::WMO301');

    my @methods
        = qw(process_soap can_process_du01 add_soap_envelope stuf_envelope);

    foreach (@methods) {
        if (!can_ok($model, $_)) {
            return;
        }
    }

    my @wmo = qw(share xml-templates zorginstituut 0202 wmo wmo302-minimal.xml);

    my $xml = $model->stuf_envelope(
        'ggk0210-du01',

        agb_code                 => '01234567',
        object_subscription_uuid => generate_uuid_v4,
        remote_subscription_id   => generate_uuid_v4,
        berichttype              => "WMO301",
        date                     => DateTime->now(),
        gemeentecode             => $model->municipality_code,
        xml                      => scalar io->catfile(@wmo)->slurp,
    );

    my $xml_schema = XML::Compile::Schema->new(
        [
            qw(
                share/xsd/zorginstituut/ggk0210_patch_04/ggk0210/ggk0210/ggkberichten/ggk0210_stuf0301_msg_ggkberichten.xsd
                share/xsd/zorginstituut/ggk0210_patch_04/ggk0210/ggk0210/ggkberichten/ggk0210_msg_ggkberichten.xsd
                share/xsd/zorginstituut/ggk0210_patch_04/ggk0210/0301/stuf0301.xsd
                )
        ]
    );

    validate_xml(
        schema    => $xml_schema,
        xml       => $xml,
        namespace => 'http://www.stufstandaarden.nl/koppelvlak/ggk0210',
        type      => 'envelopRetourbericht-ggk_Du01',
        testname  => 'Craft StUF message',
    );


    # Add a soap envelope for testing can_process
    $xml = $model->add_soap_envelope($xml);
    ok($model->can_process_du01($xml), "Is able to process correct message");

    my $case = mock_case;
    my $override = override(
        'Zaaksysteem::Zorginstituut::Model::find_case_by_reference' =>
            sub { return $case })
        ->replace(
        'Zaaksysteem::Zorginstituut::Reader::valid' => sub { return 1 });

    # validate_302
    lives_ok(
        sub {
            my $ok = $model->process_soap($xml);
            return;
        },
        "processing_soap",
    );
}

sub test_wmo302_validation {

    my @valid = qw(
        wmo302-minimal.xml
        wmo302-header.xml
        wmo302.xml
    );

    my @basedir = qw(share xml-templates zorginstituut 0202 wmo);

    my $base = 'http://www.istandaarden.nl/iwmo/2_2/wmo302/basisschema';
    my $msg  = 'http://www.istandaarden.nl/iwmo/2_2/wmo302/schema';

    foreach my $filename (@valid) {
        my $xml = io->catfile(@basedir, $filename)->slurp;

        _validate_wmo_message("WMO302", $xml, "WMO302 for $filename is valid");

        my $reader = Zaaksysteem::Zorginstituut::Reader->new(
            xml            => $xml,
            code           => 415,
            base_namespace => $base,
            msg_namespace  => $msg,
        );

        ok($reader->valid, "$filename is valid");
        is_deeply($reader->errors, {}, "$filename contains no errors");
    }


    my $filename = 'wmo302-nested_errors.xml';

    my $xml = io->catfile(@basedir, $filename)->slurp;

    _validate_wmo_message("WMO302", $xml, "WMO302 for $filename is valid");

    my $reader = Zaaksysteem::Zorginstituut::Reader->new(
        xml            => $xml,
        code           => 415,
        base_namespace => $base,
        msg_namespace  => $msg,
        version        => "2.2"
    );

    ok(!$reader->valid, "$filename is invalid");

    my $errors = {
        'Toegewezen product' => [233, 9071],
        'Beschikking'        => [233],
        'Contactgegevens'    => [233]
    };

    cmp_deeply($reader->errors, $errors, "$filename contains errors");

}

sub test_live_wmo302_stuf {

    my $provider = Zaaksysteem::Zorginstituut::Provider::Generic->new(
        di01_endpoint  => $di01_endpoint,
    );

    isa_ok($provider, 'Zaaksysteem::Zorginstituut::Provider::Generic');

    my $interface   = mock_interface;
    my $schema      = mock_dbix_schema;

    my $model = Zaaksysteem::Zorginstituut::Model::0202::WMO301->new(
        schema            => $schema,
        municipality_code => '0716',
        interface         => $interface,
        provider          => $provider,
    );

    my $xml = io->catfile($ENV{LIVE_WMO302})->slurp;
    $xml = $model->add_soap_envelope($xml);

    _validate_wmo302_xml($model, $xml, "Live WMO302 test");
}

sub test_live_wmo302 {

    my $xml = io->catfile($ENV{LIVE_WMO302})->slurp;
    my $reader = _test_wmo302_ok($xml, $ENV{LIVE_WMO302});
    my $msg = "$ENV{LIVE_WMO302} is valid and contains no errors";

    if (!$reader->valid) {
        diag explain $reader->errors;
        fail($msg);
    }
    else {
        pass($msg);
    }
}


sub _validate_wmo302_xml {
    my ($model, $xml, $testname) = @_;

    ok($model->can_process_du01($xml),
        "$testname: Is able to process correct message");

    my $du01 = $model->get_stuf_du01($xml);
    my $validator = $model->du01_to_xml($du01);
    my $ok =  $validator->valid;

    if (!$ok) {
        diag explain $validator->errors;
        pass("$testname: WMO302 is processed correctly with errors");
    }
    else {
        pass("$testname: WMO302 is processed correctly without errors");
    }
}

sub _build_wmo302 {
    my $model = shift;
    my $file  = shift;

    my $wmo302 = io->catfile($file)->slurp;
    my $xml    = $model->stuf_envelope(
        'ggk0210-du01',

        agb_code                 => '01234567',
        object_subscription_uuid => generate_uuid_v4,
        remote_subscription_id   => generate_uuid_v4,
        berichttype              => "WMO302",
        date                     => DateTime->now(),
        gemeentecode             => $model->municipality_code,
        xml                      => '<here>be</here><xml/>',
    );
    return $model->add_soap_envelope($xml);
}


sub test_wmo301_message {

    my $schema = mock_dbix_schema;
    my $case   = mock_case(aanvrager_object => mock_natuurlijk_persoon);
    my $record = mock_transaction_record;

    my $now   = DateTime->now();
    my $start = $now->add(days => 7);
    my $end   = $start->add(days => 14);

    my $mapped_attributes = _mock_mapped_attributes(
        beschikkingsnummer           => [ $case->id + 1 ],
        toegewezen_product_categorie => [ 10 ],
    );

    my $interface = mock_interface(
        get_mapped_attributes_from_case => sub {
            return $mapped_attributes;
        }
    );

    my $msg = Zaaksysteem::Zorginstituut::Messages::0202::WMO301->new(
        schema           => $schema,
        case             => $case,
        interface        => $interface,
        municipality_code => 9999,
        provider         => mock_provider,
    );

    isa_ok($msg, 'Zaaksysteem::Zorginstituut::Messages::0202::WMO301');

    my @attributes = qw(
        is_start
        is_stop
        beschikkingsnummer
        attribute_data
        interface
    );

    foreach (@attributes) {
        if (!can_ok($msg, $_)) {
            return;
        }
    }

    ok($msg->is_start, "Is a start message");
    ok($msg->is_stop,  "Is a stop message");

    my $xml_schema = XML::Compile::Schema->new(
        [
            qw(
                share/xsd/zorginstituut/0202/wmo/basisschema.xsd
                share/xsd/zorginstituut/0202/wmo/WMO301.xsd
                )
        ]
    );

    my $xml = $msg->start_xml;

    validate_xml(
        schema    => $xml_schema,
        namespace => 'http://www.istandaarden.nl/iwmo/2_2/wmo301/schema',
        type      => 'Bericht',
        xml       => $xml,
        testname  => "WMO 301 start",
    );

    _test_toewijzingsnummer('WMO301', $xml);


    {
        my $override = override(
            'Zaaksysteem::Zorginstituut::Messages::0202::WMO301::_find_case'
                => sub {
                return mock_case(aanvrager_object => mock_natuurlijk_persoon);
            }
        );

        my $xml = $msg->stop_xml;
        validate_xml(
            schema    => $xml_schema,
            namespace => 'http://www.istandaarden.nl/iwmo/2_2/wmo301/schema',
            type      => 'Bericht',
            xml       => $xml,
            testname  => "WMO 301 stop",
        );

        _test_toewijzingsnummer('WMO301', $xml);
    }
}

sub _test_toewijzingsnummer {
    my ($type, $xml) = @_;

    my $xp = Zaaksysteem::Test::XML::get_xpath($xml);

    my $xpath = '//msg:Bericht/msg:Client/msg:Beschikking/msg:ToegewezenProducten/msg:ToegewezenProduct/msg:ToewijzingNummer';

    $xp->registerNs('base', 'http://www.istandaarden.nl/iwmo/2_2/basisschema/schema');
    $xp->registerNs('msg', 'http://www.istandaarden.nl/iwmo/2_2/wmo301/schema');

    my $toewijzingsnummer = $xp->findvalue($xpath);

    return ok($toewijzingsnummer, "Found toewijzingsnummer $toewijzingsnummer in XML");
}

sub generate_301 {
    my ($generator, %data) = @_;

    my $client    = mock_natuurlijk_persoon;
    my $now       = DateTime->now();

    my $aanbieder = delete $data{aanbieder} // "01234567";
    my $afzender  = delete $data{afzender} // 9999;

    my ($berichtcode, $product_category, $product_code);

    if ($generator->isa('Zaaksysteem::XML::Generator::WMO::0202')) {
        $berichtcode      = 414;
        $product_category = 2;
        $product_code     = 2;
    }
    else {
        $berichtcode      = 436;
        $product_category = 31;
        $product_code     = 2;
    }

    my $begindatum = $now->clone->add(days => 7);
    my $einddatum  = $begindatum->clone->add(days => 7);

    my @producten = (
        {
            aanbieder         => $aanbieder,
            ingangsdatum      => $begindatum,
            einddatum         => $einddatum,
            toewijzingsdatum  => $now,
            toewijzingsnummer => 1,
            omvang            => {
                eenheid    => 4,
                volume     => 1,
                frequentie => 1,
            },
            categorie        => $product_category,
            code             => $product_code,
            reden_intrekking => '03',
        },
    );

    %data = (
        Header => {
            Berichtspecificatie => {
                Code      => $berichtcode,
                Versie    => 2,
                SubVersie => 2,
            },
            Afzender             => $afzender,
            Ontvanger            => $aanbieder,
            Berichtidentificatie => {
                Identificatie => 42,
                Dagtekening   => $now,
                Tekenset      => 1,
            },
        },
        client      => $client,
        beschikking => {
            nummer       => 42,
            afgiftedatum => $now,
            ingangsdatum => $begindatum,
            einddatum    => $einddatum,
        },
        gemeentecode => $afzender,
        producten    => {
            toegewezen => \@producten,

        },
        commentaar => {
            general              => "Algemeen commentaar op de beschikking",
            toegewezen_producten => "Commentaar bij het product",
        },
        %data,
    );

    return $generator->build_301(writer => \%data,);
}

sub _test_wmo302_ok {
    my $xml = shift;
    my $filename = shift;

    my $base = 'http://www.istandaarden.nl/ijw/2_2/wmo302/basisschema/2_2';
    my $msg  = 'http://www.istandaarden.nl/ijw/2_2/wmo302/schema/2_2';

    my $reader = Zaaksysteem::Zorginstituut::Reader->new(
        xml            => $xml,
        code           => 437,
        base_namespace => $base,
        msg_namespace  => $msg,
    );

    lives_ok(
        sub {
            $reader->assert_code_valid;
        },
        "Message code for $filename is valid"
    );
    return $reader;
}

sub test_wmo_jw_rules_for_enddates {

        my $case      = mock_case();
        my $record    = mock_transaction_record();
        my $interface = mock_interface();
        my $schema    = mock_dbix_schema();
        my $provider  = mock_provider();

        my $message = Zaaksysteem::Zorginstituut::Messages::0202::WMO301->new(
            schema           => $schema,
            interface        => $interface,
            municipality_code => 9999,
            provider         => $provider,
            case             => $case,
        );

        my $earliest = DateTime->now->subtract('days' => 14);

        my @dates = (
            [DateTime->now() ],
            [$earliest ],
            [DateTime->now->add('days' => 14) ]
        );

        my $found = $message->_get_earliest_date(@dates);
        is($found->[0], $earliest, "Found the earliest date");

        my $data = _mock_mapped_attributes();
        my %expect = %$data;

        $message->_set_earliest_product_enddates($data);

        cmp_deeply($data, \%expect,
            "Setting earliest end dates for products without change");

        my $now   = DateTime->now();
        my $start = $now->clone;
        my $end   = $start->clone->add(days => 2);

        $data = _mock_mapped_attributes(
            afgiftedatum                    => [ $now ],
            ingangsdatum                    => [ $start ],
            beschikkingseinddatum           => [ $end ],
            toegewezen_product_ingangsdatum => [ $start ],
        );

        %expect = %{$data};
        $expect{toegewezen_product_einddatum} =  [$end ];

        $message->_set_earliest_product_enddates($data);

        cmp_deeply($data, \%expect,
            "Setting earliest end dates for products to end of beschikking");

        my $new_end = $end->clone->add(days => 2);
        $data = _mock_mapped_attributes(
            afgiftedatum                    => [ $now ],
            ingangsdatum                    => [ $start ],
            beschikkingseinddatum           => [ $end ],
            toegewezen_product_ingangsdatum => [ $end->clone->add(days => 5) ],
        );

        %expect = %{$data};
        $expect{toegewezen_product_einddatum} = [$end ];
        $expect{toegewezen_product_ingangsdatum} = [$end ];

        $message->_set_earliest_product_enddates($data);

        cmp_deeply($data, \%expect,
            "Setting earliest end dates for products to end of beschikking incl start dates"
        );
}

sub _mock_mapped_attributes {
    my %opts = @_;

    my $now   = DateTime->now();
    my $start = $now->add(days => 7);
    my $end   = $start->add(days => 14);

    return {
        aanbieder                => [ '02345678' ],
        beschikkingsnummer       => [ 42 ],
        beschikkingsafgiftedatum => [ $now ],
        beschikkingsingangsdatum => [ $start ],
        beschikkingseinddatum    => [ $end ],
        beschikkingscommentaar   => [ "This is a comment" ],

        toegewezen_product_toewijzingsdatum  => [ $now ],
        toegewezen_product_ingangsdatum      => [ $start ],
        toegewezen_product_einddatum         => [ $end ],

        toegewezen_product_categorie         => [ 31 ],
        toegewezen_product_code              => [ "12345" ],
        toegewezen_product_omvang_volume     => [ 1 ],
        toegewezen_product_omvang_eenheid    => [ 1 ],
        toegewezen_product_omvang_frequentie => [ 1 ],
        toegewezen_product_commentaar        => [ "This is a product comment" ],
        toegewezen_product_reden_intrekking  => [ 01 ],
        commentaar                           => [ { general => 'Foo' } ],

        %opts,
    };
}

sub test_xml_generator_wmo301 {

    my $generator = Zaaksysteem::XML::Generator::WMO::0202->new();

    my $xml = generate_301($generator);

    _validate_wmo301($xml, "WMO 301 generator test");

}

sub _validate_wmo301 {
    my ($xml, $msg) = @_;

    _validate_wmo_message("WMO301", $xml, $msg);
}

sub _validate_wmo_message {
    my ($type, $xml, $msg) = @_;

    my $xsd = uc($type);
    my $ns = lc($type);

    my $xml_schema = XML::Compile::Schema->new(
        [
            'share/xsd/zorginstituut/0202/wmo/basisschema.xsd',
            "share/xsd/zorginstituut/0202/wmo/$type.xsd"
        ]
    );

    validate_xml(
        schema    => $xml_schema,
        namespace => "http://www.istandaarden.nl/iwmo/2_2/$ns/schema",
        type      => 'Bericht',
        xml       => $xml,
        testname  => $msg // "Undefined test",
    );

    if ($xsd eq 'WMO301') {
        _test_toewijzingsnummer('WMO301', $xml);
    }
}

sub test_live_wmo301 {
    my $xml = io->catfile($ENV{LIVE_WMO301})->slurp;
    _validate_wmo301($xml);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
