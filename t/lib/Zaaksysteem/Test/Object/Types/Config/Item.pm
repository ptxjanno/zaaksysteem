package Zaaksysteem::Test::Object::Types::Config::Item;
use Moose;
extends 'Zaaksysteem::Test::Moose';

use Zaaksysteem::Test;

use Zaaksysteem::Object::Types::Config::Item;
use Zaaksysteem::Object::Types::Config::Definition;

sub test_config_item {

    my $def = Zaaksysteem::Object::Types::Config::Definition->new(
        config_item_name => 'foo',
    );
    isa_ok($def, "Zaaksysteem::Object::Types::Config::Definition");

    my $item = Zaaksysteem::Object::Types::Config::Item->new(definition => $def);
    isa_ok($item, "Zaaksysteem::Object::Types::Config::Item");

}



__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::Test::Object::Types::Config::Item - Test config item object types

=head1 DESCRIPTION

=head1 SYNOPSIS

    prove -lv :: Zaaksysteem::Test::Object::Types::Config::Item

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
