package Zaaksysteem::Test::Backend::Case::Action::ResultSet;

use Zaaksysteem::Test;

use Data::UUID;
use Zaaksysteem::Backend::Case::Action::ResultSet;
use Zaaksysteem::Schema::ZaaktypeStandaardBetrokkenen;

sub test_create_from_subject {
    my $case    = mock_case();
    my $phase   = mock_one(id => 66);

    my $caseaction = mock_one(
        create => sub {
            return shift;
        }
    );

    my $zt_subject = mock_one(
        id => 66,
        'betrokkene_identifier' => 'betrokkene-natuurlijk_persoon-55',
        'naam'                  => 'T. Testpersoon',
        'rol'                   => 'Advocaat',
        'magic_string_prefix'   => 'advocaat',
        'gemachtigd'            => 1,
        'notify'                => 1,
        'uuid'                  => Data::UUID->new->create_str(),
    );

    my $rs  = "Zaaksysteem::Backend::Case::Action::ResultSet";

    my $action = Zaaksysteem::Backend::Case::Action::ResultSet::create_from_subject($caseaction,$case, $phase, $zt_subject);

    ok($action->{automatic} == 0, "Got no automatic");
    is($action->{case_id}, 1, "Got correct case_id");
    is($action->{label}, 'T. Testpersoon', "Got label: " . $action->{label});
    is($action->{type}, 'subject', "Got type: " . $action->{type});
    is($action->{casetype_status_id}, 66, "Got phase: " . $action->{casetype_status_id});

    my $data = {
      'gemachtigd' => 1,
      'rol' => 'Advocaat',
      'magic_string_prefix' => 'advocaat',
      'betrokkene_identifier' => 'betrokkene-natuurlijk_persoon-55',
      'notify' => 1,
      'naam' => 'T. Testpersoon',
      'betrokkene_type' => 'natuurlijk_persoon'
    };

    is($action->{data}->{$_}, $data->{$_}, "Got correct value for $_ : " . $data->{$_}) for keys %$data;

}

1;

__END__

=head1 NAME

Zaaksysteem::Test::Backend::Case::Action::ResultSet - Test Zaaksysteem::Backend::Case::Action::ResultSet

=head1 DESCRIPTION

Test email backend code

=head1 SYNOPSIS

    prove -l -v t

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.


