package Zaaksysteem::Test::Backend::Subject::Component;
use Moose;
extends 'Zaaksysteem::Test::Moose';

use Zaaksysteem::Test;
use Zaaksysteem::Backend::Subject::Component;

sub test_subject_permissions {
    my $bare_subject = Zaaksysteem::Backend::Subject::Component->new(
        roles => [
            mock_one(
                'X-Mock-ISA' => 'Zaaksysteem::Schema::Roles',
                name => 'Administrator'
            )
        ]
    );

    ok(
        $bare_subject->has_legacy_permission('view_sensitive_data'),
        "User has 'view_sensitive_data' permission"
    );

    ok(
        !$bare_subject->has_legacy_permission('failure'),
        "User doesn't have 'failure' permission"
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::Test::Backend::Subject::Component - Test the subject component class

=head1 DESCRIPTION

Tests for the subject component class

=head1 SYNOPSIS

    prove -lv :: Zaaksysteem::Test::Backend::Subject::Component

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
