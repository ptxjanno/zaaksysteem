package Zaaksysteem::Test::Util;
use warnings;
use strict;
use namespace::autoclean -except => 'import';

use Exporter qw(import);
use Sub::Override;

BEGIN {
    *CORE::GLOBAL::time = sub () { return CORE::time; }
}

our @EXPORT = qw(stop_clock);

=head1 NAME

Zaaksysteem::Test::Util - Small utility functions to make testing life easier

=head1 SYNOPSIS

    use Zaaksysteem::Test::Util;

    stop_clock(31337);

=head1 METHODS

=head2 stop_clock($unixtime)

Stops the clock at C<$unixtime>, and returns a handle.

Once this handle goes out of scope, the clock continues running as usual.

=cut

sub stop_clock {
    my $time = shift;

    return Sub::Override->new(
        'CORE::GLOBAL::time', sub () { return $time; }
    );
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
