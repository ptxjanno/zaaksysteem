package Zaaksysteem::Test::Objects;
use warnings;
use strict;

use Exporter qw(import);
use Zaaksysteem::Schema;
use Test::More;

our @EXPORT = qw(
    get_schema
);

=head1 NAME

Zaaksysteem::Test::Objects - Get some real life objects from the Testsuite

=head1 DESCRIPTION


=head1 SYNOPSIS

    use Zaaksysteem::Test::Objects;;
    my $schema = get_schema();

=head1 EXPORTED FUNCTIONS

=head2 get_schema

    my $schema = get_schema(
        customer_instance => {
            ...,
        },
    );

=cut


sub get_schema {
    my %opts = @_;

    my $schema = Zaaksysteem::Schema->connect();
    isa_ok($schema, "Zaaksysteem::Schema");

    foreach my $name (keys %opts) {
        my $attr = $schema->meta->find_attribute_by_name($name);
        $attr->set_value($schema, $opts{$name});
    }
    return $schema;

}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
