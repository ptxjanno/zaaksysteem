package Zaaksysteem::Test::Zaken::Roles::DeelzaakObjecten;
use Zaaksysteem::Test;

=head1 NAME

Zaaksysteem::Test::Zaken::Roles::DeelzaakObjecten - Test for the "DeelzaakObjecten" role for Zaak instances

=head1 SYNOPSIS

    prove -l :: Zaaksysteem::Test::Zaken::Roles::DeelzaakObjecten

=cut

sub test_master_number_pid {
    my $zaak1 = Test::DeelzaakObjecten->new(
        {
            id  => 1,
        }
    );
    my $zaak2 = Test::DeelzaakObjecten->new(
        {
            id  => 2,
            pid => $zaak1,
        }
    );
    my $zaak3 = Test::DeelzaakObjecten->new(
        {
            id  => 3,
            pid => $zaak2,
        }
    );

    is($zaak1->master_number, 1, "Master of a case without master is itself");
    is($zaak2->master_number, 1, "Master of a case with a parent is the parent");
    is($zaak3->master_number, 1, "Master of a case with a grandparent is the grandparent");
}

sub test_master_number_vervolg_van {
    my $zaak1 = Test::DeelzaakObjecten->new(
        {
            id  => 5,
        }
    );
    my $zaak2 = Test::DeelzaakObjecten->new(
        {
            id  => 6,
            vervolg_van => $zaak1,
        }
    );
    my $zaak3 = Test::DeelzaakObjecten->new(
        {
            id  => 7,
            vervolg_van => $zaak2,
        }
    );

    is($zaak1->master_number, 5, "Master of a case without initiator is itself");
    is($zaak2->master_number, 5, "Master of a case with an initiator is the parent");
    is($zaak3->master_number, 5, "Master of a case with an initiator that has a parent is the grandparent");
}

sub test_master_number_mixed {
    my $zaak1 = Test::DeelzaakObjecten->new(
        {
            id  => 5,
        }
    );
    my $zaak2 = Test::DeelzaakObjecten->new(
        {
            id  => 6,
            pid => $zaak1,
        }
    );
    my $zaak3 = Test::DeelzaakObjecten->new(
        {
            id  => 7,
            vervolg_van => $zaak2,
        }
    );

    is($zaak1->master_number, 5, "Master of a case without master is itself");
    is($zaak2->master_number, 5, "Master of a case with a parent is the parent");
    is($zaak3->master_number, 5, "Master of a continuation case with a parent is the initiator's parent");
}

package Test::DeelzaakObjecten {
    use Moose;
    with 'Zaaksysteem::Zaken::Roles::DeelzaakObjecten';

    has id => (
        is => 'rw',
    );

    has pid => (
        is => 'rw',
    );

    has vervolg_van => (
        is => 'rw',
    );

    sub can_volgende_fase {}
};

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
