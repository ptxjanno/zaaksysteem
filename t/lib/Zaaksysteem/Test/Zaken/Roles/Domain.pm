package Zaaksysteem::Test::Zaken::Roles::Domain;

use UUID::Tiny qw[create_uuid_as_string UUID_V4];
use Zaaksysteem::Test;

=head1 NAME

Zaaksysteem::Test::Zaken::Roles::Domain - Tests for the new DDD-inspired
case interface

=head1 SYNOPSIS

    prove -l :: Zaaksysteem::Test::Zaken::Roles::Domain

=cut

use Zaaksysteem::Zaken::ComponentZaak;

sub test_editable {
    my $deleted_zaak = Zaaksysteem::Test::Zaken::Roles::MockZaak->new(
        status => 'deleted'
    );

    ok $deleted_zaak->ddd_is_deleted, 'is_deleted is true for deleted state';
    ok !$deleted_zaak->ddd_is_resolved, 'is_resolved is false for deleted state';
    ok !$deleted_zaak->ddd_is_editable, 'is_editable is false for deleted state';

    my $resolved_zaak = Zaaksysteem::Test::Zaken::Roles::MockZaak->new(
        status => 'resolved'
    );

    ok !$resolved_zaak->ddd_is_deleted($resolved_zaak), 'is_deleted is false for resolved state';
    ok $resolved_zaak->ddd_is_resolved($resolved_zaak), 'is_resolved is true for resolved state';
    ok !$resolved_zaak->ddd_is_editable($resolved_zaak), 'is_editable is false for resolved state';

    my $open_zaak = Zaaksysteem::Test::Zaken::Roles::MockZaak->new(
        status => 'open'
    );

    ok !$open_zaak->ddd_is_deleted($open_zaak), 'is_deleted false for open state';
    ok !$open_zaak->ddd_is_resolved($open_zaak), 'is_resolved false for open state';
    ok $open_zaak->ddd_is_editable($open_zaak), 'is_editable true for open state';
}

sub test_confidentiality {
    my $confidential_zaak = Zaaksysteem::Test::Zaken::Roles::MockZaak->new(
        confidentiality => 'confidential'
    );

    ok $confidential_zaak->ddd_is_confidential, 'is_confidential is true for confidential state';
    ok !$confidential_zaak->ddd_is_public, 'is_public is false for confidential state';

    my $public_zaak = Zaaksysteem::Test::Zaken::Roles::MockZaak->new(
        confidentiality => 'public'
    );

    ok $public_zaak->ddd_is_public, 'is_public is true for public state';
    ok !$public_zaak->ddd_is_confidential, 'is_confidential is false for public state';
}

sub test_editable_by {
    my $uuid = create_uuid_as_string(UUID_V4);

    my $zaak = Zaaksysteem::Test::Zaken::Roles::MockZaak->new(
        status => 'deleted',
        behandelaar => mock_one(get_column => $uuid),
        coordinator => mock_one(get_column => $uuid)
    );

    my $perm_zaak_beheer = sub {
        my $zaak = shift;
        for my $perm (@_) {
            return 1 if $perm eq 'zaak_beheer';
        }
        return;
    };

    my $perm_zaak_edit = sub {
        my $zaak = shift;
        for my $perm (@_) {
            return 1 if $perm eq 'zaak_edit';
        }
        return;
    };

    my $perm_zaak_none = sub { };

    my $user = mock_one(
        id => 123,
        uuid => $uuid
    );

    ok !$zaak->ddd_is_item_editable_by($user, $perm_zaak_beheer, 'attributes'),
        'is_item_editable_by false if case deleted';

    $zaak->status('resolved');

    ok !$zaak->ddd_is_item_editable_by($user, $perm_zaak_beheer, 'attributes'),
        'is_item_editable_by false if case resolved';

    $zaak->status('open');

    ok $zaak->ddd_is_item_editable_by($user, $perm_zaak_beheer, 'attributes'),
        'is_item_editable_by true if case open and user is beheerder';

    ok $zaak->ddd_is_item_editable_by($user, $perm_zaak_edit, 'attributes'),
        'is_item_editable_by true if case open and user is edit-permitted';

    ok $zaak->ddd_is_item_editable_by($user, $perm_zaak_none, 'attributes'),
        'is_item_editable_by true if case open, user is assignee and coord and no perms';

    $zaak->_clear_coordinator;

    ok $zaak->ddd_is_item_editable_by($user, $perm_zaak_none, 'attributes'),
        'is_item_editable_by true if case open, user is assignee and undef coord and no perms';

    # Set a coordinator who isn't the assignee
    $zaak->coordinator(mock_one(get_column => 456));

    ok !$zaak->ddd_is_item_editable_by($user, $perm_zaak_none, 'attributes'),
        'is_item_editable_by false if case open, user is assignee and another coord and no perms';
}

package Zaaksysteem::Test::Zaken::Roles::MockZaak;

use Moose;

with qw[
    Zaaksysteem::Zaken::Roles::Domain
    MooseX::Log::Log4perl
];

has status => ( is => 'rw' );
has confidentiality => ( is => 'rw' );

has behandelaar => (
    is => 'rw',
    clearer => '_clear_assignee'
);

has coordinator => (
    is => 'rw',
    clearer => '_clear_coordinator'
);

sub get_column {
    my $self = shift;
    my $column = shift;

    return 123 if $column eq 'behandelaar' and $self->behandelaar;
    return 456 if $column eq 'coordinator' and $self->coordinator;

    return;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
