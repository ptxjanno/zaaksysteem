package Zaaksysteem::Test::Session::Invitation::Model;

use Zaaksysteem::Test;

use Zaaksysteem::Tools::SimpleStore;
use Zaaksysteem::Session::Invitation::Model;
use Zaaksysteem::Object::Query;

use DateTime;

use Moose::Util::TypeConstraints qw[subtype role_type];

=head1 NAME

Zaaksysteem::Test::Session::Invitation::Model - Test session invitation
abstraction model

=head1 SYNOPSIS

    prove -lv :: Zaaksysteem::Test::Session::Invitation::Model

=cut

sub test_create_invitation {
    my $store = Zaaksysteem::Tools::SimpleStore->new;

    my $model = Zaaksysteem::Session::Invitation::Model->new(
        store => $store
    );

    my $subject = mock_one(
        'X-Mock-ISA' => [qw[
            Zaaksysteem::Object::Types::Subject
            Zaaksysteem::Object::Reference::Instance
            Moose::Object
        ]],
        does => sub { return 1 },
        id => 123,
        type => 'subject',
    );

    my $invitation = $model->create({
        subject => $subject,
        date_expires => DateTime->now->add(days => 1)
    });

    ok defined $invitation, 'Model returns a token';

    ok scalar $store->exists(qb('session_invitation', {
        cond => qb_eq('subject', qb_lit('object_ref', $subject))
    })), 'Session invitation store has subject_id';
}

sub test_validate_invitation {
    my $store = Zaaksysteem::Tools::SimpleStore->new;

    my $model = Zaaksysteem::Session::Invitation::Model->new(
        store => $store
    );

    my $subject = mock_one(
        'X-Mock-ISA' => [qw[
            Zaaksysteem::Object::Types::Subject
            Zaaksysteem::Object::Reference::Instance
            Moose::Object
        ]],
        does => sub { return 1 },
        id => 123,
        type => 'subject',
    );

    my $invitation = $model->create({
        subject => $subject,
        date_expires => DateTime->now->add(days => 1)
    });

    ok $model->validate($invitation->token), 'Token validates';
    ok !$model->validate($invitation->token), 'Token does not validate twice';

    ok !scalar $store->exists(qb('session_invitation', {
        cond => qb_eq('subject', qb_lit('object_ref', $subject))
    })), 'Session invitation for subject_id removed';
}

sub test_validate_expired_invitation {
    my $store = Zaaksysteem::Tools::SimpleStore->new;

    my $model = Zaaksysteem::Session::Invitation::Model->new(
        store => $store
    );

    my $subject = mock_one(
        'X-Mock-ISA' => [qw[
            Zaaksysteem::Object::Types::Subject
            Zaaksysteem::Object::Reference::Instance
            Moose::Object
        ]],
        does => sub { return 1 },
        id => 123,
        type => 'subject',
    );

    my $invitation = $model->create({
        subject => $subject,
        date_expires => DateTime->now->subtract(days => 1)
    });

    ok defined $invitation, 'Model returns a token';

    ok !$model->validate($invitation->token), 'Expired token does not validate';
}

sub test_cleanup_expired_invitations {
    my $store = Zaaksysteem::Tools::SimpleStore->new;

    my $model = Zaaksysteem::Session::Invitation::Model->new(
        store => $store
    );

    my $subject = mock_one(
        'X-Mock-ISA' => [qw[
            Zaaksysteem::Object::Types::Subject
            Zaaksysteem::Object::Reference::Instance
            Moose::Object
        ]],
        does => sub { return 1 },
        id => 123,
        type => 'subject',
    );

    my $invitation = $model->create({
        subject => $subject,
        date_expires => DateTime->now->subtract(days => 1)
    });

    ok $store->exists(qb('session_invitation', {
        cond => qb_eq('token', $invitation->token)
    })), 'expired session created in store';

    $model->cleanup_expired_invitations;

    ok !$store->exists(qb('session_invitations')),
        'expired sessions cleaned up';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
