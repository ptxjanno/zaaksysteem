package Zaaksysteem::Test::External::KvKAPI;
use Moose;
extends 'Zaaksysteem::Test::Moose';

use Zaaksysteem::Test;
use Zaaksysteem::External::KvKAPI;

sub _get_model {
    return Zaaksysteem::External::KvKAPI->new(
        api_key   => 'foobar',
        spoofmode => 1,
    );
}

sub test_api_handling_spoof {
    my $test = shift;

    $test->test_report->plan(2);

    my $model = _get_model();

    my $override = override('WebService::KvKAPI::Spoof::_search' => sub {
            my $self = shift;
            my $params = shift;
            cmp_deeply($params, { foo => 'bar'}, "Delegation works for me (tm)");
            return { data => { items => [] }};
    });
    my $rv = $model->search(foo => 'bar');
    cmp_deeply($rv, [], "... and no results were found");
}

sub test_params_parsing {

    my $model = _get_model();

    throws_ok(sub {
        $model->map_params_to_kvk_query({});
    },
    qr/No KvK query can be build/, "Unable to map invalid or non-existent params");

    my $kvk_query = $model->map_params_to_kvk_query(
        {
            company             => 'Mintlab',
            coc_location_number => 12,
            coc_number          => 12,
        }
    );

    cmp_deeply(
        $kvk_query,
        {
            tradeName    => 'Mintlab',
            kvkNumber    => 0 x 6 . 12,
            branchNumber => 0 x 10 . 12
        },
        "But company => mintlab can be mapped"
    );

}

sub test_parsing_response_to_subject {

    my $model = _get_model();

    my $kvk_company = {
        "kvkNumber"    => "90001354",
        "branchNumber" => "990000541921",
        "rsin"         => "992760562",
        "tradeNames"   => {
            "businessName"          => "Grand Kontex B.V.",
            "shortBusinessName"     => "Grand Kontex B.V.",
            "currentTradeNames"     => ["Grand Kontex B.V."],
            "currentStatutoryNames" => ["Grand Kontex B.V."]
        },
        "hasEntryInBusinessRegister" => 0,
        "hasNonMailingIndication"    => 1,
        "isLegalPerson"              => 1,
        "isBranch"                   => 0,
        "isMainBranch"               => 1,
        "addresses"                  => [
            {
                "type"                => "vestigingsadres",
                "street"              => "Keizerhof",
                "houseNumber"         => "68",
                "houseNumberAddition" => "",
                "postalCode"          => "3135CT",
                "city"                => "Vlaardingen",
                "country"             => "Nederland"
            }
        ],
        "websites" => ["www.kvk.nl"]
    };

    my $subject = $model->map_kvk_company_to_subject($kvk_company);
    isa_ok($subject, "Zaaksysteem::Object::Types::Subject");
    lives_ok(
        sub {
            $subject->subject->check_object;
        },
        "Sane object found my dear",
    );

    $kvk_company = {
        "kvkNumber"  => "90001354",
        "rsin"       => "992760562",
        "tradeNames" => {
            "shortBusinessName"     => "Grand Kontex B.V.",
            "currentStatutoryNames" => ["Grand Kontex B.V."]
        },
        "hasEntryInBusinessRegister" => 1,
        "hasNonMailingIndication"    => 0,
        "isLegalPerson"              => 1,
        "isBranch"                   => 0,
        "isMainBranch"               => 0
    };

    $subject = $model->map_kvk_company_to_subject($kvk_company);
    isa_ok($subject, "Zaaksysteem::Object::Types::Subject");
    throws_ok(
        sub {
            $subject->subject->check_object;
        },
        qr/missing: one_address/,
        "KvK response without an address",
    );
}




__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::Test::External::KvKAPI - Tests for the KvKAPI model

=head1 DESCRIPTION

Glue layer between Zaaksysteem and KvKAPI.

Queries and transforms KvKAPI calls to ZS objects.

=head1 SYNOPSIS

    prove -lv :: Zaaksysteem::Test::External::KvKAPI;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
