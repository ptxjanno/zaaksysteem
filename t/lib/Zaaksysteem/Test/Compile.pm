package Zaaksysteem::Test::Compile;
use Zaaksysteem::Test;

use FindBin;
use Path::Tiny;
use Test::More;
use lib 'lib';


sub test_compile {
    my $dir = path("lib/");
    my $iter = $dir->iterator({ recurse => 1, follow_symlinks => 0 }); 

    while (my $path = $iter->()) {
        next if $path->is_dir || $path !~ /\.pm$/;

        # Skip the main Catalyst thing, it's strange
        next if $path eq 'lib/Zaaksysteem.pm';

        my $module = $path->relative;
        $module =~ s/(?:^lib\/|\.pm$)//g;
        $module =~ s/\//::/g;
        BAIL_OUT( "$module does not compile" ) unless require_ok( $module );
    }
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
