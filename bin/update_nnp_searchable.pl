#!/usr/bin/env perl
use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../lib";

use Zaaksysteem::CLI::Bedrijf::Searchable;

my $cli = Zaaksysteem::CLI::Bedrijf::Searchable->init;

$cli->run;

1;

__END__

=head1 NAME

update_nnp_searchable.pl - Fix search terms for all companies

=head1 SYNOPSIS

update_nnp_searchable.pl OPTIONS

=head1 OPTIONS

=over

=item * config

The Zaaksysteem configuration defaults to /etc/zaaksysteem/zaaksysteem.conf

=item * customer_d

The customer_d dir, defaults to the relative directory where zaaksysteem.conf is found.

=item * hostname

The hostname you want to touch cases for

=item * no-dry

Run it and commit the changes. The default is NOT to commit any changes.

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
