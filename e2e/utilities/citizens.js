export default [
    {
        name: 'Tinus Testpersoon',
        id: '1',
        uuid: '6bc9dbc3-f4a5-45ed-b57f-22077af42511'
    },
    {
        name: 'Aanvrager postcode',
        id: '5',
        uuid: 'c911e9ce-c2b1-414f-ade9-6c50a9a7fdc4'
    },
    {
        name: 'Bob Briefadres',
        id: '13',
        uuid: 'f78fe727-bfea-4032-8a4b-01c24c21d7f9'
    },
    {
        name: 'Jon Kui',
        id: '14',
        uuid: 'dc53f315-b3d3-4697-857b-1d750bb0c747'
    },
    {
        name: 'Contactgegevens aanpassen leeg',
        id: '15',
        uuid: '008b8b68-7ff1-4efe-aa1d-14bdefafcd2e'
    },
    {
        name: 'Contactgegevens aanpassen veranderen',
        id: '16',
        uuid: '1ab4888a-cd1e-48bc-8897-fbf1135c6095'
    },
    {
        name: 'Contactgegevens aanpassen verwijderen',
        id: '17',
        uuid: '8283fc81-6f47-4384-8ea6-bace8fefd405'
    },
    {
        name: 'Gegevens hergebruiken',
        id: '18',
        uuid: '7b46bda5-30f2-4bc1-a523-8ebaf87fba77'
    }
];

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
