import {
    openPageAs
} from './../../../../../functions/common/navigate';
import {
    openPhase
} from './../../../../../functions/intern/caseView/caseNav';
import {
    getClosedValue
} from './../../../../../functions/common/input/caseAttribute';

const choice = $('[data-name="boolean"]');
const paymentStatusPending = $('[data-name="betaalstatus_wachten_op_betaling"]');
const paymentStatusComplete = $('[data-name="betaalstatus_voltooid"]');

describe('when opening case 55 with a completed payment', () => {

    beforeAll(() => {

        openPageAs('admin', 55);

        openPhase('1');

        choice.$('[value="Ja"]').click();

    });

    it('the attributes should have the correct values', () => {

        expect(getClosedValue(paymentStatusPending)).toEqual('False');
        expect(getClosedValue(paymentStatusComplete)).toEqual('True');

    });

});

describe('when opening case 56 with a pending payment', () => {

    beforeAll(() => {

        openPageAs('admin', 56);

        openPhase('1');

        choice.$('[value="Ja"]').click();

    });

    it('the attributes should have the correct values', () => {

        expect(getClosedValue(paymentStatusPending)).toEqual('True');
        expect(getClosedValue(paymentStatusComplete)).toEqual('False');

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
