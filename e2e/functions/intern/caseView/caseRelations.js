export const subjectRelations = $$('.related_subjects .table-row');

export const getContactRole = contactName =>
    subjectRelations
        .filter(relation =>
            relation
                .$('[column-id="name"]')
                .getText()
                .then(name =>
                    name === contactName
                )
        )
        .first()
        .$('[column-id="role"]')
        .getText();

export const getContactAuthorisation = contactName =>
    subjectRelations
        .filter(relation =>
            relation
                .$('[column-id="name"]')
                .getText()
                .then(name =>
                    name === contactName
            )
        )
        .first()
        .$('[column-id="pip_authorized"] .mdi-check')
        .isPresent();

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
