BEGIN;

SET CLIENT_ENCODING TO 'UTF8';

INSERT INTO interface (name, active, max_retries, interface_config, multiple, module) VALUES ('Authenticatie', true, 10, '{"enable_user_login":"1"}', true, 'authldap');

INSERT INTO groups (path, name, description, date_created, date_modified) VALUES
    ('{1}',   'Development', 'Development root',    NOW(), NOW()),
    ('{1,2}', 'Backoffice',  'Default backoffice',  NOW(), NOW()),
    ('{1,3}', 'Frontoffice', 'Default frontoffice', NOW(), NOW());

INSERT INTO roles (parent_group_id, name, description, system_role, date_created, date_modified) VALUES
    ((SELECT id FROM groups WHERE name = 'Development'), 'Administrator',             'Systeemrol: Administrator', true, NOW(), NOW()),
    ((SELECT id FROM groups WHERE name = 'Development'), 'Zaaksysteembeheerder',      'Systeemrol: Zaaksysteembeheerder', true, NOW(), NOW()),
    ((SELECT id FROM groups WHERE name = 'Development'), 'Zaaktypebeheerder',         'Systeemrol: Zaaktypebeheerder', true, NOW(), NOW()),
    ((SELECT id FROM groups WHERE name = 'Development'), 'Zaakbeheerder',             'Systeemrol: Zaakbeheerder', true, NOW(), NOW()),
    ((SELECT id FROM groups WHERE name = 'Development'), 'Contactbeheerder',          'Systeemrol: Contactbeheerder', true, NOW(), NOW()),
    ((SELECT id FROM groups WHERE name = 'Development'), 'Basisregistratiebeheerder', 'Systeemrol: Basisregistratiebeheerder', true, NOW(), NOW()),
    ((SELECT id FROM groups WHERE name = 'Development'), 'Wethouder',                 'Systeemrol: Wethouder', true, NOW(), NOW()),
    ((SELECT id FROM groups WHERE name = 'Development'), 'Directielid',               'Systeemrol: Directielid', true, NOW(), NOW()),
    ((SELECT id FROM groups WHERE name = 'Development'), 'Afdelingshoofd',            'Systeemrol: Afdelingshoofd', true, NOW(), NOW()),
    ((SELECT id FROM groups WHERE name = 'Development'), 'Kcc-medewerker',            'Systeemrol: Kcc-medewerker', true, NOW(), NOW()),
    ((SELECT id FROM groups WHERE name = 'Development'), 'Zaakverdeler',              'Systeemrol: Zaakverdeler', true, NOW(), NOW()),
    ((SELECT id FROM groups WHERE name = 'Development'), 'Behandelaar',               'Systeemrol: Behandelaar', true, NOW(), NOW()),
    ((SELECT id FROM groups WHERE name = 'Development'), 'Gebruikersbeheerder',       'Systeemrol: Gebruikersbeheerder', true, NOW(), NOW()),
    ((SELECT id FROM groups WHERE name = 'Development'), 'Documentintaker',           'Systeemrol: Documentintaker', true, NOW(), NOW()),
    ((SELECT id FROM groups WHERE name = 'Development'), 'BRP externe bevrager',      'Systeemrol: BRP externe bevrager', true, NOW(), NOW()),
    ((SELECT id FROM groups WHERE name = 'Development'), 'App gebruiker',             'Systeemrol: Verleent enkel toegang tot apps', true, NOW(), NOW()),
    ((SELECT id FROM groups WHERE name = 'Development'), 'Persoonsverwerker',         'Systeemrol: Persoonsverwerker', true, NOW(), NOW()),
    ((SELECT id FROM groups WHERE name = 'Development'), 'Klantcontacter',            'Systeemrol: Klantcontacter', true, NOW(), NOW())
    ;

    INSERT INTO rights (name, description) VALUES
        (
            'user',
            'Be a user in the system'
        ),
        (
            'dashboard',
            'Be able to view a dashboard'
        ),
        (
            'case_read',
            'Be able to read cases'
        ),
        (
            'cases_read_own',
            'Be able to view cases that are created by oneself'
        ),
        (
            'case_add',
            'Be able to add cases'
        ),
        (
            'case_edit',
            'Be able to edit cases'
        ),
        (
            'case_manage',
            'Be able to manage cases'
        ),
        (
            'view_sensitive_contact_data',
            'Be able to view GDPR datas'
        );


INSERT INTO subject(subject_type, properties, settings, username, role_ids, group_ids, nobody, system) VALUES
    (
        'employee',
        '{"givenname":"Ad","initials":"A.","cn":"admin","telephonenumber":"0612345678","sn":"Mïn","displayname":"Ad Mïn","mail":"devnull@zaaksysteem.nl"}',
        '{}',
        'admin',
        ARRAY(SELECT id FROM roles WHERE name IN('Administrator', 'Behandelaar')),
        ARRAY(SELECT id FROM groups WHERE name IN('Backoffice')),
        false,
        true
    ),
    (
        'employee',
        '{"initials":"beheerder","givenname":"beheerder","telephonenumber":"0612345678","cn":"beheerder","sn":"beheerder","mail":"devnull@zaaksysteem.nl","displayname":"beheerder"}',
        '{}',
        'beheerder',
        ARRAY(SELECT id FROM roles WHERE name IN('Administrator', 'Behandelaar')),
        ARRAY(SELECT id FROM groups WHERE name IN('Backoffice')),
        false,
        true
    ),
    (
        'employee',
        '{"givenname":"gebruiker","initials":"gebruiker","cn":"gebruiker","telephonenumber":"0612345678","sn":"gebruiker","displayname":"gebruiker","mail":"devnull@zaaksysteem.nl"}',
        '{}',
        'gebruiker',
        ARRAY(SELECT id FROM roles WHERE name IN('Behandelaar')),
        ARRAY(SELECT id FROM groups WHERE name IN('Frontoffice')),
        false,
        true
    );

INSERT INTO user_entity(source_interface_id, source_identifier, subject_id, date_created, properties, password) VALUES
    (
        (SELECT id FROM interface WHERE module = 'authldap'),
        'admin',
        (SELECT id FROM subject WHERE username = 'admin'),
        NOW(),
        '{}',
        '{SSHA}UePJXd0aWZtRJG9y2f0YEHkQkBo4xDZV' -- "admin"
    ),
    (
        (SELECT id FROM interface WHERE module = 'authldap'),
        'beheerder',
        (SELECT id FROM subject WHERE username = 'beheerder'),
        NOW(),
        '{}',
        '{SSHA}4RU5KnridRYjqJGOgC6T645PL87W81j4' -- "beheerder"
    ),
    (
        (SELECT id FROM interface WHERE module = 'authldap'),
        'gebruiker',
        (SELECT id FROM subject WHERE username = 'gebruiker'),
        NOW(),
        '{}',
        '{SSHA}ypTduQij55Q/K02zcRBZ3i/O3ThIxFzz' -- "gebruiker"
    );

COMMIT;
