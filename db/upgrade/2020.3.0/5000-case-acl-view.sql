BEGIN;
  CREATE TABLE case_authorisation_map (
    key text not null unique,
    legacy_key text not null unique,
    primary key (key, legacy_key)
  );

  INSERT INTO case_authorisation_map VALUES (
    'read', 'zaak_read'
  ),
  (
    'write', 'zaak_edit'
  ),
  (
    'manage', 'zaak_beheer'
  ),
  (
    'search', 'zaak_search'
  );

COMMIT;

BEGIN;

  ALTER TABLE zaak ADD COLUMN confidential BOOLEAN NOT NULL default false;
  UPDATE zaak set confidential = true where confidentiality = 'confidential';

COMMIT;

BEGIN;

  DROP TRIGGER IF EXISTS set_confidential ON "zaak";

  CREATE OR REPLACE FUNCTION set_confidential_on_case ()
    RETURNS TRIGGER
    LANGUAGE plpgsql
    AS $$
  BEGIN
    IF NEW.confidentiality IS NOT NULL
    THEN
      IF NEW.confidentiality = 'confidential'
      THEN
        NEW.confidential = true;
      ELSE
        NEW.confidential = false;
      END IF;
    END IF;
    RETURN NEW;
  END
  $$;

  CREATE TRIGGER set_confidential
    BEFORE INSERT
    OR UPDATE
    ON zaak
    FOR EACH ROW
    EXECUTE PROCEDURE set_confidential_on_case ();

COMMIT;

BEGIN;

  DROP INDEX IF EXISTS subject_position_matrix_group_role_subject_idx;
  DROP INDEX IF EXISTS subject_position_matrix_position_subject_idx;
  DROP INDEX IF EXISTS subject_position_matrix_subject_id_idx;
  DROP INDEX IF EXISTS zaak_authorisation_all_idx;
  DROP INDEX IF EXISTS zaak_authorisation_all_without_case_idx;
  DROP INDEX IF EXISTS zaak_authorisation_entity_type_capability_idx;
  DROP INDEX IF EXISTS zaak_authorisation_entity_type_idx;
  DROP INDEX IF EXISTS zaak_confidential_idx;
  DROP INDEX IF EXISTS zaak_id_confidential_idx;
  DROP INDEX IF EXISTS zaak_open_idx;
  DROP INDEX IF EXISTS zaak_zaaktype_id_idx;
  DROP INDEX IF EXISTS zaaktype_authorisation_recht_idx;

  DROP INDEX IF EXISTS zaak_behandelaar_idx;
  DROP INDEX IF EXISTS zaak_coordinator_idx;
  DROP INDEX IF EXISTS zaak_aanvrager_idx;

  DROP INDEX IF EXISTS zaak_betrokkenen_betrokkene_id_idx;

  DROP VIEW IF EXISTS case_acl;
  DROP MATERIALIZED VIEW IF EXISTS subject_position_matrix;

  CREATE MATERIALIZED VIEW subject_position_matrix AS
  SELECT
    s.id AS "subject_id",
    unrolled_groups.gid AS "group_id",
    unrolled_roles.rid AS "role_id",
    unrolled_groups.gid::VARCHAR || '|' || unrolled_roles.rid::VARCHAR AS "position"
  FROM
    subject s,
    (
      SELECT
        s.id AS subject_id,
        unnest(s.role_ids) AS rid
      FROM
        subject s) unrolled_roles,
    (
      SELECT
        s.id AS subject_id,
        unnest(g.path) AS gid
      FROM
        subject s
        JOIN GROUPS g ON s.group_ids[1] = g.id) AS unrolled_groups,
    "roles" ro,
    "groups" gr
  WHERE
    s.id = unrolled_roles.subject_id
    AND s.id = unrolled_groups.subject_id
    AND ro.id = unrolled_roles.rid
    AND gr.id = unrolled_groups.gid;

COMMIT;

CREATE INDEX CONCURRENTLY subject_position_matrix_group_role_subject_idx ON subject_position_matrix (subject_id, role_id, group_id);
CREATE INDEX CONCURRENTLY subject_position_matrix_position_subject_idx ON subject_position_matrix (subject_id, position);
CREATE INDEX CONCURRENTLY subject_position_matrix_subject_id_idx ON subject_position_matrix (subject_id);
CREATE INDEX CONCURRENTLY zaak_authorisation_all_idx ON zaak_authorisation (entity_type, capability, zaak_id);
CREATE INDEX CONCURRENTLY zaak_authorisation_all_without_case_idx ON zaak_authorisation (entity_type, capability);
CREATE INDEX CONCURRENTLY zaak_authorisation_entity_type_capability_idx ON zaak_authorisation (entity_type, capability) where entity_type = 'position';
CREATE INDEX CONCURRENTLY zaak_authorisation_entity_type_idx ON zaak_authorisation (entity_type);
CREATE INDEX CONCURRENTLY zaak_confidential_idx ON zaak (confidential);
CREATE INDEX CONCURRENTLY zaak_id_confidential_idx ON zaak (id, confidential);
CREATE INDEX CONCURRENTLY zaak_open_idx ON zaak (id) WHERE deleted IS NULL;
CREATE INDEX CONCURRENTLY zaak_zaaktype_id_idx ON zaak (zaaktype_id);
CREATE INDEX CONCURRENTLY zaaktype_authorisation_recht_idx ON zaaktype_authorisation (recht);

CREATE INDEX CONCURRENTLY zaak_behandelaar_idx ON zaak(behandelaar);
CREATE INDEX CONCURRENTLY zaak_aanvrager_idx ON zaak(aanvrager);
CREATE INDEX CONCURRENTLY zaak_coordinator_idx ON zaak(coordinator);

CREATE INDEX CONCURRENTLY zaak_betrokkenen_betrokkene_id_idx ON zaak_betrokkenen(betrokkene_id);

BEGIN;

  DROP TRIGGER IF EXISTS refresh_subject_position_matrix ON "roles";
  DROP TRIGGER IF EXISTS refresh_subject_position_matrix ON "groups";
  DROP TRIGGER IF EXISTS refresh_subject_position_matrix ON "subject";

  CREATE OR REPLACE FUNCTION refresh_subject_position_matrix ()
    RETURNS TRIGGER
    LANGUAGE plpgsql
    AS $$
  BEGIN
    REFRESH MATERIALIZED VIEW subject_position_matrix;
    RETURN NULL;
  END
  $$;

  CREATE TRIGGER refresh_subject_position_matrix
    AFTER INSERT
    OR UPDATE
    OR DELETE
    OR TRUNCATE ON "subject" FOR EACH statement
    EXECUTE PROCEDURE refresh_subject_position_matrix ();

  CREATE TRIGGER refresh_subject_position_matrix
    AFTER INSERT
    OR UPDATE
    OR DELETE
    OR TRUNCATE ON "roles" FOR EACH statement
    EXECUTE PROCEDURE refresh_subject_position_matrix ();

  CREATE TRIGGER refresh_subject_position_matrix
    AFTER INSERT
    OR UPDATE
    OR DELETE
    OR TRUNCATE ON "groups" FOR EACH statement
    EXECUTE PROCEDURE refresh_subject_position_matrix ();

  CREATE VIEW case_acl AS
  SELECT
    z.id AS case_id,
    z.uuid AS case_uuid,
    cam.key AS permission,
    s.id AS subject_id,
    s.uuid AS subject_uuid,
    z.zaaktype_id as casetype_id
  FROM
    zaak z
    JOIN zaaktype_authorisation za on (za.zaaktype_id = z.zaaktype_id and za.confidential = z.confidential)
    JOIN subject_position_matrix spm
    JOIN subject s ON (spm.subject_id = s.id) ON (spm.role_id = za.role_id AND spm.group_id = za.ou_id)
    JOIN case_authorisation_map cam ON (za.recht = cam.legacy_key)
  UNION ALL
  SELECT
    z.id,
    z.uuid,
    za.capability,
    s.id,
    s.uuid,
    z.zaaktype_id
  FROM
    zaak z
    JOIN zaak_authorisation za ON (za.zaak_id = z.id
        AND za.entity_type = 'position')
    JOIN subject_position_matrix spm
    JOIN subject s ON (spm.subject_id = s.id) ON spm.position = za.entity_id
  UNION ALL
  SELECT
    z.id,
    z.uuid,
    za.capability,
    s.id,
    s.uuid,
    z.zaaktype_id
  FROM
    zaak z
    JOIN zaak_authorisation za ON (za.zaak_id = z.id
        AND za.entity_type = 'user')
    JOIN subject s ON (s.username = za.entity_id)
  UNION ALL
  SELECT
    z.id AS case_id,
    z.uuid AS case_uuid,
    unnest(ARRAY['read', 'write', 'search'])::text,
    s.id AS subject_id,
    s.uuid AS subject_uuid,
    z.zaaktype_id as casetype_id
  FROM
    zaak z
    JOIN zaak_betrokkenen zb ON (
      zb.id = z.aanvrager
      OR
      zb.id = z.behandelaar
      OR
      zb.id = z.coordinator
      AND zb.betrokkene_type = 'medewerker'
    )
    JOIN subject s ON (zb.betrokkene_id = s.id)
   ;

COMMIT;
