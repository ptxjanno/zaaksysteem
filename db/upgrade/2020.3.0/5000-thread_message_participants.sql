BEGIN;
ALTER TABLE thread_message_external
  ADD participants TEXT NOT NULL DEFAULT '{}';
ALTER TABLE thread_message
  ALTER created_by_uuid DROP NOT NULL;
ALTER TABLE thread_message
  ALTER created_by_displayname DROP NOT NULL;
COMMIT;

