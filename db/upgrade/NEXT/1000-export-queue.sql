BEGIN;

  -- These two are primairly for developer machines should not be run on
  -- production environments

  DROP TABLE IF EXISTS export_queue;
  ALTER TABLE filestore DROP CONSTRAINT IF EXISTS filestore_uuid_key;

COMMIT;

BEGIN;

  ALTER TABLE filestore ADD CONSTRAINT filestore_uuid_key UNIQUE (uuid);
  CREATE TABLE export_queue (
    id SERIAL primary key,
    uuid uuid DEFAULT uuid_generate_v4(),
    subject_id int REFERENCES subject(id) NOT NULL,
    subject_uuid uuid REFERENCES subject(uuid) NOT NULL,
    expires timestamp with time zone DEFAULT NOW() + INTERVAL '3d' NOT NULL,
    token text NOT NULL,
    filestore_id int REFERENCES filestore(id) NOT NULL,
    filestore_uuid uuid REFERENCES filestore(uuid) NOT NULL,
    downloaded int default 0 NOT NULL
  );

COMMIT;
