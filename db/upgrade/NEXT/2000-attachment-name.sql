BEGIN;

ALTER TABLE thread_message_attachment
    ADD filename varchar;

UPDATE
    thread_message_attachment
SET
    filename = filestore.original_name
FROM
    filestore
WHERE
    filestore.id = thread_message_attachment.filestore_id;

ALTER TABLE thread_message_attachment
    ALTER COLUMN filename SET NOT NULL;

COMMIT;

