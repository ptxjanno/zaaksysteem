BEGIN;

ALTER TABLE bag_ligplaats ALTER COLUMN documentdatum DROP NOT NULL;
ALTER TABLE bag_nummeraanduiding ALTER COLUMN documentdatum DROP NOT NULL;
ALTER TABLE bag_openbareruimte ALTER COLUMN documentdatum DROP NOT NULL;
ALTER TABLE bag_pand ALTER COLUMN documentdatum DROP NOT NULL;
ALTER TABLE bag_standplaats ALTER COLUMN documentdatum DROP NOT NULL;
ALTER TABLE bag_verblijfsobject ALTER COLUMN documentdatum DROP NOT NULL;
ALTER TABLE bag_woonplaats ALTER COLUMN documentdatum DROP NOT NULL;

ALTER TABLE bag_ligplaats ALTER COLUMN documentnummer DROP NOT NULL;
ALTER TABLE bag_nummeraanduiding ALTER COLUMN documentnummer DROP NOT NULL;
ALTER TABLE bag_openbareruimte ALTER COLUMN documentnummer DROP NOT NULL;
ALTER TABLE bag_pand ALTER COLUMN documentnummer DROP NOT NULL;
ALTER TABLE bag_standplaats ALTER COLUMN documentnummer DROP NOT NULL;
ALTER TABLE bag_verblijfsobject ALTER COLUMN documentnummer DROP NOT NULL;
ALTER TABLE bag_woonplaats ALTER COLUMN documentnummer DROP NOT NULL;

COMMIT;