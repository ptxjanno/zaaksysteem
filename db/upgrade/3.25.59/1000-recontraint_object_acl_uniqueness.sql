BEGIN;

-- Cleanup duplicate acl entrys
DELETE FROM object_acl_entry WHERE uuid IN (
    SELECT uuid FROM (
        -- Group results by all rows minus uuid
        SELECT uuid, ROW_NUMBER() OVER (
            PARTITION BY object_uuid, entity_type, entity_id, capability, scope, groupname
        ) AS rnum FROM object_acl_entry
    ) t
    -- Only select duplicate rows
    WHERE t.rnum > 1
);

CREATE UNIQUE INDEX IF NOT EXISTS object_acl_entry_unique_idx ON object_acl_entry (
    object_uuid,
    entity_type,
    entity_id,
    capability,
    scope,
    groupname
);

COMMIT;
