BEGIN;

ALTER TABLE object_relationships
    DROP CONSTRAINT IF EXISTS object_relationships_object1_uuid_fkey,
    DROP CONSTRAINT IF EXISTS object_relationships_object2_uuid_fkey,
    ADD CONSTRAINT object_relationships_object1_uuid_fkey FOREIGN KEY (object1_uuid) REFERENCES object_data(uuid) ON DELETE CASCADE,
    ADD CONSTRAINT object_relationships_object2_uuid_fkey FOREIGN KEY (object2_uuid) REFERENCES object_data(uuid) ON DELETE CASCADE;

ALTER TABLE object_acl_entry
    DROP CONSTRAINT IF EXISTS object_data_fkey,
    ADD CONSTRAINT object_data_fkey FOREIGN KEY (object_uuid) REFERENCES object_data(uuid) ON DELETE CASCADE;

COMMIT;
