BEGIN;

    UPDATE zaak_betrokkenen SET authorisation = 'write' where authorisation = 'manage';

    ALTER TABLE zaak_betrokkenen ADD CONSTRAINT zaaktype_authorisation_recht
        CHECK (authorisation = ANY (ARRAY[
        'none'::text,
        'search'::text,
        'read'::text,
        'write'::text,
        NULL
    ]));

COMMIT;
