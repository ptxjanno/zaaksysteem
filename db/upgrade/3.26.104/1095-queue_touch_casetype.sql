/*
    This will queue `touch_casetype` tasks for the `backend`.
    
    We have removed the `casetype_result` object_types from `object_relation`,
    but when we touch the case-types that (previously) had results, they will be
    recreated
*/

BEGIN;

WITH

zaaktype_joined_resultaten AS (
    
    SELECT
        zaaktype.id,
        zaaktype.zaaktype_node_id AS zaaktype_node_id,
        resultaat, label,
        standaard_keuze
    FROM
        zaaktype
    JOIN
        zaaktype_resultaten
        ON (
            zaaktype.zaaktype_node_id = zaaktype_resultaten.zaaktype_node_id
        )
),

zaaktype_to_touch AS (
    
    SELECT DISTINCT
        id AS zaaktype_id
    FROM
        zaaktype_joined_resultaten
    ORDER BY id
)

INSERT INTO
    queue (
        type,
        label,
        priority,
        metadata,
        data
    )
    SELECT
        'touch_casetype',
        'Devops: queue_touch_casetype',
        3000,
        '{"require_object_model":1, "disable_acl": 1, "target":"backend"}',
        '{"id":' || zaaktype_id || '}'
FROM zaaktype_to_touch

;

COMMIT;
