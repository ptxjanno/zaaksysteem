BEGIN;

    UPDATE file SET confidential = true WHERE id IN
        (
            SELECT f.id FROM file f
            JOIN file_metadata md ON f.metadata_id = md.id
            WHERE md.trust_level IN
                ('Vertrouwelijk', 'Confidentieel', 'Geheim', 'Zeer geheim')
        );

COMMIT;
