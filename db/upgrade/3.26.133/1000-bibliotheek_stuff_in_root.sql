BEGIN;

    -- The root folder is now usable for "regular" items
    ALTER TABLE object_bibliotheek_entry ALTER COLUMN bibliotheek_categorie_id DROP NOT NULL;

COMMIT;
