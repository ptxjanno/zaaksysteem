BEGIN;

    ALTER TABLE file_metadata DROP CONSTRAINT IF EXISTS file_metadata_trust_level_check;
    ALTER TABLE file_metadata ADD CONSTRAINT file_metadata_trust_level_check CHECK (((trust_level)::text ~
            '(Openbaar|Beperkt openbaar|Intern|Zaakvertrouwelijk|Vertrouwelijk|Confidentieel|Geheim|Zeer geheim)'::text));

COMMIT;
