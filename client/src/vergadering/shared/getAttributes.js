import partition from 'lodash/partition';
import filter from 'lodash/filter';
import get from 'lodash/get';
import seamlessImmutable from 'seamless-immutable';

const getAttributes = config => {
	const [ voorstel, vergadering ] = partition(
		filter(get(config, 'instance.interface_config.attribute_mapping', []), ( attribute ) => attribute.hasOwnProperty('internal_name') && attribute.internal_name !== null),
		( attribute ) => attribute.external_name.indexOf('voorstel') === 0
	);

	return {
		voorstel: seamlessImmutable(voorstel),
		vergadering: seamlessImmutable(vergadering),
		all: seamlessImmutable(config.instance.interface_config.attribute_mapping)
	};
};

export default getAttributes;
