import angular from 'angular';
import template from './template.html';
import composedReducerModule from '../../../shared/api/resource/composedReducer';

import './styles.scss';

export default
		angular.module('Zaaksysteem.meeting.groupListView', [
			composedReducerModule
		])
		.directive('groupListView', [ '$state', 'composedReducer', ( $state, composedReducer ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					appConfig: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( $scope ) {

					let ctrl = this,
						configReducer;

					configReducer = composedReducer( { scope: $scope }, ctrl.appConfig )
						.reduce( configs => {
							return configs.map( config => {

								return {
									id: config.instance.id,
									label: config.instance.interface_config.title_normal,
									link: config.instance.interface_config.short_name,
									href: config.instance.interface_config.defaultview === 'per_meeting' ? $state.href('meetingList', { group: config.instance.interface_config.short_name, meetingType: 'open' }) : $state.href('proposalsList', { group: config.instance.interface_config.short_name, meetingType: 'open' })
								};
							});
						});

					ctrl.getGroups = configReducer.data;

				}],
				controllerAs: 'groupListView'
			};

		}
		])
		.name;
