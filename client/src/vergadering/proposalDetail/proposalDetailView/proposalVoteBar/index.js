import angular from 'angular';
import template from './template.html';
import angularUiRouter from 'angular-ui-router';
import zsModalModule from '../../../../shared/ui/zsModal';
import templateVoteModal from './template-votemodal.html';
import first from 'lodash/head';
import zsConfirmModule from '../../../../shared/ui/zsConfirm';
import zsSpinnerModule from '../../../../shared/ui/zsSpinner';
import snackbarServiceModule from '../../../../shared/ui/zsSnackbar/snackbarService';

import './style.scss';

export default
		angular.module('Zaaksysteem.meeting.proposalVoteBar', [
			angularUiRouter,
			zsModalModule,
			zsConfirmModule,
			zsSpinnerModule,
			snackbarServiceModule
		])
		.directive('proposalVoteBar',
			[ '$window', '$timeout', '$state', '$compile', '$http', 'zsModal', 'zsConfirm', 'snackbarService',
			( $window, $timeout, $state, $compile, $http, zsModal, zsConfirm, snackbarService ) => {

			return {

				restrict: 'E',
				template,
				scope: {
					proposalReference: '&',
					proposalTitle: '&',
					proposalVoteConfig: '&',
					proposalVoteData: '&',
					proposalClosed: '&',
					isVotingPossible: '&',
					onVote: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ($scope) {

					let ctrl = this,
						voteEditModal,
						isLoading = false;

					ctrl.getButtons = ( ) => ctrl.proposalVoteConfig().choice.values;

					ctrl.getVotedValue = ( ) => first(ctrl.proposalVoteData().choice) || '';

					ctrl.comment = ctrl.proposalVoteData().comment || '';

					if (!ctrl.isVotingPossible()) {
						snackbarService.info('Dit voorstel heeft geen behandelaar en/of coördinator. Stemmen is daarom met uw rechten niet mogelijk.');
					}

					ctrl.handleVoteAction = ( action ) => {

						ctrl.choice = action;

						let voteModalTemplate = angular.element(templateVoteModal);

						voteEditModal = zsModal({
							el: $compile(angular.element(voteModalTemplate))($scope),
							title: ctrl.proposalTitle(),
							classes: 'vote-modal'
						});

						voteEditModal.open();

						$timeout(( ) => {
							voteModalTemplate.find('textarea')[0].focus();
						}, 0, false);

					};

					ctrl.isCaseClosed = ( ) => ctrl.proposalClosed();

					ctrl.submitVote = ( ) => {

						voteEditModal.hide();

						isLoading = true;

						let data = {
							values:
								{
									[ctrl.proposalVoteConfig().choice.magic_string]: [ ctrl.choice ],
									[ctrl.proposalVoteConfig().comment.magic_string]: [ ctrl.comment ]
								}
						};

						ctrl.onVote({
							$proposalReference: ctrl.proposalReference(),
							$voteData: data
						})
						.then( ( ) => {

							isLoading = false;

							voteEditModal.close();
						})
						.catch( ( ) => {

							isLoading = false;

							snackbarService.error('Er ging iets mis bij het opslaan van uw stem. Neem contact op met uw beheerder voor meer informatie.');

						});

					};

					ctrl.handleEditVote = ( ) => {

						zsConfirm( 'Weet u zeker dat u uw stem wilt wijzigen?', 'Ja')
						.then( ( ) => {

							isLoading = true;

							// We are only clearing the 'akkoord' value when editing a vote,
							// but leaving the comment untouched.

							let data = {
								values:
									{
										[ctrl.proposalVoteConfig().choice.magic_string]: [ '' ]
									}
							};

							ctrl.onVote({
								$proposalReference: ctrl.proposalReference(),
								$voteData: data
							})
							.then( ( ) => {

								isLoading = false;

							})
							.catch( ( ) => {

								isLoading = false;

								snackbarService.error('Er ging iets mis bij het wijzigen van uw stem. Neem contact op met uw beheerder voor meer informatie.');

							});


						});

					};


					ctrl.goBack = ( ) => {

						voteEditModal.close();
					};

					ctrl.isLoading = ( ) => isLoading;

					$scope.$on('$destroy', ( ) => {
						if (voteEditModal) {
							voteEditModal.close();
						}
					});

				}],
				controllerAs: 'proposalVoteBar'

			};
		}
		])
		.name;
