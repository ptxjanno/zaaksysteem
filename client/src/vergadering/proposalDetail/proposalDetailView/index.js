import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import proposalNoteBarModule from './proposalNoteBar';
import proposalVoteBarModule from './proposalVoteBar';
import rwdServiceModule from '../../../shared/util/rwdService';
import auxiliaryRouteModule from '../../../shared/util/route/auxiliaryRoute';
import snackbarServiceModule from '../../../shared/ui/zsSnackbar/snackbarService';
import zsPdfViewerModule from '../../../shared/ui/zsPdfViewer';
import zsModalModule from '../../../shared/ui/zsModal';
import sessionServiceModule from '../../../shared/user/sessionService';
import actionsModule from './actions';
import controller from './ProposalDetailViewController';
import template from './template.html';
import './styles.scss';

export default angular
	.module('Zaaksysteem.meeting.proposalDetailView', [
		actionsModule,
		angularUiRouterModule,
		auxiliaryRouteModule,
		proposalNoteBarModule,
		proposalVoteBarModule,
		rwdServiceModule,
		sessionServiceModule,
		snackbarServiceModule,
		zsPdfViewerModule,
		zsModalModule
	])
	.component('proposalDetailView', {
		bindings: {
			proposal: '&',
			documents: '&',
			casetype: '&',
			notes: '&',
			onSaveNote: '&',
			appConfig: '&'
		},
		controller,
		template
	})
	.name;
