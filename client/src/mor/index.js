import angular from 'angular';
import 'angular-i18n/angular-locale_nl';
import ngAnimate from 'angular-animate';
import routing from './routing';
import resourceModule from './../shared/api/resource';
import zsResourceConfiguration from './../shared/api/resource/resourceReducer/zsResourceConfiguration';
import configServiceModule from './shared/configService';
import appServiceModule from './shared/appService';
import serviceworkerModule from './../shared/util/serviceworker';
import get from 'lodash/get';
import without from 'lodash/without';
import uniq from 'lodash/uniq';
import createManifest from './shared/manifest';
import sessionServiceModule from '../shared/user/sessionService';
import loadIconFont from '../shared/util/loadIconFont';

export default angular
	.module('Zaaksysteem.mor', [
		routing,
		resourceModule,
		ngAnimate,
		configServiceModule,
		appServiceModule,
		serviceworkerModule,
		sessionServiceModule
	])
	.config(['$provide', ( $provide ) => {

		// make sure sourcemaps work
		// from https://github.com/angular/angular.js/issues/5217

		$provide.decorator('$exceptionHandler', ['$delegate', ( $delegate ) => {

			return ( exception, cause ) => {
				$delegate(exception, cause);
				setTimeout(() => {
					console.error(exception.stack);
				});
			};

		}]);

	}])
	.config(['appServiceProvider', appServiceProvider => {
		appServiceProvider.setDefaultState({ filters: [] })
			.reduce('filter_add', 'filters', ( filters, filterToAdd ) => uniq(filters.concat(filterToAdd)))
			.reduce('filter_remove', 'filters', ( filters, filterToRemove ) => without(filters, filterToRemove))
			.reduce('filter_clear', 'filters', () => []);
	}])
	.config(['$httpProvider', ( $httpProvider ) => {
		$httpProvider.defaults.withCredentials = true;
		$httpProvider.defaults.headers.common['X-Client-Type'] = 'web';
		$httpProvider.useApplyAsync(true);
		$httpProvider.interceptors.push([
			'zsStorage', '$window', '$q',
			( zsStorage, $window, $q ) => ({
				response: response => {
					if (response.config.url === '/api/v1/session/current') {
						if (!response.data.result.instance.logged_in_user) {
							zsStorage.clear();
							$window.location.href = `/auth/login?referer=${$window.location.pathname}`;
							return $q.reject(response);
						}
					}

					return $q.resolve(response);
				},
				responseError: ( rejection ) => {
					if (rejection.status === 401
						// configuration_incomplete implies the API is not configured for public accces,
						// which means we don't have a logged in user
						|| get(rejection, 'data.result.instance.type') === 'api/v1/configuration_incomplete'
					) {
						zsStorage.clear();
						$window.location.href = `/auth/login?referer=${$window.location.pathname}`;
					}

					return $q.reject(rejection);
				}
			})]);
	}])
	.config(['resourceProvider', resourceProvider => {
		zsResourceConfiguration(resourceProvider.configure);
	}])
	.run(loadIconFont)
	.run([
		'$document', 'configService',
		( $document, configService ) => {
			let styleEl;

			configService.getResource().onUpdate(data => {

				createManifest(get(data, 'instance.interface_config'), $document);

				let color = get(data, 'instance.interface_config.accent_color', '#FFF');
				let rules = [
					`#nprogress .bar {
					     background: ${color};
					     height: 100%;
					 }`,
					`#nprogress .peg {
					     box-shadow: 0 0 20px ${color}, 0 0 10px #FFF;
					 }`,
					`#nprogress .spinner-icon {
					     border-top-color: ${color};
					     border-left-color: ${color};
					 }`,
					`case-list-item-list-item .case-list-item--title{
					     color: ${color} !important;
					 }`,
					`case-list-item-list-item > a:hover {
					     color: ${color} !important;
					 }`,
					`.case-list-view__supportlink a {
					     color: ${color} !important;
					 }`
				];

				if (styleEl) {
					styleEl.remove();
				}

				styleEl = $document[0].createElement('style');

				$document[0].head.appendChild(styleEl);

				rules.forEach(( rule, index ) => {
					styleEl.sheet.insertRule(rule, index);
				});

			});

		}])
	.run(['$animate', $animate => {
		$animate.enabled(true);
	}])
	.run([
		'$window', 'serviceWorker',
		( $window, serviceWorker ) => {
			let enable = ENV.USE_SERVICE_WORKERS;
			let worker = serviceWorker();

			if (worker.isSupported()) {
				worker
					.isEnabled()
					.then(enabled => {
						if (enabled !== enable) {
							return (enable ?
									worker.enable()
									: worker
										.disable()
										.then(() => {
											if (enabled) {
												$window.location.reload();
											}
										})
							);
						}
					});
			}
		}
	])
	.name;
