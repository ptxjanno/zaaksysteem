import angular from 'angular';
import manifest from '../../.manifest';
import assign from 'lodash/assign';
import url from 'url';
import last from 'lodash/last';
import first from 'lodash/first';

export default ( config, $document ) => {

	let toRemove = [
			'link[rel="manifest"]',
			'link[rel="shortcut icon"]',
			'link[rel="apple-touch-icon"]',
			'link[rel="apple-touch-startup-image"]',
			'meta[name="apple-mobile-web-app-capable"]',
			'meta[name="apple-mobile-web-app-status-bar-style"]',
			'meta[name="apple-mobile-web-app-title"]'
		],
		toAppend = [];

	toRemove.forEach( selector => {
		return angular.element($document[0].querySelectorAll(selector)).remove();
	});

	{
		let manifestEl = $document[0].createElement('link');

		manifestEl.setAttribute('rel', 'manifest');

		manifestEl.setAttribute('href', url.format({
			pathname: '/clientutil/proxy/manifest',
			query: {
				manifest: JSON.stringify(
					assign(manifest, {
						short_name: config.title_small,
						name: config.title_normal,
						background_color: config.header_bgcolor,
						theme_color: config.header_bgcolor,
						display: 'standalone'
					})
				)
			}
		}));

		toAppend = toAppend.concat(manifestEl);
	}

	toAppend =
		toAppend.concat(
			manifest.icons.map(icon => angular.element(`<link rel="apple-touch-icon" sizes="${icon.sizes}" href="${icon.src}">`))
		).concat(
			angular.element(`<link rel="apple-touch-startup-image" href="${last(manifest.icons).src}">`),
			angular.element('<meta name="apple-mobile-web-app-capable" content="yes">'),
			angular.element('<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">'),
			angular.element(`<meta name="apple-mobile-web-app-title" content="${config.title_small}">`)
		)
		.concat(
			angular.element(`<link rel="shortcut icon" href="${first(manifest.icons).src}">`)
		);

	toAppend.forEach( el => {
		$document.find('head').append(el);
	});
};
