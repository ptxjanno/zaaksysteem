import angular from 'angular';
import template from './template.html';
import get from 'lodash/get';
import zsIconModule from './../../../ui/zsIcon';

export default
	angular.module('vormObjectSuggestDisplay', [
		zsIconModule
	])
		.directive('vormObjectSuggestDisplay', [ ( ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					object: '&',
					formatter: '&',
					icon: '@'
				},
				bindToController: true,
				controller: [ function ( ) {

					let ctrl = this;

					ctrl.getObjectType = ( ) => get(ctrl.object(), 'type') || null;

					ctrl.getLabel = ( ) => {

						return ctrl.formatter() ?
							ctrl.formatter()(ctrl.object()) || ''
							: get(ctrl.object(), 'label') || '';
					};


				}],
				controllerAs: 'ctrl'
			};

		}])
		.name;
