import angular from 'angular';

export default
	angular.module('Zaaksysteem.intern.zsBackdropService', [

	])
		.factory('zsBackdropService', [ ( ) => {

			let isActive = false;

			return {
				isEnabled: ( ) => isActive,
				setActive: ( ) => {
					isActive = true;
					return true;
				},
				setDisabled: ( ) => {
					isActive = false;
					return true;
				}
			};

		}])
		.name;
