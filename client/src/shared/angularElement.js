import angular from 'angular';

export const angularElement = htmlString => angular.element(htmlString);
