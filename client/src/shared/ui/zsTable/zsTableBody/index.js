import angular from 'angular';
import template from './template.html';
import cellTemplate from './cell-template.html';
import each from 'lodash/each';
import includes from 'lodash/includes';
import map from 'lodash/map';

export default
	angular.module('zsTableBody', [
	])
		.directive('zsTableBody', [ '$compile', ( $compile ) => {
			
			return {
				scope: {
					columns: '&',
					rows: '&',
					onRowClick: '&',
					useHref: '&'
				},
				bindToController: true,
				controller: [ '$scope', '$element', function ( $scope, $element ) {

					let ctrl = this,
						childScope;

					let recompile = ( ) => {

						let el,
							anchorEl;

						if (childScope) {
							childScope.$destroy();
						}

						childScope = $scope.$new();

						$element.empty();

						el = angular.element(template);

						if (ctrl.useHref()) {
							anchorEl = angular.element('<a ng-href="{{::item.href}}" class="no-underlines"></a>');

							for (let i = 0, attrs = el[0].attributes, l = attrs.length; i < l; ++i) {
								let attr = attrs[i],
									val = attr.value;

								if (attr.name !== 'ng-click') {

									if (attr.name === 'class') {
										val = val.split(' ').concat(anchorEl.attr('class').split(' ')).join(' ');
									}
									anchorEl.attr(attr.name, val);
								}

							}

							el = anchorEl;
						}

						// some cells should not have an aria-label, because the content of the cell already has one
						const columnsWithoutCellAria = [
							'case.number',
							'case.progress_status',
							'case.subject',
							'actions'
						];
						
						each(ctrl.columns(), ( column ) => {
							let cell = angular.element(cellTemplate);
							
							cell.attr('column-id', column.id);

							if(!includes(columnsWithoutCellAria, column.id)) {
								cell.attr('tabindex', "0");
								cell.attr('aria-label', `${column.ariaLabel}: {{item.ariaValues['${column.id}'] ? item.ariaValues['${column.id}'] : 'Geen waarde'}}`);
							}

							// currently doesn't support text nodes, needs a wrapper
							cell.append(angular.element(column.template));
							el.append(cell);
						});

						$compile(el)(childScope);

						$element.append(el);

					};

					$scope.$watchCollection(
						( ) => map(ctrl.columns(), 'templates'),
						recompile
					);

				}],
				controllerAs: 'zsTableBody'
			};

		}])
		.name;
