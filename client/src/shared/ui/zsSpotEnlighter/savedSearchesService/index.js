import angular from 'angular';
import immutable from 'seamless-immutable';
import assign from 'lodash/assign';
import get from 'lodash/get';
import keyBy from 'lodash/keyBy';
import mapValues from 'lodash/mapValues';
import resourceModule from '../../../api/resource/index.js';
import { mockObject } from './../../../object/mock';
import zqlEscapeFilterModule from './../../../object/zql/zqlEscapeFilter';

function resolveDynamicDateSymbol(dateSymbolOrRaw) {
	var dateMatch = /\[\{(.+)\}\]/.exec(dateSymbolOrRaw);
	var dateResult;

	if (dateMatch) {
		var dateSymbol = dateMatch[1];
		var now = new Date(Date.now());

		switch (dateSymbol) {
			case 'WEEK':
				dateResult = now.setDate(now.getDate() + 7);
			case 'MONTH':
				dateResult = now.setDate(now.getDate() + 31);
			default:
				dateResult = now;
		}
	} else {
		dateResult = dateSymbolOrRaw;
	}

	return new Date(dateResult);
}

const patchDynamicDates = (whereString, search) => {
	let withDynamicDateUpdates = whereString;
	const attributes =
		get(search, 'values.query.values.attributes.attributes') || [];
	const dynamicDateUpdates = attributes
		.filter(attr => get(attr, '_value.from', '').toString().includes('}]'))
		.map(attr => ({
			id: attr.id,
			dateSymbol: attr._value.from
		}));

	dynamicDateUpdates.forEach(update => {
		const regexp = new RegExp(update.id + '+.+(\\d{4}-.+Z)\\)');
		withDynamicDateUpdates.replace(regexp, (_, date) => {
			const updatedDate = resolveDynamicDateSymbol(
				update.dateSymbol
			).toISOString();
			withDynamicDateUpdates = withDynamicDateUpdates.replace(
				date,
				updatedDate
			);
		});
	});

	return withDynamicDateUpdates;
};

export default angular
	.module('shared.ui.zsSpotEnlighter.savedSearchesService', [
		resourceModule,
		zqlEscapeFilterModule
	])
	.factory('savedSearchesService', [
		'$rootScope',
		'resource',
		'zqlEscapeFilter',
		($rootScope, resource, zqlEscapeFilter) => {
			let service = {};
			let zqlKeywords = [
				'SELECT',
				'FROM',
				'MATCHING',
				'WHERE',
				'NUMERIC ORDER BY',
				'ORDER BY'
			];

			let predefined = immutable(
				[
					{
						id: 'mine',
						label: 'Mijn openstaande zaken'
					},
					{
						id: 'my-department',
						label: 'Mijn afdeling'
					},
					{
						id: 'all',
						label: 'Alle zaken'
					},
					{
						id: 'intake',
						label: 'Zaakintake'
					}
				].map(item =>
					immutable(
						mockObject('v0', {
							id: item.id,
							label: item.label,
							type: 'saved_search',
							values: {
								public: true,
								title: item.label,
								query: {
									predefined: true,
									objectType: {
										label: 'Zaak',
										id: 1,
										object_type: 'case'
									},
									zql: item.zql
								}
							}
						})
					)
				)
			);

			let convertLegacyTemplate = str =>
				str.replace(/<\[/g, '{{::').replace(/\]>/g, '}}');

			// TODO: move parse to zqlParser
			let parse = zql => {
				let result = mapValues(keyBy(zqlKeywords), key => {
					let match = zql.match(
						new RegExp(`${key}\\s+(.*?)\\s*(${zqlKeywords.join('|')}|$)`)
					);

					if (match) {
						return match[1];
					}

					return '';
				});

				return result;
			};

			let parseLegacyFilter = item =>
				item.merge(
					{
						values: {
							query: JSON.parse(convertLegacyTemplate(item.values.query))
						}
					},
					{
						deep: true
					}
				);

			service.getRequestOptions = query => {
				let zql = 'SELECT {} FROM saved_search';

				if (query) {
					zql += ` MATCHING ${zqlEscapeFilter(query)}`;
				}

				return {
					url: '/api/object/search/',
					params: {
						zql
					}
				};
			};

			service.getZql = (search, options = {}) => {
				let userData = options.user;
				let zql;
				let parts = {
					SELECT: '{}',
					FROM: options.from || search.values.query.objectType.object_type,
					WHERE: '',
					MATCHING: '',
					'ORDER BY': '',
					'NUMERIC ORDER BY': ''
				};
				let searchZql = get(search, 'values.query.zql');

				if (searchZql) {
					parts = assign(parts, parse(searchZql));
					parts.WHERE = patchDynamicDates(parts.WHERE, search);
				}

				if (options.columns) {
					parts.SELECT = options.columns.join(', ');
				}

				switch (search.id) {
					case 'mine':
						parts.WHERE = `(case.assignee.id = ${zqlEscapeFilter(
							userData.instance.logged_in_user.id
						)}) AND (case.status = "open")`;
						break;
					case 'my-department':
						parts.WHERE = `(case.route_ou = ${zqlEscapeFilter(
							get(userData, 'instance.logged_in_user.legacy.ou_id')
						)})`;
						break;
					case 'intake':
						parts.FROM += ' intake';
						break;
				}

				if ('matching' in options) {
					if (options.matching) {
						parts.MATCHING = zqlEscapeFilter(options.matching);
					} else {
						delete parts.MATCHING;
					}
				}

				if (options.sort) {
					let { by, order, type } = options.sort;
					let del;
					let add;

					if (by && order) {
						del = add = 'ORDER BY';

						if (type === 'numeric') {
							add = `NUMERIC ${add}`;
						} else {
							del = `NUMERIC ${del}`;
						}

						parts[add] = `${by} ${order.toUpperCase()}`;
						delete parts[del];
					}
				}

				zql = zqlKeywords
					.reduce((query, keyword) => {
						let reduced = query;

						if (parts[keyword]) {
							reduced += `${keyword} ${parts[keyword]} `;
						}

						return reduced;
					}, '')
					.trim();

				return zql;
			};

			service.getPredefinedSearches = () => predefined;

			service.convertLegacyTemplate = convertLegacyTemplate;

			service.filter = (data, query) =>
				predefined
					.filter(
						item =>
							!query ||
							item.label.toLowerCase().indexOf(query.toLowerCase()) !== -1
					)
					.concat((data || []).map(parseLegacyFilter));

			service.parseLegacyFilter = parseLegacyFilter;

			return service;
		}
	]).name;
