let iconMappings = {
	case: 'folder-outline',
	saved_search: 'file-find',
	file: 'file',
	natuurlijk_persoon: 'account',
	bedrijf: 'domain',
	vraag: 'help',
	product: 'package-variant-closed',
	defaults: 'hexagon-outline'
};

export const getIcon = type => (iconMappings[type] || iconMappings.defaults);
