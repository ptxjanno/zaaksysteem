import angular from 'angular';
import controller from './controller';
import template from './template.html';
import './styles.scss';

export default angular
	.module('zsMessageList', [])
	.component('zsMessageList', {
		bindings: {
			messages: '&'
		},
		controller,
		template
	})
	.name;
