export default class MessageListController {
	constructor() {
		this.hiddenByUser = [];
	}

	hideMessage( message ) {
		this.hiddenByUser.push(message.id);
	}

	isHidden({ id }) {
		return (this.hiddenByUser.indexOf(id) === -1);
	}

	filter(message) {
		return this.isHidden(message);
	}

	get activeMessages() {
		return this
			.messages()
			.filter(this.filter, this);
	}
}
