import angular from 'angular';
import template from './template.html';
import './styles.scss';

export default
	angular.module('zsReactIframe', [])
		.directive(
			'zsReactIframe',
			[() => {
				return {
					restrict: 'E',
					template,
					scope: {
						iframeSrc: '&',
						onLocationChange: '&',
						onMessage: '&'
					},
					bindToController: true,
					controller: [ '$scope', function (scope) {
						let ctrl = this;
						
						ctrl.onMessageListener = (event) => {
							if (event.data && event.data.type === 'locationChange') {
								let iframeUrl = event.data.data;
								if (ctrl.onLocationChange) {
									ctrl.onLocationChange({data: iframeUrl});
								}
							}

							ctrl.onMessage && ctrl.onMessage({ data: event.data });
						};

						window.addEventListener('message', ctrl.onMessageListener);
						scope.$on('$destroy',() => window.removeEventListener('message', ctrl.onMessageListener));

					}],
					controllerAs: 'vm'
				};

			}])
		.name;
