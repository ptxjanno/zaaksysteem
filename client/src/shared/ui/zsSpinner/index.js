import angular from 'angular';
import template from './template.html';

export default
	angular.module('shared.ui.zsSpinner', [
	])
		.directive('zsSpinner', [ ( ) => {

			return {
				restrict: 'E',
				scope: {
					isLoading: '&'
				},
				template
			};

		}])
		.name;
