import angular from 'angular';

export default
  angular.module('zsMoveFocusTo', [
  ])
  .directive('zsMoveFocusTo', () => {
    return {
      link ( scope, element, attr ) {
        const focusHandler = () => {
          document.querySelector(attr.zsMoveFocusTo).focus();
        };
        
        element.on('click', focusHandler);
        scope.$on('$destroy', () => {
          element.off('click', focusHandler);
        });
      },
    };

  })
    .name;
