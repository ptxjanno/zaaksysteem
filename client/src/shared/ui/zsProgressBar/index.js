import angular from 'angular';
import template from './template.html';

export default
	angular.module('zsProgressBar', [
	])
		.directive('zsProgressBar', [ ( ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					progress: '@',
					ariaLabel: '@',
				}
			};

		}])
		.name;
