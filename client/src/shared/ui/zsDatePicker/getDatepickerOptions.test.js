import getDatepickerOptions from './getDatepickerOptions';

// NB: just use date
const toDateObject = dateString => new Date(dateString);

/**
 * @test {getDatepickerOptions}
 */
describe('The `getDatepickerOptions` function', () => {

	describe('uses the `min` property of its first parameter', () => {
		test('to set `minDate` and `defaultDate` to undefined if it is not set', () => {
			const options = getDatepickerOptions({
				max: '9999-12-31'
			}, toDateObject);

			expect(options.minDate).toBe(undefined);
			expect(options.defaultDate).toBe(undefined);
		});

		test('to set `minDate` to a date if it is set', () => {
			const options = getDatepickerOptions({
				min: '1900-01-01',
				max: '9999-12-31'
			}, toDateObject);

			expect(options.minDate).toBeInstanceOf(Date);
		});

		test('to set `defaultDate` to `undefined` if it is in the `minDate`/`maxDate` range', () => {
			const date = new Date();
			const nextYear = String(date.getFullYear() + 1);
			const prevYear = String(date.getFullYear() - 1);
			const options = getDatepickerOptions({
				min: `${prevYear}-01-01`,
				max: `${nextYear}-01-01`
			}, toDateObject);

			expect(options.defaultDate).toBe(undefined);
		});

		test('to set `defaultDate` to `minDate` if `minDate` is in a future month', () => {
			const nextYear = ((new Date()).getFullYear() + 1);
			const options = getDatepickerOptions({
				min: `${nextYear}-01-01`,
				max: `${nextYear}-01-31`
			}, toDateObject);

			expect(options.defaultDate).toBe(options.minDate);
		});

		test('to set `defaultDate` to `maxDate` if `maxDate` is in a past month', () => {
			const prevYear = ((new Date()).getFullYear() - 1);
			const options = getDatepickerOptions({
				min: `${prevYear}-01-01`,
				max: `${prevYear}-01-31`
			}, toDateObject);

			expect(options.defaultDate).toBe(options.maxDate);
		});

	});

});
