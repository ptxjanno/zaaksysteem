export default {
	in: 1 / 1.19,
	in21: 1 / 1.21,
	in6: 1 / 1.06,
	ex: 1.19,
	ex6: 1.06,
	ex21: 1.21
};
