import angular from 'angular';
import './pagination.scss';
import template from './template.html';
import controller from './controller';

export default
	angular.module('zsPagination', [
	])
	.directive('zsPagination', [ ( ) => {

		return {
			restrict: 'E',
			template,
			scope: {
				currentPage: '&',
				hasNextPage: '&',
				hasPrevPage: '&',
				onPageChange: '&',
				limit: '&',
				onLimitChange: '&',
				isLoading: '&'
			},
			bindToController: true,
			controller: [ '$scope', function ( $scope ) {
				
				controller.call(this, $scope);

			}],
			controllerAs: 'zsPagination'
		};
	}]).name;

