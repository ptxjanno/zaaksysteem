export default class ZaaksysteemException {
  constructor(type, message) {
    this.type     = type;
    this.message  = message;
    this.toString = function() {
           return `${this.type}: ${this.message}`;
    };
  }
}

