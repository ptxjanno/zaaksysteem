import angular from 'angular';
import caseStatusLabelFilterModule from './../caseStatusLabelFilter';
import zsNotificationCounterModule from './../../ui/zsNotificationCounter';
import zsIconModule from './../../ui/zsIcon';
import template from './template.html';

export default
	angular.module('zsCaseStatusIcon', [
		caseStatusLabelFilterModule,
		zsIconModule,
		zsNotificationCounterModule
	])
		.directive('zsCaseStatusIcon', [ ( ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					status: '&',
					updates: '&',
					archivalState: '&',
					destructable: '&'
				},
				bindToController: true,
				controller: [ function ( ) {

					let ctrl = this;

					ctrl.getIcon = ( ) => {

						let status = ctrl.status(),
							archivalState = ctrl.archivalState(),
							icon;

						if (archivalState === 'overdragen') {
							icon = 'overdragen';
						} else if (ctrl.destructable()
							&& status === 'resolved'
							&& archivalState === 'vernietigen'
						) {
							icon = 'deleted';
						} else {
							icon = status;
						}

						return icon;

					};

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
