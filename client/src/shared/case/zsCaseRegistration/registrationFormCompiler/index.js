import angular from 'angular';
import caseAttrTemplateCompilerModule from './../../caseAttrTemplateCompiler';
import vormRelatedSubjectModule from './vormRelatedSubject';
import vormAllocationPickerModule from './../../../zs/vorm/vormAllocationPicker';

export default
	angular.module('registrationFormCompiler', [
		caseAttrTemplateCompilerModule,
		vormRelatedSubjectModule,
		vormAllocationPickerModule
	])
		.factory('registrationFormCompiler', [ 'caseAttrTemplateCompiler', 'vormRelatedSubject', ( caseAttrTemplateCompiler, vormRelatedSubject ) => {

			let compiler = caseAttrTemplateCompiler.clone();

			compiler.registerType(
				'related-subject',
				vormRelatedSubject
			);

			return compiler;

		}])
		.name;
