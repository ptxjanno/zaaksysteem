import mapValues from 'lodash/mapValues';
import isArray from 'lodash/isArray';
import flatten from 'lodash/flatten';
import parseAttributeValues from '../parseAttributeValues';
import isEmpty from '../../../vorm/util/isEmpty';
import toIsoCalendarDate from '../../../util/date/toIsoCalendarDate';
import { stringToNumber } from '../../../util/number';

const getApiValues = ( values, fieldsByName ) => {

  return mapValues(
    parseAttributeValues(fieldsByName, values),
    ( value, key ) => {

      let field = fieldsByName[key],
        val = value;

      if (!isArray(val)) {
        val = [ val ];
      }

      val = flatten(val)
        .filter(v => !isEmpty(v));

      val = val.map(
        v => {
          switch (field.$attribute.type) {
            case 'date':
              return toIsoCalendarDate(v);

            case 'file':
              return v.reference;

            case 'numeric':
            case 'valuta':
            case 'valutaex':
            case 'valutaex21':
            case 'valutaex6':
            case 'valutain':
            case 'valutain21':
            case 'valutain6':
              return stringToNumber(v);

            case 'bag_adres':
            case 'bag_adressen':
            case 'bag_straat_adres':
            case 'bag_straat_adressen':
            case 'bag_openbareruimte':
            case 'bag_openbareruimtes':
              return v.bag_id;

            default:
              return v;
          }
        }
      );

      if (field.$attribute.limit_values > 1) {
        val = [ val ];
      }

      // Checkboxes need to wrapped in a second array
      // in order to be accepted by API.
      if (field.$attribute.type === 'checkbox') {
        val = [ val ];
      }

      return val;
    }
  );
};

export default getApiValues;
