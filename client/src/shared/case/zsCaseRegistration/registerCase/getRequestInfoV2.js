import getContactInformation from './helpersV2/getContactInformation';
import formatRequestor from './helpersV2/formatRequestor';
import getAssignment from './helpersV2/getAssignment';

const getRequestInfoV2 = (ctrl, vals, intakeShowContactInfo, allocationData, apiValues) => {
  const {
    casetypeV2,
    channelOfContact,
    requestor,
    skipRequired
  } = ctrl;

  const data = {
    assignment: getAssignment(vals.$allocation, ctrl.user, allocationData),
    case_type_version_uuid: casetypeV2.id,
    confidentiality: vals.$confidentiality,
    contact_channel: channelOfContact,
    contact_information: getContactInformation(vals, intakeShowContactInfo),
    custom_fields: apiValues,
    options: {
      allow_missing_required_fields: skipRequired,
    },
    requestor: formatRequestor(requestor),
  };

  return {
    url: '/api/v2/cm/case/create_case',
    method: 'POST',
    data,
  };
};

export default getRequestInfoV2;
