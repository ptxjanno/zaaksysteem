const getContactDetails = ({ $landline, $mobile, $email }, intakeShowContactInfo) => {
  if(!intakeShowContactInfo) {
    return;
  }

  return {
    phone_number: $landline,
    mobile_number: $mobile,
    email_address: $email
  };
};

export default getContactDetails;
