import transformAttributeValues from './transformAttributeValues';

export default ( fields, values ) => transformAttributeValues(fields, values, 'formatters');
