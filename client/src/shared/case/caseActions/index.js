import angular from 'angular';
import assign from 'lodash/fp/assign';
import capitalize from 'lodash/capitalize';
import dateformat from 'dateformat';
import filter from 'lodash/filter';
import find from 'lodash/find';
import findIndex from 'lodash/findIndex';
import first from 'lodash/first';
import flatten from 'lodash/flatten';
import get from 'lodash/get';
import identity from 'lodash/identity';
import includes from 'lodash/includes';
import keyBy from 'lodash/keyBy';
import keys from 'lodash/keys';
import mapKeys from 'lodash/mapKeys';
import mapValues from 'lodash/mapValues';
import merge from 'lodash/merge';
import pickBy from 'lodash/pickBy';
import termOptions from './../termOptions';
import defaultCapabilities from './../../auth/rightPicker/defaultCapabilities';
import getRelatableObjectTypesFromCasetype from './getRelatableObjectTypesFromCasetype';
import { getComment, transformAllocationParams }  from './allocation.js';

let dateTypeOptions =
	[
		{
			value: 'calendar_days',
			label: 'Kalenderdagen'
		},
		{
			value: 'work_days',
			label: 'Werkdagen'
		},
		{
			value: 'weeks',
			label: 'Weken'
		},
		{
			value: 'fixed_date',
			label: 'Vaste einddatum'
		}
	];

let getCaseId = ( caseObj ) => {
	return get(caseObj, 'instance.number');
};

export default ( opts ) => {

	let { caseObj, casetype, user, $state, acls } = opts;

	const caseInstance  = caseObj.instance;
	const noPermissions = get(caseInstance, 'permissions', []).length === 0;
	const isAdmin  = includes(caseInstance.permissions, 'zaak_beheer') || noPermissions;
	const isEditor = includes(caseInstance.permissions, 'zaak_edit') || noPermissions;
	const isReader = includes(caseInstance.permissions, 'zaak_read') || noPermissions;
	const isResolved = caseInstance.status !== 'resolved';
	const isReadableAndUnresolved = (isReader || isAdmin) && isResolved;
	const isManagable = (isEditor && Boolean(caseInstance.assignee)) || isAdmin;
	const isManagableAndUnresolved = isManagable && isResolved;

	const casePhases = get(casetype, 'instance.phases', []);
	const relatableObjectTypes = getRelatableObjectTypesFromCasetype(casetype);

	let today = new Date();
	today = new Date(today.getFullYear(), today.getMonth(), today.getDate());

	let stalledSince = new Date(caseInstance.stalled_since);
	stalledSince = new Date(stalledSince.getFullYear(), stalledSince.getMonth(), stalledSince.getDate());

	const tomorrow = new Date(new Date(today.getFullYear(), today.getMonth(), today.getDate() + 1).getTime() - 1);

	return [
		{
			name: 'toewijzing',
			label: 'Toewijzing wijzigen',
			visible: isReadableAndUnresolved,
			fields: [
				{
					name: 'allocation_type',
					label: 'Type toewijzing',
					template: 'radio',
					data: {
						options: [
							{
								value: 'org-unit',
								label: 'Rol of afdeling'
							},
							{
								value: 'assignee',
								label: 'Specifieke behandelaar'
							},
							{
								value: 'assign_to_self',
								label: 'Zelf in behandeling nemen'
							}
						]
					},
					required: true,
					defaults: 'org-unit'
				},
				{
					name: 'assignee',
					label: 'Nieuwe behandelaar',
					template: 'object-suggest',
					data: {
						objectType: 'medewerker'
					},
					required: true,
					when: [ '$values', ( values ) => values.allocation_type === 'assignee' ]
				},
				{
					name: 'org-unit',
					label: 'Rol of afdeling',
					template: 'org-unit',
					required: true,
					data: {
						depth: 1
					},
					when: [ '$values', ( values ) => values.allocation_type === 'org-unit' ]
				},
				{
					name: 'change_department',
					label: 'Ook afdeling wijzigen',
					template: 'checkbox',
					when: [ '$values', ( values ) => values.allocation_type !== 'org-unit' ],
					defaults: true
				},
				{
					name: 'send_email',
					label: 'Verstuur e-mail',
					template: 'checkbox',
					when: [ '$values', ( values ) => values.allocation_type === 'assignee' ],
					defaults: true
				},
				{
					name: 'comment',
					label: 'Opmerking',
					template: 'text',
					defaults: '',
				}
			],
			mutate: ( values ) => {

				let params = {
						caseId: get(caseInstance, 'number'),
						allocationType: values.allocation_type,
					};

				params = merge(params, transformAllocationParams(values, user));
				params = merge(params, getComment(values));

				return {
					type: 'case/allocate',
					data: params
				};
			},
			options: ( values ) => {

				let redirect =
						pickBy({
							case: (values.allocation_type === 'assign_to_self' && $state.current.name.indexOf('home') === 0),
							home: (values.allocation_type !== 'assign_to_self' && $state.current.name.indexOf('case.') === 0)
						}, identity);

				return {
					willRedirect: keys(redirect).length > 0
				};

			}
		},
		{
			name: 'opschorten',
			label: 'Opschorten',
			fields: [
				{
					name: 'reason',
					label: 'Reden opschorting',
					template: 'text',
					required: true,
					defaults: ''
				},
				{
					name: 'term',
					label: 'Termijn',
					template: 'form',
					data: {
						fields: [
							{
								name: 'term_type',
								template: 'radio',
								data: {
									options: [
										{
											value: 'indeterminate',
											label: 'Onbepaald'
										},
										{
											value: 'determinate',
											label: 'Bepaald, namelijk'
										}
									]
								},
								required: true
							},
							{
								name: 'term_spec',
								template: 'form',
								data: {
									fields: [
										{
											name: 'term_amount_type',
											template: 'select',
											data: {
												options: dateTypeOptions
											},
											required: true
										},
										{
											name: 'term_amount',
											template: 'number',
											required: true,
											when: [ '$values', ( values ) => get(values, 'term_amount_type') && get(values, 'term_amount_type') !== 'fixed_date' ]
										},
										{
											name: 'term_date',
											template: 'date',
											required: true,
											when: [ '$values', ( values ) => get(values, 'term_amount_type') === 'fixed_date' ]
										}
									]
								},
								when: [ '$values', ( values ) => get(values, 'term_type') === 'determinate' ],
								required: true
							}
						]
					},
					defaults: {
						term_type: 'determinate',
						term_spec: {
							term_amount_type: 'weeks',
							term_amount: 6,
							term_date: null
						}
					}
				}
			],
			visible: isManagableAndUnresolved && get(caseObj, 'instance.status') !== 'stalled',
			mutate: (values) => {
				const suspension_term_type = 'indefinite';
				let settings = {
					suspension_reason: values.reason,
					suspension_term_type,
					case_uuid: caseObj.reference,
				};

				if (get(values, 'term.term_type') === 'determinate') {
					let amountType = get(values, 'term.term_spec.term_amount_type');

					merge(settings, {
						suspension_term_type: amountType,
						suspension_term_value: amountType === 'fixed_date'
							? dateformat(
								get(values, 'term.term_spec.term_date'),
								'yyyy-mm-dd'
							)
							: get(values, 'term.term_spec.term_amount')
					});
				}

				let data = {
					settings,
					type: 'pause',
				};

				return {
					type: 'case/set_settings',
					data
				};
			}
		},
		{
			name: 'hervatten',
			label: 'Hervatten',
			fields: [
				{
					name: 'reason',
					label: 'Reden hervatten',
					template: 'text',
					required: true
				},
				{
					name: 'start_date',
					label: 'Ingangsdatum opschorting',
					template: 'date',
					required: true,
					defaults: stalledSince,
					data: {
						max: tomorrow
					}
				},
				{
					name: 'end_date',
					label: 'Einddatum opschorting',
					template: 'date',
					required: true,
					defaults: today,
					data: {
						max: tomorrow,
						min: [ '$values', ( values ) => values.start_date ]
					}
				}
			],
			visible: isManagableAndUnresolved && get(caseObj, 'instance.status') === 'stalled',
			mutate: (values) => {

				let data = {
					settings: {
						case_uuid: caseObj.reference,
						resume_reason: values.reason,
						stalled_since_date: dateformat(values.start_date, 'yyyy-mm-dd'),
						stalled_until_date: dateformat(values.end_date, 'yyyy-mm-dd'),
					},
					type: 'resume'
				};

				return {
					type: 'case/set_settings',
					data
				};

			},
		},
		{
			name: 'afhandelen',
			label: 'Vroegtijdig afhandelen',
			visible: isManagableAndUnresolved,
			fields: [
				{
					name: 'reason',
					label: 'Reden afhandeling',
					template: 'text',
					required: true
				},
				{
					name: 'result_id',
					label: 'Resultaat',
					template: 'radio',
					data: {
						options: get(casetype, 'instance.results', [])
							.map(result => ({
									value: result.resultaat_id,
									label: capitalize(result.label || result.type)
								})
							)
					},
					required: true
				}
			],
			mutate: ( values ) => {

				let data = {
					caseId: get(caseObj, 'instance.number'),
					reason: values.reason,
					result_id: values.result_id
				};

				return {
					type: 'case/resolve_prematurely',
					data
				};

			}
		},
		{
			name: 'termijn',
			label: 'Termijn wijzigen',
			visible: isManagableAndUnresolved,
			fields: [
				{
					name: 'reason',
					label: 'Reden wijzigen termijn',
					template: 'text',
					required: true
				},
				{
					name: 'type_term_change',
					label: 'Type wijziging',
					template: 'radio',
					data: {
						options: [
							{
								value: 'fixedDate',
								label: 'Verlengen'
							},
							{
								value: 'changeTerm',
								label: 'Behandeltermijn wijzigen'
							}
						]
					},
					required: true
				},
				{
					name: 'new_term',
					label: 'Nieuwe termijn',
					template: 'form',
					data: {
						fields: [
							{
								name: 'term_amount',
								template: 'number',
								required: true
							},
							{
								name: 'term_amount_type',
								template: 'select',
								data: {
									options: dateTypeOptions.filter(option => option.value !== 'fixed_date')
								},
								required: true
							}
						]
					},
					when: [ '$values', ( values ) => values.type_term_change === 'changeTerm' ],
					defaults: {
						term_amount: 1,
						term_amount_type: 'calendar_days'
					}
				},
				{
					name: 'new_term_date',
					label: 'Nieuwe streefafhandeldatum',
					template: 'date',
					required: true,
					when: [ '$values', ( values ) => values.type_term_change === 'fixedDate' ]
				}
			],
			mutate: ( values ) => {

				let data = {
					caseId: get(caseObj, 'instance.number'),
					reason: values.reason,
					prolongationType: values.type_term_change
				};

				if (values.type_term_change === 'fixedDate') {
					data.prolongationDate = values.new_term_date;
				} else {
					data.termType = values.new_term.term_amount_type;
					data.termAmount = values.new_term.term_amount;
				}

				return {
					type: 'case/prolong',
					data
				};
			}
		},
		{
			name: 'relateren',
			label: 'Zaak relateren',
			visible: isManagable,
			fields: [
				{
					name: 'case_to_relate',
					label: 'Te relateren zaak',
					template: 'object-suggest',
					data: {
						objectType: 'case'
					},
					required: true
				}
			],
			mutate: ( values ) => {

				let data = {
					caseId: get(caseObj, 'instance.number'),
					relatedCaseId: values.case_to_relate.data.id
				};

				return {
					type: 'case/relate',
					data
				};

			}
		},
		{
			name: 'kopieren',
			label: 'Zaak kopiëren',
			type: 'confirm',
			visible: isManagable,
			confirm: {
				label: 'Weet u zeker dat u deze zaak wilt kopiëren?',
				verb: 'Kopieer zaak'
			},
			mutate: ( ) => {

				let data = {
					caseId: get(caseObj, 'instance.number')
				};

				return {
					type: 'case/copy',
					data
				};

			}
		},
		{
			name: 'object-relateren',
			label: 'Object relateren',
			messages: [ '$values', ( vals ) => {

				return vals.copy_attributes ?
					[
						{
							name: 'attribute-overwrite',
							label: 'Bij het uitvoeren van deze actie kunnen gegevens uit de zaak worden overschreven.',
							icon: 'alert-outline',
							classes: {
								warning: true
							}
						}
					] : null;
			}],
			fields: [
				{
					name: 'object_type',
					label: 'Type object',
					template: 'select',
					data: {
						options: relatableObjectTypes
					},
					required: true,
					defaults: get(first(relatableObjectTypes), 'value')
				},
				{
					name: 'related_object',
					label: 'Te relateren object',
					template: 'object-suggest',
					data: {
						objectType: [ '$values', vals => vals.object_type ]
					},
					required: true
				},
				{
					name: 'copy_attributes',
					label: 'Kopieer waarden object naar zaak',
					template: 'checkbox',
					data: {
						checkboxLabel: 'Ja'
					},
					when: [ '$values', ( vals ) => vals.object_type ]
				}
			],
			visible: relatableObjectTypes.length && isManagableAndUnresolved,
			mutate: ( values ) => {

				let data = {
					caseReference: get(caseObj, 'reference'),
					relatedObjectReference: values.related_object.data.id,
					copyAttributeValues: values.copy_attributes
				};

				return {
					type: 'case/object_relate',
					data
				};

			}
		},
		{
			name: 'aanvrager',
			label: 'Aanvrager wijzigen',
			fields: [
				{
					name: 'type',
					label: 'Type',
					template: 'radio',
					data: {
						options: [
							{
								value: 'natuurlijk_persoon',
								label: 'Burger'
							},
							{
								value: 'bedrijf',
								label: 'Organisatie'
							},
							{
								value: 'medewerker',
								label: 'Medewerker'
							}
						],
						readOnly: true
					},
					required: true,
					defaults: get(caseObj, 'instance.requestor.instance.subject_type')
				},
				{
					name: 'requestor',
					label: 'Aanvrager',
					template: 'object-suggest',
					data: {
						objectType: [ '$values', ( values ) => values.type ]
					},
					required: true,
					defaults: caseObj.instance.requestor ?
						{
							label: caseObj.instance.requestor.instance.name,
							data: {
								id: get(caseObj, 'instance.requestor.instance.id') ?
									caseObj.instance.requestor.instance.id.match(/\d+$/)[0]
									: caseObj.instance.requestor.instance.old_subject_identifier.match(/\d+$/)[0]
							}
						}
						: null
				}
			],
			processChange: ( name, value, values ) => {

				let vals = values.merge({ [name]: value });

				if (name === 'type') {

					vals = vals.merge({ requestor: null });

				}

				return vals;

			},
			mutate: ( values ) => {

				let data = {
						caseId: getCaseId(caseObj),
						settings: {
							aanvrager_id: `betrokkene-${values.type}-${values.requestor.data.id}`
						}
					};

				return {
					type: 'case/set_setting',
					data
				};

			},
			context: 'admin'
		},
		{
			name: 'coordinator',
			label: 'Coordinator wijzigen',
			fields: [
				{
					name: 'coordinator',
					label: 'Coordinator',
					template: 'object-suggest',
					data: {
						objectType: 'medewerker'
					},
					required: true
				}
			],
			context: 'admin',
			mutate: ( values ) => {

				let data = {
					caseId: caseObj.instance.number,
					settings: {
						coordinator_uuid: values.coordinator.data.uuid,
						case_uuid: caseObj.reference,
					},
					type: 'change_case_coordinator'
				};

				return {
					type: 'case/set_settings',
					data
				};

			}
		},
		{
			name: 'afdeling',
			label: 'Afdeling en rol wijzigen',
			fields: [
				{
					name: 'org_unit',
					label: '',
					template: 'org-unit',
					required: true,
					data: {
						depth: 1
					},
					defaults: caseObj.instance.route_ou === 1 ?
						null
						: {
							unit: String(caseObj.instance.route_ou),
							role: String(caseObj.instance.route_role)
						}
				}
			],
			mutate: ( values ) => {

				let data = {
					caseId: getCaseId(caseObj),
					settings: {
						ou_id: Number(values.org_unit.unit),
						role_id: Number(values.org_unit.role),
						change_only_route_fields: 1,
						wijzig_route_force: 1
					}
				};

				return {
					type: 'case/set_setting',
					data
				};

			},
			context: 'admin'
		},
		{
			name: 'registratiedatum',
			label: 'Registratiedatum wijzigen',
			fields: [
				{
					name: 'registratiedatum',
					label: 'Registratiedatum',
					template: 'date',
					required: true,
					defaults: caseObj.instance.date_of_registration ?
						new Date(caseObj.instance.date_of_registration)
						: null
				}
			],
			mutate: ( values ) => {

				let data = {
					settings: {
						target_date: dateformat(values.registratiedatum, 'yyyy-mm-dd'),
						case_uuid: caseObj.reference,
					},
					type: 'set_registration_date'
				};

				return {
					type: 'case/set_settings',
					data
				};

			},
			context: 'admin'
		},
		{
			name: 'streefafhandeldatum',
			label: 'Streefafhandeldatum wijzigen',
			fields: [
				{
					name: 'streefafhandeldatum',
					template: 'date',
					required: true,
					defaults: caseObj.instance.date_target ?
						new Date(caseObj.instance.date_target)
						: null
				}
			],
			mutate: ( values ) => {

				let data = {
					settings: {
						target_date: dateformat(values.streefafhandeldatum, 'yyyy-mm-dd'),
						case_uuid: caseObj.reference,
					},
					type: 'set_target_completion_date',
				};

				return {
					type: 'case/set_settings',
					data
				};

			},
			context: 'admin'
		},
		{
			name: 'afhandeldatum',
			label: 'Afhandeldatum wijzigen',
			fields: [
				{
					name: 'afhandeldatum',
					template: 'date',
					required: true,
					defaults: caseObj.instance.date_of_completion ?
						new Date(caseObj.instance.date__of_completion)
						: null
				}
			],
			mutate: ( values ) => {

				let data = {
					settings: {
						target_date: dateformat(values.afhandeldatum, 'yyyy-mm-dd'),
						case_uuid: caseObj.reference,
					},
					type: 'set_completion_date'
				};

				return {
					type: 'case/set_settings',
					data
				};

			},
			context: 'admin'
		},
		{
			name: 'vernietigingsdatum',
			label: 'Vernietigingsdatum wijzigen',
			fields: [
				{
					name: 'vernietigingsdatum',
					template: 'form',
					data: {
						fields: [
							{
								name: 'change',
								template: 'radio',
								label: '',
								data: {
									options: [
										{
											value: 'change',
											label: 'Nieuwe vernietigingsdatum'
										}
									]
								}
							},
							{
								name: 'date',
								template: 'date',
								label: ''
							}
						]
					},
					defaults: {
						date:
							caseObj.instance.date_destruction ?
							new Date(caseObj.instance.date_destruction)
							: null,
						change: false
					},
					required: true,
					valid: [ '$values', ( vals ) => !!( (get(vals, 'vernietigingsdatum.change') && get(vals, 'vernietigingsdatum.date')) || get(vals, 'vernietigingstermijn.change') && get(vals, 'vernietigingstermijn.term'))]
				},
				{
					name: 'vernietigingstermijn',
					template: 'form',
					data: {
						fields: [
							{
								name: 'change',
								template: 'radio',
								data: {
									options: [
										{
											value: 'change',
											label: 'Herberekenen'
										}
									]
								},
								required: false
							},
							{
								name: 'term',
								template: 'select',
								label: '',
								data: {
									options: termOptions
								},
								required: [ '$values', ( vals ) => {
									return get(vals, 'change') === 'change';
								}]
							}
						]
					},
					defaults: {
						term: null,
						change: false
					},
					required: true,
					valid: [ '$values', ( vals ) => !!( (get(vals, 'vernietigingsdatum.change') && get(vals, 'vernietigingsdatum.date')) || get(vals, 'vernietigingstermijn.change') && get(vals, 'vernietigingstermijn.term'))]
				},
				{
					name: 'vernietigingsdatum_reden',
					label: 'Reden van wijziging',
					template: 'select',
					required: true,
					data: {
						options: [
							'In belang van de aanvrager',
							'Uniek of bijzonder karakter voor de organisatie',
							'Bijzondere tijdsomstandigheid of gebeurtenis',
							'Beeldbepalend karakter',
							'Samenvatting van gegevens',
							'Betrokkene(n) is van bijzondere betekenis geweest',
							'Vervanging van stukken bij calamiteit',
							'Aanleiding van algemene regelgeving',
							'Verstoring van logische samenhang'
						]
							.map(reason => {
								return {
									value: reason,
									label: reason
								};
							})
					},
					defaults: 'In belang van de aanvrager'
				}
			],
			processChange: ( name, value, values ) => {

				let vals = values;

				if (name === 'vernietigingsdatum') {
					vals = values.merge({
						vernietigingsdatum: {
							change: 'change',
							date: value.date
						},
						vernietigingstermijn: {
							change: null,
							term: null
						}
					}, { deep: true });
				}


				if (name === 'vernietigingstermijn') {
					vals = values.merge({
						vernietigingstermijn: {
							change: 'change',
							term: value.term
						},
						vernietigingsdatum: {
							change: null,
							date: values.vernietigingsdatum.date
						}
					}, { deep: true });
				}

				if (name === 'vernietigingsdatum_reden') {
					vals = values.merge({
						vernietigingsdatum_reden: value
					});
				}

				return vals;

			},
			valid: ( values ) => {
				return values.vernietigingsdatum.change || values.vernietigingstermijn.change;
			},
			mutate: ( values ) => {

				let settings = {
						vernietigingsdatum_reden: values.vernietigingsdatum_reden
					},
					data;

				if (values.vernietigingsdatum.change) {
					settings.vernietigingsdatum_type = 'termijn';
					settings.vernietigingsdatum = dateformat(values.vernietigingsdatum.date, 'dd-mm-yyyy');
				} else {
					settings.vernietigingsdatum_type = 'bewaren';
					settings.vernietigingsdatum_recalculate = values.vernietigingstermijn.term;
				}

				data = {
					caseId: getCaseId(caseObj),
					settings
				};

				return {
					type: 'case/set_setting',
					data
				};

			},
			context: 'admin'
		},
		{
			name: 'kenmerken',
			label: 'Kenmerken wijzigen',
			fields: [
				{
					name: 'attributes',
					template: 'case-admin-attribute-list',
					data: {
						available:
							flatten(
								get(casetype, 'instance.phases', []).map(phase => phase.fields)
							),
						defaults: caseObj.instance.attributes
					},
					required: true
				}
			],
			mutate: ( values ) => {

				let fields =
						keyBy(
							flatten(
								get(casetype, 'instance.phases', []).map(phase => phase.fields)
							),
							'magic_string'
						),
					mappedValues =
						mapValues(
							values.attributes,
							( value ) => {

								let type,
									valueProcessed;

								type =
									get(
										find(fields, ( field ) => values.attributes[field.magic_string] === value),
										'type'
									);

								switch (type) {

									case 'bag_adres':
									case 'bag_openbareruimte':
									case 'bag_straat_adres':
									valueProcessed = value.bag_id;
									break;

									case 'bag_adressen':
									case 'bag_openbareruimtes':
									case 'bag_straat_adressen':
									valueProcessed = value.map( address => {
										return address.bag_id;
									});
									break;

									case 'checkbox':
									valueProcessed = Object.keys(value);
									break;

									default:
									valueProcessed = value;
									break;

								}

								return valueProcessed;

							}
						),
					settings =
						mapKeys(
							mappedValues,
							( value, key ) => {

								let field = fields[key];

								return `bibliotheek_kenmerk_${field.catalogue_id}`;

							}
						);

				return {
					type: 'case/set_setting',
					data: {
						caseId: getCaseId(caseObj),
						settings
					}
				};

			},
			context: 'admin'
		},
		{
			name: 'fase',
			label: 'Fase wijzigen',
			fields: [
				{
					name: 'phase',
					template: 'select',
					data: {
						options:
							casePhases.slice(
								1,
								caseObj.instance.phase ?
									findIndex(casePhases, { name: caseObj.instance.phase }) + 1
									: casePhases.length
							)
								.map(phase => {
									return {
										value: phase.seq,
										label: phase.name
									};
								}),
						notSelectedLabel: caseObj.instance.phase ?
							null
							: '[Afgehandeld]'
					},
					defaults: caseObj.instance.phase ?
						findIndex(casePhases, { name: caseObj.instance.phase }) + 1
						: null,
					required: true
				}
			],
			mutate: ( values ) => {

				let data = {
						caseId: getCaseId(caseObj),
						settings: {
							// milestone is zero-indexed
							milestone: values.phase - 1
						}
					};

				return {
					type: 'case/set_setting',
					data
				};

			},
			context: 'admin'
		},
		{
			name: 'status',
			label: 'Status wijzigen',
			fields: [
				{
					name: 'status',
					template: 'select',
					required: true,
					data: {
						options: [
							{
								value: 'new',
								label: 'Nieuw'
							},
							{
								value: 'open',
								label: 'In behandeling'
							},
							{
								value: 'stalled',
								label: 'Opgeschort'
							},
							{
								value: 'resolved',
								label: 'Afgehandeld'
							}
						]
					},
					defaults: caseObj.instance.status
				}
			],
			mutate: ( values ) => {

				let data = {
					caseId: getCaseId(caseObj),
					settings: values
				};

				return {
					type: 'case/manage_action',
					data
				};

			},
			context: 'admin'
		},
		{
			name: 'rechten',
			label: 'Rechten wijzigen',
			fields: [
				{
					name: 'acls',
					template: {
						inherits: 'right',
						control: ( element ) => {

							let el = element.clone();

							el.attr('data-disabled', 'vm.disabled()||delegate.value.readOnly');

							return angular.element(`
								<div class="case-action-right-picker-wrapper list-item-block">
									${el[0].outerHTML}
									<button type="button"
										ng-click="vm.clearDelegate(delegate)"
										ng-show="!vm.disabled()&&!delegate.value.readOnly"
									>
										<zs-icon icon-type="close"></zs-icon>
									</button>
								</div>
							`);

						}
					},
					limit: -1,
					addLabel: 'Recht toevoegen',
					defaults:
						filter(acls, identity)
							.map(acl => {

								let spl = acl.instance.entity_id.split('|');

								return {
									capabilities: defaultCapabilities().map(
										cap => assign(cap, { selected: includes(acl.instance.capabilities, cap.name) })
									),
									position: {
										unit: spl[0],
										role: spl[1]
									},
									readOnly: !!acl.instance.read_only
								};
							})
				},
				{
					name: 'cascadeTo',
					label: 'Geldt ook voor:',
					template: 'checkbox-list',
					data: {
						options: [
							{
								label: 'Gerelateerde zaken',
								value: 'related'
							},
							{
								label: 'Deelzaken',
								value: 'partial',
							},
							{
								label: 'Vervolgzaken',
								value: 'continuation'
							}
						]
					},
					classes: {
						'acls-cascade-to': true
					}
				}
			],
			mutate: ( values ) => {
				return {
					type: 'case/update_acls',
					data: {
						caseReference: caseObj.reference,
						acls: filter(values.acls, acl => !acl.readOnly),
						cascadeTo: values.cascadeTo
					}
				};

			},
			visible: isAdmin,
			context: 'admin'
		},
		{
			name: 'resultaat',
			label: 'Resultaat wijzigen',
			fields: [
				{
					name: 'result',
					label: 'Resultaat',
					template: 'radio',
					data: {
						options: get(casetype, 'instance.results', [])
							.map( ( result ) => {
								return {
									// we need id, not type for set_setting
									value: result.resultaat_id,
									label: capitalize(result.label || result.type)
								};
							})
					},
					required: true
				}
			],
			mutate: ( values ) => {

				let data = {
						caseId: getCaseId(caseObj),
						settings: {
							resultaat: values.result
						}
					};

				return {
					type: 'case/set_setting',
					data
				};

			},
			visible: isAdmin && caseObj.instance.status === 'resolved',
			context: 'admin'
		},
		{
			name: 'zaaktype',
			label: 'Zaaktype wijzigen',
			fields: [
				{
					name: 'casetype',
					label: 'Zaaktype',
					template: 'object-suggest',
					data: {
						objectType: 'casetype'
					},
					required: true
				}
			],
			mutate: ( values ) => {

				let data =
					{
						caseId: getCaseId(caseObj),
						settings: {
							zaaktype_id: get(values, 'casetype.data.id')
						}
					};

				return {
					type: 'case/set_setting',
					data
				};

			},
			options: {
				reloadRoute: true
			},
			context: 'admin'
		},
		{
			name: 'payment_status',
			label: 'Betaalstatus wijzigen',
			fields: [
				{
					name: 'payment_status',
					template: 'select',
					required: true,
					data: {
						options: [
							{
								value: 'success',
								label: 'Geslaagd'
							},
							{
								value: 'failed',
								label: 'Niet geslaagd'
							},
							{
								value: 'pending',
								label: 'Wachten op bevestiging'
							},
							{
								value: 'offline',
								label: 'Later betalen'
							}
						]
					},
					defaults: get(caseObj.instance.payment_status, 'original')
				}
			],
			mutate: ( values ) => {

				let data = {
					caseReference: caseObj.reference,
					paymentStatus: values.payment_status
				};

				return {
					type: 'case/set_payment_status',
					data
				};

			},
			context: 'admin'
		}
	]
		.filter(action => {

			let visible;

			if (action.visible !== undefined) {
				visible = action.visible;
			} else if (action.context === 'admin') {
				visible = isAdmin;
			} else {
				visible = true;
			}

			return visible;
		});
};
