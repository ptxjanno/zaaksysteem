import angular from 'angular';
import rightPickerModule from './../../../auth/rightPicker';
import rightPickerDisplayModule from './../../../auth/rightPicker/rightPickerDisplay';
import template from './template.html';
import assign from 'lodash/fp/assign';
import vormTemplateServiceModule from './../../../vorm/vormTemplateService';

export default
	angular.module('vormRightPicker', [
		rightPickerModule,
		vormTemplateServiceModule,
		rightPickerDisplayModule
	])
		.component('vormRightPicker', {
			template,
			bindings: {
				disabled: '<'
			},
			require: {
				ngModel: 'ngModel'
			},
			controller: [ function ( ) {

				let ctrl = this;

				ctrl.handleRightChange = ( right ) => {
					ctrl.ngModel.$setViewValue(assign({}, right));
				};

			}]
		})
		.run([ 'vormTemplateService', vormTemplateService => {

			vormTemplateService.registerType('right', {
				control: angular.element(`
					<vorm-right-picker
						ng-model
						data-disabled="vm.disabled()"
					>
					</vorm-right-picker>
				`)
			});

		}])
		.name;
