import isArray from 'lodash/isArray';

export default function evaluateFormula( formula, values ) {

	let val;

	const expression = formula.replace(/([a-zA-Z]+(?:[a-zA-Z0-9_.]+))/g, ( match, key ) => {
		let value,
			name = key;

		if (name.indexOf('.') === -1) {
			name = `attribute.${name}`;
		}

		value = values[name];

		if (isArray(value)) {
			value = value[0];
		}

		value = String(value).replace(',', '.');

		return (isNaN(value) || value === undefined || value === null || value === '') ?
			0
			: Number(value);
	});

	try {
		val = new Function(`return ${expression}`)();

		if (isNaN(val) || val === Number.POSITIVE_INFINITY || val === Number.NEGATIVE_INFINITY) {
			val = null;
		} else {
			val = Math.round(val * 100) / 100;
		}
	} catch (e) {
		val = null;
	}

	return val;

}
