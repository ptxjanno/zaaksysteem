import evaluateFormula from '.';

describe('evaluateFormula', () => {
	test('should return the result of the formula', () => {
		expect(evaluateFormula('foo * 5', {
			'attribute.foo': 3
		})).toBe(15);
		expect(evaluateFormula('foo * bar', {
			'attribute.foo': 3,
			'attribute.bar': 4
		})).toBe(12);
		expect(evaluateFormula('foo / bar', {
			'attribute.foo': 4,
			'attribute.bar': 2
		})).toBe(2);
		expect(evaluateFormula('foo - bar', {
			'attribute.foo': 4,
			'attribute.bar': 1
		})).toBe(3);
		expect(evaluateFormula('bar - foo', {
			'attribute.foo': 4,
			'attribute.bar': 1
		})).toBe(-3);
	});

	test('should normalize non-numeric values to 0', () => {
		expect(evaluateFormula('bar - foo', {
			'attribute.foo': 4,
			'attribute.bar': NaN
		})).toBe(-4);
		expect(evaluateFormula('bar - foo + blah', {
			'attribute.foo': 2,
			'attribute.bar': NaN
		})).toBe(-2);
	});

	test('should accept a numeric string wtesth decimal comma in the data', () => {
		expect(evaluateFormula('foo * 2', {
			'attribute.foo': '4,5'
		})).toBe(9);
	});

	test('should normalize infintesty to null', () => {
		expect(evaluateFormula('3 / 0')).toBe(null);
		expect(evaluateFormula('-3 / 0')).toBe(null);
	});

	test('should round off to two decimals', () => {
		expect(evaluateFormula('4 / 3')).toBe(1.33);
	});

	test('should catch evaluation errors and normalize to null', () => {
		expect(evaluateFormula('3-2.2.2')).toBe(null);
	});
});
