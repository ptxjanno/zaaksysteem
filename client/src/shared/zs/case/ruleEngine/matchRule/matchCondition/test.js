import matchCondition from '.';

describe('matchCondition', () => {
	let attrName = 'foo';

	describe('with a validates_true property', () => {
		test('should match accordingly', () => {
			expect(matchCondition({
				validates_true: true
			}, {})).toBe(true);

			expect(matchCondition({
				validates_true: false
			}, {})).toBe(false);
		});
	});

	describe('without a validates_true property', () => {
		describe('and the attribute value is not an array', () => {
			let attrValue = 'foo';

			test('should match when at least one of the condition values is equal to the attribute value', () => {
				expect(matchCondition({
					attribute_name: attrName,
					values: ['foo', 'bar']
				}, { [attrName]: attrValue })).toBe(true);
			});

			test('should not match when none of the condition values are equal to the attribute value', () => {
				expect(matchCondition({
					attribute_name: attrName,
					values: ['bar']
				}, { [attrName]: attrValue })).toBe(false);
			});
		});

		describe('and the attribute value is an array', () => {
			let attrValue = ['foo', 'bar'];

			test('should match when the attribute value contains at least one of the condition values', () => {
				expect(matchCondition({
					attribute_name: attrName,
					values: ['bar']
				}, { [attrName]: attrValue })).toBe(true);

				expect(matchCondition({
					attribute_name: attrName,
					values: ['bar', 'baz']
				}, { [attrName]: attrValue })).toBe(true);
			});

			test('should not match when the attribute value contains none of the condition values', () => {
				expect(matchCondition({
					attribute_name: attrName,
					values: ['baz']
				}, { [attrName]: attrValue })).toBe(false);
			});
		});
	});
});
