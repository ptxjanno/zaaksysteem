const currencyExpression = /^-?\d+(,\d{1,2})?$/;

/**
 * @param {string} value
 * @return {boolean}
 */
export const isValidCurrency = value =>
	((typeof value === 'string')
	&& currencyExpression.test(value));
