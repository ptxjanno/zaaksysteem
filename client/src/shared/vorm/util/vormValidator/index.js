import angular from 'angular';
import keyBy from 'lodash/keyBy';
import mapValues from 'lodash/mapValues';
import isArray from 'lodash/isArray';
import every from 'lodash/every';
import compact from 'lodash/compact';
import get from 'lodash/get';
import defaults from 'lodash/defaults';
import defaultMessages from './messages';
import isEmpty from './../isEmpty';

export default angular
	.module('vormValidator', [])
	.factory('vormValidator', ['vormInvoke', ( vormInvoke ) => {
		return ( fields, values, messages = null, givenLocals, validator ) => {
			const msgs = defaults({}, defaultMessages, messages);
			const locals = givenLocals || { $values: values };
			const result = mapValues(
				keyBy(
					fields.filter(field => {
						return field.when === undefined
							|| field.when === null
							|| !!vormInvoke(field.when, locals);
					}),
					'name'
				),
				( field, key ) => {
					let value = values[key],
						isRequired = !!vormInvoke(field.required, locals),
						validation,
						type = typeof field.template === 'string' ? field.template : get(field.template.inherits);

					if (!isArray(value)) {
						value = [value];
					} else if (!value || value.length === 0) {
						value = [null];
					}

					if (field.valid !== undefined) {
						validation = vormInvoke(field.valid, locals);
					} else {
						validation = value.map(
							( val, index ) => {
								let empty = isEmpty(val),
									valueValidation = {};

								if (empty && isRequired && index === 0) {
									const msgKey = (type === 'date') ? 'requiredOrInvalid' : 'required';

									valueValidation.required = msgs[msgKey] || true;
								}

								switch (type) {

								case 'date':
									if (!empty) {
										let minDate = vormInvoke(get(field.data, 'min'), locals),
											maxDate = vormInvoke(get(field.data, 'max'), locals),
											time = val.getTime();

										if (minDate) {
											let date = new Date(minDate);

											date = new Date(date.getFullYear(), date.getMonth(), date.getDate());

											if (date.getTime() > time) {
												valueValidation.minDate = vormInvoke(msgs.minDate, { $minDate: new Date(minDate) }) || true;
											}
										}

										if (maxDate) {
											let date = new Date(maxDate);

											date = new Date(date.getFullYear(), date.getMonth(), date.getDate() + 1, 0, 0, -1);

											if (date.getTime() < time) {
												valueValidation.maxDate = vormInvoke(msgs.maxDate, { $maxDate: new Date(maxDate) }) || true;
											}
										}
									}

									break;
								}

								if (validator) {
									valueValidation = validator(field, val, valueValidation);
								}

								return Object.keys(valueValidation).length > 0 ? valueValidation : null;
							}
						);
					}

					return validation;
				}
			);

			return {
				validations: result,
				valid: every(result, validation => {
					return isArray(validation) ?
						compact(validation).length === 0
						: !!validation;
				})
			};
		};
	}])
	.name;
