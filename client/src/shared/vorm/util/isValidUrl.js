export default ( value ) => {
  // ZS-14308: Built a regex to match the multitude of valid URIs that can be entered.
  // We attempted to catch a lot of patterns and succeeded with the use cases we could
  // think of (see ticket for a list of things that match and dont match), but if more
  // patterns are required, its probably best to use a dedicated URL match library such
  // as Url-Regex (https://www.npmjs.com/package/url-regex)
  let regex = /^(?:file:\/\/.*|[a-zA-Z]:\\.*|(?:(?:[a-zA-Z][a-zA-Z0-9+-.]*):\/\/)?(?:\w+@)?(?:[\w-]+)(?:\.[\w-]+)*(?::[0-9]+)?(?:\/.*)?)$/;

  return regex.test(value);
};
