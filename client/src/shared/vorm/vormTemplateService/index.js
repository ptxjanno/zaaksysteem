import angular from 'angular';
import zsIconModule from './../../ui/zsIcon';
import compiler from './compiler';
 
export default
	angular.module('vorm.vormTemplateService', [
		zsIconModule
	])
		.factory('vormTemplateService', [ '$interpolate', '$compile', ( $interpolate, $compile ) => {
			
			return compiler($compile, $interpolate);
			
		}])
		.name;
