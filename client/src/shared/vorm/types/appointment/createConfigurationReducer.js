import assign from 'lodash/assign';

const createConfigurationReducer = ( composedReducer, scope, configuration ) =>
	composedReducer({ scope }, configuration)
		.reduce(config => assign({}, configuration, {
			isCorrectlyConfigured:
				Boolean(
					config.appointmentInterfaceUuid
					&& config.productId
					&& config.locationId
					&& config.requestor
				)
		}));

export default createConfigurationReducer;
