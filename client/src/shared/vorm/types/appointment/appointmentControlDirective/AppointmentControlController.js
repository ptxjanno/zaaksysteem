import assign from 'lodash/assign';
import first from 'lodash/head';
import get from 'lodash/get';
import seamlessImmutable from 'seamless-immutable';
import createConfigurationReducer from '../createConfigurationReducer';
import {
	APPOINTMENT_CREATE_ERROR,
	APPOINTMENT_CREATE_THROW,
	APPOINTMENT_CREATE_PENDING
} from '../messages';

export default class AppointmentController {

	constructor( scope, $http, resource, composedReducer, dateFilter, snackbarService ) {
		const ctrl = this;
		let interacted,
			ngModel,
			selectedDate,
			selectedTime;

		ctrl.link = ( ...controllers ) => {
			[ngModel] = controllers;
		};

		const providerConfigurationReducer = createConfigurationReducer(composedReducer, scope, ctrl.provider());

		const getDateResourceOptions = () => {
			if (interacted) {
				const {
					appointmentInterfaceUuid,
					locationId,
					productId
				} = providerConfigurationReducer.data();

				return {
					url: `/api/v1/sysin/interface/${appointmentInterfaceUuid}/trigger/get_dates?product_id=${productId}&location_id=${locationId}`
				};
			}
		};

		const dateResource = resource(getDateResourceOptions, {
			scope,
			cache: {
				disabled: true
			}
		})
			.reduce(( requestOptions, data ) => {
				return data ? get(first(data), 'instance.data') : seamlessImmutable([]);
			});

		const dateOptionReducer = composedReducer({ scope }, dateResource)
			.reduce(dates => {
				return dates ?
					dates.map(
						date => {
							let myDate = new Date(date);

							return {
								name: date,
								label: `${dateFilter(myDate, 'dd-MM-yyyy')} - ${myDate.toLocaleDateString('nl-NL', { weekday: 'long' })}`,
								click: ( ) => {
									selectedDate = date;
								}
							};
						}
					)
					: null;
			});

		const getTimeResourceOptions = () => {
			let opts;

			if (selectedDate) {
				opts = {
					url: `/api/v1/sysin/interface/${providerConfigurationReducer.data().appointmentInterfaceUuid}/trigger/get_timeslots?date=${selectedDate}&product_id=${providerConfigurationReducer.data().productId}&location_id=${providerConfigurationReducer.data().locationId}`
				};
			}

			return opts;
		};

		const timeResource = resource(getTimeResourceOptions, {
			scope,
			cache: {
				disabled: true
			}
		})
			.reduce(( requestOptions, data ) => {
				selectedTime = null;

				return data ?
					get(first(data), 'instance.data')
					: seamlessImmutable([]);
			});

		const getTimeLabel = timeslot =>
			`${dateFilter(new Date(timeslot.start_time), 'HH:mm')} - ${dateFilter(new Date(timeslot.end_time), 'HH:mm')}`;

		const timeOptionReducer = composedReducer({ scope }, timeResource)
			.reduce(times => {
				return times ?
					times.map(
						time => {
							return {
								name: time.start_time,
								label: getTimeLabel(time),
								click: () => {
									selectedTime = time;
								}
							};
						}
					)
					: null;
			});

		ctrl.confirmAppointment = () => {
			let data = {
				references: {
					requestor: {
						reference: providerConfigurationReducer.data().requestor,
						type: 'subject'
					}
				},
				appointment_data: assign({}, selectedTime)
			};

			snackbarService.wait(
				APPOINTMENT_CREATE_PENDING, {
					collapse: 0,
					promise:
						$http({
							method: 'POST',
							url: `/api/v1/sysin/interface/${providerConfigurationReducer.data().appointmentInterfaceUuid}/trigger/book_appointment`,
							data
						})
						.then(response => {
							let responseObj = response.data;

							if (responseObj.status_code !== 200) {
								throw new Error(APPOINTMENT_CREATE_THROW);
							}

							ngModel.$setViewValue(responseObj.result.reference, 'click');
						}),
					catch: () => APPOINTMENT_CREATE_ERROR
				}
			);
		};

		ctrl.getDateLabel = () => {
			return selectedDate ?
				dateFilter(new Date(selectedDate), 'dd-MM-yyyy')
				: 'Kies een dag';
		};

		ctrl.getTimeLabel = () => {
			return selectedTime ?
				getTimeLabel(selectedTime)
				: 'Kies een tijd';
		};

		ctrl.getDateOptions = dateOptionReducer.data;

		ctrl.getTimeOptions = timeOptionReducer.data;

		ctrl.canConfirm = () => selectedDate && selectedTime;

		ctrl.closeForm = () => {
			selectedDate = selectedTime = null;
		};

		ctrl.onDateOpen = () => {
			interacted = true;
		};

		ctrl.isDateLoading = () => dateOptionReducer.state() === 'pending';

		ctrl.isTimeLoading = () => timeOptionReducer.state() === 'pending';

		ctrl.isTimeVisible = () => !!selectedDate;

		ctrl.isUnavailable = () => !get(providerConfigurationReducer.data(), 'isCorrectlyConfigured');
	}

}
