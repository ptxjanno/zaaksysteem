import propCheck from './../../util/propCheck';

export default ( units, props ) => {

	propCheck.throw(
		propCheck.shape({
			units: propCheck.arrayOf(
				propCheck.shape({
					depth: propCheck.number,
					name: propCheck.string,
					org_unit_id: propCheck.string,
					roles: propCheck.arrayOf(
						propCheck.shape({
							name: propCheck.string,
							role_id: propCheck.string
						})
					)
				})
			),
			props: propCheck.shape({
				unit: propCheck.string,
				role: propCheck.string
			})
		}),
		{
			units,
			props
		}
	);

	let unit,
		role;

	unit = units.filter(
		u => u.org_unit_id === props.unit
	)[0];

	role = unit.roles.filter(r => r.role_id === props.role)[0];

	return {
		unit,
		role
	};


};
