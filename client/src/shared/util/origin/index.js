const pathExpression = /^\/[^/]/;

/**
 * Rudimentary origin test for client-side API calls.
 * This does not actually compare origins but probes for its absence.
 *
 * @param {strong} url
 *   Absolute path to API endpoint under the same origin
 * @return {boolean}
 */
export const hasSameOrigin = url => pathExpression.test(url);
