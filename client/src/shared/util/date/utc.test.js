import { toUtc } from './utc';

/**
 * @test {toUtc}
 */
describe('The `toUtc` function', () => {
  test('exports a function', () => {
    expect(typeof toUtc).toBe('function');
  });

  test('can resolve winter time', () => {
    const date = new Date(1980, 0, 1);

    expect(toUtc(date).toISOString()).toBe('1980-01-01T00:00:00.000Z');
  });

  test('can resolve summer time', () => {
    const date = new Date(1980, 5, 1);

    expect(toUtc(date).toISOString()).toBe('1980-06-01T00:00:00.000Z');
  });
});
