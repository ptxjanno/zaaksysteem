/**
 * @param {string} dateString
 * @returns {Object}
 * @property {number} year
 * @property {number} month
 * @property {number} day
 */
function extract(dateString) {
	const [ yearString, monthString, dayString ] = dateString.split('-');

	return {
		get year() {
			return Number(yearString);
		},

		get month() {
			return (Number(monthString) - 1);
		},

		get day() {
			return Number(dayString);
		}
	};
}

/**
 * Get a local date object from a local ISO 8601 calendar date string.
 *
 * Cf. {@link toIsoCalendarDate}
 *
 * @param {String} dateString
 * @return {Date}
 */
function fromIsoCalendarDate(dateString) {
	const { year, month, day } = extract(dateString);

	return new Date(year, month, day);
}

export default fromIsoCalendarDate;
