export const getOffsetInMilliseconds = dateObject =>
  (dateObject.getTimezoneOffset() * 60 * 1000);

/**
 * Get a UTC date object from a date object.
 *
 * @param {Date} dateObject
 * @return {Date}
 */
export const toUtc = dateObject =>
  new Date(dateObject.getTime() - getOffsetInMilliseconds(dateObject));

