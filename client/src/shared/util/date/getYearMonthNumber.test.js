import getYearMonthNumber from './getYearMonthNumber';

/**
 * @test {getYearMonthNumber}
 */
describe('The `getYearMonthNumber` function', () => {
	test('returns a `Number`', () => {
		const now = new Date();

		expect(typeof getYearMonthNumber(now)).toBe('number');
	});

	test('adds the actual month number', () => {
		const first = getYearMonthNumber(new Date(2000, 8, 1));
		const second = getYearMonthNumber(new Date(2000, 9, 1));

		expect(first).toBe(200009);
		expect(second).toBe(200010);
	});

	test('compares dates as numbers', () => {
		const first = getYearMonthNumber(new Date(2000, 8, 1));
		const second = getYearMonthNumber(new Date(2000, 9, 1));

		expect(second > first).toBe(true);
	});
});
