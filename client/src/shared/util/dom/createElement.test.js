import createElement from './createElement';

describe('the createElement function', () => {
  test('creates an element node', () => {
    const span = createElement('span');

    expect(span.nodeName).toBe('SPAN');
  });

  test('creates attributes', () => {
    const span = createElement('span', { title: 'hello'});

    expect(span.title).toBe('hello');
  });
});
