import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import get from 'lodash/get';
import includes from 'lodash/includes';
import resourceModule from '../../../shared/api/resource';
import snackbarServiceModule from './../../../shared/ui/zsSnackbar/snackbarService';

export default angular
  .module('Zaaksysteem.intern.communication.route', [
    angularUiRouterModule,
    resourceModule,
    snackbarServiceModule,
  ])
  .config([
    '$stateProvider', '$urlMatcherFactoryProvider',
    ( $stateProvider, $urlMatcherFactoryProvider ) => {
      $urlMatcherFactoryProvider.strictMode(false);

      $stateProvider
        .state('communication', {
          url: '/communication/:action/:id',
          title: 'Communication',
          template: '<zs-communication-view />',
          params: {
            action: {
              squash: true,
              value: null
            },
            id: {
              squash: true,
              value: null
            }
          },
          resolve: {
            __SIDE_EFFECT__: [
              '$rootScope', '$ocLazyLoad', '$q', 'user', 'snackbarService',
              ($rootScope, $ocLazyLoad, $q, user, snackbarService) => {
                const loggedInUser = get(user.data(), 'instance.logged_in_user');
                const capabilities = get(loggedInUser, 'capabilities');
                const systemRoles = get(loggedInUser, 'system_roles');
                const authorized = includes(systemRoles, 'Behandelaar') && includes(capabilities, 'message_intake');

                if(!authorized) {
                  snackbarService
                    .error('Helaas, u heeft geen toegang tot dit gedeelte van het zaaksysteem.', {
                      actions: []
                    });

                  return $q.reject('Not authorized');
                }

                return $q(resolve => {
                  require([
                    './index.js'
                  ], (...names) => {
                    $rootScope.$evalAsync(() => {
                      $ocLazyLoad
                        .load(names.map(name => ({ name })));
                      resolve();
                    });
                  });
                });
              }]
          }
        });
    }
  ])
  .name;
