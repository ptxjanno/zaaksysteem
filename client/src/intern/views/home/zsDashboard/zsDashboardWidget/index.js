import angular from 'angular';
import _ from 'lodash';
import zsDashboardWidgetSearch from './types/zsDashboardWidgetSearch';
import zsDashboardWidgetCasetype from './types/zsDashboardWidgetCasetype';
import zsDashboardWidgetTasks from './types/zsDashboardWidgetTasks';
import zsDashboardWidgetCreate from './types/zsDashboardWidgetCreate';
import template from './template.html';
import oneWayBind from './../../../../../shared/util/oneWayBind';
import snackbarServiceModule from './../../../../../shared/ui/zsSnackbar/snackbarService';
import find from 'lodash/find';
import zsScrollFadeModule from './../../../../../shared/ui/zsScrollFade';
import './styles.scss';

export default angular.module('Zaaksysteem.intern.home.zsDashboard.zsDashboardWidget', [
		zsDashboardWidgetSearch,
		zsDashboardWidgetCasetype,
		zsDashboardWidgetCreate,
		zsDashboardWidgetTasks,
		snackbarServiceModule,
		zsScrollFadeModule
	])
		.directive('zsDashboardWidget', [ '$timeout', 'snackbarService', ( $timeout, snackbarService ) => {
			return {
				restrict: 'E',
				scope: {
					widget: '&',
					onWidgetCreate: '&',
					onWidgetRemove: '&',
					onWidgetDataChange: '&',
					disabled: '&',
					widgets: '&',
					compact: '&',
					settings: '&'
				},
				template,
				bindToController: true,
				controller: [ '$scope', '$element', '$compile', function ( $scope, $element, $compile ) {

					let ctrl = this,
						compiled,
						type = ctrl.widget().widget,
						tag = `zs-dashboard-widget-${type}`,
						el = angular.element(`<${tag}></${tag}>`),
						filterVisible = false,
						widgetId = _.uniqueId('widget');

					$scope.widgetId = widgetId;
					$element.attr('id', widgetId);
					$element.attr('aria-labelledby', `${widgetId  }-title`);
					$element.attr('role', 'region');
					switch (type) {
						case 'create':
						el.attr('on-widget-create', 'zsDashboardWidget.handleWidgetCreate($widgetType)');
						break;

						default:
						case 'search':
						el.attr('widget-data', 'zsDashboardWidget.getWidgetData()');
						el.attr('on-data-change', 'zsDashboardWidget.handleDataChange($newData)');
						el.attr('is-widget-loading', 'zsDashboardWidget.isWidgetLoading($getter)');
						el.attr('filter-query', 'zsDashboardWidget.query');
						break;
					}


					if (type === 'search') {
						el.attr('on-select', 'zsDashboardWidget.clearQuery()');
					}

					el.attr('widget-title', 'zsDashboardWidget.getWidgetTitle($getter)');
					el.attr('compact', 'zsDashboardWidget.compact()');
					el.attr('settings', 'zsDashboardWidget.settings()');

					ctrl.handleWidgetCreate = ( widgetType ) => {
						let forceSingleInstance = [ 'casetype' ].indexOf(widgetType) !== -1;

						if (forceSingleInstance && !!find(ctrl.widgets(), { widget: widgetType })) {
							snackbarService.error('Dit type widget mag maar één keer worden toegevoegd.');
						} else {
							ctrl.onWidgetCreate( { $widgetType: widgetType } );
						}
					};

					ctrl.handleRemoveClick = ( ) => {
						ctrl.onWidgetRemove({ $widget: ctrl.widget() } );
					};

					ctrl.handleDataChange = ( data ) => {
						ctrl.onWidgetDataChange({ $id: ctrl.widget().id, $data: data });
					};

					ctrl.getWidgetData = ( ) => ctrl.widget().data;

					ctrl.getWidgetTitle = oneWayBind();
					ctrl.isWidgetLoading = oneWayBind();

					ctrl.getModelOptions = ( ) => [];

					ctrl.isFilterVisible = ( ) => filterVisible;

					ctrl.handleFilterToggleClick = ( ) => {

						if (filterVisible) {
							ctrl.query = '';
						}

						filterVisible = !filterVisible;

						if (filterVisible) {
							$timeout(( ) => {
								$element.find('input')[0].focus();
							}, 0, false);
						}

					};

					ctrl.clearQuery = ( ) => {
						ctrl.query = '';

						if (filterVisible) {
							ctrl.handleFilterToggleClick();
						}
					};

					compiled = $compile(el)($scope.$new());

					// make sure close button doesn't trigger a widget drag
					$element.find('button').bind('mousedown', ( event ) => {
						event.stopPropagation();
					});

					$element.find('zs-dashboard-widget-content').replaceWith(compiled);

				}],
				controllerAs: 'zsDashboardWidget'
			};
		}])
		.name;
