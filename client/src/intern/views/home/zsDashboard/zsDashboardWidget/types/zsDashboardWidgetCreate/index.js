import angular from 'angular';
import template from './template.html';

export default angular.module('zsDashboardWidgetCreate', [
	])
		.directive('zsDashboardWidgetCreate', [ ( ) => {
			return {
				restrict: 'E',
				scope: {
					onWidgetCreate: '&'
				},
				template,
				bindToController: true,
				controller: [ function ( ) {

					let ctrl = this,
						widgetTypes;

					widgetTypes = [
						{
							label: 'Zoekopdracht',
							value: 'search',
							iconClass: 'file-find'
						},
						{
							label: 'Favoriete zaaktypen',
							value: 'casetype',
							iconClass: 'cube'
						},
						{
							label: 'Mijn openstaande taken',
							value: 'tasks',
							iconClass: 'checkbox-multiple-marked-outline'
						}
					];

					ctrl.getWidgetTypes = () => widgetTypes;

					ctrl.selectType = ( option ) => {
						ctrl.onWidgetCreate( { $widgetType: option.value });
					};
					
					return ctrl;
				}],
				controllerAs: 'zsDashboardWidgetCreate'
			};
		}])
		.name;
