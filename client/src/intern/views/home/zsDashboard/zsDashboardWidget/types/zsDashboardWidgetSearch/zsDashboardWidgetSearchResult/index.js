import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import assign from 'lodash/assign';
import compact from 'lodash/compact';
import escapeString from 'lodash/escape';
import filter from 'lodash/filter';
import find from 'lodash/find';
import first from 'lodash/head';
import get from 'lodash/get';
import identity from 'lodash/identity';
import immutable from 'seamless-immutable';
import includes from 'lodash/includes';
import initial from 'lodash/initial';
import isArray from 'lodash/isArray';
import isObject from 'lodash/isObject';
import last from 'lodash/last';
import mapValues from 'lodash/mapValues';
import merge from 'lodash/merge';
import shortid from 'shortid';
import uniq from 'lodash/uniq';
import caseActions from './../../../../../../../../shared/case/caseActions';
import composedReducerModule from './../../../../../../../../shared/api/resource/composedReducer';
import convertv0Case from './../../../../../../../../shared/api/mock/convertv0Case';
import defaultCompactTemplate from './default-compact-template.html';
import resourceModule from './../../../../../../../../shared/api/resource';
import savedSearchesServiceModule from './../../../../../../../../shared/ui/zsSpotEnlighter/savedSearchesService';
import sessionServiceModule from './../../../../../../../../shared/user/sessionService';
import snackbarServiceModule from './../../../../../../../../shared/ui/zsSnackbar/snackbarService';
import zqlEscapeFilterModule from './../../../../../../../../shared/object/zql/zqlEscapeFilter';
import zsCaseAdminViewModule from './../../../../../../case/zsCaseView/zsCaseAdminView';
import zsCaseIntakeActionListModule from './../../../../../../../../shared/case/zsCaseIntakeActionList';
import zsCaseSearchResultCompactModule from './zsCaseSearchResultCompact';
import zsCaseStatusIconModule from './../../../../../../../../shared/case/zsCaseStatusIcon';
import zsConfirmModule from './../../../../../../../../shared/ui/zsConfirm';
import zsModalModule from './../../../../../../../../shared/ui/zsModal';
import zsPaginationModule from './../../../../../../../../shared/ui/zsPagination';
import zsProgressBarModule from './../../../../../../../../shared/ui/zsProgressBar';
import zsSpinnerModule from './../../../../../../../../shared/ui/zsSpinner';
import zsTableModule from './../../../../../../../../shared/ui/zsTable';
import zsTruncateModule from '../../../../../../../../shared/ui/zsTruncate';
import reloadWithRetry from '../../../../../../../../shared/api/resource/reloadWithRetry';

import { confirmCaseRejectAllocation } from './../../../../../../../../shared/case/caseActions/allocation';

import './styles.scss';
import template from './template.html';

export default angular
	.module('zsDashboardWidgetSearchResult', [
		angularUiRouterModule,
		resourceModule,
		composedReducerModule,
		savedSearchesServiceModule,
		zqlEscapeFilterModule,
		zsPaginationModule,
		zsTableModule,
		sessionServiceModule,
		zsSpinnerModule,
		zsProgressBarModule,
		zsCaseAdminViewModule,
		zsModalModule,
		zsConfirmModule,
		snackbarServiceModule,
		zsCaseIntakeActionListModule,
		zsTruncateModule,
		zsCaseSearchResultCompactModule,
		zsCaseStatusIconModule
	])
	.directive('zsDashboardWidgetSearchResult', [
		'$compile',
		'$parse',
		'$state',
		'$timeout',
		'$http',
		'resource',
		'composedReducer',
		'savedSearchesService',
		'sessionService',
		'zqlEscapeFilter',
		'zsModal',
		'zsConfirm',
		'snackbarService',
		(
			$compile,
			$parse,
			$state,
			$timeout,
			$http,
			resource,
			composedReducer,
			savedSearchesService,
			sessionService,
			zqlEscapeFilter,
			zsModal,
			zsConfirm,
			snackbarService
		) => {
			let templates = {
				intake_actions: {
					template:
						'<zs-case-intake-action-list data-case="item.data.case" data-user="item.data.user" on-self-assign="item.data.onSelfAssign()" on-reject="item.data.onReject()"></zs-case-intake-action-list>'
				}
			};

			return {
				restrict: 'E',
				scope: {
					widgetData: '&',
					widgetTitle: '&',
					widgetLoading: '&',
					onDataChange: '&',
					filterQuery: '&',
					compact: '&',
					settings: '&'
				},
				template,
				bindToController: true,
				controller: [
					'$element',
					'$scope',
					function($element, $scope) {
						let ctrl = this,
							searchReducer,
							configResource,
							userResource = sessionService.createResource($scope),
							columnReducer,
							zqlReducer,
							resultResource,
							itemReducer,
							sortReducer,
							filterByReducer,
							userSort = immutable({});

						let getSearchId = () => get(ctrl.widgetData(), 'search_id');

						let getSearchFromPredefined = () => {
							return savedSearchesService
								.getPredefinedSearches()
								.filter(search => search.id === getSearchId())[0];
						};

						let createCaseResource = caseObj => {
							return resource(`/api/case/${caseObj.values['case.number']}`, {
								scope: $scope
							}).reduce((requestOptions, data) => {
								return first((data || []).map(convertv0Case));
							});
						};

						let handleActionModal = (caseObj, action) => {
							let scope = $scope.$new(true),
								modal;

							let cleanup = () => {
								scope.caseResource.destroy();
								scope.casetypeResource.destroy();
							};

							scope.action = action;

							scope.close = promise => {
								modal.hide();

								promise
									.then(() => {
										reloadWithRetry(resultResource, () => {
											modal.close();
											cleanup();
										});
									})
									.catch(() => {
										modal.show();
									});
							};

							scope.user = userResource.data().instance.logged_in_user;

							scope.caseResource = createCaseResource(caseObj);

							scope.casetypeResource = resource(
								() => {
									let v0Case = scope.caseResource.data();

									return v0Case
										? {
												url: `/api/v1/casetype/${v0Case.instance.casetype.reference}`,
												params: {
													version: v0Case.instance.casetype.instance.version
												}
										  }
										: null;
								},
								{
									scope: $scope
								}
							).reduce((requestOptions, data) => {
								return first(data);
							});

							modal = zsModal({
								title: action.label,
								el: $compile(
									`<zs-case-admin-view
									data-action="action"
									data-user="user"
									case-resource="caseResource"
									casetype-resource="casetypeResource"
									on-submit="close($promise)"
								>
								</zs-case-admin-view>`
								)(scope)
							});

							modal.open();

							modal.onClose(() => {
								cleanup();
							});
						};

						let handleActionConfirm = (caseObj, action) => {
							let caseResource = createCaseResource(caseObj),
								mutation = action.mutate();

							zsConfirm(action.confirm.label, action.confirm.verb)
								.then(() => {
									return caseResource
										.mutate(mutation.type, mutation.data)
										.asPromise();
								})
								.finally(() => {
									caseResource.destroy();
								});
						};

						let handleActionClick = (caseObj, action) => {
							if (action.confirm) {
								handleActionConfirm(caseObj, action);
							} else {
								handleActionModal(caseObj, action);
							}
						};

						searchReducer = composedReducer(
							{ scope: $scope },
							getSearchFromPredefined() ||
								resource(
									() => {
										let zql = `SELECT {} FROM saved_search WHERE object.uuid = ${zqlEscapeFilter(
											getSearchId()
										)}`;

										return { url: '/api/object/search', params: { zql } };
									},
									{ scope: $scope }
								).reduce((requestOptions, data) => {
									let search = first(data);

									if (search) {
										search = savedSearchesService.parseLegacyFilter(search);
									}

									return search;
								})
						).reduce(identity);

						filterByReducer = composedReducer(
							{ scope: $scope },
							searchReducer,
							ctrl.filterQuery
						).reduce((search, filterQuery) => {
							let filterBy = [filterQuery],
								searchZql = get(search, 'values.query.zql', '');

							if (searchZql) {
								let match = searchZql.match(/MATCHING "(.*?)"/);

								if (match) {
									filterBy = filterBy.concat(match[1]);
								}
							}

							return compact(filterBy);
						});

						configResource = resource(
							() => {
								let objectTypeId = get(
										searchReducer.data(),
										'values.query.objectType.object_type'
									),
									opts = objectTypeId
										? { url: `/api/search/config/${objectTypeId}` }
										: null;

								return opts;
							},
							{ scope: $scope }
						).reduce((requestOptions, data) => first(data));

						// columns without visual labels are given a label
						const ariaLabelDictionary = {
							'case.status': 'Status',
							'case.number': 'Zaaknummer',
							'case.days_left': 'Dagen te gaan',
							'intake_actions': 'Zaakintake acties',
							'actions': 'Zaakacties'
						};

						columnReducer = composedReducer(
							{ scope: $scope },
							searchReducer,
							configResource
						).reduce((search, config) => {
							let columnConfig = merge(
									{},
									get(config, 'columns.templates'),
									templates
								),
								columns = [],
								defaults = get(config, 'columns.default', immutable([]));

							if (search && search.values.query.columns) {
								columns = search.values.query.columns;
							} else if (search) {
								columns = defaults.map(columnId => {
									return { id: columnId };
								});
							}

							if (search.id === 'intake') {
								columns = immutable(
									initial(columns)
										.concat([{ id: 'intake_actions' }])
										.concat(last(columns))
								);
							}

							columns =
								// notifications are now in the status column, so filter
								// out the column if it's in a saved search
								columns
									.filter(column => column.id !== 'notifications')
									.map(columnToMap => {
										let column = columnToMap,
											data = columnConfig[column.id] || {},
											resolve;

										if (data.template) {
											data = assign({}, data, {
												template: savedSearchesService.convertLegacyTemplate(
													data.template
												)
											});
										}

										column = column.merge(data);

										if (
											!column.template ||
											(includes(defaults, column.id) && !data.template)
										) {
											resolve = column.resolve
												? `item.${column.resolve}`
												: `item.values['${column.id}']`;

											if (column.filter) {
												resolve += ` | ${column.filter}`;
											}

											column = column.merge({
												template: `<span>{{::${resolve}}}</span>`
											});
										}

										// make sure everything is wrapped in a span
										if (column.template.indexOf('<') !== 0) {
											column = column.merge({
												template: `<span>${column.template}</span>`
											});
										}

										column = column.merge({
											ariaLabel: get(ariaLabelDictionary, column.id, column.label),
										});

										return column;
									});

							return columns;
						});

						sortReducer = composedReducer(
							{ scope: $scope },
							searchReducer,
							columnReducer,
							configResource,
							() => userSort
						).reduce((search, columns, config, userSortData) => {
							let sort,
								searchSort =
									get(search, 'values.query.options.sort') || immutable({}),
								defaultSort = get(config, 'sort') || immutable({});

							sort = defaultSort.merge([searchSort, userSortData]);

							sort = sort.merge({
								type: get(
									find(columns, { id: sort.by }),
									'sort.type',
									'alphanumeric'
								)
							});

							if (!sort.by) {
								sort = {
									by: get(first(columns), 'id'),
									order: 'asc'
								};
							}

							return sort;
						});

						zqlReducer = composedReducer(
							{ scope: $scope },
							searchReducer,
							columnReducer,
							configResource,
							userResource,
							sortReducer,
							filterByReducer
						).reduce((search, columns, config, user, sort, matching) => {
							let zql = get(search, 'zql'),
								columnsToFetch = columns,
								opts;

							if (!zql && search && user && sort) {
								columnsToFetch = columns
									.flatMap(column => get(column, 'data.columns', column.id))
									.concat(get(config, 'always', []));

								if (
									get(search, 'values.query.objectType.object_type') === 'case'
								) {
									columnsToFetch = columnsToFetch.concat([
										'case.archival_state',
										'case.destructable',
										'case.casetype.id',
										'case.requestor.id',
										'case.status',
										'case.assignee.id'
									]);
								}

								if (search.id === 'intake') {
									columnsToFetch = columnsToFetch.concat(
										'case.assignee.department',
										'case.casetype.department',
										'case.casetype.id'
									);
								}

								opts = {
									user,
									sort,
									columns: uniq(columnsToFetch)
								};

								if (matching) {
									opts.matching = matching.join(' ');
								}

								zql = savedSearchesService.getZql(search, opts);
							}

							return zql;
						});

						resultResource = resource(
							() => {
								let zql = zqlReducer.data(),
									limit = get(ctrl.widgetData(), 'limit', 10),
									opts;

								if (zql && resultResource) {
									opts = {
										url: '/api/object/search',
										params: { zql, zapi_num_rows: limit }
									};
								}

								return opts;
							},
							{ scope: $scope }
						);

						itemReducer = composedReducer(
							{ scope: $scope },
							resultResource,
							configResource,
							userResource,
							searchReducer,
							ctrl.compact
						).reduce(
							(resultData, config, user, search, isCompact) => {
								let items = resultData,
									classConfig = mapValues(get(config, 'style.classes'), expr =>
										$parse(expr)
									),
									objectType = get(
										search,
										'values.query.objectType.object_type',
										'case'
									);

								// make sure we are able to differentiate between empty and non-existent result sets
								if (!items) {
									return null;
								}

								let humanIdentifierOrValue = val =>
									isObject(val) && val.hasOwnProperty('human_identifier')
										? val.human_identifier
										: val;

								let transformValues = itemValues => {
									return mapValues(itemValues, (value, key) => {
										if (key === 'case.subject' && !isCompact) {
											value = escapeString(value);
										}

										if (isArray(value)) {
											return filter(value, identity)
												.map(val => {
													let mimetype = get(val, 'mimetype');
													if (
														isObject(val) &&
														typeof mimetype === 'string' &&
														mimetype !== ''
													) {
														return (
															get(val, 'filename') || get(val, 'original_name')
														);
													}

													return humanIdentifierOrValue(val);
												})
												.join(', ');
										}

										return humanIdentifierOrValue(value);
									});
								};

								const statusTranslations = {
									'new': 'Nieuw',
									'open': 'In behandeling',
									'stalled': 'Opgeschort',
									'closed': 'Afgehandeld'
								};

								// in normal circumstances cases always have a requestor and days_left
								// the safety checks are for the test enviroment where broken cases are frequent
								const getCompactAriaValue = item => {
									const values = get(item, 'values');
									const caseType = values['case.casetype.name'];
									const subject = values['case.subject'];
									const subjectText = subject ? `, ${subject}` : '';
									const requestor = values['case.requestor.name'];
									const requestorText = requestor ? `Aanvrager: ${requestor.replace('.', '')}` : '';
									const daysLeft = values['case.days_left'];
									let daysLeftText;

									if(daysLeft) {
										const daysLeftStr = daysLeft.toString();
										const isOvertime = daysLeftStr[0] === '-';
										
										daysLeftText = isOvertime ?
											`${daysLeftStr.substring(1)} dagen te laat`
											: `${daysLeftStr} dagen te gaan`;
									} else {
										daysLeftText = 'Onbekend aantal dagen te gaan';
									}

									return `${caseType}${subjectText}, ${requestorText}, ${daysLeftText}`;
								};

								if (objectType === 'case') {
									items = items.map(item => {

										let caseId = item.values['case.number'],
											reference = item.id;

										let ariaValues = merge({}, item.values, {
											'case.status': statusTranslations[item.values['case.status']],
										});

										if(isCompact) {
											ariaValues = merge({}, ariaValues, {
												compact: getCompactAriaValue(item)
											})
										}

										return item.merge({
											id: shortid(),
											ariaLabel: `Zaak: ${caseId}`,
											ariaValues,
											href: $state.href('case', { caseId }),
											style: mapValues(classConfig, getter => getter(item)),
											itemActions: caseActions({
												caseObj: convertv0Case(item),
												casetype: null,
												user: user.instance.logged_in_user,
												$state,
												acls: []
											})
												.filter(action => action.context !== 'admin')
												.map(action => {
													return {
														name: action.name,
														label: action.label,
														click: () => {
															handleActionClick(item, action);
														}
													};
												}),
											data: {
												case: convertv0Case(item),
												user,
												onReject: () => {
													confirmCaseRejectAllocation({
														http: $http,
														resource: resultResource.reload,
														caseUUID: reference,
														settings: ctrl.settings(),
														snackbar: snackbarService,
														confirm: zsConfirm
													});
												},
												onSelfAssign: () => {
													snackbarService
														.wait('Zaak wordt in behandeling genomen.', {
															promise: $http({
																url: '/api/v2/cm/case/assign_case_to_self',
																method: 'POST',
																data: {
																	case_uuid: reference
																}
															}),
															then: () => {
																return 'Zaak is in behandeling genomen.';
															},
															catch: () => {
																return 'Zaak kon niet in behandeling worden genomen. Neem contact op met uw beheeerder voor meer informatie.';
															}
														})
														.then(() => {
															$state.go('case', { caseId });
														});
												}
											},
											values: transformValues(item.values)
										});
									});
								} else {
									items = items.map(item => {
										return item.merge({
											id: item.id,
											href: `/object/${item.id}`,
											values: transformValues(item.values)
										});
									});
								}

								return items;
							}
						);

						ctrl.getRows = itemReducer.data;

						ctrl.getColumns = composedReducer(
							{ scope: $scope },
							columnReducer,
							sortReducer,
							configResource,
							ctrl.compact
						).reduce((columns, sort, config, isCompact) => {
							let visibleColumns = columns;

							if (isCompact) {
								visibleColumns = immutable([
									{
										id: 'compact',
										label: '',
										ariaLabel: 'Zaakinformatie',
										template: get(
											config,
											'columns.compact',
											defaultCompactTemplate
										)
									}
								]);

								visibleColumns = visibleColumns.concat(
									columns.filter(
										col => col.id === 'actions' || col.id === 'intake_actions'
									)
								);
							}

							let styledColumns = visibleColumns.map(column => {
								let sortedOn = column.id === get(sort, 'by'),
									sortReversed =
										sortedOn && get(sort, 'order', 'asc') === 'desc',
									iconType;

								if (sortedOn) {
									iconType = `chevron-${sortReversed ? 'up' : 'down'}`;
								}

								return column.merge({
									style: {
										'column-sort-active': sortedOn,
										'column-sort-reversed': sortReversed,
										'column-sort-supported': column.sort
									},
									iconType
								});
							});

							return styledColumns;
						}).data;

						ctrl.handleColumnClick = columnId => {
							let sort = sortReducer.data(),
								column = find(columnReducer.data(), { id: columnId });

							if (!get(column, 'sort', null)) {
								return;
							}

							if (sort.by === columnId) {
								sort = sort.merge({
									order: sort.order === 'desc' ? 'asc' : 'desc'
								});
							} else {
								sort = sort.merge({ by: columnId, order: 'asc' });
							}

							userSort = sort;
						};

						ctrl.getCurrentPage = () => resultResource.cursor();

						ctrl.handlePageChange = page => {
							resultResource.request(resultResource.cursor(page));
						};

						ctrl.handleLimitChange = limit => {
							ctrl.onDataChange({
								$data: assign({}, ctrl.widgetData(), { limit })
							});
							resultResource.request(resultResource.cursor(1));
						};

						ctrl.hasNextPage = () => !!resultResource.next();

						ctrl.hasPrevPage = () => !!resultResource.prev();

						ctrl.getLimit = () => resultResource.limit();

						ctrl.isLoading = () => zqlReducer.state() === 'pending';

						ctrl.isResultLoading = () =>
							!ctrl.isLoading() &&
							(itemReducer.state() === 'pending' || resultResource.fetching());

						ctrl.isEmpty = () => get(ctrl.getRows(), 'length', false) === 0;

						ctrl.widgetTitle({
							$getter: () => {
								let label = get(searchReducer.data(), 'label') || '';

								if (resultResource.totalRows()) {
									label += ` (${resultResource.totalRows()})`;
								}

								return label;
							}
						});

						ctrl.widgetLoading({
							$getter: ctrl.isResultLoading
						});

						return ctrl;
					}
				],
				controllerAs: 'vm'
			};
		}
	]).name;
