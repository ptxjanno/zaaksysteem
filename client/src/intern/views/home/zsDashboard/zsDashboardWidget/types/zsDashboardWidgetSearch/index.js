import template from './template.html';
import angular from 'angular';
import zsDashboardWidgetSearchSelectList from './zsDashboardWidgetSearchSelectList';
import zsDashboardWidgetSearchResult from './zsDashboardWidgetSearchResult';
import assign from 'lodash/assign';

export default angular.module('Zaaksysteem.intern.home.zsDashboard.zsDashboardWidget.types.zsDashboardWidgetSearch', [
		zsDashboardWidgetSearchSelectList,
		zsDashboardWidgetSearchResult
	])
		.directive('zsDashboardWidgetSearch', [ ( ) => {
			return {
				restrict: 'E',
				scope: {
					onDataChange: '&',
					widgetData: '&',
					widgetTitle: '&',
					isWidgetLoading: '&',
					filterQuery: '&',
					onSelect: '&',
					compact: '&',
					settings: '&'
				},
				template,
				bindToController: true,
				controller: [ function ( ) {
					
					let ctrl = this;

					ctrl.getState = ( ) => {
						const data = ctrl.widgetData();

						return data && data.search_id ? 'search' : 'list';
					};

					ctrl.handleSearchSelect = ( search ) => {
						
						ctrl.onDataChange(
							{
								$newData: assign({}, ctrl.widgetData(), { search_id: search.id })
							}
						);

						ctrl.onSelect();
					};

					ctrl.getWidgetTitle = ( getter ) => {
						ctrl.widgetTitle({ $getter: getter });
					};

					return ctrl;
				}],
				controllerAs: 'zsDashboardWidgetSearch'
			};
		}])
		.name;
