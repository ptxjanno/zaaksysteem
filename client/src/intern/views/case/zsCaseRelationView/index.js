import angular from 'angular';
import auxiliaryRouteModule from './../../../../shared/util/route/auxiliaryRoute';
import angularUiRouterModule from 'angular-ui-router';
import resourceModule from './../../../../shared/api/resource';
import composedReducerModule from './../../../../shared/api/resource/composedReducer';
import zsTableModule from './../../../../shared/ui/zsTable';
import zsProgressBarModule from './../../../../shared/ui/zsProgressBar';
import actionsModule from './actions';
import zsConfirmModule from './../../../../shared/ui/zsConfirm';
import zsModalModule from './../../../../shared/ui/zsModal';
import vormFormModule from './../../../../shared/vorm/vormForm';
import contextualActionServiceModule from './../../../../shared/ui/zsContextualActionMenu/contextualActionService';
import angularDragulaModule from 'angular-dragula';
import each from 'lodash/each';
import assign from 'lodash/assign';
import createRoleResource from './../zsCaseAddSubject/createRoleResource';
import {
	parentAndChildCasesTable,
	plannedCasesTable,
	relatedCasesTable,
	relatedSubjectsTable,
	relatedObjectsTable,
	plannedEmailsTable
} from './tables';
import template from './template.html';
import './styles.scss';
import openForm from './tables/library/openForm';

export default
	angular.module('zsCaseRelationView', [
		angularDragulaModule(angular),
		angularUiRouterModule,
		auxiliaryRouteModule,
		composedReducerModule,
		zsTableModule,
		zsProgressBarModule,
		actionsModule,
		zsConfirmModule,
		zsModalModule,
		vormFormModule,
		contextualActionServiceModule,
		resourceModule
	])
		.directive(
			'zsCaseRelationView',
			[ '$q', '$compile', '$state', '$timeout', 'resource', 'composedReducer', 'dateFilter', 'zsConfirm', 'zsModal', 'contextualActionService', 'auxiliaryRouteService',
			( $q, $compile, $state, $timeout, resource, composedReducer, dateFilter, zsConfirm, zsModal, contextualActionService, auxiliaryRouteService ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					relatedCases: '&',
					childCases: '&',
					parentCase: '&',
					currentCase: '&',
					siblingCases: '&',
					relatedObjects: '&',
					plannedCases: '&',
					relatedSubjects: '&',
					plannedEmails: '&',
					canRelate: '&',
					onCaseRelate: '&',
					onCaseUnrelate: '&',
					onCaseReorder: '&',
					onPlannedCaseUpdate: '&',
					onPlannedCaseRemove: '&',
					onObjectRelate: '&',
					onRelatedSubjectUpdate: '&',
					onRelatedSubjectRemove: '&',
					onEmailReschedule: '&',
					supportsPlannedEmails: '&',
					hasRelatableObjectTypes: '&',
					disabled: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( scope ) {

					let ctrl = this,
						tableReducer,
						listeners = [];

					let roleResource = createRoleResource(resource, scope);

					tableReducer = composedReducer( { scope }, ctrl.parentCase, ctrl.siblingCases, ctrl.childCases, ctrl.relatedCases, ctrl.relatedObjects, ctrl.relatedSubjects, ctrl.plannedCases, ctrl.plannedEmails, ctrl.disabled, ctrl.supportsPlannedEmails, ctrl.hasRelatableObjectTypes, ctrl.canRelate, roleResource)
						.reduce( ( parentCase = null, siblingCases = [], childCases = [], relatedCases = [], relatedObjects = [], relatedSubjectsResource = [], plannedCases = [], plannedEmails = [], isDisabled, supportsPlannedEmails, hasRelatableObjectTypes, canRelate, roles ) => {
							const callOpenForm = options => openForm($q, $compile, scope, zsModal, options);

							let tables = [
								parentAndChildCasesTable($state, parentCase, siblingCases, childCases),
								relatedCasesTable($state, relatedCases, canRelate, ctrl.onCaseRelate, ctrl.onCaseUnrelate, zsConfirm),
								plannedCasesTable($q, callOpenForm, plannedCases, ctrl.onPlannedCaseUpdate, ctrl.onPlannedCaseRemove, isDisabled, dateFilter, zsConfirm, contextualActionService),
								relatedSubjectsTable($timeout, callOpenForm, relatedSubjectsResource, roles, isDisabled, zsConfirm, resource, scope, ctrl.currentCase, ctrl.onRelatedSubjectUpdate, ctrl.onRelatedSubjectRemove, contextualActionService),
								relatedObjectsTable($state, relatedObjects, hasRelatableObjectTypes, auxiliaryRouteService),
								plannedEmailsTable(plannedEmails, supportsPlannedEmails, isDisabled, ctrl.onEmailReschedule)
							];

							tables = tables.filter(table => table.visible || table.visible === undefined)
								.map(table => {
									let config;

									config = assign(
										{},
										table,
										{
											columns: table.columns.map(
												col => {

													let resolve = col.resolve || col.id;

													return assign(
														{
															template: `<span>{{::item.${resolve}}}</span>`
														},
														col
													);

												}
											)
										}
									);

									if (config.actions && !isDisabled || canRelate) {

										config = assign(
											{},
											config,
											{
												columns: config.columns.concat(
													{
														id: 'actions',
														label: '',
														template:
															`<button
																ng-repeat="action in item.actions track by action.name"
																ng-if=action.visible(item)
																type="button"
																class="button-reset btn-icon"
																ng-click="action.click(item, $event)"
																zs-tooltip="{{::action.label}}"
																zs-tooltip-options="{ attachment: 'right middle', target: 'left middle', flip: true, offset: { x: -10, y: 0 } }"
															>
																<zs-icon icon-type="{{::action.icon}}"></zs-icon>
															</button>`
													}
												),
												items: config.items.map(
													item => assign({ actions: config.actions }, item)
												)
											}
										);
									}
								

									if ( (table.name === 'related-cases' && !canRelate) || (table.name !== 'related-cases' && isDisabled)) {
										config = assign({}, config, { add: null });
									}

									return config;
								}
							);

							return tables;

						});

					ctrl.getTables = tableReducer.data;

					tableReducer.subscribe( ( ) => {

						each(listeners, fn => {
							fn();
						});

						listeners = tableReducer.data()
							.map( ( table ) => {

								return scope.$on(`${table.name}.drop`, ( event, draggedElement ) => {
									const elScope = angular.element(draggedElement).scope();

									if (!elScope || !elScope.item) {
										return;
									}

									const after = draggedElement[0].previousElementSibling;
									let prevCaseId;

									if (after && after.nodeName !== '#comment') {
										prevCaseId = angular.element(after).scope().item.reference;
									}

									// the data of the table is pre-move
									// the draggedElement and the 'after' are post-move
									// therefore the direction of the move can be determined by checking their old positions
									// if you move an item down, (move A down: A B C --> B C A)
									// the new 'after' (C) was after the moved item (A) originally
									// if you move an item up, (move C up: A B C --> A C B)
									// the new 'after' (A) was before the moved item (C) originally
									const caseId = elScope.item.reference;
									const currentListUuids = table.items.map(item => item.reference);
									const oldIndexOfMovedItem = currentListUuids.indexOf(caseId);
									const oldIndexOfPrevItem = currentListUuids.indexOf(prevCaseId);
									const direction = oldIndexOfMovedItem > oldIndexOfPrevItem ? 'up' : 'down';

									ctrl.onCaseReorder( { $reference: caseId, $after: prevCaseId, $direction: direction });

								});

							});
					});

				}],
				controllerAs: 'vm'
			};

		}])
		.directive('zsCaseRelationViewTable', [ 'dragulaService', ( dragulaService ) => {

			return {
				restrict: 'A',
				link: ( scope, element, attrs ) => {

					let bagId = attrs.zsCaseRelationViewTable;

					dragulaService.options(scope, bagId, {
						moves: ( el, container, handle ) => {
							return handle.classList.contains('mdi-drag-vertical');
						},
						containers: [ element.find('zs-table-body')[0] ],
						isContainer: ( el ) => {
							let isContainer = element[0].contains(el) && el.classList.contains('table-body');

							return isContainer;
						}
					});

					scope.$on('$destroy', ( ) => {
						dragulaService.destroy(scope, bagId);
					});

				}
			};

		}])
		.name;
