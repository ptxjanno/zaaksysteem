import shortid from 'shortid';

const columns = [
  {
    id: 'date',
    label: 'Ingepland voor',
    template:
      '<span>{{::item.instance.date | date:\'dd-MM-yyyy HH:mm\'}}</span>'
  },
  {
    id: 'label',
    label: 'Sjabloon',
    resolve: 'instance.label'
  },
  {
    id: 'recipient',
    label: 'Ontvanger',
    resolve: 'instance.recipient'
  }
];

const plannedEmailsTable = (plannedEmails, supportsPlannedEmails, isDisabled, onEmailReschedule) => {
  const items = (plannedEmails || []).map(
    email => email.merge({ id: shortid() })
  );

  const collectionActions = isDisabled ? [] : [{
    type: 'button',
    data: {
      label: 'Herplannen',
      click: ( ) => {
        onEmailReschedule();
      }
    }
  }];

  return {
    name: 'planned-emails',
    label: 'Geplande e-mails',
    columns,
    items,
    collectionActions,
    visible: !!supportsPlannedEmails
  };
};

export default plannedEmailsTable;
