const getRelationLink = (type, uuid) =>
	`/redirect/contact_page?type=${type}&uuid=${uuid}`;

export default getRelationLink;
