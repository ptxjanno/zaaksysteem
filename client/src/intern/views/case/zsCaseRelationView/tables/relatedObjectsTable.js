import shortid from 'shortid';

const columns = [
  {
    id: 'type',
    label: 'Type'
  },
  {
    id: 'label',
    label: 'Naam'
  }
];

const relatedObjectsTable = ($state, relatedObjects, hasRelatableObjectTypes, auxiliaryRouteService) => {
  const items = relatedObjects.map(object => ({
    id: shortid(),
    type: object.related_object_type_label,
    label: object.label,
    href: `/object/${object.reference}`
  }));

  const collectionActions = hasRelatableObjectTypes ? [{
    type: 'link',
    data: {
      label: 'Relateer object',
      href: $state.href(
        auxiliaryRouteService.append($state.current, 'admin'),
        { action: 'object-relateren' },
        { inherit: true }
      )
    }
  }] : [];

  return {
    name: 'related_objects',
    label: 'Gerelateerde objecten',
    href: true,
    columns,
    items,
    collectionActions
  };
};

export default relatedObjectsTable;
