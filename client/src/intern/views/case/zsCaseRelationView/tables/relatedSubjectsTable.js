import angular from 'angular';
import shortid from 'shortid';
import assign from 'lodash/assign';
import capitalize from 'lodash/capitalize';
import includes from 'lodash/includes';
import seamlessImmutable from 'seamless-immutable';
import getRelationLink from './library/getRelationLink';
import relatedSubjectFormCtrl from '../../zsCaseAddSubject/form';
import createMagicStringResource from '../../zsCaseAddSubject/createMagicStringResource';

const columns = [
  {
    id: 'type',
    label: 'Type',
    template: '<span>{{::item.typeCapital}}</span>'
  },
  {
    id: 'name',
    label: 'Naam',
    template: `<a href="{{::item.link}}">{{::item.name}}</a>
      <button
        class="related-subjects-new-tab-button btn-open-new-tab btn btn-small btn-flat"
        ng-click="item.openInNewTab(item.link)"
      >
        <zs-icon
          class="related-subjects-icon related-subjects-icon-new-tab"
          icon-type="open-in-new"
        >
        </zs-icon>
      </button>`
  },
  {
    id: 'role',
    label: 'Rol'
  },
  {
    id: 'pip_authorized',
    label: 'Gemachtigd',
    template: '<zs-icon icon-type="{{::item.icon}}"></zs-icon>'
  }
];

// v2 to v0
const subjectTypeDict = {
  person: 'natuurlijk_persoon',
  organization: 'bedrijf',
  employee: 'medewerker'
};

const relatedSubjectsTable = ($timeout, openForm, relatedSubjectsResource, roles, isDisabled, zsConfirm, resource, scope, currentCase, onRelatedSubjectUpdate, onRelatedSubjectRemove, contextualActionService) => {
  const subjects = relatedSubjectsResource.map(relation => {
    return {
      betrokkene_id: relation.relationships.subject.data.id,
      subjectType: relation.relationships.subject.data.type,
      betrokkene_type: subjectTypeDict[relation.relationships.subject.data.type],
      employee_authorisation: relation.attributes.permission,
      magic_string_prefix: relation.attributes.magic_string_prefix,
      name: relation.relationships.subject.data.meta.display_name,
      pip_authorized: relation.attributes.authorized,
      role: relation.attributes.role,
      relationId: relation.id,
    };
  });

  const items = subjects.map(relatedSubject => {
    const {
      betrokkene_type,
      subjectType,
      betrokkene_id,
      name,
      role,
      pip_authorized
    } = relatedSubject;

    return {
      id: shortid(),
      type: betrokkene_type,
      link: getRelationLink(subjectType, betrokkene_id),
      typeCapital: capitalize(betrokkene_type.replace(/_/g, ' ')),
      name,
      role,
      icon: pip_authorized ?
        'check'
        : '',
      source: relatedSubject,
      openInNewTab: itemLink => {
        // Dirty, but window.open(url, target, 'noopener') opens a new window instead of a new tab on Chrome
        let link = document.createElement('a');
        link.target = '_blank';
        link.href = itemLink;
        link.rel = 'noopener';
        document.body.appendChild(link);
        link.click();
        link.parentNode.removeChild(link);
      }
    };
  });

  const actions = [
    {
      name: 'update',
      label: 'Bewerken',
      type: 'click',
      icon: 'pencil',
      click: ( item, event ) => {

        event.stopPropagation();
        event.preventDefault();

        let defaults = seamlessImmutable({
            relation_type: item.source.betrokkene_type,
            related_subject: {
              data: {
                id: item.source.betrokkene_id,
                type: item.source.betrokkene_type
              },
              label: item.source.name
            },
            related_subject_role: item.source.role,
            related_subject_role_freeform: item.source.role,
            magic_string_prefix: item.source.magic_string_prefix,
            pip_authorized: item.source.pip_authorized,
            employee_authorisation: item.source.employee_authorisation,
            notify_subject: false
          }),
          values = defaults,
          magicStringResource =
            createMagicStringResource(
              resource,
              scope,
              ( ) => {
                return {
                  role:
                    values.related_subject_role_freeform === item.source.role ?
                      null
                      : values.related_subject_role_freeform,
                  caseId: currentCase().instance.number
                };
              }
            ),
          formCtrl = relatedSubjectFormCtrl(magicStringResource, roles),
          promise;

        openForm(
          {
            form:
              assign(
                {},
                formCtrl,
                {
                  processChange: ( name, value, vals ) => {

                    values = vals;

                    if (name === 'related_subject_role' && value !== 'Anders') {
                      values = values.merge({ related_subject_role_freeform: value });
                    }

                    if ((name === 'related_subject_role' || name === 'related_subject_role_freeform') && value === item.source.role) {
                      values = values.merge({ magic_string_prefix: item.source.magic_string_prefix });
                    }

                    // todo: clean this up

                    promise = (promise || $timeout(angular.noop, 0))
                      .then(( ) => {
                        return magicStringResource.reload();
                      })
                      .then(( ) => {
                        return magicStringResource.data() ? values.merge( { magic_string_prefix: magicStringResource.data() }) : values;
                      })
                      .catch( ( ) => {
                        return values.merge( { magic_string_prefix: null });
                      })
                      .finally( ( ) => {
                        promise = null;
                      });

                    return promise;

                  },
                  fields: formCtrl.fields.filter(
                    field => field.name !== 'notify_subject'
                      && field.name !== 'relation_type'
                  )
                    .map(field => {

                      return field.name === 'related_subject' || field.name === 'related_subject_type' ?
                        assign({}, field, { disabled: true })
                        : field;
                    })
                }
              ),
            title: 'Betrokkene bewerken',
            defaults
          }
        )
          .then( vals => {

            onRelatedSubjectUpdate({
              $id: item.source.id,
              $values: assign(
                {},
                item.source,
                {
                  magic_string_prefix: vals.magic_string_prefix,
                  role:
                    vals.related_subject_role !== 'Anders' ?
                      vals.related_subject_role
                      : vals.related_subject_role_freeform,
                  notify_subject: !!vals.notify_subject,
                  pip_authorized: !!vals.pip_authorized,
                  employee_authorisation: vals.employee_authorisation,
                }
              )
            });

          })
          .finally( ( ) => {

            magicStringResource.destroy();

          });

      },
      visible: ( item ) => {
        var noEdit = ['Aanvrager', 'Behandelaar'];
        if ( !isDisabled && item.role !== null && !includes(noEdit, item.role)) {
          return true;
        }
      }
    },
    {
      name: 'remove',
      label: 'Verwijderen',
      type: 'click',
      icon: 'close',
      click: ( item, event ) => {

        event.stopPropagation();
        event.preventDefault();

        zsConfirm('Weet u zeker dat u deze betrokkene wilt verwijderen?', 'Verwijderen')
          .then(( ) => {
            onRelatedSubjectRemove({ $id: item.source.relationId });
          });

      },
      visible: ( item ) => {
        var noEdit = ['Aanvrager', 'Behandelaar'];
        if ( !isDisabled && item.role !== null && !includes(noEdit, item.role)) {
          return true;
        }
      }
    }
  ];

  const collectionActions = isDisabled ? [] : [
    {
      type: 'button',
      data: {
        label: 'Voeg toe',
        click: () => {
          contextualActionService.openAction(
            contextualActionService.findActionByName('betrokkene')
          );
        }
      }
    }
  ];

  return {
    name: 'related_subjects',
    label: 'Betrokkenen',
    columns,
    items,
    actions,
    collectionActions
  };
};

export default relatedSubjectsTable;
