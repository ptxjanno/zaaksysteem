import shortid from 'shortid';
import capitalize from 'lodash/capitalize';
import flatten from 'lodash/flatten';
import get from 'lodash/get';
import getRelationLink from './library/getRelationLink';

const parentAndChildCasesColumns = [
  {
    id: 'drag',
    label: '',
    template:
      `<div class="widget-favorite-drag list-item-drag">
        <zs-icon icon-type="drag-vertical"></zs-icon>
      </div>`
  },
  {
    id: 'type',
    label: 'Type',
    template: '<span>{{::item.relation_type_label}}</span>'
  }
];

const relatedCasesColumns = [
  {
    id: 'level',
    label: 'Niveau',
    template: '<span>{{::item.level}}</span>'
  }
];

let defaultCaseColumns =	[
  {
    id: 'number',
    label: 'Nr',
    template: '<span>{{::item.number}}</span>'
  },
  {
    id: 'progress',
    label: 'Voortgang',
    template: '<zs-progress-bar progress="{{::item.progress}}"></zs-progress-bar>'
  },
  {
    id: 'casetype',
    label: 'Zaaktype',
    template: '<span>{{::item.casetype}}</span>'
  },
  {
    id: 'subject',
    label: 'Extra informatie',
    template: '<span>{{::item.subject}}</span>'
  },
  {
    id: 'assignee',
    label: 'Behandelaar',
    template: '<a href="{{::item.assigneeHref}}">{{::item.assigneeName}}</a>'
  },
  {
    id: 'result',
    label: 'Resultaat',
    template: '<span>{{::item.result}}</span>'
  }
];

const relationTypeDict = {
  initiator: 'Initiator',
  continuation: 'Vervolgzaak',
  plain: 'Gerelateerd'
};

export const parentAndChildCasesTable = ($state, parentCase, siblingCases, childCases) => {
  const formatCase = ({
    instance: {	number,	progress_status, casetype,	subject, assignee, result },
    reference,
  }, level) => ({
    id: shortid(),
    level,
    number,
    progress: progress_status,
    casetype: get(casetype, 'instance.name', 'Onbekend'),
    subject: subject || '',
    assigneeHref: `/betrokkene/${get(assignee, 'instance.id')}?gm=1&type=medewerker`,
    assigneeName: get(assignee, 'instance.assignee'),
    result: capitalize(result) || 'Onbekend',
    reference,
    href: $state.href('case', { caseId: number }),
  });

  const groups = parentCase
    ? { 'A': [parentCase], 'B': siblingCases, 'C': childCases}
    : { 'A': siblingCases, 'B': childCases};

  const groupedItems = Object.keys(groups).map(level =>
    groups[level].map(caseObj => formatCase(caseObj, level)));

  const items = flatten(groupedItems);

  return {
    name: 'sub-cases',
    label: 'Hoofd- en deelzaken',
    columns: relatedCasesColumns.concat(defaultCaseColumns),
    items,
    actions: [],
    href: true
  };
};

export const relatedCasesTable = ($state, relatedCases, canRelate, onCaseRelate, onCaseUnrelate, zsConfirm) => {
  const formatCase = ({
    id,
    relationships: {
      other_case: {
        data: {
          attributes: {
            number,
            progress_status,
            casetype_title,
            summary,
            result
          },
          relationships: {
            assignee
          }
        },
        meta: {
          relation_type
        }
      }
    }
  }) => ({
    id: shortid(),
    number,
    progress: progress_status * 100,
    casetype: casetype_title,
    subject: summary,
    assigneeHref: assignee ? getRelationLink('employee', get(assignee, 'data.id')) : null,
    assigneeName:  assignee ? get(assignee, 'meta.display_name') : null,
    result: result ? capitalize(result) : 'Onbekend',
    reference: id,
    href: $state.href('case', { caseId: number }),
    relation_type_label: relationTypeDict[relation_type]
  });

  const actions = canRelate ? [
    {
      name: 'remove',
      type: 'click',
      label: 'Verbreek relatie',
      icon: 'close',
      click: ( item, event ) => {
        event.stopPropagation();
        event.preventDefault();

        zsConfirm('Weet u zeker dat u de relatie met deze zaak wilt verbreken?', 'Verwijderen')
          .then( ( ) => {
            onCaseUnrelate( { $reference: item.reference });
          });
      },
      visible: () => true,
    }
  ] : [];

  const add = {
    type: 'suggest',
    data: {
      type: 'case',
      placeholder: 'Begin te typen',
      onSelect: ( item ) => {
        onCaseRelate({ $reference: item.id });
      }
    }
  };

  return {
    name: 'related-cases',
    label: 'Gerelateerde zaken',
    columns: parentAndChildCasesColumns.concat(defaultCaseColumns),
    items: relatedCases.map(formatCase),
    actions,
    add,
    href: true
  };
};
