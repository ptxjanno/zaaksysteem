import angular from 'angular';
import template from './template.html';
import composedReducerModule from './../../../../shared/api/resource/composedReducer';
import assign from 'lodash/assign';
import capitalize from 'lodash/capitalize';
import get from 'lodash/get';
import shortid from 'shortid';
import isEmpty from './../../../../shared/vorm/util/isEmpty';
import './styles.scss';

export default
	angular.module('zsCaseAboutView', [
		composedReducerModule
	])
		.directive('zsCaseAboutView', [ 'composedReducer', 'dateFilter', ( composedReducer, dateFilter ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					caseResource: '&',
					casetypeResource: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( scope ) {

					let ctrl = this,
						groupReducer;

					groupReducer = composedReducer( { scope }, ctrl.caseResource(), ctrl.casetypeResource())
						.reduce( ( caseObj, casetypeObj ) => {

							const lead_time_legal_type = Object.keys(casetypeObj.instance.properties.lead_time_legal)[0];
							const lead_time_service_type = Object.keys(casetypeObj.instance.properties.lead_time_service)[0];

							return [
								{
									name: 'info',
									label: 'Algemene zaakinformatie',
									fields: [
										{
											label: 'Zaaknummer',
											value: caseObj.instance.number
										},
										{
											label: 'Zaaktype',
											value: caseObj.instance.casetype.instance.name
										},
										{
											label: 'Extra informatie',
											value: caseObj.instance.subject
										},
										{
											label: 'Contactkanaal',
											value: capitalize(caseObj.instance.channel_of_contact)
										},
										{
											label: 'Afdeling',
											value: caseObj.instance.casetype.instance.department
										},
										{
											label: 'Aanvrager',
											value: caseObj.instance.requestor.instance.name
										},
										{
											label: 'Behandelaar',
											value: get(caseObj.instance.assignee, 'instance.assignee')
										},
										{
											label: 'Coordinator',
											value: get(caseObj.instance, 'coordinator.instance.coordinator')
										},
										{
											label: 'Registratiedatum',
											value: dateFilter(caseObj.instance.date_of_registration, 'dd-MM-yyyy')
										},
										{
											label: 'Streefafhandeldatum',
											value: dateFilter(caseObj.instance.date_target, 'dd-MM-yyyy')
										},
										{
											label: 'Afhandeldatum',
											value: caseObj.instance.date_of_completion ?
												dateFilter(caseObj.instance.date_of_completion, 'dd-MM-yyyy')
												: null
										},
										{
											label: 'Afhandeltermijn wettelijk',
											value: `${casetypeObj.instance.properties.lead_time_legal[lead_time_legal_type]} ${lead_time_legal_type}`
										},
										{
											label: 'Afhandeltermijn norm',
											value: `${casetypeObj.instance.properties.lead_time_service[lead_time_service_type]} ${lead_time_service_type}`
										},
										{
											label: 'Archiefnominatie',
											value: caseObj.instance.archival_state
										},
										{
											label: 'Uiterste vernietigingsdatum',
											value: caseObj.instance.date_destruction ?
												dateFilter(caseObj.instance.date_destruction, 'dd-MM-yyyy')
												: null
										},
										{
											label: 'Status online betaling',
											value: caseObj.instance.payment_status ?
                                                caseObj.instance.payment_status.mapped
                                                : null
										},
										{
											label: 'Zaak bedrag',
											value: caseObj.instance.price
										},
										{
											label: 'Wettelijke grondslag',
											value: caseObj.instance.principle_national
										},
										{
											label: 'Lokale grondslag',
											value: caseObj.instance.casetype.instance.principle_local
										},
										{
											label: 'Verantwoordingsrelatie',
											value: caseObj.instance.casetype.instance.supervisor_relation
										},
										{
											label: 'Openbaarheid',
											value: caseObj.instance.confidentiality.mapped
										},
										{
											label: 'Procesbeschrijving',
											value: caseObj.instance.process_description
                                        },
                                        {
											label: 'Correspondentiegegevens bij registratie',
											value:
												[
													caseObj.instance.requestor.instance[
														'requestor_snapshot.name'
													],
													caseObj.instance.requestor.instance[
														'requestor_snapshot.street'
													] +
														' ' +
														caseObj.instance.requestor.instance[
															'requestor_snapshot.residence_house_number'
														],
													caseObj.instance.requestor.instance[
														'requestor_snapshot.residence_zipcode'
													],
													caseObj.instance.requestor.instance[
														'requestor_snapshot.place_of_residence'
													],
													caseObj.instance.requestor.instance[
														'requestor_snapshot.email'
													]
												]
													.filter(Boolean)
													.join('\n') || '-'
										}
									]
								}
							]
								.map(group => {
									return assign(
										{ id: shortid() },
										group,
										{
											fields: group.fields.map(
												field => {
													return assign({ id: shortid() }, field, { value: isEmpty(field.value) ? '-' : field.value });
												}
											)
										}
									);
								});

						});

					ctrl.getGroups = groupReducer.data;

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
