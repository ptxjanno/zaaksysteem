import get from 'lodash/get';
import formFields from '../../zsCaseAddSubject/form';
import merge from 'lodash/merge';
import identity from 'lodash/identity';

export default ( data ) => {

	let capabilities = {
			expand: false,
			edit: false
		};

	return {
		getDefaults: ( ) => {

			return {
				related_subject: get(data, 'name'),
				related_subject_role: get(data, 'role'),
				magic_string_prefix: get(data, 'magic_string_prefix'),
				pip_authorized: get(data, 'is_authorized'),
				notify_subject: get(data, 'email_confirmation'),
				employee_authorisation: get(data, 'employee_authorisation'),
			};

		},
		getCapabilities: ( ) => capabilities,
		fields: ( ) => {

			return formFields().fields
				.filter( ( field ) => {
					return field.name !== 'relation_type' ? field : null;
				})
				.filter( identity )
				.map( ( field ) => {
					return field.name === 'magic_string_prefix' ?
						merge(
							field,
							{ template: 'text' },
							{ disabled: true },
							{ data: null },
							{ required: false }
						)
						: merge(
							field,
							{ disabled: true },
							{ template: 'text' }
						);
				});
		}
	};

};
