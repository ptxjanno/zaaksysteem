import shortid from 'shortid';

export default class CasePhaseSidebarController {

	static get $inject() {
		return ['$scope', '$state', 'rwdService', 'composedReducer'];
	}

	constructor( $scope, $state, rwdService, composedReducer ) {
		let ctrl = this,
			tabReducer;

		ctrl.handleCollapseClick = () => {
			$state.go('case.phase', { tab: null }, { inherit: true });
		};

		ctrl.isCollapsed({
			$getter: () => {
				return ctrl.isSidebarCollapsed();
			}
		});

		ctrl.handleActionAutomaticToggle = ( actionId, automatic ) => {
			ctrl.onActionAutomaticToggle({
				$actionId: actionId,
				$automatic: automatic
			});
		};

		ctrl.handleActionUntaint = ( actionId ) => {
			ctrl.onActionUntaint({ $actionId: actionId });
		};

		ctrl.handleActionTrigger = ( action, trigger, data ) => {
			return ctrl.onActionTrigger({
				$action: action,
				$trigger: trigger,
				$data: data
			});
		};

		ctrl.getSelectedTab = () => {
			let tab = ctrl.tab()
				|| (rwdService.isActive('large-and-up') ? 'acties' : null);

			return tab;
		};

		ctrl.canEditActionList = () => {
			return ctrl.canEdit();
		};

		ctrl.canEditCheckList = () => {
			return ctrl.canEdit();
		};

		ctrl.isSidebarCollapsed = () => {
			return !ctrl.getSelectedTab() && !rwdService.isActive('large-and-up');
		};

		tabReducer = composedReducer({ scope: $scope }, ctrl.actions, ctrl.checklist, ctrl.getSelectedTab, ctrl.phaseState)
			.reduce(( actions, checklist, tab, phaseState ) => {
				let filteredActions =	(actions || []).filter(action => action.type !== 'object_mutation');

				return [
					{
						id: shortid(),
						name: 'acties',
						label: 'Acties',
						count: (filteredActions || []).filter(action => action.automatic).length,
						selected: tab === 'acties',
						icon: 'play-circle-outline',
						link: $state.href($state.current, { tab: 'acties' }, { inherit: true }),
						children: filteredActions,
						condition: true,
					},
					{
						id: shortid(),
						name: 'taken',
						label: 'Taken',
						count: (checklist || []).filter(item => !item.attributes.completed).length,
						selected: tab === 'tasks',
						icon: 'checkbox-multiple-marked-outline',
						link: $state.href($state.current, { tab: 'tasks' }, { inherit: true }),
						children: checklist,
						condition: !phaseState.isFirst
					}
				].filter(tab => tab.condition);
			});

		ctrl.getTabs = tabReducer.data;
	}

}
