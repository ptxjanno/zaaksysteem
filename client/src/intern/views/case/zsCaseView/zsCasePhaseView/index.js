import angular from 'angular';
import immutable from 'seamless-immutable';
import template from './template.html';
import composedReducerModule from './../../../../../shared/api/resource/composedReducer';
import vormFieldsetModule from './../../../../../shared/vorm/vormFieldset';
import attrToVorm from './../../../../../shared/zs/vorm/attrToVorm';
import inputModule from './../../../../../shared/vorm/types/input';
import radioModule from './../../../../../shared/vorm/types/radio';
import selectModule from './../../../../../shared/vorm/types/select';
import textareaModule from './../../../../../shared/vorm/types/textarea';
import fileModule from './../../../../../shared/vorm/types/file';
import checkboxListModule from './../../../../../shared/vorm/types/checkboxList';
import richTextModule from './../../../../../shared/vorm/types/richText';
import mapModule from './../../../../../shared/vorm/types/map';
import calendarModule from './../../../../../shared/vorm/types/calendar';
import appointmentModule from './../../../../../shared/vorm/types/appointment';
import textblockModule from './../../../../../shared/vorm/types/textblock';
import vormObjectSuggestModule from './../../../../../shared/object/vormObjectSuggest';
import zsCasePhaseSidebarModule from './zsCasePhaseSidebar';
import zsCasePhaseNavModule from './zsCasePhaseNav';
import zsCaseResultModule from './zsCaseResult';
import snackbarServiceModule from './../../../../../shared/ui/zsSnackbar/snackbarService';
import oneWayBind from './../../../../../shared/util/oneWayBind';
import ruleEngine from './../../../../../shared/zs/case/ruleEngine';
import validateAttributes from './../../../../../shared/zs/case/validateAttributes';
import validationMessages from '../../../../../shared/vorm/util/vormValidator/messages';
import zsBtwDisplayModule from './../../../../../shared/ui/zsBtwDisplay';
import zsPendingChangeModule from './../../../../../shared/case/zsPendingChange';
import zsCaseObjectMutationListModule from './zsCaseObjectMutationList';
import zsModalModule from './../../../../../shared/ui/zsModal';
import zsPdfViewerModule from './../../../../../shared/ui/zsPdfViewer';
import caseAttrTemplateCompilerModule from './../../../../../shared/case/caseAttrTemplateCompiler';
import windowUnloadModule from './../../../../../shared/util/windowUnload';
import zsConfirmModule from './../../../../../shared/ui/zsConfirm';
import zsTruncateHtmlModule from './../../../../../shared/ui/zsTruncate/zsTruncateHtml';
import zsCaseAttributeUpdateRequestModule from './zsCaseAttributeUpdateRequest';
import find from 'lodash/find';
import get from 'lodash/get';
import mapValues from 'lodash/mapValues';
import mapKeys from 'lodash/mapKeys';
import keyBy from 'lodash/keyBy';
import findIndex from 'lodash/findIndex';
import filter from 'lodash/filter';
import merge from 'lodash/merge';
import pickBy from 'lodash/pickBy';
import assign from 'lodash/assign';
import identity from 'lodash/identity';
import isArray from 'lodash/isArray';
import first from 'lodash/head';
import last from 'lodash/last';
import every from 'lodash/every';
import uniq from 'lodash/uniq';
import values from 'lodash/values';
import map from 'lodash/map';
import difference from 'lodash/difference';
import isEqual from 'lodash/isEqual';
import isEmpty from 'lodash/isEmpty';
import partition from 'lodash/partition';
import compact from 'lodash/compact';
import includes from 'lodash/includes';
import debounce from 'lodash/debounce';
import sortBy from 'lodash/sortBy';
import flatten from 'lodash/flatten';
import isOptionVisible from './isOptionVisible';
import intersection from 'lodash/intersection';
import shortid from 'shortid';
import actionsModule from './../../actions';
import normalizeCaseValue from './normalizeCaseValue';
import normalizePhaseName from './normalizePhaseName';
import getGroupsFromPhase from './getGroupsFromPhase';
import './styles.scss';

export default angular
	.module('zsCasePhaseView', [
		vormFieldsetModule,
		composedReducerModule,
		inputModule,
		radioModule,
		selectModule,
		textareaModule,
		checkboxListModule,
		fileModule,
		richTextModule,
		mapModule,
		calendarModule,
		appointmentModule,
		textblockModule,
		vormObjectSuggestModule,
		zsCasePhaseSidebarModule,
		zsCasePhaseNavModule,
		zsCaseResultModule,
		zsBtwDisplayModule,
		zsPendingChangeModule,
		zsCaseObjectMutationListModule,
		zsModalModule,
		zsPdfViewerModule,
		snackbarServiceModule,
		caseAttrTemplateCompilerModule,
		windowUnloadModule,
		zsConfirmModule,
		actionsModule,
		zsTruncateHtmlModule,
		zsCaseAttributeUpdateRequestModule
	])
	.directive('zsCasePhaseView', [
		'$q', '$window', '$state', '$timeout', '$animate', '$compile', '$sce',
		'snackbarService', 'composedReducer', 'zsModal', 'caseAttrTemplateCompiler', 'windowUnload', 'zsConfirm',
		(
			$q, $window, $state, $timeout, $animate, $compile, $sce,
			snackbarService, composedReducer, zsModal, caseAttrTemplateCompiler, windowUnload, zsConfirm
		) => {
			return {
				restrict: 'E',
				template,
				scope: {
					casetype: '&',
					case: '&',
					caseId: '&',
					caseResource: '&',
					caseActionResource: '&',
					caseActions: '&',
					caseActionsLoading: '&',
					checklistResource: '&',
					checklists: '&',
					checklistsLoading: '&',
					mutationResource: '&',
					mutations: '&',
					mutationsLoading: '&',
					phaseName: '&',
					rules: '&',
					tab: '&',
					templates: '&',
					caseDocuments: '&',
					canAdvance: '&',
					canEdit: '&',
					requestor: '&',
					user: '&',
					isResolved: '&',
					onPhaseAdvance: '&'
				},
				bindToController: true,
				controller: ['$scope', '$element', function ( $scope, $element ) {
					let ctrl = this,
						phaseReducer,
						caseActionsReducer,
						checklistsReducer,
						activePhaseNameReducer,
						selectedPhaseReducer,
						phaseLockedReducer,
						permissionsReducer,
						fieldReducer,
						fieldsetReducer,
						ruleResultReducer,
						valueReducer,
						caseValueReducer,
						advanceButtonReducer,
						validityReducer,
						isValidReducer,
						validityMessagesReducer,
						queueAttributeChangesReducer,
						fieldStyleReducer,
						phaseStateReducer,
						changes = immutable([]),
						queuedSave,
						modelOptions = {
							debounce: {
								default: 250,
								click: 0,
								blur: 0
							},
							updateOn: 'default blur click select'
						},
						queueChangesReducer,
						pendingChangesReducer,
						userUnlocked = false,
						unlockedFields = immutable({}),
						isAdvancing = false,
						showRuleInfluence = false;

					let getCaseId = () => get(ctrl.case().data(), 'instance.number');
					let getCaseUuid = () => get(ctrl.case().data(), 'reference');

					let isLastPhase = () => {
						return get(selectedPhaseReducer.data(), 'seq', 0) === get(phaseReducer.data(), 'length');
					};

					let getResult = () => {
						let caseResult = get(ctrl.case().data(), 'instance.result_id'),
							ruleResult = get(ruleResultReducer.data(), 'values["case.result_id"]', caseResult);
							
						return ruleResult;
					};

					let getRuleValues = ( fieldsByAttrName, caseObj, changedValues ) => {

						let caseValues = mapValues(
							assign({}, caseObj.instance.attributes, changedValues),
							( value, key ) => {
								let field = fieldsByAttrName[key],
									val = value;

								if (field
									&& (field.limit === 1 && isArray(val))
									&& field.$attribute.type !== 'checkbox'
								) {
									val = first(val);
								}

								return val;
							}
							),
							ruleValues =
								merge(
									mapKeys(
										caseValues,
										( value, key ) => `attribute.${key}`
									),
									...(['casetype', 'requestor'])
										.map(
											( instanceKey ) => {
												return mapKeys(
													caseObj.instance[instanceKey].instance,
													( value, key ) => `case.${instanceKey}.${key}`
												);
											}
										),
									{
										'case.channel_of_contact': caseObj.instance.channel_of_contact,
										'case.payment_status': get(caseObj.instance.payment_status, 'original'),
										'case.confidentiality': caseObj.instance.confidentiality.original
									}
								);

						return ruleValues;
					};

					let getRuleResult = ( rules, ruleValues, options ) => ruleEngine(rules, ruleValues, options);

					let saveValidChanges = () => {

						let updates,
							promise;

						if (queueChangesReducer.data() && !queueAttributeChangesReducer.data().length) {
							return $q.when(true);
						}

						[updates, changes] = partition(queueAttributeChangesReducer.data(), change => {
							let validations = validityReducer.data()[change.name],
								isValid =
									// save empty values
									(validations || []).filter(validation => {
										let errors = validation ? Object.keys(validation) : [];

										return errors.length > 0 && !(errors.length === 1 && errors[0] === 'required');
									}).length === 0;

							if (change.attribute.type === 'file') {
								return false;
							}

							return isValid;
						});

						changes = immutable(changes);

						if (updates.length) {
							promise =
								ctrl.case().mutate(
									'CASE_ATTRIBUTE_UPDATE',
									{
										caseId: get(ctrl.case().data(), 'instance.number'),
										updates,
										phaseId: get(selectedPhaseReducer.data(), 'id')
									}
								)
								.asPromise()
								.then(() => ctrl.caseActionResource().reload());
						}

						return promise || $q.when(true);
					};

					let invalidateChanges = debounce(() => {
						$scope.$evalAsync(() => {
							queuedSave = $q.when(queuedSave)
								.finally(saveValidChanges);
						});
					}, 5000, {
						leading: true,
						trailing: true
					});

					let getPhaseInfo = ( phases, activePhaseName, selectedPhases ) => {
						let indexOfActivePhase = findIndex(phases, ( phase ) => normalizePhaseName(phase.name) === activePhaseName),
							indexOfPhase = phases.indexOf(selectedPhases),
							active = indexOfPhase === indexOfActivePhase,
							phaseResolved = indexOfPhase < indexOfActivePhase || indexOfActivePhase === -1,
							isLast = indexOfPhase === phases.length - 1;

						return {
							active,
							phaseResolved,
							last: isLast
						};
					};

					const isPdfSupported = () => {
						const getActiveXObject = (name) => {
							try {
								return new window.ActiveXObject(name);
							} catch(e) {
								// allow failures
							}
						};
			
						const hasAcrobatInstalled = getActiveXObject('AcroPDF.PDF') || getActiveXObject('PDF.PdfCtrl');
						const isIos = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
						
						return navigator.mimeTypes['application/pdf'] || hasAcrobatInstalled || !isIos;
                    };

					phaseReducer = composedReducer({ scope: $scope }, ctrl.case(), ctrl.casetype())
						.reduce(( caseObject, casetype ) => {
							/*
								### deadlines ###
								desired format: 'X of Y' (X = days spent, Y = acceptable amount of days)
								the Y comes along with the other phases-data from the casetypeObject
								when the phase deadline is 0 we assume the feature is disabled

								the X will be added to this data
									from deadline_timeline for the previous phases
									from current_deadline for the current phase
									set as 0 for the upcoming phases

								the first phase has no deadline

								note:	a user can put a case in a previous phase
											this will cause the deadline_timeline to contain duplicates
											we therefore reverse the timeline, to easily find the most recent item

								note: a user can copy a case
											this will cause the copy to have an empty timeline
											because there is no data on the previous phases,
											we do not display the deadlines for those phases

								note: a user can copy a case that is in phase 3+
											the copy will be in phase 3+, but the current_deadline will be for phase 2
											when verified that the data is invalid, we hide all deadline info

											the current_timeline will be valid again after advancing to the next phase
											but now the previous incorrect data is added to the timeline
											we therefore hide all deadline info, with an exception for when
												the latest item in the timeline is for the phase previous to the current phase

								note: a user can change the casetype
											the deadline_timeline info will persist and the data is therefore invalid
											phase.id's are (presumed) unique across casetypes,
											so when using phase.id's (rather than phase.seq) the problem solves itself
											and the deadline info is hidden for those phases

											when changing casetype to the same casetype it'd be ok to display the deadlines
											when changing a casetype from phase 3+, the same issue occurs as with copy
											however, we could make an exception for when casetypes are changed from phase 2
											we therefore hide all deadline info, with an exception for when
												the latest item in the timeline is for the same phase as the current phase
							*/

							const phases = get(casetype, 'instance.phases');
							const currentDeadline = get(caseObject, 'instance.current_deadline');
							const currentPhaseTitle = get(caseObject, 'instance.phase');
							const currentPhaseNumber = findIndex(phases, phase => phase.name === currentPhaseTitle + 1);
							const currentDeadlinePhaseNumber = get(currentDeadline, 'phase_no');

							const deadlineTimeline = get(caseObject, 'instance.deadline_timeline', []);
							const deadlineTimelineDesc = [].concat(deadlineTimeline).reverse();
							const latestPhaseInTimeline = get(deadlineTimelineDesc, '[0].phase_no');

							const currentDeadlineInvalid = currentPhaseNumber !== currentDeadlinePhaseNumber;
							const itemInTimelineIsSame = currentPhaseNumber === latestPhaseInTimeline;
							const itemInTimelineIsPrevious = currentPhaseNumber - 1 === latestPhaseInTimeline;
							const phaseIsTwoAndTimelineIsEmpty = currentPhaseNumber === 2 && typeof latestPhaseInTimeline === 'undefined';
							const isDeadlineDataInvalid =	currentDeadlineInvalid ||
								!(itemInTimelineIsSame || itemInTimelineIsPrevious || phaseIsTwoAndTimelineIsEmpty);

							return phases.map(phase => {
								let days;

								if(!phase.deadline || isDeadlineDataInvalid || phase.seq === 1) {
									days = null;
								} else if (phase.seq < currentDeadlinePhaseNumber ) {
									const timelineItem = deadlineTimelineDesc.find(item => item.phase_id === phase.id);

									days = get(timelineItem, 'current', null);
								} else if (phase.seq === currentDeadlinePhaseNumber) {
									days = get(currentDeadline, 'current');
								} else {
									days = 0;
								};

								return assign(
									{},
									phase,
									{	days }
								);
							});
						});

					activePhaseNameReducer = composedReducer({ scope: $scope }, phaseReducer, ctrl.case())
						.reduce(( phases, caseObj ) => {
							let activePhaseName = normalizePhaseName((get(caseObj, 'instance[\'phase\']') || ''));

							return normalizePhaseName(
								get(
									find(phases, ( phase ) => {
										return normalizePhaseName(phase.name) === activePhaseName;
									}),
									'name',
									''
								)
							);
						});

					selectedPhaseReducer = composedReducer({ scope: $scope }, phaseReducer, ctrl.phaseName)
						.reduce(( phases, selectedPhaseName ) => {
							return find(phases, ( p ) => normalizePhaseName(p.name) === normalizePhaseName(selectedPhaseName));
						});

					phaseLockedReducer = composedReducer({ scope: $scope }, selectedPhaseReducer, () => userUnlocked)
						.reduce(( selectedPhase, unlocked ) => {
							let isPhaseLocked = !unlocked && (selectedPhase && selectedPhase.locked);

							return isPhaseLocked;

						});

					queueChangesReducer = composedReducer({ scope: $scope }, ctrl.casetype(), ctrl.case(), ctrl.user, ctrl.canEdit)
						.reduce(( casetype, caseObj, user, canEdit ) => {
							// we need to determine per attribute whether queue is enabled for not
							let assignee = caseObj.instance.assignee,
								queue =
									!(includes(user.system_roles, 'Administrator') || includes(user.system_roles, 'Zaaksysteembeheerder'))
									&& get(casetype, 'instance.queue_coworker_changes')
									&& canEdit
									&& user.id !== get(assignee, 'instance.id');

							return queue;
						});

					permissionsReducer = composedReducer({ scope: $scope }, selectedPhaseReducer, ctrl.user(), ctrl.canEdit)
						.reduce(( selectedPhase, user, canEdit ) => {
							let isAdmin = includes(user.system_roles, 'Administrator')
								|| includes(user.system_roles, 'Zaaksysteembeheerder'),
								groups = uniq(user.legacy.parent_ou_ids.concat(user.legacy.ou_id)),
								roles = user.legacy.role_ids,
								messages =
									mapValues(
										keyBy(
											selectedPhase.fields.filter(field => !(field.is_system || field.is_group)),
											'magic_string'
										),
										attribute => {
											let applicable,
												editable;

											let getRolesText = ( permissions ) => {

												return `<ul>
													${permissions.map(
													permission => {
														return `<li><b>Afdeling</b>: ${permission.group.instance.name}<br/><b>Rol</b>: ${permission.role.instance.name}</li>`;
													}
												).join('')}
												</ul>`;
											};

											if (!canEdit) {
												return {
													editable: false
												};
											}


											if (get(attribute, 'permissions.length', 0) === 0) {
												return {
													editable: true
												};
											}

											applicable = attribute.permissions.filter(
												permission => {
													return includes(groups, permission.group.instance.id)
														&& includes(roles, permission.role.instance.id);
												}
											);

											editable = !!(isAdmin || applicable.length);

											return {
												editable,
												message:
													editable ?
														`U heeft rechten om dit kenmerk te wijzigen, omdat u in één of meer van de volgende rollen zit:${getRolesText(attribute.permissions)}`
														: `Dit kenmerk kan alleen gewijzigd worden door de volgende rollen:${getRolesText(attribute.permissions)}`
											};
										}
									);

							return messages;
						});

					queueAttributeChangesReducer = composedReducer({ scope: $scope }, permissionsReducer, queueChangesReducer, () => changes)
						.reduce(( permissions, queueEnabled, changedAttributes ) => {
							// If the queue is enabled, then filter the attributes that are changed based on whether the
							// skip change approval attribute is set and the user has the required permissions to change
							// it.

							// The changes that are filtered out are changes that need to be proposed by the user to
							// the case owner through the queue changes mechanism. Those are saved separately by
							// ctrl.handleAttributeUpdateRequest in this file.
							return queueEnabled ?
								filter(changedAttributes, ( attribute ) => {
									if (Number(get(attribute, 'attribute.properties.skip_change_approval', '0')) && permissions[attribute.attribute.magic_string].editable) {
										return true;
									}
									return false;
								})
								: changedAttributes;
						});

					function attributesByPhase( selectedPhase, caseId, requestor, debugRules ) {
						const attrs = get(selectedPhase, 'fields', []);

						const fields = keyBy(
							attrs
								.filter(field => !field.is_group && (debugRules || !field.is_system))
								.map(attr => {
										let config = attrToVorm(attr),
											type = attr.type;

										switch (type) {
										case 'geolatlon':
											config = merge(
												{},
												config,
												{
													data: {
														onFeaturesSelect: ( features ) => {

															let attributeId = get(attr, 'properties.map_wms_feature_attribute_id'),
																attribute = find(attrs, { catalogue_id: attributeId }),
																val;

															if (attribute) {

																if (features.length) {
																	val = last(features);
																} else {
																	val = null;
																}

																ctrl.handleChange(attribute.magic_string, val);
															}
														}
													}
												}
											);
											break;

										case 'file':
											config = merge(
												{},
												config,
												{
													data: {
														getDownloadUrl: ( file ) => {
															return file && file.uuid ?
																`/zaak/${caseId}/document/${file.uuid}/download`
																: null;
														},
														click: ( file ) => {
															const baseUrl = `/zaak/${caseId}/document/${file.uuid}/download`;
															const url = `${baseUrl}/pdf`;
															//support old model view for IE11 and iPad (document attribute)
															const modalContent = isPdfSupported()
																? `<iframe id="document-viewer" data-ng-src="${url}" width="100%" height="100%" allowfullscreen webkitallowfullscreen></iframe>`
																: `<zs-pdf-viewer url=${url}></zs-pdf-viewer>`;

															const modalScope = $scope.$new();
															const modal = zsModal({
																title: `Bekijk ${file.filename}`,
																downloadUrl: baseUrl,
																el:
																	$compile(modalContent)(modalScope),
																classes: 'full-screen-modal preview-modal'
															});

															modal.open();
															modal.onClose(() => {
																modalScope.$destroy();
															});

														},
														target: `/zaak/${caseId}/upload`,
														data: ['$file', ( file ) => {
															return {
																action: 'upload',
																fileToUpload: file,
																file_id: `kenmerk_id_${get(attr, 'catalogue_id')}`
															};

														}]
													}
												}
											);
											break;

										case 'calendar':
										case 'calendar_supersaas':
											config = merge(
												{},
												config,
												{
													data: {
														provider: {
															requestor: requestor.instance.id
														}
													}
												}
											);
											break;

										case 'appointment':
											config = merge(
												{},
												config,
												{
													data: {
														provider: {
															requestor: requestor.instance.uuid,
															appointmentInterfaceUuid: attr.properties.appointment_interface_uuid,
															locationId: attr.properties.location_id,
															productId: attr.properties.product_id
														}
													}
												}
											);
											break;

										case 'text_block':
											config = merge(
												{},
												config,
												{
													name: `text_block_${config.id}`
												}
											);
											break;
										}

										return config;
									}
								)
								.filter(identity),
							'name'
						);

						return fields;
					}

					const allFieldReducer = composedReducer({ scope: $scope }, ctrl.case(), ctrl.requestor, phaseReducer, () => showRuleInfluence)
						.reduce(( caseObj, requestor, phases, ruleDebug ) => {
							const fieldsByPhase = phases.map(
								(phase) => attributesByPhase(phase, caseObj.instance.number, requestor, ruleDebug)
							);

							return fieldsByPhase;
						});

					fieldReducer = composedReducer({ scope: $scope }, selectedPhaseReducer, allFieldReducer)
						.reduce((selectedPhase, allFields) => {
							return allFields[selectedPhase.seq - 1];
						});

					ruleResultReducer = composedReducer(
						{
							scope: $scope
						},
						fieldReducer,
						ctrl.case(),
						ctrl.rules(),
						() => changes,
						() => showRuleInfluence
					)
						.reduce(( fieldsByAttrName, caseObj, rules, userChanges, debugRules ) => {
							let ruleValues = getRuleValues(
								fieldsByAttrName,
								caseObj,
								assign(
									{},
									...userChanges.map(change => {
										return assign(
											{ [change.name]: change.value },
											change.implied
										);
									})
								)
								),
								ruleResult;

							// needs to be mutable because rule engine doesn't create new
							// objects because of performance concerns
							ruleResult = getRuleResult(
								immutable(rules).asMutable({ deep: true }),
								immutable(ruleValues).asMutable(),
								{ debug: debugRules }
							);

							return ruleResult;
						});

					caseValueReducer = composedReducer({ scope: $scope }, ctrl.case(), ctrl.mutations, ruleResultReducer, fieldReducer, () => changes)
						.reduce(( caseObj, mutations, ruleResult, fieldsById, userChanges ) => {
							let textBlocks = mapValues(
								pickBy(fieldsById, field => (field.type === 'text_block')),
								value => value.$attribute.properties.text_content
							);

							let fieldValues =
								assign(
									{},
									caseObj.instance.attributes,
									textBlocks,
									...userChanges.map(change => {
										return assign(
											{ [change.name]: change.value },
											change.implied
										);
									})
								);

							assign(
								fieldValues,
								mapKeys(
									mutations,
									( mutations, type ) => {
										return `object.${type}`;
									}
								)
							);

							return fieldValues;
						});

					valueReducer = composedReducer({ scope: $scope }, caseValueReducer, fieldReducer)
						.reduce(( caseValues, fieldsById ) => {
							let fieldValues;

							let formatValue = ( field, source ) => {
								return field.formatters.reduce(( current, reducer ) => {
									return reducer(current);
								}, source);
							};

							fieldValues = mapValues(
								assign(
									{},
									caseValues
								),
								( value, key ) => {
									let field = fieldsById[key],
										attribute,
										val = value;

									if (field && field.type !== 'object') {
										attribute = field.$attribute;

										if (field.limit === 1
											&& isArray(val)
											&& (attribute.type !== 'checkbox')
										) {
											val = first(val);
										}

										if (field.formatters) {
											val = (isArray(val) && (attribute.type !== 'checkbox')) ?
												val.map(source => formatValue(field, source))
												: formatValue(field, val);
										}
									}

									return val;
								}
							);

							return fieldValues;
						});

					validityReducer = composedReducer({ scope: $scope }, ctrl.case(), ctrl.mutations, ctrl.checklists, selectedPhaseReducer, fieldReducer, valueReducer, isLastPhase, getResult, ruleResultReducer)
						.reduce(( caseObj, mutations, checklists, selectedPhase, fieldsById, fieldValues, lastPhase, result, ruleResult ) => {
							let validations =
									validateAttributes(
										mapValues(
											pickBy(fieldsById, ( field, key ) => !ruleResult.hidden[`attribute.${key}`] && field.type !== 'object'),
											( field ) => field.$attribute
										),
										fieldValues,
										validationMessages
									),
								resultValid = !lastPhase || !!result,
								assigned = caseObj.instance.assignee,
								coordinator = caseObj.instance.coordinator,
								checklistValid =
									(get(checklists, selectedPhase.seq, [])
										.filter(item => item.attributes.completed === false).length === 0),
                communicationValid = !Boolean(caseObj.instance.num_unread_communication),
								acceptedFilesValid = (caseObj.instance.num_unaccepted_files === 0),
								pendingChangesValid = (values(caseObj.instance.pending_changes).length === 0),
								mutationsValid =
									filter(selectedPhase.fields, { type: 'object' })
										.filter(field =>
											(get(mutations, field.object_type_prefix, []))
												.filter(mutation => !mutation.complete)
													.length > 0
										).length === 0;
							const caseStatusValid = caseObj.instance.status !== 'stalled';

							assign(
								validations,
								mapValues(
									{
										$result: !resultValid,
										$assigned: !assigned,
										$coordinator: !coordinator,
										$unacceptedFiles: !acceptedFilesValid,
										$pendingChanges: !pendingChangesValid,
										$mutations: !mutationsValid,
										$status: !caseStatusValid,
										$taken: !checklistValid,
										$unreadCommunication: !communicationValid
									},
									value => {
										return value
											? [{ required: 'Dit onderdeel is verplicht' }]
											: [];
									}
								)
							);

							return validations;
						});

					isValidReducer = composedReducer({ scope: $scope }, validityReducer)
						.reduce(validity => {
							let isValid = every(validity, ( validations ) => {
								return validations.filter(identity).length === 0;
							});

							return isValid;
						});

					advanceButtonReducer = composedReducer({ scope: $scope }, selectedPhaseReducer, activePhaseNameReducer, phaseReducer, isValidReducer, ctrl.canAdvance, ctrl.isResolved, () => isAdvancing)
						.reduce(( selectedPhase, activePhaseName, phases, isValid, canAdvance, caseResolved, advancing ) => {
							let indexOfActivePhase = findIndex(phases, ( phase ) => normalizePhaseName(phase.name) === activePhaseName),
								indexOfPhase = phases.indexOf(selectedPhase),
								resolved = caseResolved,
								isActivePhase = indexOfPhase === indexOfActivePhase,
								isPhaseResolved = indexOfPhase < indexOfActivePhase || indexOfActivePhase === -1,
								advancable = isValid && canAdvance && !resolved && isActivePhase && !advancing,
								valid = isValid,
								isLast = indexOfPhase === phases.length - 1,
								label;

							if (advancing) {
								label = 'Fase wordt afgerond';
							} else if (advancable) {
								label = isLast ? 'Afhandelen' : 'Fase afronden';
							} else if (resolved && isLast && valid) {
								label = 'Afgehandeld';
							} else if (resolved && isLast && !valid) {
								label = 'Afgehandeld, maar incompleet';
							} else if (!valid && !isPhaseResolved) {
								label = 'Fase incompleet';
							} else if (!valid && isPhaseResolved) {
								label = 'Fase afgerond, maar incompleet';
							} else if (valid && isPhaseResolved) {
								label = 'Fase afgerond';
							} else if (valid && !isPhaseResolved) {
								label = 'Fase compleet';
							}

							return {
								id: shortid(),
								label,
								disabled: !advancable,
								classes: {
									resolved: isPhaseResolved,
									last: isLast,
									invalid: !isValid,
									valid: isValid
								},
								visible: resolved || isActivePhase,
								icon:
									advancable ?
										'arrow-right'
										: (valid ?
										(advancing ? 'timelapse' : 'check')
										: 'alert-circle'),
								reference: angular.element($element[0].querySelector('.phase-advance-button'))
							};
						});

					validityMessagesReducer = composedReducer({ scope: $scope }, isValidReducer, validityReducer, fieldReducer)
						.reduce(( isValid, validations, fieldsById ) => {
							let defaults = {
									'$result': {
										icon: 'briefcase-check',
										label: 'Resultaat',
										click: () => {
											ctrl.scrollTo('case.result');
										}
									},
									'$unacceptedFiles': {
										icon: 'file',
										label: 'Documenten in de wachtrij',
										link: $state.href('case.docs')
									},
									'$pendingChanges': {
										icon: 'comment-question-outline',
										label: 'Ongeaccepteerde wijzigingen'
									},
									'$taken': {
										icon: 'checkbox-marked-outline',
										label: 'Taken',
										link: $state.href($state.current, { tab: 'tasks' }, { inherit: true })
									},
									'$unreadCommunication': {
										icon: 'message-text-outline',
                                        label: 'Communicatie',
                                        link: $state.href('case.communication')
									},
									'$mutations': {
										icon: 'hexagon',
										label: 'Objectmutaties'
									},
									'$assigned': {
										icon: 'account',
										label: 'Behandelaar'
									},
									'$coordinator': {
										icon: 'account',
										label: 'Coördinator'
									},
									'$status': {
										icon: 'pause',
										label: 'Zaak is opgeschort',
									}
								},
								messages = isValid ?
									null
									: mapValues(
										pickBy(
											validations,
											valueValidations => compact(valueValidations).length
										),
										( valueValidations, key ) => {

											let opts = defaults[key];

											if (!opts) {
												opts = {
													label: fieldsById[key] ?
														fieldsById[key].label
														: null,
													click: () => {
														ctrl.scrollTo(key);
													}
												};
											}

											opts.name = key;

											return opts;
										}
									);

							return sortBy(
								messages,
								( value ) => {
									return value.name.indexOf('$') === 0 ? 0 : 1;
								}
							).filter(identity).map(
								message => {
									let type = message.click ? 'button' : 'link';

									return assign({
										type,
										style: {
											disabled: type === 'link' && !message.link,
											'has-icon': !!message.icon
										}
									}, message);
								}
							);

						});

					fieldStyleReducer = composedReducer({ scope: $scope }, fieldReducer, validityReducer, valueReducer)
						.reduce(( fieldsById, validations, vals ) => {
							let styles = mapValues(
								fieldsById,
								( field, key ) => {

									let valid = compact(validations[key]).length === 0,
										value = vals[key],
										empty = false;

									if (value === null
										|| value === undefined
										|| isArray(value) && (value.length === 0 || compact(value).length === 0)
									) {
										empty = true;
									}

									return {
										required: field.required,
										[`attribute-type-${field.$attribute.type}`]: true,
										empty,
										valid,
										invalid: !valid,
										multiple: field.limit === -1,
										// special class for styling ease of use
										'object-suggest-multiple': field.template === 'object-suggest' && field.limit === -1
									};
								}
							);

							return styles;
						});

					pendingChangesReducer = composedReducer({ scope: $scope }, ctrl.case(), allFieldReducer)
						.reduce(( caseObj, allFieldsByPhase ) => {
							const fieldsById = merge({}, ...allFieldsByPhase);

							let pendingChanges = caseObj.instance.pending_changes || {},
								indexed =
									mapValues(fieldsById, ( field/*, name*/ ) => {

										let attribute = field.$attribute;

										return pendingChanges[attribute.catalogue_id];

									});

							return indexed;
						});

					fieldsetReducer = composedReducer({ scope: $scope }, ctrl.case(), caseValueReducer, selectedPhaseReducer, fieldReducer, ruleResultReducer, validityReducer, fieldStyleReducer, ctrl.canEdit, permissionsReducer, phaseLockedReducer, queueChangesReducer, () => unlockedFields, valueReducer, pendingChangesReducer, () => showRuleInfluence)
						.reduce(( caseObj, caseValues, selectedPhase, fieldsById, ruleResult, validations, fieldStyle, canEdit, permissions, isPhaseLocked, queueChanges, fieldsUnlocked, phaseValues, pendingChanges, debugRules ) => {
							let groups = getGroupsFromPhase(selectedPhase),
								fieldsets;

							fieldsets = groups.map(group => {

								let hasParent = !!caseObj.instance.number_parent;

								return assign(
									{},
									group,
									{
										fields:
											immutable(group.fields)
												.map(attr => {

													let key;

													switch (attr.type) {
													case 'object':
														key = `object.${attr.object_type_prefix.toLowerCase()}`;
														break;

													case 'text_block':
														key = `text_block_${attr.id}`;
														break;

													default:
														key = attr.magic_string;
													}

													return fieldsById[key];

												})
												.filter(field => {
													return field
														&& (ruleResult.hidden[`attribute.${field.name}`] !== true && ruleResult.hidden[`${field.name}`] !== true || debugRules);
												})
												.map(field => {

													let disabled,
														editable,
														queuable,
														unlocked,
														validity,
														skipQueable = !!(Number(get(field, '$attribute.properties.skip_change_approval')) && permissions[field.name].editable),
														system = field.$attribute.is_system,
														hidden = system || ruleResult.hidden[`attribute.${field.name}`] || ruleResult.hidden[`${field.name}`],
														value,
														style,
														permissionMessage,
														pendingChangesForField,
														merged = field,
														icons,
														hideLabel = !!(field.type === 'text_block');

													editable =
														canEdit
														&& (!hasParent || !field.$attribute.referential)
														&& (!permissions[field.name] || permissions[field.name].editable)
														&& !ruleResult.disabled[`attribute.${field.name}`];

													disabled =
														!!(!editable
															|| isPhaseLocked
															|| hidden
															|| system
															|| (queueChanges && !fieldsUnlocked[field.name] && !skipQueable)
														);

													queuable = !!(queueChanges && editable && !skipQueable);

													unlocked = !!(queueChanges && fieldsUnlocked[field.name]);

													validity = validations[field.name];

													value = phaseValues[field.name];

													style = fieldStyle[field.name];

													if (debugRules) {
														style.hidden = hidden;
														style.system = system;
													}

													permissionMessage = permissions[field.name] && permissions[field.name].message;

													pendingChangesForField = pendingChanges && pendingChanges[field.name];

													if (field.data && field.data.options) {

														merged = field.merge({
															data:
																field.data.merge({
																	options:
																		field.data.options.filter(
																			opt => isOptionVisible(opt, value)
																		)
																})
														});
													}

													icons = [];

													if (permissionMessage) {
														icons = icons.concat({
															type: editable ? 'lock-open' : 'lock',
															classes: {
																'vorm-field-item-extra-permissions': true
															},
															tooltip: permissionMessage
														});
													}

													if (debugRules) {

														let tooltip = 'Er zijn geen regels van kracht op dit kenmerk',
															info = ruleResult.debug[`attribute.${field.name}`],
															isFile = field.$attribute.type === 'file',
															savedValue = isFile ? null : normalizeCaseValue(caseValues[field.name]),
															ruleValue = isFile ? null : normalizeCaseValue(ruleResult.values[`attribute.${field.name}`]);

														if (info) {
															tooltip = map(info, ( obj ) => {

																let icon = obj.rule.matches ?
																	'check'
																	: 'close',
																	label = obj.rule.label || '(Geen naam)';

																return `<i class="mdi mdi-${icon}"></i> ${label}:<br/><em>${obj.type}</em>`;

															}).join('<br/>');
														}

														icons = icons.concat({
															type: 'lightbulb',
															classes: {
																'vorm-field-item-extra-rule-debug': true
															},
															options: {
																sticky: true
															},
															tooltip
														});

														if (!isEqual(
																savedValue,
																ruleValue
															)) {
															icons = immutable([
																{
																	type: 'alert-circle',
																	classes: {
																		'vorm-field-item-extra-rule-error': true,
																		error: true
																	},
																	tooltip: `De berekende regelwaarde is niet gelijk aan de opgeslagen waarde:<br/>
																		Opgeslagen: ${savedValue.join(',')}<br/>
																		Verwacht: ${ruleValue.join(',')}<br/>`
																}
															]).concat(icons);
														}

													}

													icons = icons.concat(
														{
															type: 'puzzle',
															classes: {
																'vorm-field-item-extra-name': true
															},
															tooltip: field.name
														}
													);

													icons = immutable(icons).map(
														icon => {

															return icon.merge({
																classes: {
																	'vorm-field-item-extra-icon': true
																},
																options: {
																	attachment: 'left middle',
																	target: 'right middle',
																	offset: {
																		x: 10,
																		y: 0
																	}
																}
															}, { deep: true });
														}
													);

													merged = merged.merge({
														disabled,
														editable,
														unlocked,
														queuable,
														validity,
														style,
														value,
														icons,
														hideLabel,
														pendingChanges: [pendingChangesForField].filter(identity)
													});

													return merged;

												})
												.asMutable({ deep: true })
									}
								);

							});

							return fieldsets.filter(fieldset => fieldset.fields.length)
								.map(fieldset => {
									return assign(
										fieldset,
										{
											description: $sce.trustAsHtml(fieldset.description)
										}
									);
								});
						});

					ctrl.getPhases = phaseReducer.data;

					ctrl.isCaseStalled = () => get(ctrl.case().data(), 'instance.status') === 'stalled';

					ctrl.getActivePhase = activePhaseNameReducer.data;

					ctrl.getMilestone = () => get(selectedPhaseReducer.data(), 'seq');

					ctrl.getFieldsets = fieldsetReducer.data;

					ctrl.getPhaseTitle = () => get(selectedPhaseReducer.data(), 'name');

					const getDeadlineInfo = () => {
						const phaseData = selectedPhaseReducer.data();
						const days = get(phaseData, 'days');
						const deadline = get(phaseData, 'deadline');

						return { days, deadline };
					}

					ctrl.getPhaseDeadline = () => {
						const { days, deadline } = getDeadlineInfo();

						if(days === null) {
							return;
						}

						return `(${days}\u00A0van\u00A0${deadline})`;
					}

					ctrl.isDeadlineMissed = () => {
						const { days, deadline } = getDeadlineInfo();

						return days > deadline;
					}

					ctrl.advanceButton = advanceButtonReducer.data;

					ctrl.isSidebarCollapsed = oneWayBind();

					caseActionsReducer = composedReducer({ scope: $scope }, ctrl.caseActions, ctrl.getMilestone)
						.reduce((caseActions, milestone) => get(caseActions, milestone));

					ctrl.getActions = () => caseActionsReducer.data();

					checklistsReducer = composedReducer({ scope: $scope }, ctrl.checklists, ctrl.getMilestone)
						.reduce((checklists, milestone) => get(checklists, milestone));

					ctrl.getChecklist = () => checklistsReducer.data();

					ctrl.getValidity = validityReducer.data;

					ctrl.isValid = isValidReducer.data;

					ctrl.getValidationMessages = validityMessagesReducer.data;

					ctrl.getResult = getResult;

					ctrl.getResultOptions = () => get(ctrl.casetype().data(), 'instance.results');

					ctrl.canChangeResult = () => ctrl.canEdit() && !get(ruleResultReducer.data(), 'values["case.result_id"]');

					ctrl.isLastPhase = isLastPhase;

					ctrl.getPendingChange = ( name ) => pendingChangesReducer.data()[name];

					ctrl.getPendingChanges = () => pendingChangesReducer.data();

					ctrl.handleChange = ( name, source ) => {

						let field,
							value,
							ruleValues,
							ruleResult,
							formerRuleResult,
							ruleDifference,
							attribute,
							implied;

						let parseValue = ( val ) => {
							return field.parsers.reduce(( current, reducer ) => {
								return reducer(current);
							}, val);
						};

						field = fieldReducer.data()[name];

						value = field.parsers ?
							(isArray(source) ? source.map(parseValue) : parseValue(source))
							: source;

						ruleValues = assign(
							{},
							getRuleValues(
								fieldReducer.data(),
								ctrl.case().data(),
								assign(
									{},
									...changes.map(change => {
										return assign(
											{ [change.name]: change.value },
											change.implied
										);
									}),
									{ [name]: value }
								)
							)
						);

						ruleResult = getRuleResult(
							immutable(ctrl.rules().data()).asMutable({ deep: true }),
							immutable(ruleValues).asMutable()
						);

						formerRuleResult = ruleResultReducer.data();

						attribute = field.$attribute;

						// collect the implied changes by comparing rule results
						// after/before change, merge values w/ prefill
						// to make sure prefill values are only saved
						// when user does something which changes the prefill value

						ruleDifference =
							pickBy(
								mapValues(ruleResult, ( result, type ) => {

									let former = formerRuleResult[type] || {};

									return pickBy(result, ( val, attrName ) => {
										return !isEqual(former[attrName], val);
									});

								}),
								( val ) => !isEmpty(val)
							);

						implied =
							pickBy(
								mapKeys(
									assign(
										{},
										pickBy(
											assign({}, ruleDifference.values, ruleDifference.prefill),
											( val, key ) => key.replace('attribute.', '') !== name
										),
										mapValues(
											pickBy(ruleDifference.hidden, identity),
											() => null
										)
									),
									( val, key ) => key.replace('attribute.', '')
								),
								( val, key ) => {

									let curField =
										find(
											flatten(
												map(ctrl.casetype().data().instance.phases, 'fields')
											),
											attr => attr.magic_string === key.replace('attribute.', '')
										);

									return !curField || curField.type !== 'file';
								}
							);

						changes = changes.concat({
							name,
							value,
							implied,
							attribute
						});

						if (attribute.type === 'file') {

							let oldIds = map(filter(valueReducer.data()[name], identity), 'uuid'),
								newIds = map(filter(value, identity), 'uuid'),
								added = difference(newIds, oldIds),
								removed = difference(oldIds, newIds),
								promise;

							if (removed.length) {
								promise = $q.all(
									removed.map(( fileId ) => {

										let mutation = ctrl.case().mutate(
											'CASE_FILE_REMOVE',
											{
												caseId: get(ctrl.case().data(), 'instance.number'),
												attributeName: name,
												fileId
											}
										)
											.asPromise();

										return mutation;
									})
								);

							} else if (added.length) {
								promise = ctrl.case().reload();
							}

							if (promise) {
								promise.finally(() => {
									changes = changes.filter(change => change.name !== name);
								});
							}

						}

						validityReducer.onUpdate(() => {

							invalidateChanges();

						}, {
							once: true,
							immediate: false
						});

					};

					ctrl.handleResultChange = ( result_id ) => {

						ctrl.case()
							.mutate('CASE_RESULT_CHANGE', {
								caseId: get(ctrl.case().data(), 'instance.number'),
								result_id
							})
							.asPromise()
							.then(() => ctrl.caseActionResource().reload());

					};

					ctrl.scrollTo = ( name ) => {

						let element,
							focusable;

						if (name === 'case.result') {
							element = $element.find('zs-case-result')[0];
						} else {
							element = $element[0].querySelector(`a[name="${name}"]`).parentNode;
						}

						element.scrollIntoView(true);

						$animate.addClass(element, 'highlighted')
							.finally(() => {
								$animate.removeClass(element, 'highlighted');
							});

						focusable = element.querySelector('input');

						if (focusable) {
							focusable.focus();
						}
					};

					ctrl.getModelOptions = () => modelOptions;

					ctrl.getCaseId = getCaseId;
					ctrl.getCaseUuid = getCaseUuid;

					ctrl.handleAdvanceClick = () => {

						let activePhaseName = activePhaseNameReducer.data(),
							indexOfActivePhase = findIndex(ctrl.casetype().data().instance.phases, ( phase ) => {
								return normalizePhaseName(phase.name) === normalizePhaseName(activePhaseName);
							}),
							nextPhaseName = get(ctrl.casetype().data().instance.phases[indexOfActivePhase + 1], 'name', null),
							redirect =
								{
									resolved: !nextPhaseName,
									allocationChanged: !!get(find(ctrl.getActions(), { type: 'allocation' }), 'automatic', false)
								},
							redirectToDashboard = values(redirect).filter(identity).length;

						// save pending changes, let the mutationService take care of queuing

						isAdvancing = true;

						saveValidChanges()
							.then(() => {

								if(redirectToDashboard) {
									$state.go('home');
								}

								ctrl.case()
									.mutate('CASE_ADVANCE', {
										caseId: getCaseId(),
										nextPhaseName,
										redirect
									})
									.asPromise()
									.then(( data ) => {

										if (nextPhaseName && !redirectToDashboard) {

											$state.go($state.current, { phaseName: normalizePhaseName(nextPhaseName) });

											ctrl.onPhaseAdvance({
												$toPhase: nextPhaseName ? normalizePhaseName(nextPhaseName) : null,
												$taskIds: get(data, 'result[0].queued_item_refs', [])
											});

										} else if (!redirectToDashboard) {
											$state.reload();
										}

									});

							})
							.finally(() => {
								isAdvancing = true;
							});

					};

					ctrl.handleActionAutomaticToggle = ( actionId, automatic ) => {
						ctrl.caseActionResource()
							.mutate('CASE_ACTION_UPDATE', {
								caseId: getCaseId(),
								milestone: ctrl.getMilestone(),
								actionId,
								automatic
							});
					};

					ctrl.handleActionUntaint = ( actionId ) => {
						ctrl.caseActionResource()
							.mutate('CASE_ACTION_UNTAINT', {
								caseId: getCaseId(),
								milestone: ctrl.getMilestone(),
								actionId
							});
					};

					ctrl.handleActionTrigger = ( action, trigger, data ) => {
						return ctrl.caseActionResource()
							.mutate('CASE_ACTION_TRIGGER', {
								caseId: getCaseId(),
								milestone: ctrl.getMilestone(),
								actionId: action.id,
								type: action.type,
								trigger,
								data
							})
							.asPromise();
					};

					ctrl.handleChangeAccept = ( change ) => {
						ctrl.case()
							.mutate('CASE_PENDING_RESOLVE', {
								caseId: getCaseId(),
								bibliotheekKenmerkenId: change.bibliotheek_kenmerken_id,
								name:
								find(selectedPhaseReducer.data().fields, { catalogue_id: Number(change.bibliotheek_kenmerken_id) }).magic_string,
								value: change.value,
								accepted: true
							});
					};

					ctrl.handleChangeReject = ( change ) => {
						ctrl.case()
							.mutate('CASE_PENDING_RESOLVE', {
								caseId: getCaseId(),
								bibliotheekKenmerkenId: change.bibliotheek_kenmerken_id,
								accepted: false
							});
					};

					ctrl.handleMutationCreate = ( name, type, label, objectId, vals ) => {
						return ctrl.mutationResource()
							.mutate('CASE_MUTATION_ADD', {
								caseId: getCaseId(),
								objectType: name.replace('object.', ''),
								mutationType: type,
								objectId,
								values: vals,
								label
							})
							.asPromise();
					};

					ctrl.handleMutationUpdate = ( name, mutation, label, vals ) => {
						return snackbarService.wait('Wijzigingen mutatie worden opgeslagen', {
							promise: ctrl.mutationResource()
								.mutate('CASE_MUTATION_UPDATE', {
									caseId: getCaseId(),
									objectType: name.replace('object.', ''),
									mutation: assign({}, mutation, {
										label,
										values: vals
									})
								})
								.asPromise(),
							then: () => 'Wijziging mutatie opgeslagen'
						});
					};

					ctrl.handleMutationRemove = ( name, mutation ) => {
						return snackbarService.wait('De mutatie wordt verwijderd', {
							promise: ctrl.mutationResource()
								.mutate('CASE_MUTATION_REMOVE', {
									caseId: getCaseId(),
									mutationId: mutation.id,
									objectType: name.replace('object.', '')
								})
								.asPromise(),
							then: () => 'Mutatie verwijderd'
						});
					};

					ctrl.getCaseValues = caseValueReducer.data;

					ctrl.isFieldVisible = ( field ) => {
						let visible = !!field.when();

						return visible;
					};

					ctrl.canEditSidebar = () => {
						return ctrl.canEdit()
							&& !getPhaseInfo(phaseReducer.data(), activePhaseNameReducer.data(), selectedPhaseReducer.data()).phaseResolved;
					};

					ctrl.getCompiler = () => caseAttrTemplateCompiler;

					ctrl.isLocked = phaseLockedReducer.data;

					ctrl.canUnlock = () => get(selectedPhaseReducer.data(), 'locked', false) && ctrl.canEdit();

					ctrl.handleUnlockClick = () => {
						if (ctrl.isLocked()) {
							zsConfirm('Weet u zeker dat u deze fase wilt ontgrendelen? Dit kan juridische risico\'s opleveren.', 'Ontgrendelen')
								.then(() => {
									userUnlocked = true;
								});
						} else {
							userUnlocked = false;
						}
					};

					ctrl.unlockField = ( name ) => {
						unlockedFields = unlockedFields.merge({ [name]: true });
					};

					ctrl.lockField = ( name ) => {
						changes = changes.filter(change => change.name !== name);
						unlockedFields = unlockedFields.merge({ [name]: false });
					};

					ctrl.handleAttributeUpdateRequest = ( name, description ) => {
						let updates,
							field = fieldReducer.data()[name],
							user = ctrl.user(),
							update;

						[changes, updates] = partition(changes, change => change.name !== name);

						update = last(updates) || { value: null };

						ctrl.case().mutate(
							'CASE_ATTRIBUTE_REQUEST_UPDATE',
							{
								caseId: getCaseId(),
								catalogueId: String(field.$attribute.catalogue_id),
								value: update.value,
								description,
								requestorId: `betrokkene-medewerker-${user.id}`,
								requestorName: user.display_name
							}
						);

						unlockedFields = unlockedFields.merge({ [name]: false });
					};

					ctrl.canAcceptPendingChange = () => {
						return ctrl.canEdit()
							&& (
								(queueChangesReducer.data() && ctrl.user().id === get(ctrl.case().data(), 'instance.assignee.id'))
								|| !queueChangesReducer.data()
							);
					};

					ctrl.getPhaseOptions = composedReducer({ scope: $scope }, () => showRuleInfluence, ctrl.user)
						.reduce(( debugRules, user ) => {
							return intersection(['Zaaksysteembeheerder', 'Zaaktypebeheerder', 'Administrator'], user.system_roles).length > 0 ?
								[
									{
										name: 'debug_rules',
										label: `${ debugRules ? 'Verberg' : 'Toon' } regelinvloeden`,
										icon: debugRules ? 'lightbulb' : 'lightbulb-outline',
										click: () => {
											showRuleInfluence = !showRuleInfluence;
										}
									}
								]
								: [];
						}).data;

					phaseStateReducer = composedReducer({ scope: $scope }, () => get(ctrl.casetype().data(), 'instance.phases'), selectedPhaseReducer)
						.reduce(( phases, currentlyActivePhase ) => {
							let currentPhase = currentlyActivePhase.name,
								allPhases = phases.map(phase => phase.name);

							return {
								allPhases,
								currentPhase,
								isLast: last(allPhases) === currentPhase,
								isFirst: first(allPhases) === currentPhase
							};
						});

					ctrl.getPhaseState = phaseStateReducer.data;

					windowUnload.register($scope, () => {
						let hasPending = !isEmpty(changes),
							hasMutations = ctrl.case().mutations().length > 0,
							blockUnload = hasPending || hasMutations;

						if (hasPending) {
							queuedSave = $q.when(queuedSave).finally(saveValidChanges);
						}

						return blockUnload;
					});

					$scope.$on('$destroy', saveValidChanges);

					let statusEl = angular.element($element[0].querySelector('.phase-header-status')),
						phaseAdvanceButton = angular.element($element[0].querySelector('.phase-advance-button-wrapper')),
						hideValidation = true;

					phaseAdvanceButton.bind('mouseenter', () => {
						if (!validityMessagesReducer.data().length) {
							return;
						}

						$scope.$evalAsync(() => {
							hideValidation = false;
						});

						let onMouseLeave = () => {
							statusEl.unbind('mouseleave', onMouseLeave);
							$scope.$evalAsync(() => {
								hideValidation = true;
							});
						};

						statusEl.bind('mouseleave', onMouseLeave);
					});

					ctrl.isValidationVisible = () =>
						(((validityMessagesReducer.data() || []).length > 0) && !hideValidation);

				}],
				controllerAs: 'vm'
			};
		}])
	.name;
