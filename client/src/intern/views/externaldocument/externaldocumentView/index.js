import angular from 'angular';
import composedReducerModule from '../../../../shared/api/resource/composedReducer';
import snackbarServiceModule from '../../../../shared/ui/zsSnackbar/snackbarService';
import externalSearchServiceModule from '../../../../shared/ui/zsSpotEnlighter/externalSearchService';
import controller from './ExternalDocumentViewController';
import template from './template.html';
import './styles.scss';

export default angular
  .module('Zaaksysteem.intern.externaldocument.externalDocumentView', [
    composedReducerModule,
    snackbarServiceModule,
    externalSearchServiceModule
  ])
  .filter('dash', () => (input) => {
    const ZERO_WIDTH_SPACE = '\u200B';

    return input.replace(/(-+)/g, `$1${ZERO_WIDTH_SPACE}`);
  })
  .component('externalDocumentView', {
    bindings: {
      document: '&',
      thumbnail: '&',
      relatedDocuments: '&'
    },
    controller,
    template
  })
  .name;
