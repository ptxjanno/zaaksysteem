import getParamsFromResourceError from './getParamsFromResourceError';
import propCheck from './../../../shared/util/propCheck';

export default ( resource, $state, options = {} ) => {

	propCheck.throw(
		propCheck.shape({
			resource: propCheck.object,
			$state: propCheck.object,
			options: propCheck.shape({
				description: propCheck.string.optional
			}).optional
		}),
		{
			resource,
			$state,
			options
		}
	);

	resource.onError(err => {
		$state.go('error', getParamsFromResourceError(err, { description: options.description }), { location: false });
	});

	return resource;

};
