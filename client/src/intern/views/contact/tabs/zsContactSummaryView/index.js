import angular from 'angular';
import template from './template.html';
import composedReducerModule from '../../../../../shared/api/resource/composedReducer';
import zsTimelineModule from '../../../../../shared/ui/zsTimeline';
import { extract } from '../../api/contact';
import partition from 'lodash/partition';
import './styles.scss';

export default
	angular.module('zsContactSummaryView', [
		composedReducerModule,
		zsTimelineModule
	])
		.component('zsContactSummaryView', {
			bindings: {
				subject: '&',
				cases: '&',
				contactMoments: '&'
			},
			controller: [ '$scope', '$sce', 'composedReducer', function ( scope, sce, composedReducer ) {

				let ctrl = this,
					subjectInformationReducer,
					casesReducer;

				const WIDGETS =
					partition(
						[ 'timeline', 'cases', 'information' ].map( widget => {

							let label,
								moreLabel;

							switch (widget) {
								case 'timeline':
								label = 'Recente contactmomenten';
								moreLabel = 'timeline';
								break;
							
								case 'cases':
								label = 'Openstaande zaken';
								moreLabel = 'alle zaken';
								break;

								case 'information':
								label = 'Gegevens';
								moreLabel = 'alle gegevens';
								break;
							}
							
							return {
								name: widget,
								label,
								state: `contact.${widget}`,
								moreLabel
							};
						}),
					( widget ) => widget.name === 'timeline'
				);

				const COLUMNS = ['col1', 'col2'].map( col => {
					return {
						name: col,
						children: col === 'col1' ? WIDGETS[0] : WIDGETS[1]
					};
				});

				ctrl.getSubjectReference = ( ) => ctrl.subject().data().reference;

				subjectInformationReducer = composedReducer({ scope }, ctrl.subject())
					.reduce(subject => {
						if (subject) {
							const summary = extract(subject, sce, false)
								.filter(descriptor => descriptor.summary);

							return summary;
						}
					});

				ctrl.getSubjectInformation = subjectInformationReducer.data;

				casesReducer = composedReducer({ scope }, ctrl.cases() )
					.reduce( cases => {
						return cases;
					});

				ctrl.getCases = casesReducer.data;

				ctrl.getWidgets = ( ) => COLUMNS;

				ctrl.getRelevantEventTypes = ( ) => {
					return ['subject/contact_moment/create', 'subject/contactmoment/create', 'contactmoment/note'];
				};

			}],
			template
		})
		.name;
