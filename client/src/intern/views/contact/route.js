import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import ocLazyLoadModule from 'oclazyload/dist/ocLazyLoad';
import resourceModule from './../../../shared/api/resource';
import composedReducerModule from './../../../shared/api/resource/composedReducer';
import snackbarServiceModule from './../../../shared/ui/zsSnackbar/snackbarService';
import get from 'lodash/get';
import first from 'lodash/head';
import includes from 'lodash/includes';
import assignToScope from './../../../shared/util/route/assignToScope';
import template from './template.html';
import getFullName from '../../../shared/util/subject/getFullName';

export default
	angular.module('Zaaksysteem.intern.contact.route',
		[
			angularUiRouterModule,
			ocLazyLoadModule,
			resourceModule,
			composedReducerModule,
			snackbarServiceModule
		]
	)
		.config([ '$stateProvider', '$urlMatcherFactoryProvider', ( $stateProvider, $urlMatcherFactoryProvider ) => {

			$urlMatcherFactoryProvider.strictMode(false);

			$stateProvider
				.state('contact', {
					url: '/contact/{subjectUuid:string}',
					resolve: {
						timeout: [ '$timeout', ( $timeout ) => {
							return $timeout(angular.noop, 50);
						}],
						subject: [ '$rootScope', '$q', '$state', '$stateParams', 'snackbarService', 'resource', ( $rootScope, $q, $state, $stateParams, snackbarService, resource ) => {

							let subjectResource =
								resource({
									url: `/api/v1/subject/${$stateParams.subjectUuid}`
								},
								{ scope: $rootScope })
									.reduce( ( requestOptions, data ) => {

										return first(data) ? first(data) : null;

									});

							return subjectResource.asPromise()
								.then(( ) => subjectResource)
								.catch( response => {

									let type = get(first(response.data.result), 'type'),
										msg;

									switch (type) {
										case 'api/v1/subject/forbidden':
										msg = 'U heeft niet voldoende rechten om deze betrokkene te bekijken.';
										break;

										case 'api/v1/subject/invalid_uuid':
										case 'api/v1/subject/uuid_not_found':
										msg = 'Er kon geen betrokkene gevonden worden met dit identificatiekenmerk.';
										break;

										default:
										msg = 'Er ging iets mis bij het laden van deze betrokkene.';
										break;
									}

									snackbarService.error(`${msg} Neem contact op met uw beheerder voor meer informatie.`, {
										actions: [
											{
												type: 'link',
												label: 'Terug naar dashboard',
												link: $state.href('home')
											}
										]
									});

									return $q.reject(response);

								});

						}],
						cases: [ '$rootScope', '$q', '$state', '$stateParams', 'snackbarService', 'resource', 'subject', ( $rootScope, $q, $state, $stateParams, snackbarService, resource, subject ) => {
							
							let casesResource =
								resource({
									url: `/api/v1/subject/${subject.data().reference}/cases/owned`,
									params: {
										zql: 'SELECT {} FROM case WHERE (case.status IN ("open", "new")) NUMERIC ORDER BY case.number',
										rows_per_page: 3
									}
								},
								{ scope: $rootScope })
									.reduce( ( requestOptions, data ) => {

										return data ? data : null;
									});

							return casesResource.asPromise()
								.then(( ) => casesResource)
								.catch( response => {

									let type = get(first(response.data.result), 'type'),
										msg;

									switch (type) {
										case 'api/v1/case/query_fault':
										msg = 'Er is een fout opgetreden bij het opbouwen va de zoekactie om de zaken op te halen.';
										break;

										case 'api/v1/case/retrieval_fault':
										default:
										msg = 'Er ging iets mis bij het ophalen van de zaken.';
										break;
									}


									snackbarService.error(`${msg} Neem contact op met uw beheerder voor meer informatie.`, {
										actions: [
											{
												type: 'link',
												label: 'Terug naar dashboard',
												link: $state.href('home')
											}
										]
									});

									return $q.reject(response);

								});

						}],
						module: [ '$rootScope', '$ocLazyLoad', '$q', ( $rootScope, $ocLazyLoad, $q ) => {

							return $q(( resolve/*, reject*/ ) => {

								require([ './', './controller' ], ( ...names ) => {

									$rootScope.$evalAsync( ( ) => {
										$ocLazyLoad.load(names.map(name => {
											return { name };
										}));

										resolve();
									});

								});

							});
						}]
					},
					template,
					controller: 'ContactController',
					controllerAs: 'vm',
					onActivate: [ '$state', '$location', 'auxiliaryRouteService', ( $state, $location, auxiliaryRouteService ) => {

						let names = auxiliaryRouteService.parseNames($state.current.name, $location.url());

						if (names.base === 'contact') {
							$state.go('contact.summary', { location: false });
						}

					}],
					title: [ 'subject', ( subjectResource ) => getFullName(subjectResource.data()) ],
					actions: [
					]
				})
				.state('contact.summary', {
					url: '/overzicht/',
					title: 'Overzicht',
					template:
						`<zs-contact-summary-view
							data-subject="subject"
							data-cases="cases"
						></zs-contact-summary-view>`,
					controller: assignToScope('subject', 'cases'),
					params: {
						tab: { squash: true }
					}
				})
				.state('contact.cases', {
					url: '/zaken/',
					template:
						`<zs-contact-cases-view
							data-subject="subject"
							data-cases="cases"
						></zs-contact-cases-view>`,
					title: 'Zaken',
					controller: assignToScope('subject', 'cases'),
					params: {
						tab: { squash: true }
					}
				})
				.state('contact.timeline', {
					url: '/timeline/',
					title: 'Timeline',
					controller: assignToScope('subject'),
					template:
						`<zs-contact-timeline-view
							data-subject="subject"
						></zs-contact-timeline-view>`,
					params: {
						tab: { squash: true }
					}
				})
				.state('contact.information', {
					url: '/gegevens/',
					title: 'Gegevens',
					controller: assignToScope('subject'),
					template:
						`<zs-contact-information-view
							data-subject="subject"
						></zs-contact-information-view>`,
					params: {
						tab: { squash: true }
					}
				})
				.state('contact.domains', {
					url: '/omgevingen',
					title: 'Omgevingen',
					template:
						`<zs-contact-domains-view
							data-domains="domains"
						>
						</zs-contact-domains-view>`,
					resolve: {
						domains: [ '$rootScope', '$q', 'resource', 'user', ( $rootScope, $q, resource, user ) => {

							let domainsResource =
								includes(user.data().instance.logged_in_user.capabilities, 'admin')
								|| includes(user.data().instance.logged_in_user.capabilities, 'beheer') ?
									resource(
										'/api/v1/controlpanel',
										{ scope: $rootScope }
									)
								: null;

							return domainsResource.asPromise()
								.then(( ) => domainsResource)
								.catch( err => {

									console.error(err);
									
									return $q.resolve(domainsResource);
								});

						}]
					},
					controller: [
						'$scope', function ( $scope ) {

							// let ctrl;

							$scope.$on('$destroy', ( ) => {
								
								// resourcesToDestroy.forEach(res => {
								// 	res.destroy();
								// });
							});

						}],
						controllerAs: 'relationVm'
				})
				.state('contact.settings', {
					url: '/instellingen/',
					title: 'Instellingen',
					template:
						`<zs-contact-settings-view
							data-contact="contact"
						></zs-contact-settings-view>`,
					params: {
						tab: { squash: true }
					}
				});
		}])
		.name;
