import {
	getValue,
	getAddressValue,
	getBooleanValue,
	getDateValue,
	getEmailValue,
	getGenderValue,
	getSourceValue
} from './format';

const summary = true;

export default [
	{
		summary,
		key: 'external_subscription',
		label: 'Bron',
		getValue: getSourceValue
	},
	{
		summary,
		key: 'personal_number',
		label: 'BSN',
		getValue
	},
	{
		summary,
		key: 'first_names',
		label: 'Voornamen',
		getValue
	},
	{
		summary,
		key: 'surname',
		label: 'Achternaam',
		getValue
	},
	{
		key: 'gender',
		label: 'Geslacht',
		getValue: getGenderValue
	},
	{
		summary,
		key: 'date_of_birth',
		label: 'Geboortedatum',
		getValue: getDateValue
	},
	{
		summary,
		key: 'date_of_death',
		label: 'Overlijdensdatum',
		getValue: getDateValue,
		noEmpty: true
	},
	{
		key: 'is_local_resident',
		label: 'Binnengemeentelijk',
		getValue: getBooleanValue
	},
	{
		summary,
		key: 'address_residence',
		label: 'Verblijfadres',
		getValue: getAddressValue
	},
	{
		summary,
		key: 'address_correspondence',
		label: 'Briefadres',
		getValue: getAddressValue,
		noEmpty: true
	},
	{
		key: 'phone_number',
		label: 'Telefoonnummer',
		getValue
	},
	{
		key: 'mobile_phone_number',
		label: 'Telefoonnummer (mobiel)',
		getValue
	},
	{
		key: 'email_address',
		label: 'E-mailadres',
		getValue: getEmailValue
	}
];
