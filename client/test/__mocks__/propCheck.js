const check = require('api-check');

module.exports = check({
  verbose: false,
  disabled: false,
});
