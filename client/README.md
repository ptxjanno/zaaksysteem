# Zaaksysteem /client

## Application code: `/client/src`

Directories in `src`:

- `shared`: shared modules 
- everything else is a single page app
  - `intern` is the main Zaaksysteem app

## `/client/src/intern`

### `./views`

Dynamically loaded top-level `intern` routes.

Cf. `require.context` call in `/client/src/intern/routing.js`

## Cross dependencies

Some parts of `/client` depend on code in `/frontend`; 
the offending modules are:

- `/client/src/intern/views/case/zsCaseDocumentView/index.js`
- `/client/src/intern/views/case/zsCaseTimelineView/index.js`

Reversely, `/frontend` depends on code in `client` for 
some global layout elements, like the Material Toolbar, 
Drawer and Flotaing Action Button components. These are
**not** shared as *production* code though, i.e. if you
make changes to those components in `/client` you need 
to run the `webpack` build in `/frontend` to see them 
there.

## Trouble shooting

### I have `webpack-dev-server` running but I don't see any changes

1. Make sure you have a volume entry for 
   `/root/assets` in your `docker-compose.override.yml`
2. If you accessed `/intern` in your local environment with a 
   production build before, you have a service worker installed
   and your browser won't do network requests until it expires.
   Remove the service worker and refresh the page.
    - Google Chrome: 
      Developer Tools > Application > Service Workers > [host name] > Unregister

## See also

- `webpack/README.md` for more information about the build.
- `test/README.md` for more information about tesing.
